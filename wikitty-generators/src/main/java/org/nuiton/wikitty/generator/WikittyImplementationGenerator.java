/*
 * #%L
 * Wikitty :: generators
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.generator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

/*{generator option: writeString = }*/
/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * Wikitty implementation class generator.
 * 
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.wikitty.generator.WikittyImplementationGenerator"
 */
public class WikittyImplementationGenerator extends ObjectModelTransformerToJava {

    /** Logger */
    private static final Log log = LogFactory.getLog(WikittyImplementationGenerator.class);

    protected Map<ObjectModelClass, ObjectModelClass> processedClasses =
                              new HashMap<ObjectModelClass, ObjectModelClass>();
    
    @Override
    public void transformFromClass(ObjectModelClass clazz) {
        if (isGenerateImpl(clazz)) {

            if (WikittyTransformerUtil.isBusinessEntity(clazz)) {
                ObjectModelClass implementation = prepareImplementation(clazz);
                processBusinessEntity(clazz, implementation);
            }

            if (WikittyTransformerUtil.isMetaExtension(clazz)) {
                ObjectModelClass implementation = prepareImplementation(clazz);
                processMetaExtension(clazz, implementation);
            }
        }
    }

    protected boolean isGenerateImpl(ObjectModelClass clazz) {

        Collection<ObjectModelOperation> operations = clazz.getOperations();
        String fqn = clazz.getQualifiedName() + "Impl";

        boolean alreadyInClassPath = isInClassPath(fqn);
        if (alreadyInClassPath) {

            return false;
        }

        // On ne génère pas le impl si l'entité a des opérations
        if (!operations.isEmpty()) {

            log.info("Will not generate [" + fqn + "], there is some operations to manually implement");
            return false;
        }

        //De même, on ne génère pas le impl si il y a des opérations venant des
        // superclasses non implémentées
        for (ObjectModelOperation otherOp : clazz.getAllOtherOperations(false)) {
            if (otherOp.isAbstract()) {
                log.info("Will not generate [" + fqn + "], there is an abstract operation [" + otherOp.getName() + "] in allOtherOperations.");
                return false;
            }
        }

        return true;
    }

    protected ObjectModelClass prepareImplementation(ObjectModelClass clazz) {
        ObjectModelClass implementation = processedClasses.get(clazz);
        
        if (implementation == null) {

            implementation = createClass(
                    WikittyTransformerUtil.businessEntityToImplementationName(clazz),
                    clazz.getPackageName());

            // TODO 20100811 bleny remove unused imports
            addImport(implementation, WikittyTransformerUtil.BUSINESS_ENTITY_CLASS_FQN);
            addImport(implementation, WikittyTransformerUtil.BUSINESS_ENTITY_WIKITTY_CLASS_FQN);
            addImport(implementation, WikittyTransformerUtil.WIKITTY_CLASS_FQN);
            addImport(implementation, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyExtension");
            addImport(implementation, "org.nuiton.wikitty.WikittyUtil");
            addImport(implementation, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyUser");
            addImport(implementation, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyUserAbstract");
            addImport(implementation, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyUserImpl");
            addImport(implementation, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyTreeNode");
            addImport(implementation, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyTreeNodeAbstract");
            addImport(implementation, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyTreeNodeImpl");
            addImport(implementation, List.class);
            addImport(implementation, ArrayList.class);
            addImport(implementation, Collection.class);
            addImport(implementation, Collections.class);
            addImport(implementation, Set.class);
            addImport(implementation, Date.class);
            addImport(implementation, LinkedHashSet.class);

            setSuperClass(implementation, WikittyTransformerUtil.businessEntityToAbstractName(clazz));

            // adding a generated serialVersionUID
            Long serialVersionUIDs = GeneratorUtil.generateSerialVersionUID(clazz);
            addConstant(implementation, "serialVersionUID", "long",
            serialVersionUIDs.toString() + "L", ObjectModelModifier.PRIVATE);

            processedClasses.put(clazz, implementation);
        }
        
        return implementation;
    }
    
    protected void processBusinessEntity(ObjectModelClass clazz,
                                         ObjectModelClass implementation) {
        
        // adding constructor
        ObjectModelOperation constructor = addConstructor(implementation, ObjectModelModifier.PUBLIC);
        setOperationBody(constructor, ""
/*{
        super();
}*/);

        constructor = addConstructor(implementation, ObjectModelModifier.PUBLIC);
        addParameter(constructor, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
        setOperationBody(constructor, ""
/*{
        super(wikitty);
}*/);

        constructor = addConstructor(implementation, ObjectModelModifier.PUBLIC);
        addParameter(constructor, WikittyTransformerUtil.BUSINESS_ENTITY_WIKITTY_CLASS_FQN, "businessEntityImpl");
        setOperationBody(constructor, ""
/*{
        super(businessEntityImpl.getWikitty());
}*/);

    }

    protected void processMetaExtension(ObjectModelClass metaExtension,
                                        ObjectModelClass implementation) {


        ObjectModelOperation constructor = addConstructor(implementation, ObjectModelModifier.PUBLIC);
        addParameter(constructor, WikittyTransformerUtil.WIKITTY_EXTENSION_CLASS_FQN, "extension");
        addParameter(constructor, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
        String contractName = WikittyTransformerUtil.businessEntityToContractName(metaExtension);
        setOperationBody(constructor, ""
/*{
        this.wikitty = wikitty;
        setExtensionForMetaExtension(extension);
}*/);
    }
}
