/*
 * #%L
 * Wikitty :: generators
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.generator;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*{generator option: writeString = }*/
/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * Wikitty abstract class generator.
 * 
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.wikitty.generator.WikittyAbstractGenerator"
 */
public class WikittyAbstractGenerator extends ObjectModelTransformerToJava implements WikittyTagValue {

    private static final Log log = LogFactory.getLog(WikittyAbstractGenerator.class);

    /** map busines entity from source model to generated abstract class */
    protected Map<ObjectModelClass, ObjectModelClass> processedClasses = 
                new HashMap<ObjectModelClass, ObjectModelClass>();

    /** map "Client.name" to "getName()" or any getter to read this attribute
     * those getter names are stored while generating in order to be found
     * later when generating toString() */
    protected Map<String, String> attributeToGetterName = new HashMap<String, String>();

//    @Override
//    protected Transformer<ObjectModel, ObjectModel> initPreviousTransformer() {
//        return new WikittyPurifierTransformer();
//    }

    protected List<ObjectModelClass> entitiesWithInheritedOperations =
                                              new ArrayList<ObjectModelClass>();

    /** an entity with a dependency must add the imports defined as values
     * in the map. By filling and reading this map, imports while spread across
     * the levels of dependencies
     */
    protected Map<ObjectModelClass, List<String>> requiredDependencyImports =
                                  new HashMap<ObjectModelClass, List<String>>();

    @Override
    public void transformFromModel(ObjectModel model) {

        // contains boths businessEntities and metaExtensions
        // elements may have one of the two sterotypes or both
        // but elements with none of them aren't in this list
        List<ObjectModelClass> modelBoth = new ArrayList<ObjectModelClass>();

        // fill modelBusinessEntities with entities found in model
        for (ObjectModelClass clazz : model.getClasses()) {
            if (WikittyTransformerUtil.isBusinessEntity(clazz) ||
                WikittyTransformerUtil.isMetaExtension(clazz)) {
                modelBoth.add(clazz);
            }
        }

        for (ObjectModelClass clazz : modelBoth) {
            ObjectModelClass abstractClass = createAbstractClass(
                    WikittyTransformerUtil.businessEntityToAbstractName(clazz),
                    clazz.getPackageName());
            processedClasses.put(clazz, abstractClass);
            setSuperClass(abstractClass, "BusinessEntityImpl");
            addInterface(abstractClass, clazz.getQualifiedName());
        }

        for (ObjectModelClass clazz : modelBoth) {
            if (WikittyTransformerUtil.isMetaExtension(clazz)) {
                addMetaExtensionOperations(clazz, processedClasses.get(clazz));
            } else if (WikittyTransformerUtil.isBusinessEntity(clazz)) {
                addOperations(clazz, processedClasses.get(clazz));
            }
        }
        
        // at this time, all operations in generated abstracts are just the operations
        // like get/set etc. we will copy all operations of a given class to all children
        // that's why constructors and others operations are not yet added
        for (ObjectModelClass clazz : modelBoth) {
            if (WikittyTransformerUtil.isBusinessEntity(clazz)) {
                addInheritedOperations(clazz, processedClasses.get(clazz));
            }
        }

        for (ObjectModelClass clazz : modelBoth) {
            ObjectModelClass abstractClassForThisEntity = processedClasses.get(clazz);
            addImports(abstractClassForThisEntity);
            addConstructors(abstractClassForThisEntity);
            addConstants(clazz, abstractClassForThisEntity);
            addToString(clazz, abstractClassForThisEntity);
            
            // adding a generated serialVersionUID
            Long serialVersionUIDs = GeneratorUtil.generateSerialVersionUID(clazz);
            addConstant(abstractClassForThisEntity, "serialVersionUID", "long",
            serialVersionUIDs.toString() + "L", ObjectModelModifier.PRIVATE);
        }

        processedClasses.clear();
    }

    protected void addImports(ObjectModelClass clazz) {
        // TODO 20100811 bleny remove unused imports
        addImport(clazz, WikittyTransformerUtil.BUSINESS_ENTITY_CLASS_FQN);
        addImport(clazz, WikittyTransformerUtil.BUSINESS_ENTITY_WIKITTY_CLASS_FQN);
        addImport(clazz, WikittyTransformerUtil.WIKITTY_CLASS_FQN);
        addImport(clazz, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyExtension");
        addImport(clazz, "org.nuiton.wikitty.WikittyUtil");
        addImport(clazz, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyUser");
        addImport(clazz, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyUserAbstract");
        addImport(clazz, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyUserImpl");
        addImport(clazz, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyTreeNode");
        addImport(clazz, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyTreeNodeAbstract");
        addImport(clazz, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyTreeNodeImpl");
        addImport(clazz, List.class);
        addImport(clazz, ArrayList.class);
        addImport(clazz, Collection.class);
        addImport(clazz, Collections.class);
        addImport(clazz, Set.class);
        addImport(clazz, Date.class);
        addImport(clazz, LinkedHashSet.class);
    }

    protected void addConstructors(ObjectModelClass clazz) {

        ObjectModelOperation constructor = addConstructor(clazz, ObjectModelModifier.PUBLIC);
        setOperationBody(constructor, ""
/*{
        super();
}*/);

        constructor = addConstructor(clazz, ObjectModelModifier.PUBLIC);
        addParameter(constructor, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
        setOperationBody(constructor, ""
/*{
        super(wikitty);
}*/);

        constructor = addConstructor(clazz, ObjectModelModifier.PUBLIC);
        addParameter(constructor, WikittyTransformerUtil.BUSINESS_ENTITY_WIKITTY_CLASS_FQN, "businessEntityImpl");
        setOperationBody(constructor, ""
/*{
        super(businessEntityImpl.getWikitty());
}*/);

    }

    protected void addConstants(ObjectModelClass businessEntity, ObjectModelClass abstractClass) {
        // adding some constants about extension to abstract
        addConstant(abstractClass, "extensions", "List<WikittyExtension>", null, ObjectModelModifier.PUBLIC);
        addConstant(abstractClass, "extension" + businessEntity.getName(), "WikittyExtension", null, ObjectModelModifier.PUBLIC);

        // ... and a getter
        ObjectModelOperation getStaticExtensions = addOperation(abstractClass, "getStaticExtensions", "Collection<WikittyExtension>", ObjectModelModifier.PUBLIC);
        addAnnotation(abstractClass, getStaticExtensions, "Override");
        setOperationBody(getStaticExtensions, ""
/*{
        return extensions;
}*/);

        //// preparing a static block to initialize those constants
        ObjectModelOperation staticInitialization = addBlock(abstractClass, ObjectModelModifier.STATIC);

        // generating constructor call for extensionClient
        // we will build a string to write a call
        List<String> buildFieldMapExtensionParameters = new ArrayList<String>();


        int fieldIndex = 0;
        // now process attributes
        for(ObjectModelAttribute attribute : businessEntity.getAttributes()) {
            if (attribute.isNavigable()) {
                // now add the attribute to the piece of code that build the extension
                String wikittyType = WikittyTransformerUtil.typeToWikittyColumn(attribute.getType());
                String multiplicity = "";
                if (attribute.getMinMultiplicity() != 1 ||
                    attribute.getMaxMultiplicity() != 1) {
                    // generate a string like [1-10] or [0-*] etc.
                    multiplicity = "["
                                 + attribute.getMinMultiplicity()
                                 + "-"
                                 + (attribute.getMaxMultiplicity() == -1 ? "*" : attribute.getMaxMultiplicity())
                                 + "]";
                }

                // le tag value unique ne doit exister que si c'est une collection
                // ou que l'utilisateur a force sa creation a la main avec l'ajout
                // explicite d'un tag/value
                if (attribute.getMaxMultiplicity() > 1 || attribute.getMaxMultiplicity() <0) {
                    // generate a string line like " unique=true" if not already
                    // present in tag/value
                    if (attribute.isUnique() && !attribute.getTagValues().containsKey(TAG_UNIQUE)) {
                        attribute.getTagValues().put(TAG_UNIQUE, "true");
                    }
                }

                // si l'attribut n'a pas d'index on lui ajoute sa position par defaut
                fieldIndex++;
                if (null == attribute.getTagValue(TAG_FIELD_INDEX)) {
                    attribute.getTagValues().put(TAG_FIELD_INDEX, String.valueOf(fieldIndex));
                }

                // si le champs est de type wikitty et qu'il n'y a pas de tag value
                // on regarde s'il faut en generer un
                if (StringUtils.equals("Wikitty", wikittyType)
                        && null == attribute.getTagValue(TAG_ALLOWED)) {
                    String type = WikittyTransformerUtil.FQNtoSimpleName(attribute.getType());
                    if (!StringUtils.equals("Wikitty", type)) {
                        // dans le modele on a specifie un type particulier, on
                        // le met comme contrainte
                        attribute.getTagValues().put(TAG_ALLOWED, type);
                    }
                }

                String tagvalue = "";
                for (Map.Entry<String, String> tv : attribute.getTagValues().entrySet()) {
                    tagvalue += " " + tv.getKey() + "=\\\"" + StringEscapeUtils.escapeJava(tv.getValue()) + "\\\"";
                }
                buildFieldMapExtensionParameters.add("" // generate a line like '"Wikitty attributName[0-*] unique=true deprecated=true documentation=\"my documentation\""'
/*{                                             "<%=wikittyType%> <%=attribute.getName()%><%=multiplicity%><%=tagvalue%>"}*/);
            }
        }
        
        // finishing static block
        String extensionVersion = businessEntity.getTagValue("version");
        if (extensionVersion == null || "".equals(extensionVersion)) {
            extensionVersion = "1.0";
            log.warn("no version specified in model for " + businessEntity.getQualifiedName() + " using " + extensionVersion);
        }
        
        // a piece of code used in the static block
        String requires = "";
        String requiresSep = "";
        for (ObjectModelClass superClass : businessEntity.getSuperclasses()) {
            // using "for" but there will be 0 or 1 iteration
            addImport(abstractClass, superClass);
            addImport(abstractClass, superClass.getQualifiedName() + "Abstract");
            requires += requiresSep + WikittyTransformerUtil.classToExtensionVariableName(superClass, true);
            requiresSep = " + \",\" + ";
        }
        if (requires.isEmpty()) {
            requires = "(List)null";
        }

        String tagValues = StringEscapeUtils.escapeJava(
                WikittyTransformerUtil.tagValuesToString(businessEntity.getTagValues()));
        String buildFieldMapExtensionParametersInLine =
                StringUtils.join(buildFieldMapExtensionParameters, ", \n");
        String extensionVariableName =
                WikittyTransformerUtil.classToExtensionVariableName(businessEntity, false);
        String staticInitializationBody = ""
/*{
        extension<%=businessEntity.getName()%> = new WikittyExtension(<%=extensionVariableName%>,
                "<%=extensionVersion%>", // version
                WikittyUtil.tagValuesToMap("<%=tagValues%>"), // tag/values
                <%= requires %>,
                WikittyUtil.buildFieldMapExtension( // building field map
                    <%=buildFieldMapExtensionParametersInLine%>));

        // init extensions
        List<WikittyExtension> exts = new ArrayList<WikittyExtension>();
}*/;

        for (ObjectModelClass superClass : businessEntity.getSuperclasses()) {
            // using "for" but there will be 0 or 1 iteration
            staticInitializationBody += ""
/*{
        exts.addAll(<%=superClass.getName()%>Abstract.extensions); 
        // current after requires ones
}*/;
        }

        staticInitializationBody += ""
/*{
        exts.add(extension<%=businessEntity.getName()%>);
        extensions = Collections.unmodifiableList(exts);
}*/;
        setOperationBody(staticInitialization, staticInitializationBody);
        
    }
    
    protected void addOperations(ObjectModelClass businessEntity, ObjectModelClass abstractClass) {
        
        String helperClassName = WikittyTransformerUtil.businessEntityToHelperName(businessEntity);

        // generating operations with bodies to realize contract
        for (ObjectModelAttribute attribute : businessEntity.getAttributes()) {
            if (attribute.isNavigable()) {

                // there is a conflict, purifier transformer give as the right name to use
                String attributeName;
                if (businessEntity.hasTagValue(TAG_ALTERNATIVE_NAME + "." + businessEntity.getName() + "." + attribute.getName())) {
                    attributeName = businessEntity.getTagValue(TAG_ALTERNATIVE_NAME + "." + businessEntity.getName() + "." + attribute.getName());
                } else if (attribute.hasTagValue(TAG_ALTERNATIVE_NAME)) {
                    attributeName = attribute.getTagValue(TAG_ALTERNATIVE_NAME);
                } else {
                    attributeName = attribute.getName();
                }

                // le nom du getter peut avoir été renommé pour toute la hierarchie
                String helperGetterSetterName;
                if (attribute.hasTagValue(TAG_ALTERNATIVE_NAME)) {
                    helperGetterSetterName = attribute.getTagValue(TAG_ALTERNATIVE_NAME);
                } else {
                    helperGetterSetterName = attribute.getName();
                }

                addOperationWithName(businessEntity, abstractClass, attribute, attributeName, helperClassName, helperGetterSetterName);
            }
        }
    }

    /**
     * Methode de génération des methodes (avec corps) qui permet de generer
     * des methodes avec des nom (et corps) différent suivant si on l'appel
     * pour générer le operation de la classe actuelle ou celles de ses
     * super classes.
     * 
     * @param businessEntity class du modèle
     * @param abstractClass class a générer
     * @param attribute attribut dont on veut generer les opérations
     * @param getterSetterName le nom du getter (peut etre différent du nom
     *      de l'attribut dans le cas d'un heritage multiples)
     * @param helperClassName le nom du helper qui gere l'acces au parametre
     *      (celui de la classe courante ou un de ses supper class)
     * @param helperGetterSetterName le nom du getter a appeler pour le helper
     *      concerné
     */
    protected void addOperationWithName(ObjectModelClass businessEntity, ObjectModelClass abstractClass,
            ObjectModelAttribute attribute, String getterSetterName, String helperClassName, String helperGetterSetterName) {
        
        String extensionVariableName = WikittyTransformerUtil.classToExtensionVariableName(businessEntity, true);

        // needed below, in templates
        String fieldVariableName = WikittyTransformerUtil.attributeToFielVariableName(attribute, true);
        String attributeType = WikittyTransformerUtil.generateResultType(attribute, false);
        String attributeObjectType = WikittyTransformerUtil.generateResultObjectType(attribute, false);

        // getter can have a different name than attribute.getName()
        // but attribute.getName() must styll be used to access to helper getter method
        String attributeName = attributeName = attribute.getName();
        String capitalizedAttributeName = StringUtils.capitalize(attributeName);
        String capitalizedHelperGetterSetterName= StringUtils.capitalize(helperGetterSetterName);
        String capitalizedGetterSetterName = StringUtils.capitalize(getterSetterName);
        
        String getterName;

        if (WikittyTransformerUtil.isAttributeCollection(attribute)) {
            // attributed is a collection, we will generate operations get, add, remove and clear

            String attributeTypeCollectionStrict =
                    WikittyTransformerUtil.generateResultType(attribute, true);
            String attributeTypeCollectionGeneric =
                    "Collection<" + attributeType + ">";
            String attributeTypeVarargs = attributeType + "...";
            
            String attributeObjectTypeCollectionStrict =
                    WikittyTransformerUtil.generateResultObjectType(attribute, true);
            String attributeObjectTypeCollectionGeneric =
                    "Collection<" + attributeObjectType + ">";
            String attributeObjectTypeVarargs = attributeObjectType + "...";

            String getFieldMethodName = WikittyTransformerUtil.generateGetFieldAsCall(attribute);

            // now, for this attribute, we will generate add, remove and clear methods
            // adding operations to contract
            getterName = "get" + capitalizedGetterSetterName;
            ObjectModelOperation getter = addOperation(
                    abstractClass, getterName, attributeTypeCollectionStrict);
            addAnnotation(abstractClass, getter, "Override");
            String getterBody = ""
/*{
<%=attributeTypeCollectionStrict%> result = <%=helperClassName%>.get<%=capitalizedAttributeName%>(getWikitty());
return result;
}*/;
            setOperationBody(getter, getterBody);

            String setterName = "set" + capitalizedGetterSetterName;
            ObjectModelOperation setter = addOperation(abstractClass, setterName, "void");
            addAnnotation(abstractClass, setter, "Override");
            addParameter(setter, attributeTypeCollectionStrict, getterSetterName);
            String setterBody = ""
/*{
<%=attributeTypeCollectionStrict%> oldValue = get<%=capitalizedAttributeName%>();
<%=helperClassName%>.set<%=capitalizedHelperGetterSetterName%>(getWikitty(), <%=attributeName%>);
getPropertyChangeSupport().firePropertyChange(<%=fieldVariableName%>, oldValue, <%= getter.getName() %>());
}*/;
            setOperationBody(setter, setterBody);

            String addAllName = "addAll" + capitalizedGetterSetterName;
            ObjectModelOperation addAll = addOperation(abstractClass, addAllName, "void");
            addAnnotation(abstractClass, addAll, "Override");
            addParameter(addAll, attributeTypeCollectionGeneric, attributeName);
            String addAllBody = ""
/*{
<%=attributeTypeCollectionStrict%> oldValue = get<%=capitalizedAttributeName%>();
<%=helperClassName%>.addAll<%=capitalizedHelperGetterSetterName%>(getWikitty(), <%=attributeName%>);
getPropertyChangeSupport().firePropertyChange(<%=fieldVariableName%>, oldValue, <%= getter.getName() %>());
}*/;
            setOperationBody(addAll, addAllBody);

            String addName = "add" + capitalizedGetterSetterName;
            ObjectModelOperation adder = addOperation(abstractClass, addName, "void");
            addAnnotation(abstractClass, adder, "Override");
            addParameter(adder, attributeTypeVarargs, "element");
            String adderBody = ""
/*{
<%=attributeTypeCollectionStrict%> oldValue = get<%=capitalizedAttributeName%>();
<%=helperClassName%>.add<%=capitalizedHelperGetterSetterName%>(getWikitty(), element);
getPropertyChangeSupport().firePropertyChange(<%=fieldVariableName%>, oldValue, <%= getter.getName() %>());
}*/;
            setOperationBody(adder, adderBody);

            String removeName = "remove" + capitalizedGetterSetterName;
            ObjectModelOperation remover = addOperation(abstractClass, removeName, "void");
            addAnnotation(abstractClass, remover, "Override");
            addParameter(remover, attributeTypeVarargs, "element");
            String removerBody = ""
/*{
<%=attributeTypeCollectionStrict%> oldValue = get<%=capitalizedAttributeName%>();
<%=helperClassName%>.remove<%=capitalizedHelperGetterSetterName%>(getWikitty(), element);
getPropertyChangeSupport().firePropertyChange(<%=fieldVariableName%>, oldValue, <%=getter.getName()%>());
}*/;
            setOperationBody(remover, removerBody);

            String clearName = "clear" + capitalizedGetterSetterName;
            ObjectModelOperation clear = addOperation(abstractClass, clearName, "void");
            addAnnotation(abstractClass, clear, "Override");
            String clearBody = ""
/*{
<%=helperClassName%>.clear<%=capitalizedHelperGetterSetterName%>(getWikitty());
getPropertyChangeSupport().firePropertyChange(<%=fieldVariableName%>, null, <%=getter.getName()%>());
}*/;
            setOperationBody(clear, clearBody);

            //
            // Pour avoir acces en mode Objet entity
            //
            if (attributeObjectType != null) {
                // C'est un BusinessEntity on ajoute les getter et setter specifique

                getterName = "get" + capitalizedGetterSetterName;
                ObjectModelOperation getterObject = addOperation(
                        abstractClass, getterName, attributeObjectTypeCollectionStrict);
                addParameter(getterObject, "boolean", "exceptionIfNotLoaded");
                addAnnotation(abstractClass, getterObject, "Override");
                String getterObjectBody = ""
/*{
<%=attributeObjectTypeCollectionStrict%> result = <%=helperClassName%>.get<%=capitalizedAttributeName%>(getWikitty(), exceptionIfNotLoaded);
return result;
}*/;
                setOperationBody(getterObject, getterObjectBody);

                String setterObjectName = "set" + capitalizedGetterSetterName + "Entity";
                ObjectModelOperation setterObject = addOperation(abstractClass, setterObjectName, "void");
                addAnnotation(abstractClass, setterObject, "Override");
                addParameter(setterObject, attributeObjectTypeCollectionGeneric, getterSetterName);
                String setterObjectBody = ""
/*{
<%=attributeObjectTypeCollectionStrict%> oldValue = get<%=capitalizedAttributeName%>(false);
<%=helperClassName%>.set<%=capitalizedHelperGetterSetterName%>Entity(getWikitty(), <%=attributeName%>);
getPropertyChangeSupport().firePropertyChange(<%=fieldVariableName%>, oldValue, <%= getter.getName() %>());
}*/;
                setOperationBody(setterObject, setterObjectBody);

                String addAllNameObject = "addAll" + capitalizedGetterSetterName + "Entity";
                ObjectModelOperation addAllObject = addOperation(abstractClass, addAllNameObject, "void");
                addAnnotation(abstractClass, addAllObject, "Override");
                addParameter(addAllObject, attributeObjectTypeCollectionGeneric, attributeName);
                String addAllObjectBody = ""
/*{
<%=attributeObjectTypeCollectionStrict%> oldValue = get<%=capitalizedAttributeName%>(false);
<%=helperClassName%>.addAll<%=capitalizedHelperGetterSetterName%>Entity(getWikitty(), <%=attributeName%>);
getPropertyChangeSupport().firePropertyChange(<%=fieldVariableName%>, oldValue, <%= getterObject.getName() %>());
}*/;
                setOperationBody(addAllObject, addAllObjectBody);

                String addNameObject = "add" + capitalizedGetterSetterName;
                ObjectModelOperation adderObject = addOperation(abstractClass, addNameObject, "void");
                addAnnotation(abstractClass, adderObject, "Override");
                addParameter(adderObject, attributeObjectTypeVarargs, "element");
                String adderObjectBody = ""
/*{
<%=attributeObjectTypeCollectionStrict%> oldValue = get<%=capitalizedAttributeName%>(false);
<%=helperClassName%>.add<%=capitalizedHelperGetterSetterName%>(getWikitty(), element);
getPropertyChangeSupport().firePropertyChange(<%=fieldVariableName%>, oldValue, <%= getterObject.getName() %>());
}*/;
                setOperationBody(adderObject, adderObjectBody);

                String removeNameObject = "remove" + capitalizedGetterSetterName;
                ObjectModelOperation removerObject = addOperation(abstractClass, removeNameObject, "void");
                addAnnotation(abstractClass, removerObject, "Override");
                addParameter(removerObject, attributeObjectTypeVarargs, "element");
                String removerObjectBody = ""
/*{
<%=attributeObjectTypeCollectionStrict%> oldValue = get<%=capitalizedAttributeName%>(false);
<%=helperClassName%>.remove<%=capitalizedHelperGetterSetterName%>(getWikitty(), element);
getPropertyChangeSupport().firePropertyChange(<%=fieldVariableName%>, oldValue, <%=getterObject.getName()%>());
}*/;
                setOperationBody(removerObject, removerObjectBody);

            }

        } else {
            String getFieldMethodName = WikittyTransformerUtil.generateGetFieldAsCall(attribute);
            
            // adding getter and setter to contract
            getterName = "get" + capitalizedGetterSetterName;
            ObjectModelOperation getter = addOperation(abstractClass, getterName, attributeType);
            addAnnotation(abstractClass, getter, "Override");
            setOperationBody(getter, ""
/*{
<%=attributeType%> value = <%=helperClassName%>.get<%=capitalizedHelperGetterSetterName%>(getWikitty());
return value;
}*/);

            String setterName = "set" + capitalizedGetterSetterName;
            ObjectModelOperation setter = addOperation(abstractClass, setterName, "void");
            addAnnotation(abstractClass, setter, "Override");
            addParameter(setter, attributeType, attributeName);
            setOperationBody(setter, ""
/*{
<%=attributeType%> oldValue = <%=getterName%>();
<%=helperClassName%>.set<%=capitalizedHelperGetterSetterName%>(getWikitty(), <%=attributeName%>);
getPropertyChangeSupport().firePropertyChange(<%=fieldVariableName%>, oldValue, <%=getter.getName()%>());
}*/);

            //
            // Pour avoir acces en mode Objet entity
            //
            if (attributeObjectType != null) {
                // adding getter and setter to contract
                getterName = "get" + capitalizedGetterSetterName;
                ObjectModelOperation getterObject = addOperation(abstractClass, getterName, attributeObjectType);
                addParameter(getterObject, "boolean", "exceptionIfNotLoaded");
                addAnnotation(abstractClass, getterObject, "Override");
                setOperationBody(getterObject, ""
/*{
<%=attributeObjectType%> value = <%=helperClassName%>.get<%=capitalizedHelperGetterSetterName%>(getWikitty(), exceptionIfNotLoaded);
return value;
}*/);

                String setterNameObject = "set" + capitalizedGetterSetterName;
                ObjectModelOperation setterObject = addOperation(abstractClass, setterNameObject, "void");
                addAnnotation(abstractClass, setterObject, "Override");
                addParameter(setterObject, attributeObjectType, attributeName);
                setOperationBody(setterObject, ""
/*{
<%=attributeObjectType%> oldValue = <%=getterName%>(false);
<%=helperClassName%>.set<%=capitalizedHelperGetterSetterName%>(getWikitty(), <%=attributeName%>);
getPropertyChangeSupport().firePropertyChange(<%=fieldVariableName%>, oldValue, <%=getterObject.getName()%>());
}*/);
            }
        }

        // save the getter name for this attribute
        attributeToGetterName.put(businessEntity.getName() + "." + attributeName, getterName);
    }

    /** Add inherited operations to the abstract generated from an entity.
     * Method browse superClasses to copy methods and their bodies. The method
     * deal with imports, cause bodies call the Helper of the classe where
     * the attribute was declared.
     *
     * If A inherit from B and B inherit from and A, B, C not in the same package
     * we B need to import CHelper and A need to import BHelper. But, since A
     * has C attributes too, it needs CHelper too. So when we will process B,
     * we will save needed imports, A will get them thus import CHelper.
     * @param businessEntity
     * @param abstractClass
     */
    protected void addInheritedOperations(ObjectModelClass businessEntity, ObjectModelClass abstractClass) {

        if ( ! entitiesWithInheritedOperations.contains(businessEntity)) {

            // now, add to this abstract all operation due to inheritence from
            // other business entities

            List<ObjectModelClass> superClasses = WikittyTransformerUtil.getAllSuperClasses(model, businessEntity);
            for (ObjectModelClass superClass : superClasses) {

                // FIXME echatellier 20120106 it an eugene bug
                // super class are not same instance as model classes
                superClass = model.getClass(superClass.getQualifiedName());

                // this list will contains all imports we do due to inheritence
                List<String> imports = requiredDependencyImports.get(businessEntity);
                if (imports == null) {
                    imports = new ArrayList<String>();
                    requiredDependencyImports.put(businessEntity, imports);
                }

                // process superclasses first, so we will get inherited operations from superclass
                addInheritedOperations(superClass, processedClasses.get(superClass));

                // if super class is not in the same package, import its Helper
                if ( ! businessEntity.getPackageName().equals(superClass.getPackageName())) {
                    String parentToImport = superClass.getQualifiedName();
                    String helperToImport = superClass.getPackageName() + "." +
                            WikittyTransformerUtil.businessEntityToHelperName(superClass);

                    addImport(abstractClass, parentToImport);
                    addImport(abstractClass, helperToImport);

                    // add the last import to the map, so sub-classes will be
                    // able to know what imports they need
                    imports.add(parentToImport);
                    imports.add(helperToImport);
                }

                // we may need to some imports for the bodies of the inherited operations
                // let's get the imports done by superClass and copy them
                List<String> importsNeeded = requiredDependencyImports.get(superClass);
                if (log.isDebugEnabled()) {
                    log.debug(businessEntity + " needs imports " + importsNeeded);
                }
                if (importsNeeded != null) {
                    for (String importNeeded : importsNeeded) {
                        // superclass needed this import, so wo import too
                        addImport(abstractClass, importNeeded);

                        // subclasses will need it too, so add it
                        imports.add(importNeeded);
                    }
                }

                if (WikittyTransformerUtil.isBusinessEntity(superClass)) {

                    String helperClassName = WikittyTransformerUtil.businessEntityToHelperName(superClass);

                    // il ne faut pas copier les operations, mais les regenerer
                    // en ayant la possibilité de changer le nom
                    for (ObjectModelAttribute attribute : superClass.getAttributes()) {

                        if (attribute.isNavigable()) {
                            // there is a conflict, purifier transformer give as the right name to use
                            String attributeName;
                            if (businessEntity.hasTagValue(TAG_ALTERNATIVE_NAME + "." + superClass.getName() + "." + attribute.getName())) {
                                attributeName = businessEntity.getTagValue(TAG_ALTERNATIVE_NAME + "." + superClass.getName() + "." + attribute.getName());
                            } else if (attribute.hasTagValue(TAG_ALTERNATIVE_NAME)) {
                                attributeName = attribute.getTagValue(TAG_ALTERNATIVE_NAME);
                            } else {
                                attributeName = attribute.getName();
                            }
    
                            // le nom du getter peut avoir été renommé pour toute la hierarchie
                            String helperGetterSetterName;
                            if (attribute.hasTagValue(TAG_ALTERNATIVE_NAME)) {
                                helperGetterSetterName = attribute.getTagValue(TAG_ALTERNATIVE_NAME);
                            } else {
                                helperGetterSetterName = attribute.getName();
                            }
                            addOperationWithName(businessEntity, abstractClass, attribute, attributeName, helperClassName, helperGetterSetterName);
                        }
                    }
                }
            }

            entitiesWithInheritedOperations.add(businessEntity);
        }
    }

    /**
     * add a toString method.
     * if a toString is tagValue is attached to businessEntity, it will be used
     * to generate a toString as this :
     * <pre>
     * given "hello %Person.name|unknown$s"
     * </pre>
     * will try to replace field name by field value for extension Person.
     * if this information is not available, will do unknow.
     * <p>
     * use same syntax as <a href="http://download.oracle.com/javase/6/docs/api/java/util/Formatter.html#syntax">Syntax</a>.
     *
     * @param businessEntity
     * @param abstractClass
     */ 
    protected void addToString(ObjectModelClass businessEntity, ObjectModelClass abstractClass) {
        String helperClassName = WikittyTransformerUtil.businessEntityToHelperName(businessEntity);
        ObjectModelOperation toString = addOperation(abstractClass, "toString", "String");
        setDocumentation(toString,
                "Return toString representation. Use tagValue '"
                + TAG_TO_STRING
                + "' format, if exist, else standard toString is call");
        addAnnotation(abstractClass, toString, "Override");
        setOperationBody(toString, ""
/*{
        return <%=helperClassName%>.toString(getWikitty());
}*/);
    }
    
    protected void addMetaExtensionOperations(ObjectModelClass metaExtension,
                           ObjectModelClass abstractClassForThisMetaExtension) {

        String helperClassName = WikittyTransformerUtil.businessEntityToHelperName(metaExtension);

        // poussin 20110813 move to BusinessEntityImpl
//        ObjectModelAttribute extension = addAttribute(abstractClassForThisMetaExtension, "extensionForMetaExtension", WikittyTransformerUtil.WIKITTY_EXTENSION_CLASS_FQN);
//        setDocumentation(extension, "the metaExtension operations target this extension, may be null");

        ObjectModelOperation setExtensionForMetaExtension = addOperation(abstractClassForThisMetaExtension, "setExtensionForMetaExtension", "void");
        addParameter(setExtensionForMetaExtension, WikittyTransformerUtil.WIKITTY_EXTENSION_CLASS_FQN, "extension");
        setDocumentation(setExtensionForMetaExtension, String.format(
                "add %s meta-extension on given extension to this entity",
                metaExtension.getName()));
        setOperationBody(setExtensionForMetaExtension, ""
/*{
        extensionForMetaExtension = extension;
        <%=helperClassName%>.addMetaExtension(extension, getWikitty());
}*/);

        // generating operations with bodies to realize contract
        for (ObjectModelAttribute attribute : metaExtension.getAttributes()) {
            if (attribute.isNavigable()) {
                // needed below, in templates
                String fieldVariableName = WikittyTransformerUtil.attributeToFielVariableName(attribute, true);
                String attributeType = WikittyTransformerUtil.generateResultType(attribute, false);
                String attributeObjectType = WikittyTransformerUtil.generateResultObjectType(attribute, false);

                String attributeName = attribute.getName();
                if (attribute.hasTagValue(TAG_ALTERNATIVE_NAME)) {
                    // there is a conflict, purifier transformer give as the right name to use
                    attributeName = attribute.getTagValue(TAG_ALTERNATIVE_NAME);
                }

                String getterName;

                if (WikittyTransformerUtil.isAttributeCollection(attribute)) {
                    // attributed is a collection, we will generate operations get, add, remove and clear

                    String attributeTypeCollectionStrict =
                            WikittyTransformerUtil.generateResultType(attribute, true);
                    String attributeTypeCollectionGeneric =
                            "Collection<" + attributeType + ">";
                    String attributeTypeVarargs = attributeType + "...";

                    String attributeObjectTypeCollectionStrict =
                            WikittyTransformerUtil.generateResultObjectType(attribute, true);
                    String attributeObjectTypeCollectionGeneric =
                            "Collection<" + attributeObjectType + ">";
                    String attributeObjectTypeVarargs = attributeObjectType + "...";

                    String getFieldMethodName = WikittyTransformerUtil.generateGetFieldAsCall(attribute);
                    String attributeNameCapitalized = StringUtils.capitalize(attributeName);

                    // now, for this attribute, we will generate add, remove and clear methods
                    // adding operations to contract
                    getterName = "get" + attributeNameCapitalized;
                    ObjectModelOperation getter = addOperation(abstractClassForThisMetaExtension, getterName, attributeTypeCollectionStrict);
                    addAnnotation(abstractClassForThisMetaExtension, getter, "Override");
                    String getterBody = ""
/*{
        <%=attributeTypeCollectionStrict%> result;
        if (extensionForMetaExtension == null) {
            result = <%=helperClassName%>.<%=getterName%>(getWikitty());
        } else {
            result = <%=helperClassName%>.<%=getterName%>(extensionForMetaExtension.getName(), getWikitty());
        }
        return result;
}*/;            
                    setOperationBody(getter, getterBody);

                    String setterName = "set" + attributeNameCapitalized;
                    ObjectModelOperation setter = addOperation(abstractClassForThisMetaExtension, setterName, "void");
                    addAnnotation(abstractClassForThisMetaExtension, setter, "Override");
                    addParameter(setter, attributeTypeCollectionStrict, "values");
                    String setterBody = ""
/*{
        String fieldName;
        <%=attributeTypeCollectionStrict%> oldValue = <%=getter.getName()%>();
        if (extensionForMetaExtension == null) {
            <%=helperClassName%>.<%=setterName%>(getWikitty(), values);
            fieldName = <%=fieldVariableName%>;
        } else {
            <%=helperClassName%>.<%=setterName%>(extensionForMetaExtension.getName(), getWikitty(), values);
            fieldName = <%=helperClassName%>.getMetaFieldName(extensionForMetaExtension, "<%=attributeName%>");
        }
        getPropertyChangeSupport().firePropertyChange(fieldName, oldValue, <%=getter.getName()%>());
}*/;
                    setOperationBody(setter, setterBody);

                    String addAllName = "addAll" + attributeNameCapitalized;
                    ObjectModelOperation addAll = addOperation(abstractClassForThisMetaExtension, addAllName, "void");
                    addAnnotation(abstractClassForThisMetaExtension, addAll, "Override");
                    addParameter(addAll, attributeTypeCollectionGeneric, "values");
                    String addAllBody = ""
/*{
        String fieldName;
        <%=attributeTypeCollectionStrict%> oldValue = <%=getter.getName()%>();
        if (extensionForMetaExtension == null) {
            <%=helperClassName%>.<%=addAllName%>(getWikitty(), values);
            fieldName = <%=fieldVariableName%>;
        } else {
            <%=helperClassName%>.<%=addAllName%>(extensionForMetaExtension.getName(), getWikitty(), values);
            fieldName = <%=helperClassName%>.getMetaFieldName(extensionForMetaExtension, "<%=attributeName%>");
        }
        getPropertyChangeSupport().firePropertyChange(fieldName, oldValue, <%=getter.getName()%>());
}*/;
                    setOperationBody(addAll, addAllBody);

                    String addName = "add" + attributeNameCapitalized;
                    ObjectModelOperation adder = addOperation(abstractClassForThisMetaExtension, addName, "void");
                    addAnnotation(abstractClassForThisMetaExtension, adder, "Override");
                    addParameter(adder, attributeTypeVarargs, "element");
                    String adderBody = ""
/*{
        String fieldName;
        <%=attributeTypeCollectionStrict%> oldValue = <%=getter.getName()%>();
        if (extensionForMetaExtension == null) {
            <%=helperClassName%>.<%=addName%>(getWikitty(), element);
            fieldName = <%=fieldVariableName%>;
        } else {
            <%=helperClassName%>.<%=addName%>(extensionForMetaExtension.getName(), getWikitty(), element);
            fieldName = <%=helperClassName%>.getMetaFieldName(extensionForMetaExtension, "<%=attributeName%>");
        }
        getPropertyChangeSupport().firePropertyChange(fieldName, oldValue, <%=getter.getName()%>());
}*/;
                    setOperationBody(adder, adderBody);
                    
                    String removeName = "remove" + attributeNameCapitalized;
                    ObjectModelOperation remover = addOperation(abstractClassForThisMetaExtension, removeName, "void");
                    addAnnotation(abstractClassForThisMetaExtension, remover, "Override");
                    addParameter(remover, attributeTypeVarargs, "element");
                    String removerBody = ""
/*{
        String fieldName;
        <%=attributeTypeCollectionStrict%> oldValue = <%=getter.getName()%>();
        if (extensionForMetaExtension == null) {
            <%=helperClassName%>.<%=removeName%>(getWikitty(), element);
            fieldName = <%=fieldVariableName%>;
        } else {
            <%=helperClassName%>.<%=removeName%>(extensionForMetaExtension.getName(), getWikitty(), element);
            fieldName = <%=helperClassName%>.getMetaFieldName(extensionForMetaExtension, "<%=attributeName%>");
        }
        getPropertyChangeSupport().firePropertyChange(fieldName, oldValue, <%=getter.getName()%>());
}*/;
                    setOperationBody(remover, removerBody);
                    
                    String clearName = "clear" + attributeNameCapitalized;
                    ObjectModelOperation clear = addOperation(abstractClassForThisMetaExtension, clearName, "void");
                    addAnnotation(abstractClassForThisMetaExtension, clear, "Override");
                    String clearBody = ""
/*{
        String fieldName;
        <%=attributeTypeCollectionStrict%> oldValue = <%=getter.getName()%>();
        if (extensionForMetaExtension == null) {
            <%=helperClassName%>.<%=clearName%>(getWikitty());
            fieldName = <%=fieldVariableName%>;
        } else {
            <%=helperClassName%>.<%=clearName%>(extensionForMetaExtension.getName(), getWikitty());
            fieldName = <%=helperClassName%>.getMetaFieldName(extensionForMetaExtension, "<%=attributeName%>");
        }
        getPropertyChangeSupport().firePropertyChange(fieldName, oldValue, <%=getter.getName()%>());
}*/;
                    setOperationBody(clear, clearBody);


                    //
                    // Pour avoir acces en mode Objet entity
                    //
                    if (attributeObjectType != null) {
                        // C'est un BusinessEntity on ajoute les getter et setter specifique
                        
                        getterName = "get" + attributeNameCapitalized;
                        ObjectModelOperation getterObject = addOperation(
                                abstractClassForThisMetaExtension, getterName,
                                attributeObjectTypeCollectionStrict);
                        addParameter(getterObject, "boolean", "exceptionIfNotLoaded");
                        addAnnotation(abstractClassForThisMetaExtension, getterObject, "Override");
                        String getterObjectBody = ""
/*{
        <%=attributeObjectTypeCollectionStrict%> result;
        if (extensionForMetaExtension == null) {
            result = <%=helperClassName%>.<%=getterName%>(getWikitty(), exceptionIfNotLoaded);
        } else {
            result = <%=helperClassName%>.<%=getterName%>(extensionForMetaExtension.getName(), getWikitty(), exceptionIfNotLoaded);
        }
        return result;
}*/;
                    setOperationBody(getterObject, getterObjectBody);

                    String setterNameObject = "set" + attributeNameCapitalized + "Entity";
                    ObjectModelOperation setterObject = addOperation(
                            abstractClassForThisMetaExtension, setterNameObject, "void");
                    addAnnotation(abstractClassForThisMetaExtension, setterObject, "Override");
                    addParameter(setterObject, attributeObjectTypeCollectionGeneric, "values");
                    String setterBodyObject = ""
/*{
        String fieldName;
        <%=attributeObjectTypeCollectionStrict%> oldValue = <%=getterObject.getName()%>(false);
        if (extensionForMetaExtension == null) {
            <%=helperClassName%>.<%=setterNameObject%>(getWikitty(), values);
            fieldName = <%=fieldVariableName%>;
        } else {
            <%=helperClassName%>.<%=setterNameObject%>(extensionForMetaExtension.getName(), getWikitty(), values);
            fieldName = <%=helperClassName%>.getMetaFieldName(extensionForMetaExtension, "<%=attributeName%>");
        }
        getPropertyChangeSupport().firePropertyChange(fieldName, oldValue, <%=getter.getName()%>());
}*/;
                    setOperationBody(setterObject, setterBodyObject);

                    String addAllNameObject = "addAll" + attributeNameCapitalized + "Entity";
                    ObjectModelOperation addAllObject = addOperation(
                            abstractClassForThisMetaExtension, addAllNameObject, "void");
                    addAnnotation(abstractClassForThisMetaExtension, addAllObject, "Override");
                    addParameter(addAllObject, attributeObjectTypeCollectionGeneric, "values");
                    String addAllBodyObject = ""
/*{
        String fieldName;
        <%=attributeObjectTypeCollectionStrict%> oldValue = <%=getterObject.getName()%>(false);
        if (extensionForMetaExtension == null) {
            <%=helperClassName%>.<%=addAllNameObject%>(getWikitty(), values);
            fieldName = <%=fieldVariableName%>;
        } else {
            <%=helperClassName%>.<%=addAllNameObject%>(extensionForMetaExtension.getName(), getWikitty(), values);
            fieldName = <%=helperClassName%>.getMetaFieldName(extensionForMetaExtension, "<%=attributeName%>");
        }
        getPropertyChangeSupport().firePropertyChange(fieldName, oldValue, <%=getterObject.getName()%>(false));
}*/;
                    setOperationBody(addAllObject, addAllBodyObject);

                    String addNameObject = "add" + attributeNameCapitalized;
                    ObjectModelOperation adderObject = addOperation(
                            abstractClassForThisMetaExtension, addNameObject, "void");
                    addAnnotation(abstractClassForThisMetaExtension, adderObject, "Override");
                    addParameter(adderObject, attributeObjectTypeVarargs, "element");
                    String adderBodyObject = ""
/*{
        String fieldName;
        <%=attributeObjectTypeCollectionStrict%> oldValue = <%=getterObject.getName()%>(false);
        if (extensionForMetaExtension == null) {
            <%=helperClassName%>.<%=addNameObject%>(getWikitty(), element);
            fieldName = <%=fieldVariableName%>;
        } else {
            <%=helperClassName%>.<%=addNameObject%>(extensionForMetaExtension.getName(), getWikitty(), element);
            fieldName = <%=helperClassName%>.getMetaFieldName(extensionForMetaExtension, "<%=attributeName%>");
        }
        getPropertyChangeSupport().firePropertyChange(fieldName, oldValue, <%=getterObject.getName()%>(false));
}*/;
                    setOperationBody(adderObject, adderBodyObject);

                    String removeNameObject = "remove" + attributeNameCapitalized;
                    ObjectModelOperation removerObject = addOperation(
                            abstractClassForThisMetaExtension, removeNameObject, "void");
                    addAnnotation(abstractClassForThisMetaExtension, removerObject, "Override");
                    addParameter(removerObject, attributeObjectTypeVarargs, "element");
                    String removerBodyObject = ""
/*{
        String fieldName;
        <%=attributeObjectTypeCollectionStrict%> oldValue = <%=getterObject.getName()%>(false);
        if (extensionForMetaExtension == null) {
            <%=helperClassName%>.<%=removeNameObject%>(getWikitty(), element);
            fieldName = <%=fieldVariableName%>;
        } else {
            <%=helperClassName%>.<%=removeNameObject%>(extensionForMetaExtension.getName(), getWikitty(), element);
            fieldName = <%=helperClassName%>.getMetaFieldName(extensionForMetaExtension, "<%=attributeName%>");
        }
        getPropertyChangeSupport().firePropertyChange(fieldName, oldValue, <%=getterObject.getName()%>(false));
}*/;
                    setOperationBody(removerObject, removerBodyObject);

                    }
                } else {
                    String getFieldMethodName = WikittyTransformerUtil.generateGetFieldAsCall(attribute);

                    // adding getter and setter to contract
                    getterName = "get" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation getter = addOperation(abstractClassForThisMetaExtension, getterName, attributeType);
                    addAnnotation(abstractClassForThisMetaExtension, getter, "Override");
                    setOperationBody(getter, ""
/*{
        <%=attributeType%> value;
        if (extensionForMetaExtension == null) {
            value = <%=helperClassName%>.<%=getterName%>(getWikitty());
        } else {
            value = <%=helperClassName%>.<%=getterName%>(extensionForMetaExtension.getName(), getWikitty());
        }
        return value;
}*/);

                    String setterName = "set" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation setter = addOperation(abstractClassForThisMetaExtension, setterName, "void");
                    addAnnotation(abstractClassForThisMetaExtension, setter, "Override");
                    addParameter(setter, attributeType, attributeName);
                    setOperationBody(setter, ""
/*{
        <%=attributeType%> oldValue;
        if (extensionForMetaExtension == null) {
            oldValue = <%=getterName%>();
            <%=helperClassName%>.<%=setterName%>(getWikitty(), <%=attributeName%>);
            getPropertyChangeSupport().firePropertyChange(<%=fieldVariableName%>, oldValue, <%=getter.getName()%>());
        } else {
            oldValue = <%=getterName%>();
            <%=helperClassName%>.<%=setterName%>(extensionForMetaExtension.getName(), getWikitty(), <%=attributeName%>);
            String fieldName = <%=helperClassName%>.getMetaFieldName(extensionForMetaExtension, "<%=attributeName%>");
            getPropertyChangeSupport().firePropertyChange(fieldName, oldValue, <%=getter.getName()%>());
        }
}*/);
                    //
                    // Pour avoir acces en mode Objet entity
                    //
                    if (attributeObjectType != null) {
                        // C'est un BusinessEntity on ajoute les getter et setter specifique

                        getterName = "get" + StringUtils.capitalize(attributeName);
                        ObjectModelOperation getterObject = addOperation(
                                abstractClassForThisMetaExtension, getterName, attributeObjectType);
                        addParameter(getterObject, "boolean", "exceptionIfNotLoaded");
                        addAnnotation(abstractClassForThisMetaExtension, getterObject, "Override");
                        setOperationBody(getterObject, ""
/*{
        <%=attributeObjectType%> value;
        if (extensionForMetaExtension == null) {
            value = <%=helperClassName%>.<%=getterName%>(getWikitty(), exceptionIfNotLoaded);
        } else {
            value = <%=helperClassName%>.<%=getterName%>(extensionForMetaExtension.getName(), getWikitty(), exceptionIfNotLoaded);
        }
        return value;
}*/);

                    String setterNameObject = "set" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation setterObject = addOperation(
                            abstractClassForThisMetaExtension, setterNameObject, "void");
                    addAnnotation(abstractClassForThisMetaExtension, setterObject, "Override");
                    addParameter(setterObject, attributeObjectType, attributeName);
                    setOperationBody(setterObject, ""
/*{
        <%=attributeObjectType%> oldValue;
        if (extensionForMetaExtension == null) {
            oldValue = <%=getterName%>(false);
            <%=helperClassName%>.<%=setterNameObject%>(getWikitty(), <%=attributeName%>);
            getPropertyChangeSupport().firePropertyChange(<%=fieldVariableName%>, oldValue, <%=getterObject.getName()%>(false));
        } else {
            oldValue = <%=getterName%>(false);
            <%=helperClassName%>.<%=setterNameObject%>(extensionForMetaExtension.getName(), getWikitty(), <%=attributeName%>);
            String fieldName = <%=helperClassName%>.getMetaFieldName(extensionForMetaExtension, "<%=attributeName%>");
            getPropertyChangeSupport().firePropertyChange(fieldName, oldValue, <%=getterObject.getName()%>(false));
        }
}*/);
                    }

                }
            }
        }
    }
}
