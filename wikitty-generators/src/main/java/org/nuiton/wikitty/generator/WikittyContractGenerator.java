/*
 * #%L
 * Wikitty :: generators
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.generator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

/*{generator option: writeString = }*/
/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * Wikitty interface générator.
 * 
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.wikitty.generator.WikittyContractGenerator"
 */
public class WikittyContractGenerator extends ObjectModelTransformerToJava implements WikittyTagValue {

    private static final Log log = LogFactory.getLog(WikittyContractGenerator.class);

    /**
     * Will permet to get the result of a processed class
     * it is useful for dealing with inheritence : when an extension depend
     * of another. In this case the child can find the processed class of the parent
     * and then look into it to find methods and data to copy. 
     */
    protected Map<ObjectModelClass, ObjectModelInterface> processedClasses =
            new HashMap<ObjectModelClass, ObjectModelInterface>();

    protected Set<ObjectModelClass> processedEntities = new HashSet<ObjectModelClass>();
    
    @Override
    public void transformFromModel(ObjectModel model) {

        log.info(model.getClasses().size() + " classes to process");
        
        for (ObjectModelClass clazz : model.getClasses()) {
            if (WikittyTransformerUtil.isBusinessEntity(clazz)) {
                processEntity(clazz);
            }
            if (WikittyTransformerUtil.isMetaExtension(clazz)) {
                processMetaExtension(clazz);
            }
        }
        
        processedClasses.clear();
        processedEntities.clear();
    }
    
    /**
     * Contains code commons to entities and meta-extensions.
     * 
     * @param businessEntity
     * @return
     */
    protected ObjectModelInterface prepareOutputClass(ObjectModelClass businessEntity) {

        ObjectModelInterface contract = processedClasses.get(businessEntity);

        if (contract == null) {

            log.debug("preparing " + businessEntity.getPackageName() +
                                  "." + businessEntity.getName());

            contract = createInterface(WikittyTransformerUtil.businessEntityToContractName(businessEntity),
                                                            businessEntity.getPackageName());
            addInterface(contract, WikittyTransformerUtil.BUSINESS_ENTITY_CLASS_FQN);

            // TODO 20100811 bleny remove unused imports
            addImport(contract, WikittyTransformerUtil.BUSINESS_ENTITY_CLASS_FQN);
            addImport(contract, WikittyTransformerUtil.BUSINESS_ENTITY_WIKITTY_CLASS_FQN);
            addImport(contract, WikittyTransformerUtil.WIKITTY_CLASS_FQN);
            addImport(contract, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyExtension");
            addImport(contract, "org.nuiton.wikitty.entities.ElementField");
            addImport(contract, "org.nuiton.wikitty.WikittyUtil");
            addImport(contract, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyUser");
            addImport(contract, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyUserAbstract");
            addImport(contract, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyUserImpl");
            addImport(contract, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyTreeNode");
            addImport(contract, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyTreeNodeAbstract");
            addImport(contract, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyTreeNodeImpl");
            addImport(contract, List.class);
            addImport(contract, ArrayList.class);
            addImport(contract, Collection.class);
            addImport(contract, Collections.class);
            addImport(contract, Set.class);
            addImport(contract, Date.class);
            addImport(contract, LinkedHashSet.class);

            String documentation = businessEntity.getDocumentation();
            if (businessEntity.hasTagValue(TAG_DOCUMENTATION)) {
                documentation += "\n\n";
                documentation += businessEntity.getTagValue(TAG_DOCUMENTATION);
            }

            setDocumentation(contract, documentation);

            processedClasses.put(businessEntity, contract);

        } else {
            log.debug("already prepared : " + businessEntity.getPackageName() +
                                        "." + businessEntity.getName());
        }

        return contract;
    }

    protected void processEntity(ObjectModelClass businessEntity) {
        
        if ( processedEntities.contains(businessEntity) ) {
            log.debug("entity already processed : " + businessEntity.getPackageName() +
                    "." + businessEntity.getName());
            // already processed
            return ;
        }

        log.debug("processing entity : " + businessEntity.getPackageName() +
                "." + businessEntity.getName());

        ObjectModelInterface contract = prepareOutputClass(businessEntity);

        
        // adding public static final String EXT_CLIENT = "Client";
        addConstant(contract,
                "EXT_" + businessEntity.getName().toUpperCase(),
                "String",
                "\"" + businessEntity.getName() + "\"",
                ObjectModelModifier.PUBLIC);

        String extensionVariableName = WikittyTransformerUtil.classToExtensionVariableName(businessEntity, false);

        // add all method describe in model in contract interfaces
        // this method must be writed by developer in Impl
        for (ObjectModelOperation operation : businessEntity.getOperations()) {
            //FIXME echatellier 20110705 http://www.nuiton.org/issues/1617
            if (operation.getReturnType() == null || operation.getReturnType().isEmpty()) {
                addOperation(contract, operation.getName(), "void");
            } else {
                addOperation(contract, operation);
            }
        }

        for(ObjectModelAttribute attribute : businessEntity.getAttributes()) {
            if (attribute.isNavigable()) {
                // two variables needed below
                String fieldVariableName = WikittyTransformerUtil.attributeToFielVariableName(attribute, false);

                //Get proper name
                String name = attribute.getName();
                
                // adding constants to contract
                addConstant(contract,
                        fieldVariableName,
                        "String",
                        "\"" + name + "\"",
                        ObjectModelModifier.PUBLIC);
                // adding public static final String FQ_FIELD_CLIENT_NAME = EXT_CLIENT + ".name";
                addConstant(contract,
                        "FQ_" + fieldVariableName,
                        "String",
                        extensionVariableName + " + \"." + name + "\"",
                        ObjectModelModifier.PUBLIC);

                // adding public static final String ELEMENT_FIELD_CLIENT_NAME = new ElementField(EXT_CLIENT + ".name");
                addConstant(contract,
                        "ELEMENT_" + fieldVariableName,
                        "ElementField",
                        "new ElementField(FQ_" +fieldVariableName  + ")",
                        ObjectModelModifier.PUBLIC);
            }
        }

        for (ObjectModelAttribute attribute : businessEntity.getAttributes()) {
            if (attribute.isNavigable()) {
                // there is a conflict, purifier transformer give as the right name to use
                String attributeName;
                if (businessEntity.hasTagValue(TAG_ALTERNATIVE_NAME + "." + businessEntity.getName() + "." + attribute.getName())) {
                    attributeName = businessEntity.getTagValue(TAG_ALTERNATIVE_NAME + "." + businessEntity.getName() + "." + attribute.getName());
                } else if (attribute.hasTagValue(TAG_ALTERNATIVE_NAME)) {
                    
                    attributeName = attribute.getTagValue(TAG_ALTERNATIVE_NAME);
                } else {
                    attributeName = attribute.getName();
                }

                addOperationWithName(contract, attribute, attributeName);
            }

        }
 
        // now, add to this contract all operation due to inheritence from
        // other business entities
        // FIXME echatellier 20120106 single inheritance level here
        Collection<ObjectModelClass> superClasses = businessEntity.getSuperclasses();
        for (ObjectModelClass superClass : superClasses) {

            // FIXME echatellier 20120106 it an eugene bug
            // super class are not same instance as model classes
            superClass = model.getClass(superClass.getQualifiedName());

            addInterface(contract, superClass.getQualifiedName());

            if (WikittyTransformerUtil.isBusinessEntity(superClass)) {
                // il ne faut pas copier les operations, mais les regenerer
                // en ayant la possibilité de changer le nom
                for (ObjectModelAttribute attribute : superClass.getAttributes()) {
                    // there is a conflict, purifier transformer give as the right name to use
                    String attributeName;
                    if (businessEntity.hasTagValue(TAG_ALTERNATIVE_NAME + "." + superClass.getName() + "." + attribute.getName())) {
                        attributeName = businessEntity.getTagValue(TAG_ALTERNATIVE_NAME + "." + superClass.getName() + "." + attribute.getName());
                        addOperationWithName(contract, attribute, attributeName);
                    }
                    // sinon, on ne génère rien, on herite de la methode
                    // par heritage des interface
                }
            }
        }

        processedEntities.add(businessEntity);
    }

    /**
     * Methode de génération des methodes (sans corps) qui permet de generer
     * des methodes avec des nom différent suivant si on l'appel
     * pour générer le operation de la classe actuelle ou celles de ses
     * super classes.
     * 
     * @param contract class a générer
     * @param attribute attribut dont on veut generer les opérations
     * @param getterSetterName le nom du getter (peut etre différent du nom
     *      de l'attribut dans le cas d'un heritage multiples)
     */
    protected void addOperationWithName(ObjectModelInterface contract, ObjectModelAttribute attribute, String getterSetterName) {
        String attributeType = WikittyTransformerUtil.generateResultType(attribute, false);
        String attributeObjectType = WikittyTransformerUtil.generateResultObjectType(attribute, false);

        if (attribute.getMaxMultiplicity() > 1 || attribute.getMaxMultiplicity() == -1) {
            // attributed is a collection, we will generate operations get, add, remove and clear

            String attributeTypeCollectionStrict = WikittyTransformerUtil.generateResultType(attribute, true);
            String attributeTypeCollectionGeneric = "Collection<" + attributeType + ">";
            String attributeTypeVarargs = attributeType + "...";
    
            String attributeObjectTypeCollectionStrict =
                    WikittyTransformerUtil.generateResultObjectType(attribute, true);
            String attributeObjectTypeCollectionGeneric =
                    "Collection<" + attributeObjectType + ">";
            String attributeObjectTypeVarargs = attributeObjectType + "...";

            // now, for this attribute, we will generate add, remove and clear methods
            // adding operations to contract
            String getterName = "get" + StringUtils.capitalize(getterSetterName);
            ObjectModelOperation getter = addOperation(contract, getterName, attributeTypeCollectionStrict);
            setDocumentation(getter, attribute.getDocumentation());

            String setterName = "set" + StringUtils.capitalize(getterSetterName);
            ObjectModelOperation setter = addOperation(contract, setterName, "void");
            addParameter(setter, attributeTypeCollectionStrict, getterSetterName);

            String addAllName = "addAll" + StringUtils.capitalize(getterSetterName);
            ObjectModelOperation addAll = addOperation(contract, addAllName, "void");
            addParameter(addAll, attributeTypeCollectionGeneric, getterSetterName);

            String addName = "add" + StringUtils.capitalize(getterSetterName);
            ObjectModelOperation adder = addOperation(contract, addName, "void");
            addParameter(adder, attributeTypeVarargs, "element");

            String removeName = "remove" + StringUtils.capitalize(getterSetterName);
            ObjectModelOperation remover = addOperation(contract, removeName, "void");
            addParameter(remover, attributeTypeVarargs, "element");

            String clearName = "clear" + StringUtils.capitalize(getterSetterName);
            addOperation(contract, clearName, "void");

            //
            // Pour avoir acces en mode Objet entity
            //
            if (attributeObjectType != null) {
                // C'est un BusinessEntity on ajoute les getter et setter specifique

                String getterNameObject = "get" + StringUtils.capitalize(getterSetterName);
                ObjectModelOperation getterObject = addOperation(contract, getterNameObject, attributeObjectTypeCollectionStrict);
                addParameter(getterObject, "boolean", "exceptionIfNotLoaded");
                setDocumentation(getterObject, attribute.getDocumentation());

                String setterNameObject = "set" + StringUtils.capitalize(getterSetterName) + "Entity";
                ObjectModelOperation setterObject = addOperation(contract, setterNameObject, "void");
                addParameter(setterObject, attributeObjectTypeCollectionGeneric, getterSetterName);

                String addAllNameObject = "addAll" + StringUtils.capitalize(getterSetterName) + "Entity";
                ObjectModelOperation addAllObject = addOperation(contract, addAllNameObject, "void");
                addParameter(addAllObject, attributeObjectTypeCollectionGeneric, getterSetterName);

                String addNameObject = "add" + StringUtils.capitalize(getterSetterName);
                ObjectModelOperation adderObject = addOperation(contract, addNameObject, "void");
                addParameter(adderObject, attributeObjectTypeVarargs, "element");

                String removeNameObject = "remove" + StringUtils.capitalize(getterSetterName);
                ObjectModelOperation removerObject = addOperation(contract, removeNameObject, "void");
                addParameter(removerObject, attributeObjectTypeVarargs, "element");
            }
        } else {
            // attribute is not a collection, we generate a getter and a setter
            String getterName = "get" + StringUtils.capitalize(getterSetterName);
            ObjectModelOperation getter = addOperation(contract, getterName, attributeType);
            setDocumentation(getter, attribute.getDocumentation());

            String setterName = "set" + StringUtils.capitalize(getterSetterName);
            ObjectModelOperation setter = addOperation(contract, setterName, "void");
            addParameter(setter, attributeType, getterSetterName);

            //
            // Pour avoir acces en mode Objet entity
            //
            if (attributeObjectType != null) {
                // C'est un BusinessEntity on ajoute les getter et setter specifique

                String getterNameObject = "get" + StringUtils.capitalize(getterSetterName);
                ObjectModelOperation getterObject = addOperation(contract, getterNameObject, attributeObjectType);
                addParameter(getterObject, "boolean", "exceptionIfNotLoaded");
                setDocumentation(getterObject, attribute.getDocumentation());

                String setterNameObject = "set" + StringUtils.capitalize(getterSetterName);
                ObjectModelOperation setterObject = addOperation(contract, setterNameObject, "void");
                addParameter(setterObject, attributeObjectType, getterSetterName);
            }
        }
    }
    
    /**
     * Add stuff if input model element is stereotyped as "meta".
     *
     * @param metaExtension
     */
    protected void processMetaExtension(ObjectModelClass metaExtension) {
        // nothing to do
    }
}
