/*
 * #%L
 * Wikitty :: generators
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.generator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.AbstractMetaTransformer;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;

//TODO 20100610 use filter with /*[]*/
/*{generator option: writeString = }*/
/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * Wikitty generation steps :
 * <ol>
 * <li>transform user model to wikitty intermediate model</li>
 * <li>generate from wikitty intermediate model :
 * <ul>
 * <li>a contract;</li>
 * <li>an abstract class;</li>
 * <li>an implementation;</li>
 * <li>a helper</li>
 * </ul>
 * </li>
 * </ol>
 *
 * @author bleny
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.wikitty.generator.WikittyMetaGenerator"
 */
public class WikittyMetaTransformer extends AbstractMetaTransformer<ObjectModel> implements WikittyTagValue {

    private static final Log log = LogFactory.getLog(WikittyMetaTransformer.class);

    public WikittyMetaTransformer() {
        setTransformerTypes(WikittyPurifierTransformer.class);

        setTemplateTypes(WikittyContractGenerator.class,
                WikittyAbstractGenerator.class,
                WikittyImplementationGenerator.class,
                WikittyHelperGenerator.class);
    }

    @Override
    protected boolean validateModel(ObjectModel model) {

        if (model.getClasses().isEmpty()) {
            if (log.isWarnEnabled()) {
                log.warn("model does not contain any class to generate.");
            }
        }

        for (ObjectModelClass clazz : model.getClasses()) {

            // warn user if deprecated stereotype is used
            if (clazz.getStereotypes().contains(WikittyTransformerUtil.BUSINESS_ENTITY_STEREOTYPE_OLD_NAME)) {
                log.warn(clazz.getQualifiedName() + " uses deprecated \"" +
                         WikittyTransformerUtil.BUSINESS_ENTITY_STEREOTYPE_OLD_NAME
                         + "\" stereotype. use \"" +
                         WikittyTransformerUtil.BUSINESS_ENTITY_STEREOTYPE_NAME +
                         "\" instead");
            }

            if (!clazz.hasTagValue(TAG_VERSION)) {
                log.warn(clazz.getQualifiedName() + " misses a \"" +
                         TAG_VERSION + "\" tagValue");
            }
        }

        return true;
    }

}
