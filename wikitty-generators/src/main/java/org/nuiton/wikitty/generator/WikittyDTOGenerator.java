/*
 * #%L
 * Wikitty :: generators
 * %%
 * Copyright (C) 2010 - 2016 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.generator;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.java.extension.ObjectModelAnnotation;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelParameter;


/*{generator option: writeString = }*/
/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * Wikitty DTO générator.
 * 
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.wikitty.generator.WikittyDTOGenerator"
 */
public class WikittyDTOGenerator extends ObjectModelTransformerToJava implements WikittyTagValue {

    private static final Log log = LogFactory.getLog(WikittyDTOGenerator.class);

    /**
     * map busines entity from source model to generated abstract class
     */
    protected Map<ObjectModelClass, ObjectModelClass> processedClasses =
            new HashMap<ObjectModelClass, ObjectModelClass>();

    /**
     * pattern to parse toString tagValue
     * for the string "abc{foo|bar}defrzeg{uvw|xyz}oeira"
     * will match {foo|bar} and {uvw|xyz} with groups for foo, bar, uvw and xyz
     */
    protected Pattern toStringTagValuePattern = Pattern.compile("\\{(([^|}])*)(?:\\|([^}]*))?\\}");

    /**
     * map "Client.name" to "getName()" or any getter to read this attribute
     * those getter names are stored while generating in order to be found
     * later when generating toString()
     */
    protected Map<String, String> attributeToGetterName = new HashMap<String, String>();

//    @Override
//    protected Transformer<ObjectModel, ObjectModel> initPreviousTransformer() {
//        return new WikittyPurifierTransformer();
//    }

    protected List<ObjectModelClass> entitiesWithInheritedOperations =
            new ArrayList<ObjectModelClass>();

    @Override
    public void transformFromModel(ObjectModel model) {

        // contains boths businessEntities and metaExtensions
        // elements may have one of the two sterotypes or both
        // but elements with none of them aren't in this list
        List<ObjectModelClass> modelBoth = new ArrayList<ObjectModelClass>();

        // fill modelBusinessEntities with entities found in model
        for (ObjectModelClass clazz : model.getClasses()) {
            if (WikittyTransformerUtil.isBusinessEntity(clazz) ||
                    WikittyTransformerUtil.isMetaExtension(clazz)) {
                log.info("will treate class "+clazz.getQualifiedName());
                modelBoth.add(clazz);
            }
        }

        for (ObjectModelClass clazz : modelBoth) {
            ObjectModelClass abstractClass = createClass(
                    businessEntityToDTOName(clazz),
                    clazz.getPackageName());
            processedClasses.put(clazz, abstractClass);
            addInterface(abstractClass, clazz.getQualifiedName());
        }

        for (ObjectModelClass clazz : modelBoth) {
            if (WikittyTransformerUtil.isMetaExtension(clazz)) {
                addMetaExtensionOperations(clazz, processedClasses.get(clazz));
            } else if (WikittyTransformerUtil.isBusinessEntity(clazz)) {
                addOperations(clazz, processedClasses.get(clazz));
            }
        }

        // at this time, all operations in generated abstracts are just the operations
        // like get/set etc. we will copy all operations of a given class to all children
        // that's why constructors and others operations are not yet added
        for (ObjectModelClass clazz : modelBoth) {
            if (WikittyTransformerUtil.isBusinessEntity(clazz)) {
                addInheritedOperations(clazz, processedClasses.get(clazz));
            }
        }

        for (ObjectModelClass clazz : modelBoth) {
            ObjectModelClass abstractClassForThisEntity = processedClasses.get(clazz);
            addConstructors(abstractClassForThisEntity);
            addToString(clazz, abstractClassForThisEntity);
            addBusinessEntityMethods(clazz, abstractClassForThisEntity);
            addModelOperations(clazz, abstractClassForThisEntity);
        }

        processedClasses.clear();
    }

    protected void addSerialVersionUID(ObjectModelClass clazz) {
        // adding a generated serialVersionUID
        Random random = new Random();
        Long serialVersionUIDs = random.nextLong();
        addConstant(clazz,
                "serialVersionUID",
                "long",
                serialVersionUIDs.toString() + "L",
                ObjectModelModifier.PRIVATE);
    }

    protected void addConstructors(ObjectModelClass clazz) {

        ObjectModelOperation constructor = addConstructor(clazz, ObjectModelModifier.PUBLIC);
        setOperationBody(constructor, ""
/*{
}*/);

        constructor = addConstructor(clazz, ObjectModelModifier.PUBLIC);
        addParameter(constructor, "String", "wikittyId");
        setOperationBody(constructor, ""
/*{
        this.wikittyId=wikittyId;
}*/);

    }

    protected void addOperations(ObjectModelClass businessEntity, ObjectModelClass abstractClass) {
        // generating operations with bodies to realize contract
        for (ObjectModelAttribute attribute : businessEntity.getAttributes()) {
            if (attribute.isNavigable()) {
                // needed below, in templates
                String attributeType = WikittyTransformerUtil.generateResultType(attribute, false);
                String attributeObjectType = WikittyTransformerUtil.generateResultObjectType(attribute, false);
                String attributeName = attribute.getName();

                //add necessary import
                addImport(abstractClass, attribute.getType());

                //If alternative name, use it
                if (attribute.hasTagValue(TAG_ALTERNATIVE_NAME)) {
                    attributeName = attribute.getTagValue(TAG_ALTERNATIVE_NAME);
                }

                String getterName;

                if (WikittyTransformerUtil.isAttributeCollection(attribute)) {
                    // attribute is a collection, we will generate operations
                    // get, add, remove and clear

                    //calculate template parameters
                    String attributeTypeCollectionStrict =
                            WikittyTransformerUtil.generateResultType(attribute, true);
                    String attributeTypeCollectionGeneric =
                            "Collection<" + attributeType + ">";
                    String attributeTypeVarargs = attributeType + "...";
                   
                    String attributeObjectTypeCollectionStrict =
                            WikittyTransformerUtil.generateResultObjectType(attribute, true);
                    String attributeObjectTypeCollectionGeneric =
                            "Collection<" + attributeObjectType + ">";
                    String attributeObjectTypeVarargs = attributeObjectType + "...";

                    String attributeTypeCollectionStrictImpl = null;
                    String collectionType = WikittyTransformerUtil.getCollectionTypeName(attribute);
                    if (Set.class.getSimpleName().equals(collectionType)){
                        addImport(abstractClass, Set.class);
                        addImport(abstractClass, LinkedHashSet.class);
                        attributeTypeCollectionStrictImpl = "LinkedHashSet<String>";
                    }
                    if (List.class.getSimpleName().equals(collectionType)){
                        addImport(abstractClass, List.class);
                        addImport(abstractClass, ArrayList.class);
                        attributeTypeCollectionStrictImpl = "ArrayList<String>";
                    }


                    //adding the attribute
                    ObjectModelAttribute modelAttribute = addAttribute(abstractClass,
                            attributeName + " = new " + attributeTypeCollectionStrictImpl + "()",
                            attributeTypeCollectionStrict);
//                    String annotation = "WikittyField(fqn=\""+ businessEntity.getName() + "." + attributeName +"\")";
                    ObjectModelAnnotation annotation = addAnnotation(abstractClass, modelAttribute, "WikittyField");
                    addAnnotationParameter(abstractClass, annotation,
                                           "fqn", businessEntity.getName() + "." + attributeName);
                    addImport(abstractClass, "org.nuiton.wikitty.entities.WikittyField");


                    // adding the getter
                    getterName = "get" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation getter = addOperation(abstractClass, getterName, attributeTypeCollectionStrict);
                    addAnnotation(abstractClass, getter, "Override");
                    String getterBody = ""
/*{
        return <%=attributeName%>;
}*/;
                    setOperationBody(getter, getterBody);

                    // adding the setter
                    getterName = "set" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation setter = addOperation(abstractClass, getterName, "void");
                    addAnnotation(abstractClass, setter, "Override");
                    addParameter(setter, attributeTypeCollectionStrict, attributeName);
                    String setterBody = ""
/*{
        if (<%=attributeName%> == null){
            this.<%=attributeName%> = new <%=attributeTypeCollectionStrictImpl%>();
        } else {
            // make copy to prevent modification of source collection
            this.<%=attributeName%>=new <%=attributeTypeCollectionStrictImpl%>(<%=attributeName%>);
        }
        modificationCount++;
}*/;
                    setOperationBody(setter, setterBody);

                    // adding the addAll
                    String addAllName = "addAll" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation addAll = addOperation(abstractClass, addAllName, "void");
                    addAnnotation(abstractClass, addAll, "Override");
                    addParameter(addAll, attributeTypeCollectionGeneric, attributeName);
                    String addAllBody = ""
/*{
        if (this.<%=attributeName%> == null){
            this.<%=attributeName%> = new <%=attributeTypeCollectionStrictImpl%>();
        }
        this.<%=attributeName%>.addAll(<%=attributeName%>);
        modificationCount++;
}*/;
                    setOperationBody(addAll, addAllBody);

                    //adding the add method
                    String addName = "add" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation adder = addOperation(abstractClass, addName, "void");
                    addAnnotation(abstractClass, adder, "Override");
                    addParameter(adder, attributeTypeVarargs, "element");
                    String adderBody = ""
/*{
        if (this.<%=attributeName%> == null){
            this.<%=attributeName%> = new <%=attributeTypeCollectionStrictImpl%>();
        }
        for (<%=attributeType%> v : element) {
            this.<%=attributeName%>.add(v);
        }
        modificationCount++;
}*/;
                    setOperationBody(adder, adderBody);

                    //adding the remove method
                    String removeName = "remove" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation remover = addOperation(abstractClass, removeName, "void");
                    addAnnotation(abstractClass, remover, "Override");
                    addParameter(remover, attributeTypeVarargs, "element");
                    String removerBody = ""
/*{
        if (this.<%=attributeName%> != null) {
            for (<%=attributeType%> v : element) {
                <%=attributeName%>.remove(element);
            }
            modificationCount++;
        }
}*/;
                    setOperationBody(remover, removerBody);

                    //adding the clear method
                    String clearName = "clear" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation clear = addOperation(abstractClass, clearName, "void");
                    addAnnotation(abstractClass, clear, "Override");
                    String clearBody = ""
/*{
        if (this.<%=attributeName%> != null) {
            <%=attributeName%>.clear();
            modificationCount++;
        }
}*/;
                    setOperationBody(clear, clearBody);

                    //
                    // Pour avoir acces en mode Objet entity
                    //
                    if (attributeObjectType != null) {
                        // C'est un BusinessEntity on ajoute les getter et setter specifique

                        getterName = "get" + StringUtils.capitalize(attributeName);
                        ObjectModelOperation getterObject = addOperation(
                                abstractClass, getterName, attributeObjectTypeCollectionStrict);
                        addParameter(getterObject, "boolean", "exceptionIfNotLoaded");
                        addAnnotation(abstractClass, getterObject, "Override");
                        String getterObjectBody = ""
/*{
        if (exceptionIfNotLoaded) {
            throw new WikittyException("Preload is not implemented in DTO");
        } else {
            return null;
        }
}*/;
                        setOperationBody(getterObject, getterObjectBody);

                        // adding the setter
                        getterName = "set" + StringUtils.capitalize(attributeName);
                        ObjectModelOperation setterObject = addOperation(
                                abstractClass, getterName + "Entity", "void");
                        addAnnotation(abstractClass, setterObject, "Override");
                        addParameter(setterObject, attributeObjectTypeCollectionGeneric, attributeName);
                        String setterObjectBody = ""
/*{
        <%=attributeTypeCollectionStrictImpl%> tmp = new <%=attributeTypeCollectionStrictImpl%>();
        for (<%=attributeObjectType%> e : <%=attributeName%>) {
            tmp.add(e.getWikittyId());
        }
        <%=getterName%>(tmp);
}*/;
                        setOperationBody(setterObject, setterObjectBody);

                        // adding the addAll
                        String addAllNameObject = "addAll" + StringUtils.capitalize(attributeName);
                        ObjectModelOperation addAllObject = addOperation(
                                abstractClass, addAllNameObject + "Entity", "void");
                        addAnnotation(abstractClass, addAllObject, "Override");
                        addParameter(addAllObject, attributeObjectTypeCollectionGeneric, attributeName);
                        String addAllObjectBody = ""
/*{
        <%=attributeTypeCollectionStrictImpl%> tmp = new <%=attributeTypeCollectionStrictImpl%>();
        for (<%=attributeObjectType%> e : <%=attributeName%>) {
            tmp.add(e.getWikittyId());
        }
        <%=addAllNameObject%>(tmp);
}*/;
                        setOperationBody(addAllObject, addAllObjectBody);

                        //adding the add method
                        String addNameObject = "add" + StringUtils.capitalize(attributeName);
                        ObjectModelOperation adderObject = addOperation(
                            abstractClass, addNameObject, "void");
                        addAnnotation(abstractClass, adderObject, "Override");
                        addParameter(adderObject, attributeObjectTypeVarargs, "element");
                        String adderObjectBody = ""
/*{
        String[] tmp = new String[element.length];
        for (int i=0; i<element.length; i++) {
            tmp[i] = element[i].getWikittyId();
        }
        <%=addNameObject%>(tmp);
}*/;
                        setOperationBody(adderObject, adderObjectBody);

                        //adding the remove method
                        String removeNameObject = "remove" + StringUtils.capitalize(attributeName);
                        ObjectModelOperation removerObject = addOperation(
                                abstractClass, removeNameObject, "void");
                        addAnnotation(abstractClass, removerObject, "Override");
                        addParameter(removerObject, attributeObjectTypeVarargs, "element");
                        String removerObjectBody = ""
/*{
        String[] tmp = new String[element.length];
        for (int i=0; i<element.length; i++) {
            tmp[i] = element[i].getWikittyId();
        }
        <%=removeNameObject%>(tmp);
}*/;
                        setOperationBody(removerObject, removerObjectBody);

                    }

                } else {
                    //No multiplicity

                    //add necessary import
                    addImport(abstractClass, attribute.getType());

                    //adding the attribute
                    ObjectModelAttribute modelAttribute = addAttribute(abstractClass, attributeName, attributeType);
//                    String annotation = "WikittyField(fqn=\"" + businessEntity.getName() + "."+attributeName + "\")";
                    ObjectModelAnnotation annotation = addAnnotation(abstractClass, modelAttribute, "WikittyField");
                    addAnnotationParameter(abstractClass, annotation,
                                           "fqn", businessEntity.getName() + "." + attributeName);
                    addImport(abstractClass, "org.nuiton.wikitty.entities.WikittyField");

                    // adding getter
                    getterName = "get" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation getter = addOperation(abstractClass, getterName, attributeType);
                    addAnnotation(abstractClass, getter, "Override");
                    setOperationBody(getter, ""
/*{
        return <%=attributeName%>;
}*/);

                    //adding setter
                    String setterName = "set" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation setter = addOperation(abstractClass, setterName, "void");
                    addAnnotation(abstractClass, setter, "Override");
                    addParameter(setter, attributeType, attributeName);
                    setOperationBody(setter, ""
/*{
        this.<%=attributeName%>=<%=attributeName%>;
        modificationCount++;
}*/);

                    //
                    // Pour avoir acces en mode Objet entity
                    //
                    if (attributeObjectType != null) {
                        // C'est un BusinessEntity on ajoute les getter et setter specifique

                        getterName = "get" + StringUtils.capitalize(attributeName);
                        ObjectModelOperation getterObject = addOperation(
                                abstractClass, getterName, attributeObjectType);
                        addParameter(getterObject, "boolean", "exceptionIfNotLoaded");
                        addAnnotation(abstractClass, getterObject, "Override");
                        setOperationBody(getterObject, ""
/*{
        if (exceptionIfNotLoaded) {
            throw new WikittyException("Preload is not implemented in DTO");
        } else {
            return null;
        }
}*/);

                        //adding setter
                        String setterNameObject = "set" + StringUtils.capitalize(attributeName);
                        ObjectModelOperation setterObject = addOperation(
                                abstractClass, setterNameObject, "void");
                        addAnnotation(abstractClass, setterObject, "Override");
                        addParameter(setterObject, attributeObjectType, attributeName);
                        setOperationBody(setterObject, ""
/*{
        <%=setterNameObject%>(<%=attributeName%>.getWikittyId());
}*/);

                    }
                }

                // save the getter name for this attribute
                attributeToGetterName.put(businessEntity.getName() + "." + attributeName, getterName);
            }
        }
    }

    /**
     * Add inherited operations to the abstract generated from an entity.
     * Method browse superClasses to copy methods and their bodies. The method
     * deal with imports, cause bodies call the Helper of the classe where
     * the attribute was declared.
     * 
     * If A inherit from B and B inherit from and A, B, C not in the same package
     * we B need to import CHelper and A need to import BHelper. But, since A
     * has C attributes too, it needs CHelper too. So when we will process B,
     * we will save needed imports, A will get them thus import CHelper.
     * @param businessEntity
     * @param abstractClass
     */
    protected void addInheritedOperations(ObjectModelClass businessEntity, ObjectModelClass abstractClass) {

            // add attributes and methods inherited from super classes
            for (ObjectModelClass superClass : businessEntity.getSuperclasses()) {

                // process super classes first, so we will get inherited operations from superclass
                addInheritedOperations(superClass, abstractClass);

                if (WikittyTransformerUtil.isBusinessEntity(superClass)) {

                    //Add operations for the super class
                    addOperations(superClass, abstractClass);
                }
            }

            entitiesWithInheritedOperations.add(businessEntity);
    }

    /**
     * add a toString method
     * if a toString tagValue is attached to businessEntity, it will be used
     * to generate a toString as this :
     * 
     * given "hello {Person.name|unknow}"
     * 
     * will try to replace first {...} by name field value for extension Person.
     * if this information is not available, will do unknow.
     * @param businessEntity
     * @param abstractClass
     */
    protected void addToString(ObjectModelClass businessEntity, ObjectModelClass abstractClass) {

        String toStringOperationBody;

        if (businessEntity.hasTagValue(TAG_TO_STRING)) {
            String toStringPattern = businessEntity.getTagValue(TAG_TO_STRING);

            // toStringPattern is something like
            // "hello {Person.name|unknow} employe of {Company.name|unknow}"
            //

            Matcher matcher = toStringTagValuePattern.matcher(toStringPattern);

            while (matcher.find()) {
                String wholeMatch = matcher.group(0); // "{foo|bar}"
                String variableName = matcher.group(1); // "foo"
                String defaultValue = matcher.group(3); // "bar", may be null

                if (defaultValue == null) {
                    defaultValue = "";
                }

                if (attributeToGetterName.containsKey(variableName)) {
                    String getterName = attributeToGetterName.get(variableName);
                    toStringPattern = toStringPattern.replace(wholeMatch, ""
/*{"
             + <%=getterName%>().toString() +
             "}*/);
                } else {
                    log.warn("no field " + variableName + " in " + businessEntity.getQualifiedName());
                    toStringPattern = toStringPattern.replace(wholeMatch, defaultValue);
                }
            }

            toStringOperationBody = ""
/*{
        return "<%=toStringPattern%>";
}*/;
        } else {
            // no toString tagValue provided, generating a default toString
            toStringOperationBody = ""
/*{
        return "dto:"+getWikittyId()+":"+getWikittyVersion();
}*/;
        }

        ObjectModelOperation toString = addOperation(abstractClass, "toString", "String");
        addAnnotation(abstractClass, toString, "Override");
        setOperationBody(toString, toStringOperationBody);
    }

    protected void addMetaExtensionOperations(ObjectModelClass metaExtension,
                                              ObjectModelClass abstractClassForThisMetaExtension) {

        //add standard attribute operations for meta-extension
        addOperations(metaExtension, abstractClassForThisMetaExtension);

    }

    /**
     * Method to get the generated class name
     * @param clazz
     * @return
     */
    protected String businessEntityToDTOName(ObjectModelClass clazz) {
        return clazz.getName() + "DTO";
    }

    protected void addBusinessEntityMethods(ObjectModelClass entity, ObjectModelClass dtoClass) {

        //needed in templates
        String entityName = entity.getName();
        String dtoClassName = businessEntityToDTOName(entity);

        //adding wikittyId attribute
        addAttribute(dtoClass, "wikittyId", "String");

        //adding modificationCount attribute. Used to change version.
        addAttribute(dtoClass, "modificationCount = 0", "int");

        //adding getWikittyId method
        ObjectModelOperation getter = addOperation(dtoClass, "getWikittyId", "String");
        addAnnotation(dtoClass, getter, "Override");
        setOperationBody(getter, ""
/*{
        return wikittyId;
}*/);

        //adding setWikittyId method
        ObjectModelOperation setter = addOperation(dtoClass, "setWikittyId", "void");
        addParameter(setter, "String", "wikittyId");
        setOperationBody(setter, ""
/*{
        this.wikittyId=wikittyId;
}*/);

        //adding wikittyVersion attribute
        addAttribute(dtoClass, "wikittyVersion", "String");



        //adding getWikittyVersion method
        ObjectModelOperation getWikittyVersion = addOperation(dtoClass,
                "getWikittyVersion", "String");
        addAnnotation(dtoClass, getWikittyVersion, "Override");
        setOperationBody(getWikittyVersion, ""
/*{
        String result = wikittyVersion;
        if (modificationCount > 0) {
            result += "." + modificationCount;
        }
        return result;
}*/);

        //adding setWikittyVersion method
        ObjectModelOperation setWikittyVersion = addOperation(dtoClass,
                "setWikittyVersion", "void");
        addParameter(setWikittyVersion, "String", "wikittyVersion");
        setOperationBody(setWikittyVersion, ""
/*{
        this.wikittyVersion=wikittyVersion;
        modificationCount=0;
}*/);

        //adding necessary imports
        addImport(dtoClass, PropertyChangeListener.class);
        addImport(dtoClass, Collection.class);

        // adding addPropertyChangeListener
        ObjectModelOperation addPropertyChangeListener = addOperation(dtoClass,
                "addPropertyChangeListener", "void");
        addAnnotation(dtoClass, addPropertyChangeListener, "Override");
        addParameter(addPropertyChangeListener, "PropertyChangeListener", "listener");
        setOperationBody(addPropertyChangeListener, ""
/*{
        throw new UnsupportedOperationException("Not supported yet.");
}*/);

        // adding removePropertyChangeListener
        ObjectModelOperation removePropertyChangeListener = addOperation(dtoClass,
                "removePropertyChangeListener", "void");
        addAnnotation(dtoClass, removePropertyChangeListener, "Override");
        addParameter(removePropertyChangeListener, "PropertyChangeListener", "listener");
        setOperationBody(removePropertyChangeListener, ""
/*{
        throw new UnsupportedOperationException("Not supported yet.");
}*/);

        // adding addPropertyChangeListener 2 parameters
        ObjectModelOperation addPropertyChangeListener2 = addOperation(dtoClass,
                "addPropertyChangeListener", "void");
        addAnnotation(dtoClass, addPropertyChangeListener2, "Override");
        addParameter(addPropertyChangeListener2, "String", "property");
        addParameter(addPropertyChangeListener2, "PropertyChangeListener", "listener");
        setOperationBody(addPropertyChangeListener2, ""
/*{
        throw new UnsupportedOperationException("Not supported yet.");
}*/);

        // adding removePropertyChangeListener 2 parameters
        ObjectModelOperation removePropertyChangeListener2 = addOperation(dtoClass,
                "removePropertyChangeListener", "void");
        addAnnotation(dtoClass, removePropertyChangeListener2, "Override");
        addParameter(removePropertyChangeListener2, "String", "property");
        addParameter(removePropertyChangeListener2, "PropertyChangeListener", "listener");
        setOperationBody(removePropertyChangeListener2, ""
/*{
        throw new UnsupportedOperationException("Not supported yet.");
}*/);

        //adding getExtensionFields method
        ObjectModelOperation getExtensionFields = addOperation(dtoClass,
                "getExtensionFields", "Collection<String>");
        addAnnotation(dtoClass, getExtensionFields, "Override");
        addParameter(getExtensionFields, "String", "ext");
        setOperationBody(getExtensionFields, ""
/*{
        throw new UnsupportedOperationException("Not supported yet.");
}*/);

        //adding getExtensionNames method
        ObjectModelOperation getExtensionNames = addOperation(dtoClass,
                "getExtensionNames", "Collection<String>");
        addAnnotation(dtoClass, getExtensionNames, "Override");
        setOperationBody(getExtensionNames, ""
/*{
        throw new UnsupportedOperationException("Not supported yet.");
}*/);

        //adding getStaticExtension method
        ObjectModelOperation getStaticExtensionNames = addOperation(dtoClass,
                "getStaticExtensionNames", "Collection<String>");
        addAnnotation(dtoClass, getStaticExtensionNames, "Override");
        setOperationBody(getStaticExtensionNames, ""
/*{
        throw new UnsupportedOperationException("Not supported yet.");
}*/);

        //adding getField method
        ObjectModelOperation getField = addOperation(dtoClass, "getField", "Object");
        addAnnotation(dtoClass, getField, "Override");
        addParameter(getField, "String", "ext");
        addParameter(getField, "String", "fieldName");
        setOperationBody(getField, ""
/*{
        throw new UnsupportedOperationException("Not supported yet.");
}*/);

        //adding getField method
        ObjectModelOperation getFieldAsObject = addOperation(dtoClass, "getFieldAsObject", "Object");
        addAnnotation(dtoClass, getFieldAsObject, "Override");
        addParameter(getFieldAsObject, "String", "ext");
        addParameter(getFieldAsObject, "String", "fieldName");
        setOperationBody(getFieldAsObject, ""
/*{
        throw new UnsupportedOperationException("Not supported yet.");
}*/);

        //adding setField method
        ObjectModelOperation setField = addOperation(dtoClass, "setField", "void");
        addAnnotation(dtoClass, setField, "Override");
        addParameter(setField, "String", "ext");
        addParameter(setField, "String", "fieldName");
        addParameter(setField, "Object", "value");
        setOperationBody(setField, ""
/*{
        throw new UnsupportedOperationException("Not supported yet.");
}*/);

        //adding copyFrom method
        ObjectModelOperation copyFrom = addOperation(dtoClass, "copyFrom", "void");
        addAnnotation(dtoClass, copyFrom, "Override");
        addParameter(copyFrom, "BusinessEntity", "source");
        String copyFromBody = ""
/*{
        if (!(source instanceof <%=entityName%>)){
            throw new WikittyException("Can't copy source object " + source +
                    ". They are not of the same type");
        }

        <%=entityName%> sourceCopy = (<%=entityName%>)source;
}*/;

        copyFromBody = addCopyFromAttributes(entity, copyFromBody);

        copyFromBody +=""
/*{
        setWikittyVersion(sourceCopy.getWikittyVersion());
}*/;

        addImport(dtoClass, "org.nuiton.wikitty.WikittyException");
        addImport(dtoClass, "org.nuiton.wikitty.entities.BusinessEntity");

        setOperationBody(copyFrom, copyFromBody);
    }

    /**
     * Generate the copyFrom method. This method set all fields of the class
     * from all the fields of another instance of the same class.
     * @param businessEntity
     * @param methodBody
     * @return
     */
    protected String addCopyFromAttributes(ObjectModelClass businessEntity,
                                         String methodBody){

        // process super classes first (for multi-level inheritance)
        for (ObjectModelClass superClass : businessEntity.getSuperclasses()) {

            if (WikittyTransformerUtil.isBusinessEntity(superClass)) {
                methodBody=addCopyFromAttributes(superClass, methodBody);
            }
        }
        
        methodBody=copyFromAttributesOfClass(businessEntity, methodBody);

        return methodBody;

    }

    protected String copyFromAttributesOfClass(ObjectModelClass businessEntity,
                                               String methodBody){

        for (ObjectModelAttribute attribute : businessEntity.getAttributes()) {
            if (attribute.isNavigable()) {
                // needed below, in templates
                String attributeType = WikittyTransformerUtil.generateResultType(attribute, false);
                String attributeName = attribute.getName();
                String attributeTypeInSetImpl = null;
                String attributeTypeInSet = null;
                String collectionType = WikittyTransformerUtil.getCollectionTypeName(attribute);
                if (Set.class.getSimpleName().equals(collectionType)) {
                    attributeTypeInSet = "Set<String>";
                    attributeTypeInSetImpl = "LinkedHashSet<String>";
                }
                if (List.class.getSimpleName().equals(collectionType)) {
                    attributeTypeInSet = "List<String>";
                    attributeTypeInSetImpl = "ArrayList<String>";
                }

                //If alternative name, use it
                if (attribute.hasTagValue(TAG_ALTERNATIVE_NAME)) {
                    attributeName = attribute.getTagValue(TAG_ALTERNATIVE_NAME);
                }
                String capitalizedAttributeName = StringUtils.capitalize(attributeName);


                if (WikittyTransformerUtil.isAttributeCollection(attribute)) {
                    methodBody += ""
/*{
        <%=attributeTypeInSet%> <%=attributeName%> = sourceCopy.get<%=capitalizedAttributeName%>();
        if (<%=attributeName%> != null){
            set<%=capitalizedAttributeName%>(new <%=attributeTypeInSetImpl%>(<%=attributeName%>));
        }
}*/;
                } else {
                    methodBody += ""
/*{
        set<%=capitalizedAttributeName%>(sourceCopy.get<%=capitalizedAttributeName%>());
}*/;
                }
            }
        }
        return methodBody;
    }

    protected void addModelOperations(ObjectModelClass businessEntity, ObjectModelClass dtoClass){
        Collection<ObjectModelOperation> operations = businessEntity.getOperations();

        for (ObjectModelOperation operation:operations){
            ObjectModelOperation dtoOperation = addOperation(dtoClass,
                    operation.getName(), operation.getReturnType());

            Collection<ObjectModelParameter> parameters = operation.getParameters();
            for (ObjectModelParameter parameter:parameters){
                addParameter(dtoOperation, parameter.getType(), parameter.getName());
            }
            addAnnotation(dtoClass, dtoOperation, "Override");
            setOperationBody(dtoOperation, ""
/*{
        throw new UnsupportedOperationException("Not supported yet.");
}*/);
        }
    }

}
