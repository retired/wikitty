/*
 * #%L
 * Wikitty :: generators
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.generator;

/**
 * Definition des noms de l'ensemble des tags values utilisable dans wikitty.
 * Cette liste ne contient que les tags values qui influencent le comportement
 * de wikitty ou qui aide a la normalisation de tag value. L'utilisateur peut
 * en ajouter d'autre.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface WikittyTagValue {

    /**
     * version: indique la version de l'extension.
     * <ul>
     * <li> target: extension for generation</li>
     * </ul>
     */
    public static final String TAG_VERSION = "version";

    /**
     * alternativeName: ce champs peut etre mis pour qu'a la generation un
     * des accesseur avec un autre nom soit genere.
     * <ul>
     * <li> target: field</li>
     * </ul>
     */
    public static final String TAG_ALTERNATIVE_NAME = "alternativeName";

    /**
     * documentation: indique la documentation a mettre lors de la generation
     * peut aussi servir de documentation utilisateur a l'execution.
     * <ul>
     * <li> target: extension or field</li>
     * </ul>
     */
    public static final String TAG_DOCUMENTATION = "documentation";

    /**
     * toString: indique la facon de representer une extension textuellement.
     * exemple:
     * <ul>
     * <li> "%Person.lastName$s %Person.firstName$s: %Person.birthday$tm %Person.birthday$te,%Person.birthday$tY"</li>
     * <li> "Hello %Person.firstName|unknow$s" if firstName field doesn't exist, unknow is used</li>
     * </ul>
     * <ul>
     * <li> target: extension</li>
     * <li> target: field (pour surcharger le toString par defaut de l'objet pointe pour un champs en particulier)</li>
     * </ul>
     * 
     * See: org.nuiton.wikitty.WikittyUtil#format
     */
    public static final String TAG_TO_STRING = "toString";

    /**
     * sortOrder: indique le tri par defaut pour cette extension.
     * exemple:
     * <ul>
     * <li> "Person.lastName asc, Person.firstName, Person.birthday desc"
     * Liste des champs dans l'importance de l'ordre de trie. Il est possible
     * d'ajouter asc ou desc apres le champs pour indique qu'il faut trier en
     * ordre croissant ou decroissant.</li>
     * <li> target: extension</li>
     * </ul>
     * 
     * See org.nuiton.wikitty.WikittyUtil#format
     */
    public static final String TAG_SORT_ORDER = "sortOrder";

    /**
     * fieldIndex: indique la position de ce champs dans la liste des champs.
     * *la valeur est un nombre réel pour facilement inserer un champs entre deux
     * champs sans devoir tous les modifiers.
     */
    public static final String TAG_FIELD_INDEX = "fieldIndex";

    /** 
     * Unique: le champs doit etre une collection, ce tag indique alors s'il vaut
     * true que la collection ne peut pas contenir de doublon (Set) si unique
     * est false alors la collection peut contenir des doublons (List).
     * <ul>
     * <li> target: collection field</li>
     * </ul>
     */
    public static final String TAG_UNIQUE = "unique";

    /**
     * notNull: indique que le champs ne peut pas etre null, il doit forcement
     * avoir une valeur lors de la sauvegarde.
     * <ul>
     * <li> target: field</li>
     * </ul>
     */
    public static final String TAG_NOT_NULL = "notNull";

    /**
     * default: indique la valeur par defaut si le champs est null.
     * <ul>
     * <li> target: field</li>
     * </ul>
     * @since 2.9
     */
    public static final String TAG_DEFAULT_VALUE = "default";

    /**
     * pattern: pour une String indique que le champs doit respecter un certain
     * pattern, sinon la sauvegarde echoue. Pour un numeric ou une date pattern
     * peut-etre utilise comme masque de saisie. Exemple:
     * <ul>
     * <li>"A.*" le champs doit commencer par 'A'</li>
     * <li>"[0-9]+\.[0-9][0-9]" le nombre doit avoir deux chiffres apres la virgule
     * Lors de l'affichage si un pattern est defini, il peut etre utilise</li>
     * <li> target: numeric or String field
     * </ul>
     */
    public static final String TAG_PATTERN = "pattern";

    /**
     * indexed: indique qu'un champs ne doit pas etre indexe, par defaut si ce
     * tag value n'existe pas il vaut true. Il faut donc le positionner
     * explicitement a false si on ne veut pas indexer un champs
     * <ul>
     * <li> target: field</li>
     * </ul>
     */
    public static final String TAG_INDEXED = "indexed";

    /**
     * crypt: indique que le champs doit etre crypte avant d'etre sauve et
     * decrypter lors de la restauration, de plus le champs ne
     * sera pas indexe. Si le cryptage n'est pas possible, la sauvegarde ne se
     * fera pas (Exeption).
     * exemple:
     * <ul>
     * <li>crypt=Blowfish:password
     *
     * TODO: si le mot de passe n'est pas specifie, utiliser le mot de passe
     * du fichier de configuration</li>
     * <li> target: field</li>
     * </ul>
     * @see "http://docs.oracle.com/javase/6/docs/technotes/guides/security/SunProviders.html#SunJCEProvider"
     */
    public static final String TAG_CRYPT = "crypt";

    /**
     * preload: indique les champs de type Wikitty qui doivent etre preloade
     * lors du chargement de cette extension.
     * exemple:
     * <ul>
     * <li> preload="Company.employee,Employee.person;Company.address"</li>
     * <li> target: extension</li>
     * </ul>
     */
    public static final String TAG_PRELOAD = "preload";

    /**
     * subtype: indique que le champs a un sous type. Ce sous type est différent
     * pour chaque type possible d'un champs
     * <ul>
     * <li> Boolean: aucun pour l'instant</li>
     * <li> Binary: aucun pour l'instant</li>
     * <li> Date
     *     <ul>
     *     <li> 'date' (defaut) indique que seule la date est pertinante</li>
     *     <li> 'month' indique que seule le mois et l'annee sont pertinants</li>
     *     <li> 'time' indique que seule l'heure est pertinante</li>
     *     <li> 'datetime' indique que la date et l'heure sont pertinantes</li>
     *     </ul>
     * </li>
     * <li> Numeric
     *     <ul>
     *     <li> 'real' (defaut) indique que le nombre est de type reel</li>
     *     <li> 'integer' indique que le nombre est de type entier</li>
     *     <li> 'currency' indique que le nombre est de type reel et represente une somme d'argent</li>
     *     <li> 'percent' indique que le nombre est un pourcentage</li>
     *     </ul>
     * </li>
     * <li> String
     *     <ul>
     *     <li> 'char' indique que la chaine ne peut qu'un caractere</li>
     *     <li> 'monoline' (defaut) indique que la chaine ne peut contenir qu'une ligne</li>
     *     <li> 'multiline' indique que la chaine peut-etre multiligne</li>
     *     <li> '[mime type]' indique que la chaine represente le type mime
     * precisse. exemple: 'text/plain' ou 'text/javascript' ou 'text/html'</li>
     *     </ul>
     * </li>
     * <li> Wikitty: aucun pour l'instant</li>
     * <li> target: field</li>
     * </ul>
     */
    public static final String TAG_SUBTYPE = "subtype";

    /**
     * allowed: indique que le champs devra prendre sa valeur dans une des valeurs
     * de ce tag. Les differentes valeurs sont separees par une virgule.
     * <ul>
     * <li> Boolean ne s'applique pas</li>
     * <li> Binary: l'utilisateur ne pourra importer des binaires que du type
     * mime indique (s'additionne avec les resultats de allowedQuery).
     * Ne s'applique que si le champs binaire doit contenir un fichier.
     * Cette contrainte n'est utilisable que cote client lors de la selection
     * du fichier. Pour une verification cote serveur, il faut associer un
     * champs texte (par exemple 'mimetype') qui contiendra la valeur du fichier
     * mis dans le champs binaire et mettre sur ce champs le meme tag value
     * allowed que sur le champs binaire</li>
     * <li> Date ne s'applique pas (voir {@link #TAG_MIN} {@link #TAG_MAX})</li>
     * <li> Numeric ne s'applique pas (voir {@link #TAG_MIN} {@link #TAG_MAX})</li>
     * <li> String: l'utilisateur ne pourra mettre comme valeur que des valeurs
     * presentes dans allowed (s'additionne avec les resultats de allowedQuery).</li>
     * <li> Wikitty: l'utilisateur devra choisir l'objet dans la liste des objets
     * qui ont une des extensions listee. Si allowedQuery est aussi specifie
     * allowedQuery prend le dessus sur allowed.
     *<p>
     * exemple:
     * <ul>
     * <li> String companyType allowed="SA,SARL,SAS" allowedQuery="SELECT Company.companyType WHERE extension=Company"</li>
     * <li> Wikitty target allowed="Person,Employee,Company"</li>
     * </ul>
     * </li>
     * <li> target: field</li>
     * </ul>
     */
    public static final String TAG_ALLOWED = "allowed";
    /**
     * Sert a la meme chose que allowed et vient en plus ou en remplacement
     * (pour les Wikitties) des valeur de allowed. La valeur de ce tag doit
     * etre une requete bien formee qui retourne le bon type d'element en
     * fonction du champs (pour cela la requete commencera le plus souvent par
     * un select).
     */
    public static final String TAG_ALLOWED_QUERY = "allowedQuery";
    /**
     * choice indique que l'utilisateur sera guide dans son choix de valeurs
     * grace aux valeurs listees, mais il pourra s'il le souhaite mettre une
     * nouvelle valeur. Les valeurs sont separees par des virgules
     * <p>
     * exemple:
     * <ul>
     * <li>String type choice="SA,SAS,SARL,SARL SCOOP,EURL"</li>
     * </ul>
     */
    public static final String TAG_CHOICE = "choice";
    /**
     * Sert a la meme chose que choice et vient en plus des valeur de choice.
     * La valeur de ce tag doit etre une requete bien formee qui retourne le bon
     * type d'element en fonction du champs (pour cela la requete commencera le
     * plus souvent par un select).
     */
    public static final String TAG_CHOICE_QUERY = "choiceQuery";

    /**
     * min indique la valeur minimal que peut prendre le champs. Cela s'applique
     * au Date et Numeric.
     */
    public static final String TAG_MIN = "min";

    /**
     * minQuery sert a la meme chose que min mais prend sa valeur grace a une
     * requete. Si min et minQuery sont tous les deux présents, minQuery est
     * utilisee sauf si aucun resultat n'est retourne par la requete.
     */
    public static final String TAG_MIN_QUERY = "minQuery";

    /**
     * max indique la valeur maximal que peut prendre le champs. Cela s'applique
     * au Date et Numeric.
     */
    public static final String TAG_MAX = "max";

    /**
     * maxQuery sert a la meme chose que max mais prend sa valeur grace a une
     * requete. Si max et maxQuery sont tous les deux présents, maxQuery est
     * utilisee sauf si aucun resultat n'est retourne par la requete.
     */
    public static final String TAG_MAX_QUERY = "maxQuery";


}
