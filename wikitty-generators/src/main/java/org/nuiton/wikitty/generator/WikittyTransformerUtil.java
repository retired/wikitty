/*
 * #%L
 * Wikitty :: generators
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.generator;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;


public class WikittyTransformerUtil {
    
    /** utility class should not be instanciated */
    private WikittyTransformerUtil() {}
    
    protected static final String ENTITY_PACKAGE = "org.nuiton.wikitty.entities";
    protected static final String BUSINESS_ENTITY_CLASS_FQN =
            ENTITY_PACKAGE + ".BusinessEntity";
    protected static final String BUSINESS_ENTITY_WIKITTY_CLASS_FQN =
            ENTITY_PACKAGE + ".BusinessEntityImpl";
    protected static final String WIKITTY_CLASS_FQN =
            ENTITY_PACKAGE + ".Wikitty";
    protected static final String WIKITTY_EXTENSION_CLASS_FQN =
            ENTITY_PACKAGE + ".WikittyExtension";

    /** @deprecated name change : see ticket #798. use BUSINESS_ENTITY_STEREOTYPE_NAME */
    @Deprecated
    protected static final String BUSINESS_ENTITY_STEREOTYPE_OLD_NAME = "BusinessEntity";

    /** user will use this stereotype in his model */
    protected static final String BUSINESS_ENTITY_STEREOTYPE_NAME = "entity";
    
    /** user will use this stereotype to make an extension a meta-extension */
    protected static final String META_EXTENSION_STEREOTYPE_NAME = "meta";

    /** given a class called Client will return "EXT_CLIENT"
     * should be used as a variable name to store the extension name
     * @param clazz
     * @param withClassNamePrefix add class name as prefix (will return "Client.EXT_CLIENT")
     * @return
     */
    protected static String classToExtensionVariableName(ObjectModelClass clazz,
                                                         boolean withClassNamePrefix) {
        String extensionVariableName = "";
        if (withClassNamePrefix) {
            extensionVariableName += clazz.getName() + ".";
        }
        extensionVariableName += "EXT_" + clazz.getName().toUpperCase();
        return extensionVariableName;
    }

    /** given the field name of the class Client, will return "FIELD_CLIENT_NAME"
     * @param attribute
     * @param withClassNamePrefix add class name as prefix (ie "Client.FIELD_CLIENT_NAME")
     * @return
     */
    protected static String attributeToFielVariableName(ObjectModelAttribute attribute,
                                                        boolean withClassNamePrefix) {

        //Get attribute name
        String name = attribute.getName();

        String fieldVariableName = "";
        if (withClassNamePrefix) {
            fieldVariableName += attribute.getDeclaringElement().getName() + ".";
        }
        fieldVariableName += "FIELD_" + attribute.getDeclaringElement().getName().toUpperCase() + "_" + name.toUpperCase();
        return fieldVariableName;
    }

    /** given "my.java.package.MyClass" or "MyClass" return "MyClass"
     * @param fqn
     * @return
     */
    protected static String FQNtoSimpleName(String fqn) {
        // XXX should use GeneratorUtil#getSimpleName(String)
        int lastDotIndex = fqn.lastIndexOf(".");
        String simpleName = fqn;
        if (lastDotIndex != -1) {
            simpleName = fqn.substring(lastDotIndex + 1);
        }
        return simpleName;
    }

    protected static boolean isBusinessEntity(ObjectModelClass clazz) {
        boolean result = clazz.hasStereotype(BUSINESS_ENTITY_STEREOTYPE_NAME)
                      || clazz.hasStereotype(BUSINESS_ENTITY_STEREOTYPE_OLD_NAME);
        return result;
    }

    public static boolean isMetaExtension(ObjectModelClass clazz) {
        boolean result = clazz.hasStereotype(META_EXTENSION_STEREOTYPE_NAME);
        return result;
    }

    /** 
     * wikitty interface provide getFieldAsString, getFieldAsDate etc. methods
     * this method returns the good name of the method to call depending the
     * type given as parameter
     * @param attribute a name of a business entity or "String", "Integer" etc.
     * @return the name of a method "getFieldAsInt" for example
     */
    protected static String generateGetFieldAsCall(ObjectModelAttribute attribute) {
        String asWhat;
        if (isAttributeCollection(attribute)) {
            asWhat = getCollectionTypeName(attribute);
        } else {
            String simpleTypeName = FQNtoSimpleName(attribute.getType());
            if (commonBinary.contains(simpleTypeName)) {
                asWhat = "Bytes";
            } else if (commonTypes.contains(simpleTypeName)) {
                asWhat = StringUtils.capitalize(simpleTypeName);
            } else {
                asWhat = "Wikitty";
            }
        }
        return "getFieldAs" + asWhat;
    }

    /** for a given type of attribute, the getter returned type must be...
     * @param attribute
     * @param considerMultiplicity
     * @return*/
    protected static String generateResultType(ObjectModelAttribute attribute,
                                               boolean considerMultiplicity) {
        String simpleTypeName = FQNtoSimpleName(attribute.getType());
        if (commonBinary.contains(simpleTypeName)) {
            simpleTypeName = "byte[]"; // return a wikitty Id
        } else if (!commonTypes.contains(simpleTypeName)) {
            simpleTypeName = "String"; // return a wikitty Id
        }

        if (considerMultiplicity && isAttributeCollection(attribute)) {
            simpleTypeName = getCollectionTypeName(attribute) + "<" + simpleTypeName + ">";
        }

        return simpleTypeName;
    }

    /**
     * for a given type of attribute, if is Business Entity return it, otherwize null
     * @param attribute
     * @param considerMultiplicity
     * @return
     */
    protected static String generateResultObjectType(ObjectModelAttribute attribute,
                                               boolean considerMultiplicity) {
        String result = FQNtoSimpleName(attribute.getType());
        if (commonTypes.contains(result)) { // FIXME poussin 20120807 test pour voir si on peut traiter le type Wikitty comme un business || "Wikitty".equalsIgnoreCase(result)) {
            // On ne retourne quelque chose que si c'est un business entity type
            result = null;
        }

        if (result != null && considerMultiplicity && isAttributeCollection(attribute)) {
            result = getCollectionTypeName(attribute) + "<" + result + ">";
        }
        return result;
    }

    protected static String getCollectionTypeName(ObjectModelAttribute attribute) {
        String result;
        if (attribute.isUnique()) {
            result = Set.class.getSimpleName();
        } else {
            result = List.class.getSimpleName();
        }
        return result;
    }

    public static boolean isAttributeCollection(ObjectModelAttribute attribute) {
        return attribute.getMaxMultiplicity() == -1 // -1 is infinity 
            || attribute.getMaxMultiplicity()  >  1;
    }

    protected static String typeToWikittyColumn(String type) {
        String simpleType = FQNtoSimpleName(type);
        String result = simpleType;
        if (!commonTypes.contains(simpleType)) {
            result = "Wikitty";
        } else if(commonNumerics.contains(simpleType)) {
            result = "Numeric";
        } else if(commonStrings.contains(simpleType)) {
            result = "String";
        } else if(commonBinary.contains(simpleType)) {
            result = "Binary";
        }
        return result;
    }

    private static Set<String> commonNumerics;
    static {
        commonNumerics = new HashSet<String>();
        commonNumerics.add(byte.class.getSimpleName());
        commonNumerics.add(Byte.class.getSimpleName());
        commonNumerics.add(short.class.getSimpleName());
        commonNumerics.add(Short.class.getSimpleName());
        commonNumerics.add(int.class.getSimpleName());
        commonNumerics.add(Integer.class.getSimpleName());
        commonNumerics.add(long.class.getSimpleName());
        commonNumerics.add(Long.class.getSimpleName());
        commonNumerics.add(float.class.getSimpleName());
        commonNumerics.add(Float.class.getSimpleName());
        commonNumerics.add(double.class.getSimpleName());
        commonNumerics.add(Double.class.getSimpleName());
    }

    private static Set<String> commonStrings;
    static {
        commonStrings = new HashSet<String>();
        commonStrings.add(char.class.getSimpleName());
        commonStrings.add(Character.class.getSimpleName());
        //FIXME tchemit 2010-11-27 : this does NOT exists in jdk api ?
        commonStrings.add("Char");
        commonStrings.add(String.class.getSimpleName());
    }

    private static Set<String> commonBinary;
    static {
        commonBinary = new HashSet<String>();
        commonBinary.add("byte[]");
        // it's not a good idea to add this because this can clash with user
        // object name, but in graphique modeler, we can't add byte[] as return
        // type :(
        commonBinary.add("Binary");
    }

    private static Set<String> commonTypes;
    static {
        commonTypes = new HashSet<String>();
        commonTypes.addAll(commonNumerics);
        commonTypes.addAll(commonStrings);
        commonTypes.addAll(commonBinary);
        commonTypes.add(boolean.class.getSimpleName());
        commonTypes.add(Boolean.class.getSimpleName());
        commonTypes.add(Date.class.getSimpleName());
    }

    public static String businessEntityToContractName(ObjectModelClass clazz) {
        return clazz.getName();
    }

    public static String businessEntityToAbstractName(ObjectModelClass clazz) {
        return clazz.getName() + "Abstract";
    }

    public static String businessEntityToImplementationName(ObjectModelClass clazz) {
        return clazz.getName() + "Impl";
    }

    public static String businessEntityToHelperName(ObjectModelClass clazz) {
        return clazz.getName() + "Helper";
    }

    public static String tagValuesToString(Map<String, String> tagValues) {
        String result = "";
        if(tagValues != null) {
            for (String tag : tagValues.keySet()) {
                String value = tagValues.get(tag);
                // replaceWith " in string with \"
                value = StringEscapeUtils.escapeJava(value);
                // quote value with "..."
                result += " " + tag + "=\"" + value +"\"";
            }
        }
        return result;
    }
    
    /**
     * Get all super class of class (recursively) ordered from top class to
     * bottom classes.
     * 
     * @param model model (only usefull due to a bug in eugene)
     * @param clazz class to get super classes
     * @return all class super classes
     */
    public static List<ObjectModelClass> getAllSuperClasses(ObjectModel model, ObjectModelClass clazz) {
        List<ObjectModelClass> superClasses = new ArrayList<ObjectModelClass>();
        for (ObjectModelClass superClass : clazz.getSuperclasses()) {
            
            // FIXME echatellier 20120106 it an eugene bug
            // super class are not same instance as model classes
            superClass = model.getClass(superClass.getQualifiedName());

            superClasses.addAll(getAllSuperClasses(model, superClass));
            superClasses.add(superClass);
        }
        return superClasses;
    }
}
