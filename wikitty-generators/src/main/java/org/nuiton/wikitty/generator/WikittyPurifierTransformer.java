/*
 * #%L
 * Wikitty :: generators
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.generator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelBuilder;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelElement;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelGenerator;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelParameter;
import org.nuiton.eugene.models.object.ObjectModelTransformer;
import org.nuiton.eugene.models.object.ObjectModelType;

/**
 * This transformer read the original user model and generate the intermediate
 * model needed by generators.
 * 
 * Currently:
 *  - manage name collision for attributes or methods inherited from two
 *    different classes
 */
public class WikittyPurifierTransformer extends ObjectModelTransformer<ObjectModel> implements WikittyTagValue {

    private static final Log log =
            LogFactory.getLog(WikittyPurifierTransformer.class);

    /** for a given class, store all the names used by this class and subClasses */
    protected Map<ObjectModelClass, List<String>> namesUsedByClass = new HashMap<ObjectModelClass, List<String>>();

    /** class of the original model that are already processed */
    protected List<ObjectModelClass> processedClasses = new ArrayList<ObjectModelClass>();

    /** Generated model builder. */
    protected ObjectModelBuilder builder;

    protected void processClass(ObjectModel model, ObjectModelClass clazz) {

        if (isVerbose()) {
            log.info("will purify " + clazz.getQualifiedName());
        }

        // liste des noms deja utilisé dans la hierarchie de la
        // classe courante
        List<String> attributeNames = new ArrayList<String>();

        List<ObjectModelClass> allClasses = WikittyTransformerUtil.getAllSuperClasses(model, clazz);
        allClasses.add(clazz); // also manage current class (last one)

        for (ObjectModelClass superClass : allClasses) {

            // FIXME echatellier 20120106 it an eugene bug
            // super class are not same instance as model classes
            superClass = model.getClass(superClass.getQualifiedName());

            for (ObjectModelAttribute attribute : superClass.getAttributes()) {
                
                if (attribute.isNavigable()) {
                    String attributeName;

                    // tag value définie par le developpeur
                    if (attribute.hasTagValue(TAG_ALTERNATIVE_NAME)) {
                        attributeName = attribute.getTagValue(TAG_ALTERNATIVE_NAME);
                    } else {
                        attributeName = attribute.getName();
                    }

                    // check conflics
                    if (attributeNames.contains(attributeName)) {
                        attributeName += "From" + superClass.getName();
                        builder.addTagValue(clazz, TAG_ALTERNATIVE_NAME
                                + "." + superClass.getName() + "." + attribute.getName(), attributeName);
                    }

                    // remember new getter name
                    attributeNames.add(attributeName);
                }
            }
        }
    }
    
    @Override
    public void transformFromModel(ObjectModel model) {
        for (ObjectModelClass clazz : model.getClasses()) {
            if (log.isDebugEnabled()) {
                log.debug("Will clone " + clazz.getQualifiedName());
            }
            ObjectModelClass clone = cloneClass(clazz, true);
            if (WikittyTransformerUtil.isBusinessEntity(clone)) {
                processClass(model, clone);
            }
        }
    }

    @Override
    protected Template<ObjectModel> initOutputTemplate() {
        return new ObjectModelGenerator();
    }

    /*
     * @see org.nuiton.eugene.models.object.ObjectModelTransformer#debugOutputModel()
     */
    @Override
    protected void debugOutputModel() {

    }

    /*
     * @see org.nuiton.eugene.Transformer#initOutputModel()
     */
    @Override
    protected ObjectModel initOutputModel() {
        builder = new ObjectModelBuilder(getModel().getName());
        ObjectModel model = builder.getModel();
        return model;
    }
    
    /**
     * creates a clone of the given {@code source} class in the output model
     * and clones attributes, inheritance declarations and operations into the
     * clone
     * 
     * @param source the class to clone from the source model
     * @param cloneDocumentation flag to add documentation if some found in model
     * @return the clone of the given class
     * 
     * @deprecated echatellier 20120106 must be moved to super class : ObjectModelTransformer
     */
    @Deprecated
    public ObjectModelClass cloneClass(ObjectModelClass source,
                                       boolean cloneDocumentation) {
        ObjectModelClass outputClass =
                builder.createClass(source.getName(), source.getPackageName());
        
        cloneClassifier(source, outputClass, cloneDocumentation);

        for (ObjectModelClass superClass : source.getSuperclasses()) {
            builder.addSuperclass(outputClass, superClass.getQualifiedName());
        }

        if (!CollectionUtils.isEmpty(source.getInnerClassifiers())) {
            for (ObjectModelClassifier classifier : source.getInnerClassifiers()) {
                ObjectModelClassifier innerClassifierClone =
                        cloneClassifier(classifier, cloneDocumentation);
                builder.addInnerClassifier(outputClass,
                                   ObjectModelType.OBJECT_MODEL_CLASSIFIER,
                                   innerClassifierClone.getName());
            }
        }
        return outputClass;
    }
    
    /**
     * creates a clone of the given {@code source} classifier in the output
     * model and clones attributes, inheritance declaration and operations
     * 
     * class-specific, enumeration-specific and interface-specific features
     * of the given classifier <strong>will</strong> be present in the clone
     * 
     * @param source the classifier to clone from the source model
     * @param cloneDocumentation flag to add documentation if some found in model
     * @return the clone of the given classifier
     * 
     * @deprecated echatellier 20120106 must be moved to super class : ObjectModelTransformer
     */
    @Deprecated
    public ObjectModelClassifier cloneClassifier(ObjectModelClassifier source,
                                                 boolean cloneDocumentation) {
        ObjectModelClassifier clone = null;
        if (source.isInterface()) {
            clone = cloneInterface((ObjectModelInterface) source, cloneDocumentation);
        } else if (source.isClass()) {
            clone = cloneClass((ObjectModelClass) source, cloneDocumentation);
        } else if (source.isEnum()) {
            clone = cloneEnumeration((ObjectModelEnumeration) source, cloneDocumentation);                
        } else {
            // should never occur
            log.error("strange classifier " + source);
        }
        return clone;
    }
    
    /**
     * creates a clone of the given {@code source} interface in the output model
     * and clones attributes, inheritance declaration and operations into the
     * clone
     * 
     * @param source the interface to clone from the source model
     * @param cloneDocumentation flag to add documentation if some found in model
     * @return the clone of the given interface
     * 
     * @deprecated echatellier 20120106 must be moved to super class : ObjectModelTransformer
     */
    @Deprecated
    public ObjectModelInterface cloneInterface(ObjectModelInterface source,
                                               boolean cloneDocumentation) {
        ObjectModelInterface outputInterface =
                builder.createInterface(source.getName(), source.getPackageName());
        
        cloneClassifier(source, outputInterface, cloneDocumentation);
        // nothing more to do. copyClassifier already done the job
        
        return outputInterface;
    }

    /**
     * creates a clone of the given {@code source} enumeration in the output
     * model and clones attributes, inheritance declaration, operations and
     * literals into the clone
     * 
     * @param source the enumeration to clone from the source model
     * @param cloneDocumentation flag to add documentation if some found in model
     * @return the clone of the given enumeration
     * 
     * @deprecated echatellier 20120106 must be moved to super class : ObjectModelTransformer
     */
    @Deprecated
    public ObjectModelEnumeration cloneEnumeration(ObjectModelEnumeration source,
                                                   boolean cloneDocumentation) {
        ObjectModelEnumeration outputEnumeration =
                builder.createEnumeration(source.getName(), source.getPackageName());

        cloneClassifier(source, outputEnumeration, cloneDocumentation);
        
        for (String literal : source.getLiterals()) {
            builder.addLiteral(outputEnumeration, literal);
        }
        
        return outputEnumeration;
    }
    
    /**
     * copy attributes, interfaces declaration and operation of a given classifier
     * into another classifier.
     *
     * class-specific, enumeration-specific and interface-specific features
     * of the given classifier <strong>will not</strong> be present in the clone.
     * To copy those specific elements, use {@link #cloneClassifier(ObjectModelClassifier, boolean)}
     *
     * @param source the classifier to clone from the source model
     * @param destination where to clone the given source one
     * @param copyDocumentation flag to add documentation if some found in model
     * 
     * @deprecated echatellier 20120106 must be moved to super class : ObjectModelTransformer
     */
    @Deprecated
    protected void cloneClassifier(ObjectModelClassifier source,
                                  ObjectModelClassifier destination,
                                  boolean copyDocumentation) {

        cloneTagValues(source, destination);

        cloneStereotypes(source, destination);

        for (ObjectModelAttribute attribute : source.getAttributes()) {
            cloneAttribute(attribute, destination, copyDocumentation);
        }

        for (ObjectModelInterface interfacez : source.getInterfaces()) {
            builder.addInterface(destination, interfacez.getQualifiedName());
        }

        for (ObjectModelOperation operation : source.getOperations()) {
            cloneOperation(operation, destination, copyDocumentation);
        }
    }
    
    /**
     * Clone the {@code source} operation into the {@code destination} classifier.
     * whole signature, tagValues and body code will be cloned. You can specify
     * {@code modifiers} for the result operation.
     * 
     * @param source        operation to clone
     * @param destination   classifier where result operation will be added
     * @param cloneDocumentation flag to add documentation if some found in model
     * @param modifiers     extra modifiers
     * @return the new operation created in destination classifier
     * 
     * @deprecated echatellier 20120106 must be moved to super class : ObjectModelTransformer
     */
    @Deprecated
    public ObjectModelOperation cloneOperation(ObjectModelOperation source,
                                               ObjectModelClassifier destination,
                                               boolean cloneDocumentation,
                                               ObjectModelModifier... modifiers) {
        ObjectModelOperation outputOperation = cloneOperationSignature(
                source,
                destination,
                cloneDocumentation,
                modifiers
        );

        // add body only if operation is not abstract
        boolean opAbstract = false;
        for (ObjectModelModifier modifier : modifiers) {
            if (modifier == ObjectModelModifier.ABSTRACT) {
                opAbstract = true;
                break;
            }
        }
        if (!opAbstract) {
            builder.setOperationBody(outputOperation, source.getBodyCode());
        }
        return outputOperation;
    }
    
    /**
     * Copy all tag values for the given {@code source} to the given
     * {@code destination}.
     *
     * @param source the source element
     * @param destination the destination element
     * 
     * @deprecated echatellier 20120106 must be moved to super class : ObjectModelTransformer
     */
    @Deprecated
    protected void cloneTagValues(ObjectModelElement source,
                                  ObjectModelElement destination) {
        Map<String, String> tags = source.getTagValues();
        for (Map.Entry<String, String> entry : tags.entrySet()) {
            builder.addTagValue(destination,entry.getKey(),entry.getValue());
        }
    }

    /**
     *
     * @param source
     * @param destination
     * 
     * @deprecated echatellier 20120106 must be moved to super class : ObjectModelTransformer
     */
    @Deprecated
    protected void cloneStereotypes(ObjectModelClassifier source,
                                    ObjectModelClassifier destination) {

        Collection<String> stereotypes = source.getStereotypes();
        for (String stereotype : stereotypes) {
            builder.addStereotype(destination,stereotype);
        }
    }
    
    /**
     * Clone the {@code source} operation into the {@code destination} classifier.
     * name, returnType, parameters, exceptions and tagValues will be cloned.
     * You can specify {@code modifiers} for the result operation.
     *
     * @param source        operation to clone
     * @param destination   classifier where result operation will be added
     * @param cloneDocumentation flag to add documentation if some found in model
     * @param modifiers     extra modifiers
     * @return the new operation created in destination classifier
     * 
     * @deprecated echatellier 20120106 must be moved to super class : ObjectModelTransformer
     */
    @Deprecated
    public ObjectModelOperation cloneOperationSignature(ObjectModelOperation source,
                                                        ObjectModelClassifier destination,
                                                        boolean cloneDocumentation,
                                                        ObjectModelModifier... modifiers) {

        ObjectModelOperation outputOperation =
                builder.addOperation(destination, source.getName(), source.getReturnType(), modifiers);

        if (cloneDocumentation && GeneratorUtil.hasDocumentation(source)) {
            builder.setDocumentation(outputOperation, source.getDocumentation());
        }

        for (ObjectModelParameter parameter : source.getParameters()) {

            ObjectModelParameter outputParam =
                    builder.addParameter(outputOperation, parameter.getType(), parameter.getName());

            if (cloneDocumentation && GeneratorUtil.hasDocumentation(parameter)) {
                builder.setDocumentation(outputParam, parameter.getDocumentation());
            }
        }

        for (String exception : source.getExceptions()) {
            builder.addException(outputOperation, exception);
        }

        cloneTagValues(source, outputOperation);

        return outputOperation;
    }
    
    /**
     * clone a given attribute into a classifier of the output model
     * 
     * @param source the original attribute
     * @param destination classifier where the clone will be added 
     * @param cloneDocumentation flag to add documentation if some found in model
     * @param modifiers     extra modifiers
     * @return the clone attribute
     * 
     * @deprecated echatellier 20120106 must be moved to super class : ObjectModelTransformer
     */
    @Deprecated
    protected ObjectModelAttribute cloneAttribute(ObjectModelAttribute source,
                                                  ObjectModelClassifier destination,
                                                  boolean cloneDocumentation,
                                                  ObjectModelModifier... modifiers) {

        ObjectModelAttribute outputAttribute = builder.addAttribute(destination,
                                     source.getName(), source.getType(),
                                     source.getDefaultValue(), modifiers);

        cloneTagValues(source, outputAttribute);

        if (cloneDocumentation) {
            builder.setDocumentation(outputAttribute, source.getDocumentation());
        }

        for (String comment : source.getComments()) {
            builder.addComment(outputAttribute, comment);
        }

        builder.setMinMultiplicity(outputAttribute, source.getMinMultiplicity());
        builder.setMaxMultiplicity(outputAttribute, source.getMaxMultiplicity());
        builder.setNavigable(outputAttribute,source.isNavigable());

        return outputAttribute;
    }
}
