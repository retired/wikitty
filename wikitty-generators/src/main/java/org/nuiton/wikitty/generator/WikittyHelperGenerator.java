/*
 * #%L
 * Wikitty :: generators
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.generator;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/*{generator option: writeString = }*/
/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * Wikitty helper generator.
 * 
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.wikitty.generator.WikittyHelperGenerator"
 */
public class WikittyHelperGenerator extends ObjectModelTransformerToJava implements WikittyTagValue {

    protected static final String META_EXTENSION_SEPARATOR = ":";
    
    @Override
    public void transformFromClass(ObjectModelClass clazz) {

            if (WikittyTransformerUtil.isBusinessEntity(clazz) || WikittyTransformerUtil.isMetaExtension(clazz)) {

        ObjectModelClass helper = createClass(WikittyTransformerUtil.businessEntityToHelperName(clazz),
                                              clazz.getPackageName());
        
        // TODO 20100811 bleny remove unused imports
        addImport(helper, WikittyTransformerUtil.BUSINESS_ENTITY_CLASS_FQN);
        addImport(helper, WikittyTransformerUtil.BUSINESS_ENTITY_WIKITTY_CLASS_FQN);
        addImport(helper, WikittyTransformerUtil.WIKITTY_CLASS_FQN);
        addImport(helper, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyExtension");
        addImport(helper, "org.nuiton.wikitty.WikittyUtil");
        addImport(helper, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyUser");
        addImport(helper, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyUserAbstract");
        addImport(helper, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyUserImpl");
        addImport(helper, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyTreeNode");
        addImport(helper, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyTreeNodeAbstract");
        addImport(helper, WikittyTransformerUtil.ENTITY_PACKAGE + ".WikittyTreeNodeImpl");
        addImport(helper, List.class);
        addImport(helper, ArrayList.class);
        addImport(helper, Collection.class);
        addImport(helper, Collections.class);
        addImport(helper, Set.class);
        addImport(helper, Date.class);
        addImport(helper, LinkedHashSet.class);
        
        // making constructor for helper class (empty and private)
        ObjectModelOperation constructor = addConstructor(helper, ObjectModelModifier.PRIVATE);
        setDocumentation(constructor, "utility class all provided methods are accessible the static way");
        setOperationBody(constructor, "// empty\n"); // empty implementation

        if ( WikittyTransformerUtil.isBusinessEntity(clazz) ) {
            createOperationsForBusinessEntity(clazz, helper);
        }

        if ( WikittyTransformerUtil.isMetaExtension(clazz)) {
            createOperationForMetaExtension(clazz, helper);
        }

        }
    }

    /**
     * Add operation if input model element has stereotype "entity".
     *
     * @param entity
     * @param helper
     * */
    protected void createOperationsForBusinessEntity(ObjectModelClass entity,
                                                     ObjectModelClass helper) {

        String extensionVariableName = WikittyTransformerUtil.classToExtensionVariableName(entity, true);
        
        // generating operations with bodies
        for (ObjectModelAttribute attribute : entity.getAttributes()) {
            if (attribute.isNavigable()) {
                // needed below, in templates
                String fieldVariableName = WikittyTransformerUtil.attributeToFielVariableName(attribute, true);
                String attributeType = WikittyTransformerUtil.generateResultType(attribute, false);
                String attributeObjectType = WikittyTransformerUtil.generateResultObjectType(attribute, false);

                String attributeName = attribute.getName();
                if (attribute.hasTagValue(TAG_ALTERNATIVE_NAME)) {
                    // there is a conflict, purifier transformer give as the right name to use
                    attributeName = attribute.getTagValue(TAG_ALTERNATIVE_NAME);
                }

                if (WikittyTransformerUtil.isAttributeCollection(attribute)) {
                    // attributed is a collection, we will generate operations get, add, remove and clear

                    String attributeTypeCollectionStrict =
                            WikittyTransformerUtil.generateResultType(attribute, true);
                    String attributeTypeCollectionGeneric =
                            "Collection<" + attributeType + ">";
                    String attributeTypeVarargs = attributeType + "...";

                    String attributeObjectTypeCollectionStrict =
                            WikittyTransformerUtil.generateResultObjectType(attribute, true);
                    String attributeObjectTypeCollectionGeneric =
                            "Collection<" + attributeObjectType + ">";
                    String attributeObjectTypeVarargs = attributeObjectType + "...";

                    String getFieldMethodName =
                            WikittyTransformerUtil.generateGetFieldAsCall(attribute);
                    String attributeNameCapitalized =
                            StringUtils.capitalize(attributeName);

                    // now, for this attribute, we will generate add, remove and clear methods
                    // adding operations to contract
                    //
                    // GETTER
                    //
                    String getterName = "get" + attributeNameCapitalized;
                    ObjectModelOperation getter = addOperation(
                            helper, getterName, attributeTypeCollectionStrict,
                            ObjectModelModifier.STATIC);
                    addParameter(getter, "Wikitty", "wikitty");
                    String getterBody = ""
/*{
        <%=attributeTypeCollectionStrict%> result = wikitty.<%=getFieldMethodName%>(<%=extensionVariableName%>, <%=fieldVariableName%>, <%=attributeType%>.class);
        return result;
}*/;
                    setOperationBody(getter, getterBody);

                    //
                    // SETTER
                    //
                    String setterName = "set" + attributeNameCapitalized;
                    ObjectModelOperation setter = addOperation(
                            helper, setterName, "void", ObjectModelModifier.STATIC);
                    addParameter(setter, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                    addParameter(setter, attributeTypeCollectionStrict, attributeName);
                    String setterBody = ""
/*{
        wikitty.setField(<%=extensionVariableName%>, <%=fieldVariableName%>, <%=attributeName%>);
}*/;
                    setOperationBody(setter, setterBody);

                    //
                    // ADDALL
                    //
                    String addAllName = "addAll" + attributeNameCapitalized;
                    ObjectModelOperation addAll = addOperation(
                            helper, addAllName, "void", ObjectModelModifier.STATIC);
                    addParameter(addAll, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                    addParameter(addAll, attributeTypeCollectionGeneric, attributeName);
                    String addAllBody = ""
/*{
        if(<%=attributeName%> != null){
            for (<%=attributeType%> v:<%=attributeName%>){
                add<%=attributeNameCapitalized%>(wikitty, v);
            }
        }
}*/;
                    setOperationBody(addAll, addAllBody);

                    //
                    // ADD
                    //
                    String addName = "add" + attributeNameCapitalized;
                    ObjectModelOperation adder = addOperation(helper, addName, "void", ObjectModelModifier.STATIC);
                    addParameter(adder, "Wikitty", "wikitty");
                    addParameter(adder, attributeTypeVarargs, "element");
                    String adderBody = ""
/*{
        for (<%=attributeType%> v : element) {
            wikitty.addToField(<%=extensionVariableName%>, <%=fieldVariableName%>, v);
        }
}*/;
                    setOperationBody(adder, adderBody);

                    //
                    // REMOVE
                    //
                    String removeName = "remove" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation remover = addOperation(helper, removeName, "void", ObjectModelModifier.STATIC);
                    addParameter(remover, "Wikitty", "wikitty");
                    addParameter(remover, attributeTypeVarargs, "element");
                    String removerBody = ""
/*{
        for (<%=attributeType%> v : element) {
            wikitty.removeFromField(<%=extensionVariableName%>, <%=fieldVariableName%>, v);
        }
}*/;
                    setOperationBody(remover, removerBody);
                    
                    //
                    // CLEAR
                    //
                    String clearName = "clear" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation clear = addOperation(helper, clearName, "void", ObjectModelModifier.STATIC);
                    addParameter(clear, "Wikitty", "wikitty");
                    String clearBody = ""
/*{
        wikitty.clearField(<%=extensionVariableName%>, <%=fieldVariableName%>);
}*/;
                    setOperationBody(clear, clearBody);

                    //
                    // Pour avoir acces en mode Objet entity
                    //
                    if (attributeObjectType != null) {
                        // C'est un BusinessEntity on ajoute les getter et setter specifique
                        String collectionType = WikittyTransformerUtil.getCollectionTypeName(attribute);

                        //
                        // GETTER
                        //
                        String getterObjectName = "get" + attributeNameCapitalized;
                        ObjectModelOperation getterObject = addOperation(
                                helper, getterObjectName, attributeObjectTypeCollectionStrict,
                                ObjectModelModifier.STATIC);
                        addParameter(getterObject, "Wikitty", "wikitty");
                        addParameter(getterObject, "boolean", "exceptionIfNotLoaded");
                        String getterObjectBody = "";
                        if ("wikitty".equalsIgnoreCase(attributeObjectType)) {
                            getterObjectBody = ""
/*{
        <%=attributeObjectTypeCollectionStrict%> result = wikitty.getFieldAsWikitty<%=collectionType%>(<%=extensionVariableName%>, <%=fieldVariableName%>, exceptionIfNotLoaded);
        return result;
}*/;
                        } else {
                            getterObjectBody = ""
/*{
        <%=attributeObjectTypeCollectionStrict%> result = WikittyUtil.newInstance(<%=attributeObjectType%>.class, wikitty.getFieldAsWikitty<%=collectionType%>(<%=extensionVariableName%>, <%=fieldVariableName%>, exceptionIfNotLoaded));
        return result;
}*/;
                        }
                        setOperationBody(getterObject, getterObjectBody);

                        //
                        // SETTER
                        //
                        String setterObjectName = "set" + attributeNameCapitalized + "Entity";
                        ObjectModelOperation setterObject = addOperation(
                                helper, setterObjectName, "void", ObjectModelModifier.STATIC);
                        addParameter(setterObject, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                        addParameter(setterObject, attributeObjectTypeCollectionGeneric, attributeName);
                        String setterObjectBody = ""
/*{
        wikitty.setField(<%=extensionVariableName%>, <%=fieldVariableName%>, <%=attributeName%>);
}*/;
                    setOperationBody(setterObject, setterObjectBody);

                    //
                    // ADDALL
                    //
                    String addAllObjectName = "addAll" + attributeNameCapitalized + "Entity";
                    ObjectModelOperation addAllObject = addOperation(
                            helper, addAllObjectName, "void", ObjectModelModifier.STATIC);
                    addParameter(addAllObject, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                    addParameter(addAllObject, attributeObjectTypeCollectionGeneric, attributeName);
                    String addAllObjectBody = ""
/*{
        if(<%=attributeName%> != null){
            for (<%=attributeObjectType%> v:<%=attributeName%>){
                add<%=attributeNameCapitalized%>(wikitty, v);
            }
        }
}*/;
                    setOperationBody(addAllObject, addAllObjectBody);

                    //
                    // ADD
                    //
                    String addNameObject = "add" + attributeNameCapitalized;
                    ObjectModelOperation adderObject = addOperation(helper, addNameObject, "void", ObjectModelModifier.STATIC);
                    addParameter(adderObject, "Wikitty", "wikitty");
                    addParameter(adderObject, attributeObjectTypeVarargs, "element");
                    String adderObjectBody = ""
/*{
        for (<%=attributeObjectType%> v : element) {
            wikitty.addToField(<%=extensionVariableName%>, <%=fieldVariableName%>, v);
        }
}*/;
                    setOperationBody(adderObject, adderObjectBody);

                    //
                    // REMOVE
                    //
                    String removeNameObject = "remove" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation removerObject = addOperation(helper, removeNameObject, "void", ObjectModelModifier.STATIC);
                    addParameter(removerObject, "Wikitty", "wikitty");
                    addParameter(removerObject, attributeObjectTypeVarargs, "element");
                    String removerObjectBody = ""
/*{
        for (<%=attributeObjectType%> v : element) {
            wikitty.removeFromField(<%=extensionVariableName%>, <%=fieldVariableName%>, v);
        }
}*/;
                    setOperationBody(removerObject, removerObjectBody);

                    }

                } else {
                    String getFieldMethodName = WikittyTransformerUtil.generateGetFieldAsCall(attribute);
                    
                    // adding getter and setter to contract
                    //
                    // GETTER
                    //
                    String getterName = "get" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation getter = addOperation(helper, getterName, attributeType, ObjectModelModifier.STATIC);
                    addParameter(getter, "Wikitty", "wikitty");
                    setOperationBody(getter, ""
/*{
        <%=attributeType%> value = wikitty.<%=getFieldMethodName%>(<%=extensionVariableName%>, <%=fieldVariableName%>);
        return value;
}*/);
                    
                    //
                    // SETTER
                    //
                    String setterName = "set" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation setter = addOperation(helper, setterName, attributeType, ObjectModelModifier.STATIC);
                    addParameter(setter, "Wikitty", "wikitty");
                    addParameter(setter, attributeType, attributeName);
                    setOperationBody(setter, ""
/*{
        <%=attributeType%> oldValue = <%=getter.getName()%>(wikitty);
        wikitty.setField(<%=extensionVariableName%>, <%=fieldVariableName%>, <%=attributeName%>);
        return oldValue;
}*/);

                    //
                    // Pour avoir acces en mode Objet entity
                    //
                    if (attributeObjectType != null) {

                        //
                        // GETTER
                        //
                        String getterObjectName = "get" + StringUtils.capitalize(attributeName);
                        ObjectModelOperation getterObject = addOperation(helper,
                                getterObjectName, attributeObjectType, ObjectModelModifier.STATIC);
                        addParameter(getterObject, "Wikitty", "wikitty");
                        addParameter(getterObject, "boolean", "exceptionIfNotLoaded");
                        String getterObjectBody = "";
                        if ("wikitty".equalsIgnoreCase(attributeObjectType)) {
                            getterObjectBody = ""
/*{
        <%=attributeObjectType%> value = wikitty.getFieldAsWikitty(<%=extensionVariableName%>, <%=fieldVariableName%>, exceptionIfNotLoaded);
        return value;
}*/;
                        } else {
                            getterObjectBody = ""
/*{
        <%=attributeObjectType%> value = WikittyUtil.newInstance(<%=attributeObjectType%>.class, wikitty.getFieldAsWikitty(<%=extensionVariableName%>, <%=fieldVariableName%>, exceptionIfNotLoaded));
        return value;
}*/;
                        }
                    setOperationBody(getterObject, getterObjectBody);

                    //
                    // SETTER
                    //
                    String setterObjectName = "set" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation setterObject = addOperation(helper, setterObjectName, attributeObjectType, ObjectModelModifier.STATIC);
                    addParameter(setterObject, "Wikitty", "wikitty");
                    addParameter(setterObject, attributeObjectType, attributeName);
                    setOperationBody(setterObject, ""
/*{
        <%=attributeObjectType%> oldValue = <%=getter.getName()%>(wikitty, false);
        wikitty.setField(<%=extensionVariableName%>, <%=fieldVariableName%>, <%=attributeName%>);
        return oldValue;
}*/);

                    }

                }
            }
        }
        
        
        
        // now, adding the equals(w1, w2)

        ObjectModelOperation equals = addOperation(helper, "equals", "boolean", ObjectModelModifier.STATIC);
        addParameter(equals, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "w1");
        addParameter(equals, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "w2");
        
        // the body of the equals method, will be assembled while reading attributes
        String equalsBody = ""
/*{
    boolean result = true;
}*/;

        for(ObjectModelAttribute attribute : entity.getAttributes()) {
            if (attribute.isNavigable()) {
                // two variables needed below
                String fieldVariableName = WikittyTransformerUtil.attributeToFielVariableName(attribute, true);

                // considering field in equals body
                equalsBody += ""
/*{
        if (result) {
            Object f1 = w1.getFieldAsObject(<%= extensionVariableName %>, <%= fieldVariableName %>);
            Object f2 = w2.getFieldAsObject(<%= extensionVariableName %>, <%= fieldVariableName %>);
            result = f1 == f2 || (f1 != null && f1.equals(f2));
        };
}*/;
            }
        }

        // finishing equals body
        equalsBody += ""
/*{
    return result;
}*/;
        setOperationBody(equals, equalsBody);

        // finally, adding isExtension, hasExtension and addExtension
        
        ObjectModelOperation isExtension = addOperation(helper, "isExtension", "boolean", ObjectModelModifier.STATIC);
        addParameter(isExtension, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
        addAnnotation(helper, isExtension, "Deprecated");
        setDocumentation(isExtension, "@deprecated renamed to keep consistency, use hasExtension instead");
        setOperationBody(isExtension, ""
/*{
        return hasExtension(wikitty);
}*/);

        ObjectModelOperation hasExtension = addOperation(helper, "hasExtension", "boolean", ObjectModelModifier.STATIC);
        addParameter(hasExtension, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
        setDocumentation(hasExtension, "check if wikitty has current extension");
        setOperationBody(hasExtension, ""
/*{
        return wikitty.hasExtension(<%=extensionVariableName%>);
}*/);
        
        ObjectModelOperation addExtension = addOperation(helper, "addExtension", "void", ObjectModelModifier.STATIC);
        addParameter(addExtension, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
        setDocumentation(addExtension, "ajout les extensions static de cette classe au wikitty en argument");
        String contractName = WikittyTransformerUtil.businessEntityToAbstractName(entity);
        setOperationBody(addExtension, ""
/*{
        for (WikittyExtension ext : <%=contractName%>.extensions) {
            wikitty.addExtension(ext);
        }
}*/);


        ObjectModelOperation removeExtension = addOperation(helper, "removeExtension", "void", ObjectModelModifier.STATIC);
        addParameter(removeExtension, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
        setDocumentation(removeExtension, "supprime les extensions de cette classe au wikitty en argument");
        setOperationBody(removeExtension, ""
/*{
        for (WikittyExtension ext : <%=contractName%>.extensions) {
            wikitty.removeExtension(ext.getName());
        }
}*/);

        ObjectModelOperation toString = addOperation(helper, "toString", "String", ObjectModelModifier.STATIC);
        addParameter(toString, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
        setDocumentation(toString,
                "Return toString representation. Use tagValue '"
                + TAG_TO_STRING
                + "' format, if exist, else standard toString is call");
            setOperationBody(toString, ""
/*{

        return wikitty.toString(<%=extensionVariableName%>);
}*/);
    }

    /**
     * Add needed operations if input model element has stereotype "meta".
     *
     * @param metaExtension
     * @param helper
     */
    protected void createOperationForMetaExtension(ObjectModelClass metaExtension,
                                                   ObjectModelClass helper) {
        
        String abstractName = WikittyTransformerUtil.businessEntityToAbstractName(metaExtension);
        String contractName = WikittyTransformerUtil.businessEntityToContractName(metaExtension);
        String extensionVariableName = WikittyTransformerUtil.classToExtensionVariableName(metaExtension, true);


        ObjectModelOperation addMetaExtension = addOperation(helper, "addMetaExtension", "void", ObjectModelModifier.STATIC);
        addParameter(addMetaExtension, WikittyTransformerUtil.WIKITTY_EXTENSION_CLASS_FQN, "extension");
        addParameter(addMetaExtension, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
        setDocumentation(addMetaExtension, String.format(
                "add %s meta-extension on given extension to the given wikitty",
                metaExtension.getName()));
        setOperationBody(addMetaExtension, ""
/*{
        wikitty.addMetaExtension(<%=abstractName%>.extension<%=contractName%>, extension);
}*/);


        ObjectModelOperation hasMetaExtension = addOperation(helper, "hasMetaExtension", "boolean", ObjectModelModifier.STATIC);
        addParameter(hasMetaExtension, WikittyTransformerUtil.WIKITTY_EXTENSION_CLASS_FQN, "extension");
        addParameter(hasMetaExtension, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
        setDocumentation(hasMetaExtension, String.format(
                "true if given wikitty has %s meta-extension on given extension",
                metaExtension.getName()));
        setOperationBody(hasMetaExtension, ""
/*{
        return wikitty.hasMetaExtension(<%=extensionVariableName%>, extension.getName());
}*/);     
        

        // now adding getMetaFielName, both implementations
        
        ObjectModelOperation getMetaFieldNameFromExtension = addOperation(helper, "getMetaFieldName", "String", ObjectModelModifier.STATIC);
        addParameter(getMetaFieldNameFromExtension, WikittyTransformerUtil.WIKITTY_EXTENSION_CLASS_FQN, "extension");
        addParameter(getMetaFieldNameFromExtension, "String", "fieldName");
        setDocumentation(getMetaFieldNameFromExtension, String.format(
                "for extension 'Ext' and field 'f', return 'Ext:%s.f'",
                metaExtension.getName()));
        setOperationBody(getMetaFieldNameFromExtension, ""
/*{
        String metaFieldName = getMetaFieldName(extension.getName(), fieldName); 
        return metaFieldName;
}*/);
        
        ObjectModelOperation getMetaFieldNameFromExtensionName = addOperation(helper, "getMetaFieldName", "String", ObjectModelModifier.STATIC);
        addParameter(getMetaFieldNameFromExtensionName, "String", "extensionName");
        addParameter(getMetaFieldNameFromExtensionName, "String", "fieldName");
        setDocumentation(getMetaFieldNameFromExtensionName, String.format(
                "for extension 'Ext' and field 'f', return 'Ext:%s.f'",
                metaExtension.getName()));
        setOperationBody(getMetaFieldNameFromExtensionName, ""
/*{
        String metaFieldName = WikittyUtil.getMetaFieldName("<%=metaExtension.getName()%>", extensionName, fieldName);
        return metaFieldName;
}*/);



        // now, adding all
        
        // generating operations with bodies
        for (ObjectModelAttribute attribute : metaExtension.getAttributes()) {
            if (attribute.isNavigable()) {
                // needed below, in templates
                String fieldVariableName = WikittyTransformerUtil.attributeToFielVariableName(attribute, true);
                String attributeType = WikittyTransformerUtil.generateResultType(attribute, false);
                String attributeObjectType = WikittyTransformerUtil.generateResultObjectType(attribute, false);

                String attributeName = attribute.getName();
                if (attribute.hasTagValue(TAG_ALTERNATIVE_NAME)) {
                    // there is a conflict, purifier transformer give as the right name to use
                    attributeName = attribute.getTagValue(TAG_ALTERNATIVE_NAME);
                }

                if (WikittyTransformerUtil.isAttributeCollection(attribute)) {
                    // attributed is a collection, we will generate operations get, add, remove and clear

                    String attributeTypeCollectionStrict =
                            WikittyTransformerUtil.generateResultType(attribute, true);
                    String attributeTypeCollectionGeneric =
                            "Collection<" + attributeType + ">";
                    String attributeTypeVarargs = attributeType + "...";

                    String attributeObjectTypeCollectionStrict =
                            WikittyTransformerUtil.generateResultObjectType(attribute, true);
                    String attributeObjectTypeCollectionGeneric =
                            "Collection<" + attributeObjectType + ">";
                    String attributeObjectTypeVarargs = attributeObjectType + "...";

                    String getFieldMethodName = WikittyTransformerUtil.generateGetFieldAsCall(attribute);
                    String capitalizedAttributeName = StringUtils.capitalize(attributeName);

                    // now, for this attribute, we will generate add, remove and clear methods
                    // adding operations to contract
                    String getterName = "get" + capitalizedAttributeName;
                    ObjectModelOperation getter = addOperation(
                            helper, getterName, attributeTypeCollectionStrict,
                            ObjectModelModifier.STATIC);
                    addParameter(getter, "String", "extensionName");
                    addParameter(getter, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                    String getterBody = ""
/*{
        String fieldName = getMetaFieldName(extensionName, "<%=attributeName%>");
        <%=attributeTypeCollectionStrict%> result = (<%=attributeTypeCollectionStrict%>) wikitty.getFqField(fieldName);
        return result;
}*/;
                    setOperationBody(getter, getterBody);

                    String setterName = "set" + capitalizedAttributeName;
                    ObjectModelOperation setter = addOperation(
                            helper, setterName, "void", ObjectModelModifier.STATIC);
                    addParameter(setter, "String", "extensionName");
                    addParameter(setter, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                    addParameter(setter, attributeTypeCollectionStrict, attributeName);
                    String setterBody = ""
/*{
        clear<%=capitalizedAttributeName%>(extensionName, wikitty);
        addAll<%=capitalizedAttributeName%>(extensionName, wikitty, <%=attributeName%>);
}*/;
                    setOperationBody(setter, setterBody);

                    String addAllName = "addAll" + capitalizedAttributeName;
                    ObjectModelOperation addAll = addOperation(
                            helper, addAllName, "void", ObjectModelModifier.STATIC);
                    addParameter(addAll, "String", "extensionName");
                    addParameter(addAll, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                    addParameter(addAll, attributeTypeCollectionGeneric, attributeName);
                    String addAllBody = ""
/*{
        if(<%=attributeName%> != null){
            for (<%=attributeType%> id:<%=attributeName%>){
                add<%=capitalizedAttributeName%>(extensionName, wikitty, id);
            }
        }
}*/;
                    setOperationBody(addAll, addAllBody);

                    String addName = "add" + capitalizedAttributeName;
                    ObjectModelOperation adder = addOperation(
                            helper, addName, "void", ObjectModelModifier.STATIC);
                    addParameter(adder, "String", "extensionName");
                    addParameter(adder, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                    addParameter(adder, attributeTypeVarargs, "element");
                    String adderBody = ""
/*{
        for (<%=attributeType%> v : element) {
            String fieldName = getMetaFieldName(extensionName, "<%=attributeName%>");
            wikitty.addToField(fieldName, v);
        }
}*/;
                    setOperationBody(adder, adderBody);

                    String removeName = "remove" + capitalizedAttributeName;
                    ObjectModelOperation remover = addOperation(helper, removeName, "void", ObjectModelModifier.STATIC);
                    addParameter(remover, "String", "extensionName");
                    addParameter(remover, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                    addParameter(remover, attributeTypeVarargs, "element");
                    String removerBody = ""
/*{
        for (<%=attributeType%> v : element) {
            String fieldName = getMetaFieldName(extensionName, "<%=attributeName%>");
            wikitty.removeFromField(fieldName, v);
        }
}*/;
                    setOperationBody(remover, removerBody);

                    String clearName = "clear" + capitalizedAttributeName;
                    ObjectModelOperation clear = addOperation(helper, clearName, "void", ObjectModelModifier.STATIC);
                    addParameter(clear, "String", "extensionName");
                    addParameter(clear, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                    String clearBody = ""
/*{
        String fieldName = getMetaFieldName(extensionName, "<%=attributeName%>");
        wikitty.clearField(fieldName);
}*/;
                    setOperationBody(clear, clearBody);

                    //
                    // Pour avoir acces en mode Objet entity
                    //
                    if (attributeObjectType != null) {
                        // C'est un BusinessEntity on ajoute les getter et setter specifique
                        String collectionType = WikittyTransformerUtil.getCollectionTypeName(attribute);

                        String getterNameObject = "get" + capitalizedAttributeName;
                        ObjectModelOperation getterObject = addOperation(
                                helper, getterNameObject, attributeObjectTypeCollectionStrict,
                                ObjectModelModifier.STATIC);
                        addParameter(getterObject, "String", "extensionName");
                        addParameter(getterObject, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                        addParameter(getterObject, "boolean", "exceptionIfNotLoaded");
                        String getterObjectBody = "";
                        if ("wikitty".equalsIgnoreCase(attributeObjectType)) {
                            getterObjectBody = ""
/*{
        String fqfieldName = getMetaFieldName(extensionName, "<%=attributeName%>");
        String extName = WikittyExtension.extractExtensionName(fqfieldName);
        String fieldName = WikittyExtension.extractFieldName(fqfieldName);
        <%=attributeObjectTypeCollectionStrict%> result = wikitty.getFieldAsWikitty<%=collectionType%>(extName, fieldName, exceptionIfNotLoaded);
        return result;
}*/;
                        } else {
                            getterObjectBody = ""
/*{
        String fqfieldName = getMetaFieldName(extensionName, "<%=attributeName%>");
        String extName = WikittyExtension.extractExtensionName(fqfieldName);
        String fieldName = WikittyExtension.extractFieldName(fqfieldName);
        <%=attributeObjectTypeCollectionStrict%> result = WikittyUtil.newInstance(<%=attributeObjectType%>.class, wikitty.getFieldAsWikitty<%=collectionType%>(extName, fieldName, exceptionIfNotLoaded));
        return result;
}*/;
                        }
                        setOperationBody(getterObject, getterObjectBody);

                        String setterNameObject = "set" + capitalizedAttributeName + "Entity";
                        ObjectModelOperation setterObject = addOperation(
                                helper, setterNameObject, "void", ObjectModelModifier.STATIC);
                        addParameter(setterObject, "String", "extensionName");
                        addParameter(setterObject, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                        addParameter(setterObject, attributeObjectTypeCollectionGeneric, attributeName);
                        String setterObjectBody = ""
/*{
        clear<%=capitalizedAttributeName%>(extensionName, wikitty);
        addAll<%=capitalizedAttributeName%>Entity(extensionName, wikitty, <%=attributeName%>);
}*/;
                        setOperationBody(setterObject, setterObjectBody);

                        String addAllNameObject = "addAll" + capitalizedAttributeName + "Entity";
                        ObjectModelOperation addAllObject = addOperation(
                                helper, addAllNameObject, "void", ObjectModelModifier.STATIC);
                        addParameter(addAllObject, "String", "extensionName");
                        addParameter(addAllObject, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                        addParameter(addAllObject, attributeObjectTypeCollectionGeneric, attributeName);
                        String addAllObjectBody = ""
/*{
        if(<%=attributeName%> != null){
            for (<%=attributeObjectType%> e:<%=attributeName%>){
                add<%=capitalizedAttributeName%>(extensionName, wikitty, e);
            }
        }
}*/;
                        setOperationBody(addAllObject, addAllObjectBody);

                        String addNameObject = "add" + capitalizedAttributeName;
                        ObjectModelOperation adderObject = addOperation(
                                helper, addNameObject, "void", ObjectModelModifier.STATIC);
                        addParameter(adderObject, "String", "extensionName");
                        addParameter(adderObject, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                        addParameter(adderObject, attributeObjectTypeVarargs, "element");
                        String adderObjectBody = ""
/*{
        for (<%=attributeObjectType%> v : element) {
            String fieldName = getMetaFieldName(extensionName, "<%=attributeName%>");
            wikitty.addToField(fieldName, v);
        }
}*/;
                        setOperationBody(adderObject, adderObjectBody);

                        String removeNameObject = "remove" + capitalizedAttributeName;
                        ObjectModelOperation removerObject = addOperation(
                                helper, removeNameObject, "void", ObjectModelModifier.STATIC);
                        addParameter(removerObject, "String", "extensionName");
                        addParameter(removerObject, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                        addParameter(removerObject, attributeObjectTypeVarargs, "element");
                        String removerObjectBody = ""
/*{
        for (<%=attributeObjectType%> v : element) {
            String fieldName = getMetaFieldName(extensionName, "<%=attributeName%>");
            wikitty.removeFromField(fieldName, v);
        }
}*/;
                        setOperationBody(removerObject, removerObjectBody);
                    }

                    
                } else {
                    String getFieldMethodName = WikittyTransformerUtil.generateGetFieldAsCall(attribute);
                    
                    // adding getter and setter to contract
                    String getterName = "get" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation getter = addOperation(helper, getterName, attributeType, ObjectModelModifier.STATIC);
                    addParameter(getter, "String", "extensionName");
                    addParameter(getter, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                    setOperationBody(getter, ""
/*{
        String fieldName = getMetaFieldName(extensionName, "<%=attributeName%>");
        <%=attributeType%> value = (<%=attributeType%>) wikitty.getFqField(fieldName);        
        return value;
}*/);
                    
                    String setterName = "set" + StringUtils.capitalize(attributeName);
                    ObjectModelOperation setter = addOperation(helper, setterName, attributeType, ObjectModelModifier.STATIC);
                    addParameter(setter, "String", "extensionName");
                    addParameter(setter, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                    addParameter(setter, attributeType, attributeName);
                    setOperationBody(setter, ""
/*{
        <%=attributeType%> oldValue = <%=getter.getName()%>(extensionName, wikitty);
        String fieldName = getMetaFieldName(extensionName, "<%=attributeName%>");
        wikitty.setFqField(fieldName, <%=attributeName%>);
        return oldValue;
}*/);
                                        //
                    // Pour avoir acces en mode Objet entity
                    //
                    if (attributeObjectType != null) {
                        // C'est un BusinessEntity on ajoute les getter et setter specifique

                        String getterNameObject = "get" + StringUtils.capitalize(attributeName);
                        ObjectModelOperation getterObject = addOperation(
                                helper, getterNameObject, attributeObjectType, ObjectModelModifier.STATIC);
                        addParameter(getterObject, "String", "extensionName");
                        addParameter(getterObject, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                        addParameter(getterObject, "boolean", "exceptionIfNotLoaded");
                        String getterObjectBody = "";
                        if ("wikitty".equalsIgnoreCase(attributeObjectType)) {
                            getterObjectBody = ""
/*{
        String fqfieldName = getMetaFieldName(extensionName, "<%=attributeName%>");
        String extName = WikittyExtension.extractExtensionName(fqfieldName);
        String fieldName = WikittyExtension.extractFieldName(fqfieldName);
        <%=attributeObjectType%> value = wikitty.getFieldAsWikitty(extName, fieldName, exceptionIfNotLoaded);
        return value;
}*/;
                        } else {
                            getterObjectBody = ""
/*{
        String fqfieldName = getMetaFieldName(extensionName, "<%=attributeName%>");
        String extName = WikittyExtension.extractExtensionName(fqfieldName);
        String fieldName = WikittyExtension.extractFieldName(fqfieldName);
        <%=attributeObjectType%> value = WikittyUtil.newInstance(<%=attributeObjectType%>.class, wikitty.getFieldAsWikitty(extName, fieldName, exceptionIfNotLoaded));
        return value;
}*/;
                        }
                        setOperationBody(getterObject, getterObjectBody);

                        String setterNameObject = "set" + StringUtils.capitalize(attributeName);
                        ObjectModelOperation setterObject = addOperation(
                                helper, setterNameObject, attributeObjectType, ObjectModelModifier.STATIC);
                        addParameter(setterObject, "String", "extensionName");
                        addParameter(setterObject, WikittyTransformerUtil.WIKITTY_CLASS_FQN, "wikitty");
                        addParameter(setterObject, attributeObjectType, attributeName);
                        setOperationBody(setterObject, ""
/*{
        <%=attributeObjectType%> oldValue = <%=getterObject.getName()%>(extensionName, wikitty, false);
        String fieldName = getMetaFieldName(extensionName, "<%=attributeName%>");
        wikitty.setFqField(fieldName, <%=attributeName%>);
        return oldValue;
}*/);
                    }
                }
            }
        }
    }
}
