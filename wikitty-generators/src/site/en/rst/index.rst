.. -
.. * #%L
.. * Wikitty :: generators
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Wikitty Generators
==================

This module provides generator templates for Wikitty. The code generator used is
EUGene_ . This generator allow you to modelise the business using different UML
modellers, such as ArgoUML or TopCased.

.. _EUGene:http://eugene.nuiton.org
