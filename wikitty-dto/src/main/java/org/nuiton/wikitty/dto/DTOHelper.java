/*
 * #%L
 * Wikitty :: dto
 * %%
 * Copyright (C) 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.dto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.ObjectUtil;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.BusinessEntity;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyImpl;

/**
 * User: couteau
 * Date: 19 nov. 2010
 */
public class DTOHelper {

    static Log log = LogFactory.getLog(DTOHelper.class);

    public static <E extends BusinessEntity> E toDto(E w) {
        E result = null;

        if (w != null){
            Class clazz = w.getClass();
            String dtoClassName = clazz.getName().replace("Impl", "DTO").replace("CopyOnWrite", "DTO");
            try {
                String wikittyId = w.getWikittyId();
                result = (E)ObjectUtil.newInstance(dtoClassName+"("+wikittyId+")");

                result.copyFrom(w);
            }catch (Exception eee){
                log.error("Could not transform Wikitty to DTO", eee);
            }
        }
        return result;
    }

    public static <E extends BusinessEntity> E fromDto(WikittyClient client, Class<E> clazz, E dto) {

        E result = client.restore(clazz, dto.getWikittyId());

        if (result == null){
            try {
                String wikittyId = dto.getWikittyId();
                Wikitty wikitty = new WikittyImpl(wikittyId);
                result = WikittyUtil.newInstance(clazz, wikitty);
            } catch (Exception eee) {
                log.error("Could not transform DTO to Wikitty", eee);
            }
        }

        if (result != null) {
            result.copyFrom(dto);
        }

        return result;

    }

}
