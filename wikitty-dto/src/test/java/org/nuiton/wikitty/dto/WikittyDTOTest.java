/*
 * #%L
 * Wikitty :: dto
 * %%
 * Copyright (C) 2010 CodeLutin, Jean Couteau
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.dto;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.WikittyConfig;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.entities.WikittyLabelImpl;

public class WikittyDTOTest {

    WikittyClient client;

    @Before
    public void clear() throws Exception {

        ApplicationConfig config = WikittyConfig.getConfig();
        client = new WikittyClient(config);
    }

    //FIXME-tchemit-2014-02è07
    @Ignore
    @Test
    public void testConversions() {

        //create business entity with wikitty
        WikittyLabel label = new WikittyLabelImpl();
        label.addLabels("Test");

        //make conversion
        WikittyLabel dto = DTOHelper.toDto(label);

        //entity=>dto conversion test
        //check that dto fields are the same than the wikitty ones
        Assert.assertNotNull(dto);
        Assert.assertEquals(label.getWikittyId(),dto.getWikittyId());
        Assert.assertTrue(WikittyUtil.versionEquals(label.getWikittyVersion(),dto.getWikittyVersion()));
        Assert.assertEquals(label.getLabels(), dto.getLabels());

        //First dto=>entity conversion test, no modification
        //make conversion to wikitty
        WikittyLabel secondLabel = DTOHelper.fromDto(client, WikittyLabel.class, dto);

        //check that dto fields are the same than the wikitty ones
        Assert.assertNotNull(secondLabel);
        Assert.assertEquals(dto.getWikittyId(), secondLabel.getWikittyId());
        Assert.assertEquals(dto.getLabels(), secondLabel.getLabels());
        Assert.assertTrue(
                WikittyUtil.versionEquals(secondLabel.getWikittyVersion(),
                        dto.getWikittyVersion()));

        //Second entity=>dto conversion test, modifications on dto
        //modify dto
        dto.addLabels("toto");

        //make conversion to wikitty
        WikittyLabel thirdLabel = DTOHelper.fromDto(client,WikittyLabel.class,dto);

        //check that dto fields are the same than the wikitty ones
        Assert.assertNotNull(thirdLabel);
        Assert.assertEquals(dto.getWikittyId(),thirdLabel.getWikittyId());
        Assert.assertEquals(dto.getLabels(), thirdLabel.getLabels());
        
        //Check that version have been updated (for storing)
        Assert.assertTrue(
                WikittyUtil.versionGreaterThan(thirdLabel.getWikittyVersion(),
                                               secondLabel.getWikittyVersion()));

    }
}
