/*
 * #%L
 * Wikitty :: wikitty-solr
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.storage.solr;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.WikittyTypes;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.entities.ElementExtension;
import org.nuiton.wikitty.entities.ElementField;
import org.nuiton.wikitty.entities.ElementId;
import org.nuiton.wikitty.entities.ElementNode;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.storage.WikittyExtensionStorage;

import static org.nuiton.wikitty.storage.solr.WikittySolrConstant.*;

/**
 * Converti la valeur des objects de type {@link Element} pour etre utilisable
 * dans Solr
 * 
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ElementModifier {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ElementModifier.class);

    protected WikittyExtensionStorage extensionStorage;

    public ElementModifier(WikittyExtensionStorage extensionStorage) {
        this.extensionStorage = extensionStorage;
    }

    /**
     * Different cas d'entree
     * <ul>
     * <li> monext.monfield on recherche sur un champs, son type est retrouve dans la base</li>
     * <li> monext.monfield.NUMERIC le type n'est pas pris en compte, car le champs
     * est complement renseigne et donc on va recherche dans la base son type</li>
     * <li> *.monfield = recherche fulltext car aucun type n'est indique</li>
     * <li> *.monfield.NUMERIC recheche en utilise le type demande si on
     * final 'monfield' n'est pas de ce type alors on ne retrouvera rien car
     * on recherchera sur un champs inexistant</li>
     * <li> *.* ou * = recherche fulltext</li>
     * <li> id = recherche sur les id</li>
     * <li> extension = recherche sur les extensions</li>
     * <li> node = recherche sur les attachment d'un noeud</li>
     * <li> nodeOrSubNode = recherche sur les attachment d'un noeud ou sous noeud</li>
     * <li> root = recherche sur les noeud root</li>
     * <li> parents = recherche sur un noeud ou ses parents</li>
     * <li> #..... = un champs special force par l'utilisateur</li>
     * </ul>
     *
     * @param tx
     * @param element
     * @return
     */
    public String convertToSolr(WikittyTransaction tx, Element element) {
        if (log.isDebugEnabled()) {
            log.debug("Element: " + element.getValue() + "(" + element.getClass() + ")");
        }
        String result;
        if (element instanceof ElementId) {
            result = SOLR_ID;
        } else if (element instanceof ElementExtension) {
            result = SOLR_EXTENSIONS;
        } else if (element instanceof ElementNode) {
            if (element.equals(Element.NODE_ROOT)) {
                result = TREENODE_ROOT;
            } else if (element.equals(Element.NODE_PATH)) {
                result = TREENODE_PARENTS;
            } else if (element.equals(Element.NODE_DEPTH)) {
                result = TREENODE_DEPTH;
            } else {
                throw new WikittyException("Unknow ElementNode: " + element.getValue());
            }
        } else if (element instanceof ElementField) {
            String fieldValue = element.getValue();
            if (StringUtils.startsWith(fieldValue, WikittySolrConstant.SOLR_WIKITTY_PREFIX)) {
                // c'est un champs specifique a l'indexation, un utilisateur
                // avance a du l'utiliser directement, on lui fait confiance
                // et on le laisse comme ca
                result = fieldValue;
            } else {
                String[] searchField = fieldValue.split(WikittyUtil.FQ_FIELD_NAME_SEPARATOR_REGEX);

                if (searchField.length >= 2) {
                    String extName = searchField[0];
                    String fieldName = searchField[1];

                    if ("*".equals(extName) && "*".equals(fieldName)) {
                        result = SOLR_FULLTEXT;
                    } else if ("*".equals(extName)) {
                        result = SOLR_ALL_EXTENSIONS
                                + WikittyUtil.FQ_FIELD_NAME_SEPARATOR + fieldName;

                        String modifier = "";
                        // on recherche si le type a ete force
                        if (searchField.length >= 3) {
                            // On passe ici, si on indique dans le champs son type (ex: *.monfield.NUMERIC)
                            // utile pour force la recherche sur les bons champs lorsqu'on
                            // demande une recherche sur * == #all
                            String fieldNameType = searchField[2];
                            WikittyTypes type = WikittyTypes.valueOf(fieldNameType);
                            // Ajout du pattern solr pour discriminer le champs ex : _s, _dt, _w, ...
                            modifier = SolrUtil.getSolrFieldName("", type);
                        }

                        if (StringUtils.isEmpty(modifier)) {
                            // si on arrive ici, c'est qu'on a pas reussi a calculer le
                            // modifier cela peut arriver par exemple pour
                            // #all ou le seul moyen d'avoir le type est qu'il soit donne
                            // dans le champs, sinon on ne peut pas le trouver
                            if (log.isDebugEnabled()) {
                                log.debug("Search on multi extentions (*) without field"
                                        + " type, fallback search in fulltext");
                            }
                            result = SOLR_FULLTEXT_ALL_EXTENSIONS
                                    + WikittyUtil.FQ_FIELD_NAME_SEPARATOR + fieldName;
                        } else {
                            result += modifier;
                        }

                    } else if ("*".equals(fieldName)) {
                        // on doit recherche sur tout les champs de cette extension
                        // en fait cela revient a faire une recherche fulltext avec
                        // une 2eme condition sur le type d'extension
                        // Il ne faut donc pas le gere comme ca, on previent via
                        // une exception qui l'explique
                        throw new WikittyException(
                                "To search on all field of one extension, you must"
                                + " use two condition, one on extension type, other"
                                + " in fulltext '*.*' with the wanted constraint");
                    } else {
                        result = extName + WikittyUtil.FQ_FIELD_NAME_SEPARATOR + fieldName;

                        // Ajout du pattern solr pour discriminer le champs ex : _s, _sm, _wm ...
                        // Search type of field in extension
                        String version =
                                extensionStorage.getLastVersion(tx, extName);
                        if (version == null) {
                            // not valid extension if version == null
                            // cela arrive si le framework genere des requetes
                            // si des extensions non enregistrer en base
                            // cela est possible par exemple sur les WikittyTreeNode
                            // S'il ne sont pas en base, c'est qu'aucun objet
                            // ne les utilisent, et donc leur absence n'est pas
                            // grave
                            if (log.isDebugEnabled()) {
                                log.debug(String.format(
                                        "Can't find extension '%s'", extName));
                            }
                        } else {
                            WikittyExtension ext = extensionStorage.restore(
                                    tx, extName, version);
                            FieldType fieldType = ext.getFieldType(fieldName);
                            if (log.isDebugEnabled()) {
                                log.debug(ext.toDefinition() + " for " + fieldName);
                            }
                            if (fieldType != null) { // type can be null if extension version differ
                                WikittyTypes type = fieldType.getType();
                                result = SolrUtil.getSolrFieldName(result, type);
                            } else {
                                // on ne retrouve pas le champs, on le laisse comme il est
                                // mais normalement rien ne matchera avec
                                // TODO poussin 20111230 peut-etre transforme
                                // le champs en une recherche fulltext ???
                                // mais c peut-etre dangereux car ce n'est pas
                                // ce qu'a demande l'utilisateur
                                log.info(String.format(
                                        "Can't find field '%s' in extension '%s'"
                                        + " with definition: %s",
                                        fieldName, ext.getId(), ext.toDefinition()));
                            }
                        }
                    }
                } else if (searchField.length >= 1 && "*".equals(searchField[0])) {
                    result = SOLR_FULLTEXT;
                } else {
                    throw new WikittyException("Empty field name is unsupported");
                }
            }
        } else {
            throw new WikittyException(String.format(
                    "Unsupported Element type '%s'",
                    ClassUtils.getShortCanonicalName(element, "null")));
        }

        return result;
    }

    static public String convertToField(String solrName) {
        String fieldName = solrName.replaceAll(
                "(" + SUFFIX_SORTABLE + "$)"
                , "");
        fieldName = fieldName.replaceAll(
                "(" + SUFFIX_BINARY + "$)"
                + "|(" + SUFFIX_BOOLEAN + "$)"
                + "|(" + SUFFIX_DATE + "$)"
                + "|(" + SUFFIX_STRING + "$)"
                + "|(" + SUFFIX_WIKITTY + "$)"
                + "|(" + SUFFIX_NUMERIC + "$)"
                , "");
        if (SOLR_ID.equals(fieldName)) {
            fieldName = Element.ID.getValue();
        } else if (SOLR_EXTENSIONS.equals(fieldName)) {
            fieldName = Element.EXTENSION.getValue();
        } else if (TREENODE_ROOT.equals(fieldName)) {
            fieldName = Element.NODE_ROOT.getValue();
        } else if (TREENODE_PARENTS.equals(fieldName)) {
            fieldName = Element.NODE_PATH.getValue();
        } else if (TREENODE_DEPTH.equals(fieldName)) {
            fieldName = Element.NODE_DEPTH.getValue();
        }
        return fieldName;
    }
}
