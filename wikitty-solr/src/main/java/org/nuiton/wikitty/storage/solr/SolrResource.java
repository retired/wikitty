/*
 * #%L
 * Wikitty :: wikitty-solr
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.storage.solr;


import com.arjuna.ats.arjuna.coordinator.BasicAction;
import com.arjuna.ats.arjuna.coordinator.OnePhaseResource;
import com.arjuna.ats.arjuna.coordinator.TwoPhaseOutcome;
import com.arjuna.ats.arjuna.state.InputObjectState;
import com.arjuna.ats.arjuna.state.OutputObjectState;
import com.arjuna.ats.internal.arjuna.abstractrecords.LastResourceRecord;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.common.SolrInputDocument;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.wikitty.WikittyUtil;

/**
 * Use to plug solr indexation in JTA transaction.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SolrResource implements OnePhaseResource {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(SolrResource.class);

    protected SolrServer solrServer;
    // On utilise des ThreadLocal pour que chaque thread s'executant ait un
    // 'context' non partage avec les autres, et que si l'on commit un
    // seul ces documents soient commite et pas ceux des autres aussi en cours
    // TODO poussin 20110429 peut-etre revoir ca, pour utilise le threadLocal
    // de WikittyTransaction pour stocker ces infos
    protected ThreadLocal<Map<String, SolrInputDocument>> addedDocs;
    protected ThreadLocal<List<String>> deletedDocs;

    public SolrResource(SolrServer solrServer) {
        this.solrServer = solrServer;
        addedDocs = new ThreadLocal<Map<String, SolrInputDocument>>();
        deletedDocs = new ThreadLocal<List<String>>();

        clear();
    }

    protected void init() {
        // Add resource on phase in current transaction
        LastResourceRecord lastResourceRecord = new LastResourceRecord(this);
        BasicAction.Current().add(lastResourceRecord);
    }

    protected void close() {
        
    }

    public Map<String, SolrInputDocument> getAddedDocs() {
        Map<String, SolrInputDocument> result = addedDocs.get();
        if (result == null) {
            result = new HashMap<String, SolrInputDocument>();
            addedDocs.set(result);
        }
        return result;
    }

    public List<String> getDeletedDocs() {
        List<String> result = deletedDocs.get();
        if (result == null) {
            result = new ArrayList<String>();
            deletedDocs.set(result);
        }
        return result;
    }

    public void clear() {
        addedDocs.set(new HashMap<String, SolrInputDocument>());
        deletedDocs.set(new ArrayList<String>());
    }

    public void addDoc(String id, SolrInputDocument doc) {
        getAddedDocs().put(id, doc);
    }

    public SolrInputDocument getAddedDoc(String id) {
        SolrInputDocument result = getAddedDocs().get(id);
        return result;
    }

    public Collection<String> getAddedDocIds() {
        Collection<String> result = getAddedDocs().keySet();
        return result;
    }

    public void deleteDoc(String docId) {
        getDeletedDocs().add(docId);
    }

    @Override
    public int commit() {
        try {
            synchronized (this) {
                Collection<SolrInputDocument> docs = getAddedDocs().values();
                if (!docs.isEmpty()) {
                    addAllSolrfield(docs);
                    solrServer.add(docs);
                }
                List<String> ids = getDeletedDocs();
                if (!ids.isEmpty()) {
                    solrServer.deleteById(ids);
                }
                solrServer.commit();
            }
            clear();
            return TwoPhaseOutcome.FINISH_OK;
        } catch (Exception eee) {
            log.error("Error commit solr", eee);
            return TwoPhaseOutcome.FINISH_ERROR;
        }
    }

    /**
     * On fait ici le travail de SolR car il ne sait pas faire des copies de
     * champs avec des regexp evaluee.
     * Ce que l'on voudrait:
     * <ul>
     * <li> "ExtName.FieldName_s" avec "*\.(*)_*" devient #all.$1 et #ft.all.$1</li>
     * </ul>
     * 
     * @param docs
     */
    protected void addAllSolrfield(Collection<SolrInputDocument> docs) {
        for (SolrInputDocument doc : docs) {
            Collection<String> fieldnames = new ArrayList<String>(doc.getFieldNames());
            // on boucle une premiere fois pour supprimer les anciennes valeurs
            // on ne peut pas le faire juste avant le add, car au final plusieurs
            // champs pourrait devoir etre stocker dans le meme champs (si deux
            // extension on un champs portant le meme nom. ex: Titi.name et Toto.name)
            for (String solrFqFieldName : fieldnames) {
                if (!solrFqFieldName.startsWith(WikittySolrConstant.SOLR_WIKITTY_PREFIX)) {
                    String fqfieldName = StringUtils.substringBeforeLast(
                            solrFqFieldName, WikittySolrConstant.SOLR_WIKITTY_SUFFIX);

                    // #all.<fieldname>
                    // permet de faire des recherches inter extension sur un champs ayant
                    // le meme nom. ex:Person.name et User.name
                    // Quoi qu'il arrive pour le #all on utilise du multivalue
                    String solrAllFieldName =
                            WikittySolrConstant.SOLR_ALL_EXTENSIONS
                            + WikittyUtil.FQ_FIELD_NAME_SEPARATOR
                            + WikittyUtil.getFieldNameFromFQFieldName(solrFqFieldName);

                    // idem mais un champs sur plusieurs extension peut avoir des types
                    // different, on ajoute donc un champs pour la recherche fulltext
                    String solrFulltextAllFieldName =
                            WikittySolrConstant.SOLR_FULLTEXT_ALL_EXTENSIONS
                            + WikittyUtil.FQ_FIELD_NAME_SEPARATOR
                            + WikittyUtil.getFieldNameFromFQFieldName(fqfieldName);

                    // sortable solr field name for this field ex: myExt.myField_s_sortable
                    String solrFqFieldNameSortable =
                            solrFqFieldName + WikittySolrConstant.SUFFIX_SORTABLE;

                    doc.remove(solrAllFieldName);         // #all.myField_s
                    doc.remove(solrFulltextAllFieldName); // #fulltext.all.myField
                    doc.remove(solrFqFieldNameSortable);  // myExt.myField_s_sortable
                }
            }

            for (String solrFqFieldName : fieldnames) {
                if (!solrFqFieldName.startsWith(WikittySolrConstant.SOLR_WIKITTY_PREFIX)
                        && !solrFqFieldName.endsWith(WikittySolrConstant.SUFFIX_SORTABLE)) {
                    Object fieldValue = doc.getFieldValue(solrFqFieldName);
                    String fqfieldName = StringUtils.substringBeforeLast(
                            solrFqFieldName, WikittySolrConstant.SOLR_WIKITTY_SUFFIX);

                    // #all.<fieldname>
                    // permet de faire des recherches inter extension sur un champs ayant
                    // le meme nom. ex:Person.name et User.name
                    // Quoi qu'il arrive pour le #all on utilise du multivalue
                    String solrAllFieldName = WikittySolrConstant.SOLR_ALL_EXTENSIONS
                            + WikittyUtil.FQ_FIELD_NAME_SEPARATOR
                            + WikittyUtil.getFieldNameFromFQFieldName(solrFqFieldName);

                    // idem mais un champs sur plusieurs extension peut avoir des types
                    // different, on ajoute donc un champs pour la recherche fulltext
                    String solrFulltextAllFieldName = WikittySolrConstant.SOLR_FULLTEXT_ALL_EXTENSIONS
                            + WikittyUtil.FQ_FIELD_NAME_SEPARATOR
                            + WikittyUtil.getFieldNameFromFQFieldName(fqfieldName);

                    // sortable solr field name for this field ex: myExt.myField_s_sortable
                    String solrFqFieldNameSortable =
                            solrFqFieldName + WikittySolrConstant.SUFFIX_SORTABLE;

                    doc.addField(solrAllFieldName, fieldValue);
                    doc.addField(solrFulltextAllFieldName, fieldValue);

                    // add sortable for field #all.
                    Object oneFieldValue = SolrUtil.getOneValue(fieldValue);
                    doc.setField(solrAllFieldName + WikittySolrConstant.SUFFIX_SORTABLE,
                            oneFieldValue);
                    // add sortable for field #ft.all.
                    doc.setField(solrFulltextAllFieldName + WikittySolrConstant.SUFFIX_SORTABLE,
                            oneFieldValue);
                    // add sortable for real field
                    doc.setField(solrFqFieldNameSortable, oneFieldValue);
                }
            }
        }

    }

    @Override
    public int rollback() {
        clear();
        return TwoPhaseOutcome.FINISH_OK;
    }

    @Override
    public void pack(OutputObjectState arg0) throws IOException {
    }

    @Override
    public void unpack(InputObjectState arg0) throws IOException {
    }
}
