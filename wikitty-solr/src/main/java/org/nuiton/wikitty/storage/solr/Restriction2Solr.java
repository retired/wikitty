/*
 * #%L
 * Wikitty :: wikitty-solr
 * %%
 * Copyright (C) 2010 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.storage.solr;

import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.search.operators.Like;
import org.nuiton.wikitty.search.operators.Unlike;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.search.operators.And;
import org.nuiton.wikitty.search.operators.AssociatedRestriction;
import org.nuiton.wikitty.search.operators.Between;
import org.nuiton.wikitty.search.operators.Contains;
import org.nuiton.wikitty.search.operators.Element;
import org.nuiton.wikitty.search.operators.EndsWith;
import org.nuiton.wikitty.search.operators.Equals;
import org.nuiton.wikitty.search.operators.Greater;
import org.nuiton.wikitty.search.operators.GreaterOrEqual;
import org.nuiton.wikitty.search.operators.In;
import org.nuiton.wikitty.search.operators.Keyword;
import org.nuiton.wikitty.search.operators.Less;
import org.nuiton.wikitty.search.operators.LessOrEqual;
import org.nuiton.wikitty.search.operators.Not;
import org.nuiton.wikitty.search.operators.NotEquals;
import org.nuiton.wikitty.search.operators.Or;
import org.nuiton.wikitty.search.operators.Restriction;
import org.nuiton.wikitty.search.RestrictionHelper;
import org.nuiton.wikitty.search.operators.StartsWith;
import org.nuiton.wikitty.search.operators.Null;

/**
 * @author "Nicolas Chapurlat" &lt;nicolas.chapurlat@logica.com&gt;
 * @author "Guillaume Dufrêne" &lt;dufrene@argia.fr&gt;
 * 
 * This class is used to parse Restriction to create lucene request on
 * content. Every operators describe in RestrictionName is handle. Parsing may
 * throw exception when restriction parameters are incorrect.
 */
public class Restriction2Solr {
    
    private static final int MAX_SUBQUERY_RESULT = 100;

    protected FieldModifier fieldModifer;
    protected WikittyTransaction transaction;

    // TODO 20101201 jru improve manage transaction and fieldModifeir in helper
    public Restriction2Solr(WikittyTransaction transaction, FieldModifier fieldModifer) {
        this.transaction = transaction;
        this.fieldModifer = fieldModifer;
    }

    /*public String toSolr(Restriction restriction) {
        return toSolr(restriction, null);
    }*/
    
    public String toSolr(Restriction restriction, SolrServer solr)
            throws WikittyException {
        // ParameterValidator.checkNullParameter(restriction, "restriction");
        switch (restriction.getName()) {
            case TRUE:
                return true2solr();
            case FALSE:
                return false2solr();
            case NOT:
                Not not = (Not) restriction;
                return not2solr(not, solr);
            case AND:
                And and = (And) restriction;
                return and2solr(and, solr);
            case OR:
                Or or = (Or) restriction;
                return or2solr(or, solr);
            case EQUALS:
                Equals eq = (Equals) restriction;
                return eq2solr(eq);
            case NOT_EQUALS:
                NotEquals neq = (NotEquals) restriction;
                return neq2solr(neq);
            case LESS:
                Less less = (Less) restriction;
                return less2solr(less);
            case LESS_OR_EQUAL:
                LessOrEqual lessEq = (LessOrEqual) restriction;
                return lessEq2solr(lessEq);
            case GREATER:
                Greater great = (Greater) restriction;
                return great2solr(great);
            case GREATER_OR_EQUAL:
                GreaterOrEqual greatEq = (GreaterOrEqual) restriction;
                return greatEq2solr(greatEq);
            case BETWEEN:
                Between between = (Between) restriction;
                return between2solr(between);
            case CONTAINS:
                Contains contains = (Contains) restriction;
                return contains2solr(contains);
            case IN:
                In in = (In) restriction;
                return in2solr(in);
            case STARTS_WITH:
                StartsWith start = (StartsWith) restriction;
                return start2solr(start);
            case ENDS_WITH:
                EndsWith end = (EndsWith) restriction;
                return end2solr(end);
            case LIKE:
                Like like = (Like) restriction;
                return like2solr(like);
            case UNLIKE:
                Unlike unlike = (Unlike) restriction;
                return unlike2solr(unlike);
            case ASSOCIATED:
                AssociatedRestriction associated = (AssociatedRestriction) restriction;
                return associated2solr(associated, solr);
            case KEYWORD:
                Keyword keyword = (Keyword) restriction;
                return keyword2solr(keyword);
            case IS_NULL:
                Null isNull = (Null) restriction;
                return isNull2solr(isNull);
            case IS_NOT_NULL:
                Null isNotNull = (Null) restriction;
                return isNotNull2solr(isNotNull);
            default:
                throw new WikittyException("this kind of restriction is not supported : "
                        + restriction.getName().toString());
        }
    }

    // FIXME poussin 20101220 i think this method return bad lucene syntaxe query
    // I think the syntaxe must be the same as contains
    private String in2solr(In in) {
        if (in.getElement() == null) {
            throw new WikittyException("in.element must not be null");
        }
        if (in.getValue() == null) {
            throw new WikittyException("in.values must not be null");
        }
        if (in.getValue().size() < 1) {
            throw new WikittyException("IN is an operator that handle 1 operand at least");
        }

        String operand = "";
        StringBuilder result = new StringBuilder();
        result.append(element2solr(in.getElement())).append(':');
        result.append('(');
        for (String value : in.getValue()) {
            result.append(operand);
            result.append(value2solr(value));
            operand = " OR ";
        }
        result.append(')');
        return result.toString();
    }

    private String associated2solr(AssociatedRestriction associated, SolrServer solr) throws WikittyException {
        String subQuery = toSolr( associated.getRestriction(), solr);
        SolrQuery query = new SolrQuery(WikittySolrConstant.SOLR_QUERY_PARSER + subQuery);
        query.setRows(MAX_SUBQUERY_RESULT);
        QueryResponse resp = null;
        try {
            resp = SolrUtil.executeQuery(solr, query);
        } catch (SolrServerException e) {
            throw new WikittyException("Unable to execute associative query on "
                    + associated.getElement().getName(), e);
        }
        SolrDocumentList solrResults = resp.getResults();
        
        Restriction generatedRestriction;
        long size = solrResults.size();
        if ( size == 0 ) {
            generatedRestriction = RestrictionHelper.rFalse();
        } else if ( size == 1 ) {
            String id = SolrUtil.getStringFieldValue(
                    solrResults.get(0), WikittySolrConstant.SOLR_ID);
            generatedRestriction = RestrictionHelper.eq(associated.getElement(), id);
        } else {
            List<String> ids = new ArrayList<String>(solrResults.size());
            for (SolrDocument doc : solrResults) {
                String id = SolrUtil.getStringFieldValue(doc, WikittySolrConstant.SOLR_ID);
                ids.add(id);
            }
            generatedRestriction = new In(associated.getElement(), ids);
        }
        return toSolr(generatedRestriction, solr);
    }

    private String not2solr(Not not, SolrServer solr) throws WikittyException {
        if (not.getRestriction() == null) {
            throw new WikittyException( "not.restriction" );
        }
        // no space after '-' !!!
        return "( -" + toSolr(not.getRestriction(), solr) + " )";
    }

    private String and2solr(And and, SolrServer solr) throws WikittyException {
        if (and.getRestrictions() == null) {
            throw new WikittyException( "and.restrictions is null" );
        }
        if (and.getRestrictions().size() < 2) {
            throw new WikittyException(    "AND is an operator that handle 2 operand at least");
        }
        boolean first = true;
        StringBuffer result = new StringBuffer();
        for (Restriction restriction : and.getRestrictions()) {
            if (first) {
                result.append("( ").append(toSolr(restriction, solr));
                first = false;
            } else {
                result.append(" AND ").append(toSolr(restriction, solr));
            }
        }
        return result.append(" )").toString();
    }

    private String or2solr(Or or, SolrServer solr) throws WikittyException {
        if (or.getRestrictions() == null) {
            throw new WikittyException("or.restrictions is null");
        }
        if (or.getRestrictions().size() < 2) {
            throw new WikittyException("OR is an operator that handle 2 operand at least");
        }
        boolean first = true;
        StringBuffer result = new StringBuffer();
        for (Restriction restriction : or.getRestrictions()) {
            if (first) {
                result.append("( ");
                first = false;
            } else {
                result.append(" OR ");
            }
            result.append(toSolr(restriction, solr));
        }
        return result.append(" )").toString();
    }

    private String eq2solr(Equals eq) throws WikittyException {
        return element2solr(eq.getElement()) + ":" + value2solr(eq.getValue());
    }

    private String neq2solr(NotEquals neq)
            throws WikittyException {
        return "-" + element2solr(neq.getElement()) + ":"
                + value2solr(neq.getValue());
    }

    private String less2solr(Less less) throws WikittyException {
        return element2solr(less.getElement()) + ":{* TO "
                + value2solr(less.getValue()) + "}";
    }

    private String lessEq2solr(LessOrEqual lessEq)
            throws WikittyException {
        return element2solr(lessEq.getElement()) + ":[* TO "
                + value2solr(lessEq.getValue()) + "]";
    }

    private String great2solr(Greater great)
            throws WikittyException {
        return element2solr(great.getElement()) + ":{"
                + value2solr(great.getValue()) + " TO *}";
    }

    private String greatEq2solr(GreaterOrEqual greatEq)
            throws WikittyException {
        return element2solr(greatEq.getElement()) + ":["
                + value2solr(greatEq.getValue()) + " TO *]";
    }

    private String between2solr(Between between)
            throws WikittyException {
        if (between.getElement() == null) {
            throw new WikittyException("between.element must not be null");
        }
        if (between.getMin() == null) {
            throw new WikittyException("between.min must not be null");
        }
        if (between.getMax() == null) {
            throw new WikittyException("between.max must not be null");
        }
        return element2solr(between.getElement()) + ":["
                + value2solr(between.getMin()) + " TO "
                + value2solr(between.getMax()) + "]";
    }

    private String contains2solr(Contains contains)
            throws WikittyException {
        if (contains.getElement() == null) {
            throw new WikittyException("contains.element must not be null");
        }
        if (contains.getValue() == null) {
            throw new WikittyException("contains.values must not be null");
        }
        if (contains.getValue().size() < 1) {
            throw new WikittyException("CONTAINS is an operator that handle 1 operand at least");
        }

        String operand = "";
        StringBuilder result = new StringBuilder();
        result.append(element2solr(contains.getElement())).append(':');
        result.append('(');
        for (String value : contains.getValue()) {
            result.append(operand);
            result.append(value2solr(value));
            operand = " AND ";
        }
        result.append(')');
        return result.toString();
    }

    private String start2solr(StartsWith start)
            throws WikittyException {
        return element2solr(start.getElement()) + ":"
                + value2solr(start.getValue()) + "*";
    }

    private String end2solr(EndsWith end) {
        return element2solr(end.getElement()) + ":*"
                + value2solr(end.getValue());
    }

    private String true2solr() {
        return "( *:* )";
    }

    private String false2solr() {
        // no space after '-' !!!
        return "( -*:* )";
    }

    private String keyword2solr(Keyword keyword) {
        return value2solr(keyword.getValue());
    }

    private String isNull2solr(Null isNull) {
        return WikittySolrConstant.SOLR_NULL_FIELD + isNull.getFieldName() + ":true";
    }

    private String isNotNull2solr(Null isNotNull) {
        return WikittySolrConstant.SOLR_NULL_FIELD + isNotNull.getFieldName() + ":false";
    }

    private String element2solr(Element element) throws WikittyException {
        String result = element.getName();
        result = fieldModifer.convertToSolr(transaction, result);
        return result;
    }

    private String value2solr(String value) {
        String result;
        if (value != null) {
            result = Restriction2Solr.escapeValue(value);
        } else {
            throw new WikittyException("Parse error, value must be not empty");
        }

        if (result.contains(" ")) {
            result = "\"" + result + "\"";
        }
        return result;
    }

    private String like2solr(Like like) throws WikittyException {
        Like.SearchAs searchAs = like.getSearchAs();
        String element2solr = element2solr(like.getElement());
        if (element2solr.endsWith(WikittySolrConstant.SUFFIX_STRING)) { // is string
            switch (searchAs) {
                case AsText:
                    element2solr += WikittySolrConstant.SUFFIX_STRING_FULLTEXT;
                    break;
                case ToLowerCase:
                    element2solr += WikittySolrConstant.SUFFIX_STRING_LOWERCASE;
                    break;
            }
        }
//        FIXME REMOVE IT if search on multivalued work with new hack (specific sortable field
//        else if (element2solr.endsWith(WikittySolrConstant.SUFFIX_STRING_MULTIVALUED)) { // is multivalued string
//            // Remove _s*m*
//            element2solr = element2solr.substring(0, element2solr.length() - 1);
//            switch (searchAs) {
//                case AsText:
//                    element2solr += WikittySolrConstant.SUFFIX_STRING_FULLTEXT_MULTIVALUED;
//                    break;
//                case ToLowerCase:
//                    element2solr += WikittySolrConstant.SUFFIX_STRING_FULLTEXT_LOWERCASE;
//                    break;
//            }
//        }
        // Warning if you need add searchAs, AsText and ToLowerCase need search
        // at lowercase
        String value2solr = value2solr(like.getValue());
        if (!element2solr.endsWith(WikittySolrConstant.SUFFIX_DATE)) { // is not date
            value2solr = value2solr.toLowerCase();
        }
        return element2solr + ":" + value2solr;
    }

    private String unlike2solr(Unlike unlike) throws WikittyException {
        Like.SearchAs searchAs = unlike.getSearchAs();
        String element2solr = element2solr(unlike.getElement());
        if (element2solr.endsWith(WikittySolrConstant.SUFFIX_STRING)) { // is string
            switch (searchAs) {
                case AsText:
                    element2solr += WikittySolrConstant.SUFFIX_STRING_FULLTEXT;
                    break;
                case ToLowerCase:
                    element2solr += WikittySolrConstant.SUFFIX_STRING_LOWERCASE;
                    break;
            }
        }
        // Warning if you need add searchAs, AsText and ToLowerCase need search
        // at lowercase
        String value2solr = value2solr(unlike.getValue());
        if (!element2solr.endsWith(WikittySolrConstant.SUFFIX_DATE)) { // is not date
            value2solr = value2solr.toLowerCase();
        }
        return "-" + element2solr + ":" + value2solr;
    }


    private static String escapeValue(String value) {
        final String LUCENE_REPLACE_PATTERN = "\\+" + "|-" + "|&&" + "|\\|"
                + "|!" + "|\\(|\\)" + "|\\[|\\]" + "|\\{|\\}" + "|\"" + "|:";
        return value.replaceAll(LUCENE_REPLACE_PATTERN, "\\\\$0");
    }
}

