/*
 * #%L
 * Wikitty :: wikitty-solr
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.storage.solr;

/**
 * Extraction des constantes de WikittySearchEngineSolr pour pouvoir les
 * utiliser dans les différentes implantations de WikittySearchEngine car il
 * est très lié au fichier de configuration Solr partagé par les différentes
 * implantation (car impossible de trouver comment specifier les fichiers de
 * configuration a utiliser pour une implantation donnees :()
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface WikittySolrConstant {

    /**
     * Precise the query parser to use. Now this is configured in solrconfig.xml
     * in the '/select' handler as {!lucene q.op=AND df=#fulltext}.
     */
    static final public String SOLR_QUERY_PARSER = "";

    /**
     * Prefix utiliser pour les champs ajouter lors de l'indexation. Ce prefix
     * evite d'avoir des conflits entre un nom d'extension et un champs ajoute
     */
    static final public String SOLR_WIKITTY_PREFIX = "#";

    /** id field in solr */
    static final public String SOLR_ID = SOLR_WIKITTY_PREFIX + "id";

    /** if field is null, this extra field is set to true otherwize is set to false */
    static final public String SOLR_NULL_FIELD = SOLR_WIKITTY_PREFIX + "null_field-";

    /** extensions field name in solr */
    static final public String SOLR_EXTENSIONS = SOLR_WIKITTY_PREFIX + "extensions";

    /** default field to fulltext search */
    static final public String SOLR_FULLTEXT = SOLR_WIKITTY_PREFIX + "fulltext";

    /** extension use to store field without extension to search on all extension */
    static final public String SOLR_ALL_EXTENSIONS = SOLR_WIKITTY_PREFIX + "all";

    /** extension use to store field without extension to search on all extension in fulltext mode*/
    static final public String SOLR_FULLTEXT_ALL_EXTENSIONS = SOLR_WIKITTY_PREFIX + "ft.all";

    /** Use for indexation tree node */
    static final public String TREENODE_PREFIX = SOLR_WIKITTY_PREFIX + "tree.";
    /** Use as field on TreeNode */
    static final public String TREENODE_ROOT = TREENODE_PREFIX + "root";
    /** Use as field on TreeNode, contains parent node id and himself node id */
    static final public String TREENODE_PARENTS = TREENODE_PREFIX + "parents";
    /** Use as field on TreeNode, number of parents (root node depth=1) */
    static final public String TREENODE_DEPTH = TREENODE_PREFIX + "depth";
    /** Use as field on Wikitty object attached on TreeNode, TreeNodeId is added at end */
    static final public String TREENODE_ATTACHED = TREENODE_PREFIX + "attached.";
    /** Use as field on Wikitty object attached on TreeNode, TreeNodeId is added at end
        used for facetisation. Le champs est cree dynamiquement par solr via schema.xml */
    static final public String TREENODE_ATTACHED_ALL = TREENODE_PREFIX + "attached-all";

    static final public String SOLR_WIKITTY_SUFFIX = "_";
    static final public String SUFFIX_BINARY = SOLR_WIKITTY_SUFFIX + "bi";
    static final public String SUFFIX_BOOLEAN = SOLR_WIKITTY_SUFFIX + "b";
    static final public String SUFFIX_NUMERIC = SOLR_WIKITTY_SUFFIX + "d";
    static final public String SUFFIX_DATE = SOLR_WIKITTY_SUFFIX + "dt";
    static final public String SUFFIX_STRING = SOLR_WIKITTY_SUFFIX + "s";
    static final public String SUFFIX_WIKITTY = SOLR_WIKITTY_SUFFIX + "w";

    static final public String SUFFIX_SORTABLE = SOLR_WIKITTY_SUFFIX + "sortable";

    static final public String SUFFIX_STRING_LOWERCASE = SOLR_WIKITTY_SUFFIX + "c";
    static final public String SUFFIX_STRING_FULLTEXT = SOLR_WIKITTY_SUFFIX + "t";

    // expression always false. (-*:*) /!\ no space between '-' and '*:*'
    static final public String SOLR_FALSE = "(-*:*)";
}
