/*
 * #%L
 * Wikitty :: wikitty-solr
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin POUSSIN
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.storage.solr;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.embedded.EmbeddedSolrServer;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.core.CoreContainer;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.TimeLog;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.entities.ElementField;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyTreeNode;
import org.nuiton.wikitty.entities.WikittyTreeNodeHelper;
import org.nuiton.wikitty.entities.WikittyTypes;
import org.nuiton.wikitty.query.FacetQuery;
import org.nuiton.wikitty.query.FacetTopic;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.query.WikittyQueryResultTreeNode;
import org.nuiton.wikitty.query.conditions.Condition;
import org.nuiton.wikitty.query.conditions.Select;
import org.nuiton.wikitty.query.conditions.True;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;
import org.nuiton.wikitty.search.TreeNodeResult;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.storage.WikittyExtensionStorage;
import org.nuiton.wikitty.storage.WikittySearchEngine;

import static org.nuiton.wikitty.storage.solr.WikittySolrConstant.SOLR_ID;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittySearchEngineSolr implements WikittySearchEngine, WikittySolrConstant {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(WikittySearchEngineSolr.class);
    final static private TimeLog timeLog = new TimeLog(WikittySearchEngineSolr.class);

    protected ApplicationConfig config;

    /** solr server */
    protected SolrServer solrServer;

    /** Field modifier use to transform to solr format */
    @Deprecated
    protected FieldModifier fieldModifier;

    /** Field modifier use to transform to solr format */
    protected ElementModifier elementModifier;

    /** JTA resource */
    protected SolrResource solrResource;

    /**
     * Init wikitty search engine on solr embedded server.
     * 
     * @param extensionStorage extension storage
     * @param config app config (can be null)
     */
    public WikittySearchEngineSolr(
            ApplicationConfig config, WikittyExtensionStorage extensionStorage) {

        this.config = config;
        solrServer = SolrUtil.getSolrServer(config);
        fieldModifier = new FieldModifier(extensionStorage);
        elementModifier = new ElementModifier(extensionStorage);
        solrResource = new SolrResource(solrServer);
    }

    public ApplicationConfig getConfig() {
        return config;
    }

    public SolrServer getSolrServer() {
        return solrServer;
    }

    @Override
    public void clear(WikittyTransaction transaction) {
        try {
            // FIXME poussin 20100618 pourquoi n'est pas fait dans la transaction ?
            solrResource.init();
            getSolrServer().deleteByQuery("*:*");
        } catch (Exception eee) {
            throw new WikittyException("Error during clearing SolR data", eee);
        }
    }

    @Override
    public void store(WikittyTransaction transaction,
            Collection<Wikitty> wikitties, boolean force) {
        long startTime = TimeLog.getTime();
        try {
            solrResource.init();

            // tous les wikitties passes en parametre
            Map<String, Wikitty> allWikitties = new HashMap<String, Wikitty>();
            // les ids des wikitties en parametre reellement modifier (a reindexer)
            Set<String> dirtyObject = new HashSet<String>();
            // les ids des TreeNodes dont le champs parent a change (est aussi
            // contenu dans dirtyObject
            Set<String> dirtyParent = new HashSet<String>();
            // les valeur du champs parent des TreeNodes dont le champs parent
            // a change (sauf si parent = null)
            Set<String> dirtyParentParentId = new HashSet<String>();

            // remplissage des collections
            for(Wikitty w : wikitties) {
                allWikitties.put(w.getWikittyId(), w);
                if (force || !w.getDirty().isEmpty() ||
                        WikittyUtil.versionGreaterThan("1", w.getWikittyVersion())) {
                    // s'il y a au moins un champs a reindexer ou que l'objet
                    // n'a jamais ete sauve (1 > version)
                    dirtyObject.add(w.getWikittyId());
                    if (WikittyTreeNodeHelper.hasExtension(w) && (force
                            ||w.getDirty().contains(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_PARENT)
                            || null == WikittyTreeNodeHelper.getParent(w))) {
                            // si le pere a change
                            // ou qu'il est null (creation d'un nouvel arbre)
                            // il faut indexer le noeud
                        dirtyParent.add(w.getWikittyId());
                        String parent = WikittyTreeNodeHelper.getParent(w);
                        if (parent != null) {
                            dirtyParentParentId.add(parent);
                        }
                    }
                }
            }

            // recuperation des documents Solr deja indexes, pour minimiser la reindexation
            Map<String, SolrDocument> dirtyObjectDoc =
                    SolrUtil.findAllById(getSolrServer(), dirtyObject);
            Map<String, SolrDocument> dirtyParentDoc =
                    SolrUtil.findAllByParents(getSolrServer(), dirtyParent);
            Map<String, SolrDocument> parents =
                    SolrUtil.findAllById(getSolrServer(), dirtyParentParentId);

            // On genere en meme temps la liste des attachments qui doivent
            // etre reindexe
            AttachmentInTree attachmentInTree = new AttachmentInTree();
            
            //
            // Phase 1: on indexe les objets passe en paremetre, on copie si
            //     besoin #tree.attached des wikitties et #tree.* des
            //     TreeNode dont leur champs parent n'a pas ete modifie, et
            //     dans ce cas on collecte les modif d'attachments des TreeNode
            //

            for(String id : dirtyObject) {
                Wikitty w = allWikitties.get(id);
                SolrDocument oldDoc = dirtyObjectDoc.get(id);
                SolrInputDocument doc = createIndexDocument(w);
                if (oldDoc != null) {
                    // On a un ancien document partiel ou complet
                    // s'il etait partiel (seulement l'indexation arbre
                    // cela veut dire que l'objet TreeNode a ete store
                    // sans que l'attachment le soit et qu'il l'est que
                    // maintenant

                    // copy des champs #tree.attached des documents partiels ou non
                    SolrUtil.copySolrDocument(oldDoc, doc, TREENODE_ATTACHED + ".*");
                    if (WikittyTreeNodeHelper.hasExtension(w)
                            && !dirtyParentDoc.containsKey(id)) {
                        // si c'est un TreeNode, mais qu'aucun pere n'a change
                        // on recopie l'ancienne indexation d'arbre
                        // si elle existe
                        SolrUtil.copySolrDocument(oldDoc, doc, TREENODE_PREFIX + ".*");

                        // il faut verifier les objets attaches
                        // attaches ajoute/supprime
                        // on ne traite ici que les TreeNode sans modif d'indexation
                        // pour les autres les attachments seront traites dans
                        // la phase suivante
                        Set<String> newAtt = WikittyTreeNodeHelper.getAttachment(w);

                        Collection<String> oldAtt = SolrUtil.getStringFieldValues(
                                oldDoc,
                                WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_ATTACHMENT,
                                WikittyTypes.WIKITTY);
                        // il faut supprimer l'indexation arbre des noeuds
                        // qui sont dans old, mais pas dans new
                        Set<String> toRemove = new HashSet<String>();
                        if (oldAtt != null) {
                            toRemove.addAll(oldAtt);
                        }
                        if (newAtt != null) {
                            toRemove.removeAll(newAtt);
                        }
                        attachmentInTree.remove(id, toRemove);
                        // il faut ajouter l'indexation arbre des noeuds
                        // qui sont dans new, mais pas dans old
                        Set<String> toAdd = new HashSet<String>();
                        if (newAtt != null) {
                            toAdd.addAll(newAtt);
                        }
                        if (oldAtt != null) {
                            toAdd.removeAll(oldAtt);
                        }
                        attachmentInTree.add(id, toAdd);
                    }
                }
                solrResource.addDoc(id, doc);
            }

            //
            // Phase 2: on reindexe tous les TreeNode qui en ont besoin
            //          nouveau TreeNode ou TreeNode ayant un parent modifie
            //

            // on ajoute tous les TreeNode qui doivent aussi etre reindexe
            // noeud du sous arbre d'un noeud dont le pere a ete modifie
            dirtyParent.addAll(dirtyParentDoc.keySet());

            for (String id : dirtyParent) {
                // w et oldDoc peuvent etre null, mais pas en meme temps
                // w est null si c'est un noeud dont la reindexation est force
                // parce que un de ces peres a change de parent
                // oldDoc est null, si l'objet n'a jamais ete indexe (nouveau)
                Wikitty w = allWikitties.get(id);
                SolrDocument oldDoc = dirtyParentDoc.get(id);
                SolrInputDocument doc = solrResource.getAddedDoc(id);
                if (w == null) {
                    // on reindexe un ancien objet
                    // normalement doc doit etre null
                    doc = new SolrInputDocument();
                    // on recopie tous les champs, sauf l'indexation arbre
                    SolrUtil.copySolrDocumentExcludeSomeField(
                            oldDoc, doc, TREENODE_PREFIX + ".*");

                    // modifie les champs root, parents
                    addTreeIndexField(solrResource, doc, parents);

                    attachmentInTree.remove(oldDoc);
                    attachmentInTree.add(oldDoc);
                } else if (oldDoc == null) {
                    // ajoute les champs root, parents
                    addTreeIndexField(solrResource, doc, parents);

                    // on indexe un nouvel objet, il faut ajouter tous les
                    // attachment pour indexation
                    attachmentInTree.add(w);
                } else {
                    // ni w, ni oldDoc ne sont pas null, c'est une modification
                    // dans la phase precendente on a deja indexe les champs
                    // normaux

                    // ajoute les champs root, parents
                    addTreeIndexField(solrResource, doc, parents);

                    // il faut supprimer tous les anciens attaches
                    // et ajouter tous nouveaux pour la reindexation
                    attachmentInTree.remove(oldDoc);
                    attachmentInTree.add(w);
                }
                solrResource.addDoc(id, doc);
            }

            //
            // Phase 3: on reindexe les attachments qui en ont besoin
            //

            addTreeIndexField(solrResource, null, attachmentInTree);
        } catch (Exception eee) {
            throw new WikittyException("Can't store wikitty " + wikitties, eee);
        }
        timeLog.log(startTime, "store", String.format(
                "nb %s in force mode %s", wikitties.size(), force));
    }

    /**
     * Plusieurs actions possibles en fontion du type d'objet:
     *
     * <ul>
     * <li> suppression d'un objet NON noeud
     *  <ul>
     *   <li> suppression de cet objets</li>
     *   <li> suppression de cet objets dans les attachments des noeuds qui le contiennent</li>
     *  </ul>
     * </li>
     * <li> suppression d'un noeud d'arbre
     *  <ul>
     *   <li> suppression du noeud</li>
     *   <li> reindexation des noeuds qui le contenait comme parent</li>
     *   <li> suppression des attached sur les objets contenus dans les attachments de ce noeud</li>
     *   <li> reindexation des objets qui le contenait comme parent dans un champs attached</li>
     *  </ul>
     * </li>
     * </ul>
     *
     * @param transaction wikitty transaction
     * @param ids to deletes
     * @throws WikittyException
     */
    @Override
    public void delete(WikittyTransaction transaction, Collection<String> ids) throws WikittyException {
        long startTime = TimeLog.getTime();
        try {
            solrResource.init();

            AttachmentInTree attachmentInTree = new AttachmentInTree();

            // list de tous les objets a supprimer
            Map<String, SolrDocument> removed = 
                    SolrUtil.findAllById(getSolrServer(), ids);
            // list des TreeNode supprimer
            Map<String, SolrDocument> treeNodeRemoved =
                    new HashMap<String, SolrDocument>();

            // list des TreeNode dont des attachments ont ete supprime
            Set<String> treeNodeAttachmentRemovedId = new HashSet<String>();

            //
            // Phase 1: Suppression des objets demandes en parametre
            //
            for (Map.Entry<String, SolrDocument> e : removed.entrySet()) {
                String id = e.getKey();
                SolrDocument doc = e.getValue();
                if (doc.containsKey(TREENODE_ROOT)) {
                    treeNodeRemoved.put(id, doc);
                    attachmentInTree.remove(doc);
                }
                // recherche tous les noeuds sur lequel est attache cet objet
                
                // TODO poussin 20110217 optimiser cette requete pour la faire
                // hors de la boucle et pour tous les doc a supprimer en meme
                // temps (donc 1 seul requete au lieu de N)
                Set<String> treeNodeIds = SolrUtil.getAttachedTreeNode(doc);
                treeNodeAttachmentRemovedId.addAll(treeNodeIds);
                
                solrResource.deleteDoc(id);
            }

            //
            // Phase 2: Suppression dans les listes d'attachment des TreeNode supprimes
            //

            // on essaie pas de modifier un noeud que l'on vient de supprimer
            treeNodeAttachmentRemovedId.removeAll(ids);
            if (treeNodeAttachmentRemovedId.size() > 0) {

                // recuperation des noeuds dont il faut modifier les attachments
                Map<String, SolrDocument> treeNodeAttachmentRemoved =
                        SolrUtil.findAllById(getSolrServer(), treeNodeAttachmentRemovedId);
                for (Map.Entry<String, SolrDocument> e : treeNodeAttachmentRemoved.entrySet()) {
                    String id = e.getKey();

                    // le noeud n'a pas ete supprime, donc il faut le mettre a jour
                    SolrDocument doc = e.getValue();
                    SolrInputDocument newDoc = new SolrInputDocument();
                    String field = SolrUtil.getSolrFieldName(
                            WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_ATTACHMENT,
                            WikittyTypes.WIKITTY);
                    SolrUtil.copySolrDocumentExcludeSomeField(doc, newDoc, field);
                    Collection<String> atts = SolrUtil.getStringFieldValues(doc, field);
                    // remove deleted attachment
                    Set<String> newAtts = new HashSet<String>(atts);
                    newAtts.removeAll(ids);
                    // si apres effacement des attachments, il n'y a plus
                    // d'attachment sur le node, cela revient a avoir
                    // le field attachment null
                    if (newAtts.isEmpty()) {
                        newAtts = null;
                    }

                    addToIndexDocument(newDoc, WikittyTypes.WIKITTY, field, newAtts, true);
                    solrResource.addDoc(id, newDoc);
                }
            }

            // les phases suivantes ne sont a faire que s'il y a des TreeNode
            // supprimes
            Map<String, SolrDocument> treeNodeParentRemoved;
            if (treeNodeRemoved != null && treeNodeRemoved.size() > 0) {
                //
                // Phase 3: reindexation des noeuds dont un pere a disparu
                //

                // list des TreeNode dont un des pere a ete supprime
                treeNodeParentRemoved =
                        SolrUtil.findAllByParents(getSolrServer(), ids);

                // on commence par supprimer des noeuds a reindexer les noeuds supprimes
                treeNodeParentRemoved.keySet().removeAll(treeNodeRemoved.keySet());

                for (Map.Entry<String, SolrDocument> e : treeNodeParentRemoved.entrySet()) {
                    SolrDocument oldDoc = e.getValue();

                    SolrInputDocument doc = new SolrInputDocument();
                    // on recopie tous les champs, sauf l'indexation arbre
                    SolrUtil.copySolrDocumentExcludeSomeField(
                            oldDoc, doc, TREENODE_PREFIX + ".*");

                    // modifie les champs root, parents
                    addTreeIndexField(solrResource, doc, treeNodeParentRemoved);

                    // il faut reindexer tous les attachments de ce noeud
                    attachmentInTree.remove(oldDoc);
                    attachmentInTree.add(doc);
                }

                //
                // Phase 4: on reindexe les attachments qui en ont besoin
                //
                attachmentInTree.clean(ids);
                addTreeIndexField(solrResource, treeNodeParentRemoved, attachmentInTree);
            }
        } catch (Exception eee) {
            throw new WikittyException("Can't delete wikitty in index", eee);
        } finally {
            solrResource.close();
        }
        timeLog.log(startTime, "delete", String.format(
                "nb %s", ids.size()));

    }

    /**
     * Modifie/Ajoute les champs specifique a l'indexation des arbres sur les
     * TreeNode.
     *
     * On se base sur le fait que si un TreeNode est dans {@link SolrResource} il ne
     * peut etre que dans deux etats. Soit il a ete reindexe pour les arbres
     * et il a les champs d'indexation arbre. Soit il a pas encore ete reindexe
     * pour les arbres et dans ce cas il ne doit pas avoir les champs d'indexation
     * d'arbre. (il est donc interdit d'avoir des champs d'indexation arbre
     * obsolete si le document est dans {@link SolrResource})
     *
     * @param solrResource solR resource
     * @param doc les documents representant le TreeNode
     * @param tree tous les autres noeuds d'arbre dont on pourrait avoir
     * besoin pour l'indexation
     */
    protected void addTreeIndexField(SolrResource solrResource,
            SolrInputDocument doc, Map<String, SolrDocument> tree) {
        Set<String> parents = new HashSet<String>();
        String root = null;
        String treeNodeId = SolrUtil.getStringFieldValue(doc, WikittySolrConstant.SOLR_ID);
        String parentId = treeNodeId;
        if (parentId == null) {
            throw new WikittyException("parentId is null, but this must be impossible");
        }
        parents.add(parentId);
        while (root == null) {
            String nextParentId = null;
            SolrInputDocument parentDoc = solrResource.getAddedDoc(parentId);
            if (parentDoc != null) {
                // si parentDoc a deja ete indexe pour l'arbre, on peut reutiliser
                // directement les valeurs et sortir de la boucle
                if (parentDoc.containsKey(TREENODE_ROOT)) {
                    root = SolrUtil.getStringFieldValue(parentDoc, TREENODE_ROOT);
                    Collection<String> p = SolrUtil.getStringFieldValues(parentDoc, TREENODE_PARENTS);
                    parents.addAll(p);
                    break;
                } else {
                    nextParentId = SolrUtil.getStringFieldValue(parentDoc,
                            WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_PARENT,
                            WikittyTypes.WIKITTY);
                }
            } else {
                SolrDocument oldParentDoc = tree.get(parentId);
                if (oldParentDoc != null) {
                    // si parentDoc a deja ete indexe pour l'arbre, on peut reutiliser
                    // directement les valeurs et sortir de la boucle
                    if (oldParentDoc.containsKey(TREENODE_ROOT)) {
                        root = SolrUtil.getStringFieldValue(oldParentDoc,TREENODE_ROOT);
                        Collection<String> p = SolrUtil.getStringFieldValues(oldParentDoc, TREENODE_PARENTS);
                        parents.addAll(p);
                        break;
                    } else {
                        nextParentId = SolrUtil.getStringFieldValue(oldParentDoc,
                                WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_PARENT,
                                WikittyTypes.WIKITTY);
                    }
                }
            }
            if (nextParentId != null) {
                if (parents.contains(nextParentId)) {
                    log.error(String.format("Tree with TreeNode '%s' have loop"
                            + " at node %s->%s all parents are %s. Set root with"
                            + " last valide parent '%s'",
                            treeNodeId, parentId, nextParentId, parents, parentId));
                    root = parentId;
                } else {
                    parents.add(nextParentId);
                    parentId = nextParentId;
                }
            } else {
                root = parentId;
            }
        }

        doc.removeField(TREENODE_ROOT);
        doc.removeField(TREENODE_PARENTS);
        doc.removeField(TREENODE_DEPTH);

        doc.addField(TREENODE_ROOT, root);
        doc.addField(TREENODE_DEPTH, parents.size());
        for (String id : parents) {
            doc.addField(TREENODE_PARENTS, id);
        }
    }

    /**
     * Update attached extra field on all objects passed in argument
     * allAttachmentToIndex
     *
     * @param solrResource must contains reindexed TreeNode, that contains attachment
     * @param tree solr document for some TreeNode (used when TreeNode not find in solrResource)
     * @param attachmentInTree attachment added and removed from TreeNode
     */
    protected void addTreeIndexField(SolrResource solrResource,
            Map<String, SolrDocument> tree, AttachmentInTree attachmentInTree) {

        if (attachmentInTree.size() > 0) {
            Map<String, SolrDocument> attachments =
                    SolrUtil.findAllById(getSolrServer(), attachmentInTree.getAll());

            for (String treeNodeId : attachmentInTree.getRemoved().keySet()) {
                for (String attId : attachmentInTree.getRemoved().get(treeNodeId)) {
                    SolrDocument oldDoc = attachments.get(attId);
                    SolrInputDocument doc = solrResource.getAddedDoc(attId);
                    if (oldDoc != null || doc != null) {
                        if (doc == null) {
                            doc = new SolrInputDocument();
                            SolrUtil.copySolrDocument(oldDoc, doc);
                            solrResource.addDoc(attId, doc);
                        }
                        doc.remove(TREENODE_ATTACHED + treeNodeId);
                    }
                }
            }
            for (String treeNodeId : attachmentInTree.getAdded().keySet()) {
                Collection<String> treeNodeParents = null;
                SolrInputDocument treeNodeDoc = solrResource.getAddedDoc(treeNodeId);
                if (treeNodeDoc != null) {
                    treeNodeParents = SolrUtil.getStringFieldValues(
                            treeNodeDoc, TREENODE_PARENTS);
                } else if (tree != null) {
                    SolrDocument doc = tree.get(treeNodeId);
                    treeNodeParents = SolrUtil.getStringFieldValues(
                            doc, TREENODE_PARENTS);
                } else {
                    log.error("SolR doc not found in Transaction or in tree."
                            + "This is a bug !!!");
                }
                // add tree indexation on all attachments for this treeNodeId
                for (String attId : attachmentInTree.getAdded().get(treeNodeId)) {
                    SolrDocument oldDoc = attachments.get(attId);
                    SolrInputDocument doc = solrResource.getAddedDoc(attId);
                    // il faut que oldDoc ou doc soit different de null pour
                    // pouvoir ajouter l'indexation d'arbre. Le cas on les deux
                    // sont nuls arrivent lorsqu'on demande la sauvegarde d'un
                    // TreeNode alors qu'on a pas encore sauve les attachments
                    // (ex: durant un syncSearchEngine ou l'on demande une
                    // reindexation totale). Ceci n'est pas un probleme car
                    // les attachments seront convenablement indexes lorsqu'ils
                    // seront ajoutes
                    if (oldDoc == null && doc == null) {
                        // L'objet en attachment du TreeNode, n'est pas encore
                        // store et n'est pas dans le meme appel de la methode
                        // store. On cree donc un document Partiel (seulement
                        // constitue de l'id et de l'indexation d'arbre qui sera
                        // reutilise lors du store de reel objet
                        doc = new SolrInputDocument();
                        addToIndexDocument(doc, null, SOLR_ID, attId, true);
                    } else if (doc == null) {
                        doc = new SolrInputDocument();
                        SolrUtil.copySolrDocument(oldDoc, doc);
                        solrResource.addDoc(attId, doc);
                    }
                    doc.removeField(TREENODE_ATTACHED + treeNodeId);
                    for (Object id : treeNodeParents) {
                        doc.addField(TREENODE_ATTACHED + treeNodeId, id);
                    }
                }
            }
        }
    }

    @Override
    public WikittyQueryResult<Map<String, Object>> findAllByQuery(
            WikittyTransaction transaction, WikittyQuery query) {
        try {

            // la condition select du query, sera traite a la fin
            Select select = null;
            WikittyQuery queryWithoutSelect = query;

            if (query.isSelectQuery()) {
                select = query.getSelect();
                // copy de la query sans le select
                queryWithoutSelect = query.getWhereQuery();
                if (queryWithoutSelect.getCondition() == null) {
                    queryWithoutSelect.setCondition(new True());
                }

            }

            // Create querySolr
            WikittyQueryVisitorToSolr v = new WikittyQueryVisitorToSolr(
                    transaction, this, elementModifier, queryWithoutSelect.getWikittyFieldSearchDepth());
            queryWithoutSelect.getCondition().accept(v);
            String queryString = v.getSolrQuery();
            SolrQuery querySolr = new SolrQuery(SOLR_QUERY_PARSER + queryString);

            // Add paged
            int offset = queryWithoutSelect.getOffset();
            int limit = queryWithoutSelect.getLimit();

            if (limit == Integer.MAX_VALUE) {
                // WARNING It is necessary to substract 'start' otherwise,
                // there is a capacity overlow in solR
                limit = Integer.MAX_VALUE - offset;
            }
            querySolr.setStart(offset);
            querySolr.setRows(limit);

            // Add sorting
            List<Element> sortAscending = queryWithoutSelect.getSortAscending();
            if(sortAscending != null) {
                for (Element sort : sortAscending) {
                    String tranform = elementModifier.convertToSolr(transaction, sort);
                    tranform += WikittySolrConstant.SUFFIX_SORTABLE;
                    querySolr.addSortField(tranform, SolrQuery.ORDER.asc);
                }
            }

            List<Element> sortDescending = queryWithoutSelect.getSortDescending();
            if(sortDescending != null) {
                for (Element sort : sortDescending) {
                    String tranform = elementModifier.convertToSolr(transaction, sort);
                    tranform += WikittySolrConstant.SUFFIX_SORTABLE;
                    querySolr.addSortField(tranform, SolrQuery.ORDER.desc);
                }
            }

            // Add faceting
            boolean isFacetExtension = queryWithoutSelect.isFacetExtension();
            List<Element> facetField = queryWithoutSelect.getFacetField();
            List<FacetQuery> facetQuery = queryWithoutSelect.getFacetQuery();

            // use to map query string to criteria facet name
            Map<String, String> facetQueryToName = new HashMap<String, String>();

            if (isFacetExtension
                    || CollectionUtils.isNotEmpty(facetField)
                    || CollectionUtils.isNotEmpty(facetQuery)) {
                querySolr.setFacet(true);
                querySolr.setFacetMinCount(queryWithoutSelect.getFacetMinCount());
                querySolr.setFacetLimit(queryWithoutSelect.getFacetLimit());
                querySolr.setFacetSort(queryWithoutSelect.getFacetSort().solrValue);

                if (isFacetExtension) {
                    querySolr.addFacetField(WikittySolrConstant.SOLR_EXTENSIONS);
                }

                // field facetisation
                if (facetField != null) {
                    for (Element fqfieldName : facetField) {
                        String tranform = elementModifier.convertToSolr(transaction, fqfieldName);
                        querySolr.addFacetField(tranform);
                    }
                }

                // query facetisation
                if (facetQuery != null) {
                    for (FacetQuery facet : facetQuery) {
                        v = new WikittyQueryVisitorToSolr(
                                    transaction, this, elementModifier, 0); // pas de recherche recursive sur les champs wikitty pour les facettes
                        facet.getCondition().accept(v);
                        String queryFacet = v.getSolrQuery();
                        facetQueryToName.put(queryFacet, facet.getName());
                        querySolr.addFacetQuery(queryFacet);
                    }
                }
            }

            QueryResponse resp = SolrUtil.executeQuery(getSolrServer(), querySolr);
            SolrDocumentList solrResults = resp.getResults();

            Map<String, List<FacetTopic>> facets = new HashMap<String, List<FacetTopic>>();

            // la facet sur les extensions est directement et convenablement
            // gere comme un champs
            if (isFacetExtension || CollectionUtils.isNotEmpty(facetField)) {
                for (FacetField facet : resp.getFacetFields()) {
                    String facetName = elementModifier.convertToField(facet.getName());
                    List<FacetTopic> topics = new ArrayList<FacetTopic>();
                    if (facet.getValues() != null) {
                        for (FacetField.Count value : facet.getValues()) {
                            String topicName = value.getName();
                            int topicCount = (int) value.getCount();
                            FacetTopic topic = new FacetTopic(facetName, topicName, topicCount);
                            topics.add(topic);
                        }
                    }
                    facets.put(facetName, topics);
                }
            }

            if (CollectionUtils.isNotEmpty(facetQuery)) {
                for (Map.Entry<String, Integer> facet : resp.getFacetQuery().entrySet()) {
                    String facetName = facet.getKey();
                    // don't use contains because, map can have key with null value
                    if (null != facetQueryToName.get(facetName)) {
                        facetName = facetQueryToName.get(facetName);
                    }
                    Integer count = facet.getValue();
                    List<FacetTopic> topics = new ArrayList<FacetTopic>();
                    FacetTopic topic = new FacetTopic(facetName, facetName, count);
                    topics.add(topic);
                    facets.put(facetName, topics);
                }
            }

            // Get total num found
            int numFound = (int)resp.getResults().getNumFound();

            // collecte des id
            List<String> wikittyId = new ArrayList<String>(solrResults.size());
            for (SolrDocument doc : solrResults) {
                String id = SolrUtil.getStringFieldValue(doc, SOLR_ID);
                wikittyId.add(id);
            }

            List<Map<String, Object>> selectResult = null;
            List<Map<String, Object>> values =
                    new ArrayList<Map<String, Object>>(wikittyId.size());
            if (select == null) {
                // Extract ids
                String idTag = org.nuiton.wikitty.entities.Element.ID.getValue();
                for (Object id : wikittyId) {
                    values.add(WikittyUtil.singletonMap(idTag, id));
                }
            } else {
                // Extract data
                for (SolrDocument doc : solrResults) {
                    Map<String, Object> map = SolrUtil.convertToTypedMap(doc);
                    values.add(map);
                }

                values = select.getFunction().call(query, values);
                selectResult = values;
            }
            

            // Build paged result
            WikittyQueryResult result = new WikittyQueryResult(
                    query.getName(), offset, numFound, query, queryString,
                    values, selectResult, wikittyId, facets,
                    0, 0);

            return result;
        } catch (SolrServerException eee) {
            throw new WikittyException(String.format(
                    "Error during find of query '%s'", query), eee);
        }
    }

    /**
     * Si l'argument n'est pas un TreeNode, une exception est levee
     *
     * @param transaction wikitty transaction
     * @param wikittyId l'objet root du resultat
     * @param depth profondeur souhaite pour la recherche des fils, ou -1 pour tous
     * @param count vrai si l'on souhaite avoir le nombre d'attachment associe
     * au noeud retourne
     * @param filter filtre utilise pour compter le nombre d'attachment
     * @return all childrens count
     */
    @Override
    public WikittyQueryResultTreeNode<String> findAllChildrenCount(
            WikittyTransaction transaction,
            String wikittyId, int depth, boolean count, WikittyQuery filter) {
        try {
            WikittyQueryResultTreeNode<String> result = null;

            SolrDocument doc = SolrUtil.findById(getSolrServer(), wikittyId);
            if (doc != null) {
                // on verifie que l'argument est bien un TreeNode
                if (doc.containsKey(TREENODE_DEPTH)) {
                    
                    WikittyQueryMaker treeSearch = new WikittyQueryMaker()
                            .and().eq(TREENODE_PARENTS, wikittyId);
                    if (depth >= 0) {
                        Integer d = SolrUtil.getIntFieldValue(doc, TREENODE_DEPTH);
                        depth = d + depth;
                        if (depth < 0) {
                            // debordement, on fixe la valeur a MAX_VALUE
                            depth = Integer.MAX_VALUE;
                        }
                        treeSearch = treeSearch.bw(TREENODE_DEPTH, d, depth);
                    }
                    WikittyQuery treeQuery = treeSearch.getQuery();

                    // on a dans treeSearch uniquement le noeud passe en parametre
                    // et ses enfants jusqu'a la profondeur demandee
                    // Create querySolr
                    WikittyQueryVisitorToSolr v = new WikittyQueryVisitorToSolr(
                            transaction, this, elementModifier, 0);
                    treeQuery.getCondition().accept(v);
                    String queryString = v.getSolrQuery();
                    SolrQuery querySolr = new SolrQuery(SOLR_QUERY_PARSER + queryString);

                    QueryResponse resp = SolrUtil.executeQuery(getSolrServer(), querySolr);
                    SolrDocumentList solrResults = resp.getResults();

                    Map<String, Integer> counts = new HashMap<String, Integer>();
                    // recuperation si demande du nombre d'attachment par noeud
                    if (count) {
                        // TODO poussin 20110128 regarder si on ne peut pas
                        // restreindre les facettes aux noeuds trouve dans la recherche
                        // precedente
                        WikittyQueryMaker attQueryMaker = new WikittyQueryMaker();
                        if (filter != null) {
                            attQueryMaker.and().condition(filter.getCondition());
                        }
                        WikittyQuery attQuery =
                                attQueryMaker.eq(TREENODE_ATTACHED_ALL, wikittyId).end()
                                .setOffset(0).setLimit(0)
                                .addFacetField(new ElementField(TREENODE_ATTACHED_ALL))
                                // l'arbre peut avoir beaucoup de branche, il
                                // faut que toutes les branches aient un count
                                // des attachments
                                .setFacetLimit(Integer.MAX_VALUE);
                        WikittyQueryResult<String> attSearch =
                                findAllByQuery(transaction, attQuery).convertMapToSimple();
                        List<FacetTopic> topics = attSearch.getTopic(TREENODE_ATTACHED_ALL);
                        if (topics != null) {
                            for (FacetTopic topic : topics) {
                                String topicName = topic.getTopicName();
                                int topicCount = topic.getCount();
                                counts.put(topicName, topicCount);
                            }
                        }
                    }

                    // construction du resultat, il proceder en 2 phases car
                    // sinon si on construit un fils avant son pere, il ne sera
                    // jamais associe
                    Map<String, WikittyQueryResultTreeNode<String>> allTreeNodeResult =
                            new HashMap<String, WikittyQueryResultTreeNode<String>>();
                    // key: id de l'enfant, value: l'id du parent
                    Map<String, String> childParent = new HashMap<String, String>();
                    // construction de tous les TreeNodeResult qui permettront
                    // de construire l'arbre
                    for (SolrDocument d : solrResults) {
                        String id = SolrUtil.getStringFieldValue(d, SOLR_ID);

                        String parentId = SolrUtil.getStringFieldValue(d,
                                WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_PARENT,
                                WikittyTypes.WIKITTY);
                        int nb = counts.containsKey(id) ? counts.get(id) : 0;
                        WikittyQueryResultTreeNode<String> child =
                                new WikittyQueryResultTreeNode<String>(id, nb);
                        allTreeNodeResult.put(id, child);
                                childParent.put(id, parentId);
                    }
                    // construction de l'arbre avant de le retourner
                    for(Map.Entry<String, WikittyQueryResultTreeNode<String>> e : allTreeNodeResult.entrySet()) {
                        String id = e.getKey();
                        String parentId = childParent.get(id);
                        if (allTreeNodeResult.containsKey(parentId)) {
                            WikittyQueryResultTreeNode<String> child = e.getValue();
                            WikittyQueryResultTreeNode<String> parent = allTreeNodeResult.get(parentId);
                            if (parent != child) {
                                parent.add(child);
                            }
                        }
                    }
                    result = allTreeNodeResult.get(wikittyId);
                } else {
                    throw new WikittyException(String.format(
                            "Wikitty '%s' do not handle extension %s",
                            wikittyId, WikittyTreeNode.EXT_WIKITTYTREENODE));
                }
            }
            return result;
        } catch (SolrServerException eee) {
            throw new WikittyException("Error during find", eee);
        }

    }

    /**
     * Ajoute un champs dans un document a indexer
     */
    protected void addToIndexDocument(SolrInputDocument doc,
            WikittyTypes type, String fqfieldName, Object fieldValue,
            boolean collection) {
        if (fqfieldName.startsWith(SOLR_WIKITTY_PREFIX)) {
            doc.remove(fqfieldName);
            doc.addField(fqfieldName, fieldValue);
        } else {
            String solrFqFieldName;
            
            // add suffix like _s for string type ex: myExt.myField_s
            solrFqFieldName = SolrUtil.getSolrFieldName(fqfieldName, type);

            String solrNullFieldFqFieldName = SOLR_NULL_FIELD + fqfieldName;

            doc.remove(solrFqFieldName);          // myExt.myField_s
            doc.remove(solrNullFieldFqFieldName); // #null_field-myExt.myField

            String solrNullFieldFqFieldNameValue = "true";
            if(fieldValue != null) {
                doc.addField(solrFqFieldName, fieldValue);
                solrNullFieldFqFieldNameValue = "false";
                if (log.isTraceEnabled()) {
                    log.trace(String.format("index field '%s' with value '%s'",
                            solrFqFieldName,
                            StringUtils.abbreviate(String.valueOf(fieldValue), 50)));
                }
            }
            doc.addField(solrNullFieldFqFieldName, solrNullFieldFqFieldNameValue);
        }
    }

    /**
     * modify one field in SolrInputDocument
     * 
     * @param doc SolrInputDocument to modify
     * @param w wikitty used to find field value
     * @param fqfieldName field to index
     */
    protected void addToIndexDocument(SolrInputDocument doc, Wikitty w, String fqfieldName) {
        WikittyTypes type = null;
        Object fieldValue = null;
        boolean collection = false;
        // indique si ce champs doit etre reellement indexe ou non
        boolean mustIndex = true;
        if (SOLR_ID.equals(fqfieldName)) {
            // extra field #id
            fieldValue = w.getWikittyId();
        } else if (SOLR_EXTENSIONS.equals(fqfieldName)) {
            // extra field #extensions
            fieldValue= w.getExtensionNames();
        } else {
            // un champs normal
            FieldType fieldType = w.getFieldType(fqfieldName);
            mustIndex = fieldType.isIndexed();
            if (mustIndex) {
                type = fieldType.getType();
                collection = fieldType.isCollection();
                fieldValue = w.getFqField(fqfieldName);
            }
        }
        if (mustIndex) {
            addToIndexDocument(doc, type, fqfieldName, fieldValue, collection);
        }
    }

    /**
     * Create all index document to used to modify indexation.
     * this method don't modify index.
     * 
     * The document looks like :
     * #id : wikittyId
     * #extensions : extensionNames
     * [fieldName] : [fieldValue]
     * #null_field-[fieldname] : [true|false]
     *
     * @param w all wikitties object to index
     * @return solrInputDocument used to modify index
     */
    protected SolrInputDocument createIndexDocument(Wikitty w) {
        if (log.isTraceEnabled()) {
            log.trace(String.format("create index wikitty for '%s'", w.getWikittyId()));
        }

        SolrInputDocument doc = new SolrInputDocument();
        addToIndexDocument(doc, w, SOLR_ID);
        addToIndexDocument(doc, w, SOLR_EXTENSIONS);
        // iter sur tous les champs et pas seulement ceux qui ont une valeur
        // pour pouvoir les indexer comme des champs a null
        for (String fqfieldName : w.getAllFieldNames()) {
            addToIndexDocument(doc, w, fqfieldName);
        }
        return doc;
    }

    /**
     * Method to destroy properly the search engine. Mainly used in tests
     */
    protected void destroy() {
        getSolrServer().shutdown();
    }

    @Override
    public PagedResult<String> findAllByCriteria(WikittyTransaction transaction, Criteria criteria) {
        try {
            // Create query with restriction
            Restriction2Solr restriction2Solr = new Restriction2Solr(transaction, fieldModifier);
            String queryString = restriction2Solr.toSolr(criteria.getRestriction(), getSolrServer());
            SolrQuery query = new SolrQuery(SOLR_QUERY_PARSER + queryString);

            // Add paged
            int firstIndex = criteria.getFirstIndex();
            int endIndex = criteria.getEndIndex();

            query.setStart(firstIndex);
            int nbRows;
            if (endIndex == -1) {
                // WARNING It is necessary to substract 'start' otherwise,
                // there is a capacity overlow in solR
                nbRows = Integer.MAX_VALUE - firstIndex;
            } else {
                nbRows = endIndex - firstIndex + 1;
            }
            query.setRows(nbRows);

            // Add sorting
            List<String> sortAscending = criteria.getSortAscending();
            if(sortAscending != null) {
                for (String sort : sortAscending) {
                    String tranform = fieldModifier.convertToSolr(transaction, sort);
                    tranform += WikittySolrConstant.SUFFIX_SORTABLE;
                    query.addSortField(tranform, SolrQuery.ORDER.asc);
                }
            }

            List<String> sortDescending = criteria.getSortDescending();
            if(sortDescending != null) {
                for (String sort : sortDescending) {
                    String tranform = fieldModifier.convertToSolr(transaction, sort);
                    tranform += WikittySolrConstant.SUFFIX_SORTABLE;
                    query.addSortField(tranform, SolrQuery.ORDER.desc);
                }
            }

            // task : #1785 Add select method to allow specify wikittyId return field
            // If select is not empty, add facet on field
            String select = criteria.getSelect();
            boolean hasSelect = StringUtils.isNotEmpty(select);
            if (hasSelect) {

                // Limit on wikitty
                String selectWikitty = SolrUtil.getSolrFieldName(select, WikittyTypes.WIKITTY);
                criteria.addFacetField(selectWikitty);

                // We need no result, just facet
                criteria.setEndIndex(firstIndex);
            }

            // Add faceting
            List<String> facetField = criteria.getFacetField();
            List<Criteria> facetCriteria = criteria.getFacetCriteria();

            // use to map query string to criteria facet name
            Map<String, String> facetQueryToName = new HashMap<String, String>();

            if ((facetField != null && !facetField.isEmpty())
                    || (facetCriteria != null && !facetCriteria.isEmpty())) {
                query.setFacet(true);
                query.setFacetMinCount(criteria.getFacetMinCount());
                query.setFacetLimit(criteria.getFacetLimit());

                // field facetisation
                if (facetField != null) {
                    for (String fqfieldName : facetField) {
                        String tranform = fieldModifier.convertToSolr(transaction, fqfieldName);
                        query.addFacetField(tranform);
                    }
                }

                // query facetisation
                if (facetCriteria != null) {
                    for (Criteria facet : facetCriteria) {
                        String queryFacet =
                                restriction2Solr.toSolr(facet.getRestriction(), getSolrServer());
                        facetQueryToName.put(queryFacet, facet.getName());
                        query.addFacetQuery(queryFacet);
                    }
                }
            }

            QueryResponse resp = SolrUtil.executeQuery(getSolrServer(), query);
            SolrDocumentList solrResults = resp.getResults();

            Map<String, List<org.nuiton.wikitty.search.FacetTopic>> facets = new HashMap<String, List<org.nuiton.wikitty.search.FacetTopic>>();
            if (facetField != null && !facetField.isEmpty()) {
                for (FacetField facet : resp.getFacetFields()) {
                    String facetName = fieldModifier.convertToField(transaction, facet.getName());
                    List<org.nuiton.wikitty.search.FacetTopic> topics = new ArrayList<org.nuiton.wikitty.search.FacetTopic>();
                    if (facet.getValues() != null) {
                        for (FacetField.Count value : facet.getValues()) {
                            String topicName = value.getName();
                            int topicCount = (int) value.getCount();
                            org.nuiton.wikitty.search.FacetTopic topic = new org.nuiton.wikitty.search.FacetTopic(facetName, topicName, topicCount);
                            topics.add(topic);
                        }
                    }
                    facets.put(facetName, topics);
                }
            }
            if (facetCriteria != null && !facetCriteria.isEmpty()) {
                for (Map.Entry<String, Integer> facet : resp.getFacetQuery().entrySet()) {
                    String facetName = facet.getKey();
                    // don't use contains because, map can have key with null value
                    if (null != facetQueryToName.get(facetName)) {
                        facetName = facetQueryToName.get(facetName);
                    }
                    Integer count = facet.getValue();
                    List<org.nuiton.wikitty.search.FacetTopic> topics = new ArrayList<org.nuiton.wikitty.search.FacetTopic>();
                    org.nuiton.wikitty.search.FacetTopic topic = new org.nuiton.wikitty.search.FacetTopic(facetName, facetName, count);
                    topics.add(topic);
                    facets.put(facetName, topics);
                }
            }

            List<String> ids;
            int numFound;
            if (hasSelect) {

                // Get select facet
                List<org.nuiton.wikitty.search.FacetTopic> facetTopics = facets.get(select);

                // Remove this one
                facets.remove(select);

                // Total found
                numFound = facetTopics.size();

                // Extract ids starting on firstIndex
                ids = new ArrayList<String>();

                // If all must be return, use nbRows
                endIndex = (endIndex == -1 ? nbRows : endIndex);
                for (int i = firstIndex;i <= endIndex && i < numFound;i++) {

                    org.nuiton.wikitty.search.FacetTopic topic = facetTopics.get(i);
                    ids.add(topic.getTopicName());
                }

            } else {

                // Extract ids
                ids = new ArrayList<String>(solrResults.size());
                for (SolrDocument doc : solrResults) {

                    String id = SolrUtil.getStringFieldValue(doc, SOLR_ID);
                    ids.add(id);
                }

                // Get total num found
                numFound = (int)resp.getResults().getNumFound();
            }

            // Build paged result
            PagedResult<String> result = new PagedResult<String>(
                    criteria.getName(),
                    firstIndex, numFound, queryString, facets, ids);

            return result;
        } catch (SolrServerException eee) {
            throw new WikittyException(String.format(
                    "Error during find of criteria '%s'", criteria), eee);
        }
    }

    /**
     * Si l'argument n'est pas un TreeNode, une exception est levee
     *
     * @param transaction wikitty transaction
     * @param wikittyId l'objet root du resultat
     * @param depth profondeur souhaite pour la recherche des fils
     * @param count vrai si l'on souhaite avoir le nombre d'attachment associe
     * au noeud retourne
     * @param filter filtre utilise pour compter le nombre d'attachment
     * @return all childrens count
     */
    @Override
    public TreeNodeResult<String> findAllChildrenCount(
            WikittyTransaction transaction,
            String wikittyId, int depth, boolean count, Criteria filter) {
        try {
            TreeNodeResult<String> result = null;

            SolrDocument doc = SolrUtil.findById(getSolrServer(), wikittyId);
            if (doc != null) {
                // on verifie que l'argument est bien un TreeNode
                if (doc.containsKey(TREENODE_DEPTH)) {

                    Search treeSearch = Search.query().and().eq(TREENODE_PARENTS, wikittyId);
                    if (depth >= 0) {
                        Integer d = SolrUtil.getIntFieldValue(doc, TREENODE_DEPTH);
                        treeSearch = treeSearch.bw(TREENODE_DEPTH,
                                String.valueOf(d),
                                String.valueOf(d + depth));
                    }
                    Criteria treeCriteria = treeSearch.criteria();

                    // on a dans treeSearch uniquement le noeud passe en parametre
                    // et ses enfants jusqu'a la profondeur demandee
                    Restriction2Solr restriction2Solr =
                            new Restriction2Solr(transaction, fieldModifier);
                    String queryString = restriction2Solr.toSolr(
                            treeCriteria.getRestriction(), getSolrServer());
                    SolrQuery query = new SolrQuery(SOLR_QUERY_PARSER + queryString);
                    QueryResponse resp = SolrUtil.executeQuery(getSolrServer(), query);
                    SolrDocumentList solrResults = resp.getResults();

                    Map<String, Integer> counts = new HashMap<String, Integer>();
                    // recuperation si demande du nombre d'attachment par noeud
                    if (count) {
                        // TODO poussin 20110128 regarder si on ne peut pas
                        // restreindre les facettes aux noeuds trouve dans la recherche
                        // precedente
                        Criteria attCriteria = Search.query(filter).eq(
                                TREENODE_ATTACHED_ALL, wikittyId).criteria()
                                .setFirstIndex(0).setEndIndex(0)
                                .addFacetField(TREENODE_ATTACHED_ALL);
                        PagedResult<String> attSearch =
                                findAllByCriteria(transaction, attCriteria);
                        List<org.nuiton.wikitty.search.FacetTopic> topics = attSearch.getTopic(TREENODE_ATTACHED_ALL);
                        if (topics != null) {
                            for (org.nuiton.wikitty.search.FacetTopic topic : topics) {
                                String topicName = topic.getTopicName();
                                int topicCount = topic.getCount();
                                counts.put(topicName, topicCount);
                            }
                        }
                    }

                    // construction du resultat, il proceder en 2 phases car
                    // sinon si on construit un fils avant son pere, il ne sera
                    // jamais associe
                    Map<String, TreeNodeResult<String>> allTreeNodeResult =
                            new HashMap<String, TreeNodeResult<String>>();
                    // key: id de l'enfant, value: l'id du parent
                    Map<String, String> childParent = new HashMap<String, String>();
                    // construction de tous les TreeNodeResult qui permettront
                    // de construire l'arbre
                    for (SolrDocument d : solrResults) {
                        String id = SolrUtil.getStringFieldValue(d, SOLR_ID);

                        String parentId = SolrUtil.getStringFieldValue(d,
                                WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_PARENT,
                                WikittyTypes.WIKITTY);
                        int nb = counts.containsKey(id) ? counts.get(id) : 0;
                        TreeNodeResult<String> child = new TreeNodeResult<String>(id, nb);
                        allTreeNodeResult.put(id, child);
                                childParent.put(id, parentId);
                    }
                    // construction de l'arbre avant de le retourner
                    for(Map.Entry<String, TreeNodeResult<String>> e : allTreeNodeResult.entrySet()) {
                        String id = e.getKey();
                        String parentId = childParent.get(id);
                        if (allTreeNodeResult.containsKey(parentId)) {
                            TreeNodeResult<String> child = e.getValue();
                            TreeNodeResult<String> parent = allTreeNodeResult.get(parentId);
                            if (parent != child) {
                                parent.add(child);
                            }
                        }
                    }
                    result = allTreeNodeResult.get(wikittyId);
                } else {
                    throw new WikittyException(String.format(
                            "Wikitty '%s' do not handle extension %s",
                            wikittyId, WikittyTreeNode.EXT_WIKITTYTREENODE));
                }
            }
            return result;
        } catch (SolrServerException eee) {
            throw new WikittyException("Error during find", eee);
        }

    }

}
