/*
 * #%L
 * Wikitty :: wikitty-solr
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.storage.solr;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.WikittyTypes;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyTypes;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.operators.Element;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.storage.WikittyExtensionStorage;

import static org.nuiton.wikitty.storage.solr.WikittySolrConstant.*;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 * @deprecated since 3.3 use new {@link ElementModifier} with new query API
 */
@Deprecated
public class FieldModifier {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(FieldModifier.class);

    protected WikittyExtensionStorage extensionStorage;

    public FieldModifier(WikittyExtensionStorage extensionStorage) {
        this.extensionStorage = extensionStorage;
    }

    /**
     * Different cas d'entree
     * <ul>
     * <li> monext.monfield</li>
     * <li> *.monfield</li>
     * <li> monext.monfield.NUMERIC</li>
     * <li> *.monfield.NUMERIC</li>
     * </ul>
     *
     * @param transaction
     * @param fqfieldname
     * @return
     */
    public String convertToSolr(WikittyTransaction transaction, String fqfieldname) {
        String result = fqfieldname;
        String[] searchField = fqfieldname.split(WikittyUtil.FQ_FIELD_NAME_SEPARATOR_REGEX);

        if (Element.ELT_EXTENSION.equals(fqfieldname)) {
            result = SOLR_EXTENSIONS;

        } else if (Element.ELT_ID.equals(fqfieldname)) {
            result = SOLR_ID;

        } else if (searchField.length >= 2) {
            String extName = searchField[0];
            String fieldName = searchField[1];

            if (Criteria.ALL_EXTENSIONS.equals(extName)) {
                result = SOLR_ALL_EXTENSIONS
                        + WikittyUtil.FQ_FIELD_NAME_SEPARATOR + fieldName;
                // en #all le seul moyen d'avoir le type est qu'il soit donne dans le champs, sinon on ne peut pas le
                // trouver
                if (searchField.length >= 3) {
                    // On passe ici, si on indique dans le champs son type (ex: *.monfield.NUMERIC)
                    // utile pour force la recherche sur les bons champs lorsqu'on demande une recherche sur * == #all
                    String fieldNameType = searchField[2];
                    WikittyTypes type = WikittyTypes.valueOf(fieldNameType);
                    // Ajout du pattern solr pour discriminer le champs ex : _s, _dt, _w, ...
//        FIXME REMOVE IT if search on multivalued work with new hack (specific sortable field
//                    result = SolrUtil.getSolrCollectionFieldName(result, type);
                    result = SolrUtil.getSolrFieldName(result, type);
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("Search on multi extentions (*) without field"
                                + " type, fallback search in fulltext");
                    }
                    result = SOLR_FULLTEXT_ALL_EXTENSIONS
                        + WikittyUtil.FQ_FIELD_NAME_SEPARATOR + fieldName;
                }
            } else {
                result = extName + WikittyUtil.FQ_FIELD_NAME_SEPARATOR + fieldName;

                // Ajout du pattern solr pour discriminer le champs ex : _s, _sm, _wm ...
                // Search type of field in extension
                String version =
                        extensionStorage.getLastVersion(transaction, extName);
                if (version != null) { // not valid extension if version == null
                    WikittyExtension ext = extensionStorage.restore(
                            transaction, extName, version);
                    FieldType fieldType = ext.getFieldType(fieldName);
                    if (log.isDebugEnabled()) {
                        log.debug(ext.toDefinition() + " for " + fieldName);
                    }
                    if (fieldType != null) { // type can be null if extension version differ
                        WikittyTypes type = fieldType.getType();
//        FIXME REMOVE IT if search on multivalued work with new hack (specific sortable field
//                        if (fieldType.isCollection()) {
//                            result = SolrUtil.getSolrCollectionFieldName(result, type);
//                        } else {
                            result = SolrUtil.getSolrFieldName(result, type);
//                        }
                    }
                }
            }
        }

        return result;
    }
    
    public String convertToField(WikittyTransaction transaction, String solrName) {
        String fieldName = solrName.replaceAll(
                "(" + SUFFIX_BINARY + "$)"
                + "|(" + SUFFIX_BOOLEAN + "$)"
                + "|(" + SUFFIX_DATE + "$)"
                + "|(" + SUFFIX_STRING + "$)"
                + "|(" + SUFFIX_WIKITTY + "$)"
                + "|(" + SUFFIX_NUMERIC + "$)"
//        FIXME REMOVE IT if search on multivalued work with new hack (specific sortable field
//                + "|(" + SUFFIX_BINARY_MULTIVALUED + "$)"
//                + "|(" + SUFFIX_BOOLEAN_MULTIVALUED + "$)"
//                + "|(" + SUFFIX_DATE_MULTIVALUED + "$)"
//                + "|(" + SUFFIX_STRING_MULTIVALUED + "$)"
//                + "|(" + SUFFIX_WIKITTY_MULTIVALUED + "$)"
//                + "|(" + SUFFIX_NUMERIC_MULTIVALUED + "$)"
                + "|(" + SUFFIX_SORTABLE + "$)"
                , "");
        if (SOLR_EXTENSIONS.equals(fieldName)) {
            fieldName = Element.ELT_EXTENSION;
        }
        return fieldName;
    }
}
