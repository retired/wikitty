/*
 * #%L
 * Wikitty :: wikitty-solr
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.storage.solr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.query.WikittyQueryVisitor;
import org.nuiton.wikitty.query.conditions.And;
import org.nuiton.wikitty.query.conditions.Between;
import org.nuiton.wikitty.query.conditions.Condition;
import org.nuiton.wikitty.query.conditions.ConditionValue;
import org.nuiton.wikitty.query.conditions.ConditionValueString;
import org.nuiton.wikitty.query.conditions.ContainsAll;
import org.nuiton.wikitty.query.conditions.ContainsOne;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.conditions.Equals;
import org.nuiton.wikitty.query.conditions.False;
import org.nuiton.wikitty.query.conditions.Greater;
import org.nuiton.wikitty.query.conditions.GreaterOrEquals;
import org.nuiton.wikitty.query.conditions.Keyword;
import org.nuiton.wikitty.query.conditions.Less;
import org.nuiton.wikitty.query.conditions.LessOrEquals;
import org.nuiton.wikitty.query.conditions.Like;
import org.nuiton.wikitty.query.conditions.Not;
import org.nuiton.wikitty.query.conditions.NotEquals;
import org.nuiton.wikitty.query.conditions.NotNull;
import org.nuiton.wikitty.query.conditions.Null;
import org.nuiton.wikitty.query.conditions.Or;
import org.nuiton.wikitty.query.conditions.Select;
import org.nuiton.wikitty.query.conditions.True;
import org.nuiton.wikitty.query.conditions.Unlike;
import org.nuiton.wikitty.query.function.FunctionValue;
import org.nuiton.wikitty.query.function.WikittyQueryFunction;
import org.nuiton.wikitty.services.WikittyTransaction;

/**
 * Converti une {@link Condition} en une requete Solr, une fois la condition
 * converti on peut la recuperer par {@link #getSolrQuery()}.
 *
 * Cette objet ne peut etre utiliser qu'un seul fois, il faut creer un nouveau
 * visiteur pour chaque convertion.
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyQueryVisitorToSolr extends WikittyQueryVisitor {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyQueryVisitorToSolr.class);

    protected WikittyTransaction tx;
    protected WikittySearchEngineSolr searchEngine;
    protected ElementModifier elementModifier;

    protected String solrQuery = "";
    /** indique la profondeur de recherche en suivant les liens des champs de type wikitty */
    protected int wikittyFieldSearchDepth;

    public String getSolrQuery() {
        return solrQuery;
    }

    public WikittyQueryVisitorToSolr(WikittyTransaction tx,
            WikittySearchEngineSolr solrServer, ElementModifier elementModifier,
            int wikittyFieldSearchDepth) {
        this.tx = tx;
        this.searchEngine = solrServer;
        this.elementModifier = elementModifier;
        this.wikittyFieldSearchDepth = wikittyFieldSearchDepth;
    }

    /**
     * Impossible de trouver le bon analyser Solr pour la recherche fulltext.
     * Solr meme si on lui dit de mettre en minuscule et d'enlever les accents
     * le fait pour l'indexation, mais pas la recherche :(. On le fait donc pour
     * lui ici tant que le bon analyser n'est pas trouve
     *
     * Apres recherche il semble que ce soit normal :(
     * sur la page
     * http://wiki.apache.org/solr/AnalyzersTokenizersTokenFilters#Analyzers
     * on peut lire
     * <pre>
     * On wildcard and fuzzy searches, no text analysis is performed on the search word.
     * </pre>
     * Donc dès qu'on met des '*' les transformations ne sont plus faites :(
     *
     * plus d'explication:
     * <li> http://java.dzone.com/articles/what%E2%80%99s-lowercasing-wildcard
     * <li> http://wiki.apache.org/solr/MultitermQueryAnalysis
     *
     * @param element2solr
     * @param value
     * @return
     */
    private String fixSolrBug(String element2solr, String value) {
        String result = value;
//        if (element2solr.equals(WikittySolrConstant.SOLR_FULLTEXT) ||
//                element2solr.startsWith(WikittySolrConstant.SOLR_FULLTEXT_ALL_EXTENSIONS) ||
//                element2solr.endsWith(WikittySolrConstant.SUFFIX_STRING_FULLTEXT)||
//                element2solr.endsWith(WikittySolrConstant.SUFFIX_STRING_LOWERCASE)) { // is string
//            // TODO poussin 20120107 impossible de comprendre pourquoi il faut force
//            // la chaine en lower case, ca devrait etre le role de solr/lucene via
//            // la description du schema de mettre la chaine dans la bonne forme
//            // pour la recheche en fonction du type du champs souhaite (ici fulltext)
//            // tant que ca marche pas on force a la main
//            // idem pour les accents :(
//            result = StringUtils.stripAccents(result);
//            result = result.toLowerCase();
//        }
        return result;
    }

    private String element2solr(Element element) {
        String result = elementModifier.convertToSolr(tx, element);
        return result;
    }

    private String escape2solr(String value) {
        String result;
        if (StringUtils.isNotEmpty(value)) {
            final String LUCENE_REPLACE_PATTERN = "\\+" + "|-" + "|&&" + "|\\|"
                    + "|!" + "|\\(|\\)" + "|\\[|\\]" + "|\\{|\\}" + "|\"" + "|:";
            result = value.replaceAll(LUCENE_REPLACE_PATTERN, "\\\\$0");
            if (result.contains(" ")) {
                result = "\"" + result + "\"";
            }
        } else {
//            throw new WikittyException("Parse error, value must be not empty");
            result = "\"\"";
        }

        return result;
    }

    protected List<String> evalConditionValueAsList(List<ConditionValue> o) {
        List<String> result = new ArrayList<String>(o.size());
        for (ConditionValue c : o) {
            result.addAll(evalConditionValueAsList(c));
        }
        return result;
    }

    protected List<String> evalConditionValueAsList(ConditionValue o) {
        List<String> result = new ArrayList<String>();
        if (o instanceof Select) {
            WikittyQuery query = new WikittyQuery(o);
            // eval select
            WikittyQueryResult<String> selectResult =
                    searchEngine.findAllByQuery(tx, query).convertMapToSimpleString();
            for (String s : selectResult) {
                result.add(escape2solr(s));
            }
        } else if (o instanceof ConditionValueString) {
            result.add(escape2solr(((ConditionValueString)o).getValue()));
        } else {
            throw new WikittyException(String.format(
                    "ConditionValue type unsupported %s",
                    ClassUtils.getShortCanonicalName(o, "null")));
        }
        return result;

    }
    protected String evalConditionValue(ConditionValue o) {
        String result;
        if (o instanceof Select) {
            WikittyQuery query = new WikittyQuery(o).setLimit(WikittyQuery.MAX);
            // eval select
            WikittyQueryResult<String> selectResult =
                    searchEngine.findAllByQuery(tx, query).convertMapToSimpleString();
            if (selectResult.size() == 0) {
                    throw new WikittyException(String.format(
                            "Select return no result query was '%s' transformed to '%s'",
                            o.toString(),
                            selectResult.getQueryString()));
                } else if (selectResult.size() > 1) {
                    if (log.isWarnEnabled()) {
                        log.warn(String.format(
                                "Select return more than one result, only first"
                                + " is used. Query was '%s' transformed to '%s'",
                                o.toString(),
                                selectResult.getQueryString()));
                    }
                }
            result = selectResult.peek();
        } else if (o instanceof ConditionValueString) {
            result = ((ConditionValueString)o).getValue();
        } else {
            throw new WikittyException(String.format(
                    "ConditionValue type unsupported %s",
                    ClassUtils.getShortCanonicalName(o, "null")));
        }
        result = escape2solr(result);
        return result;
    }

    /**
     * Si element2solr est de type Wikitty, alors on fait une recherche pour
     * recuperer tous les ids des objets qui contiennent dans 1 de leur champs
     * une valeur de l'argument values.
     *
     * La chaine retournee est (query OR element2solr:(valeur retournee par la recherche concatenee par de OR))
     * ou directement query si element2solr n'est pas de type Wikitty
     *
     * @param query la requete initial de l'utilisateur
     * @param element2solr l'element sur lequel la requete est faite
     * @param operator l'operateur pour concatener les sous requetes
     * @param values les differentes valeurs a rechercher
     * @return
     */
    protected String deepSearchOnWikittyField(String query, String element2solr, String operator, Collection<String> values) {
        if (log.isDebugEnabled()) {
          log.debug("enter deepSearchOnWikittyField query: "+ query+ " element2solr:" + element2solr + " values:" + values);
        }
        if ( wikittyFieldSearchDepth > 0 ) {
            // La recherche ce fait sur un champs de type wikitty, vu qu'on ne
            // stocke que des id, si l'utilisateur passe autre chose, la recherche
            // echouera. Ici on aide donc l'utilisateur en faisant pour lui
            // la recherche sur les wikitties pour recuperer les ids qui pourraient
            // convenir et faire la recherche sur ces ids
            if (element2solr == null
                    || element2solr.endsWith(WikittySolrConstant.SUFFIX_WIKITTY)
                    || element2solr.startsWith(WikittySolrConstant.SOLR_ALL_EXTENSIONS)
                    || element2solr.startsWith(WikittySolrConstant.SOLR_FULLTEXT_ALL_EXTENSIONS)
                    ) {

                WikittyQuery sub = new WikittyQueryMaker()
                        .containsOne(Element.ALL_FIELD, values)
                        .end().setLimit(WikittyQuery.MAX)
                        .setWikittyFieldSearchDepth(wikittyFieldSearchDepth - 1);
                // eval sub
                WikittyQueryResult<String> subResult =
                        searchEngine.findAllByQuery(tx, sub).convertMapToSimpleString();

                // si on a bien des resultat en plus on modifie la requete
                if (subResult.size() > 0) {
                    query = "(" + query + " " + operator + " ";
                    // on ajoute les * car sinon pour la recherche fulltext
                    // solr modifie la requete et fini par retourne le monde
                    // entier lorsqu'on passe un id. Cela vient peut-etre de
                    // solr.WordDelimiterFilterFactory qui a un splitOnNumerics="1"
                    String subResultString = "*" + StringUtils.join(subResult, "* OR *") + "*";
                    if (element2solr == null) {
                        query += "(" + subResultString + ")";
                    } else {
                        query += element2solr + ":(" + subResultString + ")";
                    }
                    query += ")";
                }

//                String subResult = "";
//                try {
//                    // on fait la recherhce le nombre de recursion demande
//                    Collection<String> currentValue = values;
//                    for (int i=0; i<wikittyFieldSearchDepth; i++) {
//                        System.out.println("## boucle " + i + " currentValue:" + currentValue);
//                        String queryString = "";
//                        String sep = "";
//                        for(String e : currentValue) {
//                            queryString += sep + e;
//                            sep = " OR ";
//                        }
//                        SolrQuery querySolr = new SolrQuery(
//                                WikittySolrConstant.SOLR_QUERY_PARSER + queryString);
//                        QueryResponse resp = SolrUtil.executeQuery(
//                                searchEngine.getSolrServer(), querySolr);
//                        SolrDocumentList solrResults = resp.getResults();
//                        // Extract ids
//                        currentValue = new LinkedList<String>();
//                        for (SolrDocument doc : solrResults) {
//                            String id = SolrUtil.getStringFieldValue(doc, WikittySolrConstant.SOLR_ID);
//                            currentValue.add(id);
//                        }
//                    }
//                    // a la suite de la derniere recursion on transforme en requete texte
//                    String sep = "";
//                    for (String id : currentValue) {
//                        subResult += sep + "*" + escape2solr(id) + "*";
//                        sep = " OR ";
//                    }
//
//                } catch (Exception eee) {
//                    log.info("Can't make deep search on wikitty field, continue without", eee);
//                }
//
//                // si on a bien des resultat en plus on modifie la requete
//                if (StringUtils.isNotBlank(subResult)) {
//                    query = "(" + query + " " + operator + " ";
//                    if (element2solr == null) {
//                        query += "(" + subResult + ")";
//                    } else {
//                        query += element2solr + ":(" + subResult + ")";
//                    }
//                    query += ")";
//                }
            }
        }
        if(log.isDebugEnabled()) {
            log.debug("exit deepSearchOnWikittyField query: "+ query);
        }
        return query;
    }
    /**
     *
     * @see #deepSearchOnWikittyField(String, String, String, java.util.Collection)
     */
    protected String deepSearchOnWikittyField(String query, String element2solr, String operateur, String value) {
        return deepSearchOnWikittyField(query, element2solr, operateur, Collections.singleton(value));
    }

    @Override
    public void visit(ConditionValueString o) {
        // do nothing
    }

    @Override
    public boolean visitEnter(WikittyQuery o) {
        // do nothing
        return true;
    }

    @Override
    public void visitLeave(WikittyQuery o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(And o) {
        solrQuery += "(";
        return true;
    }

    @Override
    public boolean visitMiddle(And o) {
        solrQuery += " AND ";
        return true;
    }

    @Override
    public void visitLeave(And o, boolean enterResult) {
        solrQuery += ")";
    }

    @Override
    public boolean visitEnter(Or o) {
        solrQuery += "(";
        return true;
    }

    @Override
    public boolean visitMiddle(Or o) {
        solrQuery += " OR ";
        return true;
    }

    @Override
    public void visitLeave(Or o, boolean enterResult) {
        solrQuery += ")";
    }

    @Override
    public boolean visitEnter(Select o) {
        // nothing to do
        // select is done in WikittySearchEngineSolr method
        return true;
    }

    @Override
    public boolean visitMiddle(Select o) {
        // nothing to do
        // select is done in WikittySearchEngineSolr method
        return true;
    }

    @Override
    public void visitLeave(Select o, boolean enterOrMiddleResult) {
        // nothing to do
    }

    @Override
    public boolean visitEnter(Not o) {
        solrQuery += "NOT(";
        return true;
    }

    @Override
    public void visitLeave(Not o, boolean enterResult) {
        solrQuery += ")";
    }

    @Override
    public boolean visitEnter(Between o) {
        String element2solr = element2solr(o.getElement());
        String min = evalConditionValue(o.getMin());
        String max = evalConditionValue(o.getMax());

        min = fixSolrBug(element2solr, min);
        max = fixSolrBug(element2solr, max);

        solrQuery += element2solr + ":[" + min + " TO " + max + "]";
        return false;
    }

    @Override
    public boolean visitMiddle(Between o) {
        // to nothing
        return true;
    }

    @Override
    public void visitLeave(Between o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(ContainsAll o) {
        List<String> values = evalConditionValueAsList(o.getValues());
        if (values.size() == 0) {
            solrQuery += WikittySolrConstant.SOLR_FALSE; // false / rien
        } else {
            String element2solr = element2solr(o.getElement());

            solrQuery += element2solr + ":(";

            String sep = "";
            for(String e : values) {
                solrQuery += sep + fixSolrBug(element2solr, e);
                sep = " AND ";
            }
            solrQuery += ")";
        }
        return false;
    }

    @Override
    public boolean visitMiddle(ContainsAll o) {
        // do nothing
        return false;
    }

    @Override
    public void visitLeave(ContainsAll o, boolean enterOrMiddleResult) {
            // do nothing
    }

    @Override
    public boolean visitEnter(ContainsOne o) {
        List<String> values = evalConditionValueAsList(o.getValues());
        if (values.size() == 0) {
            solrQuery += WikittySolrConstant.SOLR_FALSE; // false / rien
        } else {
            String element2solr = element2solr(o.getElement());


            // query contient ce qu'il faut ajouter a solrQuery
            // on pre-calcul au cas ou il y aurait d'autres traitements a faire
            // (pour les champs de type wikitty)
            String query = element2solr + ":(";
            String sep = "";
            for(String e : values) {
                query += sep + fixSolrBug(element2solr, e);
                sep = " OR ";
            }
            query += ")";

            solrQuery += deepSearchOnWikittyField(query, element2solr, "OR", values);

//            solrQuery += element2solr + ":(";
//            String sep = "";
//            for(String e : values) {
//                solrQuery += sep + fixSolrBug(element2solr, e);
//                sep = " OR ";
//            }
//            solrQuery += ")";
        }
        return false;
    }

    @Override
    public boolean visitMiddle(ContainsOne o) {
        // do nothing
        return false;
    }

    @Override
    public void visitLeave(ContainsOne o, boolean enterOrMiddleResult) {
        // do nothing
    }
    
    @Override
    public boolean visitEnter(Equals o) {
        String element2solr = element2solr(o.getElement());
        String value = evalConditionValue(o.getValue());

        if (element2solr.endsWith(WikittySolrConstant.SUFFIX_STRING)) { // is string
            if (o.isIgnoreCaseAndAccent()) {
                element2solr += WikittySolrConstant.SUFFIX_STRING_LOWERCASE;
            }
        }

        value = fixSolrBug(element2solr, value);

        String query = element2solr + ":" + value;
        solrQuery += deepSearchOnWikittyField(query, element2solr, "OR", value);

//        solrQuery += element2solr + ":" + value;
        
        return false;
    }

    @Override
    public void visitLeave(Equals o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(NotEquals o) {
        String element2solr = element2solr(o.getElement());
        String value = evalConditionValue(o.getValue());

        if (element2solr.endsWith(WikittySolrConstant.SUFFIX_STRING)) { // is string
            if (o.isIgnoreCaseAndAccent()) {
                element2solr += WikittySolrConstant.SUFFIX_STRING_LOWERCASE;
            }
        }
        
        value= fixSolrBug(element2solr, value);

        String query = "-" + element2solr + ":" + value;
        solrQuery += deepSearchOnWikittyField(query, element2solr, "AND NOT", value);
//        solrQuery += "-" + element2solr + ":" + value;
        
        return false;
    }

    @Override
    public void visitLeave(NotEquals o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(Greater o) {
        String element2solr = element2solr(o.getElement());
        String value = evalConditionValue(o.getValue());
        
        value = fixSolrBug(element2solr, value);
        
        solrQuery += element2solr + ":{" + value + " TO *}";
        return false;
    }

    @Override
    public void visitLeave(Greater o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(GreaterOrEquals o) {
        String element2solr = element2solr(o.getElement());
        String value = evalConditionValue(o.getValue());

        value = fixSolrBug(element2solr, value);

        solrQuery += element2solr + ":[" + value + " TO *]";
        return false;
    }

    @Override
    public void visitLeave(GreaterOrEquals o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(Keyword o) {
        String value = evalConditionValue(o.getValue());

        value = fixSolrBug(WikittySolrConstant.SOLR_FULLTEXT, value);

        // pour keyword on ajout automatiquement les *, sinon il faut faire un like
        value = "*" + value + "*";
        String query = value;
        solrQuery += deepSearchOnWikittyField(query, null, "OR", value);
//        solrQuery += value;
        return false;
    }

    @Override
    public void visitLeave(Keyword o, boolean enterOrMiddleResult) {
        // do nothing
    }
    
    @Override
    public boolean visitEnter(Less o) {
        String element2solr = element2solr(o.getElement());
        String value = evalConditionValue(o.getValue());

        value = fixSolrBug(element2solr, value);

        solrQuery += element2solr + ":{* TO " + value + "}";
        return false;
    }

    @Override
    public void visitLeave(Less o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(LessOrEquals o) {
        String element2solr = element2solr(o.getElement());
        String value = evalConditionValue(o.getValue());

        value = fixSolrBug(element2solr, value);

        solrQuery += element2solr + ":[* TO " + value + "]";
        return false;
    }

    @Override
    public void visitLeave(LessOrEquals o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(Like o) {
        String element2solr = element2solr(o.getElement());
        String value = evalConditionValue(o.getValue());

        if (element2solr.endsWith(WikittySolrConstant.SUFFIX_STRING)) { // is string
            element2solr += WikittySolrConstant.SUFFIX_STRING_FULLTEXT;
        }

        value = fixSolrBug(element2solr, value);

        String query = element2solr + ":" + value;
        solrQuery += deepSearchOnWikittyField(query, element2solr, "OR", value);
//        solrQuery += element2solr + ":" + value;
        return false;
    }

    @Override
    public void visitLeave(Like o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(Unlike o) {
        String element2solr = element2solr(o.getElement());
        String value = evalConditionValue(o.getValue());

        if (element2solr.endsWith(WikittySolrConstant.SUFFIX_STRING)) { // is string
            element2solr += WikittySolrConstant.SUFFIX_STRING_FULLTEXT;
        }

        value = fixSolrBug(element2solr, value);
        String query = "-" + element2solr + ":" + value;
        solrQuery += deepSearchOnWikittyField(query, element2solr, "AND NOT", value);
//        solrQuery += "-" + element2solr + ":" + value;
        return false;
    }

    @Override
    public void visitLeave(Unlike o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public void visit(Null o) {
        solrQuery += WikittySolrConstant.SOLR_NULL_FIELD
                + o.getElement().getValue() + ":true";
    }

    @Override
    public void visit(NotNull o) {
        solrQuery += WikittySolrConstant.SOLR_NULL_FIELD
                + o.getElement().getValue() + ":false";
    }

    @Override
    public void visit(True o) {
        solrQuery += "( *:* )";
    }

    @Override
    public void visit(False o) {
        solrQuery += WikittySolrConstant.SOLR_FALSE;
    }

    @Override
    public boolean visitEnter(WikittyQueryFunction function) {
        // do nothing
        return true;
    }

    @Override
    public boolean visitMiddle(WikittyQueryFunction function) {
        // do nothing
        return true;
    }

    @Override
    public void visitLeave(WikittyQueryFunction function, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public void visit(FunctionValue function) {
                // do nothing
    }

    @Override
    public void defaultVisit(Object o) {
        throw new UnsupportedOperationException("Not supported:" + o.getClass());
    }

    @Override
    public boolean defaultVisitEnter(Object o) {
        throw new UnsupportedOperationException("Not supported:" + o.getClass());
    }

    @Override
    public boolean defaultVisitMiddle(Object o) {
        throw new UnsupportedOperationException("Not supported:" + o.getClass());
    }

    @Override
    public void defaultVisitLeave(Object o, boolean enterResult) {
        throw new UnsupportedOperationException("Not supported:" + o.getClass());
    }
}
