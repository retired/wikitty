/*
 * #%L
 * Wikitty :: wikitty-solr
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.storage.solr;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.embedded.EmbeddedSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.core.CoreContainer;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.TimeLog;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.WikittyTreeNode;
import org.nuiton.wikitty.entities.WikittyTypes;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SolrUtil implements WikittySolrConstant {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(SolrUtil.class);
    final static private TimeLog timeLog = new TimeLog(SolrUtil.class);

    /**
     * Cache permettant de ne pas instancier 2 solr serveur qui travaillerait
     * avec les memes fichiers sur le disque.
     */
    static Map<String, SolrServer> solrServerCache = new HashMap<String, SolrServer>();

    public static SolrServer getSolrServer(ApplicationConfig config) {

        boolean removeSystemSolrDirFactoryKey = false;
        String solrDirFactoryKey = null;
        boolean removeSystemSolrDataDirKey = false;
        String solrDataDirKey = null;
        
        String solrDataDir = null;

        // init system env solr.data.dir
        if (config != null) {
            // choix du storage (file or Ram)
            solrDirFactoryKey =
                    WikittyConfigOption.WIKITTY_SEARCHENGINE_SOLR_DIRECTORY_FACTORY.getKey();
            String solrDirFactory = config.getOption(solrDirFactoryKey);
            if (solrDirFactory != null) {
                removeSystemSolrDirFactoryKey = null == System.getProperty(solrDirFactoryKey);
                System.setProperty(solrDirFactoryKey, solrDirFactory);
            }

            // on utilise le directory que si on est pas en Ram
            if (solrDirFactory != null && !solrDirFactory.contains("RAMDirectoryFactory")) {
                solrDataDirKey =
                        WikittyConfigOption.WIKITTY_SEARCHENGINE_SOLR_DIRECTORY_DATA.getKey();
                solrDataDir = config.getOption(solrDataDirKey);
                // make sure that dir exists
                if (solrDataDir != null) {
                    File file = new File(solrDataDir);
                    if (!file.exists() && !file.mkdirs()) {
                        throw new WikittyException(String.format(
                                "Can't create directory '%s'", solrDataDir));
                    }
                    log.info(String.format("Use SolR directory '%s'", solrDataDir));
                    removeSystemSolrDataDirKey = null == System.getProperty(solrDataDirKey);
                    System.setProperty(solrDataDirKey, solrDataDir);
                }
            }
        }

        SolrServer result = solrServerCache.get(solrDataDir);
        if (result == null) {
            try {
                CoreContainer.Initializer initializer = new CoreContainer.Initializer();
                CoreContainer solrCore = initializer.initialize();
                result = new EmbeddedSolrServer(solrCore, "");
                if (solrDataDir != null) {
                    solrServerCache.put(solrDataDir, result);
                }
            } catch (Exception eee) {
                throw new WikittyException("SolR initialization error", eee);
            }
        } else {
            log.debug("re-use already instanciate SolrServer for " + solrDataDir);
        }

        if (removeSystemSolrDirFactoryKey) {
            System.clearProperty(solrDirFactoryKey);
        }
        if (removeSystemSolrDataDirKey) {
            System.clearProperty(solrDataDirKey);
        }

        return result;
    }

    /**
     * Recherche tous les TreeNode auquel appartient en Attachment l'objet passe
     * en parametre
     * 
     * @param doc le document representant l'objet
     * @return attached treeNodes
     * @since 3.1
     */
    static public Set<String> getAttachedTreeNode(SolrDocument doc) {
        Set<String> result = new HashSet<String>();
        for (String field : doc.getFieldNames()) {
            if (field.startsWith(TREENODE_ATTACHED)) {
                String id = field.substring(TREENODE_ATTACHED.length());
                result.add(id);
            }
        }
        return result;
    }

    /**
     * Find solr document by id
     * @param solrServer solR serveur
     * @param id to find
     * @return solR document found
     */
    static public SolrDocument findById(SolrServer solrServer, String id) {
        id = quoteForSolr(id);
        SolrQuery query = new SolrQuery(SOLR_ID + ":" + id);
        QueryResponse response;
        try {
            response = solrServer.query(query);
        } catch (SolrServerException eee) {
            throw new WikittyException("Error during find", eee);
        }

        SolrDocumentList results = response.getResults();
        long numFound = results.getNumFound();
        if(numFound == 1) {
            return results.get(0);
        }

        return null;
    }

    /**
     * Find solr document by id
     *
     * @param solrServer solR serveur
     * @param ids to find
     * @return solR documents found
     * @since 3.1
     */
    static public Map<String, SolrDocument> findAllById(
            SolrServer solrServer, Collection<String> ids) {
        String solrField = SOLR_ID;
        Map<String, SolrDocument> result = findAllByField(solrServer, solrField, ids);
        return result;
    }

    /**
     * Find solr document by TreeNode parents extra field
     *
     * @param solrServer solR server
     * @param ids id that must be find in parents list
     * @return Map key:TreeNode id, value; solr document associate with id
     * @since 3.1
     */
    static public Map<String, SolrDocument> findAllByParents(
            SolrServer solrServer, Collection<String> ids) {
        String solrField = TREENODE_PARENTS;
        Map<String, SolrDocument> result = findAllByField(solrServer, solrField, ids);
        return result;
    }

    /**
     * Find solr document by TreeNode attachment field
     *
     * @param solrServer solR server
     * @param ids id that must be find in attachment list
     * @return Map key:TreeNode id, value; solr document associate with id
     * @since 3.1
     */
    static public Map<String, SolrDocument> findAllByAttachment(
            SolrServer solrServer, Collection<String> ids) {
        String solrField = SolrUtil.getSolrFieldName(
                WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_ATTACHMENT, WikittyTypes.WIKITTY);
        Map<String, SolrDocument> result = findAllByField(solrServer, solrField, ids);
        return result;
    }

    /**
     * Find solr document by specified field in argument
     *
     * @param solrServer solR serveur
     * @param solrField field where we looking for ids
     * @param ids ids that must be in solrField
     * @return Map key:TreeNode id, value; solr document associate with id
     * @since 3.1
     */
    static public Map<String, SolrDocument> findAllByField(
           SolrServer solrServer, String solrField, Collection<String> ids) {
       Map<String, SolrDocument> result = new HashMap<String, SolrDocument>();
       if (ids != null && ids.size() > 0) {
           try {
               String request = solrField + ":(";
               String or = "";
               for (String id : ids) {
                   id = quoteForSolr(id);
                   request += or + id;
                   or = " OR ";
               }
               request += ")";

               SolrQuery query = new SolrQuery(request);
               QueryResponse response = executeQuery(solrServer, query);

               SolrDocumentList results = response.getResults();
               for (SolrDocument doc : results) {
                   String id = getStringFieldValue(doc, SOLR_ID);
                   result.put(id, doc);
               }
           } catch (SolrServerException eee) {
               throw new WikittyException("Error during find", eee);
           }
       }
       return result;
    }

    /**
     * Execute SolrQuery on specified SolrServer and return the result
     * 
     * @param solrServer solR server
     * @param query to execute
     * @return QueryResponse
     * @throws SolrServerException if exception occured on request execution
     */
    static public QueryResponse executeQuery(SolrServer solrServer, SolrQuery query)
            throws SolrServerException {
        long start = TimeLog.getTime();

        if (query.getRows() == null) {
            // par defaut SolR pagine par 10, alors que dans tous par defaut
            query.setRows(Integer.MAX_VALUE);
        }
        QueryResponse result = solrServer.query(query);

        long numGet = -1;
        long numfound = -1;
        try {
            numGet = result.getResults().size();
            numfound = result.getResults().getNumFound();
        } catch (Exception eee) {
            log.error("Can't show normal log because exception occur during param evaluation", eee);
        }
        timeLog.log(start, "executeQuery", String.format(
                "nb result %s/%s query was '%s'", numGet, numfound, query));
        if (log.isDebugEnabled()) {
            log.debug(String.format(
                "nb result %s/%s query was '%s'", numGet, numfound, query));
        }

        return result;
    }

    /**
     * Converti un document Solr en une map dont les elements sont types
     * (les boolean, date, numeric sont convertis dans leur type)
     * 
     * @param doc
     * @return
     */
    static public Map<String, Object> convertToTypedMap(SolrDocument doc) {
        Map<String, Object> result = new LinkedHashMap<String, Object>();
        for (String solrField : doc.getFieldNames()) {
            String fieldName = ElementModifier.convertToField(solrField);
            String fieldType = StringUtils.substringAfter(solrField, fieldName);

            // si on a encore le prefix c'est un champs pure SolR, on ne le
            // mais pas dans la map retournee
            if (!StringUtils.startsWith(fieldName, WikittySolrConstant.SOLR_WIKITTY_PREFIX)) {

                FieldType type;
                if (fieldType.startsWith(SUFFIX_BOOLEAN)) {
                    type = new FieldType(WikittyTypes.BOOLEAN, 0, Integer.MAX_VALUE);
                } else if (fieldType.startsWith(SUFFIX_DATE)) {
                    type = new FieldType(WikittyTypes.DATE, 0, Integer.MAX_VALUE);
                } else if (fieldType.startsWith(SUFFIX_NUMERIC)) {
                    type = new FieldType(WikittyTypes.NUMERIC, 0, Integer.MAX_VALUE);
                } else {
                    type = new FieldType(WikittyTypes.STRING, 0, Integer.MAX_VALUE);
                }

                Collection values = doc.getFieldValues(solrField);
                values = (Collection)type.getValidValue(values);

                Object value;
                if (values.isEmpty()) {
                    value = null;
                } else if (values.size() == 1) {
                    value = values.iterator().next();
                } else {
                    value = values;
                }

                result.put(fieldName, value);
            }
        }
        return result;
    }

    /**
     * if you change this method, change
     * {@link FieldModifier#convertToField(org.nuiton.wikitty.services.WikittyTransaction, java.lang.String)}
     * too
     *
     * @param fqfieldName FQ field name
     * @param type of field
     * @return field name
     */
    static public String getSolrFieldName(String fqfieldName, WikittyTypes type) {
        String result = fqfieldName;
        if (type != null) {
            switch (type) {
                case BINARY:
                    result = fqfieldName + SUFFIX_BINARY;
                    break;
                case BOOLEAN:
                    result = fqfieldName + SUFFIX_BOOLEAN;
                    break;
                case DATE:
                    result = fqfieldName + SUFFIX_DATE;
                    break;
                case STRING:
                    result = fqfieldName + SUFFIX_STRING;
                    break;
                case NUMERIC:
                    result = fqfieldName + SUFFIX_NUMERIC;
                    break;
                case WIKITTY:
                    result = fqfieldName + SUFFIX_WIKITTY;
                    break;
                default:
                    result = fqfieldName;
            }
        }
        return result;
    }

//    /**
//     * if you change this method, change
//     * {@link FieldModifier#convertToField(org.nuiton.wikitty.services.WikittyTransaction, java.lang.String)}
//     * too
//     *
//     * @param fqfieldName FQ field name
//     * @param type        of field
//     * @return field name
//     */
//    static public String getSolrCollectionFieldName(String fqfieldName, TYPE type) {
//        switch (type) {
//            case BINARY:
//                return fqfieldName + SUFFIX_BINARY_MULTIVALUED;
//            case BOOLEAN:
//                return fqfieldName + SUFFIX_BOOLEAN_MULTIVALUED;
//            case DATE:
//                return fqfieldName + SUFFIX_DATE_MULTIVALUED;
//            case STRING:
//                return fqfieldName + SUFFIX_STRING_MULTIVALUED;
//            case NUMERIC:
//                return fqfieldName + SUFFIX_NUMERIC_MULTIVALUED;
//            case WIKITTY:
//                return fqfieldName + SUFFIX_WIKITTY_MULTIVALUED;
//            default:
//                return fqfieldName;
//        }
//    }

    /**
     * Copy solr document
     *
     * @param source solr document source
     * @param dest solr document destination
     * @param fieldToInclude only copy thes fields, if null or empty, copy all field
     * @param fieldToExclude to not copy these fields
     */
    static public void copySolrDocument(
            SolrDocument source, SolrInputDocument dest,
            String[] fieldToInclude, String[] fieldToExclude) {
        Collection<String> fieldNames = source.getFieldNames();

        Set<String> fieldToCopy = new HashSet<String>();
        if (fieldToInclude == null || fieldToInclude.length == 0) {
            fieldToCopy.addAll(fieldNames);
        } else {
            for (String fieldName : fieldNames) {
                for (String fieldRegexp : fieldToInclude) {
                    if (fieldName.matches(fieldRegexp)) {
                        fieldToCopy.add(fieldName);
                        break;
                    }
                }
            }
        }
        
        if (fieldToExclude != null && fieldToExclude.length > 0) {
            for (String fieldName : fieldNames) {
                for (String fieldRegexp : fieldToExclude) {
                    if (fieldName.matches(fieldRegexp)) {
                        fieldToCopy.remove(fieldName);
                        break;
                    }
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug(String.format(
                    "Copiable field are %s but only field %s are copied",
                    fieldNames, fieldToCopy));
        }

        for (String fieldName : fieldToCopy) {
            dest.removeField(fieldName); // to prevent add in already exist dest field
            Collection<Object> fieldValues = source.getFieldValues(fieldName);
            for (Object fieldValue : fieldValues) {
                dest.addField(fieldName, fieldValue);
            }
        }
    }

    /**
     * Copy solr document
     *
     * @param source solr document source
     * @param dest solr document destination
     * @param fieldToInclude only copy this field, if null or empty, copy all field
     * @since 3.1
     */
    static public void copySolrDocument(
            SolrDocument source, SolrInputDocument dest, String... fieldToInclude) {
        copySolrDocument(source, dest, fieldToInclude, null);
    }

    /**
     * Copy solr document exlude some fields
     *
     * @param source solr document source
     * @param dest solr document destination
     * @param fieldToExclude not copy these fields
     * @since 3.1
     */
    static public void copySolrDocumentExcludeSomeField(
            SolrDocument source, SolrInputDocument dest, String... fieldToExclude) {
        copySolrDocument(source, dest, null, fieldToExclude);
    }

    static public Collection<String> getStringFieldValues (SolrDocument d, String fieldname) {
        Collection<String> result = getStringFieldValues(d, fieldname, null);
        return result;
    }

    static public Collection<String> getStringFieldValues (SolrInputDocument d, String fieldname) {
        Collection<String> result = getStringFieldValues(d, fieldname, null);
        return result;
    }

    static public Collection<String> getStringFieldValues (
            SolrDocument d, String fieldname, WikittyTypes type) {
        // petit hack, car la methode retourne un Collection<Object> alors
        // qu'il sagit en fait d'un Collection<String>, de cette facon on force
        // la conversion en passant par une colleciton non typee
        String solrFieldName = SolrUtil.getSolrFieldName(fieldname, type);
        Collection tmp = d.getFieldValues(solrFieldName);
        Collection<String> result = (Collection<String>)tmp;
        return result;
    }

    static public Collection<String> getStringFieldValues (
            SolrInputDocument d, String fieldname, WikittyTypes type) {
        // petit hack, car la methode retourne un Collection<Object> alors
        // qu'il sagit en fait d'un Collection<String>, de cette facon on force
        // la conversion en passant par une colleciton non typee
        String solrFieldName = SolrUtil.getSolrFieldName(fieldname, type);
        Collection tmp = d.getFieldValues(solrFieldName);
        Collection<String> result = (Collection<String>)tmp;
        return result;
    }

    /**
     * get value of field in SolrDocument, field must have only one value
     * @param d
     * @param fieldname
     * @return
     */
    static public String getStringFieldValue(SolrInputDocument d, String fieldname) {
        String result = getStringFieldValue(d, fieldname, null);
        return result;
    }

    /**
     * get value of field in SolrDocument, field must have only one value
     * @param d
     * @param fieldname
     * @param type optional type to generate solr field name
     * @return
     */
    static public String getStringFieldValue(
            SolrInputDocument d, String fieldname, WikittyTypes type) {
        String solrFieldName = SolrUtil.getSolrFieldName(fieldname, type);

        Object value = d.getFieldValue(solrFieldName);
        String result = convertToString(value, solrFieldName);
        return result;
    }

    /**
     * get value of field in SolrDocument, field must have only one value
     * @param d
     * @param fieldname
     * @return
     */
    static public String getStringFieldValue(SolrDocument d, String fieldname) {
        String result = getStringFieldValue(d, fieldname, null);
        return result;
    }

    /**
     * get value of field in SolrDocument, field must have only one value
     * @param d
     * @param fieldname
     * @param type optional type to generate solr field name
     * @return
     */
    static public String getStringFieldValue(SolrDocument d, String fieldname, WikittyTypes type) {
        String solrFieldName = SolrUtil.getSolrFieldName(fieldname, type);

        Object value = d.getFieldValue(solrFieldName);
        String result = convertToString(value, solrFieldName);
        return result;
    }

    /**
     * get value of field in SolrDocument, field must have only one value
     * @param d
     * @param fieldname
     * @return
     */
    static public Integer getIntFieldValue(SolrDocument d, String fieldname) {
        Integer result = getIntFieldValue(d, fieldname, null);
        return result;
    }

    /**
     * get value of field in SolrDocument, field must have only one value
     * @param d
     * @param fieldname
     * @param type optional type to generate solr field name
     * @return
     */
    static public Integer getIntFieldValue(SolrDocument d, String fieldname, WikittyTypes type) {
        String solrFieldName = SolrUtil.getSolrFieldName(fieldname, type);

        Object value = d.getFieldValue(solrFieldName);
        Integer result = convertToInteger(value, solrFieldName);
        return result;
    }

    /**
     * Converti un Object en String, si l'objet est de type String un simple
     * cast est fait, si l'objet est un tableau, on prend le 1er element, si
     * le tableau contient plus de 1 element une exception est levee
     *
     * @param value
     * @return une string ou null si value est null ou est un tableau vide
     */
    static public String convertToString(Object value, String solrFieldName) {
        String result;
        if (value == null) {
            result = null;
        } else if (value instanceof String) {
            // c'est un champs monovalue
            result = (String)value;
        } else  if (value instanceof String[]) {
            // c'est un champs multivalue
            String[] values = (String[])value;
            if (values.length == 0) {
                result = null;
            } else if (values.length == 1) {
                result = values[0];
            } else {
                throw new WikittyException(String.format(
                        "You can't get one value from field (%s) with many (%s) value",
                        solrFieldName, values.length));
            }
        } else if (value instanceof Collection) {
            Collection c = (Collection)value;
            if (c.isEmpty()) {
                result = null;
            } else if (c.size() == 1){
                Object o = c.iterator().next();
                result = convertToString(o, solrFieldName);
            } else {
                throw new WikittyException(String.format(
                        "You can't get one value from field (%s) with many (%s) value",
                        solrFieldName, c.size()));                
            }
        } else {
            throw new WikittyException(String.format(
                    "Field (%s) is not an String but %s",
                    solrFieldName, value.getClass().getName()));
        }
        return result;
    }

    /**
     * Converti un Object en String, si l'objet est de type String un simple
     * cast est fait, si l'objet est un tableau, on prend le 1er element, si
     * le tableau contient plus de 1 element une exception est levee
     *
     * @param value
     * @return une string ou null si value est null ou est un tableau vide
     */
    static public Integer convertToInteger(Object value, String solrFieldName) {
        Integer result;
        if (value == null) {
            result = null;
        } else if (value instanceof Integer) {
            // c'est un champs monovalue
            result = (Integer)value;
        } else if (value instanceof Integer[]){
            // c'est un champs multivalue
            Integer[] values = (Integer[])value;
            if (values.length == 0) {
                result = null;
            } else if (values.length == 1) {
                result = values[0];
            } else {
                throw new WikittyException(String.format(
                        "You can't get one value from field (%s) with many (%s) value",
                        solrFieldName, values.length));
            }
        } else {
            throw new WikittyException(String.format(
                    "Field (%s) is not an Integer but %s",
                    solrFieldName, value.getClass().getName()));
        }
        return result;
    }

    /**
     * If value is collection or array get only the first element, else
     * juste return value.
     * 
     * @param value
     * @return 
     */
    static public Object getOneValue(Object value) {
        Object result;
        if (value == null) {
            result = null;
        } else  if (value instanceof Object[]) {
            // c'est un champs multivalue
            Object[] values = (Object[])value;
            if (values.length == 0) {
                result = null;
            } else {
                result = values[0];
            }
        } else if (value instanceof Collection) {
            Collection c = (Collection)value;
            if (c.isEmpty()) {
                result = null;
            } else {
                Object o = c.iterator().next();
                result = getOneValue(o);
            }
        } else {
            result = value;
        }
        return result;
    }

    /**
     * Quote s for solr. Currently only ':' is escaped
     * @param s to quote
     * @return new string solr compliant
     */
    static public String quoteForSolr(String s) {
        String result = s.replaceAll(":", "\\\\:");
        return result;
    }
}
