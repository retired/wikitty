/*
 * #%L
 * Wikitty :: wikitty-solr
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * <h1>Indexation</h1>
 *
 * Ce module sert a l'indexation des wikitties dans SolR. Chaque champs d'un
 * wikitty est indexe de differente facon selon son type.
 * 
 * Chaque type de champs est suffixe par un marqueur.
 *
 * <table border="2" summary="Chaque type de champs est suffixe par un marqueur">
 * <tr>
 * <th>Type wikitty</th><th>Suffixe</th><th>Type d'indexation</th><th>valeur</th><th>stored</th><th>multiValued</th>
 * </tr>
 * <tr>
 * <td>{@link org.nuiton.wikitty.entities.WikittyTypes#BINARY}</td><td>_bi {@link org.nuiton.wikitty.storage.solr.WikittySearchEngineSolr#SUFFIX_BINARY}</td><td>aucun</td><td>vide</td><td>non</td><td>true</td>
 * </tr>
 * <tr>
 * <td>{@link org.nuiton.wikitty.entities.WikittyTypes#BOOLEAN}</td><td>_b {@link org.nuiton.wikitty.storage.solr.WikittySearchEngineSolr#SUFFIX_BOOLEAN}</td><td>boolean</td><td>la valeur du champs</td><td>true</td><td>true</td>
 * </tr>
 * <tr>
 * <td>{@link org.nuiton.wikitty.entities.WikittyTypes#DATE}</td><td>_dt {@link org.nuiton.wikitty.storage.solr.WikittySearchEngineSolr#SUFFIX_DATE}</td><td>date</td><td>la valeur du champs</td><td>true</td><td>true</td>
 * </tr>
 * <tr>
 * <td>{@link org.nuiton.wikitty.entities.WikittyTypes#NUMERIC}</td><td>_d {@link org.nuiton.wikitty.storage.solr.WikittySearchEngineSolr#SUFFIX_NUMERIC}</td><td>sdouble</td><td>la valeur du champs</td><td>true</td><td>true</td>
 * </tr>
 * <tr>
 * <td>{@link org.nuiton.wikitty.entities.WikittyTypes#WIKITTY}</td><td>_w {@link org.nuiton.wikitty.storage.solr.WikittySearchEngineSolr#SUFFIX_WIKITTY}</td><td>string</td><td>l'id du wikitty</td><td>true</td><td>true</td>
 * </tr>
 * <tr>
 * <td rowspan="3">{@link org.nuiton.wikitty.entities.WikittyTypes#STRING}</td><td>_s {@link org.nuiton.wikitty.storage.solr.WikittySearchEngineSolr#SUFFIX_STRING}</td><td>string</td><td>la valeur du champs</td><td>true</td><td>true</td>
 * </tr>
 * <tr>
 * <td>_s_c {@link org.nuiton.wikitty.storage.solr.WikittySearchEngineSolr#SUFFIX_STRING_LOWERCASE}</td><td>string</td><td>la valeur du champs en minuscule</td><td>true</td><td>true</td>
 * </tr>
 * <tr>
 * <td>_s_t {@link org.nuiton.wikitty.storage.solr.WikittySearchEngineSolr#SUFFIX_STRING_FULLTEXT}</td><td>text</td><td>la valeur du champs</td><td>true</td><td>true</td>
 * </tr>
 * </table>
 *
 * D'autres champs sont indexes
 * <table border="2" summary="Autres champs indexes">
 * <tr>
 * <th>champs</th><th>Type d'indexation</th><th>valeur</th><th>stored</th><th>multiValued</th>
 * </tr>
 * <tr>
 * <td>#id</td><td>string</td><td>l'id du wikitty</td><td>true</td><td>false</td>
 * </tr>
 * <tr>
 * <td>#extensions</td><td>string</td><td>la liste des extensions</td><td>true</td><td>true</td>
 * </tr>
 * <tr>
 * <td>#null_field-[fieldname]</td><td>boolean</td><td>vrai si le champs est null</td><td>true</td><td>false</td>
 * </tr>
 * <tr>
 * <td>#fulltext</td><td>text</td><td>la valeur de tous les champs ayant un suffix</td><td>true</td><td>true</td>
 * </tr>
 * </table>
 *
 * <p>
 * Pour les TreeNode, on ajoute des champs sur les objets attaches mais aussi
 * sur les TreeNode eux meme.
 * Sur les TreeNode:
 * <ul>
 * <li> #tree.root le nom racine de l'arbre</li>
 * <li> #tree.parents la liste de tous les parents de ce noeud, lui et le root compris</li>
 * <li> #tree.depth la profondeur de ce noeud dans l'arbre (le root = 1)</li>
 * </ul>
 *
 * Sur les objets attaches:
 * <ul>
 * <li> #tree.attached.[TreeNode.id] liste des parents du TreeNode (TreeNode.id)
 *      sur lequel est attache l'objet (l'objet pouvant etre sur plusieurs noeud</li>
 * <li> #tree.attached-all liste de tous les parents sur lequel est attache l'objet
 *      quelque soit le noeud de rattachement (utilise pour les facetisations)</li>
 * </ul>
 *
 *
 * <p>
 * Les champs sont tous restockes dans une extension 'all' pour pouvoir faire
 * des recherches sur toutes les extensions en meme temps. Par exemple rechercher
 * tout ce qui porte le 'nom' 'portable' quelque soit l'extension (*.nom:portable)
 * <p>
 * Les chaines de caracteres doivent obligatoirement etre indexee en type string
 * si l'on veut pouvoir faire des facettes dessus. Il faut donc obligatoirement
 * indexer les chaines en 'string' et aussi en 'text' pour pouvoir les utiliser
 * dans les facettes mais aussi que la recheche soit plus permissive.
 * <p>
 * SolR copie tous les champs dans le champs 'fulltext' pour la recherche fulltext
 * ce champs est le champs par defaut de recherche.
 * <p>
 * id est marque comme devant etre un champs unique (et donc lorsqu'on enregistre
 * un nouveau document avec le meme id, l'ancien est supprime)
 * <p>
 * Tous les champs sont marque stored car lors de la reindexation des arbres
 * on a besoin de faire une copie de l'ancien document et donc de pouvoir
 * recuperer la valeur des champs
 * <p>
 * Par exemple si on a un champs <b>product.description: String</b> nous le
 * retrouverons dans 7 champs de l'index:
 *
 * <ul>
 * <li> #fulltext : text (car est la copie de tous les champs)</li>
 * <li> product.description_s : string (necessaire pour la facetisation)</li>
 * <li> product.description_s_c : string</li>
 * <li> product.description_s_t : text</li>
 * <li> all.description_s : string</li>
 * <li> all.description_s_c : string</li>
 * <li> all.description_s_t : text</li>
 * </ul>
 *
 * il faudrait que les 5 derniers soit autogenere par solr en utilisant un
 * <b>copyField</b> dans le schema.xml et qu'il ne soit pas stocke. Mais pour
 * cela il faudrait que <b>copyField</b> permette l'utilisation de regexp
 * (faire un patch a SolR ?)
 *
 * &lt;copyField source="*_s" dest="*_s_c"/&gt;
 * &lt;copyField source="*_s" dest="*_s_t"/&gt;
 *
 * &lt;copyField source="*.*_s" dest="all.*_s"/&gt;
 * &lt;copyField source="*.*_s" dest="all.*_s_c"/&gt;
 * &lt;copyField source="*.*_s" dest="all.*_s_t"/&gt;
 *
 * et aussi definir les all pour les autres types
 * &lt;copyField source="*.*_b" dest="all.*_b"/&gt;
 * &lt;copyField source="*.*_dt" dest="all.*_dt"/&gt;
 * &lt;copyField source="*.*_d" dest="all.*_d"/&gt;
 * &lt;copyField source="*.*_w" dest="all.*_w"/&gt;
 *
 * copyField ne support que une * et au debut ou a la fin, donc actuellement
 * il serait possible d'avoir
 *
 * <h2>alternative au stockage de tout les champs</h2>
 * <p>
 * Une alternative serait de ne reprendre que les champs reels (pas les copies)
 * et recreer les copies a partir de ceux la. Les copies pourront ne plus etre
 * stored=true.
 * <p>
 * Une autre alternative serait de récuperer l'objet dans le Storage et de le
 * reindexer completement
 *
 */
package org.nuiton.wikitty.storage.solr;
