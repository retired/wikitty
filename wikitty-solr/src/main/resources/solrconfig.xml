<?xml version="1.0" encoding="UTF-8" ?>
<!--
  #%L
  Wikitty :: wikitty-solr
  %%
  Copyright (C) 2009 - 2010 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<config>

  <luceneMatchVersion>LUCENE_41</luceneMatchVersion>
  
  <!-- Used to specify an alternate directory to hold all index data
       other than the default ./data under the Solr home.
       If replication is in use, this should match the replication configuration. -->
  <dataDir>${wikitty.searchengine.solr.directory.data:./solr/data}</dataDir>

  <!--  The DirectoryFactory to use for indexes.

       solr.StandardDirectoryFactory is filesystem
       based and tries to pick the best implementation for the current
       JVM and platform.  solr.NRTCachingDirectoryFactory, the default,
       wraps solr.StandardDirectoryFactory and caches small files in memory
       for better NRT performance.

       One can force a particular implementation via solr.MMapDirectoryFactory,
       solr.NIOFSDirectoryFactory, or solr.SimpleFSDirectoryFactory.

       solr.RAMDirectoryFactory is memory based, not
       persistent, and doesn't work with replication.
     -->
  <directoryFactory name="DirectoryFactory" class="${wikitty.searchengine.solr.directory.factory:solr.StandardDirectoryFactory}"/>
  <directoryFactory name="IndexReaderFactory" class="${wikitty.searchengine.solr.indexReader.factory:solr.StandardIndexReaderFactory}"/>


  <indexConfig>
    <!-- LockFactory

         This option specifies which Lucene LockFactory implementation
         to use.

         single = SingleInstanceLockFactory - suggested for a
                  read-only index or when there is no possibility of
                  another process trying to modify the index.
         native = NativeFSLockFactory - uses OS native file locking.
                  Do not use when multiple solr webapps in the same
                  JVM are attempting to share a single index.
         simple = SimpleFSLockFactory  - uses a plain file for locking

         Defaults: 'native' is default for Solr3.6 and later, otherwise
                   'simple' is the default

         More details on the nuances of each LockFactory...
         http://wiki.apache.org/lucene-java/AvailableLockFactories
    -->
    <lockType>${wikitty.searchengine.solr.lockType:single}</lockType>

    <!-- Unlock On Startup

         If true, unlock any held write or commit locks on startup.
         This defeats the locking mechanism that allows multiple
         processes to safely access a lucene index, and should be used
         with care. Default is "false".

         This is not needed if lock type is 'single'
     -->
    <unlockOnStartup>${wikitty.searchengine.solr.unlockOnStartup:true}</unlockOnStartup>
  </indexConfig>
  
  <!--    Enables JMX if and only if an existing MBeanServer is found, use 
          this if you want to configure JMX through JVM parameters. Remove
          this to disable exposing Solr configuration and statistics to JMX.
          
        If you want to connect to a particular server, specify the agentId
        e.g. <jmx agentId="myAgent" />
        
        If you want to start a new MBeanServer, specify the serviceUrl
        e.g <jmx serviceurl="service:jmx:rmi:///jndi/rmi://localhost:9999/solr" />
        
        For more details see http://wiki.apache.org/solr/SolrJmx
  -->
  <jmx />

  <!-- the default high-performance update handler -->
  <updateHandler class="solr.DirectUpdateHandler2">
  </updateHandler>


  <query>
    <!-- Maximum number of clauses in a boolean query (default: 1024). can affect
        range or prefix queries that expand to big boolean
        queries.  An exception is thrown if exceeded.  -->
    <!-- wikitty need large value to work -->
    <maxBooleanClauses>2147483647</maxBooleanClauses>

    
    <!-- Solr Internal Query Caches

         There are two implementations of cache available for Solr,
         LRUCache, based on a synchronized LinkedHashMap, and
         FastLRUCache, based on a ConcurrentHashMap.

         FastLRUCache has faster gets and slower puts in single
         threaded operation and thus is generally faster than LRUCache
         when the hit ratio of the cache is high (> 75%), and may be
         faster under other scenarios on multi-cpu systems.
    -->

    <!-- Filter Cache

         Cache used by SolrIndexSearcher for filters (DocSets),
         unordered sets of *all* documents that match a query.  When a
         new searcher is opened, its caches may be prepopulated or
         "autowarmed" using data from caches in the old searcher.
         autowarmCount is the number of items to prepopulate.  For
         LRUCache, the autowarmed items will be the most recently
         accessed items.

         Parameters:
           class - the SolrCache implementation LRUCache or
               (LRUCache or FastLRUCache)
           size - the maximum number of entries in the cache
           initialSize - the initial capacity (number of entries) of
               the cache.  (see java.util.HashMap)
           autowarmCount - the number of entries to prepopulate from
               and old cache.
      -->
    <filterCache
      class="solr.FastLRUCache"
      size="512"
      initialSize="512"
      autowarmCount="0"/>

   <!-- Query Result Cache

         Caches results of searches - ordered lists of document ids
         (DocList) based on a query, a sort, and the range of documents requested.
      -->
    <queryResultCache
      class="solr.LRUCache"
      size="512"
      initialSize="512"
      autowarmCount="0"/>

  <!-- Document Cache

         Caches Lucene Document objects (the stored fields for each
         document).  Since Lucene internal document ids are transient,
         this cache will not be autowarmed.
      -->
    <documentCache
      class="solr.LRUCache"
      size="512"
      initialSize="512"
      autowarmCount="0"/>

    <!-- Lazy Field Loading

         If true, stored fields that are not requested will be loaded
         lazily.  This can result in a significant speed improvement
         if the usual case is to not load all stored fields,
         especially if the skipped fields are large compressed text
         fields.
    -->
    -->
    <enableLazyFieldLoading>true</enableLazyFieldLoading>

   <!-- Result Window Size

        An optimization for use with the queryResultCache.  When a search
        is requested, a superset of the requested number of document ids
        are collected.  For example, if a search for a particular query
        requests matching documents 10 through 19, and queryWindowSize is 50,
        then documents 0 through 49 will be collected and cached.  Any further
        requests in that range can be satisfied via the cache.
     -->
    <!--
     | Code Lutin
     | 1 si Wikitty est plus utilise pour de l'ecriture que de la lecture
     | la valeur par defaut est 50. Il semble convenable de mettre deux fois
     | la taille de la recherche. Si on pagine par 25, 50 est une bonne valeur.
     +-->
    <queryResultWindowSize>1</queryResultWindowSize>
    
    <!-- Maximum number of documents to cache for any entry in the
         queryResultCache. -->
    <queryResultMaxDocsCached>200</queryResultMaxDocsCached>

    <!-- This entry enables an int hash representation for filters (DocSets)
         when the number of items in the set is less than maxSize.  For smaller
         sets, this representation is more memory efficient, more efficient to
         iterate over, and faster to take intersections.  -->
    <HashDocSet maxSize="3000" loadFactor="0.75"/>

    <!-- If a search request comes in and there is no current registered searcher,
         then immediately register the still warming searcher and use it.  If
         "false" then all requests will block until the first searcher is done
         warming. -->
    <useColdSearcher>false</useColdSearcher>

    <!-- Maximum number of searchers that may be warming in the background
      concurrently.  An error is returned if this limit is exceeded. Recommend
      1-2 for read-only slaves, higher for masters w/o cache warming. -->
    <maxWarmingSearchers>5</maxWarmingSearchers>

  </query>

  <!-- Request Dispatcher

       This section contains instructions for how the SolrDispatchFilter
       should behave when processing requests for this SolrCore.

       handleSelect is a legacy option that affects the behavior of requests
       such as /select?qt=XXX

       handleSelect="true" will cause the SolrDispatchFilter to process
       the request and dispatch the query to a handler specified by the
       "qt" param, assuming "/select" isn't already registered.

       handleSelect="false" will cause the SolrDispatchFilter to
       ignore "/select" requests, resulting in a 404 unless a handler
       is explicitly registered with the name "/select"

       handleSelect="true" is not recommended for new users, but is the default
       for backwards compatibility
       -->
  <requestDispatcher handleSelect="true" >
    <!--Make sure your system has some authentication before enabling remote streaming!  -->
    <requestParsers enableRemoteStreaming="false" multipartUploadLimitInKB="2048" />
  </requestDispatcher>



<!-- FIXME clean requestHandler to keep only necessary requestHandler -->



    <!-- requestHandler plugins... incoming queries will be dispatched to the
     correct handler based on the path or the qt (query type) param.
     Names starting with a '/' are accessed with the a path equal to the
     registered name.  Names without a leading '/' are accessed with:
      http://host/app/select?qt=name
     If no qt is defined, the requestHandler that declares default="true"
     will be used.
  -->
  <requestHandler name="/select" class="solr.SearchHandler" default="true">
    <!-- default values for query parameters -->
     <lst name="defaults">
       <str name="echoParams">explicit</str>
       <str name="defType">lucene</str>
       <str name="q.op">AND</str>
       <str name="df">#fulltext</str>
       <!--
       <int name="rows">10</int>
       <str name="fl">*</str>
       <str name="version">2.1</str>
        -->
     </lst>
  </requestHandler>

  <!-- Update request handler.

       Note: Since solr1.1 requestHandlers requires a valid content type header if posted in
       the body. For example, curl now requires: -H 'Content-type:text/xml; charset=utf-8'
       The response format differs from solr1.1 formatting and returns a standard error code.

       To enable solr1.1 behavior, remove the /update handler or change its path
    -->
  <requestHandler name="/update" class="solr.XmlUpdateRequestHandler" />

  <!-- Admin Handlers

     Admin Handlers - This will register all the standard admin
     RequestHandlers.

     "/admin/luke"
     "/admin/system"
     "/admin/plugins"
     "/admin/threads"
     "/admin/properties"
     "/admin/file"
  -->
  <requestHandler name="/admin/" class="solr.admin.AdminHandlers" />

  <!-- ping/healthcheck -->
  <requestHandler name="/admin/ping" class="PingRequestHandler">
    <lst name="defaults">
      <str name="qt">standard</str>
      <str name="q">solrpingquery</str>
      <str name="echoParams">all</str>
    </lst>
  </requestHandler>

<!--
  <queryParser name="lucene" class="org.nuiton.wikitty.storage.solr.WikittySolrQueryParser"/>
  <queryParser name="wikitty" class="org.nuiton.wikitty.storage.solr.WikittySolrQueryParser"/>
-->

  <!-- example of registering a query parser
  <queryParser name="lucene" class="org.apache.solr.search.LuceneQParserPlugin"/>
  -->

  <!-- example of registering a custom function parser 
  <valueSourceParser name="myfunc" class="com.mycompany.MyValueSourceParser" />
  -->
    
  <!-- config for the admin interface --> 
  <admin>
    <defaultQuery>solr</defaultQuery>
    
    <!-- configure a healthcheck file for servers behind a loadbalancer
    <healthcheck type="file">server-enabled</healthcheck>
    -->
  </admin>

</config>
