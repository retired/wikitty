package org.nuiton.wikitty.storage.solr;

/*
 * #%L
 * Wikitty :: wikitty-solr
 * %%
 * Copyright (C) 2009 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.junit.Test;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyServiceFactory;
import org.nuiton.wikitty.services.WikittyServiceStorage;
import org.nuiton.wikitty.storage.WikittySearchEngine;

/**
 * Classe permettant de tester des requetes purement SolR et voir le resultat
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SolrQueryTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(SolrQueryTest.class);

    SolrServer server = null;

    public SolrServer getServer() throws ArgumentsParserException {
        if (server == null) {
            ApplicationConfig config = new ApplicationConfig();
            config.loadDefaultOptions(WikittyConfigOption.values());
            config.setOption("wikitty.data.directory", "/var/local/chorem/localhost");
            config.setOption("wikitty.searchengine.solr.directory.data",
                    "${wikitty.data.directory}/data/solr");

            // Parse args
            config.parse();

            server = SolrUtil.getSolrServer(config);
        }
        return server;
    }



    public QueryResponse doQuery(String query) throws Exception {
        SolrServer server = getServer();
        SolrQuery querySolr = new SolrQuery(query);
        QueryResponse result = SolrUtil.executeQuery(server, querySolr);
        return result;
    }

    public QueryResponse doQuery(SolrQuery querySolr) throws Exception {
        SolrServer server = getServer();
        QueryResponse result = SolrUtil.executeQuery(server, querySolr);
        return result;
    }

    public void testSubQuery() throws Exception {
//FinancialTransaction.VAT_d:19.6 AND _query_:
//{!join from=#id to=FinancialTransaction.payer_w v=_query_:{!join from=Employee.company_w to=#id v=_query_:{!join from=#id to=Employee.person_w v=Person.firstName_s:Catherine AND Person.lastName_s:Heintz}}}
//        String query = "FinancialTransaction.VAT_d:19.6 AND "
//                + "_query_:{!join from=#id to=FinancialTransaction.payer_w "
//                + "v=_query_:{!join from=Employee.company_w to=#id "
//                + "v=_query_:{!join from=#id to=Employee.person_w "
//                + "v=\"Person.firstName_s:Catherine AND Person.lastName_s:Heintz\"}}}"
//                + "";

//        String query = "FinancialTransaction.VAT_d:19.6 AND " +
//                "_query_:{!join from=Employee.company_w to=FinancialTransaction.payer_w " +
//                "v=_query_:{!join from=#id to=Employee.person_w " +
//                "v=\"Person.firstName_s:Catherine AND Person.lastName_s:Heintz\"}}";

        String query = "FinancialTransaction.VAT_d:19.6 AND "
                + "_query_:\"{!join from=#id to=FinancialTransaction.payer_w}Company.name_s_c:Néréide\"";
        
        SolrQuery querySolr = new SolrQuery(query);
        querySolr.setParam("fl", "score,#id,ref:FinancialTransaction.reference_s,amount:FinancialTransaction.amount_d,tva:FinancialTransaction:VAT_d,payer:FinancialTransaction.payer_w");
//        querySolr.setParam("debugQuery", "true");

        System.out.println("Query:" + querySolr);
        QueryResponse result = doQuery(querySolr);
        System.out.println("result:" + result);

    }

    public void testSelectQuery() throws Exception {
        String query = "FinancialTransaction.VAT_d:19.6 AND FinancialTransaction.payer_w:\"d32c4e83-1918-4575-ba18-293af75f5ee3\"";
        SolrQuery querySolr = new SolrQuery(query);
        querySolr.setParam("fl", "FinancialTransaction.amount_d,FinancialTransaction.VAT_d,ttc:product(FinancialTransaction.amount_d_sortable,linear(FinancialTransaction.VAT_d_sortable,0.01,1))");

        System.out.println("Query:" + querySolr);
        QueryResponse result = doQuery(querySolr);
        System.out.println("result:" + result);
    }

    public void testGroupQuery() throws Exception {
        String query = "FinancialTransaction.VAT_d:19.6";
        SolrQuery querySolr = new SolrQuery(query);
        querySolr.setParam("group", true);
        querySolr.setParam("group.field", "FinancialTransaction.payer_w_sortable");

        System.out.println("Query:" + querySolr);
        QueryResponse result = doQuery(querySolr);
        System.out.println("result:" + result);
        System.out.println("explain:" + result.getExplainMap());
//        result.getResults();
//        result.getGroupResponse().getValues().get(0).getValues().get(0).getResult();

    }

}
