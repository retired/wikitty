```diff
- Since 20180416 : This project as been retired due to inactivity and is no longer maintained
```
Wikitty est un système de stockage et de recherche de type clé/valeur proposant de nombreux services destinées à la création simples d'applications modulaires.

Vous pouvez accéder:
- [à la documentation] (http://wikitty.nuiton.org/v/latest/index.html)
- [à la roadmap] (https://gitlab.nuiton.org/nuiton/wikitty/milestones)
- [aux sources (git)] (https://gitlab.nuiton.org/nuiton/wikitty/tree/develop)
- [à l'analyse du code] (https://qa.nuiton.org/project/index/org.nuiton:wikitty)
- [aux binaires] (http://nexus.nuiton.org/nexus/content/repositories/other-releases/org/nuiton/wikitty)

Il existe différentes listes de discussion autour du projet:
- Liste pour que les utilisateurs puissent échanger : [wikitty-users](https://ml.nuiton.org/cgi-bin/mailman/listinfo/wikitty-users)
- Liste de discussion des développeurs : [wikitty-devel](https://ml.nuiton.org/cgi-bin/mailman/listinfo/wikitty-devel)
- Liste recevant tous les commits gitlab : [wikitty-commits](https://ml.nuiton.org/cgi-bin/mailman/listinfo/wikitty-commit)
- Liste recevant toutes les notification de build Jenkins : [wikitty-build](https://ml.nuiton.org/cgi-bin/mailman/listinfo/wikitty-build)
