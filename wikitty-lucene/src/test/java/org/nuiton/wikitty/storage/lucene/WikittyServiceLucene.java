/*
 * #%L
 * Wikitty :: wikitty-lucene
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.storage.lucene;

import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.services.WikittyServiceStorage;
import org.nuiton.wikitty.storage.WikittyExtensionStorageInMemory;
import org.nuiton.wikitty.storage.WikittyStorageInMemory;

/**
 * Wikitty service utilisant lucene comme search engine.
 * 
 * @author chatellier
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceLucene extends WikittyServiceStorage {

    public WikittyServiceLucene(ApplicationConfig config) {
        super(config);
        extensionStorage = new WikittyExtensionStorageInMemory();
        wikittyStorage = new WikittyStorageInMemory();
        searchEngine = new WikittySearchEngineLucene(config, extensionStorage);
    }
}
