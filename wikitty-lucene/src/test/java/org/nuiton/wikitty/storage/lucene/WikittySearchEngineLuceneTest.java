/*
 * #%L
 * Wikitty :: wikitty-lucene
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.storage.lucene;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.entities.WikittyLabelImpl;
import org.nuiton.wikitty.entities.WikittyTreeNode;
import org.nuiton.wikitty.entities.WikittyTreeNodeImpl;
import org.nuiton.wikitty.entities.WikittyUserImpl;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.FacetTopic;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;
import org.nuiton.wikitty.search.TreeNodeResult;
import org.nuiton.wikitty.search.operators.Element;
import org.nuiton.wikitty.services.WikittyEvent;

/**
 * Test for class {@link WikittySearchEngineLucene}.
 * 
 * @author echatellier
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittySearchEngineLuceneTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static final Log log = LogFactory.getLog(WikittySearchEngineLuceneTest.class);

    protected static WikittyServiceLucene ws;

    protected static ApplicationConfig instance = WikittyConfig.getConfig("wikitty-config-sample-inmemory.properties");

    protected static WikittyProxy proxy;

    @BeforeClass
    public static void initTests() throws Exception {
        String tmpdir = System.getProperty("java.io.tmpdir");
        File indexDir = new File(tmpdir, "test");
        instance = WikittyConfig.getConfig("wikitty-config-sample-inmemory.properties");
        instance.setOption(WikittyConfigOption.WIKITTY_DATA_DIR.key, indexDir.getAbsolutePath());

        ws = new WikittyServiceLucene(instance);
        proxy= new WikittyProxy(ws);
    }

    @Before
    public void deleteAll() throws Exception {
        ws.clear(null);
    }

    @Test
    public void testFullTextSearch() throws Exception {
        // for id for easy debugging
        WikittyImpl w = new WikittyImpl("label");
        WikittyLabelImpl l = new WikittyLabelImpl(w);
        l.addLabels("label");
        w = new WikittyImpl("LABEL");
        WikittyLabelImpl l2 = new WikittyLabelImpl(w);
        l2.addLabels("OTHER LABEL");
        w = new WikittyImpl("TATA");
        WikittyUserImpl u = new WikittyUserImpl(w);
        u.setLogin("tata");

        List<Wikitty> toStore = new ArrayList<Wikitty>();
        toStore.add(l.getWikitty());
        toStore.add(l2.getWikitty());
        toStore.add(u.getWikitty());

        ws.store(null, toStore, false);

        Criteria c = Search.query().keyword("lab*").criteria();
        PagedResult<String> result = ws.findAllByCriteria(
                null, Collections.singletonList(c)).get(0);
        if (log.isInfoEnabled()) {
            log.info("Results = " + result.getAll());
        }
        Assert.assertEquals(2, result.getNumFound());

        Criteria c2 = Search.query().keyword("*a*").criteria();
        PagedResult<String> result2 = ws.findAllByCriteria(
                null, Collections.singletonList(c2)).get(0);
        if (log.isInfoEnabled()) {
            log.info("Results = " + result2.getAll());
        }
        Assert.assertEquals(3, result2.getNumFound());

    }

    /*@Test
    public void testCountAttachment() throws Exception {
        List<Wikitty> toStore = new ArrayList<Wikitty>();

        WikittyImpl w1 = new WikittyImpl("at1");
        WikittyImpl w2 = new WikittyImpl("at2");
        WikittyImpl w3 = new WikittyImpl("at3");

        WikittyImpl root = new WikittyImpl("the-root");
        WikittyTreeNodeImpl n = new WikittyTreeNodeImpl(root);
        n.addAttachment(w1.getId());
        n.addAttachment(w2.getId());
        n.addAttachment(w3.getId());

        toStore.add(w1);
        toStore.add(w2);
        toStore.add(w3);
        toStore.add(root);

        ws.store(null, toStore, false);
        toStore.clear();

        WikittyImpl w11 = new WikittyImpl("at11");
        WikittyImpl w12 = new WikittyImpl("at12");
        WikittyImpl w13 = new WikittyImpl("at13");

        WikittyImpl node1 = new WikittyImpl("node1");
        n = new WikittyTreeNodeImpl(node1);
        n.setParent(root.getId());
        n.addAttachment(w11.getId());
        n.addAttachment(w12.getId());
        n.addAttachment(w13.getId());

        toStore.add(w11);
        toStore.add(w12);
        toStore.add(w13);
        toStore.add(node1);

        ws.store(null, toStore, false);
        toStore.clear();

        WikittyImpl w21 = new WikittyImpl("at21");
        WikittyImpl w22 = new WikittyImpl("at22");
        WikittyImpl w23 = new WikittyImpl("at23");
        WikittyImpl w24 = new WikittyImpl("at24");

        WikittyImpl node2 = new WikittyImpl("node2");
        n = new WikittyTreeNodeImpl(node2);
        n.setParent(root.getId());
        n.addAttachment(w21.getId());
        n.addAttachment(w22.getId());
        n.addAttachment(w23.getId());
        n.addAttachment(w24.getId());

        toStore.add(w21);
        toStore.add(w22);
        toStore.add(w23);
        toStore.add(w24);
        toStore.add(node2);

        ws.store(null, toStore, false);
        toStore.clear();

        WikittyImpl w111 = new WikittyImpl("at111");
        WikittyImpl w112 = new WikittyImpl("at112");

        WikittyImpl node11 = new WikittyImpl("node11");
        n = new WikittyTreeNodeImpl(node11);
        n.setParent(node1.getId());
        n.addAttachment(w111.getId());
        n.addAttachment(w112.getId());

        toStore.add(w111);
        toStore.add(w112);
        toStore.add(node11);

        ws.store(null, toStore, false);
        toStore.clear();

        TreeNodeResult<String> treeNodeResult =
                ws.getSearchEngine().findAllChildrenCount(
                null, root.getId(), 0, true, null);
        int val = treeNodeResult.getAttCount();
//        Integer val = ws.getSearchEngine().findNodeCount(null, root, null);
        Assert.assertEquals(12, val);

//        Map<WikittyTreeNode, Integer> children = ws.findTreeNode(null, root.getId(), null);
//        Map<String, Integer> children = ws.getSearchEngine().findAllChildrenCount(null, root, null);
        TreeNodeResult<String> children =
                ws.getSearchEngine().findAllChildrenCount(
                null, root.getId(), 1, true, null);
        if (log.isDebugEnabled()) {
            log.debug("Children : " + children);
        }
        
        Assert.assertEquals(2, children.getChildCount());
        Assert.assertEquals(5, children.getChild(node1.getId()).getAttCount());
        Assert.assertEquals(4, children.getChild(node2.getId()).getAttCount());
    }*/

    /**
     * Test que lors de la reindexation les noeuds indexés le sont
     * correctement si leur attachement ne sont pas encore
     * présent dans l'index.
     * 
     * Stocke les attachements avant le noeud.
     */
    @Test
    public void testReindexWithAttachement() {

        // store attachement
        Wikitty attach1 = new WikittyImpl("att1");
        Wikitty attach2 = new WikittyImpl("att2");
        Collection<Wikitty> toStore = new ArrayList<Wikitty>();
        toStore.add(attach1);
        toStore.add(attach2);
        ws.store(null, toStore, false);
        
        // store first node
        Wikitty treeNode = new WikittyImpl();
        WikittyTreeNode treeNodeImpl = new WikittyTreeNodeImpl(treeNode);
        treeNodeImpl.setName("root");
        treeNodeImpl.addAttachment(attach1.getId());
        treeNodeImpl.addAttachment(attach2.getId());
        ws.store(null, Collections.singleton(treeNode), false);
        
        // set resync
        ws.syncSearchEngine(null);
        
        // search wikitty with attachement
        Search query = Search.query();
        query.eq(Element.ELT_EXTENSION, WikittyTreeNodeImpl.EXT_WIKITTYTREENODE);
        query.eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "root");
        PagedResult<Wikitty> results = proxy.findAllByCriteria(query.criteria());
        Assert.assertEquals(1, results.size());
        WikittyTreeNode resultTreeNode = new WikittyTreeNodeImpl(results.get(0));
        Assert.assertEquals(2, resultTreeNode.getAttachment().size());
    }
    
    /**
     * Test que lors de la reindexation les noeuds indexés le sont
     * correctement si leur attachement ne sont pas encore
     * présent dans l'index.
     * 
     * Stocke le noeud avant les attachements.
     */
    @Test
    public void testReindexWithAttachementOrdering() {
        
        // store first node
        Wikitty treeNode = new WikittyImpl();
        WikittyTreeNode treeNodeImpl = new WikittyTreeNodeImpl(treeNode);
        treeNodeImpl.setName("root");
        WikittyEvent event = ws.store(null, Collections.singleton(treeNode), false);
        treeNode = event.getWikitties().get(treeNode.getId());
        treeNodeImpl = new WikittyTreeNodeImpl(treeNode);
        
        // store attachement
        Wikitty attach1 = new WikittyImpl("att1");
        Wikitty attach2 = new WikittyImpl("att2");
        treeNodeImpl.addAttachment(attach1.getId());
        treeNodeImpl.addAttachment(attach2.getId());
        Collection<Wikitty> toStore = new ArrayList<Wikitty>();
        toStore.add(attach1);
        toStore.add(attach2);
        toStore.add(treeNode);
        ws.store(null, toStore, false);

        // set resync
        ws.syncSearchEngine(null);
        
        // search wikitty with attachement
        Search query = Search.query();
        query.eq(Element.ELT_EXTENSION, WikittyTreeNodeImpl.EXT_WIKITTYTREENODE);
        query.eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "root");
        PagedResult<Wikitty> results = proxy.findAllByCriteria(query.criteria());
        Assert.assertEquals(1, results.size());
        WikittyTreeNode resultTreeNode = new WikittyTreeNodeImpl(results.get(0));
        Assert.assertEquals(2, resultTreeNode.getAttachment().size());
    }

    /**
     * Test que lorsqu'un document solr est supprimé, il l'est réelement
     * car le proxy gere les retours null pour des documents qui n'existe
     * plus dans le base, mais les recherches continue de les trouver.
     */
    @Test
    public void testSolrDeleteDocument() {
        // store new wikitty
        Wikitty toDeleteWikitty = new WikittyImpl("wikkitytodelete");
        ws.store(null, Collections.singleton(toDeleteWikitty), false);
        // look for it
        Search query = Search.query();
        query.eq(Element.ELT_ID, "wikkitytodelete");
        Assert.assertEquals(1, ws.findAllByCriteria(null,
                Collections.singletonList(query.criteria())).get(0).getNumFound());
        Assert.assertEquals(1, proxy.findAllByCriteria(query.criteria()).getNumFound());

        // delete it
        ws.delete(null, Collections.singleton(toDeleteWikitty.getId()));

        // try to look for it after deletion
        Assert.assertEquals(0, proxy.findAllByCriteria(query.criteria()).getNumFound());
        Assert.assertEquals(0, ws.findAllByCriteria(null,
                Collections.singletonList(query.criteria())).get(0).getNumFound());
    }

    // Nombre of iteration for test delete
    public static final int NB_DOCS_TO_DELETE = 17;

    /**
     * Test que lorsque plus de 10 documents solr sont supprimés, il le sont réelement
     * car par défault, solr limte les resultats à 10
     */
    @Test
    public void testSolrDeleteMoreThan10Documents() {

        List<WikittyLabel> toDelete = new ArrayList<WikittyLabel>();

        for (int i = 0;i < NB_DOCS_TO_DELETE;i++) {
            WikittyLabel toDeleteEntity = new WikittyLabelImpl();

            // store new wikitty
            toDeleteEntity.addLabels("toDeleteEntity" + i);
            toDelete.add(toDeleteEntity);
        }

        // Store all
        proxy.store(toDelete);

        // look for it
        Search query = Search.query();
        query.eq(Element.ELT_EXTENSION, WikittyLabel.EXT_WIKITTYLABEL);
        Assert.assertEquals(NB_DOCS_TO_DELETE, ws.findAllByCriteria(null,
                Collections.singletonList(query.criteria())).get(0).getNumFound());
        Assert.assertEquals(NB_DOCS_TO_DELETE, proxy.findAllByCriteria(query.criteria()).getNumFound());
        Assert.assertEquals(NB_DOCS_TO_DELETE, proxy.findAllByCriteria(WikittyLabel.class, query.criteria()).getNumFound());

        // delete it
        proxy.delete(toDelete);

        // try to look for it after deletion
        Assert.assertEquals(0, proxy.findAllByCriteria(query.criteria()).getNumFound());
        Assert.assertEquals(0, ws.findAllByCriteria(null,
                Collections.singletonList(query.criteria())).get(0).getNumFound());
        Assert.assertEquals(0, proxy.findAllByCriteria(WikittyLabel.class, query.criteria()).size());
    }

    /**
     * Test to store duplicated wikitty.
     */
    @Test
    public void testLuceneDoubleStore() {
        WikittyLabel myLabel1 = new WikittyLabelImpl();
        
        proxy.store(myLabel1);
        proxy.store(myLabel1);
        
        Criteria criteria = Search.query().exteq(WikittyLabel.EXT_WIKITTYLABEL).criteria();
        criteria.setEndIndex(0); // just count
        PagedResult<Wikitty> result = proxy.findAllByCriteria(criteria);
        Assert.assertEquals(1, result.getNumFound());
    }

    /**
     * Test que l'implementation des facettes sur bobo fonctionne correctement.
     */
    @Test
    public void testLuceneFacets() {
        WikittyTreeNode myLabel1 = new WikittyTreeNodeImpl();
        myLabel1.setName("apple");
        WikittyTreeNode myLabel2 = new WikittyTreeNodeImpl();
        myLabel2.setName("apple");
        WikittyTreeNode myLabel3 = new WikittyTreeNodeImpl();
        myLabel3.setName("babana");
        WikittyTreeNode myLabel4 = new WikittyTreeNodeImpl();
        myLabel4.setName("babana");
        WikittyTreeNode myLabel5 = new WikittyTreeNodeImpl();
        myLabel5.setName("pineapple"); // ananas

        proxy.store(myLabel1, myLabel2, myLabel3, myLabel4, myLabel5);

        Criteria criteria = Search.query().exteq(WikittyTreeNode.EXT_WIKITTYTREENODE).criteria();
        criteria.addFacetField(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME);
        criteria.setEndIndex(0); // just count
        PagedResult<Wikitty> result = proxy.findAllByCriteria(criteria);

        // just les nombre pour chaque facette
        List<FacetTopic> nameTopics = result.getFacets().get(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME);
        Assert.assertEquals(3, nameTopics.size());
        for (FacetTopic nameTopic : nameTopics) {
            if (log.isInfoEnabled()) {
                log.info("Facet found " + nameTopic.getTopicName() + " : " + nameTopic.getCount());
            }
            if (nameTopic.getTopicName().equals("apple")) {
                Assert.assertEquals(2, nameTopic.getCount());
            } else if (nameTopic.getTopicName().equals("babana")) {
                Assert.assertEquals(2, nameTopic.getCount());
            } else if (nameTopic.getTopicName().equals("pineapple")) {
                Assert.assertEquals(1, nameTopic.getCount());
            } else {
                Assert.fail("Unknown topic value : " + nameTopic.getTopicName());
            }
        }
    }
    
    /**
     * Test que l'implementation des facettes sur bobo fonctionne correctement.
     * Test une facet criteria.
     * 
     * TODO bobo n'a pas l'air de supporter les facettes de type query
     */
    @Ignore
    public void testLuceneFacetCriteria() {
        WikittyTreeNode myLabel1 = new WikittyTreeNodeImpl();
        myLabel1.setName("apple");
        WikittyTreeNode myLabel2 = new WikittyTreeNodeImpl();
        myLabel2.setName("apple");
        WikittyTreeNode myLabel3 = new WikittyTreeNodeImpl();
        myLabel3.setName("babana");
        WikittyTreeNode myLabel4 = new WikittyTreeNodeImpl();
        myLabel4.setName("babana");
        WikittyTreeNode myLabel5 = new WikittyTreeNodeImpl();
        myLabel5.setName("pineapple"); // ananas

        proxy.store(myLabel1, myLabel2, myLabel3, myLabel4, myLabel5);

        Criteria nameFacet = Search.query().keyword("*apple").criteria();
        nameFacet.setName("allapples");
        Criteria criteria = Search.query().exteq(WikittyTreeNode.EXT_WIKITTYTREENODE).criteria();
        criteria.addFacetCriteria(nameFacet);
        criteria.setEndIndex(0); // just count
        PagedResult<Wikitty> result = proxy.findAllByCriteria(criteria);

        // just les nombre pour chaque facette
        List<FacetTopic> nameTopics = result.getFacets().get("allapples");
        Assert.assertEquals(2, nameTopics.size());
        for (FacetTopic nameTopic : nameTopics) {
            if (log.isInfoEnabled()) {
                log.info("Facet found " + nameTopic.getTopicName() + " : " + nameTopic.getCount());
            }
            if (nameTopic.getTopicName().equals("apple")) {
                Assert.assertEquals(2, nameTopic.getCount());
            } else if (nameTopic.getTopicName().equals("pineapple")) {
                Assert.assertEquals(1, nameTopic.getCount());
            } else {
                Assert.fail("Unknown topic value : " + nameTopic.getTopicName());
            }
        }
    }

    /**
     * Test des operateur speciaux sur les champs analyzés.
     */
    @Test
    public void testLuceneOperatorAnalyzed() {
        WikittyTreeNode myLabel1 = new WikittyTreeNodeImpl();
        myLabel1.setName("ApPlE");

        proxy.store(myLabel1);

        // keyword
        Criteria criteria = Search.query().keyword("aPpLe").criteria();
        PagedResult<Wikitty> result = proxy.findAllByCriteria(criteria);
        Assert.assertEquals(1, result.getNumFound());

        // equals qui ne retourne rien
        criteria = Search.query().eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME,"aPpLe").criteria();
        result = proxy.findAllByCriteria(criteria);
        Assert.assertEquals(0, result.getNumFound());

        // like
        criteria = Search.query().like(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME,"aPpLe").criteria();
        result = proxy.findAllByCriteria(criteria);
        Assert.assertEquals(1, result.getNumFound());
    }
}
