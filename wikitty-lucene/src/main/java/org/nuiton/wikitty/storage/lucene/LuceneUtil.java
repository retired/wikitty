/*
 * #%L
 * 
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.storage.lucene;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Fieldable;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.search.operators.Element;

/**
 * TODO add comment here.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 * @since 3.2
 */
public class LuceneUtil {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(LuceneUtil.class);

    /**
     * Get field name used in lucene storage for a field name in query.
     * 
     * @param fieldName field name to convert
     * @return field name in lucene
     */
    public static String getLuceneFieldName(String fieldName) {
        String result;
        if (Element.ELT_ID.equals(fieldName)) {
            result = WikittyLuceneConstants.LUCENE_ID;
        } else if (Element.ELT_EXTENSION.equals(fieldName)) {
            result = WikittyLuceneConstants.LUCENE_EXTENSIONS;
        } else {
            result = fieldName;
        }
        return result;
    }
    
    /**
     * Get field name used in lucene storage for a field name in query.
     * 
     * @param fieldName field name to convert
     * @return field name in lucene
     */
    public static String getWikittyFieldName(String fieldName) {
        String result;
        if (WikittyLuceneConstants.LUCENE_ID.equals(fieldName)) {
            result = Element.ELT_ID;
        } else if (WikittyLuceneConstants.LUCENE_EXTENSIONS.equals(fieldName)) {
            result = Element.ELT_EXTENSION;
        } else {
            result = fieldName;
        }
        return result;
    }

    /*
     * get value of field in Document, field must have only one value
     * @param d
     * @param fieldname
     * @param type optional type to generate lucene field name
     * @return
     *
    static public Integer getIntFieldValue(Document d, String fieldname) {
        String luceneFieldName = getLuceneFieldName(fieldname);

        Object value = d.getFieldable(luceneFieldName).stringValue();
        Integer result = convertToInteger(value, luceneFieldName);
        return result;
    }*/

    /**
     * Converti un Object en String, si l'objet est de type String un simple
     * cast est fait, si l'objet est un tableau, on prend le 1er element, si
     * le tableau contient plus de 1 element une exception est levee
     *
     * @param value
     * @param luceneFieldName
     * @return une string ou null si value est null ou est un tableau vide
     */
    static public String convertToString(Object value, String luceneFieldName) {
        String result;
        if (value == null) {
            result = null;
        } else if (value instanceof String) {
            // c'est un champs monovalue
            result = (String)value;
        } else if (value instanceof String[]) {
            // c'est un champs multivalue
            String[] values = (String[])value;
            if (values.length == 0) {
                result = null;
            } else if (values.length == 1) {
                result = values[0];
            } else {
                throw new WikittyException(String.format(
                        "You can't get one value from field (%s) with many (%s) value",
                        luceneFieldName, values.length));
            }
        } else if (value instanceof Collection) {
            Collection c = (Collection)value;
            if (c.isEmpty()) {
                result = null;
            } else if (c.size() == 1){
                Object o = c.iterator().next();
                result = convertToString(o, luceneFieldName);
            } else {
                throw new WikittyException(String.format(
                        "You can't get one value from field (%s) with many (%s) value",
                        luceneFieldName, c.size()));                
            }
        } else {
            throw new WikittyException(String.format(
                    "Field (%s) is not an String but %s",
                    luceneFieldName, value.getClass().getName()));
        }
        return result;
    }

    /**
     * Converti un Object en String, si l'objet est de type String un simple
     * cast est fait, si l'objet est un tableau, on prend le 1er element, si
     * le tableau contient plus de 1 element une exception est levee
     *
     * @param value
     * @param luceneFieldName
     * @return une string ou null si value est null ou est un tableau vide
     */
    static public Integer convertToInteger(Object value, String luceneFieldName) {
        Integer result;
        if (value == null) {
            result = null;
        } else if (value instanceof Integer) {
            // c'est un champs monovalue
            result = (Integer)value;
        } else if (value instanceof Integer[]){
            // c'est un champs multivalue
            Integer[] values = (Integer[])value;
            if (values.length == 0) {
                result = null;
            } else if (values.length == 1) {
                result = values[0];
            } else {
                throw new WikittyException(String.format(
                        "You can't get one value from field (%s) with many (%s) value",
                        luceneFieldName, values.length));
            }
        } else {
            throw new WikittyException(String.format(
                    "Field (%s) is not an Integer but %s",
                    luceneFieldName, value.getClass().getName()));
        }
        return result;
    }
    
    /**
     * Copy lucene document
     *
     * @param source lucene document source
     * @param dest lucene document destination
     * @param fieldToInclude only copy this fields, if null or empty, copy all field
     * @param fieldToExclude to not copy these fields
     */
    static public void copyLuceneDocument(Document source, Document dest,
            String[] fieldToInclude, String[] fieldToExclude) {
        Collection<Fieldable> fields = source.getFields();
        Collection<String> fieldNames = new ArrayList<String>();
        for (Fieldable field : fields) {
            fieldNames.add(field.name());
        }

        Set<String> fieldToCopy = new HashSet<String>();
        if (fieldToInclude == null || fieldToInclude.length == 0) {
            fieldToCopy.addAll(fieldNames);
        } else {
            for (String fieldName : fieldNames) {
                for (String fieldRegexp : fieldToInclude) {
                    if (fieldName.matches(fieldRegexp)) {
                        fieldToCopy.add(fieldName);
                        break;
                    }
                }
            }
        }
        
        if (fieldToExclude != null && fieldToExclude.length > 0) {
            for (String fieldName : fieldNames) {
                for (String fieldRegexp : fieldToExclude) {
                    if (fieldName.matches(fieldRegexp)) {
                        fieldToCopy.remove(fieldName);
                        break;
                    }
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug(String.format(
                    "Copiable field are %s but only field %s are copied",
                    fieldNames, fieldToCopy));
        }

        for (String fieldName : fieldToCopy) {
            dest.removeField(fieldName); // to prevent add in already exist dest field
            Fieldable[] scrFields = source.getFieldables(fieldName);
            for (Fieldable scrField : scrFields) {
                dest.add(scrField);
            }
        }
    }

    /**
     * Copy lucene document
     *
     * @param source lucene document source
     * @param dest lucene document destination
     * @param fieldToInclude only copy this field, if null or empty, copy all field
     * @since 3.2
     */
    static public void copyLuceneDocument(Document source, Document dest, String... fieldToInclude) {
        copyLuceneDocument(source, dest, fieldToInclude, null);
    }

    /**
     * Copy lucene document exlude some fields
     *
     * @param source lucene document source
     * @param dest lucene document destination
     * @param fieldToExclude not copy these fields
     * @since 3.2
     */
    static public void copyLuceneDocumentExcludeSomeField(Document source, Document dest, String... fieldToExclude) {
        copyLuceneDocument(source, dest, null, fieldToExclude);
    }
}
