/*
 * #%L
 * Wikitty :: wikitty-lucene
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.storage.lucene;

import static org.nuiton.wikitty.storage.lucene.WikittyLuceneConstants.LUCENE_ID;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.map.LazyMap;
import org.apache.lucene.document.Document;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyTreeNode;
import org.nuiton.wikitty.entities.WikittyTreeNodeHelper;

/**
 * Class permettant de construire la liste des objets qui ont ete ajoute
 * ou supprimer d'un noeud
 *
 * @author poussin
 * @version $Revision$
 * @since 3.1
 *
 * Last update: $Date$
 * by : $Author$
 */
public class AttachmentInTree {

    // On genere en meme temps la liste des attachments qui doivent
    // etre reindexe
    protected Set<String> allAttachmentToIndex = new HashSet<String>();

    protected Factory listFactory = new Factory() {
        @Override
        public Object create() {
            return new HashSet<String>();
        }
    };

    // key: TreeNode id, value: list of attached id
    protected Map<String, Set<String>> attachmentRemovedInTree = LazyMap.decorate(
            new HashMap<String, Set<String>>(), listFactory);
    // key: TreeNode id, value: list of attached id
    protected Map<String, Set<String>> attachmentAddedInTree = LazyMap.decorate(
            new HashMap<String, Set<String>>(), listFactory);

    /**
     * Remove all ids in attachment list. Ids is object already deleted
     * reindex it is not necessary
     * 
     * @param ids
     */
    public void clean(Collection<String> ids) {
        if (ids != null) {
            allAttachmentToIndex.removeAll(ids);
            for (Set<String> set : attachmentRemovedInTree.values()) {
                set.removeAll(ids);
            }
            for (Set<String> set : attachmentAddedInTree.values()) {
                set.removeAll(ids);
            }
        }
    }

    public int size() {
        return allAttachmentToIndex.size();
    }
    
    public Set<String> getAll() {
        return allAttachmentToIndex;
    }

    public Map<String, Set<String>> getAdded() {
        return attachmentAddedInTree;
    }

    public Map<String, Set<String>> getRemoved() {
        return attachmentRemovedInTree;
    }

    /**
     * @param id TreeNode id
     * @param ids attachment id
     */
    public void remove(String id, Collection<String> attId) {
        if (attId != null && !attId.isEmpty()) {
            attachmentRemovedInTree.get(id).addAll(attId);
            allAttachmentToIndex.addAll(attId);
        }
    }

    /**
     * @param doc TreeNode document representation
     */
    public void remove(Document doc) {
        //String id = SolrUtil.getStringFieldValue(doc, WikittySolrConstant.SOLR_ID);
        String id = doc.get(LUCENE_ID);

        //Collection<String> att = SolrUtil.getStringFieldValues(doc,
        //        WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_ATTACHMENT,
        //        TYPE.WIKITTY);
        
        String[] values = doc.getValues(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_ATTACHMENT);
        List<String> att = Arrays.asList(values);
        remove(id, att);
    }

    /**
     * Ajout les attachment du TreeNode passe en parametre via son Id
     *
     * @param id TreeNode id
     * @param attId attachment id
     */
    public void add(String id, Collection<String> attId) {
        if (attId != null && !attId.isEmpty()) {
            attachmentAddedInTree.get(id).addAll(attId);
            allAttachmentToIndex.addAll(attId);
        }
    }

   /**
     * Ajout l'attachment du TreeNode passe en parametre via son Id
     *
     * @param id TreeNode id
     * @param attId attachment id
     * @since 3.0.5
     */
    public void add(String id, String attId) {
        if (attId != null) {
            attachmentAddedInTree.get(id).add(attId);
            allAttachmentToIndex.add(attId);
        }
    }

    /**
     * Ajout les attachment du TreeNode passe en parametre sous forme de doc Solr
     * @param doc TreeNode document representation
     */
    public void add(Document doc) {
        //String id = SolrUtil.getStringFieldValue(doc, WikittySolrConstant.SOLR_ID);
        String id = doc.get(LUCENE_ID);

        //Collection<String> att = SolrUtil.getStringFieldValues(doc,
        //        WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_ATTACHMENT,
        //        TYPE.WIKITTY);
        
        String[] values = doc.getValues(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_ATTACHMENT);
        List<String> att = Arrays.asList(values);
        add(id, att);
    }

    /**
     * Ajout les attachment du TreeNode passe en parametre sous forme de doc Solr
     * Mais seulement si l'attachment est aussi dans la liste restriction
     *
     * @param doc TreeNode document representation
     * @param restriction la liste accepte de wikitty a ajouter
     * @since 3.0.5
     */
    public void add(Document doc, Set<String> restriction) {
      //String id = SolrUtil.getStringFieldValue(doc, WikittySolrConstant.SOLR_ID);
        String id = doc.get(LUCENE_ID);

        //Collection<String> att = SolrUtil.getStringFieldValues(doc,
        //        WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_ATTACHMENT,
        //        TYPE.WIKITTY);
        
        String[] values = doc.getValues(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_ATTACHMENT);
        List<String> att = Arrays.asList(values);
        if (att != null) {
            for (String attId : att) {
                if (restriction.contains(attId)) {
                    add(id, attId);
                }
            }
        }
    }

    /**
     * Ajout les attachment du TreeNode passe en parametre sous forme de Wikitty
     * @param doc TreeNode document representation
     */
    public void add(Wikitty w) {
        String id = w.getId();
        Set<String> att = WikittyTreeNodeHelper.getAttachment(w);
        add(id, att);
    }

}
