/*
 * #%L
 * 
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.storage.lucene;

import org.nuiton.wikitty.search.operators.Element;
import org.nuiton.wikitty.storage.WikittyExtensionStorage;

/**
 * Field modifier. In lucene field are splited in.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FieldModifier {

    protected WikittyExtensionStorage extensionStorage;

    public FieldModifier(WikittyExtensionStorage extensionStorage) {
        this.extensionStorage = extensionStorage;
    }

    public String getLuceneFieldName(String wikittyFieldName, boolean analyzed) {
        String result;
        if (Element.ELT_ID.equals(wikittyFieldName)) {
            result = WikittyLuceneConstants.LUCENE_ID;
        } else if (Element.ELT_EXTENSION.equals(wikittyFieldName)) {
            result = WikittyLuceneConstants.LUCENE_EXTENSIONS;
        } else if (analyzed) {
            // TODO peut etre recupererle type suivant le champs là
            result = wikittyFieldName + "_a";
        } else {
            result = wikittyFieldName;
        }
        return result;
    }
}
