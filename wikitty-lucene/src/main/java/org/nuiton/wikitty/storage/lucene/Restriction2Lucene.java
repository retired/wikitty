/*
 * #%L
 * 
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.storage.lucene;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeQuery;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.search.operators.And;
import org.nuiton.wikitty.search.operators.AssociatedRestriction;
import org.nuiton.wikitty.search.operators.Between;
import org.nuiton.wikitty.search.operators.Contains;
import org.nuiton.wikitty.search.operators.Element;
import org.nuiton.wikitty.search.operators.EndsWith;
import org.nuiton.wikitty.search.operators.Equals;
import org.nuiton.wikitty.search.operators.Greater;
import org.nuiton.wikitty.search.operators.GreaterOrEqual;
import org.nuiton.wikitty.search.operators.In;
import org.nuiton.wikitty.search.operators.Keyword;
import org.nuiton.wikitty.search.operators.Less;
import org.nuiton.wikitty.search.operators.LessOrEqual;
import org.nuiton.wikitty.search.operators.Like;
import org.nuiton.wikitty.search.operators.Not;
import org.nuiton.wikitty.search.operators.NotEquals;
import org.nuiton.wikitty.search.operators.Null;
import org.nuiton.wikitty.search.operators.Or;
import org.nuiton.wikitty.search.operators.Restriction;
import org.nuiton.wikitty.search.operators.StartsWith;
import org.nuiton.wikitty.search.operators.Unlike;

/**
 * Convert wikitty restriction to lucene search query.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class Restriction2Lucene {

    /** Query parser depending on {@link Analyzer}. */
    protected QueryParser parser;

    protected FieldModifier fieldModifier;

    public Restriction2Lucene(Analyzer analyzer, FieldModifier fieldModifier) {
        this.fieldModifier = fieldModifier;
        parser = new QueryParser(WikittySearchEngineLucene.WIKITTY_LUCENE_VERSION,
                WikittyLuceneConstants.LUCENE_DEFAULT_FIELD + WikittyLuceneConstants.SUFFIX_ANALYZED,
                analyzer);

        // allow "*" or "?" if first character wildcard query
        parser.setAllowLeadingWildcard(true);
    }

    /**
     * Convert a wikitty restriction to lucene search query.
     * 
     * @param restriction restriction to convert
     * @return lucene query
     * @throws WikittyException if query can't be parsed
     */
    public Query toLucene(Restriction restriction) throws WikittyException {

        Query query = null;

        try {
            switch (restriction.getName()) {
                case TRUE:
                    query = true2Lucene();
                    break;
                case FALSE:
                    query = false2Lucene();
                    break;
                case NOT:
                    query = not2Lucene((Not)restriction);
                    break;
                case AND:
                    query = and2Lucene((And)restriction);
                    break;
                case OR:
                    query = or2Lucene((Or)restriction);
                    break;
                case EQUALS:
                    query = eq2Lucene((Equals)restriction);
                    break;
                case NOT_EQUALS:
                    query = neq2Lucene((NotEquals)restriction);
                    break;
                case LESS:
                    query = less2Lucene((Less)restriction);
                    break;
                case LESS_OR_EQUAL:
                    query = lessEq2Lucene((LessOrEqual)restriction);
                    break;
                case GREATER:
                    query = great2Lucene((Greater)restriction);
                    break;
                case GREATER_OR_EQUAL:
                    query = greatEq2Lucene((GreaterOrEqual)restriction);
                    break;
                case BETWEEN:
                    query = between2Lucene((Between)restriction);
                    break;
                case CONTAINS:
                    query = contains2Lucene((Contains)restriction);
                    break;
                case IN:
                    query = in2Lucene((In)restriction);
                    break;
                case STARTS_WITH:
                    query = start2Lucene((StartsWith)restriction);
                    break;
                case ENDS_WITH:
                    query = end2Lucene((EndsWith)restriction);
                    break;
                case LIKE:
                    query = like2Lucene((Like)restriction);
                    break;
                case UNLIKE:
                    query = unlike2Lucene((Unlike)restriction);
                    break;
                case ASSOCIATED:
                    query = associated2Lucene((AssociatedRestriction)restriction);
                    break;
                case KEYWORD:
                    query = keyword2Lucene((Keyword)restriction);
                    break;
                case IS_NULL:
                    query = isNull2Lucene((Null)restriction);
                    break;
                case IS_NOT_NULL:
                    query = isNotNull2Lucene((Null)restriction);
                    break;
                default:
                    throw new WikittyException("this kind of restriction is not supported : "
                            + restriction.getName().toString());
            }
        } catch (ParseException ex) {
            throw new WikittyException("Can't transform restriction to lucene", ex);
        }
        return query;
    }

    protected String element2Lucene(Element element) {
        return element2Lucene(element, false);
    }
    
    protected String element2Lucene(Element element, boolean analyzed) {
        String result = element.getName();
        result = fieldModifier.getLuceneFieldName(result, analyzed);
        return result;
    }

    /**
     * @param restriction
     * @return
     */
    protected Query isNotNull2Lucene(Null restriction) {
        String fieldName = WikittyLuceneConstants.LUCENE_NULL_FIELD + restriction.getFieldName();
        TermQuery query = new TermQuery(new Term(fieldName, "false"));
        return query;
    }

    /**
     * @param restriction
     * @return
     */
    protected Query isNull2Lucene(Null restriction) {
        String fieldName = WikittyLuceneConstants.LUCENE_NULL_FIELD + restriction.getFieldName();
        TermQuery query = new TermQuery(new Term(fieldName, "true"));
        return query;
    }

    /**
     * @param restriction
     * @return
     * @throws ParseException 
     */
    protected Query keyword2Lucene(Keyword restriction) throws ParseException {
        String value = restriction.getValue();
        Query query = parser.parse(value);
        return query;
    }

    /**
     * @param restriction
     * @return
     */
    protected Query associated2Lucene(AssociatedRestriction restriction) {
        throw new NotImplementedException("Not yet implemented");
    }

    /**
     * @param restriction
     * @return
     */
    protected Query unlike2Lucene(Unlike restriction) {
        String field = element2Lucene(restriction.getElement(), true);
        Term term = new Term(field, restriction.getValue());
        TermQuery query = new TermQuery(term);
        BooleanQuery bQuery = new BooleanQuery();
        bQuery.add(query, Occur.MUST_NOT);
        return bQuery;
    }

    /**
     * Like operator.
     * 
     * @param restriction restriction
     * @return lucene query
     * @throws ParseException 
     */
    protected Query like2Lucene(Like restriction) throws ParseException {
        String field = element2Lucene(restriction.getElement(), true);
        // il y a seulement besoin d'analyser la value
        // pour la tokeniser
        Query query = parser.parse(field + ":" + restriction.getValue());
        return query;
    }

    /**
     * @param restriction
     * @return
     */
    protected Query end2Lucene(EndsWith restriction) {
        String field = element2Lucene(restriction.getElement());
        Term term = new Term(field, "*" + restriction.getValue());
        TermQuery query = new TermQuery(term);
        return query;
    }

    /**
     * @param restriction
     * @return
     */
    protected Query start2Lucene(StartsWith restriction) {
        String field = element2Lucene(restriction.getElement());
        Term term = new Term(field, restriction.getValue() + "*");
        TermQuery query = new TermQuery(term);
        return query;
    }

    /**
     * @param restriction
     * @return
     */
    protected Query in2Lucene(In restriction) {
        throw new NotImplementedException("Not yet implemented");
    }

    /**
     * @param restriction
     * @return
     */
    protected Query contains2Lucene(Contains restriction) {
        throw new NotImplementedException("Not yet implemented");
    }

    /**
     * @param restriction
     * @return
     */
    protected Query between2Lucene(Between restriction) {
        String fieldName = element2Lucene(restriction.getElement());
        String min = restriction.getMin();
        String max = restriction.getMax();
        
        TermRangeQuery query = new TermRangeQuery(fieldName, min, max, true, true);
        return query;
    }

    /**
     * @param restriction
     * @return
     */
    protected Query greatEq2Lucene(GreaterOrEqual restriction) {
        String fieldName = element2Lucene(restriction.getElement());
        String value = restriction.getValue();
        
        // ne fonctionne pas, mais devrait
        // TermRangeQuery query = new TermRangeQuery(fieldName, value, "*", true, true);
        
        BooleanQuery query = new BooleanQuery();
        query.add(new TermQuery(new Term(fieldName, "*")), Occur.MUST);
        TermRangeQuery rangeQuery = new TermRangeQuery(fieldName, "*", value, true, true);
        query.add(rangeQuery, Occur.MUST_NOT);

        return query;
    }

    /**
     * @param restriction
     * @return
     */
    protected Query great2Lucene(Greater restriction) {
        String fieldName = element2Lucene(restriction.getElement());
        String value = restriction.getValue();
        
        // ne fonctionne pas, mais devrait
        //TermRangeQuery query = new TermRangeQuery(fieldName, value, "*", false, false);
        
        BooleanQuery query = new BooleanQuery();
        query.add(new TermQuery(new Term(fieldName, "*")), Occur.MUST);
        TermRangeQuery rangeQuery = new TermRangeQuery(fieldName, "*", value, false, false);
        query.add(rangeQuery, Occur.MUST_NOT);

        return query;
    }

    /**
     * @param restriction
     * @return
     */
    protected Query lessEq2Lucene(LessOrEqual restriction) {
        String fieldName = element2Lucene(restriction.getElement());
        String value = restriction.getValue();
        
        TermRangeQuery query = new TermRangeQuery(fieldName, "*", value, true, true);
        return query;
    }

    /**
     * @param restriction
     * @return
     */
    protected Query less2Lucene(Less restriction) {
        String fieldName = element2Lucene(restriction.getElement());
        String value = restriction.getValue();
        
        TermRangeQuery query = new TermRangeQuery(fieldName, "*", value, false, false);
        return query;
    }

    /**
     * @param restriction
     * @return
     * @throws ParseException 
     */
    protected Query eq2Lucene(Equals restriction) throws ParseException {
        String field = element2Lucene(restriction.getElement());
        Term term = new Term(field, restriction.getValue());
        TermQuery query = new TermQuery(term);

        return query;
    }

    /**
     * @param restriction
     * @return
     * @throws ParseException 
     */
    protected Query neq2Lucene(NotEquals restriction) throws ParseException {
        String field = element2Lucene(restriction.getElement());
        Term term = new Term(field, restriction.getValue());
        TermQuery query = new TermQuery(term);
        BooleanQuery bquery = new BooleanQuery();
        bquery.add(new MatchAllDocsQuery(), Occur.MUST);
        bquery.add(query, Occur.MUST_NOT);
        return bquery;
    }

    /**
     * @param or
     * @return
     */
    protected Query or2Lucene(Or or) {
        if (or.getRestrictions() == null) {
            throw new WikittyException("or.restrictions is null");
        }
        if (or.getRestrictions().size() < 2) {
            throw new WikittyException("OR is an operator that handle 2 operand at least");
        }
        BooleanQuery query = new BooleanQuery();
        for (Restriction restriction : or.getRestrictions()) {
            Query subQuery = toLucene(restriction);
            query.add(subQuery, Occur.SHOULD);
        }
        return query;
    }

    /**
     * @param and
     * @return
     */
    protected Query and2Lucene(And and) {
        if (and.getRestrictions() == null) {
            throw new WikittyException("and.restrictions is null");
        }
        if (and.getRestrictions().size() < 2) {
            throw new WikittyException("AND is an operator that handle 2 operand at least");
        }
        BooleanQuery query = new BooleanQuery();
        for (Restriction restriction : and.getRestrictions()) {
            Query subQuery = toLucene(restriction);
            query.add(subQuery, Occur.MUST);
        }
        return query;
    }

    protected Query true2Lucene() {
        // *:*
        Query query = new MatchAllDocsQuery();
        return query;
    }
    
    protected Query false2Lucene() {
        // *:* - *:*
        BooleanQuery query = new BooleanQuery();
        query.add(new MatchAllDocsQuery(), Occur.SHOULD);
        query.add(new MatchAllDocsQuery(), Occur.MUST_NOT);
        return query;
    }
    
    protected Query not2Lucene(Not not) {
        // *:* - *:*
        BooleanQuery query = new BooleanQuery();
        query.add(new MatchAllDocsQuery(), Occur.SHOULD);
        Query subQuery = toLucene(not.getRestriction());
        query.add(subQuery, Occur.MUST_NOT);
        return query;
    }
}
