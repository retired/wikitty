/*
 * #%L
 * 
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.storage.lucene;

import static org.nuiton.wikitty.storage.lucene.WikittyLuceneConstants.LUCENE_DEFAULT_FIELD;
import static org.nuiton.wikitty.storage.lucene.WikittyLuceneConstants.LUCENE_ID;
import static org.nuiton.wikitty.storage.lucene.WikittyLuceneConstants.SUFFIX_ANALYZED;
import static org.nuiton.wikitty.storage.lucene.WikittyLuceneConstants.TREENODE_ATTACHED;
import static org.nuiton.wikitty.storage.lucene.WikittyLuceneConstants.TREENODE_ATTACHED_ALL;
import static org.nuiton.wikitty.storage.lucene.WikittyLuceneConstants.TREENODE_DEPTH;
import static org.nuiton.wikitty.storage.lucene.WikittyLuceneConstants.TREENODE_PARENTS;
import static org.nuiton.wikitty.storage.lucene.WikittyLuceneConstants.TREENODE_PREFIX;
import static org.nuiton.wikitty.storage.lucene.WikittyLuceneConstants.TREENODE_ROOT;
import static org.nuiton.wikitty.storage.lucene.WikittyLuceneConstants.WIKITTY_LUCENE_PREFIX;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.Fieldable;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.NIOFSDirectory;
import org.apache.lucene.util.Version;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.TimeLog;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyTreeNode;
import org.nuiton.wikitty.entities.WikittyTreeNodeHelper;
import org.nuiton.wikitty.entities.WikittyTypes;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.query.WikittyQueryResultTreeNode;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.FacetTopic;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;
import org.nuiton.wikitty.search.TreeNodeResult;
import org.nuiton.wikitty.search.operators.Restriction;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.storage.WikittyExtensionStorage;
import org.nuiton.wikitty.storage.WikittySearchEngine;

import com.browseengine.bobo.api.BoboBrowser;
import com.browseengine.bobo.api.BoboIndexReader;
import com.browseengine.bobo.api.Browsable;
import com.browseengine.bobo.api.BrowseException;
import com.browseengine.bobo.api.BrowseFacet;
import com.browseengine.bobo.api.BrowseHit;
import com.browseengine.bobo.api.BrowseRequest;
import com.browseengine.bobo.api.BrowseResult;
import com.browseengine.bobo.api.FacetAccessible;
import com.browseengine.bobo.api.FacetSpec;
import com.browseengine.bobo.facets.FacetHandler;
import com.browseengine.bobo.facets.impl.MultiValueFacetHandler;

/**
 * Wikitty search engine implementation based on Lucene for storage and search
 * engine and bobo for facetting support.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class WikittySearchEngineLucene implements WikittySearchEngine {

    /** Class logger. */
    final static private Log log = LogFactory.getLog(WikittySearchEngineLucene.class);
    final static private TimeLog timeLog = new TimeLog(WikittySearchEngineLucene.class);

    /** Index directory. */
    protected Directory indexDirectory;

    /** Reader, writer and query analyzer. */
    protected Analyzer indexAnalyzer;

    /** Field modifier. */
    protected FieldModifier fieldModifier;

    /** Lucene version used by wikitty. */
    protected static final Version WIKITTY_LUCENE_VERSION = Version.LUCENE_35;

    /**
     * Constructor.
     * 
     * @param config wikitty config
     * @param extensionStorage extension storage
     */
    public WikittySearchEngineLucene(ApplicationConfig config, WikittyExtensionStorage extensionStorage) {

        try {
            indexDirectory = getIndexDirectory(config);

            // hack to create lucene index
            // new IndexSearcher fails with no index
            if (!IndexReader.indexExists(indexDirectory)) {
                IndexWriter writer = null;
                try {
                    IndexWriterConfig indexWriterConfig = new IndexWriterConfig(WIKITTY_LUCENE_VERSION, indexAnalyzer);
                    writer = new IndexWriter(indexDirectory, indexWriterConfig);
                } finally {
                    IOUtils.closeQuietly(writer);
                }
            }
            
            indexAnalyzer = getIndexAnalyzer(config);

            fieldModifier = new FieldModifier(extensionStorage);
        } catch (IOException ex) {
            throw new WikittyException("Can't init lucene directory", ex);
        }

    }

    /**
     * Get directory object representing lucene index directory.
     * 
     * @param config application config
     * @return index directory
     * @throws IOException 
     */
    protected Directory getIndexDirectory(ApplicationConfig config) throws IOException {
        
        String luceneDirFactoryKey = WikittyConfigOption.WIKITTY_SEARCHENGINE_LUCENE_INDEX_DIRECTORY.getKey();
        String indexPath = config.getOption(luceneDirFactoryKey);
        File indexFile = new File(indexPath);

        if (log.isInfoEnabled()) {
            log.info("Using lucene directory : " + indexFile);
        }

        Directory directory = new NIOFSDirectory(indexFile);
        
        return directory;
    }
    
    /**
     * Return index analyzer defined in configuration or fallback to
     * {@link StandardAnalyzer}.
     * 
     * @param config
     * @return
     */
    protected Analyzer getIndexAnalyzer(ApplicationConfig config) {

        Analyzer analyzer = null;

        String luceneAnalyzerFactoryKey = WikittyConfigOption.WIKITTY_SEARCHENGINE_LUCENE_INDEX_ANALYZER.getKey();

        try {
            Class analyzerClass = config.getOptionAsClass(luceneAnalyzerFactoryKey);

            if (analyzerClass != null) {
                // get correct constructor
                Constructor constructor = analyzerClass.getConstructor(Version.class);
                analyzer = (Analyzer)constructor.newInstance(WIKITTY_LUCENE_VERSION);
            }
        } catch (Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't init lucene analyzer : " + config.getOption(luceneAnalyzerFactoryKey), ex);
            }
        }

        // default to StandardAnalyzer
        if (analyzer == null) {
            analyzer = new StandardAnalyzer(WIKITTY_LUCENE_VERSION);
            //analyzer = new KeywordAnalyzer();
        }

        if (log.isInfoEnabled()) {
            log.info("Using lucene analyzer : " + analyzer);
        }

        return analyzer;
    }

    /*
     * @see org.nuiton.wikitty.storage.WikittySearchEngine#clear(org.nuiton.wikitty.services.WikittyTransaction)
     */
    @Override
    public void clear(WikittyTransaction transaction) throws WikittyException {
        
        // TODO manage transaction

        IndexWriter writer = null;
        try {
            // must be instancied every time (single instance per writer)
            IndexWriterConfig indexWriterConfig = new IndexWriterConfig(WIKITTY_LUCENE_VERSION, indexAnalyzer);
            writer = new IndexWriter(indexDirectory, indexWriterConfig);
            writer.deleteAll();
            writer.commit();
        } catch (IOException ex) {
            if (writer != null) {
                try {
                    writer.rollback();
                } catch (IOException e) {
                    throw new WikittyException("Can't rollback index", ex);
                }
            }
            throw new WikittyException("Can't clear index", ex);
        } finally {
            IOUtils.closeQuietly(writer);
        }

    }

    /*
     * @see org.nuiton.wikitty.storage.WikittySearchEngine#store(org.nuiton.wikitty.services.WikittyTransaction, java.util.Collection, boolean)
     */
    @Override
    public void store(WikittyTransaction transaction,
            Collection<Wikitty> wikitties, boolean force) {
        long startTime = TimeLog.getTime();

        IndexWriter writer = null;
        IndexSearcher searcher = null;
        try {
            // must be instancied every time (single instance per writer)
            IndexWriterConfig indexWriterConfig = new IndexWriterConfig(WIKITTY_LUCENE_VERSION, indexAnalyzer);
            writer = new IndexWriter(indexDirectory, indexWriterConfig);
            searcher = new IndexSearcher(indexDirectory);

            // tous les wikitties passes en parametre
            Map<String, Wikitty> allWikitties = new HashMap<String, Wikitty>();
            // les ids des wikitties en parametre reellement modifier (a reindexer)
            Set<String> dirtyObject = new HashSet<String>();
            // les ids des TreeNodes dont le champs parent a change (est aussi
            // contenu dans dirtyObject
            Set<String> dirtyParent = new HashSet<String>();
            // les valeur du champs parent des TreeNodes dont le champs parent
            // a change (sauf si parent = null)
            Set<String> dirtyParentParentId = new HashSet<String>();

            // doc that will be stored at end of process
            Map<String, Document> addedDocs = new HashMap<String, Document>();

            // remplissage des collections
            for(Wikitty w : wikitties) {
                allWikitties.put(w.getId(), w);
                if (force || !w.getDirty().isEmpty() ||
                        WikittyUtil.versionGreaterThan("1", w.getVersion())) {
                    // s'il y a au moins un champs a reindexer ou que l'objet
                    // n'a jamais ete sauve (1 > version)
                    dirtyObject.add(w.getId());
                    if (WikittyTreeNodeHelper.hasExtension(w) && (force
                            ||w.getDirty().contains(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_PARENT)
                            || null == WikittyTreeNodeHelper.getParent(w))) {
                            // si le pere a change
                            // ou qu'il est null (creation d'un nouvel arbre)
                            // il faut indexer le noeud
                        dirtyParent.add(w.getId());
                        String parent = WikittyTreeNodeHelper.getParent(w);
                        if (parent != null) {
                            dirtyParentParentId.add(parent);
                        }
                    }
                }
            }

            // recuperation des documents Solr deja indexes, pour minimiser la reindexation
            Map<String, Document> dirtyObjectDoc = findAllById(searcher, dirtyObject);
            Map<String, Document> dirtyParentDoc = findAllByField(searcher, TREENODE_PARENTS, dirtyParent);
            Map<String, Document> parents = findAllById(searcher, dirtyParentParentId);

            // On genere en meme temps la liste des attachments qui doivent
            // etre reindexe
            AttachmentInTree attachmentInTree = new AttachmentInTree();
            
            //
            // Phase 1: on indexe les objets passe en paremetre, on copie si
            //     besoin #tree.attached des wikitties et #tree.* des
            //     TreeNode dont leur champs parent n'a pas ete modifie, et
            //     dans ce cas on collecte les modif d'attachments des TreeNode
            //

            for (String id : dirtyObject) {
                Wikitty w = allWikitties.get(id);
                Document oldDoc = dirtyObjectDoc.get(id);
                Document doc = convertWikittyToDoc(w);
                if (oldDoc != null) {
                    // On a un ancien document partiel ou complet
                    // s'il etait partiel (seulement l'indexation arbre
                    // cela veut dire que l'objet TreeNode a ete store
                    // sans que l'attachment le soit et qu'il l'est que
                    // maintenant

                    // copy des champs #tree.attached des documents partiels ou non
                    LuceneUtil.copyLuceneDocument(oldDoc, doc, TREENODE_ATTACHED + ".*");
                    if (WikittyTreeNodeHelper.hasExtension(w)
                            && !dirtyParentDoc.containsKey(id)) {
                        // si c'est un TreeNode, mais qu'aucun pere n'a change
                        // on recopie l'ancienne indexation d'arbre
                        // si elle existe
                        LuceneUtil.copyLuceneDocument(oldDoc, doc, TREENODE_PREFIX + ".*");

                        // il faut verifier les objets attaches
                        // attaches ajoute/supprime
                        // on ne traite ici que les TreeNode sans modif d'indexation
                        // pour les autres les attachments seront traites dans
                        // la phase suivante
                        Set<String> newAtt = WikittyTreeNodeHelper.getAttachment(w);

                        //Collection<String> oldAtt = SolrUtil.getStringFieldValues(
                        //        oldDoc,
                        //        WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_ATTACHMENT,
                        //        TYPE.WIKITTY);
                        String[] values = oldDoc.getValues(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_ATTACHMENT);
                        Collection<String> oldAtt = Arrays.asList(values);

                        // il faut supprimer l'indexation arbre des noeuds
                        // qui sont dans old, mais pas dans new
                        Set<String> toRemove = new HashSet<String>();
                        if (oldAtt != null) {
                            toRemove.addAll(oldAtt);
                        }
                        if (newAtt != null) {
                            toRemove.removeAll(newAtt);
                        }
                        attachmentInTree.remove(id, toRemove);
                        // il faut ajouter l'indexation arbre des noeuds
                        // qui sont dans new, mais pas dans old
                        Set<String> toAdd = new HashSet<String>();
                        if (newAtt != null) {
                            toAdd.addAll(newAtt);
                        }
                        if (oldAtt != null) {
                            toAdd.removeAll(oldAtt);
                        }
                        attachmentInTree.add(id, toAdd);
                    }
                }
                addedDocs.put(doc.get(LUCENE_ID), doc);
            }

            //
            // Phase 2: on reindexe tous les TreeNode qui en ont besoin
            //          nouveau TreeNode ou TreeNode ayant un parent modifie
            //

            // on ajoute tous les TreeNode qui doivent aussi etre reindexe
            // noeud du sous arbre d'un noeud dont le pere a ete modifie
            dirtyParent.addAll(dirtyParentDoc.keySet());

            for (String id : dirtyParent) {
                // w et oldDoc peuvent etre null, mais pas en meme temps
                // w est null si c'est un noeud dont la reindexation est force
                // parce que un de ces peres a change de parent
                // oldDoc est null, si l'objet n'a jamais ete indexe (nouveau)
                Wikitty w = allWikitties.get(id);
                Document oldDoc = dirtyParentDoc.get(id);
                Document doc = addedDocs.get(id);
                if (w == null) {
                    // on reindexe un ancien objet
                    // normalement doc doit etre null
                    doc = new Document();
                    // on recopie tous les champs, sauf l'indexation arbre
                    LuceneUtil.copyLuceneDocumentExcludeSomeField(
                            oldDoc, doc, TREENODE_PREFIX + ".*");

                    // modifie les champs root, parents
                    addTreeIndexField(addedDocs, doc, parents);

                    attachmentInTree.remove(oldDoc);
                    attachmentInTree.add(oldDoc);
                } else if (oldDoc == null) {
                    // ajoute les champs root, parents
                    addTreeIndexField(addedDocs, doc, parents);

                    // on indexe un nouvel objet, il faut ajouter tous les
                    // attachment pour indexation
                    attachmentInTree.add(w);
                } else {
                    // ni w, ni oldDoc ne sont pas null, c'est une modification
                    // dans la phase precendente on a deja indexe les champs
                    // normaux

                    // ajoute les champs root, parents
                    addTreeIndexField(addedDocs, doc, parents);

                    // il faut supprimer tous les anciens attaches
                    // et ajouter tous nouveaux pour la reindexation
                    attachmentInTree.remove(oldDoc);
                    attachmentInTree.add(w);
                }
                //solrResource.addDoc(id, doc);
                addedDocs.put(doc.get(LUCENE_ID), doc);
            }

            //
            // Phase 3: on reindexe les attachments qui en ont besoin
            //

            addTreeIndexField(writer, searcher, addedDocs, null, attachmentInTree);

            // on ne peut faire le add reel qu'à ce moment
            // car les meme doc sont ajouter plusieurs fois dans la map
            for (Document doc : addedDocs.values()) {
                writer.addDocument(doc);
            }
            writer.commit();
        } catch (Exception eee) {
            throw new WikittyException("Can't store wikitty", eee);
        } finally {
            IOUtils.closeQuietly(writer);
            IOUtils.closeQuietly(searcher);
        }
        timeLog.log(startTime, "store", String.format(
                "nb %s in force mode %s", wikitties.size(), force));
    }
    
    /**
     * Modifie/Ajoute les champs specifique a l'indexation des arbres sur les
     * TreeNode.
     *
     * On se base sur le fait que si un TreeNode est dans {@link SolrResource} il ne
     * peut etre que dans deux etats. Soit il a ete reindexe pour les arbres
     * et il a les champs d'indexation arbre. Soit il a pas encore ete reindexe
     * pour les arbres et dans ce cas il ne doit pas avoir les champs d'indexation
     * d'arbre. (il est donc interdit d'avoir des champs d'indexation arbre
     * obsolete si le document est dans {@link SolrResource})
     *
     * @param solrResource solR resource
     * @param doc les documents representant le TreeNode
     * @param tree tous les autres noeuds d'arbre dont on pourrait avoir
     * besoin pour l'indexation
     */
    protected void addTreeIndexField(Map<String, Document> addedDocs, Document doc, Map<String, Document> tree) {
        Set<String> parents = new HashSet<String>();
        String root = null;
        String treeNodeId = doc.get(LUCENE_ID);
        String parentId = treeNodeId;
        if (parentId == null) {
            throw new WikittyException("parentId is null, but this must be impossible");
        }
        parents.add(parentId);
        while (root == null) {
            String nextParentId = null;
            Document parentDoc = addedDocs.get(parentId);
            if (parentDoc != null) {
                // si parentDoc a deja ete indexe pour l'arbre, on peut reutiliser
                // directement les valeurs et sortir de la boucle
                if (parentDoc.get(TREENODE_ROOT) != null) {
                    root = parentDoc.get(TREENODE_ROOT);
                    //Collection<String> p = SolrUtil.getStringFieldValues(parentDoc, TREENODE_PARENTS);
                    String[] values = parentDoc.getValues(TREENODE_PARENTS);
                    Collection<String> p = Arrays.asList(values);
                    parents.addAll(p);
                    break;
                } else {
                    //nextParentId = SolrUtil.getStringFieldValue(parentDoc,
                    //        WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_PARENT,
                    //        TYPE.WIKITTY);
                    nextParentId = parentDoc.get(
                            WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_PARENT);
                }
            } else {
                Document oldParentDoc = tree.get(parentId);
                if (oldParentDoc != null) {
                    // si parentDoc a deja ete indexe pour l'arbre, on peut reutiliser
                    // directement les valeurs et sortir de la boucle
                    if (oldParentDoc.get(TREENODE_ROOT) != null) {
                        //root = SolrUtil.getStringFieldValue(oldParentDoc,TREENODE_ROOT);
                        root = oldParentDoc.get(TREENODE_ROOT);
                        //Collection<String> p = SolrUtil.getStringFieldValues(oldParentDoc, TREENODE_PARENTS);
                        String[] values = oldParentDoc.getValues(TREENODE_PARENTS);
                        Collection<String> p = Arrays.asList(values);
                        parents.addAll(p);
                        break;
                    } else {
                        //nextParentId = SolrUtil.getStringFieldValue(oldParentDoc,
                        //        WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_PARENT,
                        //        TYPE.WIKITTY);
                        nextParentId = oldParentDoc.get(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_PARENT);
                    }
                }
            }
            if (nextParentId != null) {
                if (parents.contains(nextParentId)) {
                    log.error(String.format("Tree with TreeNode '%s' have loop"
                            + " at node %s->%s all parents are %s. Set root with"
                            + " last valide parent '%s'",
                            treeNodeId, parentId, nextParentId, parents, parentId));
                    root = parentId;
                } else {
                    parents.add(nextParentId);
                    parentId = nextParentId;
                }
            } else {
                root = parentId;
            }
        }

        doc.removeField(TREENODE_ROOT);
        doc.removeField(TREENODE_PARENTS);
        doc.removeField(TREENODE_DEPTH);

        /*doc.addField(TREENODE_ROOT, root);
        doc.addField(TREENODE_DEPTH, parents.size());
        for (String id : parents) {
            doc.addField(TREENODE_PARENTS, id);
        }*/
        doc.add(new Field(TREENODE_ROOT, root, Store.YES, Index.NOT_ANALYZED));
        doc.add(new Field(TREENODE_DEPTH, String.valueOf(parents.size()), Store.YES, Index.NOT_ANALYZED));
        for (String id : parents) {
            doc.add(new Field(TREENODE_PARENTS, id, Store.YES, Index.NOT_ANALYZED));
        }
    }
    
    /**
     * Update attached extra field on all objects passed in argument
     * allAttachmentToIndex
     *
     * @param solrResource must contains reindexed TreeNode, that contains attachment
     * @param tree solr document for some TreeNode (used when TreeNode not find in solrResource)
     * @param attachmentInTree attachment added and removed from TreeNode
     * @throws IOException 
     */
    protected void addTreeIndexField(IndexWriter indexWriter, IndexSearcher indexSearcher, Map<String, Document> addedDocs,
            Map<String, Document> tree, AttachmentInTree attachmentInTree) throws IOException {

        if (attachmentInTree.size() > 0) {
            Map<String, Document> attachments = findAllById(indexSearcher, attachmentInTree.getAll());

            for (String treeNodeId : attachmentInTree.getRemoved().keySet()) {
                for (String attId : attachmentInTree.getRemoved().get(treeNodeId)) {
                    Document oldDoc = attachments.get(attId);
                    Document doc = addedDocs.get(attId);
                    if (oldDoc != null || doc != null) {
                        if (doc == null) {
                            doc = new Document();
                            LuceneUtil.copyLuceneDocument(oldDoc, doc);
                            addedDocs.put(attId, doc);
                        }
                        doc.removeField(TREENODE_ATTACHED + treeNodeId);
                        doc.removeField(TREENODE_ATTACHED_ALL);
                    }
                }
            }

            for (String treeNodeId : attachmentInTree.getAdded().keySet()) {
                Collection<String> treeNodeParents = null;
                Document treeNodeDoc = addedDocs.get(treeNodeId);
                if (treeNodeDoc != null) {
                    //treeNodeParents = SolrUtil.getStringFieldValues(
                    //        treeNodeDoc, TREENODE_PARENTS);
                    String[] treeNodeParent = treeNodeDoc.getValues(TREENODE_PARENTS);
                    treeNodeParents = Arrays.asList(treeNodeParent);
                } else if (tree != null) {
                    Document doc = tree.get(treeNodeId);
                    //treeNodeParents = SolrUtil.getStringFieldValues(
                    //        doc, TREENODE_PARENTS);
                    String[] treeNodeParent = doc.getValues(TREENODE_PARENTS);
                    treeNodeParents = Arrays.asList(treeNodeParent);
                } else {
                    log.error("Lucene doc not found in Transaction or in tree."
                            + "This is a bug !!!");
                }
                // add tree indexation on all attachments for this treeNodeId
                for (String attId : attachmentInTree.getAdded().get(treeNodeId)) {
                    Document oldDoc = attachments.get(attId);
                    Document doc = addedDocs.get(attId);
                    // il faut que oldDoc ou doc soit different de null pour
                    // pouvoir ajouter l'indexation d'arbre. Le cas on les deux
                    // sont nuls arrivent lorsqu'on demande la sauvegarde d'un
                    // TreeNode alors qu'on a pas encore sauve les attachments
                    // (ex: durant un syncSearchEngine ou l'on demande une
                    // reindexation totale). Ceci n'est pas un probleme car
                    // les attachments seront convenablement indexes lorsqu'ils
                    // seront ajoutes
                    if (oldDoc == null && doc == null) {
                        // L'objet en attachment du TreeNode, n'est pas encore
                        // store et n'est pas dans le meme appel de la methode
                        // store. On cree donc un document Partiel (seulement
                        // constitue de l'id et de l'indexation d'arbre qui sera
                        // reutilise lors du store de reel objet
                        doc = new Document();
                        addToIndexDocument(doc, null, LUCENE_ID, attId, true);
                    } else if (doc == null) {
                        doc = new Document();
                        LuceneUtil.copyLuceneDocument(oldDoc, doc);
                        //solrResource.addDoc(attId, doc);
                        addedDocs.put(attId, doc);
                    }
                    doc.removeField(TREENODE_ATTACHED + treeNodeId);
                    for (String id : treeNodeParents) {
                        //doc.addField(TREENODE_ATTACHED + treeNodeId, id);
                        doc.add(new Field(TREENODE_ATTACHED + treeNodeId, id, Store.YES, Index.NOT_ANALYZED));
                        doc.add(new Field(TREENODE_ATTACHED_ALL, id, Store.NO, Index.NOT_ANALYZED));
                    }
                }
            }
        }
    }

    /**
     * Ajoute un champs dans un document a indexer
     */
    protected void addToIndexDocument(Document doc,
            WikittyTypes type, String fqfieldName, String fieldValue,
            boolean collection) {
        if (fqfieldName.startsWith(WIKITTY_LUCENE_PREFIX)) {
            doc.removeField(fqfieldName);
            //doc.addField(fqfieldName, fieldValue);
            doc.add(new Field(fqfieldName, fieldValue, Store.YES, Index.NOT_ANALYZED));
        } else {
            String solrFqFieldName;
   /*         
//        FIXME REMOVE IT if search on multivalued work with new hack (specific sortable field
//            if (collection) {
//                solrFqFieldName = SolrUtil.getSolrCollectionFieldName(fqfieldName, type);
//            } else {
            // add suffix like _s for string type ex: myExt.myField_s
            solrFqFieldName = SolrUtil.getSolrFieldName(fqfieldName, type);
//            }

            // #all.<fieldname>
            // permet de faire des recherches inter extension sur un champs ayant
            // le meme nom. ex:Person.name et User.name
            // Quoi qu'il arrive pour le #all on utilise du multivalue
//        FIXME REMOVE IT if search on multivalued work with new hack (specific sortable field
//            String solrAllFqFieldName = SolrUtil.getSolrCollectionFieldName(fqfieldName, type);
            String solrAllFieldName = SOLR_ALL_EXTENSIONS
                    + WikittyUtil.FQ_FIELD_NAME_SEPARATOR
                    + WikittyUtil.getFieldNameFromFQFieldName(solrFqFieldName);

            // idem mais un champs sur plusieurs extension peut avoir des types
            // different, on ajoute donc un champs pour la recherche fulltext
            String solrFulltextAllFieldName = SOLR_FULLTEXT_ALL_EXTENSIONS
                    + WikittyUtil.FQ_FIELD_NAME_SEPARATOR
                    + WikittyUtil.getFieldNameFromFQFieldName(fqfieldName);

            String solrNullFieldFqFieldName = SOLR_NULL_FIELD + fqfieldName;

            // sortable solr field name for this field ex: myExt.myField_s_sortable
            String solrFqFieldNameSortable = solrFqFieldName + SUFFIX_SORTABLE;

            doc.remove(solrFqFieldName);          // myExt.myField_s
            doc.remove(solrNullFieldFqFieldName); // #null_field-myExt.myField
            doc.remove(solrAllFieldName);         // #all.myField_s
            doc.remove(solrFulltextAllFieldName); // #fulltext.all.myField
            doc.remove(solrFqFieldNameSortable);  // myExt.myField_s_sortable

            String solrNullFieldFqFieldNameValue = "true";
            if(fieldValue != null) {
                doc.addField(solrFqFieldName, fieldValue);
                doc.addField(solrAllFieldName, fieldValue);
                doc.addField(solrFulltextAllFieldName, fieldValue);
                Object oneFieldValue = SolrUtil.getOneValue(fieldValue);
                doc.addField(solrFqFieldNameSortable, oneFieldValue);
                solrNullFieldFqFieldNameValue = "false";
                if (log.isTraceEnabled()) {
                    log.trace(String.format("index field '%s' with value '%s'",
                            solrFqFieldName,
                            StringUtils.abbreviate(String.valueOf(fieldValue), 50)));
                }
            }
            doc.addField(solrNullFieldFqFieldName, solrNullFieldFqFieldNameValue);*/
        }
    }

    /**
     * Convert a wikitty object to a lucene {@link Document}.
     * 
     * Le schema est le suivant:
     * - n'est stocké (Store.YES) que ce qui peut servir à faire des facettes
     * - n'est not tokennisé (Index.NOT_ANALYZED) ce qui peut servir a faire des facettes
     * - dans le cas où le champ peut porter sur des requettes like
     *   il faut le stocker une deuxieme fois, tokenisé (Index.ANALYZED) c'est
     *   à dire tokenisé par l'analyser
     * 
     * @param w wikitty to convert
     * @return lucene document
     */
    protected Document convertWikittyToDoc(Wikitty w) {

        Document document = new Document();

        // wikitty id
        Field luceneIdField = new Field(WikittyLuceneConstants.LUCENE_ID, w.getId(),
                Store.YES, Index.NOT_ANALYZED);
        document.add(luceneIdField);

        // wikitty extension
        for (String extName : w.getExtensionNames()) {
            Field luceneExtField = new Field(WikittyLuceneConstants.LUCENE_EXTENSIONS,
                    extName, Store.YES, Index.NOT_ANALYZED);
            document.add(luceneExtField);
        }

        // all other wikitty fields
        for (String wikyttyField : w.getAllFieldNames()) {
            String ext = WikittyUtil.getExtensionNameFromFQFieldName(wikyttyField);
            String fieldName = WikittyUtil.getFieldNameFromFQFieldName(wikyttyField);

            boolean hasFieldValue = false;

            // field value storing
            FieldType type = w.getFieldType(wikyttyField);
            if (type.isCollection()) {
                List<String> values = w.getFieldAsList(ext, fieldName, String.class);
                // Store.YES = mandatory for facets
                // Index.NOT_ANALYZED = mandatory for search on field
                if (values != null) {
                    hasFieldValue = true;
                    for (String value : values) {
                        addSingleFieldToDocument(document, wikyttyField, type, value);
                    }
                }
            } else {
                String value = w.getFieldAsString(ext, fieldName);
                if (StringUtils.isNotEmpty(value)) {
                    hasFieldValue = true;
                    addSingleFieldToDocument(document, wikyttyField, type, value);
                }
            }
            
            // null field
            // not field (no stored, no values)
            Field luceneExtField = new Field(WikittyLuceneConstants.LUCENE_NULL_FIELD + wikyttyField,
                    String.valueOf(!hasFieldValue), Store.YES, Index.NOT_ANALYZED);
            document.add(luceneExtField);
        }

        return document;
    }

    /**
     * Ajout d'un champ unique dans un document lucene. Si la methode
     * est appelée plusieurs fois avec le même champ (fqFieldName), le champ
     * sera multivalué.
     * 
     * @param document lucene document
     * @param fqFieldName field name
     * @param type field type
     * @param value value
     */
    protected void addSingleFieldToDocument(Document document, String fqFieldName, FieldType type, String value) {
        if (log.isTraceEnabled()) {
            log.trace("add lucene field : " + fqFieldName + " = " + value);
        }

        // Store.YES = mandatory for facets
        // Index.NOT_ANALYZED = mandatory for strict equality
        Field luceneField = new Field(fqFieldName, value, Store.YES, Index.NOT_ANALYZED);
        document.add(luceneField);

        switch (type.getType()) {
        case STRING:

            // Store.NO = pas de facette dessus
            // Index.ANALYZED = mandatory for strict equality (pour recherche like)
            Field aLuceneField = new Field(fqFieldName + SUFFIX_ANALYZED, value, Store.NO, Index.ANALYZED);
            document.add(aLuceneField);

            // copy content to #fulltext field (multivalued)
            Field luceneFullTextField = new Field(LUCENE_DEFAULT_FIELD, value, Store.YES, Index.NOT_ANALYZED);
            document.add(luceneFullTextField);

            // copy content to #fulltext_a field (multivalued)
            Field aLuceneFullTextField = new Field(LUCENE_DEFAULT_FIELD + SUFFIX_ANALYZED, value, Store.NO, Index.ANALYZED);
            document.add(aLuceneFullTextField);
            
            break;
        case BINARY:
        case DATE:
        case BOOLEAN:
        case NUMERIC:
        case WIKITTY:
            break;
        default :
            throw new WikittyException("Not managed wikitty type : " + type.getType());
        }
    }

    /*
     * @see org.nuiton.wikitty.storage.WikittySearchEngine#delete(org.nuiton.wikitty.services.WikittyTransaction, java.util.Collection)
     */
    @Override
    public void delete(WikittyTransaction transaction, Collection<String> idList)
            throws WikittyException {

        // TODO manage transaction

        IndexWriter writer = null;
        try {
            // must be instancied every time (single instance per writer)
            IndexWriterConfig indexWriterConfig = new IndexWriterConfig(WIKITTY_LUCENE_VERSION, indexAnalyzer);
            writer = new IndexWriter(indexDirectory, indexWriterConfig);

            for (String id : idList) {
                Term term = new Term(WikittyLuceneConstants.LUCENE_ID, id);
                if (log.isDebugEnabled()) {
                    log.debug("Delete with query : " + term);
                }
                writer.deleteDocuments(term);
            }

            writer.commit();

        } catch (IOException ex) {
            try {
                writer.rollback();
            } catch (IOException e) {
                throw new WikittyException("Can't rollback index", ex);
            }
            throw new WikittyException("Can't store to index", ex);
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

    /*
     * @see org.nuiton.wikitty.storage.WikittySearchEngine#findAllByCriteria(org.nuiton.wikitty.services.WikittyTransaction, org.nuiton.wikitty.search.Criteria)
     */
    @Override
    public PagedResult<String> findAllByCriteria(WikittyTransaction transaction,
            Criteria criteria) throws WikittyException {

        // TODO manage transaction

        PagedResult<String> pagedResult = null;
        IndexReader indexReader = null;
        //IndexSearcher indexSearcher = null;
        try {
            indexReader = IndexReader.open(indexDirectory);
            //indexSearcher = new IndexSearcher(indexDirectory);

            // get lucene query
            Restriction restriction = criteria.getRestriction();
            Restriction2Lucene restriction2Lucene = new Restriction2Lucene(indexAnalyzer, fieldModifier);
            Query query = restriction2Lucene.toLucene(restriction);

            if (log.isDebugEnabled()) {
                log.debug("Performing search query : " + query.toString());
            }

            // configure sorting
            List<SortField> sortFields = new ArrayList<SortField>();
            List<String> sortAscending = criteria.getSortAscending();
            if (sortAscending != null) {
                for (String sort : sortAscending) {
                    //String tranform = fieldModifier.convertToSolr(transaction, sort);
                    //tranform += WikittySolrConstant.SUFFIX_SORTABLE;
                    String luceneField = LuceneUtil.getLuceneFieldName(sort);
                    SortField sortField = new SortField(luceneField, SortField.STRING);
                    sortFields.add(sortField);
                }
            }
            List<String> sortDescending = criteria.getSortDescending();
            if (sortDescending != null) {
                for (String sort : sortDescending) {
                    //String tranform = fieldModifier.convertToSolr(transaction, sort);
                    //tranform += WikittySolrConstant.SUFFIX_SORTABLE;
                    String luceneField = LuceneUtil.getLuceneFieldName(sort);
                    SortField sortField = new SortField(luceneField, SortField.STRING, true);
                    sortFields.add(sortField);
                }
            }

            // get search offset and limit
            int firstIndex = criteria.getFirstIndex();
            int endIndex = criteria.getEndIndex();
            if (endIndex < 0) {
                endIndex = indexReader.maxDoc(); // indexSearcher.maxDoc();
            }
            
            // facetting support : bobo
            List<String> facetFields = criteria.getFacetField();
            List<FacetHandler<?>> facetHandlers = new ArrayList<FacetHandler<?>>();
            if (facetFields != null) {
                for (String facetField : facetFields) {
                    String luceneField = LuceneUtil.getLuceneFieldName(facetField);
                    MultiValueFacetHandler facetHandler = new MultiValueFacetHandler(luceneField);
                    facetHandlers.add(facetHandler);
                }
            }
            BoboIndexReader boboReader = BoboIndexReader.getInstance(indexReader, facetHandlers);

            // get browse request
            BrowseRequest br = new BrowseRequest();
            br.setOffset(firstIndex);
            br.setCount(endIndex - firstIndex);
            br.setQuery(query);

            if (!sortFields.isEmpty()) {
                SortField[] sortFieldsA = sortFields.toArray(new SortField[sortFields.size()]);
                br.setSort(sortFieldsA);
            }

            // manage facets
            //Map<String, String> facetQueryToName = new HashMap<String, String>();
            
            //List<Criteria> facetCriterias = criteria.getFacetCriteria();
            if (facetFields != null) {
                for (String facetField : facetFields) {
                    FacetSpec facetSpec = new FacetSpec();
                    //facetSpec.setOrderBy(FacetSortSpec.OrderHitsDesc);
                    facetSpec.setMinHitCount(1);
                    String luceneField = LuceneUtil.getLuceneFieldName(facetField);
                    br.setFacetSpec(luceneField, facetSpec);
                }
            }
            
            //// manage criteria facets
            List<Criteria> facetCriterias = criteria.getFacetCriteria();
            if (facetCriterias != null) {
                throw new WikittyException("Facet criteria not supported yet");
            //    for (Criteria facetCriteria : facetCriterias) {
            //        Query facetQuery = restriction2Lucene.toLucene(facetCriteria.getRestriction());
            //    }
            }

            // perform request
            Browsable browser = new BoboBrowser(boboReader);
            BrowseResult result = browser.browse(br);

            // manage request results (real results)
            int pagedResultCount = result.getNumHits();
            BrowseHit[] hits = result.getHits();
            List<String> results = new ArrayList<String>();
            for (BrowseHit hit : hits) {
                //if (currentDocIndex >= firstIndex && currentDocIndex <= endIndex) {
                    int luceneId = hit.getDocid();
                    Document document = indexReader.document(luceneId);
                    String wikittyId = document.get(WikittyLuceneConstants.LUCENE_ID);
                    results.add(wikittyId);
                //}
            }
            
            // manage result facets
            Map<String, FacetAccessible> resultFacets = result.getFacetMap();
            Map<String, List<FacetTopic>> pagedResultFacets = new HashMap<String, List<FacetTopic>>();
            for (Map.Entry<String, FacetAccessible> resultFacet : resultFacets.entrySet()) {
                String facetName = resultFacet.getKey();
                FacetAccessible facetValue = resultFacet.getValue();
                String wikittyField = LuceneUtil.getWikittyFieldName(facetName);

                List<FacetTopic> facetTopics = new ArrayList<FacetTopic>();
                List<BrowseFacet> browseFacets = facetValue.getFacets();
                for (BrowseFacet browseFacet : browseFacets) {
                    FacetTopic topic = new FacetTopic(wikittyField, browseFacet.getValue(),
                            browseFacet.getFacetValueHitCount());
                    facetTopics.add(topic);
                }
                
                pagedResultFacets.put(wikittyField, facetTopics);
                
                // clean up. This is to free up the resources used in facet counting
                // and this is new for bobo 2.5. Very important to call.
                facetValue.close(); 
            }
            
            pagedResult = new PagedResult<String>(null, firstIndex, pagedResultCount,
                    query.toString(), pagedResultFacets, results);

            /*// execute search query
            List<String> results = new ArrayList<String>();
            int totalHitCount = 0;
            if (endIndex == 0) {
                // il faut un cas special pour le 0, sinon lucene rale.
                // Utilisation d'un collector qui ne fait que compter
                // le nombre de résultat
                TotalHitCountCollector collector = new TotalHitCountCollector();
                indexSearcher.search(query, null, collector);
                totalHitCount = collector.getTotalHits();
            } else {

                // ca où il y a vraiment des documents retourné par la requete
                TopDocs topDocs = null;
                if (sortFields.isEmpty()) {
                    topDocs = indexSearcher.search(query, null, endIndex);
                } else {
                    Sort sortOption = new Sort(sortFields.toArray(new SortField[sortFields.size()]));
                    topDocs = indexSearcher.search(query, null, endIndex, sortOption);
                }
                ScoreDoc[] scoreDocs = topDocs.scoreDocs;

                int currentDocIndex = 0;
                
                for (ScoreDoc scoreDoc : scoreDocs) {
                    if (currentDocIndex >= firstIndex && currentDocIndex <= endIndex) {
                        int luceneId = scoreDoc.doc;
                        Document document = indexSearcher.doc(luceneId);
                        String wikittyId = document.get(WikittyLuceneConstants.LUCENE_ID);
                        results.add(wikittyId);
                    }
                }
                
                totalHitCount = topDocs.totalHits;
                
                pagedResult = new PagedResult<String>(firstIndex, totalHitCount,
                    query.toString(), null, results);
            }*/

        } catch (IOException ex) {
            throw new WikittyException("Can't search on index", ex);
        } catch (BrowseException ex) {
            throw new WikittyException("Can't search on index", ex);
        } finally {
            IOUtils.closeQuietly(indexReader);
            //IOUtils.closeQuietly(indexSearcher);
        }

        return pagedResult;
    }

    /**
     * Look for a lucene document by id.
     * 
     * @param indexSearcher index searcher
     * @param id id to find
     * @return found document
     * @throws IOException 
     */
    protected Document findById(IndexSearcher indexSearcher, String id) throws IOException {
        return findByField(indexSearcher, LUCENE_ID, id);
    }
    
    /**
     * Look for a lucene document by id.
     * 
     * @param indexSearcher index searcher
     * @param fieldName field name to search into
     * @param fieldValue field value to find
     * @return found document
     * @throws IOException 
     */
    protected Document findByField(IndexSearcher indexSearcher, String fieldName, String fieldValue) throws IOException {
        Term term = new Term(fieldName, fieldValue);
        TermQuery termQuery = new TermQuery(term);
        TopDocs topDocs = indexSearcher.search(termQuery, null, 1);
        Document result = null;
        if (topDocs.totalHits == 1) {
            int docId = topDocs.scoreDocs[0].doc;
            result = indexSearcher.doc(docId);
        }
        return result;
    }

    /**
     * Look for a lucene document by id.
     * 
     * @param indexSearcher index searcher
     * @param id id to find
     * @return found document
     * @throws IOException 
     */
    protected Map<String, Document> findAllById(IndexSearcher indexSearcher, Collection<String> ids) throws IOException {
        return findAllByField(indexSearcher, LUCENE_ID, ids);
    }

    /**
     * Look for a lucene document by id.
     * 
     * @param indexSearcher index searcher
     * @param fieldName field name to search into
     * @param fieldValues field values to find
     * @return found document
     * @throws IOException 
     */
    protected Map<String, Document> findAllByField(IndexSearcher indexSearcher, String fieldName, Collection<String> fieldValues) throws IOException {
        BooleanQuery query = new BooleanQuery();
        for (String fieldValue : fieldValues) {
            Term term = new Term(fieldName, fieldValue);
            TermQuery termQuery = new TermQuery(term);
            query.add(termQuery, BooleanClause.Occur.SHOULD);
        }
        TopDocs topDocs = indexSearcher.search(query, null, 1000);
        Map<String, Document> result = new HashMap<String, Document>();
        for (ScoreDoc socreDoc : topDocs.scoreDocs) {
            int docId = socreDoc.doc;
            Document doc = indexSearcher.doc(docId);
            String wikittyId = doc.getFieldable(LUCENE_ID).stringValue();
            result.put(wikittyId, doc);
        }
        return result;
    }

    /*
     * @see org.nuiton.wikitty.storage.WikittySearchEngine#findAllChildrenCount(org.nuiton.wikitty.services.WikittyTransaction, java.lang.String, int, boolean, org.nuiton.wikitty.search.Criteria)
     */
    @Override
    public TreeNodeResult<String> findAllChildrenCount(
            WikittyTransaction transaction, String wikittyId, int depth,
            boolean count, Criteria filter) {

        TreeNodeResult<String> result = null;
        IndexSearcher searcher = null;
        try {
            searcher = new IndexSearcher(indexDirectory);

            Document luceneDoc = findById(searcher, wikittyId);
            if (luceneDoc != null) {
                
                // on verifie que l'argument est bien un TreeNode
                if (luceneDoc.getFieldable(TREENODE_DEPTH) != null) {
                    Search treeSearch = Search.query().and().eq(TREENODE_PARENTS, wikittyId);
                    if (depth >= 0) {
                        //Integer d = LuceneUtil.getIntFieldValue(luceneDoc, TREENODE_DEPTH);
                        String sField = luceneDoc.get(TREENODE_DEPTH);
                        Integer d = Integer.parseInt(sField);
                        treeSearch = treeSearch.bw(TREENODE_DEPTH,
                                String.valueOf(d), String.valueOf(d + depth));
                    }
                    Criteria treeCriteria = treeSearch.criteria();

                    // on a dans treeSearch uniquement le noeud passe en parametre
                    // et ses enfants jusqu'a la profondeur demandee
                    Restriction2Lucene restriction2Lucene =
                            new Restriction2Lucene(indexAnalyzer, fieldModifier);
                    Query query = restriction2Lucene.toLucene(treeCriteria.getRestriction());
                    TopDocs topDocs = searcher.search(query, null, 1000);
                    //SolrQuery query = new SolrQuery(SOLR_QUERY_PARSER + queryString);
                    //QueryResponse resp = SolrUtil.executeQuery(solrServer, query);
                    //SolrDocumentList solrResults = resp.getResults();

                    // recuperation si demande du nombre d'attachment par noeud
                    Map<String, Integer> counts = new HashMap<String, Integer>();
                    if (count) {
                        // TODO poussin 20110128 regarder si on ne peut pas
                        // restreindre les facettes aux noeuds trouve dans la recherche
                        // precedente
                        Criteria attCriteria = Search.query(filter).eq(
                                TREENODE_ATTACHED_ALL, wikittyId).criteria()
                                .setFirstIndex(0).setEndIndex(0)
                                .addFacetField(TREENODE_ATTACHED_ALL);
                        PagedResult<String> attSearch =
                                findAllByCriteria(transaction, attCriteria);
                        List<FacetTopic> topics = attSearch.getTopic(TREENODE_ATTACHED_ALL);
                        if (topics != null) {
                            for (FacetTopic topic : topics) {
                                String topicName = topic.getTopicName();
                                int topicCount = topic.getCount();
                                counts.put(topicName, topicCount);
                            }
                        }
                    }

                    // construction du resultat, il proceder en 2 phases car
                    // sinon si on construit un fils avant son pere, il ne sera
                    // jamais associe
                    Map<String, TreeNodeResult<String>> allTreeNodeResult =
                            new HashMap<String, TreeNodeResult<String>>();
                    // key: id de l'enfant, value: l'id du parent
                    Map<String, String> childParent = new HashMap<String, String>();
                    // construction de tous les TreeNodeResult qui permettront
                    // de construire l'arbre
                    for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                        Document doc = searcher.doc(scoreDoc.doc);
                        String id = doc.getFieldable(LUCENE_ID).stringValue();

                        String parentId = null;
                        Fieldable fieldable = doc.getFieldable(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_PARENT);
                        if (fieldable != null) {
                            parentId = fieldable.stringValue();
                        }
                        int nb = counts.containsKey(id) ? counts.get(id) : 0;
                        TreeNodeResult<String> child = new TreeNodeResult<String>(id, nb);
                        allTreeNodeResult.put(id, child);
                        childParent.put(id, parentId);
                    }
                    // construction de l'arbre avant de le retourner
                    for(Map.Entry<String, TreeNodeResult<String>> e : allTreeNodeResult.entrySet()) {
                        String id = e.getKey();
                        String parentId = childParent.get(id);
                        if (allTreeNodeResult.containsKey(parentId)) {
                            TreeNodeResult<String> child = e.getValue();
                            TreeNodeResult<String> parent = allTreeNodeResult.get(parentId);
                            if (parent != child) {
                                parent.add(child);
                            }
                        }
                    }
                    result = allTreeNodeResult.get(wikittyId);
                }
            }
        } catch (IOException ex) {
            throw new WikittyException("Can't search on index", ex);
        } finally {
            IOUtils.closeQuietly(searcher);
        }
        return result;
    }

    /*
     * @see org.nuiton.wikitty.storage.WikittySearchEngine#findAllByQuery(org.nuiton.wikitty.services.WikittyTransaction, org.nuiton.wikitty.query.WikittyQuery)
     */
    @Override
    public WikittyQueryResult<String> findAllByQuery(
            WikittyTransaction transaction, WikittyQuery queries) {
        return null;
    }

    /*
     * @see org.nuiton.wikitty.storage.WikittySearchEngine#findAllChildrenCount(org.nuiton.wikitty.services.WikittyTransaction, java.lang.String, int, boolean, org.nuiton.wikitty.query.WikittyQuery)
     */
    @Override
    public WikittyQueryResultTreeNode<String> findAllChildrenCount(
            WikittyTransaction transaction, String wikittyId, int depth,
            boolean count, WikittyQuery filter) {
        // TODO Auto-generated method stub
        return null;
    }
}
