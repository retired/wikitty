/*
 * #%L
 * 
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.storage.lucene;

/**
 * Constants used in wikitty lucene search engine.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class WikittyLuceneConstants {

    /**
     * Prefix utiliser pour les champs ajouter lors de l'indexation. Ce prefix
     * evite d'avoir des conflits entre un nom d'extension et un champs ajoute.
     */
    static final public String WIKITTY_LUCENE_PREFIX = "#";

    /** Id field in lucene. */
    static final public String LUCENE_ID = WIKITTY_LUCENE_PREFIX + "id";

    /** extensions field name in lucene */
    static final public String LUCENE_EXTENSIONS = WIKITTY_LUCENE_PREFIX + "extensions";
    
    /** extensions field name in lucene */
    static final public String LUCENE_DEFAULT_FIELD = WIKITTY_LUCENE_PREFIX + "fulltext";
    
    /** Use for indexation tree node */
    static final public String TREENODE_PREFIX = WIKITTY_LUCENE_PREFIX + "tree.";

    /** Use as field on TreeNode */
    static final public String TREENODE_ROOT = TREENODE_PREFIX + "root";

    /** Use as field on TreeNode, contains parent node id and himself node id */
    static final public String TREENODE_PARENTS = TREENODE_PREFIX + "parents";

    /** Use as field on TreeNode, number of parents (root node depth=1) */
    static final public String TREENODE_DEPTH = TREENODE_PREFIX + "depth";

    /** Use as field on Wikitty object attached on TreeNode, TreeNodeId is added at end */
    static final public String TREENODE_ATTACHED = TREENODE_PREFIX + "attached.";

    /** Use as field on Wikitty object attached on TreeNode, TreeNodeId is added at end used for facetisation */
    static final public String TREENODE_ATTACHED_ALL = TREENODE_PREFIX + "attached.all";
    
    /** if field is null, this extra field is set to true otherwise is set to false */
    static final public String LUCENE_NULL_FIELD = WIKITTY_LUCENE_PREFIX + "nullfield.";

    /** Suffixe pour les champs stockés en facon analysé. */
    static final public String SUFFIX_ANALYZED = "_a";
}
