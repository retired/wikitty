/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;

import java.util.Map;


/**
 * Abstract class for the skeleton and signature for wikittyPubText content
 * transform and compile as Java class.
 * 
 * This class allow the evaluator engine to call the eval method to return
 * the result of the compiled code from the wikitty pub text content.
 * 
 * 
 * @author mfortun
 *
 */
public abstract class AbstractDecoredClass {
    
       
    public abstract Object eval(Map<String, Object> bindings) throws Exception ;
    
    
}
