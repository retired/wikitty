/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;

/**
 * class used to store filter option used to decorate UI content inside 
 * wikittyPubText
 * 
 * @author mfortun
 *
 */
public class FilterOption {

    
    public static String OPENING_TEMPLATE = "OpeningTemplate";
    public static String WRITE_STRING = "WriteString";
    public static String STRING_DELIM = "StringDelim";
    public static String CONCAT_CHAR = "ConcatChar";
    public static String ENDING_CAR = "EndingCar";
    public static String CLOSING_WRITER_CHAR = "ClosingWriterChar";
    public static String OPENING_WRITER_CHAR = "OpeningWriterChar";
    public static String CLOSING_TEMPLATE = "ClosingTemplate";
    public static String KEY = "Key";
    
    
    protected String writeString;
    protected String stringDelim;
    protected String concatChar;
    protected String endingCar;
    protected String openingTemplate;
    protected String closingWriterChar;
    protected String closingTemplate;
    protected String key;

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getOpeningTemplate() {
        return openingTemplate;
    }
    public void setOpeningTemplate(String openingTemplate) {
        this.openingTemplate = openingTemplate;
    }
    public String getClosingTemplate() {
        return closingTemplate;
    }
    public void setClosingTemplate(String closingTemplate) {
        this.closingTemplate = closingTemplate;
    }
    public String getClosingWriterChar() {
        return closingWriterChar;
    }
    public void setClosingWriterChar(String closingWriterChar) {
        this.closingWriterChar = closingWriterChar;
    }
    public String getOpeningWriterChar() {
        return openingWriterChar;
    }
    public void setOpeningWriterChar(String openingWriterChar) {
        this.openingWriterChar = openingWriterChar;
    }
    protected String openingWriterChar;
    
    public String getWriteString() {
        return writeString;
    }
    public void setWriteString(String writeString) {
        this.writeString = writeString;
    }
    public String getStringDelim() {
        return stringDelim;
    }
    public void setStringDelim(String stringDelim) {
        this.stringDelim = stringDelim;
    }
    public String getConcatChar() {
        return concatChar;
    }
    public void setConcatChar(String concatChar) {
        this.concatChar = concatChar;
    }

    public String getEndingCar() {
        return endingCar;
    }
    public void setEndingCar(String endingCar) {
        this.endingCar = endingCar;
    }

    
    
    
}
