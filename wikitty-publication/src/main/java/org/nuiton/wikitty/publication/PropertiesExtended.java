/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * Class usefull when load properties file, update, delete, then save again File
 * used to load properties is store, and this allow to save again the properties
 * inside the file used to load them.
 * 
 * @author mfortun
 * 
 */
public class PropertiesExtended extends Properties {

    /**
     * 
     */
    private static final long serialVersionUID = -264337198024996529L;

    /**
     * The original file used to load and create properties
     */
    protected File origin;

    
    public File getOrigin() {
        return origin;
    }

    public void setOrigin(File origin) {
        this.origin = origin;
    }

    
    
    /**
     * Default constructor, need a file from whom load the properties. This is
     * equivalent of a basic creation of properties and load after property from
     * a file.
     * 
     * @param origin
     *            the file from whom load the property, it is save for store.
     * @throws IOException
     *             if error while reading the file
     * @throws FileNotFoundException
     *             if file not found
     */
    public PropertiesExtended(File origin) throws IOException {
        load(origin);

    }


    /**
     * Load the property from the file, and store the file, for storage.
     * 
     * @param file
     *            the file from whom to load the property, it replace the file
     *            use to create this class
     * @throws IOException
     *             if error while reading the file
     * @throws FileNotFoundException
     *             if file not found
     */
    public void load(File file) throws IOException {
        origin = file;
        FileReader fr = new FileReader(origin);
        try {
            load(fr);
        } finally {
            fr.close();
        }
    }

    /**
     * Store the properties inside the last file used to load the property (or
     * by default file used to create the class), equivalent of store(new
     * FileWriter('File'),"");
     * 
     * @throws IOException
     *             if error while reading the file
     * @throws FileNotFoundException
     *             if file not found
     */
    public void store() throws IOException {
        FileWriter fs = new FileWriter(origin);
        try {
            store(fs, "");
        } finally {
            fs.close();
        }

    }

}
