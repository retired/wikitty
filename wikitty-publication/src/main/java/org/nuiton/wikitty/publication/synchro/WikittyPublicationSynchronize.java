/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin mfortun
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.synchro;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.ArgumentsParserException;
import org.nuiton.util.FileUtil;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyServiceFactory;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.entities.WikittyLabelHelper;
import org.nuiton.wikitty.publication.PropertiesExtended;
import org.nuiton.wikitty.publication.WikittyFileUtil;
import org.nuiton.wikitty.publication.entities.WikittyPubData;
import org.nuiton.wikitty.publication.entities.WikittyPubText;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.Search;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Main class of the sync part of wikitty publication, this class is the entry
 * point for sync operation. Existing, delete and update.
 * 
 * 
 * @author mfortun
 * 
 */
public class WikittyPublicationSynchronize {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory
            .getLog(WikittyPublicationSynchronize.class);

    /**
     * Key for the other uri, usefull in the case of commit/update with a file
     * system wikitty service.
     */
    static public String WIKITTY_SERVICE_INTERLOCUTEUR = "wikitty.service.interlocuteur";

    static protected ApplicationConfig applicationConfig;

    /**
     * for recursion option
     */
    static public String IS_RECURSION_OPTION = "isRecur";
    /**
     * for delete option
     */
    static public String IS_DELETE_OPTION = "delete";
    /**
     * for existing option
     */
    static public String IS_EXISTING_OPTION = "existing";

    /**
     * Use to save the label by the wikitty publication file system
     */
    static public String LABEL_KEY = "working.label";

    /**
     * the string that mark the beginnning of the label
     */
    static public String LABEL_DELIM = "#";
    /**
     * regex to select the label part of the uri
     */
    static public String LABEL_REGEX = "\\#.*";

    /**
     * the prefix use in file url
     */
    static public String FILE_URI_PREFIX = "file";

    /**
     * the prefix use in cajo url
     */
    static public String CAJO_URI_PREFIX = "cajo";

    /**
     * the prefix use in hessian url
     */
    static public String HESSIAN_URI_PREFIX = "hessian";

    /**
     * The file name of the wikitty service property
     */
    static public String WIKITTY_FILE_SERVICE = "ws.properties";

    /*
     * Two flag for commit/update mechanism if main true and flag commit false
     * and if service is servive over file system, so write property to "know"
     * the last service used to synchronise
     */
    static protected boolean MAIN_ENABLE = false;
    static protected boolean FLAG_COMMIT_UPDATE = false;

    static {
        applicationConfig = new ApplicationConfig();

        /*
         * needed to ensure that fileUtils used inside execution is set with the
         * correct encoding
         */
        FileUtil.ENCODING = "UTF-8";

        /*
         * TODO mfortun-2011-04-14 construct option def instance to initialize
         * correctly application config
         */

        applicationConfig.setDefaultOption(IS_DELETE_OPTION, "false");
        applicationConfig.setDefaultOption(IS_EXISTING_OPTION, "false");
        applicationConfig.setDefaultOption(IS_RECURSION_OPTION, "true");

        // allias for norecursion
        applicationConfig.addAlias("--norecursion", "--option",
                IS_RECURSION_OPTION, "false");

        applicationConfig.addAlias("--delete", "--option", IS_DELETE_OPTION,
                "true");

        applicationConfig.addAlias("--existing", "--option",
                IS_EXISTING_OPTION, "true");

        // allias for all the action
        applicationConfig.addAlias("wp sync", "--option", "sync");

        applicationConfig.addAlias("wp commit", "--option", "commit");

        applicationConfig.addAlias("wp update", "--option", "update");

        applicationConfig
                .addActionAlias("sync",
                        "org.nuiton.wikitty.publication.synchro.WikittyPublication#synchronisation");

        applicationConfig
                .addActionAlias("commit",
                        "org.nuiton.wikitty.publication.synchro.WikittyPublication#commit");

        applicationConfig
                .addActionAlias("update",
                        "org.nuiton.wikitty.publication.synchro.WikittyPublication#update");
    }

    /*
     * Class don't have to be instantiate
     */
    private WikittyPublicationSynchronize() {

    }

    /**
     * @param args
     * @throws ArgumentsParserException
     */
    static public void main(String[] args) throws Exception {
        MAIN_ENABLE = true;

        // parsing
        applicationConfig.parse(args);
        // execution
        applicationConfig.doAction(0);
    }

    static public void synchronisation(String origin, String target)
            throws URISyntaxException {

        boolean isRecur = applicationConfig
                .getOptionAsBoolean(IS_RECURSION_OPTION);

        boolean isDelete = applicationConfig
                .getOptionAsBoolean(IS_DELETE_OPTION);
        boolean isExisting = applicationConfig
                .getOptionAsBoolean(IS_EXISTING_OPTION);

        synchronisationServices(origin, target, isRecur, isDelete, isExisting);
    }

    /**
     * Synchronize the wikitty service designed by their Uri
     * @param origin Uri of the wikitty service from
     * @param target Uri of the wikitty service to
     * @param isRecur If recursion for synchronize (only label or label and sub label)
     * @param isDelete if only delete wikitty that are on origin but not on target
     * @param isExisting if only update wikitty and not send wikitty that are on origin and not on target
     * @throws URISyntaxException
     */
    static public void synchronisationServices(String origin, String target,
            boolean isRecur, boolean isDelete, boolean isExisting)
            throws URISyntaxException {

        // update operation is the default operation
        boolean isUpdate = !isDelete && !isExisting;

        log.info("Sync uri origin: " + origin + " uri target: " + target
                + " isRecur:" + isRecur + " isUpdate:" + isUpdate
                + " isDelete:" + isDelete + "isExisting:" + isExisting);

        URI uriOrigin = new URI(origin);

        URI uriTarget = new URI(target);

        /*
         * necessary to have property correctly initialize in order to obtain
         * the correct implementation of the wikitty service
         */

        // once on the service origin
        applicationConfig.setOption(WIKITTY_SERVICE_INTERLOCUTEUR, new String(
                target.replaceAll(LABEL_REGEX, StringUtils.EMPTY)));

        ApplicationConfig temp1 = setUpApplicationConfigServerConnector(uriOrigin);

        WikittyProxy proxyOrigin = new WikittyProxy(
                WikittyServiceFactory.buildWikittyService(temp1));

        // store the other uri in the application, if the service is a
        // wikittypublication file system
        // it can need it to store wikittyservice property
        applicationConfig.setOption(WIKITTY_SERVICE_INTERLOCUTEUR, new String(
                origin.replaceAll(LABEL_REGEX, StringUtils.EMPTY)));
        // once on the service target
        ApplicationConfig temp2 = setUpApplicationConfigServerConnector(uriTarget);

        WikittyProxy proxyTarget = new WikittyProxy(
                WikittyServiceFactory.buildWikittyService(temp2));

        String labelOrigin = uriOrigin.getFragment();
        String labelTarget = uriTarget.getFragment();

        Criteria critOrigin = constructCriteriaLabelRecur(labelOrigin, isRecur);
        Criteria critTarget = constructCriteriaLabelRecur(labelTarget, isRecur);

        List<String> listOrigin = proxyOrigin.findAllIdByCriteria(critOrigin)
                .getAll();
        List<String> listTarget = proxyTarget.findAllIdByCriteria(critTarget)
                .getAll();

        // construct list of wikitty contained in both location
        List<String> existInBoth = new ArrayList<String>();
        existInBoth.addAll(listOrigin);
        existInBoth.retainAll(listTarget);

        // construct list of wikitty contained only in origin location
        List<String> existOnlyOnOrigin = new ArrayList<String>();
        existOnlyOnOrigin.addAll(listOrigin);
        existOnlyOnOrigin.removeAll(listTarget);

        // construct list of wikitty contained only in target location
        List<String> existOnlyOnTarget = new ArrayList<String>();
        existOnlyOnTarget.addAll(listTarget);
        existOnlyOnTarget.removeAll(listOrigin);

        if (log.isDebugEnabled()) {
            log.debug("Wikitty exist on both: " + existInBoth.size());
            for (String ex : existInBoth) {
                log.debug(ex);
            }

            log.debug("Wikitty exist only on origin: "
                    + existOnlyOnOrigin.size());
            for (String ex : existOnlyOnOrigin) {
                log.debug(ex);
            }

            log.debug("Wikitty exist only on target: "
                    + existOnlyOnTarget.size());
            for (String ex : existOnlyOnTarget) {
                log.debug(ex);
            }
        }

        /*
         * FIXME mfortun-2011-04-27 remove all that stuff for the safety of the
         * version when a solution rise for the wikitty version
         */

        /*
         * if option is update send wikitty that are not in the target.
         */
        if (isUpdate) {

            log.info("Store on target new wikitty");

            List<Wikitty> newWikitties = proxyOrigin.restore(existOnlyOnOrigin);

            for (Wikitty wikittyNew : newWikitties) {

                Set<String> saveLabelOrigin = WikittyLabelHelper
                        .getLabels(wikittyNew);

                Set<String> targetLabels = new HashSet<String>();

                // prepare set of target label
                // we save all the label except the one corresponding
                // to origin, determine by isRecur
                for (String labels : saveLabelOrigin) {

                    if (isRecur && labels.startsWith(labelOrigin)) {
                        String finalLabelTarge = new String(labels.replace(
                                labelOrigin, labelTarget));
                        targetLabels.add(finalLabelTarge);
                    } else if (!isRecur && labels.equals(labelOrigin)) {
                        targetLabels.add(labelTarget);
                    } else {
                        targetLabels.add(labels);
                    }

                }

                // save the version before reset label
                String wikittyVersionLocal = wikittyNew.getVersion();
                WikittyLabelHelper.setLabels(wikittyNew, targetLabels);
                // restore the version
                wikittyNew.setVersion(wikittyVersionLocal);
                proxyTarget.store(wikittyNew);

                String versionSaveTarget = wikittyNew.getVersion();

                WikittyLabelHelper.setLabels(wikittyNew, saveLabelOrigin);
                wikittyNew.setVersion(versionSaveTarget);

                // we re store on the origin to ensure wikitty version

                try {
                    proxyOrigin.store(wikittyNew);
                } catch (Exception e) {

                    // FIXME when a wikitty service store a
                    // wikitty that he does'nt know
                    // he set the version to 1.0
                    e.printStackTrace();
                }
            }

        }
        /*
         * if option delete remove those who are on the target but not on origin
         */
        if (isDelete) {

            log.info("Remove from target deleted wikitty");

            for (String id : existOnlyOnTarget) {

                Wikitty w = proxyTarget.restore(id);

                WikittyLabelHelper.removeLabels(w, labelTarget);

                proxyTarget.store(w);

            }
        } else {

            log.info("Update existing wikitty");
            /*
             * case existing and update, update those which are on both location
             */
            for (String id : existInBoth) {
                Wikitty fromTarget = proxyTarget.restore(id);
                Wikitty fromOrigin = proxyOrigin.restore(id);

                String versionTarget = fromTarget.getVersion();

                String versionOrigin = fromOrigin.getVersion();

                // check version for update

                /*
                 * we replace origin labels by target's labels if we udpate.
                 */
                if (WikittyUtil
                        .versionGreaterThan(versionOrigin, versionTarget)) {
                    Set<String> setLabelTarget = WikittyLabelHelper
                            .getLabels(fromTarget);

                    Set<String> setLabelOrigin = WikittyLabelHelper
                            .getLabels(fromOrigin);

                    // save version before reset the label
                    String versionLocalSave = fromOrigin.getVersion();
                    // replace labels origins, by targets label
                    WikittyLabelHelper.setLabels(fromOrigin, setLabelTarget);
                    // restore the version
                    fromOrigin.setVersion(versionLocalSave);
                    // send modified origin to target wikitty service
                    proxyTarget.store(fromOrigin);
                    // re store on origin to ensure version is the same on both

                    // save the version case re set labels increment version
                    String saveVersion = fromOrigin.getVersion();
                    // re set correctly labels
                    WikittyLabelHelper.setLabels(fromOrigin, setLabelOrigin);
                    // re set the version
                    fromOrigin.setVersion(saveVersion);

                    // re store the wikitty with the correct version
                    // and labels
                    proxyOrigin.store(fromOrigin);
                }
            }

        }// */

    }

    /**
     * Used to construct criteria on wikittypubdata and pubtext on the
     * wikittylabel extension
     * 
     * @param label
     *            the label criteria
     * @param isRecur
     *            is recusion
     * @return the constructed criteria
     */
    static protected Criteria constructCriteriaLabelRecur(String label,
            boolean isRecur) {

        log.info("Construct criteria with label: " + label + " isRecur:"
                + isRecur);

        // Construct the criteria
        Criteria criteriaOnLabels;
        Search mainRequest = Search.query();
        Search subRoqu = mainRequest.or();

        // must have the type of wikittypubtext/wikittypubdata
        subRoqu.exteq(WikittyPubText.EXT_WIKITTYPUBTEXT).exteq(
                WikittyPubData.EXT_WIKITTYPUBDATA);
        if (isRecur) {

            // and extension with the name that containt the label (recursivity)
            criteriaOnLabels = mainRequest.exteq(WikittyLabel.EXT_WIKITTYLABEL)
                    .sw(WikittyLabel.FQ_FIELD_WIKITTYLABEL_LABELS, label)
                    .criteria();

        } else {

            // and extension with the name strictly equals to the label (no
            // recursivity)
            criteriaOnLabels = mainRequest.exteq(WikittyLabel.EXT_WIKITTYLABEL)
                    .eq(WikittyLabel.FQ_FIELD_WIKITTYLABEL_LABELS, label)
                    .criteria();

        }

        log.debug(criteriaOnLabels);

        return criteriaOnLabels;

    }

    /**
     * Use to setup correct url property in the application config and correct
     * component for the wikittyservice
     * 
     * @param uri
     *            of the targeted wikitty service
     */
    static protected ApplicationConfig setUpApplicationConfigServerConnector(
            URI uri) {

        log.info("Construct application config for uri: " + uri);

        // prepare new application config
        ApplicationConfig result = new ApplicationConfig();

        // transfert main properties to new application config
        result.setOptions(applicationConfig.getFlatOptions());
        String url = uri.toASCIIString();

        if (uri.getScheme().equals(FILE_URI_PREFIX)) {

            result.setOption(
                    WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS
                            .getKey(), WikittyPublicationFileSystem.class
                            .getName());

            writeProperty(url);
        } else if (uri.getScheme().equals(CAJO_URI_PREFIX)) {
            result.setOption(
                    WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS
                            .getKey(),
                    "org.nuiton.wikitty.services.WikittyServiceCajoClient");

            // remove fragment from the uri.
            url = new String(url.replaceAll(LABEL_REGEX, StringUtils.EMPTY));

        } else if (uri.getScheme().equals(HESSIAN_URI_PREFIX)) {
            result.setOption(
                    WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS
                            .getKey(),
                    "org.nuiton.wikitty.services.WikittyServiceHessianClient");
            // remove fragment from the uri.
            url = new String(url.replaceAll(LABEL_REGEX, StringUtils.EMPTY));
        }

        // set protocol to http, no use finally
        /*
         * url = url.replaceFirst("["+uri.getScheme()+"]", "http");
         */

        log.info("set url "
                + url
                + " with component :"
                + result.getOption(WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS
                        .getKey()));

        result.setOption(WikittyConfigOption.WIKITTY_SERVER_URL.getKey(), url);

        log.debug("Application config: " + result.getFlatOptions());

        return result;

    }

    static public void update(String label, String... uriFileSystem)
            throws Exception {

        // only difference between update and commit is the param's order
        // when calling synchronisation method
        commitUpdateDelegate(label, false, uriFileSystem);

    }

    static public void commit(String label, String... uriFileSystem)
            throws Exception {

        // only difference between update and commit is the param's order
        // when calling synchronisation method
        commitUpdateDelegate(label, true, uriFileSystem);

    }

    static protected void commitUpdateDelegate(String label, boolean isCommit,
            String... uriFileSystem) throws Exception {

        FLAG_COMMIT_UPDATE = true;

        File currentDir = new File(FileUtil.getCurrentDirectory()
                .getAbsolutePath());

        if (isCommit) {
            log.info("Commit args.length:+" + uriFileSystem.length);
        } else {
            log.info("Update args.length:+" + uriFileSystem.length);
        }

        if (log.isDebugEnabled()) {
            for (String args : uriFileSystem) {
                log.debug(args);
            }
            log.debug("Current dir:" + currentDir);
        }

        /*
         * Alors c'est facile si on a un élément dans le param c'est que on
         * spécifie l'endroit du FS à updater/commit, ce qui veut dire que on
         * doit aller chercher ensuite dans le dossier donné l'adresse du
         * wikitty service. Notons que si il y a un élément ça doit forcément
         * être une uri avec le protocole file. après on appelle simplement la
         * méthode synchro avec l'ordre correct entre uritarget et uri origin.
         * 
         * dans le cas ou ya pas d'argument ça veut dire que on doit
         * commit/update le dossier courant donc aller chercher dans
         * l'arborescence l'adresse du wikitty service.
         * 
         * et pareil on va construire les adresses pour faire une synchro
         */

        // update is from wikitty service store to wikitty service File System

        String wikittyServiceInter = StringUtils.EMPTY;
        String wikittyServiceFileSystem = StringUtils.EMPTY;

        String labelInitial;
        PropertiesExtended homeProperty = null;
        // Check number of argument
        switch (uriFileSystem.length) {
        // if none, then the current dir have to be commit.
        case 0:
            // search for the home property dir that containt uri to the wikitty
            // service
            File homePropertyDir = searchWikittyPublicationHomePropertie(currentDir);

            homeProperty = WikittyPublicationFileSystem
                    .getWikittyPublicationProperties(homePropertyDir,
                            WIKITTY_FILE_SERVICE);

            // load the wikitty service uri (distant one)
            wikittyServiceInter = homeProperty
                    .getProperty(WIKITTY_SERVICE_INTERLOCUTEUR);

            // construct the current uri for wikitty service file system
            // search the current label
            PropertiesExtended metaPropertiesExtended = WikittyPublicationFileSystem
                    .getWikittyPublicationProperties(currentDir,
                            WikittyFileUtil.WIKITTY_FILE_META_PROPERTIES_FILE);
            String labelCurrent = metaPropertiesExtended
                    .getProperty(WikittyPublicationFileSystem.META_CURRENT_LABEL);

            // construct uri of the wikitty publication file system
            wikittyServiceFileSystem = "file://"
                    + homePropertyDir.getAbsolutePath() + LABEL_DELIM
                    + labelCurrent;
            labelInitial = homeProperty.getProperty(LABEL_KEY);

            wikittyServiceInter += LABEL_DELIM + label
                    + labelCurrent.replaceFirst(labelInitial, "");

            break;
        // if a param is set it mean that the uri of the File system is set
        case 1:

            wikittyServiceFileSystem = uriFileSystem[0];
            URI originUri = new URI(wikittyServiceFileSystem);
            // check if uri of wikitty publication file system is well formed,
            // must be a file "protocol"
            if (!originUri.getScheme().equals(FILE_URI_PREFIX)) {
                // Exception
            }
            // then search for the home property file to load the wikitty
            // service uri
            File workingDir = new File(originUri.getPath());
            homeProperty = WikittyPublicationFileSystem
                    .getWikittyPublicationProperties(workingDir,
                            WIKITTY_FILE_SERVICE);

            wikittyServiceInter = (String) homeProperty
                    .get(WIKITTY_SERVICE_INTERLOCUTEUR);

            labelInitial = homeProperty.getProperty(LABEL_KEY);

            wikittyServiceInter += LABEL_DELIM + label
                    + originUri.getFragment().replaceFirst(labelInitial, "");

            break;

        // Exception, correct number of argument is 0 or 1
        default:

            // exception
            break;
        }

        if (log.isDebugEnabled()) {

            log.debug("homeProperty :" + homeProperty.getOrigin());
            for (Entry<Object, Object> ee : homeProperty.entrySet()) {
                log.debug(ee.getKey() + "=" + ee.getValue());
            }

            log.debug("wikitty Inter:" + wikittyServiceInter
                    + " wikittyFileSystem" + wikittyServiceFileSystem);
        }

        // delegate to synchronisation
        // only difference between update and commit is the param's order
        if (isCommit) {
            synchronisation(wikittyServiceFileSystem, wikittyServiceInter);
        } else {
            synchronisation(wikittyServiceInter, wikittyServiceFileSystem);
        }

    }

    static public void usage() {

        System.out.println("");

        String usage = "Usage"
                + "\n"
                + "''wp sync [--norecursion] "
                + "[--delete|--existing] [URI origin] [URI target]''"
                + "\n \nwith URI :\n"
                + "file:///truc/machin/#label\n"
                + "hessian://www.adresse.com:8827/etc/etc#label\n"
                + "cajo://www.adresse.com:8827/etc/etc#label"
                + " \n\n or: \n"
                + "''wp [update|commit] [--norecursion] [--delete|--existing] [URI file]''";
        System.out.println(usage);
    }

    /**
     * Called when syncrhonising with file system, used to store info about the
     * "other" service used. Infos used when try to commit update from a service
     * over file system
     * 
     * @param label
     */
    protected static void writeProperty(String ur) {

        // Only if not commit/update and syncrhonising from main
        if (!FLAG_COMMIT_UPDATE && MAIN_ENABLE) {
            try {
                URI uri = new URI(ur);
                String label = uri.getFragment();

                File homeFile = new File(uri.getPath());

                File homeProperty = new File(homeFile.getAbsolutePath()
                        + File.separator
                        + WikittyPublicationFileSystem.PROPERTY_DIRECTORY);

                if (!homeProperty.exists()) {
                    homeProperty.mkdir();
                }

                // write/update the "home property file"
                PropertiesExtended propertyWikittyService;

                propertyWikittyService = WikittyPublicationFileSystem
                        .getWikittyPublicationProperties(homeFile,
                                WIKITTY_FILE_SERVICE);

                // the original label use to create if not exist
                if (!propertyWikittyService
                        .containsKey(WikittyPublicationSynchronize.LABEL_KEY)) {
                    log.debug("Writing home property label"
                            + propertyWikittyService.getOrigin());
                    propertyWikittyService.setProperty(
                            WikittyPublicationSynchronize.LABEL_KEY, label);
                }
                // the service use to update or commit
                String uriService = applicationConfig
                        .getOption(WikittyPublicationSynchronize.WIKITTY_SERVICE_INTERLOCUTEUR);
                if (uriService != null) {
                    log.debug("Writing home property service on:"
                            + propertyWikittyService.getOrigin() + " uri"
                            + uriService);

                    propertyWikittyService
                            .setProperty(
                                    WikittyPublicationSynchronize.WIKITTY_SERVICE_INTERLOCUTEUR,
                                    uriService);
                }

                propertyWikittyService.store();

            } catch (Exception e) {
                // TODO mfortun-2011-08-25
                if (log.isErrorEnabled()) {
                    log.error(
                            "Error while writing properties for commit update",
                            e);
                }
            }
        }
    }

    /**
     * Use to search and return the home property that containt informations
     * about wikitty service use to synchronise a wikitty publication file
     * system
     * 
     * @param start
     *            the directory where starts the search
     * @return PropertiesExtended the home property
     * @throws IOException
     *             if error while reading property file
     */
    static public File searchWikittyPublicationHomePropertie(File start)
            throws IOException {

        log.info("Search for home propertie from :" + start);

        if (start != null && start.exists() && start.isDirectory()) {

            // recursion, we search in the parents for the home property file
            // those which containt the wikitty service property withe the
            // adress of the interlocuteur

            File propertyDirectory = new File(start.getCanonicalPath()
                    + File.separator
                    + WikittyPublicationFileSystem.PROPERTY_DIRECTORY);

            if (propertyDirectory.exists()) {
                File propertie = new File(propertyDirectory.getCanonicalPath()
                        + File.separator + WIKITTY_FILE_SERVICE);
                if (propertie.exists()) {
                    return start;
                }
            }

            return searchWikittyPublicationHomePropertie(start.getParentFile());
        } else {
            if (start == null) {
                throw new IOException("Start cannot be null");
            }
            if (!start.exists()) {
                throw new IOException(start + " doen't exist");
            }
            throw new IOException(start + " is not a directory");
        }
    }
}
