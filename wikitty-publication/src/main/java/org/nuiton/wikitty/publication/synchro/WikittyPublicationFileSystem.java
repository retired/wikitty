/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin mfortun
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.synchro;

import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.CollectionUtil;
import org.nuiton.util.FileUtil;
import org.nuiton.util.MD5InputStream;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.entities.WikittyLabelHelper;
import org.nuiton.wikitty.entities.WikittyLabelImpl;
import org.nuiton.wikitty.publication.AbstractWikittyFileService;
import org.nuiton.wikitty.publication.MimeTypePubHelper;
import org.nuiton.wikitty.publication.PropertiesExtended;
import org.nuiton.wikitty.publication.WikittyFileUtil;
import org.nuiton.wikitty.publication.entities.WikittyPubData;
import org.nuiton.wikitty.publication.entities.WikittyPubDataHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubDataImpl;
import org.nuiton.wikitty.publication.entities.WikittyPubText;
import org.nuiton.wikitty.publication.entities.WikittyPubTextHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubTextImpl;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.TreeNodeResult;
import org.nuiton.wikitty.services.WikittyEvent;
import org.nuiton.wikitty.services.WikittyListener;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class WikittyPublicationFileSystem extends AbstractWikittyFileService {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static Log log = LogFactory
            .getLog(WikittyPublicationFileSystem.class);



    /**
     * The working directory of the wikitty service
     */
    protected File homeFile;

    /**
     * if recursion is chose
     */
    protected boolean recursion;

    /**
     * The initial label of the wikitties, and the path to the first wikitties
     * of the working directory
     */
    protected String label;

    /**
     * Property key to store the current label in a wikittyProperty meta file.
     * Usefull to retrieve easily the wikitties and convert file as wikitty.
     */
    static public String META_CURRENT_LABEL = "current.label";

    /*
     * in the WIKITTY_FILE_META_PROP ERTIES_FILE the keys for version are:
     * filename + META_SUFFIX_KEY_VERSION we save the id too because with this
     * solution un case of delete we can easily read the id and make operation
     * on the wikitty
     */
    /**
     * property key used in the metaPropertyFile to store the wikitty's version
     */
    static public String META_PREFIX_KEY_VERSION = "version.";

    /**
     * property key used in the metaPropertyFile to store the wikitty's content
     * checksum
     */
    static public String META_PREFIX_KEY_CHECKSUM = "checksum.";

    /**
     * property key used in the metaPropertyFile to store the wikitty's id
     */
    static public String META_PREFIX_KEY_ID = "id.";

    /**
     * the file name of the property directory
     */
    static public String PROPERTY_DIRECTORY = ".wp";

    /**
     * the mimeType Handler used to determine mimeType for file extension and if
     * file have to be converted as wikittyPubText or WikittyPubData
     */
    protected MimeTypePubHelper mimeHelper;

    /**
     * List of directory name that does'nt have to be harvest exampe : .svn,
     * .git
     */
    protected List<String> directoryNameBlackList;

    /**
     * Default constructor, call by a factory
     * 
     * @param app
     *            application config needed to correctly initialize this. Need
     *            wikitty url to be set (with URI format) and option for
     *            recursion, @see WikittyPublication for constant
     * @throws URISyntaxException
     *             if serveur url is not in the proper format (URI format)
     * @throws IOException
     *             if error while creating/reading the wikitty service
     *             properties
     */
    public WikittyPublicationFileSystem(ApplicationConfig app)
            throws URISyntaxException, IOException {

        mimeHelper = new MimeTypePubHelper();

        String url = app.getOption(WikittyConfigOption.WIKITTY_SERVER_URL
                .getKey());
        URI uri = new URI(url);

        homeFile = new File(uri.getPath());

        if (!homeFile.exists()) {
            File cur = FileUtil.getCurrentDirectory();
            homeFile = new File(cur.getAbsolutePath());
        }

        label = uri.getFragment();

        if (label == null) {
            label = homeFile.getName();
        }

        log.info("HomeDir:" + homeFile + "Label:" + label);


        recursion = true;
        if (app.getOptions().containsKey(
                WikittyPublicationSynchronize.IS_RECURSION_OPTION)) {

            recursion = app
                    .getOptionAsBoolean(WikittyPublicationSynchronize.IS_RECURSION_OPTION);
        }
        // TODO mfotun-2011-04-28 add a support for filtered file with a
        // property file
        directoryNameBlackList = new ArrayList<String>();
        directoryNameBlackList.add(".svn");
        directoryNameBlackList.add(".git");

    }

    public File getHomeFile() {
        return homeFile;
    }

    public void setHomeFile(File homeFile) {
        this.homeFile = homeFile;
    }

    public boolean isRecursion() {
        return recursion;
    }

    public void setRecursion(boolean recursion) {
        this.recursion = recursion;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public void addWikittyServiceListener(WikittyListener listener,
            ServiceListenerType type) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        //

    }

    @Override
    public void removeWikittyServiceListener(WikittyListener listener,
            ServiceListenerType type) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        //

    }

    @Override
    public String login(String login, String password) {
        // TODO mfortun-2011-04-18
        throw new UnsupportedOperationException("not yet implemented");
        //

    }

    @Override
    public void logout(String securityToken) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        //

    }

    @Override
    public WikittyEvent clear(String securityToken) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public boolean canWrite(String securityToken, Wikitty wikitty) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        // return false;

    }

    @Override
    public boolean canDelete(String securityToken, String wikittyId) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        // return false;

    }

    @Override
    public boolean canRead(String securityToken, String wikittyId) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        // return false;

    }

    @Override
    public boolean exists(String securityToken, String wikittyId) {
        return getAllWikitties().containsKey(wikittyId);

    }

    @Override
    public boolean isDeleted(String securityToken, String wikittyId) {
        return !exists(securityToken, wikittyId);
    }

    @Override
    public WikittyEvent replay(String securityToken, List<WikittyEvent> events,
            boolean force) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    /**
     * Store wikitty as file 
     */
    public WikittyEvent store(String securityToken,
            Collection<Wikitty> wikitties, boolean force) {
        log.info("Store wikitty: " + wikitties.size() + " isForce" + force);
        WikittyEvent result = new WikittyEvent(this);
        try {

            for (Wikitty w : wikitties) {
                log.debug("Works on : " + w);

                Set<String> set = WikittyLabelHelper.getLabels(w);
                String ourDir = "";

                // search for our label recursion determine how search the label
                // if have to match exactly or startswith
                log.debug("Sets of labels (" + "isRecur" + recursion
                        + " label:" + label + ")");
                for (String name : set) {
                    log.debug(name);
                    if (!recursion && name.equalsIgnoreCase(label)) {
                        ourDir = name;
                    } else if (recursion && name.startsWith(label)) {
                        ourDir = name;
                    }
                }

                // if label is empty it means that the wikitty have to be remove
                // from the FileSystem
                if ("".equals(ourDir)) {
                    // remove file if the wikitty already exist
                    BidiMap location = harvestLocalWikitties(homeFile, true);

                    FileSystemWIkittyId idSystem = (FileSystemWIkittyId) location
                            .get(w.getId());

                    if (idSystem != null) {

                        File wikittyFile = new File(idSystem.getPath()
                                + File.separator + idSystem.getFileName());

                        log.debug("Deleted wikitty id:" + w.getId() + " File:"
                                + wikittyFile);

                        if (wikittyFile.exists()) {
                            wikittyFile.delete();
                        }
                    }
                    // jump to the next wikitty to store
                    continue;
                }

                // create the directories from the label
                boolean pathFilecreated = WikittyFileUtil
                        .createFilesFromLabelPath(homeFile, ourDir);

                if (pathFilecreated) {
                    // create the path with the label
                    String path = homeFile.getCanonicalFile() + File.separator
                            + ourDir.replace(".", File.separator);

                    // correct the problem with directory name begin by .
                    path = path.replace(File.separator + File.separator,
                            File.separator + ".");

                    File wikittyParenFile = new File(path);
                    // create the propertie directory if necessary
                    File propertieDirectory = new File(path + File.separator
                            + PROPERTY_DIRECTORY);

                    if (!propertieDirectory.exists()
                            || !propertieDirectory.isDirectory()) {
                        propertieDirectory.mkdir();
                    }

                    String name;
                    String extension;

                    File wikittyFile;
                    // construct the file differently if wikittyPubData or
                    // wikittyPubText
                    if (w.hasExtension(WikittyPubData.EXT_WIKITTYPUBDATA)) {
                        log.debug("Wikitty has wikittyPubData Ext");
                        name = WikittyPubDataHelper.getName(w);
                        // String mime = WikittyPubDataHelper.getMimeType(w);
                        byte[] content = WikittyPubDataHelper.getContent(w);

                        String mime = WikittyPubDataHelper.getMimeType(w);
                        extension = mimeHelper.getExtensionForMime(mime);

                        wikittyFile = new File(path + File.separator + name
                                + "." + extension);

                        wikittyFile.createNewFile();

                        FileUtil.byteToFile(content, wikittyFile);

                    } else if (w
                            .hasExtension(WikittyPubText.EXT_WIKITTYPUBTEXT)) {
                        log.debug("Wikitty has wikittyPubText Ext");
                        name = WikittyPubTextHelper.getName(w);
                        // String mime = WikittyPubTextHelper.getMimeType(w);
                        String content = WikittyPubTextHelper.getContent(w);
                        String mime = WikittyPubTextHelper.getMimeType(w);

                        extension = mimeHelper.getExtensionForMime(mime);

                        wikittyFile = new File(path + File.separator + name
                                + "." + extension);

                        wikittyFile.createNewFile();

                        FileUtil.writeString(wikittyFile, content);
                    } else {
                        // unsupported kind of wikitty, just jump.
                        continue;
                    }

                    // if file properly write, write wikittyProperties
                    if (wikittyFile != null) {

                        // write current label
                        PropertiesExtended metaProperties = getWikittyPublicationProperties(
                                wikittyParenFile,
                                WikittyFileUtil.WIKITTY_FILE_META_PROPERTIES_FILE);
                        // update
                        metaProperties
                                .setProperty(
                                        META_CURRENT_LABEL,
                                        ourDir);
                        metaProperties.store();

                        // write common properties
                        writeWikittyFileProperties(wikittyFile, w.getId(),
                                w.getVersion());
                    }
                }
            }

        } catch (IOException e) {
            // TODO mfortun-2011-08-12 Exception not really handled
            if (log.isErrorEnabled()){
                log.error("Error while store wikitty on FS", e );
            }
        }

        return result;
    }

    @Override
    public List<String> getAllExtensionIds(String securityToken) {

        List<String> result = new LinkedList<String>();
        result.add(WikittyPubData.EXT_WIKITTYPUBDATA);
        result.add(WikittyPubText.EXT_WIKITTYPUBTEXT);
        // result.add(WikittyPubTextCompiled.EXT_WIKITTYPUBTEXTCOMPILED);
        result.add(WikittyLabel.EXT_WIKITTYLABEL);

        return result;
    }

    @Override
    public List<String> getAllExtensionsRequires(String securityToken,
            String extensionName) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public WikittyEvent storeExtension(String securityToken,
            Collection<WikittyExtension> exts) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public WikittyEvent deleteExtension(String securityToken,
            Collection<String> extNames) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public WikittyExtension restoreExtension(String securityToken,
            String extensionId) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }


    @Override
    /**
     * restore file as wikitty
     */
    public List<Wikitty> restore(String securityToken, List<String> id) {
        FileSystemWIkittyId localisation = null;
        List<Wikitty> result = new ArrayList<Wikitty>();
        log.info("restore wikitty, number: " + id.size());
        try {
            // construct the starts file from the label and the working
            // directory
            String labelToDir = WikittyFileUtil.labelToPath(label);
            File starts = new File(homeFile.getAbsolutePath() + File.separator
                    + labelToDir);

            // harvest all wikitty from the file system
            BidiMap locations = harvestLocalWikitties(starts, recursion);

            // register wikitty in the result
            for (String wikid : id) {
                Object value = locations.get(wikid);

                if (value != null) {
                    localisation = (FileSystemWIkittyId) value;

                    result.add(restore(wikid, localisation));
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            // TODO mfortun-2011-08-16 really handle exception
            if (log.isErrorEnabled()){
                log.error("Error while restoring wikitty located:"+localisation, e );
            }
        }

        return result;
    }

    /*
     * TODO mfortun-2011-04-29 see if method really needed ?
     */
    @Override
    public WikittyEvent delete(String securityToken, Collection<String> ids) {

        log.info("Delete called wikitty size:" + ids.size());

        try {
            // load localisation of all the wikitties
            BidiMap location = harvestLocalWikitties(homeFile, true);

            for (String id : ids) {

                Object value = location.get(id);

                log.debug("delete wikitty id: " + id + " corresponding file :"
                        + value);
                if (value != null) {

                    FileSystemWIkittyId localisation = (FileSystemWIkittyId) value;

                    String path = localisation.getPath();
                    String fileName = localisation.getFileName();

                    // load properties
                    PropertiesExtended propsProperties = getWikittyPublicationProperties(
                            new File(path),
                            WikittyFileUtil.WIKITTY_FILE_META_PROPERTIES_FILE);

                    // update remove properties
                    propsProperties.remove(META_PREFIX_KEY_CHECKSUM + fileName);
                    propsProperties.remove(META_PREFIX_KEY_VERSION + fileName);
                    propsProperties.remove(META_PREFIX_KEY_ID + fileName);

                    // resave
                    propsProperties.store();

                    // load properties
                    PropertiesExtended idProperties = getWikittyPublicationProperties(
                            new File(path),
                            WikittyFileUtil.WIKITTY_ID_PROPERTIES_FILE);
                    // update
                    idProperties.remove(id);
                    // resave
                    idProperties.store();

                    File wikittyFile = new File(path + File.separator
                            + fileName);

                    if (wikittyFile.exists()) {
                        wikittyFile.delete();
                    }

                }
            }
        } catch (IOException e) {
            // TODO mfortun-2011-08-16 Exception not really handled
            if (log.isErrorEnabled()){
                log.error("Error while deleting wikitty on FS", e );
            }
        }

        WikittyEvent result = new WikittyEvent(this);
        return result;
    }

    @Override
    protected Map<String, Wikitty> getAllWikitties() {
        Map<String, Wikitty> wikitties = new HashMap<String, Wikitty>();
        try {
            // construct properly with the working directory and the label
            // the starts directory
            String labelToDir = WikittyFileUtil.labelToPath(label);
            File starts = new File(homeFile.getAbsolutePath() + File.separator
                    + labelToDir);
            if (starts.exists()) {
                // check for new file, modifications and deleted wikitty
                harvestNewCheckModificationsAndDeleted(starts, label);
            }

            // load all locals wikitty
            BidiMap map = harvestLocalWikitties(starts, true);

            // put all local wikitties in a map

            for (Object o : map.keySet()) {
                String id = (String) o;
                FileSystemWIkittyId location = (FileSystemWIkittyId) map
                        .get(id);

                wikitties.put(id, restore(id, location));

            }

        } catch (IOException e) {
            // TODO mfortun-2011-08-16 Exception not really handled
            if (log.isErrorEnabled()){
                log.error("Error while search forAllWikitty on FS", e );
            }
        }
        return wikitties;
    }

    @Override
    public WikittyEvent deleteTree(String securityToken, String treeNodeId) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public TreeNodeResult<String> findTreeNode(String securityToken,
            String wikittyId, int depth, boolean count, Criteria filter) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public Wikitty restoreVersion(String securityToken, String wikittyId,
            String version) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public void syncSearchEngine(String securityToken) {
        // TODO mfortun-2011-04-05
        throw new UnsupportedOperationException("not yet implemented");
        //

    }

    /**
     * Method that create a list of the properties directory
     * 
     * @param starts
     *            harvested directory
     * @param recursivly
     * @return list of harvested file
     */
    public List<File> harvestPropertyDirectory(File starts, boolean recursivly) {

        log.info("harvest property directory starts:" + starts + "isRecur"
                + recursivly);
        List<File> result = new ArrayList<File>();

        for (File child : starts.listFiles()) {
            // search for all property directory, usefull to retrieve wikitty
            if (child.isDirectory()
                    && child.getName().equals(PROPERTY_DIRECTORY)) {
                result.add(child);
            } else if (child.isDirectory() && recursivly
                    && !directoryNameBlackList.contains(child.getName())) {
                result.addAll(harvestPropertyDirectory(child, recursivly));
            }
        }
        return result;

    }

    /**
     * 
     * @param starts
     * @param recursivly
     * @return
     * @throws IOException 
     */
    public BidiMap harvestLocalWikitties(File starts, boolean recursivly) throws IOException
             {
        BidiMap result = new DualHashBidiMap();

        log.info("harvest localWikitty");
        List<File> propertiesDirectory = harvestPropertyDirectory(starts,
                recursivly);

        // use the list of property to retrieve wikitty
        for (File propsDir : propertiesDirectory) {

            log.debug("harvest wikitty on " + propertiesDirectory);

            Properties idProps = new Properties();
            File idFile = new File(propsDir.getCanonicalPath() + File.separator
                    + WikittyFileUtil.WIKITTY_ID_PROPERTIES_FILE);
            if (idFile.exists()) {
                idProps.load(new FileReader(idFile));
            }

            Set<Object> ids = idProps.keySet();
            // simply load the id property file to retrieve wikitty id
            // with the path to the propertyfile and the value of the file
            // construct FileSytemWikittyId
            log.debug("List of file/wikitty");
            for (Object id : ids) {
                String name = idProps.getProperty((String) id);
                String path = propsDir.getParent();
                FileSystemWIkittyId value = new FileSystemWIkittyId(name, path);

                log.debug("ID:" + id + " fileInformation:" + value);

                result.put(id, value);
            }

        }

        /*
         * Pour l'arborescence des fichiers on va aller chercher les fichiers de
         * propriété id et meta dans le dossier .wp pour aller récup les labels
         * courant et les id liés on va renvoyer la map
         * 
         * id, label
         */

        return result;
    }

    /**
     * Simply return the propertyExtended corresponding that can be found in
     * starts/.wp/ directory. It create the corresponding property file if
     * does'nt exist
     * 
     * @param starts
     *            the directory that containt a .wp directory containing the
     *            property
     * @param name
     *            the property file name, use constants
     * @return the properties extended requested
     * @throws IOException
     *             if error while reading the file
     */
    static public PropertiesExtended getWikittyPublicationProperties(
            File starts, String name) throws IOException {

        log.debug("getwikittyPublicationProperties :" + name + " on dir:"
                + starts);
        File propertieDirectory = new File(starts.getCanonicalPath()
                + File.separator
                + PROPERTY_DIRECTORY);

        // load/create meta propertie file
        File propertieFile = new File(propertieDirectory.getCanonicalPath()
                + File.separator + name);
        if (!propertieFile.exists()) {
            propertieFile.createNewFile();
        }

        PropertiesExtended result = new PropertiesExtended(propertieFile);

        return result;
    }

    /**
     * Restore the corresponding wikitty identify by the id and is location on
     * the filesytem
     * 
     * @param id
     *            the wikitty id
     * @param fileId
     *            the wikitty file system id (his localisation on the
     *            wikittyFileSystem)
     * @return the restored wikitty
     * @throws IOException
     *             if errors while reading the file
     */
    protected Wikitty restore(String id, FileSystemWIkittyId fileId)
            throws IOException {

        log.debug("restore wikitty id:" + id + " file: " + fileId);

        Wikitty result = new WikittyImpl(id);

        result.addExtension(WikittyLabelImpl.extensionWikittyLabel);

        // preparation for mime research and file research
        String path = fileId.getPath();
        String completeName = fileId.getFileName();

        // search for the file
        File fileToTransform = new File(path + File.separator + completeName);

        if (!fileToTransform.exists()) {
            return null;
        }

        String extension = FileUtil.extension(fileToTransform);
        String name = FileUtil.basename(completeName, "." + extension);

        // search for the mimetype
        String mimeType = mimeHelper.getMimeForExtension(extension);

        // load properties
        File wikittyParentDir = new File(path);
        PropertiesExtended props = getWikittyPublicationProperties(
                wikittyParentDir,
                WikittyFileUtil.WIKITTY_FILE_META_PROPERTIES_FILE);

        // set the current label
        WikittyLabelHelper.addLabels(result, props
                .getProperty(META_CURRENT_LABEL));

        // create the correct wikittypubxxx
        if (mimeHelper.isPubTextMime(mimeType)) {
            log.debug("restore wikitty id:" + id + " file: " + fileId
                    + " pubTextType");
            result.addExtension(WikittyPubTextImpl.extensionWikittyPubText);
            WikittyPubTextHelper.setName(result, name);
            WikittyPubTextHelper.setMimeType(result, mimeType);
            WikittyPubTextHelper.setContent(result,
                    FileUtil.readAsString(fileToTransform));
        } else {
            log.debug("restore wikitty id:" + id + " file: " + fileId
                    + " pubDataType");
            result.addExtension(WikittyPubDataImpl.extensionWikittyPubData);
            WikittyPubDataHelper.setName(result, name);
            WikittyPubDataHelper.setMimeType(result, mimeType);
            WikittyPubDataHelper.setContent(result,
                    FileUtil.fileToByte(fileToTransform));
        }

        // re set the version (re set the version now allow the version to be
        // not altered by operation on the wikitty to restore it
        result.setVersion(props
                .getProperty(META_PREFIX_KEY_VERSION
                        + completeName));

        return result;
    }

    /**
     * Check on local file if there new file that have to be converted into
     * wikitty, check wikitty if they have been modified, and check if some
     * wikitty were deleted and update property
     * 
     * @param starts
     *            the starting directory
     * @param label
     *            the current label, normaly equals to the starting directory
     * @throws IOException
     *             if error while read file, read property
     */
    protected void harvestNewCheckModificationsAndDeleted(File starts,
            String label) throws IOException {

        log.info("Check for wikitty on disk: new, deleted and modified. Directory:"
                + starts + " Label:" + label);
        // write property directory if not exist
        File propertyFile = new File(starts + File.separator
                + PROPERTY_DIRECTORY);

        if (!propertyFile.exists() || !propertyFile.isDirectory()) {
            propertyFile.mkdir();

        }

        // create/load property file
        PropertiesExtended ids = getWikittyPublicationProperties(starts,
                WikittyFileUtil.WIKITTY_ID_PROPERTIES_FILE);
        PropertiesExtended meta = getWikittyPublicationProperties(starts,
                WikittyFileUtil.WIKITTY_FILE_META_PROPERTIES_FILE);

        // set label
        meta.put(META_CURRENT_LABEL, label);
        meta.store();

        // for the child directory check on them
        List<String> listChildFileNonDir = new ArrayList<String>();

        for (File child : starts.listFiles()) {
            if (child.isDirectory()
                    && !child.getName().equals(PROPERTY_DIRECTORY)) {
                harvestNewCheckModificationsAndDeleted(child, label
                        + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR + child.getName());
            } else if (!child.isDirectory()) {
                // list all of the file non dir
                listChildFileNonDir.add(child.getName());
                // check if file is a wikitty by check if they is a id/file name
                // in the correct properties file
                List<String> filesWikitty = new ArrayList<String>();
                filesWikitty.addAll(CollectionUtil.toGenericCollection(
                        ids.values(), String.class));
                // if file is not a wikitty create it.
                if (!filesWikitty.contains(child.getName())) {
                    writeWikittyFileProperties(child, null, null);
                } else {
                    // if the file is a wikitty check if there was modification
                    checkModifications(child);
                }
            }
        }
        // after adding all new file, check for deleted wikitty
        ids = getWikittyPublicationProperties(starts,
                WikittyFileUtil.WIKITTY_ID_PROPERTIES_FILE);

        List<String> filesWikitty = new ArrayList<String>();
        filesWikitty.addAll(CollectionUtil.toGenericCollection(ids.values(),
                String.class));

        // if number of wikitty registered != number of current file (not dir)
        // check for the deleted
        if (filesWikitty.size() != listChildFileNonDir.size()) {

            log.info("remove wikitty informations for deleted file");

            meta = getWikittyPublicationProperties(starts,
                    WikittyFileUtil.WIKITTY_FILE_META_PROPERTIES_FILE);

            filesWikitty.removeAll(listChildFileNonDir);

            for (String fileRemoved : filesWikitty) {

                log.debug("remove wikitty informations for:" + filesWikitty);
                // remove properties
                ids.remove(meta.get(META_PREFIX_KEY_ID + fileRemoved));

                meta.remove(META_PREFIX_KEY_CHECKSUM + fileRemoved);
                meta.remove(META_PREFIX_KEY_VERSION + fileRemoved);
                meta.remove(META_PREFIX_KEY_ID + fileRemoved);
            }
            meta.store();

        }

        ids.store();

    }

    /**
     * Check with the corresponding checksum if the file have been modified
     * since the last check. If so, it increment the minor version of the
     * wikitty and store the new checksum and version
     * 
     * @param child
     *            the file need to check if modified
     * @throws IOException
     */
    protected void checkModifications(File child) throws IOException {

        log.info("Check if there was modification on :" + child);

        // will check if there was modification with checksum
        BufferedInputStream input = new BufferedInputStream(
                new FileInputStream(child));

        byte[] byt = MD5InputStream.hash(input);

        String localMd5 = StringUtil.asHex(byt);

        PropertiesExtended meta = getWikittyPublicationProperties(
                child.getParentFile(),
                WikittyFileUtil.WIKITTY_FILE_META_PROPERTIES_FILE);

        String registeredMD5 = meta.getProperty(META_PREFIX_KEY_CHECKSUM
                + child.getName());
        // compare the different checksum
        if (!localMd5.equals(registeredMD5)) {
            // increment version and set new checksum
            String currentVersion = meta.getProperty(META_PREFIX_KEY_VERSION
                    + child.getName());
            meta.setProperty(META_PREFIX_KEY_VERSION + child.getName(),
                    WikittyUtil.incrementMinorRevision(currentVersion));
            meta.setProperty(META_PREFIX_KEY_CHECKSUM + child.getName(),
                    localMd5);
        }
        meta.store();
    }

    /**
     * write in properties file property about the file corresponding to a
     * wikitty. If the file does not match a wikitty yet set wikittyID and
     * Wikittyversion to null
     * 
     * @param towrite
     *            the file
     * @param wikittyID
     *            wiitty id, null if wikitty does not exist yet
     * @param wikittyVersion
     *            wikitty version, null if wikitty not exist yet
     * @throws IOException
     */
    protected void writeWikittyFileProperties(File towrite, String wikittyID,
            String wikittyVersion) throws IOException {

        log.info("Write wikitty information file: " + towrite + " Id:"
                + wikittyID + " version:" + wikittyVersion);

        if (towrite.exists() && !towrite.isDirectory()) {
            Wikitty wikitty = new WikittyImpl(wikittyID);
            if (wikittyVersion != null) {
                wikitty.setVersion(wikittyVersion);
            } else {
                // simulate operations en wikitty if new
                wikitty.setVersion(WikittyUtil.incrementMinorRevision(wikitty
                        .getVersion()));
            }

            File parent = towrite.getParentFile();

            PropertiesExtended id = getWikittyPublicationProperties(parent,
                    WikittyFileUtil.WIKITTY_ID_PROPERTIES_FILE);
            PropertiesExtended meta = getWikittyPublicationProperties(parent,
                    WikittyFileUtil.WIKITTY_FILE_META_PROPERTIES_FILE);

            id.put(wikitty.getId(), towrite.getName());

            BufferedInputStream input = new BufferedInputStream(
                    new FileInputStream(towrite));

            byte[] byt = MD5InputStream.hash(input);

            String localMd5 = StringUtil.asHex(byt);

            meta.put(META_PREFIX_KEY_VERSION + towrite.getName(),
                    wikitty.getVersion());
            meta.put(META_PREFIX_KEY_CHECKSUM + towrite.getName(), localMd5);

            meta.put(META_PREFIX_KEY_ID + towrite.getName(), wikitty.getId());

            id.store();
            meta.store();
        } else {
            throw new IOException(towrite.toString()
                    + " not exist or is a directory");
        }

    }

}
