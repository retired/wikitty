/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;

import java.io.File;
import java.io.IOException;
/**
 * 
 * Class that containt utils method when handle wikitty store as file.
 * 
 * @author mfortun
 * 
 */
public class WikittyFileUtil {

    /**
     * The file name of the meta property file
     */
    static public String WIKITTY_FILE_META_PROPERTIES_FILE = "meta.properties";

    static public String WIKITTY_LABEL_SEPARATOR=".";
    /*
     * Need a different file for id and meta information about wikittiesFiles
     * because with this solution we can simply read the ids with props.keySet()
     */
    /**
     * The file Name of the id property file
     */
    static public String WIKITTY_ID_PROPERTIES_FILE = "ids.properties";

    final static Log log = LogFactory.getLog(WikittyFileUtil.class);

    /**
     * Construct correctly the path from a label
     * 
     * @param label
     *            the label
     * @return the correct path
     */
    public static String labelToPath(String label) {

        String result = label;

        result = result.replace(WIKITTY_LABEL_SEPARATOR, File.separator);

        // correct the pb with directory name begin by .
        result = result.replace(File.separator + File.separator, File.separator
                + ".");

        log.info("Convert label to path: " + label + " path:" + result);

        return result;
    }

    /**
     * Creates all the file system require from a label path in the working
     * directory
     * 
     * @param homeFile
     * @param label the path string
     * @return if all the path was created
     * @throws IOException
     */
    public static boolean createFilesFromLabelPath(File homeFile, String label)
            throws IOException {

        label = labelToPath(label);

        log.info("Create directory from path:" + label);

        String[] pathElements = StringUtil.split(label, File.separator);

        boolean result = false;

        if (homeFile.exists() && homeFile.isDirectory()) {
            String path = homeFile.getCanonicalPath();
            result = true;
            for (int i = 0; i < pathElements.length; i++) {

                path = path + File.separator + pathElements[i];
                File temp = new File(path);
                FileUtil.createDirectoryIfNecessary(temp);
                result = result && temp.exists();
            }
        }

        return result;
    }

}
