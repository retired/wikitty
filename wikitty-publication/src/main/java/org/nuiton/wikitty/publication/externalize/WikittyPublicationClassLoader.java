/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.externalize;

import java.net.URL;
import java.net.URLClassLoader;


/**
 * A simple classloader extension to allow adding java class with bytecode
 * @author mfortun
 *
 */
public class WikittyPublicationClassLoader extends URLClassLoader {

    public WikittyPublicationClassLoader(URL[] urls) {
        super(urls,WikittyPublicationClassLoader.class.getClassLoader());
        

    }

    
    /**
     * use to add a class inside the class path from bytecode
     * @param name the name of the class
     * @param b the bytecode of the class
     * @return the class loaded
     */
    public Class<?> addClass(String name, byte[] b) {
        int off = 0;
        int len = b.length;

        return defineClass(name, b, off, len);
    }

    
    
    /*
     * Réutiliser cette classe pour la création du class loader pour wikitty publication
     * 
     * 
     * Charger tout les jar contenu dans le wikitty publication en fonction du context
     * et du context apps.
     * 
     * faire un md5 du contenu de tout les wikittypubcontenant les jar.
     * et si il est différent de celui qu'on devrait avoir on reload
     * sinon on réutilise les jar.
     * 
     * mettre les jar dans un dossier ?
     * 
     * 
     */
    
}
