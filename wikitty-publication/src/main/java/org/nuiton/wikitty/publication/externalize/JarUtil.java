/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.externalize;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;
import org.nuiton.util.MD5OutputStream;
import org.nuiton.util.StringUtil;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

/**
 * Opérations sur des fichiers Jar. Compression et décompression avec ou sans
 * filtres, scan des fichiers créés ou écrasés lors de la décompression...
 * 
 * Créée à partir de org.nuiton.util.ZipUtil
 * <p/>
 * 
 * @author mfortun
 */
public class JarUtil {

    public static FileFilter jarFilter = new FileFilter() {
        @Override
        public boolean accept(File pathname) {
            return (!pathname.isDirectory())&&pathname.getName().endsWith(".jar");
            
        }
    };
    
    
    /** Class logger. */
    private static Log log = LogFactory.getLog(JarUtil.class);

    /** Taille du buffer pour les lectures/écritures. */
    private static final int BUFFER_SIZE = 8 * 1024;

    /** Le séparateur de fichier en local. */
    private static final String LOCAL_SEP = File.separator;

    private static final String LOCAL_SEP_PATTERN = "\\".equals(LOCAL_SEP) ? LOCAL_SEP
            + LOCAL_SEP
            : LOCAL_SEP;

    /** Le séparateur jar. */
    private static final String JAR_SEP = "/";

    private static final String ZIP_SEP_PATTERN = "/";

    /** Accept all file pattern. */
    protected static FileFilter ALL_FILE_FILTER = new FileFilter() {
        public boolean accept(File pathname) {
            return true;
        }
    };

    /**
     * Uncompress jarped file in targetDir.
     * 
     * @param file
     *            the jar source file
     * @param targetDir
     *            the destination directory
     * @return return last entry name
     * @throws IOException
     *             if any problem while uncompressing
     */
    public static String uncompress(File file, File targetDir)
            throws IOException {
        String result;
        result = uncompressAndRename(file, targetDir, null, null);
        return result;
    }

    /**
     * Uncompress jarped file in targetDir, and rename uncompressed file if
     * necessary. If renameFrom or renameTo is null no renaming is done
     * <p/>
     * file in jar use / to separate directory and not begin with / each
     * directory ended with /
     * 
     * @param file
     *            the jar source file
     * @param targetDir
     *            the destination directory
     * @param renameFrom
     *            pattern to permit rename file before uncompress it
     * @param renameTo
     *            new name for file if renameFrom is applicable to it you can
     *            use $1, $2, ... if you have '(' ')' in renameFrom
     * @return return last entry name
     * @throws IOException
     *             if any problem while uncompressing
     */
    public static String uncompressAndRename(File file, File targetDir,
            String renameFrom, String renameTo) throws IOException {
        String result = "";
        JarInputStream in = new JarInputStream(new FileInputStream(file));
        JarEntry entry;
        while ((entry = in.getNextJarEntry()) != null) {
            String name = entry.getName();
            if (renameFrom != null && renameTo != null) {
                name = name.replaceAll(renameFrom, renameTo);
                if (log.isDebugEnabled()) {
                    log.debug("rename " + entry.getName() + " -> " + name);
                }
            }
            result = name;
            File target = new File(targetDir, name);
            if (entry.isDirectory()) {
                target.mkdirs();
            } else {
                target.getParentFile().mkdirs();
                OutputStream out = new BufferedOutputStream(
                        new FileOutputStream(target));
                try {
                    byte[] buffer = new byte[BUFFER_SIZE];
                    int len;
                    while ((len = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
                        out.write(buffer, 0, len);
                    }
                } finally {
                    out.close();
                }
            }
        }
        in.close();
        return result;
    }

    
    /**
     * 
     * @param jarFile
     * @param fileOrDirectory
     * @throws IOException
     */
    public static void compressFiles(File jarFile, File fileOrDirectory)
            throws IOException {

        FileFilter filter = ALL_FILE_FILTER;

        List<File> files = new ArrayList<File>();
        if (fileOrDirectory.isDirectory()) {
            files = FileUtil.getFilteredElements(fileOrDirectory, filter, true);
        } else if (filter.accept(fileOrDirectory)) {
            files.add(fileOrDirectory);
        }

        compressFiles(jarFile, fileOrDirectory, null, files, false);

    }

    /**
     * Compress 'includes' files in jarFile. If file in includes is directory
     * only the directory is put in jarFile, not the file contained in directory
     * 
     * @param jarFile
     *            the destination jar file
     * @param root
     *            for all file in includes that is in this directory, then we
     *            remove this directory in jar entry name (aka -C for tar), can
     *            be null;
     * @param includes
     *            the files to include in jar
     * @throws IOException
     *             if any problem while compressing
     */
    public static void compressFiles(File jarFile, File root,
            Collection<File> includes) throws IOException {
        compressFiles(jarFile, root, null, includes, false);
    }

    /**
     * Compress 'includes' files in jarFile. If file in includes is directory
     * only the directory is put in jarFile, not the file contained in directory
     * 
     * @param jarFile
     *            the destination jar file
     * @param root
     *            for all file in includes that is in this directory, then we
     *            remove this directory in jar entry name (aka -C for tar), can
     *            be null;
     * @param includes
     *            the files to include in jar
     * @param createMD5
     *            also create a MD5 file (jar name + .md5). MD5 file is created
     *            after jar.
     * @throws IOException
     *             if any problem while compressing
     */
    public static void compressFiles(File jarFile, File root, Manifest mf,
            Collection<File> includes, boolean createMD5) throws IOException {

        if (mf == null) {
            mf = new Manifest();
        }

        OutputStream oStream = new FileOutputStream(jarFile);

        // if md5 creation flag
        if (createMD5) {
            oStream = new MD5OutputStream(oStream);
        }
        try {
            JarOutputStream jarOStream = new JarOutputStream(oStream, mf);

            for (File file : includes) {
                String entryName = toJarEntryName(root, file);

                // Création d'une nouvelle entrée dans le jar
                JarEntry entry = new JarEntry(entryName);
                entry.setTime(file.lastModified());
                jarOStream.putNextEntry(entry);

                if (file.isFile() && file.canRead()) {
                    byte[] readBuffer = new byte[BUFFER_SIZE];
                    int bytesIn;
                    BufferedInputStream bis = new BufferedInputStream(
                            new FileInputStream(file), BUFFER_SIZE);
                    try {
                        while ((bytesIn = bis.read(readBuffer, 0, BUFFER_SIZE)) != -1) {
                            jarOStream.write(readBuffer, 0, bytesIn);
                        }
                    } finally {
                        bis.close();
                    }
                }
                jarOStream.closeEntry();
            }
            jarOStream.close();

            // if md5 creation flag
            if (createMD5) {
                String md5hash = StringUtil.asHex(((MD5OutputStream) oStream)
                        .hash());
                File md5File = new File(jarFile.getAbsoluteFile() + ".md5");
                FileUtil.writeString(md5File, md5hash);
            }
        } finally {
            oStream.close();
        }
    }

    /**
     * If fileOrDirectory is directory Compress recursively all file in this
     * directory, else if is just file compress one file.
     * <p/>
     * Entry result name in jar start at fileOrDirectory. example: if we
     * compress /etc/apache, entry will be apache/http.conf, ...
     * 
     * @param jarFile
     *            the target jar file
     * @param fileOrDirectory
     *            the file or directory to compress
     * @throws IOException
     *             if any problem while compressing
     */
    public static void compress(File jarFile, File fileOrDirectory)
            throws IOException {
        compress(jarFile, fileOrDirectory, null, null, false);
    }

    
    /**
     * If fileOrDirectory is directory Compress recursively all file in this
     * directory, else if is just file compress one file.
     * <p/>
     * Entry result name in jar start at fileOrDirectory. example: if we
     * compress /etc/apache, entry will be apache/http.conf, ...
     * 
     * @param jarFile
     *            the target jar file
     * @param fileOrDirectory
     *            the file or directory to compress
     * @param mf the manifest for the jar
     * @throws IOException
     *             if any problem while compressing
     */
    public static void compress(File jarFile, File fileOrDirectory, Manifest mf)
            throws IOException {
        compress(jarFile, fileOrDirectory, mf, null, false);
    }

    /**
     * If fileOrDirectory is directory Compress recursively all file in this
     * directory, else if is just file compress one file.
     * <p/>
     * Entry result name in jar start at fileOrDirectory. example: if we
     * compress /etc/apache, entry will be apache/http.conf, ...
     * 
     * @param jarFile
     *            the target jar file
     * @param fileOrDirectory
     *            the file or directory to compress
     * @param filter
     *            used to accept file, if null, all file is accepted
     * @throws IOException
     *             if any problem while compressing
     */
    public static void compress(File jarFile, File fileOrDirectory,
            FileFilter filter) throws IOException {
        compress(jarFile, fileOrDirectory, null, filter, false);
    }

    /**
     * If fileOrDirectory is directory Compress recursively all file in this
     * directory, else if is just file compress one file.
     * <p/>
     * Entry result name in jar start at fileOrDirectory. example: if we
     * compress /etc/apache, entry will be apache/http.conf, ...
     * 
     * @param jarFile
     *            the target jar file
     * @param fileOrDirectory
     *            the file or directory to compress
     * @param filter
     *            used to accept file, if null, all file is accepted
     * @param createMD5
     *            also create a MD5 file (jar name + .md5). MD5 file is created
     *            after jar.
     * @throws IOException
     *             if any problem while compressing
     */
    public static void compress(File jarFile, File fileOrDirectory,
            Manifest mf, FileFilter filter, boolean createMD5)
            throws IOException {

        if (filter == null) {
            filter = ALL_FILE_FILTER;
        }
        List<File> files = new ArrayList<File>();
        if (fileOrDirectory.isDirectory()) {
            files = FileUtil.getFilteredElements(fileOrDirectory, filter, true);
        } else if (filter.accept(fileOrDirectory)) {
            files.add(fileOrDirectory);
        }

        compressFiles(jarFile, fileOrDirectory.getParentFile(), mf, files,
                createMD5);
    }

    /**
     * <li>supprime le root du fichier <li>Converti les '\' en '/' car les jar
     * entry utilise des '/' <li>ajoute un '/' a la fin pour les repertoires <li>
     * supprime le premier '/' si la chaine commence par un '/'
     * 
     * @param root
     *            the root directory
     * @param file
     *            the file to treate
     * @return the jar entry name corresponding to the given <code>file</code>
     *         from <code>root</code> dir.
     */
    private static String toJarEntryName(File root, File file) {
        String result = file.getPath();

        if (root != null) {
            String rootPath = root.getPath();
            if (result.startsWith(rootPath)) {
                result = result.substring(rootPath.length());
            }
        }

        result = result.replace('\\', '/');
        if (file.isDirectory()) {
            result += '/';
        }
        while (result.startsWith("/")) {
            result = result.substring(1);
        }
        return result;
    }

    /**
     * Scan a jarFile, and fill two lists of relative paths corresponding of jar
     * entries. First list contains all entries to be added while a uncompress
     * operation on the destination directory </code>targetDir</code>. Second
     * list contains all entries to be overwritten while a uncompress operation
     * on the destination directory <code>targetDir</code>. <br>
     * If <code>targetDir</code> is <code>null</code> we don't fill
     * <cide>existingFiles<code/> list.
     * 
     * @param jarFile
     *            location of the jar to scanJar
     * @param targetDir
     *            location of destination for a uncompress operation. If
     *            <code>null</code> we don't test to find overwritten files.
     * @param newFiles
     *            list of files to be added while a uncompress
     * @param existingFiles
     *            list of files to be overwritten while a uncompress if the
     *            <code>targetDir</code>, (only use if <code>targetDir</code> is
     *            not <code>null</code>)
     * @param excludeFilter
     *            used to exclude some files
     * @param renameFrom
     *            {@link #uncompressAndRename(File, File, String, String)}
     * @param renameTo
     *            {@link #uncompressAndRename(File, File, String, String)}
     * @throws IOException
     *             if any exception while dealing with jarfile
     */
    public static void scan(File jarFile, File targetDir,
            List<String> newFiles, List<String> existingFiles,
            FileFilter excludeFilter, String renameFrom, String renameTo)
            throws IOException {
        JarFile jar = new JarFile(jarFile);
        try {
            boolean findExisting = targetDir != null && targetDir.exists();
            boolean filter = findExisting && excludeFilter != null;
            boolean rename = renameFrom != null && renameTo != null;
            Enumeration<? extends JarEntry> entries = jar.entries();
            while (entries.hasMoreElements()) {
                String entryName = entries.nextElement().getName();
                if (rename) {
                    entryName = entryName.replaceAll(renameFrom, renameTo);
                }
                String name = convertToLocalEntryName(entryName);
                if (findExisting || filter) {
                    File file = new File(targetDir, name);
                    if (filter && excludeFilter.accept(file))
                        continue;
                    if (file.exists()) {
                        existingFiles.add(name);
                        continue;
                    }
                }
                newFiles.add(name);
            }
        } finally {
            if (jar != null) {
                jar.close();
            }
        }
    }

    @SuppressWarnings({ "unchecked" })
    public static List<String>[] scanAndExplodeJar(File source, File root,
            FileFilter excludeFilter) throws IOException {

        List<String> overwrittenFiles = new ArrayList<String>();
        List<String> newFiles = new ArrayList<String>();

        // obtain list of relative paths (to add or overwrite)
        scan(source, root, newFiles, overwrittenFiles, excludeFilter, null,
                null);

        return new List[] { newFiles, overwrittenFiles };
    }

    /**
     * uncompress jarped file in targetDir.
     * <p/>
     * If <code>toTreate</code> if not null nor empty, we use it to filter
     * entries to uncompress : it contains a list of relative local path of
     * files to uncompress. Otherwise just delegate to
     * {@link JarUtil#uncompress(File,File)}.
     * 
     * @param file
     *            location of jar file
     * @param targetDir
     *            destination directory
     * @param toTreate
     *            list of relative local path of entries to treate
     * @param renameFrom
     *            {@link #uncompressAndRename(File, File, String, String)}
     * @param renameTo
     *            {@link #uncompressAndRename(File, File, String, String)}
     * @return return last entry name
     * @throws IOException
     *             if nay exception while operation
     */
    public static String uncompress(File file, File targetDir,
            List<String> toTreate, String renameFrom, String renameTo)
            throws IOException {
        if (toTreate == null || toTreate.isEmpty()) {
            return uncompressAndRename(file, targetDir, renameFrom, renameTo);
        }

        boolean rename = renameFrom != null && renameTo != null;

        String result = "";
        JarEntry entry;
        JarInputStream in = new JarInputStream(new FileInputStream(file));

        try {
            while ((entry = in.getNextJarEntry()) != null) {
                String name = entry.getName();
                if (rename) {
                    result = convertToLocalEntryName(name.replaceAll(
                            renameFrom, renameTo));
                } else {
                    result = convertToLocalEntryName(name);
                }

                if (log.isDebugEnabled()) {
                    log.debug("open [" + name + "] : " + result);
                }
                if (!toTreate.contains(result)) {
                    continue;
                }

                if (log.isDebugEnabled()) {
                    log.debug("copy [" + name + "] : " + result);
                }
                File target = new File(targetDir, result);
                if (entry.isDirectory()) {
                    target.mkdirs();
                } else {
                    target.getParentFile().mkdirs();
                    OutputStream out = new BufferedOutputStream(
                            new FileOutputStream(target));
                    try {
                        byte[] buffer = new byte[BUFFER_SIZE];
                        int len;
                        while ((len = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
                            out.write(buffer, 0, len);
                        }
                    } finally {
                        out.close();
                    }
                }
            }
        } finally {
            in.close();
        }
        return result;
    }

    /**
     * Unjar compressed archive and keep non excluded patterns.
     * 
     * @param file
     *            archive file
     * @param targetDir
     *            destination file
     * @param excludes
     *            excludes pattern (pattern must match complete entry name
     *            including root folder)
     * @throws IOException
     */
    public static void uncompressFiltred(File file, File targetDir,
            String... excludes) throws IOException {

        JarFile jarFile = new JarFile(file);

        Enumeration<? extends JarEntry> entries = jarFile.entries();

        while (entries.hasMoreElements()) {
            JarEntry entry = entries.nextElement();

            String name = entry.getName();
            // add continue to break loop
            boolean excludeEntry = false;
            if (excludes != null) {
                for (String exclude : excludes) {
                    if (name.matches(exclude)) {
                        excludeEntry = true;
                    }
                }
            }

            if (!excludeEntry) {
                File target = new File(targetDir, name);
                if (entry.isDirectory()) {
                    target.mkdirs();
                } else {
                    // get inputstream only here
                    target.getParentFile().mkdirs();
                    InputStream in = jarFile.getInputStream(entry);
                    try {
                        OutputStream out = new BufferedOutputStream(
                                new FileOutputStream(target));
                        try {
                            byte[] buffer = new byte[8 * 1024];
                            int len;

                            while ((len = in.read(buffer, 0, 8 * 1024)) != -1) {
                                out.write(buffer, 0, len);
                            }
                        } finally {
                            out.close();
                        }
                    } finally {
                        in.close();
                    }
                }
            }
        }
    }

    protected static String convertToLocalEntryName(String txt) {
        String s = txt.replaceAll(ZIP_SEP_PATTERN, LOCAL_SEP_PATTERN);
        if (s.endsWith(JAR_SEP)) {
            s = s.substring(0, s.length() - 1);
        }
        return s;
    }

    public static String getStringContent(JarFile jar, JarEntry jarEntry)
            throws IOException {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(
                jar.getInputStream(jarEntry)));

        StringBuffer result = new StringBuffer();
        char[] cbuf = new char[2000];

        int nb = buffer.read(cbuf);
        while (nb != -1) {
            result.append(cbuf, 0, nb);
            nb = buffer.read(cbuf);
        }
        buffer.close();
        return result.toString();

    }

    public static byte[] getByteContent(JarFile jar, JarEntry jarEnt)
            throws IOException {

        ByteArrayOutputStream content;
        // Extract this to a byte utils ?
        InputStream in = new BufferedInputStream(jar.getInputStream(jarEnt));
        try {
            content = new ByteArrayOutputStream();
            BufferedOutputStream tmp = new BufferedOutputStream(content);
            try {
                for (int b = in.read(); b != -1; b = in.read()) {
                    tmp.write(b);
                }
            } finally {
                tmp.close();
            }
        } finally {
            in.close();
        }
        return content.toByteArray();
    }

}
