/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;

import java.util.Properties;
import java.util.Set;

/**
 * 
 * Class used to store properties used as wikitty index.
 * Used by the wikitty service over jar.
 * 
 * @author mfortun
 *
 */
public class WikittyPropertieIndex {

    /**
     * Index for wikitty 
     * id-> path/wikittypubName
     */
    protected Properties wikittyIndex;
    /**
     * index for version information and mime type
     * 
     * id.mimetype-> "mimetype"
     * id.version -> "version"
     */
    protected Properties wikittyMetadata;
    
    
    
    
    public WikittyPropertieIndex(Properties wikittyIndex,
            Properties wikittyMetadata) {
        this.wikittyIndex = wikittyIndex;
        this.wikittyMetadata = wikittyMetadata;
    }
    public Properties getWikittyIndex() {
        return wikittyIndex;
    }
    public void setWikittyIndex(Properties wikittyIndex) {
        this.wikittyIndex = wikittyIndex;
    }
    public Properties getWikittyMetadata() {
        return wikittyMetadata;
    }
    public void setWikittyMetadata(Properties wikittyMetadata) {
        this.wikittyMetadata = wikittyMetadata;
    }
    
    /**
     * return if the properties that index wikitty name and id
     * contain the id
     * @param id
     * @return if the id is inside the index
     */
    public boolean containtId(String id){
        return wikittyIndex.containsKey(id);
    }
    
    public Set<Object> getIds(){
        return wikittyIndex.keySet();
    }
    
    
}
