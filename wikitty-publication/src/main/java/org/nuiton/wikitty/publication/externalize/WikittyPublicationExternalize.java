/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.externalize;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.ArgumentsParserException;
import org.nuiton.util.FileUtil;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyServiceFactory;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.entities.WikittyLabelHelper;
import org.nuiton.wikitty.publication.MimeTypePubHelper;
import org.nuiton.wikitty.publication.PropertiesExtended;
import org.nuiton.wikitty.publication.CodeDecorator;
import org.nuiton.wikitty.publication.WikittyFileUtil;
import org.nuiton.wikitty.publication.entities.WikittyPubData;
import org.nuiton.wikitty.publication.entities.WikittyPubDataHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubText;
import org.nuiton.wikitty.publication.entities.WikittyPubTextHelper;
import org.nuiton.wikitty.publication.synchro.WikittyPublicationSynchronize;
import org.nuiton.wikitty.publication.synchro.WikittyPublicationFileSystem;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;

/**
 * Entry point for the main externalize functionality
 * 
 * @author mfortun
 * 
 */
public class WikittyPublicationExternalize {

    static private Log log = LogFactory
            .getLog(WikittyPublicationExternalize.class);
    
    static public String JAR_CLASSPATH_DIR="jars";
    
    static public String TEMPORARY_FILE_NAME = "tempBeforeJar";
    static public String DEFAULT_JAR_NAME = "pub-externalized";

    static public String VERSION_SUFFIX = ".version";
    static public String MIME_SUFFIX = ".mime";

    static public String SOURCE_EXTENSION = ".java";
    static public String COMPILED_EXTENSION = ".class";

    /*
     * needed to ensure that fileUtils used inside execution is set with the
     * correct encoding
     */
    static {
        FileUtil.ENCODING = "UTF-8";
    }
     
     /* Class don't have to be instantiate
     */
    private WikittyPublicationExternalize() {
        
    }

    /**
     * Externalize the current dir as if it was a wikitty service on
     * file system.
     * 
     * Create the Jar in the current directory
     * 
     * @param args
     * @throws ArgumentsParserException
     * @throws IOException
     */
    static public void main(String[] args) throws ArgumentsParserException,
            IOException {

        ApplicationConfig appconfig = new ApplicationConfig();
        appconfig.parse(args);
        // Set the correct service use to parse local file
        appconfig.setOption(
                WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS.getKey(),
                WikittyPublicationFileSystem.class.getName());

        // construct the url for local repo
        File currentFile = new File(".");
        currentFile = new File(currentFile.getAbsolutePath()).getParentFile();
        // currentFile = new File("/home/User/testWP");
        String Label = currentFile.getName();

        // String urlFileSystem = "file:///home/User/testWP#wp";
        String urlFileSystem = currentFile.getParent()
                + WikittyPublicationSynchronize.LABEL_DELIM + Label;

        appconfig.setOption(WikittyConfigOption.WIKITTY_SERVER_URL.getKey(),
                urlFileSystem);

        externalize(appconfig);

    }

    /**
     * Externalize all the wikitty found on the wikitty service designed 
     * by the application config
     * 
     * @param conf application config initialized to create a wikitty service
     * @throws ArgumentsParserException
     * @throws IOException
     */
    static public void externalize(ApplicationConfig conf)
            throws ArgumentsParserException, IOException {
        // Found all wikity on the file System
        Criteria findAllCrit = Search.query().keyword("*").criteria();

        externalize(conf, findAllCrit);
    }
    
    /**
     * Externalize all the wikitty found(with the criteria) on the wikitty service designed 
     * by the application config
     * @param conf application config initialized to create a wikitty service
     * @param crit the criteria for the wikitty to externalize
     * @throws ArgumentsParserException
     * @throws IOException
     */
    static public void externalize(ApplicationConfig conf, Criteria crit)
    throws ArgumentsParserException, IOException {
        
        
        File currentFile = new File(".");
        currentFile = new File(currentFile.getAbsolutePath()).getParentFile();
        externalize(conf, crit, currentFile);
        
    }
    /**
     * Externalize all the wikitty found(with the criteria) on the wikitty service designed 
     * by the application config and put the result jar in the target dir
     * @param conf application config initialized to create a wikitty service
     * @param crit the criteria for the wikitty to externalize
     * @param targetDir where the jar will be create
     * @throws ArgumentsParserException
     * @throws IOException
     */
    static public void externalize(ApplicationConfig conf, Criteria crit, File targetDir)
    throws ArgumentsParserException, IOException {
        
        String jarName = DEFAULT_JAR_NAME;
        
        externalize(conf, crit, targetDir, jarName);
    }
    
    /**
     * Externalize all the wikitty found(with the criteria) on the wikitty service designed 
     * by the application config and put the result jar in the target dir with the specific name
     * @param conf application config initialized to create a wikitty service
     * @param crit the criteria for the wikitty to externalize
     * @param targetDir where the jar will be create
     * @param jarName the name for the jar
     * @return the jar created
     * @throws ArgumentsParserException
     * @throws IOException
     */
    static public File externalize(ApplicationConfig conf, Criteria crit, File TargetDir, String jarName)
            throws ArgumentsParserException, IOException {
        MimeTypePubHelper mimeHelper = new MimeTypePubHelper();
        CodeDecorator decorator = new CodeDecorator();
       
       

        WikittyProxy proxy = new WikittyProxy(
                WikittyServiceFactory.buildWikittyService(conf));

        PagedResult<Wikitty> allWikittyOnFS = proxy.findAllByCriteria(crit);

        // make tempfile
        File tempDirectory = new File(TargetDir + File.separator
                + TEMPORARY_FILE_NAME);
        tempDirectory.mkdir();

        File filePropertiesId = new File(tempDirectory + File.separator
                + WikittyFileUtil.WIKITTY_ID_PROPERTIES_FILE);
        filePropertiesId.createNewFile();
        PropertiesExtended idProperties = new PropertiesExtended(
                filePropertiesId);

        File filePropertiesMeta = new File(tempDirectory + File.separator
                + WikittyFileUtil.WIKITTY_FILE_META_PROPERTIES_FILE);
        filePropertiesMeta.createNewFile();
        PropertiesExtended metaProperties = new PropertiesExtended(
                filePropertiesMeta);

        // iterate wikitty
        // - write file
        // - write property
        // - write

        /*
         * Format des propriétés:
         * 
         * id.property 18114-1811-181-18=/bob/truc/nuiton/Script
         * 
         * metadata 18114-1811-181-18.version=1.0 18114-1811-181-18.exention=js
         */
        
        List<File> classPathAdds = extractJarFromServiceForCompil(proxy);
        
        for (Wikitty wikit : allWikittyOnFS) {
            if (!wikit.hasExtension(WikittyLabel.EXT_WIKITTYLABEL)) {
                // non usable wikitty
                continue;
            }

            // construct directory with labels

            Set<String> labeles = WikittyLabelHelper.getLabels(wikit);
            if (labeles.size() == 0) {
                continue;
            }

            String label = labeles.toArray()[0].toString();

            WikittyFileUtil.createFilesFromLabelPath(tempDirectory, label);

            // preparation for properties
            String labelPath = WikittyFileUtil.labelToPath(label);
            labelPath += File.separator;

            String id = wikit.getId();
            metaProperties.put(id + VERSION_SUFFIX, wikit.getVersion());

            String extension;
            File wikittyFile;
            String name;
            String mimeType;

            if (wikit.hasExtension(WikittyPubText.EXT_WIKITTYPUBTEXT)) {
                name = WikittyPubTextHelper.getName(wikit);
                mimeType = WikittyPubTextHelper.getMimeType(wikit);
                extension = mimeHelper.getExtensionForMime(mimeType);
                String content = WikittyPubTextHelper.getContent(wikit);

                // TODO 2011-07-08 change this later or not, java source will be
                // erase by the second file.

                // Write original file
                wikittyFile = new File(tempDirectory.getAbsolutePath()
                        + File.separator + labelPath + name + "." + extension);
                wikittyFile.createNewFile();
                FileUtil.writeString(wikittyFile, content);

                File javaFile = new File(tempDirectory.getAbsolutePath()
                        + File.separator + labelPath + name + SOURCE_EXTENSION);
                javaFile.createNewFile();
                String codeClass = decorator.getCode(wikit);
                FileUtil.writeString(javaFile, codeClass);

                File classDirectory = new File(tempDirectory.getAbsolutePath()
                        + File.separator + labelPath);// + name +
                                                      // COMPILED_EXTENSION);
                // classFile.createNewFile();

                PrintWriter writer = new PrintWriter(System.out);
                // compile
                CompileHelper.compile(classPathAdds,tempDirectory, javaFile, classDirectory,
                        writer);

                // write properties
                idProperties.put(id, labelPath + name);
                metaProperties.put(id + MIME_SUFFIX,
                        mimeType);
            }

            if (wikit.hasExtension(WikittyPubData.EXT_WIKITTYPUBDATA)) {
                byte[] content = WikittyPubDataHelper.getContent(wikit);
                mimeType = WikittyPubDataHelper.getMimeType(wikit);
                extension = mimeHelper.getExtensionForMime(mimeType);
                name = WikittyPubDataHelper.getName(wikit);

                // write file on the temporary directory
                wikittyFile = new File(tempDirectory.getAbsolutePath()
                        + File.separator + labelPath + name + "." + extension);
                wikittyFile.createNewFile();
                FileUtil.byteToFile(content, wikittyFile);

                // write properties
                metaProperties.put(id + MIME_SUFFIX, mimeType);
                idProperties.put(id, labelPath + name);
            }
        }

        metaProperties.store();
        idProperties.store();

        // construct jar
        File jarFile = new File(TargetDir + File.separator + jarName
                + ".jar");
        JarUtil.compressFiles(jarFile, tempDirectory);

        // delete tempfile
        FileUtil.deleteRecursively(tempDirectory);
        
        return jarFile;

    }

    /*
     * Algo fonctionnement
     * 
     * done: Selectionner tout les wikitty sur le file system qui sont dans le
     * dossier courant, en servant d'un wikitty publication fileSystem.
     * 
     * done: On va créer un dossier pour le jar "tempJar" pour chaque wikitty en
     * fonction du type: -wikittyPubData: on va l'écrire sous son label le
     * fichier dans le dossier tempJar et on va le référencer dans l'index
     * central du dossier
     * 
     * stay: -WikittyPubText: .on va écrire le pub text sous son label le
     * fichier correspondant .on va décorer le contenu du wikittyPubText avec
     * les éléments script engine etc, on va écrire le .java correspondant
     * .ensuite on va compiler le java et écrire le .class correspondant .on
     * écrit les metadata et les élément d'index du wikitty pour le retrouver
     * 
     * 
     * 
     * done: Une fois tout les wikitty traité on va packager le dossier tempJar
     * en jar et on va supprimer le dossier tempJar
     */

    
    static protected  List<File> extractJarFromServiceForCompil(WikittyProxy prox){
        List<File> result = new LinkedList<File>();
        
        // check for temporaryn dir
        if (!FileUtils.getTempDirectory().exists()) {
            FileUtils.getTempDirectory().mkdir();
        }
        
        Search wikittyPubDataJarCrit = Search.query().eq(
                WikittyPubData.FQ_FIELD_WIKITTYPUBDATA_MIMETYPE,
                MimeTypePubHelper.JAR_TYPE);
        
        String jarRepoPath = FileUtils.getTempDirectory().getAbsolutePath()
        + File.separator + JAR_CLASSPATH_DIR+"_"+WikittyPublicationSynchronize.class.getSimpleName();
        
        File jarRepo = new File(jarRepoPath);
        if (!jarRepo.exists()){
            jarRepo.mkdir();
        }
        
        result.add(jarRepo);
        PagedResult<Wikitty> jardatas = prox.findAllByCriteria(
                wikittyPubDataJarCrit.criteria());
        
        for (Wikitty w : jardatas) {
            String name = WikittyPubDataHelper.getName(w);
            byte[] content = WikittyPubDataHelper.getContent(w);
            File tempJar = new File(jarRepo.getAbsolutePath()
                    + File.separator + name + ".jar");
            try {
                FileUtils.writeByteArrayToFile(tempJar, content);
                result.add(tempJar);
            } catch (IOException e) {
                log.error("Error while writing jars for compile classpath",e);
                // TODO Mfortun-2011-08-24- not really handled
            }
        }
        
        
        
        return result;
    }
   

}
