/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;


import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.processor.Processor;
import org.nuiton.processor.filters.GeneratorTemplatesFilter;
import org.nuiton.processor.filters.GeneratorTemplatesFilterIn;
import org.nuiton.util.Resource;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.publication.entities.WikittyPubText;
import org.nuiton.wikitty.publication.entities.WikittyPubTextHelper;
import org.nuiton.wikitty.search.operators.Element;

/**
 * This class is used to decorate wikitty pub text content.
 * Used before compile wikittyPubText to encapsulated content inside java method.
 * Used to apply filter over wikittyPubText content when containing ui directive
 * of others directive that need to be filtered before rendering.
 * Directive that have to be encapsulate inside script vars 
 *
 * @author mfortun
 *
 */
public class CodeDecorator {

    protected static final String FILTERS_FILE_NAME_REGEX = "filters-\\w*\\.properties";

    static private Log log = LogFactory.getLog(CodeDecorator.class);

    /*
     * Après le script engine le ui Code décorateur
     * 
     * décorer à l'inverse en fait transformer du html en js qui contient du
     * html par exemple
     * 
     * regex for html end token need to add /n at the end "</[\\w\\d]*>"
     */

    static protected String MIME_SEP = "/";

    // TODO mfortun-2011-08-30 move those to properties file.
    static protected String filtersOptionDirName = "filters-properties/";
    static protected String filtersOptionDefault = "filters-default.properties";

    protected Map<String, FilterOption> filtersOptions;

    /**
     * Mime type helper used to transform mime
     */

    static protected FilterOption defaultFilterOption;

    static {

        InputStream input = CodeDecorator.class.getResourceAsStream("/"
                + filtersOptionDirName + filtersOptionDefault);

        Properties mimeProps = new Properties();

        try {
            mimeProps.load(input);

        } catch (IOException e) {
            // TODO mfortun-2011-08-16 Exception not really handled
            if (log.isErrorEnabled()) {
                log.error(
                        "Error while reading properties Fie that containt filters option mapping ",
                        e);
            }
        }

        defaultFilterOption = new FilterOption();
        defaultFilterOption.setOpeningTemplate(mimeProps
                .getProperty(FilterOption.OPENING_TEMPLATE));
        defaultFilterOption.setWriteString(mimeProps
                .getProperty(FilterOption.WRITE_STRING));
        defaultFilterOption.setStringDelim(mimeProps
                .getProperty(FilterOption.STRING_DELIM));
        defaultFilterOption.setConcatChar(mimeProps
                .getProperty(FilterOption.CONCAT_CHAR));
        defaultFilterOption.setEndingCar(mimeProps
                .getProperty(FilterOption.ENDING_CAR));
        defaultFilterOption.setClosingWriterChar(mimeProps
                .getProperty(FilterOption.CLOSING_WRITER_CHAR));
        defaultFilterOption.setOpeningWriterChar(mimeProps
                .getProperty(FilterOption.OPENING_WRITER_CHAR));
        defaultFilterOption.setClosingTemplate(mimeProps
                .getProperty(FilterOption.CLOSING_TEMPLATE));
        defaultFilterOption.setKey(mimeProps.getProperty(FilterOption.KEY));


    }

    public CodeDecorator() {
        filtersOptions = new HashMap<String, FilterOption>();
        
        // add basic value js and htmlp
        filtersOptions.put(defaultFilterOption.getKey(), defaultFilterOption);
        try {
            List<URL> urlsRessourceFilters = Resource.getResources(
                    filtersOptionDirName + FILTERS_FILE_NAME_REGEX, Thread
                            .currentThread().getContextClassLoader());

            for (URL propFil : urlsRessourceFilters) {

                Properties props = new Properties();
                InputStream propsstrem = propFil.openStream();
                props.load(propsstrem);

                // parse properties
                FilterOption option = new FilterOption();
                option.setOpeningTemplate(props
                        .getProperty(FilterOption.OPENING_TEMPLATE));
                option.setWriteString(props
                        .getProperty(FilterOption.WRITE_STRING));
                option.setStringDelim(props
                        .getProperty(FilterOption.STRING_DELIM));
                option.setConcatChar(props.getProperty(FilterOption.CONCAT_CHAR));
                option.setEndingCar(props.getProperty(FilterOption.ENDING_CAR));
                option.setClosingWriterChar(props
                        .getProperty(FilterOption.CLOSING_WRITER_CHAR));
                option.setOpeningWriterChar(props
                        .getProperty(FilterOption.OPENING_WRITER_CHAR));
                option.setClosingTemplate(props
                        .getProperty(FilterOption.CLOSING_TEMPLATE));
                option.setKey(props.getProperty(FilterOption.KEY));

                // addfilters options
                filtersOptions.put(option.getKey(), option);
            }

        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                // TODO mfortun-2011-08-26 Exception simply handled
                log.error("Error While reading filters option properties file",
                        e);
            }
        }
    }

    public Wikitty transformPubUIToPubText(Wikitty wikitty) throws IOException {

        if (!wikitty.hasExtension(WikittyPubText.EXT_WIKITTYPUBTEXT)) {
            return wikitty;
        }

        GeneratorTemplatesFilter filter = new GeneratorTemplatesFilter();
        GeneratorTemplatesFilterIn filterIn = new GeneratorTemplatesFilterIn(
                filter);

        String mime = WikittyPubTextHelper.getMimeType(wikitty);
        String initialVersion = wikitty.getVersion();
        
        MimeTypePubHelper helper = new MimeTypePubHelper();
        String mimeAfterTransformation = helper.pubUiMimeToTargetMime(mime);
        String contentType = helper.uiMimeContentType(mime);

        /*
         * changement de mime type changement de
         */
        /*
         * Fonction du mime type
         */

        String[] mimeTab = StringUtil.split(mime, MIME_SEP);

        FilterOption foption = filtersOptions.get(mimeTab[1]);
        if (foption == null) {
            foption = defaultFilterOption;
        }

        filterIn.setConcacChar(foption.getConcatChar());
        filterIn.setStringDelim(foption.getStringDelim());
        filterIn.getParent().setWriteString(foption.getWriteString());
        filterIn.setEndCar(foption.getEndingCar());
        filterIn.setOpeningWriterChar(foption.getOpeningWriterChar());
        filterIn.setClosingWriterChar(foption.getClosingWriterChar());

        String decoringAffect = foption.getOpeningTemplate();
        // initialize new content
        String decoredContent = StringUtils.EMPTY;

        // set default content result (override if wikitty content define it)
        decoredContent += WikittyPublicationConstant.CONTEXT_VAR
                + ".setContentType(\"" +contentType + "\")"
                + foption.getEndingCar();

        Processor proc = new Processor();
        proc.setInputFilter(filterIn);

        StringWriter writer = new StringWriter();
        String content = WikittyPubTextHelper.getContent(wikitty);
        StringReader reader = new StringReader(content);
        // process string
        proc.process(reader, writer);

        decoredContent += decoringAffect;
        // replace element between <% %> and <%= %>
        decoredContent += writer.toString();

        // end of string
        decoredContent += foption.getClosingTemplate();
        // set the content
        WikittyPubTextHelper.setContent(wikitty, decoredContent);
        // get associated mimeType and set it
        WikittyPubTextHelper.setMimeType(wikitty, mimeAfterTransformation);
        // restore version
        wikitty.setVersion(initialVersion);

        return wikitty;
    }

    public boolean isTransformationNeeded(Wikitty wikitty) {
        if (!wikitty.hasExtension(WikittyPubText.EXT_WIKITTYPUBTEXT)) {
            return false;
        }
        String mime = WikittyPubTextHelper.getMimeType(wikitty);
        return isMimeTypeUi(mime);
    }

    public boolean isTransformationNeeded(WikittyPubText wikitty) {
        String mime = wikitty.getMimeType();
        return isMimeTypeUi(mime);
    }

    public boolean isMimeTypeUi(String mime) {
        MimeTypePubHelper helper = new MimeTypePubHelper();
        String mimeKeyOption = helper.uiMimeToFilterOptionKey(mime);
        return filtersOptions.containsKey(mimeKeyOption);
    }

    public boolean isDecorated(String string) {
        return filtersOptions.containsKey(string);

    }

    /**
     * Use to transform wikittyPubText.content into compilable Java
     *
     * @param wikitty
     *            the wikitty pub text
     * @return the java code
     */
    public String getCode(Wikitty wikitty) {

        if (isTransformationNeeded(wikitty)) {
            try {
                wikitty = transformPubUIToPubText(wikitty);
            } catch (IOException e) {
                log.debug("Error while transform ui");
            }
        }

        String content = WikittyPubTextHelper.getContent(wikitty);
        String mimeType = WikittyPubTextHelper.getMimeType(wikitty);
        String className = WikittyPubTextHelper.getName(wikitty);// +"PublicationClass";
        String classContent = StringUtils.EMPTY;

        // Set<String> labels = (Set<String>)
        // wikitty.getField(WikittyLabel.EXT_WIKITTYLABEL,
        // WikittyLabel.FIELD_WIKITTYLABEL_LABELS);
        // classContent += "package org.nuiton.wikitty.publication;" ;
        // classContent += "package "+ labels.toArray()[0].toString() +";";
        classContent += "import org.apache.commons.logging.Log;";
        classContent += "import org.apache.commons.logging.LogFactory;";
        classContent += "import org.nuiton.wikitty.ScriptEvaluator;";
        classContent += "import " + AbstractDecoredClass.class.getName() + ";";
        classContent += "import org.nuiton.wikitty.entities.*;";
        classContent += "import org.nuiton.wikitty.publication.entities.*;";
        classContent += "import org.nuiton.wikitty.publication.*;";
        classContent += "import java.util.*;";

        classContent += "public class " + className + " extends "
                + AbstractDecoredClass.class.getSimpleName() + " {";

        classContent += "public Object eval(Map<String, Object> bindings ) throws Exception {";

        if (mimeType.equals(MimeTypePubHelper.JAVA_TYPE)) {

            /*
             * Iterate on element that must be in the context and write
             * 
             * classContent = "+type+" "+name+" = bindings.get(\""+name+"\");";
             * 
             * ?
             */
            // contruct variables that can be used inside the java code.

            classContent += PublicationContext.class.getSimpleName() + " "
                    + WikittyPublicationConstant.CONTEXT_VAR + " = ("
                    + PublicationContext.class.getSimpleName()
                    + ") bindings.get(\""
                    + WikittyPublicationConstant.CONTEXT_VAR + "\");";
            classContent += EvalInterface.class.getSimpleName() + " "
                    + WikittyPublicationConstant.EVAL_VAR + " = ("
                    + EvalInterface.class.getSimpleName() + ") bindings.get(\""
                    + WikittyPublicationConstant.EVAL_VAR + "\");";
            classContent += "String "
                    + WikittyPublicationConstant.PAGE_NAME_VAR
                    + " = (String) bindings.get(\""
                    + WikittyPublicationConstant.PAGE_NAME_VAR + "\");";
            classContent += "List<String> "
                    + WikittyPublicationConstant.SUBCONTEXT_VAR
                    + " = (List<String>) bindings.get(\""
                    + WikittyPublicationConstant.SUBCONTEXT_VAR + "\");";
            classContent += "Wikitty " + WikittyPublicationConstant.WIKITTY_VAR
                    + " = (Wikitty) bindings.get(\""
                    + WikittyPublicationConstant.WIKITTY_VAR + "\");";

            classContent += content;

        } else {
            // TODO mfortun-2011-07-08 write a better "filter" must isolate
            // specific
            // mime type that correspond to precompilable langague
            classContent += "Object result = null;";
            classContent += "String content = \""
                    + StringEscapeUtils.escapeJava(content) + "\";";
            classContent += "String mimeType = \""
                    + StringEscapeUtils.escapeJava(mimeType) + "\";";
            classContent += "String criteriaName=  \"" + Element.ELT_ID + ":"
                    + wikitty.getId() + "\";";
            classContent += " result = ScriptEvaluator.eval(null, criteriaName, content, mimeType, bindings);";
            classContent += "return result;";
        }

        classContent += "\n}\n}\n";

        return classContent;
    }

    public Map<String, FilterOption> getFiltersOptions() {
        return filtersOptions;
    }

    public void setFiltersOptions(Map<String, FilterOption> filtersOptions) {
        this.filtersOptions = filtersOptions;
    }

}
