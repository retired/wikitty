/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;

import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.ScriptEvaluator;

import javax.script.ScriptEngineManager;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

/**
 * Class used to determine mimetype for an extension, used to determine type of
 * wikittypub (data or text) with the extension and the mime type.
 * 
 * This class is the one which handle mime type <-> file extension mapping.
 * 
 * 
 * 
 * @author mfortun
 * 
 */
public class MimeTypePubHelper {

    static private Log log = LogFactory.getLog(MimeTypePubHelper.class);

    public static final String ACTION_TYPE = "text/java.action";
    public static final String JAVA_TYPE = "text/java";
    public static final String JAR_TYPE = "application/jar";
    /**
     * Mapping between extention and mime type, key: extension, value: mimeType
     */
    protected BidiMap bidiMap;
    // TODO mfortun-2011-08-05 change this, this is a hack
    // change this to a handler to have specific langage compiler as jython etc

    protected ScriptEngineManager manager;


    static public String COMMON_MIME_SEP = "/";
    static public String PUBLICATION_MIME_SEP = ".";

    static public String PREFIX_MIME_PUB_TEXT = "text";
    /**
     * MimeType properties files name
     */
    static public String MIME_PROPERTIE = "mimetype.properties";
    /**
     * match pub text that must be converted via ui decorateur text/\w*\.\w* as
     * text/html.javascript
     */
    static public String REGEX_PUB_TEXT_TRANSFORM = "text/\\w*\\.\\w*";

    /**
     * The default mime type
     */
    public static String DEFAULT_MIME_TYPE = "application/octet-stream";

    public MimeTypePubHelper() {

        bidiMap = new DualHashBidiMap();
        manager = ScriptEvaluator.getScriptEnginManager(null);

        // load properties for mime type and file extension
        InputStream input = MimeTypePubHelper.class
                .getResourceAsStream("/mimetype.properties");

        Properties mimeProps = new Properties();
        try {
            mimeProps.load(input);

        } catch (IOException e) {
            // TODO mfortun-2011-08-16 Exception not really handled
            if (log.isErrorEnabled()) {
                log.error(
                        "Error while reading properties Fie that containt mimetype mapping ",
                        e);
            }
        }

        for (Map.Entry<Object, Object> en : mimeProps.entrySet()) {
            bidiMap.put(en.getValue(), en.getKey());
        }

        /*
         * 
         * si type commence par text alors -> pubtext si text/machin.truc ça
         * veut dire qu'on doit le préprosesser avec le bon truc
         */

    }

    /**
     * Return the corresponding mime Type for an extension, default if not
     * recognized
     * 
     * @param mime
     *            the extension
     * @return the corresponding mimeTypeForExt DEFAULT_MIME_TYPE if ext not
     *         recognized
     */
    public String getExtensionForMime(String mime) {
        bidiMap = bidiMap.inverseBidiMap();
        String result = (String) bidiMap.get(mime);
        bidiMap = bidiMap.inverseBidiMap();
        return result;
    }

    /**
     * Return the corresponding mime Type for an extension, default if not
     * recognized
     * 
     * @param ext
     *            the extension
     * @return the corresponding mimeTypeForExt DEFAULT_MIME_TYPE if ext not
     *         recognized
     */
    public String getMimeForExtension(String ext) {
        String result = (String) bidiMap.get(ext);

        if (result == null) {
            result = DEFAULT_MIME_TYPE;
        }

        return result;
    }

    /**
     * Used to check if a file have to be converted as a wikittyPubText
     * 
     * @param mimeType
     *            of the file
     * @return if the mimetype of a file correspond to a wikittyPubText
     */
    public boolean isPubTextMime(String mimeType) {
        // will check if there is an engine for the mimetype

        boolean result;

        // check if mimetype startwith the PUbtext prefix
        result = mimeType.startsWith(PREFIX_MIME_PUB_TEXT);
        CodeDecorator decorator = new CodeDecorator();
        // if mimetype match the regex for wikitty pub text that must be
        // decorate by uidecorator,
        if (mimeType.matches(REGEX_PUB_TEXT_TRANSFORM)) {
            String[] mimeTab = StringUtil.split(mimeType, COMMON_MIME_SEP);
            // construct final mime of the script after transformation
            String originalMime = mimeTab[0];
            originalMime += COMMON_MIME_SEP
                    + StringUtil.split(mimeType, PUBLICATION_MIME_SEP)[1];

            // check if there is an engine to execute final script
            result = manager.getEngineByMimeType(originalMime) != null
                    || originalMime.equals(JAVA_TYPE);
            // check if there rules to transform the script
            result = result && decorator.isDecorated(mimeTab[1]);
        }

        return result;
    }

    /**
     * return the convert part of a mime type, part used as a key
     * for converter option.
     * 
     * for example : text/html.java return html.java
     * @param mime
     * @return
     */
    public String uiMimeToFilterOptionKey(String mime) {
        String result = mime;
        if (mime.matches(REGEX_PUB_TEXT_TRANSFORM)) {
            result = StringUtil.split(mime, COMMON_MIME_SEP)[1];
        }
        return result;
    }
    
    public String uiMimeContentType(String mime) {
        String result = mime;
        if (mime.matches(REGEX_PUB_TEXT_TRANSFORM)) {
            result = StringUtil.split(mime, COMMON_MIME_SEP)[0];
        }
        return result;
    }
    
    

    /**
     * Used to convert uiMime to the mimeType after content decoration
     * 
     * @param mime
     * @return the mimeType after Ui content decoration
     */
    public String pubUiMimeToTargetMime(String mime) {
        String result = mime;
        if (mime.matches(REGEX_PUB_TEXT_TRANSFORM)) {
            String[] mimeTab = StringUtil.split(mime, COMMON_MIME_SEP);
            // construct final mime of the script after transformation
            result = mimeTab[0];
            result += COMMON_MIME_SEP
                    + StringUtil.split(mime, PUBLICATION_MIME_SEP)[1];
        }
        return result;
    }

    /**
     * used to check if a file have to be converted as a wikittyPubText with his
     * extension similar to : <br>
     * isPubTextMime(getMimeForExtension(extension))</br>
     * 
     * @param extension
     *            the file extension
     * @return if the extension correspond to a wikittyPubText
     */
    public boolean isPubTextExtension(String extension) {
        return isPubTextMime(getMimeForExtension(extension));
    }

    /**
     * Add an entry in the map that store mapping between extension and
     * mimeType.
     * 
     * @param extension
     * @param mime
     */
    public void addExtensionMime(String extension, String mime) {
        bidiMap.put(extension, mime);
    }

    public ScriptEngineManager getManager() {
        return manager;
    }

    public void setManager(ScriptEngineManager manager) {
        this.manager = manager;
    }


    
    
}
