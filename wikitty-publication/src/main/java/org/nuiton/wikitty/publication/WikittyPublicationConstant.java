/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;
/**
 * Wikitty Publication Constant use inside wikitty Publication.
 * @author mfortun
 *
 */
public class WikittyPublicationConstant {

    /*
     * Variable relative to scriping/binding
     */
    /** variable contenant l'instance de la classe ActionEval */
    static final public String EVAL_VAR = "wpEval";
    /** variable name use to put context in script and jsp */
    static final public String CONTEXT_VAR = "wpContext";
    /** contient la liste des arguments mandatory non encore utilise */
    static final public String SUBCONTEXT_VAR = "wpSubContext";
    /**
     * contient le nom de la page WikittyPubText (ex: Wiki) ou la requete ayant
     * permis de trouver la page (ex: MyScript.name=df)
     */
    static final public String PAGE_NAME_VAR = "wpPage";
    /**
     * contient le wikitty utilise comme script
     */
    static final public String WIKITTY_VAR = "wpWikitty";

    static final public String LABEL_DELIM = "#";
    
    
    
    
}
