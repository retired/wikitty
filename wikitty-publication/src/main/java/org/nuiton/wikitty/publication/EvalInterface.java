/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;

import java.util.List;

/**
 * Interface of the eval action. Used 
 * @author mfortun
 *
 */
public interface EvalInterface {

    /**
     * eval the current publicat ion context
     * @param context the current context 
     * @param subContext list of the subcontext
     * @return the result of the evaluation
     */
    public Object doAction(PublicationContext context, List<String> subContext);

    /**
     *  eval the current publication context
     * @param context the current context
     * @param subContextAsText the string to be evaluated
     * @return the result of the evaluation
     */
    public Object doAction(PublicationContext context, String subContextAsText);

    /**
     * eval the current publication context
     * @param context the current context
     * @return the result of the evaluation
     */
    public Object doAction(PublicationContext context);

}