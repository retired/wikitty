/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;

import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Interface for eval action.
 * 
 * @author mfortun
 *
 */
public interface PublicationContext {

    HttpServletRequest getRequest();

    HttpServletResponse getResponse();
   
    /**
     * the current wikitty proxy
     * @return
     */
    WikittyProxy getWikittyProxy();

    /**
     * add context to the url and parameter if necessary
     * @param url
     * @return
     */
    String makeUrl(String url);
    
    /**
     * the current wikitty service
     * @return
     */
    WikittyService getWikittyService();

    List<String> getMandatoryArguments();

    String getArgument(String name);
    
    /**
     * Get
     * @param name
     * @param defaultValue
     * @return
     */
    String getArgument(String name, String defaultValue);

    /**
     * return the actual return content tye for the page
     * @return
     */
    String getContentType();

    /**
     * Set the content type for the return page
     * @param contentType
     */
    void setContentType(String contentType);
    
    String toString();
    
    /**
     * the map of the arguments in the context
     * @return
     */
    Map<String,String> getArguments();

}