/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.externalize;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.entities.WikittyLabelHelper;
import org.nuiton.wikitty.entities.WikittyLabelImpl;
import org.nuiton.wikitty.publication.AbstractWikittyFileService;
import org.nuiton.wikitty.publication.MimeTypePubHelper;
import org.nuiton.wikitty.publication.WikittyFileUtil;
import org.nuiton.wikitty.publication.WikittyPropertieIndex;
import org.nuiton.wikitty.publication.entities.WikittyPubData;
import org.nuiton.wikitty.publication.entities.WikittyPubDataHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubDataImpl;
import org.nuiton.wikitty.publication.entities.WikittyPubText;
import org.nuiton.wikitty.publication.entities.WikittyPubTextCompiled;
import org.nuiton.wikitty.publication.entities.WikittyPubTextCompiledHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubTextCompiledImpl;
import org.nuiton.wikitty.publication.entities.WikittyPubTextHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubTextImpl;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.TreeNodeResult;
import org.nuiton.wikitty.services.WikittyEvent;
import org.nuiton.wikitty.services.WikittyListener;

/**
 * Wikitty service use to load wikitty store inside jar file
 * 
 * @author mfortun
 * 
 */
public class WikittyServiceJarLoader extends AbstractWikittyFileService {

    public static String JAR_LOCATION_KEY = "wikitty.publication.repository.jar";

    public static String JAR_PATH_SEPARATOR = "/"; 
    
    /** Class logger. */
    private static Log log = LogFactory.getLog(WikittyServiceJarLoader.class);
    
    protected String dirLocation;
    protected String MD5;
    
    protected Map<JarFile, WikittyPropertieIndex> index;
    
    protected MimeTypePubHelper mimeHelper;

    /*
     * 
     * Possible d'uploader un diagramme uml
     * le mettre dedans un wikittypubdata
     * et généré le jar avec les sources en fonction de ce modèle
     * 
     * On saura que c'est un modèle grace au mime type
     */

    public WikittyServiceJarLoader(ApplicationConfig config) {
        mimeHelper = new MimeTypePubHelper();
        dirLocation =  config.getOption(JAR_LOCATION_KEY);
        index = new HashMap<JarFile, WikittyPropertieIndex>();
        MD5 = StringUtil.encodeMD5(StringUtils.EMPTY);
        constructIndex();
    }

    protected void constructIndex() {        
        try {
            log.info("Initialize Jar Index on dir" + dirLocation);
            // initialise location
            File jarLocation = new File(dirLocation);
            //found jar
            File[] jarFiles = jarLocation.listFiles(JarUtil.jarFilter);
            
            // TODO mfortun-2011-08-18 BAD solution: need a notifier system
            // to allow service to rebuild his index
            String sumMd5 = StringUtils.EMPTY;
            if (jarFiles!=null){
                for (File jf : jarFiles){
                    sumMd5+=jf.getAbsolutePath();
                }
            }
            sumMd5 = StringUtil.encodeMD5(sumMd5);
            // check if md5 is equal to the last reindaxtion
            // if not rebuilt the index
            if (!sumMd5.equals(MD5)){
                index = new HashMap<JarFile, WikittyPropertieIndex>();
                // iterate over jar to load ids properties 
                // and meta properties to handle them
                for (File jf : jarFiles) {
                    JarFile tempJF = new JarFile(jf);

                    JarEntry metaEntry = tempJF
                            .getJarEntry(WikittyFileUtil.WIKITTY_FILE_META_PROPERTIES_FILE);
                    JarEntry idEntry = tempJF
                            .getJarEntry(WikittyFileUtil.WIKITTY_ID_PROPERTIES_FILE);

                    Properties wikittyIndex = new Properties();
                    wikittyIndex.load(tempJF.getInputStream(idEntry));

                    Properties wikittyMetadata = new Properties();
                    wikittyMetadata.load(tempJF.getInputStream(metaEntry));

                    WikittyPropertieIndex indexProps = new WikittyPropertieIndex(
                            wikittyIndex, wikittyMetadata);

                    index.put(tempJF, indexProps);

                }
            }
            
        } catch (IOException e) {
            // TODO mfortun-2011-08-12 Exception not really handled
            if (log.isErrorEnabled()){
                log.error("Error while loading jars:", e );
            }
        }
        
    }

    @Override
    public void addWikittyServiceListener(WikittyListener listener,
            ServiceListenerType type) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        //

    }

    @Override
    public void removeWikittyServiceListener(WikittyListener listener,
            ServiceListenerType type) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        //

    }

    @Override
    public String login(String login, String password) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public void logout(String securityToken) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        //

    }

    @Override
    public WikittyEvent clear(String securityToken) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public boolean canWrite(String securityToken, Wikitty wikitty) {
        return false;

    }

    @Override
    public boolean canDelete(String securityToken, String wikittyId) {
        return false;
    }

    @Override
    public boolean canRead(String securityToken, String wikittyId) {
        return exists(securityToken, wikittyId);
    }

    @Override
    public boolean exists(String securityToken, String wikittyId) {
        return exists(wikittyId);
    }

    protected boolean exists(String wikittyId) {
        boolean result = false;
        for (WikittyPropertieIndex in : index.values()) {
            result = result || in.containtId(wikittyId);
            if (result) {
                break;
            }
        }
        
        return result;
    }

    @Override
    public boolean isDeleted(String securityToken, String wikittyId) {
        return false;
    }

    @Override
    public WikittyEvent replay(String securityToken, List<WikittyEvent> events,
            boolean force) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public WikittyEvent store(String securityToken,
            Collection<Wikitty> wikitties, boolean force) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public List<String> getAllExtensionIds(String securityToken) {

        List<String> result = new LinkedList<String>();
        result.add(WikittyPubData.EXT_WIKITTYPUBDATA);
        result.add(WikittyPubText.EXT_WIKITTYPUBTEXT);
        result.add(WikittyPubTextCompiled.EXT_WIKITTYPUBTEXTCOMPILED);
        result.add(WikittyLabel.EXT_WIKITTYLABEL);

        return result;
    }

    @Override
    public List<String> getAllExtensionsRequires(String securityToken,
            String extensionName) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public WikittyEvent storeExtension(String securityToken,
            Collection<WikittyExtension> exts) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public WikittyEvent deleteExtension(String securityToken,
            Collection<String> extNames) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public WikittyExtension restoreExtension(String securityToken,
            String extensionId) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public WikittyExtension restoreExtensionLastVersion(String securityToken,
            String name) {

        WikittyExtension result = super.restoreExtensionLastVersion(
                securityToken, name);
        if (result != null) {
            return result;
        }

        if (name.equals(WikittyPubTextCompiled.EXT_WIKITTYPUBTEXTCOMPILED)) {
            return WikittyPubTextCompiledImpl.extensionWikittyPubTextCompiled;
        }
        
        return null;
    }

    @Override
    public List<Wikitty> restore(String securityToken, List<String> ids) {

        List<Wikitty> result = new LinkedList<Wikitty>();

        for (String wikittyId : ids) {
            // if wikitty doesn't exist jump to the next id

            result.add(restore(wikittyId));
        }

        return result;
    }

    @Override
    public WikittyEvent delete(String securityToken, Collection<String> ids) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    protected Map<String, Wikitty> getAllWikitties() {
        constructIndex();
        
        Map<String, Wikitty> result = new HashMap<String, Wikitty>();

        for (WikittyPropertieIndex in : index.values()) {
            for (Object oId : in.getIds()) {
                String id = oId.toString();
                result.put(id, restore(id));
            }
        }

        return result;
    }

    @Override
    public WikittyEvent deleteTree(String securityToken, String treeNodeId) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public TreeNodeResult<String> findTreeNode(String securityToken,
            String wikittyId, int depth, boolean count, Criteria filter) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public Wikitty restoreVersion(String securityToken, String wikittyId,
            String version) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        // return null;

    }

    @Override
    public void syncSearchEngine(String securityToken) {
        // TODO mfortun
        throw new UnsupportedOperationException("not yet implemented");
        //

    }

    protected Wikitty restore(String wikittyId) {

        Wikitty wikit = null;

        if (!exists(wikittyId)) {
            return null;
        }

        Properties wikittyMetadata = null;
        Properties wikittyIndex = null;
        JarFile wikittyJarRepository = null;
        
        for (Entry<JarFile, WikittyPropertieIndex> en :index.entrySet()){
            if (en.getValue().containtId(wikittyId)){
                wikittyJarRepository = en.getKey();
                wikittyIndex = en.getValue().getWikittyIndex();
                wikittyMetadata = en.getValue().getWikittyMetadata();
            }
        }
        if (log.isDebugEnabled()){
            log.debug("restoring on "+ wikittyJarRepository);
        }
        
        String mime = wikittyMetadata.getProperty(wikittyId
                + WikittyPublicationExternalize.MIME_SUFFIX);
        String fileExtension = mimeHelper.getExtensionForMime(mime);
        String path = wikittyIndex.getProperty(wikittyId);
        String version = wikittyMetadata.getProperty(wikittyId
                + WikittyPublicationExternalize.VERSION_SUFFIX);

        JarEntry wikittyJared = wikittyJarRepository.getJarEntry(path + "."
                + fileExtension);

        wikit = new WikittyImpl(wikittyId);
        wikit.setVersion(version);

        wikit.addExtension(WikittyLabelImpl.extensionWikittyLabel);

        // set labels computate name
        String[] namPath = StringUtil.split(path, JAR_PATH_SEPARATOR);
        String wikittyName = namPath[namPath.length - 1];

        String label = "";
        for (int i = 0; i < namPath.length - 1; i++) {
            label += namPath[i];
            if (i < namPath.length - 2) {
                label += WikittyFileUtil.WIKITTY_LABEL_SEPARATOR;
            }
        }

        WikittyLabelHelper.addLabels(wikit, label);
        try {
            if (mimeHelper.isPubTextMime(mime)) {
                if(log.isDebugEnabled()){
                    log.debug("Handle Wikitty pubtext: " + wikit);
                }
                // wikitty pub text compiled
                wikit.addExtension(WikittyPubTextImpl.extensions);
                wikit.addExtension(WikittyPubTextCompiledImpl.extensionWikittyPubTextCompiled);

                // basic field
                WikittyPubTextHelper.setMimeType(wikit, mime);
                WikittyPubTextHelper.setName(wikit, wikittyName);

                // contents fields:
                JarEntry wikittyclass = wikittyJarRepository.getJarEntry(path
                        + WikittyPublicationExternalize.COMPILED_EXTENSION);

                WikittyPubTextHelper.setContent(wikit, JarUtil
                        .getStringContent(wikittyJarRepository, wikittyJared));
                byte[] bytCont= JarUtil
                .getByteContent(wikittyJarRepository, wikittyclass);
                
                WikittyPubTextCompiledHelper.setByteCode(wikit,bytCont);

            } else {
                if(log.isDebugEnabled()){
                    log.debug("Handle Wikitty pubdata: " + wikit);
                }
                // wikitty pub data
                wikit.addExtension(WikittyPubDataImpl.extensionWikittyPubData);
                WikittyPubDataHelper.setMimeType(wikit, mime);

                WikittyPubDataHelper.setName(wikit, wikittyName);

                WikittyPubDataHelper.setContent(wikit, JarUtil.getByteContent(
                        wikittyJarRepository, wikittyJared));
            }
        } catch (IOException e) {
            // TODO mfortun-2011-08-12 Exception not really handled
            if (log.isErrorEnabled()){
                log.error("Error while reading jar:"+wikittyJarRepository.getName(), e );
            }
        }
        

        return wikit;
    }

}
