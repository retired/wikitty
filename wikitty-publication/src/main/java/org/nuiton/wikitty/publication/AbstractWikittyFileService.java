/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.entities.WikittyLabelImpl;
import org.nuiton.wikitty.entities.FieldType.TYPE;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.publication.entities.WikittyPubData;
import org.nuiton.wikitty.publication.entities.WikittyPubDataImpl;
import org.nuiton.wikitty.publication.entities.WikittyPubText;
import org.nuiton.wikitty.publication.entities.WikittyPubTextImpl;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.operators.And;
import org.nuiton.wikitty.search.operators.AssociatedRestriction;
import org.nuiton.wikitty.search.operators.Between;
import org.nuiton.wikitty.search.operators.BinaryOperator;
import org.nuiton.wikitty.search.operators.Contains;
import org.nuiton.wikitty.search.operators.Element;
import org.nuiton.wikitty.search.operators.False;
import org.nuiton.wikitty.search.operators.In;
import org.nuiton.wikitty.search.operators.Keyword;
import org.nuiton.wikitty.search.operators.Not;
import org.nuiton.wikitty.search.operators.Null;
import org.nuiton.wikitty.search.operators.Or;
import org.nuiton.wikitty.search.operators.Restriction;
import org.nuiton.wikitty.search.operators.RestrictionName;
import org.nuiton.wikitty.search.operators.True;
import org.nuiton.wikitty.storage.WikittySearchEngineInMemory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.collections.CollectionUtils;


/**
 * This service must be extends by wikitty service that are over file system
 * directly or this kind of storage for wikitty.
 * 
 * 
 * 
 * @author mfortun
 *
 */
public abstract class AbstractWikittyFileService implements WikittyService{

    
    final static Log log = LogFactory.getLog(AbstractWikittyFileService.class);

    public AbstractWikittyFileService() {
    }
    
    /**
     * Method that must be implemented by the extended service. This must return
     * all the wikitty stored. The result will be use to checkrestriction for
     * a find.
     * @return a Map with wikittyId as Key and wikitty as value
     */
    protected abstract Map<String, Wikitty> getAllWikitties();
    
    

    /**
     * Write by jcouteau, used to check if a wikitty check a restriction
     * 
     * @see WikittySearchEngineInMemory#checkRestriction
     * 
     * @param restriction
     *            the restriction
     * @param w
     *            the wikitty to check
     * @return if the wikitty check the restriction
     */
    public boolean checkRestriction(Restriction restriction, Wikitty w) {
        
        
        if (restriction instanceof BinaryOperator) {
            BinaryOperator binOp = (BinaryOperator) restriction;
    
            String fqfieldName = binOp.getElement().getName();
    
            // Checks on extensions
            if (Element.ELT_EXTENSION.equals(fqfieldName)) {
                boolean checked = false;
    
                switch (restriction.getName()) {
                case NOT_EQUALS:
                    checked = !w.getExtensionNames().contains(binOp.getValue());
                    break;
                case EQUALS:
                    checked = w.getExtensionNames().contains(binOp.getValue());
                    break;
                }
    
                return checked;
    
                // Checks on id
            } else if (Element.ELT_ID.equals(fqfieldName)) {
    
                boolean checked = false;
    
                switch (restriction.getName()) {
                case NOT_EQUALS:
                    checked = !w.getId().equals(binOp.getValue());
                    break;
                case EQUALS:
                    checked = w.getId().equals(binOp.getValue());
                    break;
                }
    
                return checked;
            }
    
            // si les wikitty n'ont meme pas l'extension concerné
            // Le check restriction, ne doit pas tester les champs
            // si les wikitty n'ont meme pas l'extension concerné
            String[] extName = fqfieldName.split("\\.");
            if (!w.hasField(extName[0], extName[1])) {
    
                // return true in case of not equals
                if (RestrictionName.NOT_EQUALS == restriction.getName()) {
                    return true;
                }
    
                return false;
            }
            // recupere la valeur dans le wikitty
            Object o = w.getFqField(fqfieldName);
    
            // recupere le type de la valeur
            FieldType t = w.getFieldType(fqfieldName);
            // convertie la valeur a verifier dans le meme type que la valeur
            // du wikitty
            Object value = binOp.getValue();
            if (!(value instanceof Collection) && t.isCollection()) {
                // on doit encapsuler dans une collection, car la creation
                // de la requete ajoute autant de v == o && ... que de valeurs
                // dans la collection (champs multi-value solr). Mais
                // dans le inmemory on doit retrouve des collections et non pas
                // des objets seuls :(
                value = Collections.singleton(value);
            }
            value = t.getValidValue(value);
    
            boolean checked = false;
    
            switch (restriction.getName()) {
            case EQUALS:
    
                if (value instanceof String && o instanceof String) {
                    String pattern = (String) value;
                    pattern = pattern.replace("*", "\\p{ASCII}*");
                    pattern = pattern.replace("?", "\\p{ASCII}");
    
                    Pattern p = Pattern.compile(pattern);
                    Matcher m = p.matcher((String) o);
                    checked = m.matches();
                } else {
                    checked = value.equals(o);
                }
                break;
            case LESS:
                checked = ((Comparable) o).compareTo(value) < 0;
                break;
            case LESS_OR_EQUAL:
                checked = ((Comparable) o).compareTo(value) <= 0;
                break;
            case GREATER:
                checked = ((Comparable) o).compareTo(value) > 0;
                break;
            case GREATER_OR_EQUAL:
                checked = ((Comparable) o).compareTo(value) >= 0;
                break;
            case NOT_EQUALS:
                checked = !value.equals(o);
                break;
            case ENDS_WITH:
                if (t.getType() != TYPE.STRING) {
                    throw new WikittyException(
                            "Can't search for contents that 'ends with' on attribute type different of String. "
                                    + "Attribute "
                                    + fqfieldName
                                    + " is "
                                    + t.getType().name());
                }
                checked = ((String) o).endsWith((String) value);
                break;
            case STARTS_WITH:
                if (t.getType() != TYPE.STRING) {
                    throw new WikittyException(
                            "Can't search for contents that 'starts with' on attribute type different of String. "
                                    + "Attribute "
                                    + fqfieldName
                                    + " is "
                                    + t.getType().name());
                }
    
                // FIXME mfortun-2011-04-20 rustine pour champs multivalué de
                // type string
                // et restriction startwith dessus. ça marche mais faudrait
                // quelque chose de plus
                // propre, et surtout généraliser pour toutes les restrictions
    
                if (o instanceof Collection<?>
                        && value instanceof Collection<?>) {
    
                    for (Object val : (Collection) value) {
    
                        String valu = (String) val;
    
                        for (Object oo : (Collection) o) {
                            String cotainedO = (String) oo;
                            if (cotainedO != null) {
                                checked = checked || cotainedO.startsWith(valu);
                            }
                        }
    
                    }
    
                } else {
    
                    checked = ((String) o).startsWith((String) value);
                }
                break;
            }
            return checked;
        } else if (restriction instanceof Null) {
            Null nullRes = (Null) restriction;
    
            String fqfieldName = nullRes.getFieldName();
    
            // check my wikitty got the right extension before doing anything.
            String[] extName = fqfieldName.split("\\.");
            if (!w.hasField(extName[0], extName[1])) {
                return false;
            }
            // get the value in the wikitty
            Object o = w.getFqField(fqfieldName);
    
            // No null on extensions, always return false
            if (fqfieldName.equals(Element.ELT_EXTENSION)) {
                return false;
            }
    
            // No null on ids, always return false
            if (fqfieldName.equals(Element.ELT_ID)) {
                return false;
            }
    
            boolean checked = false;
    
            switch (nullRes.getName()) {
            case IS_NULL:
                checked = o == null;
                break;
            case IS_NOT_NULL:
                checked = o != null;
                break;
            }
    
            return checked;
    
        } else if (restriction instanceof In) {
            In in = (In) restriction;
            String fqfieldName = in.getElement().getName();
            String testedValue = String.valueOf(w.getFqField(fqfieldName));
            for (String value : in.getValue()) {
                if (testedValue.equals(value)) {
                    return true;
                }
            }
    
            return false;
    
        } else if (restriction instanceof True) {
            return true;
        } else if (restriction instanceof False) {
            return false;
        } else if (restriction instanceof Contains) {
            Contains contains = (Contains) restriction;
    
            String fqfieldName = contains.getElement().getName();
            List<String> values = contains.getValue();
    
            String extension = WikittyUtil
                    .getExtensionNameFromFQFieldName(fqfieldName);
            String fieldName = WikittyUtil
                    .getFieldNameFromFQFieldName(fqfieldName);
    
            if (!w.hasField(extension, fieldName)) {
                return false;
            }
    
            // Get field as string and then split it to take into account not
            // multivalued fields.
            String testedValuesAsString = w.getFieldAsString(extension,
                    fieldName);
    
            if ('[' == testedValuesAsString.charAt(0)) {
                testedValuesAsString = testedValuesAsString.substring(1,
                        testedValuesAsString.length());
            }
    
            List<String> testedValues = Arrays.asList(testedValuesAsString
                    .split(","));
    
            for (Object value : values) {
                if (!testedValues.contains(String.valueOf(value))) {
                    return false;
                }
            }
    
            return true;
    
        } else if (restriction instanceof And) {
            And and = (And) restriction;
            for (Restriction sub : and.getRestrictions()) {
                if (!checkRestriction(sub, w)) {
                    return false;
                }
            }
            return true;
        } else if (restriction instanceof Or) {
            Or or = (Or) restriction;
            for (Restriction sub : or.getRestrictions()) {
                if (checkRestriction(sub, w)) {
                    return true;
                }
            }
            return false;
        } else if (restriction instanceof Keyword) {
            Keyword keyword = (Keyword) restriction;
    
            String value = keyword.getValue();
            
            //TODO mfortun-2011-07-06 hack to ensure that * is intepreted as
            // the real meaning aka any
            if (value.equals("*")){
                return true;
            }
    
            for (String fieldName : w.getAllFieldNames()) {
                String testedValue = String.valueOf(w.getFqField(fieldName));
                if (testedValue.contains(value)) {
                    return true;
                }
            }
            return false;
        } else if (restriction instanceof Not) {
            Not or = (Not) restriction;
            Restriction sub = or.getRestriction();
            return !checkRestriction(sub, w);
        } else if (restriction instanceof AssociatedRestriction) {
    
            AssociatedRestriction ass = (AssociatedRestriction) restriction;
    
            String fqfieldName = ass.getElement().getName();
    
            // check my wikitty got the right extension before doing anything.
            String[] extName = fqfieldName.split("\\.");
            if (!w.hasField(extName[0], extName[1])) {
                return false;
            }
            // get the value in the wikitty, it is a wikitty's id
            Object o = w.getFqField(fqfieldName);
    
            // Get sub-restriction
            Restriction sub = ass.getRestriction();
    
            Criteria associatedSearch = new Criteria();
            associatedSearch.setRestriction(sub);
    
            // find everything that validate the sub-restriction
    
            List<Criteria> dummyList = new ArrayList<Criteria>();
            dummyList.add(associatedSearch);
            // same as proxy for "fix" unique param to list param
            PagedResult<String> associatedResult = findAllByCriteria("",
                    dummyList).get(0);
    
            List<String> associatedList = associatedResult.getAll();
    
            // Check that my field is contained in the sub-restriction results.
            return associatedList.contains(String.valueOf(o));
        } else if (restriction instanceof Between) {
    
            Between op = (Between) restriction;
    
            Object max = op.getMax();
            Object min = op.getMin();
    
            // No between on extensions, always return false
            if (op.getElement().getName().equals(Element.ELT_EXTENSION)) {
                return false;
            }
    
            // No between on ids, always return false
            if (op.getElement().getName().equals(Element.ELT_ID)) {
                return false;
            }
    
            String fqfieldName = op.getElement().getName();
    
            // si les wikitty n'ont meme pas l'extension concerné
            // Le check restriction, ne doit pas tester les champs
            // si les wikitty n'ont meme pas l'extension concerné
            String[] extName = fqfieldName.split("\\.");
            if (!w.hasField(extName[0], extName[1])) {
                return false;
            }
    
            // recupere la valeur dans le wikitty
            Object o = w.getFqField(fqfieldName);
    
            // recupere le type de la valeur
            FieldType t = w.getFieldType(fqfieldName);
    
            if (!(min instanceof Collection) && t.isCollection()) {
                // on doit encapsuler dans une collection, car la creation
                // de la requete ajoute autant de v == o && ... que de valeurs
                // dans la collection (champs multi-value solr). Mais
                // dans le inmemory on doit retrouve des collections et non pas
                // des objets seuls :(
                min = Collections.singleton(min);
            }
            min = t.getValidValue(min);
    
            if (!(max instanceof Collection) && t.isCollection()) {
                // on doit encapsuler dans une collection, car la creation
                // de la requete ajoute autant de v == o && ... que de valeurs
                // dans la collection (champs multi-value solr). Mais
                // dans le inmemory on doit retrouve des collections et non pas
                // des objets seuls :(
                max = Collections.singleton(max);
            }
            max = t.getValidValue(max);
    
            return ((Comparable) o).compareTo(min) >= 0
                    && ((Comparable) o).compareTo(max) <= 0;
        } else {
            throw new UnsupportedOperationException(restriction.getName()
                    + " Search Not yet implemented");
        }
    }

   
    @Override
    public List<PagedResult<String>> findAllByCriteria(String securityToken,
            List<Criteria> criteria) {
        
     
        List<PagedResult<String>> result = new ArrayList<PagedResult<String>>();

        Map<String, Wikitty> wikitties = getAllWikitties();
        
        // for each criteria
        for (Criteria cr : criteria) {
            // prepare restriction on result
            int firstIndex = cr.getFirstIndex();
            int endIndex = cr.getEndIndex();
            List<String> ids = new LinkedList<String>();
            int currentIndex = 0;
            Restriction restriction = cr.getRestriction();
            // for each wikitty check if it match the resttriction
            for (Entry<String, Wikitty> entry : wikitties.entrySet()) {
                String id = entry.getKey();
                Wikitty w = entry.getValue();
                // if macth


                if (checkRestriction(restriction, w)) {

                    // increment result number
                    currentIndex++;
                    if (currentIndex > firstIndex) {
                        ids.add(id);
                    }
                    // if the number of wikitty found is match
                    // stop the search for other wikitty
                    if (endIndex >= 0 && currentIndex >= endIndex) {
                        break;
                    }
                }
            }
            result.add(new PagedResult<String>(
                    cr.getName(),
                    firstIndex, ids.size(),
                    restriction.toString(), null, ids));
            
        }
        return result;
    }
    
    
    @Override
    public List<String> findByCriteria(String securityToken,
            List<Criteria> criteria) {
        List<String> result = new ArrayList<String>();

        Map<String, Wikitty> wikitties = getAllWikitties();
        // for each criteria
        for (Criteria cr : criteria) {
            // prepare restriction on result
            int firstIndex = cr.getFirstIndex();
            int endIndex = cr.getEndIndex();
            List<String> ids = new LinkedList<String>();
            int currentIndex = 0;
            Restriction restriction = cr.getRestriction();
            // for each wikitty check if it match the resttriction
            for (Entry<String, Wikitty> entry : wikitties.entrySet()) {
                String id = entry.getKey();
                Wikitty w = entry.getValue();
                // if macth

                log.debug("Check restriction for wikitty: " + w
                        + " Restriction:" + restriction);

                if (checkRestriction(restriction, w)) {
                    // increment result number
                    currentIndex++;
                    if (currentIndex > firstIndex) {
                        ids.add(id);
                    }
                    // if the number of wikitty found is match
                    // stop the search for other wikitty
                    if (endIndex >= 0 && currentIndex >= endIndex) {
                        break;
                    }
                }
            }
            result.addAll(ids);
        }
        return result;
    }
    
    
    @Override
    public WikittyExtension restoreExtensionLastVersion(String securityToken,
            String name) {

        if (name.equals(WikittyPubText.EXT_WIKITTYPUBTEXT)) {
            return WikittyPubTextImpl.extensionWikittyPubText;
        }

        if (name.equals(WikittyPubData.EXT_WIKITTYPUBDATA)) {
            return WikittyPubDataImpl.extensionWikittyPubData;
        }

        if (name.equals(WikittyLabel.EXT_WIKITTYLABEL)) {
            return WikittyLabelImpl.extensionWikittyLabel;
        }

        return null;
    }
    
    @Override
    public List<WikittyExtension> restoreExtensionAndDependenciesLastVesion(String securityToken, Collection<String> extensionNames) {
        List<WikittyExtension> result = new ArrayList<WikittyExtension>();

        for (String extName : extensionNames) {
            WikittyExtension ext = restoreExtensionLastVersion(
                    securityToken, extName);
            if (ext != null) {
                // on recherche les dependances de cette extension ...
                List<String> requires = ext.getRequires();
                if (CollectionUtils.isNotEmpty(requires)) {
                    List<WikittyExtension> dependencies =
                            restoreExtensionAndDependenciesLastVesion(
                            securityToken, requires);
                    // ... et on les ajoute avant dans le resultat
                    result .addAll(dependencies);
                }
                result.add(ext);
            }
        }
        return result;
    }

   
}