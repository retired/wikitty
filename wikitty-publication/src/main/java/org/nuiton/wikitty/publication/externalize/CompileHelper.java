/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.externalize;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

/**
 * Class reuse from the isis fish project.
 * 
 * 
 * Compile helper used to compile Java code.
 * 
 * JDK must be installed to use compilation. (JRE won't work).
 * 
 * Created: 12 janv. 2006 15:29:53
 * 
 * @author poussin
 * @version $Revision$
 * 
 *          Last update: $Date$
 *          by : $Author$
 */
public class CompileHelper {

    /** Logger for this class. */
    private static final Log log = LogFactory.getLog(CompileHelper.class);

    /**
     * Recherche tous les fichiers qui un source plus recent que la version
     * compilé.
     * 
     * @param srcDir
     * @param destDir
     * @return File list
     */
    public static List<File> searchSrcToCompile(File srcDir, File destDir) {
        List<File> result = new ArrayList<File>();
        for (File src : srcDir.listFiles()) {
            File dest = new File(FileUtil.basename(src, ".java"), ".class");
            if (src.getName().endsWith(".java") && FileUtil.isNewer(src, dest)) {
                result.add(src);
            }
        }
        return result;
    }

    /**
     * Methode permettant de compiler un fichier Java.
     * 
     * @param rootSrc
     *            le répertoire ou se trouve les sources
     * @param src
     *            Le fichier source a compiler, il doit etre dans un sous
     *            répertoire de rootSrc en fonction du package
     * @param dest
     *            le repertoire destination de la compilation
     * @param out
     *            l'objet sur lequel on ecrit la sortie (erreur) de la
     *            compilation
     * @return un nombre different de 0 s'il y a une erreur <li>-1000 si
     *         l'exception vient de la recherche du compilateur par
     *         introspection <li>-10000 si une autre exception <li>sinon les
     *         valeurs retourné par le compilateur java
     */
    public static int compile(List<File> addToClassPath, File rootSrc, File src, File dest, PrintWriter out) {
        int result = compile(addToClassPath, rootSrc, Collections.singletonList(src), dest, out);
        return result;
    }

    public static int compile( File rootSrc, File src, File dest, PrintWriter out) {
        int result = compile(null, rootSrc, Collections.singletonList(src), dest, out);
        return result;
    }

    
    /**
     * Methode permettant de compiler un ensemble de fichiers Java.
     * 
     * @param rootSrc
     *            le répertoire ou se trouve les sources
     * @param src
     *            Le fichier source a compiler, il doit etre dans un sous
     *            répertoire de rootSrc en fonction du package
     * @param dest
     *            le repertoire destination de la compilation
     * @param out
     *            l'objet sur lequel on ecrit la sortie (erreur) de la
     *            compilation
     * @return un nombre different de 0 s'il y a une erreur <li>-1000 si
     *         l'exception vient de la recherche du compilateur par
     *         introspection <li>-10000 si une autre exception <li>sinon les
     *         valeurs retourné par le compilateur java
     */
    public static int compile(List<File> addToClassPath, File rootSrc, Collection<File> src, File dest,
            PrintWriter out) {
        int result = -10000;
        try {
            List<File> classpath = new ArrayList<File>();
            if (addToClassPath!=null){
            classpath.addAll(addToClassPath);
            }
            classpath.add(rootSrc.getAbsoluteFile());

            result = compile(classpath, src, dest, out);
        } catch (Exception eee) {
            if (log.isWarnEnabled()) {
                log.warn("Compilation failed", eee);
            }
        }
        return result;
    }

    /**
     * Compile un fichier java.
     * 
     * @param classpath
     * @param src
     *            les fichiers java source
     * @param dest
     *            le repertoire destination
     * @param out
     * @return
     */
    protected static int compile(List<File> classpath, Collection<File> src,
            File dest, PrintWriter out) {
        dest.mkdirs();

        int result = -1000;
        try {
            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
            // Use system compiler
            // JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
            StandardJavaFileManager fileManager = compiler
                    .getStandardFileManager(null, null, null);
            Iterable<? extends JavaFileObject> compilationUnits = fileManager
                    .getJavaFileObjectsFromFiles(src);

            // Options de compilations
            String classpathAsString = getClassPathAsString(classpath);
            List<String> args = new ArrayList<String>();
            args.add("-g");
            // Show a description of each use or override of a deprecated member
            // or class.
            args.add("-deprecation");
            args.add("-classpath");
            args.add(classpathAsString);
            args.add("-d");
            args.add(dest.getAbsolutePath());

            // Compilation
            boolean b = compiler.getTask(out, fileManager, null, args, null,
                    compilationUnits).call();
            // on retourne 0 si tout s'est bien déroulé et -1 sinon
            result = b ? 0 : -1;

            fileManager.close();
        } catch (Exception eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't get compiler", eee);
            }
        }
        return result;
    }

    /**
     * Return full classpath (for compilation or javadoc) as string. Separated
     * by {@link File#pathSeparator}.
     * 
     * Add :
     * <ul>
     * <li>System.getProperty("java.class.path")
     * <li>All first jar dependency (META-INF/MANIFEST.MF)
     * </ul>
     * 
     * @param classpath
     *            initial classpath
     * @return classpath as string
     * @throws Exception
     */
    public static String getClassPathAsString(List<File> classpath)
            throws Exception {
        String result = StringUtils.join(classpath.iterator(),
                File.pathSeparator);

        // chatellier : since 20090512 java.class.path in not added to
        // classpath.
        // result in duplicated entry or non normalised entry
        // en compilation fail some times
        // + File.pathSeparator + System.getProperty("java.class.path");

        // Ajout des jars
        for (Enumeration<?> e = CompileHelper.class.getClassLoader()
                .getResources("META-INF/MANIFEST.MF"); e.hasMoreElements();) {
            URL url = (URL) e.nextElement();
            if (log.isDebugEnabled()) {
                log.debug("Found manifest : " + url);
            }
            if (url != null && url.getFile().startsWith("file:/")) {
                String jarName = url.getPath().substring(5,
                        url.getPath().indexOf("!"));
                if (!result.contains(jarName)) {
                    result += File.pathSeparator + jarName;
                }
            }
        }

        // chatellier : mais sous eclipse par exemple, on a besoin du classpath
        // on test que le class path n'est pas un jar et qu'il n'apparait
        // pas deja :
        String systemClassPath = System.getProperty("java.class.path");
        String[] systemClassPathes = systemClassPath.split(File.pathSeparator);
        for (String path : systemClassPathes) {
            String absolutePath = new File(path).getCanonicalPath();
            if (!result.contains(absolutePath)) {
                result += File.pathSeparator + absolutePath;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("CLASSPATH : " + result);
        }

        return result;
    }

}
