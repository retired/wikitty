/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.ArgumentsParserException;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyServiceFactory;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;
import org.nuiton.wikitty.search.TreeNodeResult;
import org.nuiton.wikitty.services.WikittyEvent;
import org.nuiton.wikitty.services.WikittyListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;

/**
 * class meant to handle multicontext for wikitty publication. If want to use
 * this, you must declare in your properties file the WIKITTY_FALLBACK_FILE_KEY
 * property that declare the name of the property file needed to initialize
 * fallbackService
 * 
 * @author mfortun
 * 
 */
public class WikittyPublicationFallbackService implements WikittyService {

    /**
     * property that define the name of the property file needed to initialize
     * fallback service
     * */
    static public String WIKITTY_FALLBACK_FILE_KEY = "wikitty.fallback.file";
    
    /**
     * property flag to know if copy property from the main appconfig and
     * overide them with the content of the fallback property file or just tak
     * it as standalone
     */
    static public String WIKITTY_FALLBACK_OVERRIDE="wikitty.fallback.override";

    static private Log log = LogFactory.getLog(WikittyPublicationFallbackService.class);
    
    protected WikittyService mainService;
    protected WikittyService fallbackService;

    public WikittyPublicationFallbackService(WikittyService serv,
            ApplicationConfig config) throws ArgumentsParserException {
        mainService = serv;
        if(config != null){
            boolean isOverriding = config.getOptionAsBoolean(WIKITTY_FALLBACK_OVERRIDE);
            String fileName = config.getOption(WIKITTY_FALLBACK_FILE_KEY);
            
            if (fileName!=null){
                ApplicationConfig confFallBack;
                if (isOverriding){
                     confFallBack= new ApplicationConfig(null, null,config.getFlatOptions(), fileName);
                    
                }else{
                     confFallBack= new ApplicationConfig(fileName);
                }
                confFallBack.parse(null);
                fallbackService = WikittyServiceFactory.buildWikittyService(confFallBack);
            }
        }
    }

    public boolean isFallBack() {
        return fallbackService != null;
    }

    /*
     * TODO mfortun-2011-05-20 see if allow a special mode to enable login on
     * both wikitty service, see if fallback service has to be construct with a
     * special stack of component in the application config under specific
     * properties name, or if the fallback service is already construct and pass
     * threw this constructor
     */

    public void addWikittyServiceListener(WikittyListener listener,
            ServiceListenerType type) {
        mainService.addWikittyServiceListener(listener, type);
    }

    public void removeWikittyServiceListener(WikittyListener listener,
            ServiceListenerType type) {
        mainService.removeWikittyServiceListener(listener, type);
    }

    public String login(String login, String password) {
        return mainService.login(login, password);
    }

    public void logout(String securityToken) {
        mainService.logout(securityToken);
    }

    public WikittyEvent clear(String securityToken) {
        return mainService.clear(securityToken);
    }

    public boolean canWrite(String securityToken, Wikitty wikitty) {
        return mainService.canWrite(securityToken, wikitty);
    }

    public boolean canDelete(String securityToken, String wikittyId) {
        return mainService.canDelete(securityToken, wikittyId);
    }

    public boolean canRead(String securityToken, String wikittyId) {
        return mainService.canRead(securityToken, wikittyId);
    }

    public boolean exists(String securityToken, String wikittyId) {
        boolean result = mainService.exists(securityToken, wikittyId);

        if (isFallBack()) {
            result = result || fallbackService.exists(securityToken, wikittyId);
        }

        return result;
    }

    public boolean isDeleted(String securityToken, String wikittyId) {
        boolean result = mainService.isDeleted(securityToken, wikittyId);

        if (isFallBack()) {
            result = result
                    || fallbackService.isDeleted(securityToken, wikittyId);
        }

        return result;
    }

    public WikittyEvent replay(String securityToken, List<WikittyEvent> events,
            boolean force) {

        // TODO mfortun-2011-05-25 do something here with fallback service

        return mainService.replay(securityToken, events, force);
    }

    public WikittyEvent store(String securityToken,
            Collection<Wikitty> wikitties, boolean force) {
        return mainService.store(securityToken, wikitties, force);
    }

    public List<String> getAllExtensionIds(String securityToken) {

        List<String> result = new LinkedList<String>();
        result.addAll(mainService.getAllExtensionIds(securityToken));

        // TODO mfortun-2011-05-25 do again this with set ?
        if (isFallBack()) {
            List<String> temp = fallbackService
                    .getAllExtensionIds(securityToken);
            // assert that not have duplicate entry
            result.removeAll(temp);
            result.addAll(temp);
        }

        return result;
    }

    public List<String> getAllExtensionsRequires(String securityToken,
            String extensionName) {

        List<String> result = new LinkedList<String>();
        result.addAll(mainService.getAllExtensionsRequires(securityToken,
                extensionName));

        // TODO mfortun-2011-05-25 do again this with set ?
        if (isFallBack()) {
            List<String> temp = fallbackService.getAllExtensionsRequires(
                    securityToken, extensionName);
            // assert that not have duplicate entry
            result.removeAll(temp);
            result.addAll(temp);
        }

        return result;
    }

    public WikittyEvent storeExtension(String securityToken,
            Collection<WikittyExtension> exts) {
        return mainService.storeExtension(securityToken, exts);
    }

    public WikittyEvent deleteExtension(String securityToken,
            Collection<String> extNames) {
        return mainService.deleteExtension(securityToken, extNames);
    }

    public WikittyExtension restoreExtension(String securityToken,
            String extensionId) {

        WikittyExtension result = mainService.restoreExtension(securityToken,
                extensionId);
        if (isFallBack() && result == null) {
            result = fallbackService.restoreExtension(securityToken,
                    extensionId);
        }
        return result;
    }

    public WikittyExtension restoreExtensionLastVersion(String securityToken,
            String name) {

        
        WikittyExtension result = mainService.restoreExtensionLastVersion(
                securityToken, name);
        
        
        if (isFallBack() && result == null) {
            result = fallbackService.restoreExtensionLastVersion(securityToken,
                    name);
        }
        return result;
    }

    @Override
    public List<WikittyExtension> restoreExtensionAndDependenciesLastVesion(String securityToken, Collection<String> extensionNames) {
        List<WikittyExtension> result = new ArrayList<WikittyExtension>();

        for (String extName : extensionNames) {
            WikittyExtension ext = restoreExtensionLastVersion(
                    securityToken, extName);
            if (ext != null) {
                // on recherche les dependances de cette extension ...
                List<String> requires = ext.getRequires();
                if (CollectionUtils.isNotEmpty(requires)) {
                    List<WikittyExtension> dependencies =
                            restoreExtensionAndDependenciesLastVesion(
                            securityToken, requires);
                    // ... et on les ajoute avant dans le resultat
                    result .addAll(dependencies);
                }
                result.add(ext);
            }
        }
        return result;
    }

    public List<Wikitty> restore(String securityToken, List<String> id) {

        List<Wikitty> result = new LinkedList<Wikitty>();

        List<Wikitty> tempListResult = mainService.restore(securityToken, id);
        for (Wikitty wikitty : tempListResult) {
            if( wikitty !=null) {
                result.add(wikitty);
            }
        }
        // can't trust result's size because result can contain null
        if (isFallBack()) {
            // prepare a list with wikitty id that have not been retrieve by the
            // first proxy
            List<String> unusedId = new LinkedList<String>();
            unusedId.addAll(id);

            for (Wikitty wikitty : result) {
                if (wikitty != null) {
                    unusedId.remove(wikitty.getId());
                }
            }

            result.addAll(fallbackService.restore(securityToken, unusedId));
        }

        return result;
    }

    public WikittyEvent delete(String securityToken, Collection<String> ids) {
        return mainService.delete(securityToken, ids);
    }

    public List<PagedResult<String>> findAllByCriteria(String securityToken,
            List<Criteria> criteria) {

        List<PagedResult<String>> result = mainService.findAllByCriteria(
                securityToken, criteria);

        if (isFallBack()) {


            
            for (int i = 0; i < result.size(); i++) {

                /*
                 * get the curent criteria and corresponding result to check if
                 * the expected number of result is match
                 */
                Criteria currentCrit = criteria.get(i);
                PagedResult<String> currentResult = result.get(i);

                int resultSizeExpected = currentCrit.getEndIndex()
                        - currentCrit.getFirstIndex();
                log.debug("Size expected"+ resultSizeExpected);
                
                if ( resultSizeExpected==-1 || currentResult.size() < resultSizeExpected ) {
                    /*
                     * if result empty, just put the result of the fallback's
                     * requestresult
                     */

                    /*
                     * rebuild the criteria to search properly on the second
                     * service we search for wikitty not in the same position,
                     * but in the beginning
                     */
                    Criteria critFallback = Search.query(currentCrit)
                            .criteria();
                    critFallback.setFirstIndex(0);
                    critFallback.setEndIndex(currentCrit.getEndIndex()
                            - currentCrit.getFirstIndex());

                    if (result.size() == 0) {

                        List<Criteria> tempCritFallBack = new ArrayList<Criteria>();
                        tempCritFallBack.add(critFallback);

                        PagedResult<String> tempPagedResult = fallbackService
                                .findAllByCriteria(securityToken,
                                        tempCritFallBack).get(0);

                        // rebuild the result to match the expected criteria
                        tempPagedResult = new PagedResult<String>(
                                currentCrit.getName(),
                                currentCrit.getFirstIndex(),
                                tempPagedResult.getNumFound(),
                                currentCrit.toString(),
                                tempPagedResult.getFacets(),
                                tempPagedResult.getAll());

                        result.set(i, tempPagedResult);
                    } else {

                        /*
                         * we will search to complete the result with id from
                         * the fallback service so we reduce the number of
                         * element requested on the fallback service and we add
                         * a restriction on wikitty id to exclude id already in
                         * the first part of the result
                         * 
                         * it not assume that wikitty id found does not exist on
                         * the first part of a criteria, for example if first
                         * index where 12, nothing assure that we found wikitty
                         * in the fall back that aren't in the 11 first wikitty
                         * of the main service
                         */
                        // remove null from the first result
                        List<String> fromMainWithoutNull= new LinkedList<String>();
                        for(String st :currentResult.getAll()){
                            if (st!=null){
                                fromMainWithoutNull.add(st);
                            }
                        }
                        
                        Criteria excluding = idNotInCriteriaConstructor(
                                critFallback,fromMainWithoutNull );

                        excluding.setEndIndex(resultSizeExpected
                                - result.size());

                        List<Criteria> tempExcludingCriteriaList = new ArrayList<Criteria>();
                        tempExcludingCriteriaList.add(excluding);

                        PagedResult<String> resultFallback = fallbackService
                                .findAllByCriteria(securityToken,
                                        tempExcludingCriteriaList).get(0);

                        List<String> allresult = new LinkedList<String>();
                        allresult.addAll(fromMainWithoutNull);
                        allresult.addAll(resultFallback.getAll());
                        
                        int number = fromMainWithoutNull.size()
                                + resultFallback.getNumFound();

                        // rebuilt the result
                        PagedResult<String> tempPagedResult = new PagedResult<String>(
                                currentCrit.getName(),
                                currentCrit.getFirstIndex(), number,
                                currentCrit.toString(),
                                currentResult.getFacets(), allresult);
                        // re set the result in the list
                        result.set(i, tempPagedResult);
                    }
                }

            }
        }

        return result;

    }

    public List<String> findByCriteria(String securityToken,
            List<Criteria> criteria) {
    
        
        List<String> result = new LinkedList<String>();
        List<String> tempFromMain = mainService.findByCriteria(securityToken,
                criteria);
        // remove null element from result
        for (String st : tempFromMain) {
            if (st != null) {
                result.add(st);
            }
        }

        if (isFallBack()) {

            if (result.size() == 0) {
                result.addAll(fallbackService.findByCriteria(securityToken,
                        criteria));
            } else {
                // merge list
                List<String> resultFallback = new LinkedList<String>();

                resultFallback.addAll(fallbackService.findByCriteria(securityToken, criteria));
                // this allow to element from main to prevail

                resultFallback.removeAll(result);
                result.addAll(resultFallback);
            }
        }

        if (result.size()==0){
            result.add(null);
        }
        
        return result;
    }

    public WikittyEvent deleteTree(String securityToken, String treeNodeId) {
        return mainService.deleteTree(securityToken, treeNodeId);
    }

    public TreeNodeResult<String> findTreeNode(String securityToken,
            String wikittyId, int depth, boolean count, Criteria filter) {
        TreeNodeResult<String> result = mainService.findTreeNode(securityToken,
                wikittyId, depth, count, filter);

        if (isFallBack() && result == null) {
            result = fallbackService.findTreeNode(securityToken, wikittyId,
                    depth, count, filter);
        }

        return result;
    }

    public Wikitty restoreVersion(String securityToken, String wikittyId,
            String version) {
        Wikitty result = mainService.restoreVersion(securityToken, wikittyId,
                version);

        if (isFallBack() && result == null) {
            result = fallbackService.restoreVersion(securityToken, wikittyId,
                    version);
        }
        return result;
    }

    public void syncSearchEngine(String securityToken) {
        mainService.syncSearchEngine(securityToken);
    }

 
    /**
     * Create a criteria to exclude a list of if from a criteria
     * 
     * @param origin
     *            the criteria from whom exclude id
     * @param toExclude
     *            list of id to exlude
     * @return the criteria excluding ids
     */
    protected Criteria idNotInCriteriaConstructor(Criteria origin,
            List<String> toExclude) {
        Criteria result;

        Search search = Search.query(origin);

        for (String id : toExclude) {
            search.idneq(id);
        }

        result = search.criteria();

        return result;
    }

    public WikittyService getMainService() {
        return mainService;
    }

    public void setMainService(WikittyService mainService) {
        this.mainService = mainService;
    }

    public WikittyService getFallbackService() {
        return fallbackService;
    }

    public void setFallbackService(WikittyService fallbackService) {
        this.fallbackService = fallbackService;
    }

    
    
}
