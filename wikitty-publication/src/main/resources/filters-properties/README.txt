Those filters options are use to transform WikittyPubText content with a
nuiton processor and filters. The goal of this is to include html (and after
other langage design for ui inside web browser) with scripting mechanism. 
Instead of write the script and include inside result value html tag, do the 
opposite, that what allow those filters.
In this case use script langage inside tag like in a jsp: <% %> and <%= %>


Filters options doc:

OpeningTemplate: the string openning the template
WriteString: instruction used to write string
StringDelim: string delimiter example : "
ConcatChar: string used to concat char example: +
EndingCar: string that end instruction
ClosingWriterChar: string after write string and before String delim
OpeningWriterChar: string after string delim, before ending car
ClosingTemplate: string that close the template
Key: mimetype special usualy originMime.targetMime for example html.javascript
this is linked to mimetype properties. Pattern [xxx.yyy]

 
Example :


<html>
<%var name="bob"%>


<h1>Hello World <%=name%>!</h1>
</html>
with template:

wpContext.setContentType("text/[xxx]")[EndingCar]

[OpeningTemplate]<html>[StringDelim][ClosingWriterChar][EndingCar]	
[WriteString][OpeningWriterChar][StringDelim]bob[StringDelim][ClosingWriterChar][EndingCar]
[WriteString][OpeningWriterChar][StringDelim]<h1>Hello World [StringDelim][ConcatChar]name[ConcatChar][StringDelim]!</h1>[StringDelim][ClosingWriterChar][EndingCar]
[WriteString][OpeningWriterChar][StringDelim]</html>[StringDelim][ClosingWriterChar][EndingCar][ClosingTemplate]
	
This filters will be enable when try to evaluate wikittyPubText with mime type:
text/[xxx.yyy], the mime type of the wikittyPubText after will be: text/[yyy]


If using the default filters: 


wpContext.setContentType("text/html; charset=UTF-8");

var wp_result="<html>";
var name= bob;
wp_result+="<h1>Hello World "+name+"!</h1>";

wp_result+="</html>";

This filters will be enable when try to evaluate wikittyPubText with mime type:
text/html.javascript, the mime type of the wikittyPubText after will be: application/javascript

Template:

OpeningTemplate=
WriteString=
StringDelim=
ConcatChar=
EndingCar=
ClosingWriterChar=
OpeningWriterChar=
ClosingTemplate=
Key=

