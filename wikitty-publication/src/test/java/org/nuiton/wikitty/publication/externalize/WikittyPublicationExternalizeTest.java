/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.externalize;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import junit.framework.Assert;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.ArgumentsParserException;
import org.nuiton.util.FileUtil;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyServiceFactory;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyLabelHelper;
import org.nuiton.wikitty.entities.WikittyLabelImpl;
import org.nuiton.wikitty.publication.WikittyFileUtil;
import org.nuiton.wikitty.publication.entities.WikittyPubDataHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubDataImpl;
import org.nuiton.wikitty.publication.entities.WikittyPubTextHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubTextImpl;
import org.nuiton.wikitty.publication.synchro.WikittyPublicationFileSystem;
import org.nuiton.wikitty.publication.synchro.WikittyPublicationSynchronize;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.Search;

public class WikittyPublicationExternalizeTest {
    
    protected File tempHome;
    protected List<Wikitty> listWikittyPubData;
    protected List<Wikitty> listWikittyPubText;
    protected List<String> labels;
    protected ApplicationConfig config;
    protected WikittyProxy proxy;
    protected Criteria allCrit;
    protected File starts;

    
    
    @Before
    public void init() throws IOException {

        if (!FileUtils.getTempDirectory().exists()) {
            FileUtils.getTempDirectory().mkdir();
        }

        tempHome = new File(FileUtils.getTempDirectory().getCanonicalFile()
                + File.separator + "WikittyPublicationFileSystemTest");

        if (tempHome.exists()) {
            FileUtil.deleteRecursively(tempHome);

        }
        tempHome.mkdir();

        // prepare the wikitties list
        listWikittyPubData = new LinkedList<Wikitty>();
        listWikittyPubText = new LinkedList<Wikitty>();

        labels = new ArrayList<String>(6);
        labels.add(0, "sub");
        labels.add(1, labels.get(0) + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR
                + "sub11");
        labels.add(2, labels.get(1) + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR
                + "sub12");
        labels.add(3, labels.get(0) + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR
                + "sub3");
        labels.add(4, labels.get(0) + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR
                + "sub21");
        labels.add(5, labels.get(4) + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR
                + "sub21");

        for (int i = 0; i < 6; i++) {

            Wikitty pubText = new WikittyImpl();
            Wikitty pubData = new WikittyImpl();

            pubData.addExtension(WikittyPubDataImpl.extensionWikittyPubData);
            pubText.addExtension(WikittyPubTextImpl.extensionWikittyPubText);

            WikittyPubTextHelper.setContent(pubText, "content_" + i);

            WikittyPubTextHelper.setMimeType(pubText, "text/javascript");
            WikittyPubTextHelper.setName(pubText, "pubtextnum_" + i);

            WikittyPubDataHelper.setContent(pubData,
                    ("content data" + i).getBytes());
           
            WikittyPubDataHelper.setMimeType(pubData, "image/jpg");
            WikittyPubDataHelper.setName(pubData, "pubdatanum_" + i);

            pubData.addExtension(WikittyLabelImpl.extensions);
            pubText.addExtension(WikittyLabelImpl.extensions);

            WikittyLabelHelper.addLabels(pubData, labels.get(i));
            WikittyLabelHelper.addLabels(pubText, labels.get(i));

            listWikittyPubData.add(pubData);
            listWikittyPubText.add(pubText);
        }

        // then initialise application config for wikitty proxy on service FS
        config = new ApplicationConfig();
        config.setOption(WikittyPublicationSynchronize.IS_RECURSION_OPTION,
                "true");
        config.setOption(
                WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS.getKey(),
                WikittyPublicationFileSystem.class.getName());

        starts = new File(tempHome.getAbsolutePath() + File.separator + "sub");
        if (starts.exists()) {
            FileUtil.deleteRecursively(starts);
        }
        starts.mkdir();

        String url = "file:///" + tempHome.getAbsolutePath() + "#sub";

        config.setOption(WikittyConfigOption.WIKITTY_SERVER_URL.getKey(), url);

        proxy = new WikittyProxy(
                WikittyServiceFactory.buildWikittyService(config));

        allCrit = Search.query().keyword("*").criteria();

    }

    @After
    public void delete() {
        FileUtil.deleteRecursively(tempHome);
    }

    /**
     * check save wikitty
     */
    @Test
    public void saveWikittyOnFS() {

        Assert.assertEquals(0, proxy.findAllIdByCriteria(allCrit).getNumFound());

        proxy.storeWikitty(listWikittyPubData);
        proxy.storeWikitty(listWikittyPubText);

        int result = listWikittyPubData.size() + listWikittyPubText.size();

        Assert.assertEquals(result, proxy.findAllIdByCriteria(allCrit)
                .getNumFound());

    }
    
    
    @Test
    public void testExternalize(){
        // Jar is created 
        
        
    }
    
    @Test
    public void testFileInJar(){
        // test that there are normal file and class and java.
        
        
    }
    
    
   

}
