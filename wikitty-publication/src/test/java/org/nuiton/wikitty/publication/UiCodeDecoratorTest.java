/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;

public class UiCodeDecoratorTest {

    
    /* 
     *Test pour différent code.
     *voir si ça gère quand ya des <%=%>
     *ou pas
     *
     */
    
    /*
     * créer du code html avec rien comme code et voir que le code est bien
     * décoré quand même
     * 
     * 2) mettre uniquement des <%= %> dedans
     * 3) mettre uniquement <% %> dedans
     * 
     * 4) mixer <% et <%=
     * 
     * 5) balises + html
     */
    
}
