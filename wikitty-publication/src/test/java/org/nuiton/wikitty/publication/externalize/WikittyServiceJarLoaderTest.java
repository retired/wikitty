/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.externalize;

public class WikittyServiceJarLoaderTest {

    
    /*
     * Liste des tests à faire: 
     * 
     * 1) S'assurer du nombre de wikitty sauvegardé
     * 
     * 2) s'assurer que les wikittys sont toujours les mêmes
     * 
     * 3) s'assurer que les stores ne fonctionnent pas
     * 
     * 4) la liste des extension disponible
     * 
     * 5) faire des recherches celon critéria différents
     * 
     */
    
}
