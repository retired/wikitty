/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.synchro;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyServiceFactory;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.entities.WikittyLabelHelper;
import org.nuiton.wikitty.entities.WikittyLabelImpl;
import org.nuiton.wikitty.publication.WikittyFileUtil;
import org.nuiton.wikitty.publication.entities.WikittyPubData;
import org.nuiton.wikitty.publication.entities.WikittyPubDataHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubDataImpl;
import org.nuiton.wikitty.publication.entities.WikittyPubText;
import org.nuiton.wikitty.publication.entities.WikittyPubTextHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubTextImpl;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.Search;

public class WikittyPublicationFileSystemTest {

    protected File tempHome;
    protected List<Wikitty> listWikittyPubData;
    protected List<Wikitty> listWikittyPubText;
    protected List<String> labels;
    protected ApplicationConfig config;
    protected WikittyProxy proxy;
    protected Criteria allCrit;
    protected File starts;

    static protected String PUB_TEXT_MIME = "text/javascript";
    
    @Before
    public void init() throws IOException {

        if (!FileUtils.getTempDirectory().exists()) {
            FileUtils.getTempDirectory().mkdir();
        }

        tempHome = new File(FileUtils.getTempDirectory().getCanonicalFile()
                + File.separator + "WikittyPublicationFileSystemTest");

        if (tempHome.exists()) {
            FileUtil.deleteRecursively(tempHome);

        }
        tempHome.mkdir();

        // prepare the wikitties list
        listWikittyPubData = new LinkedList<Wikitty>();
        listWikittyPubText = new LinkedList<Wikitty>();

        labels = new ArrayList<String>(6);
        labels.add(0, "sub");
        labels.add(1, labels.get(0) + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR
                + "sub11");
        labels.add(2, labels.get(1) + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR
                + "sub12");
        labels.add(3, labels.get(0) + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR
                + "sub3");
        labels.add(4, labels.get(0) + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR
                + "sub21");
        labels.add(5, labels.get(4) + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR
                + "sub21");

        for (int i = 0; i < 6; i++) {

            Wikitty pubText = new WikittyImpl();
            Wikitty pubData = new WikittyImpl();

            pubData.addExtension(WikittyPubDataImpl.extensionWikittyPubData);
            pubText.addExtension(WikittyPubTextImpl.extensionWikittyPubText);

            WikittyPubTextHelper.setContent(pubText, "content_" + i);
            WikittyPubTextHelper.setMimeType(pubText, PUB_TEXT_MIME);
            WikittyPubTextHelper.setName(pubText, "pubtextnum_" + i);

            WikittyPubDataHelper.setContent(pubData,
                    ("content data" + i).getBytes());
            WikittyPubDataHelper.setMimeType(pubData, "image/jpg");
            WikittyPubDataHelper.setName(pubData, "pubdatanum_" + i);

            pubData.addExtension(WikittyLabelImpl.extensions);
            pubText.addExtension(WikittyLabelImpl.extensions);

            WikittyLabelHelper.addLabels(pubData, labels.get(i));
            WikittyLabelHelper.addLabels(pubText, labels.get(i));

            listWikittyPubData.add(pubData);
            listWikittyPubText.add(pubText);
        }

        // then initialise application config for wikitty proxy on service FS
        config = new ApplicationConfig();
        config.setOption(WikittyPublicationSynchronize.IS_RECURSION_OPTION,
                "true");
        config.setOption(
                WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS.getKey(),
                WikittyPublicationFileSystem.class.getName());

        starts = new File(tempHome.getAbsolutePath() + File.separator + "sub");
        if (starts.exists()) {
            FileUtil.deleteRecursively(starts);
        }
        starts.mkdir();

        String url = "file:///" + tempHome.getAbsolutePath() + "#sub";

        config.setOption(WikittyConfigOption.WIKITTY_SERVER_URL.getKey(), url);

        proxy = new WikittyProxy(
                WikittyServiceFactory.buildWikittyService(config));

        allCrit = Search.query().keyword("*").criteria();

    }

    @After
    public void delete() {
        FileUtil.deleteRecursively(tempHome);
    }

    /**
     * check save wikitty
     */
    @Test
    public void saveWikittyOnFS() {

        Assert.assertEquals(0, proxy.findAllIdByCriteria(allCrit).getNumFound());

        proxy.storeWikitty(listWikittyPubData);
        proxy.storeWikitty(listWikittyPubText);

        int result = listWikittyPubData.size() + listWikittyPubText.size();

        Assert.assertEquals(result, proxy.findAllIdByCriteria(allCrit)
                .getNumFound());

    }

    /**
     * check that the wikitty save are restorable
     */
    @Test
    public void testRestoreFromFS() {
        saveWikittyOnFS();

        Map<String, Wikitty> mapwikitty = new HashMap<String, Wikitty>();

        for (Wikitty w : listWikittyPubData) {
            mapwikitty.put(w.getId(), w);
        }

        for (Wikitty w : listWikittyPubText) {
            mapwikitty.put(w.getId(), w);
        }

        // Assert that wikitty are the same after save
        // wikitty service FS doesn't not alterate version
        for (Wikitty w : proxy.findAllByCriteria(allCrit)) {
            Assert.assertEquals(mapwikitty.get(w.getId()), w);
        }
    }

    /**
     * Test that service know if wikitty change
     * @throws IOException
     */
    @Test
    public void testUpdateFileDirectly() throws IOException {
        saveWikittyOnFS();

        String label = WikittyFileUtil.labelToPath(labels.get(1));

        File wikittyOnFS = new File(tempHome.getCanonicalPath()
                + File.separator + label + File.separator + "pubtextnum_1.wp");
        // check file exist
        Assert.assertTrue(wikittyOnFS.exists());

        WikittyPubTextImpl wik = new WikittyPubTextImpl();
        wik.setName("pubtextnum_1");

        String newContent = "unNouveauContenu";

        WikittyPubTextImpl result = proxy.findByExample(wik);
        // check content on actual wikitty
        Assert.assertNotSame(newContent, result.getContent());

        // update the file
        FileWriter fw = new FileWriter(wikittyOnFS);

        fw.write(newContent);
        fw.flush();
        fw.close();

        // Wikitty

        result = proxy.findByExample(wik);
        // check that the wikitty is now updated with the new content
        Assert.assertEquals(newContent, result.getContent());

    }
    
    /**
     * Test that the service know if file are deleted
     * @throws IOException
     */
    @Test
    public void testDeleteFileDirectly() throws IOException {
        saveWikittyOnFS();

        String label = WikittyFileUtil.labelToPath(labels.get(1));

        File wikittyOnFS = new File(tempHome.getCanonicalPath()
                + File.separator + label + File.separator + "pubtextnum_1.wp");
        // check file exist
        Assert.assertTrue(wikittyOnFS.exists());

        WikittyPubTextImpl wik = new WikittyPubTextImpl();
        wik.setName("pubtextnum_1");
        
        WikittyPubTextImpl result = proxy.findByExample(wik);
        String id = result.getWikittyId();
        // check that the wikitty exist
        Assert.assertNotNull(result);

        //delete the file
        wikittyOnFS.delete();

        // check that the wikitty is now deleted
        Assert.assertNull(proxy.restore(id));

    }
    
    
    /**
     * Test delete with the service
     * @throws IOException
     */
    @Test
    public void deleteWithService() throws IOException{
        saveWikittyOnFS();

        String label = WikittyFileUtil.labelToPath(labels.get(1));

        File wikittyOnFS = new File(tempHome.getCanonicalPath()
                + File.separator + label + File.separator + "pubtextnum_1.wp");
        // check file exist
        Assert.assertTrue(wikittyOnFS.exists());
        
        WikittyPubTextImpl wik = new WikittyPubTextImpl();
        wik.setName("pubtextnum_1");
        
        WikittyPubTextImpl result = proxy.findByExample(wik);
        String id = result.getWikittyId();
        // delete the wikitt
        proxy.delete(id);
        // check that the wikitty and the file are now deleted
        Assert.assertNull(proxy.restore(id));
        Assert.assertFalse(wikittyOnFS.exists());

    }
    
    /**
     * Test delete a directory, chech if that delete correctly wikitty
     * under corresponding labels
     * @throws IOException
     */
    @Test
    public void testDeleteDirectoryDirectly() throws IOException {
        saveWikittyOnFS();

        String label = WikittyFileUtil.labelToPath(labels.get(1));

        File wikittyOnFS = new File(tempHome.getCanonicalPath()
                + File.separator + label );
        // check file exist
        Assert.assertTrue(wikittyOnFS.exists());
        
        // Criteria to find wikittypubtext/data with label starts with 
        Search mainRequest = Search.query();
        Search subRoqu = mainRequest.or();

        // must have the type of wikittypubtext/wikittypubdata
        subRoqu.exteq(WikittyPubText.EXT_WIKITTYPUBTEXT).exteq(
                WikittyPubData.EXT_WIKITTYPUBDATA);
        Criteria criteriaOnLabels = mainRequest.exteq(WikittyLabel.EXT_WIKITTYLABEL)
        .sw(WikittyLabel.FQ_FIELD_WIKITTYLABEL_LABELS, labels.get(1))
        .criteria();
        
        int totalBeforeDelete = proxy.findAllIdByCriteria(allCrit).getNumFound();
        
        int numConcerned = proxy.findAllIdByCriteria(criteriaOnLabels).getNumFound();
        
        
        FileUtil.deleteRecursively( wikittyOnFS);
        

        int numAfterDelete=proxy.findAllIdByCriteria(allCrit).getNumFound();
        // assert all wikitty concerned are deleted
        Assert.assertNotSame(numAfterDelete, totalBeforeDelete);
        Assert.assertEquals(numAfterDelete, totalBeforeDelete-numConcerned);

    }
    
   
}
