/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.FileUtil;

public class PropertiesExtendedTest {
    
    protected File tempHome;
    protected Properties props;
    protected File propsFile;
    
    
    @Before
    public void init() throws IOException{
        
        if (! FileUtils.getTempDirectory().exists()){
            FileUtils.getTempDirectory().mkdir();
        }
                
        tempHome = new File(FileUtils.getTempDirectory().getCanonicalFile()
                + File.separator + "wikittyFileUtilTest");
        
        System.out.println(tempHome);
        
        if (tempHome.exists()){
            FileUtil.deleteRecursively(tempHome);
            
        }
        tempHome.mkdir();
        
        propsFile = new File(tempHome.getCanonicalPath()+File.separator+"props.properties");
        
        if(propsFile.exists()){
            propsFile.delete();
        }
        propsFile.createNewFile();
        
        
        props = new Properties();
        props.put("propertie1", "value1");
        props.put("propertie2", "value2");
        props.put("propertie3", "value3");
        props.put("propertie4", "value4");
        props.put("propertie5", "value5");
        props.put("propertie6", "value6");
        
    }
    
    @After
    public void delete(){
        FileUtil.deleteRecursively(tempHome);
        propsFile.delete();
    }
    
    /**
     * test how to write and store with properties extended
     * @throws FileNotFoundException
     * @throws IOException
     */
    @Test
    public void testWrite() throws FileNotFoundException, IOException{
        
        PropertiesExtended propsExtended = new PropertiesExtended(propsFile);
        // assert empty
        Assert.assertEquals(0, propsExtended.keySet().size());
        
        // put in
        propsExtended.put("propertie1", "value1");
        
        Properties proper = new Properties();
        
        proper.load(new FileReader(propsFile));
        
        Assert.assertNull(proper.get("propertie1"));
        
        propsExtended.store();
        
        proper.load(new FileReader(propsFile));
        // assert after save that propertie have been set
        Assert.assertEquals("value1", proper.get("propertie1"));
    }
    
    @Test
    public void testLoad() throws FileNotFoundException, IOException {
        PropertiesExtended propsExtended = new PropertiesExtended(propsFile);
        // assert empty
        Assert.assertEquals(0, propsExtended.keySet().size());
        
        FileWriter fw = new FileWriter(propsFile);
        
        props.store(fw, "");
        
        // load properties from file
        propsExtended.load(propsFile);
        
        // assert properties are write
        Assert.assertEquals(props.size(), propsExtended.keySet().size());
    }
}
