/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.ArgumentsParserException;
import org.nuiton.util.FileUtil;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyServiceFactory;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyLabelHelper;
import org.nuiton.wikitty.entities.WikittyLabelImpl;
import org.nuiton.wikitty.publication.entities.WikittyPubTextHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubTextImpl;
import org.nuiton.wikitty.publication.synchro.WikittyPublicationFileSystem;
import org.nuiton.wikitty.publication.synchro.WikittyPublicationSynchronize;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.Search;

public class WikittyPublicationFallbackServiceTest {

    protected File tempHome;
    protected WikittyProxy proxyOnMain;
    protected WikittyProxy proxyOnFallBack;
    protected WikittyProxy proxy;
    protected Map<String, Wikitty> onMain;
    protected Map<String, Wikitty> onFallBack;
    protected Map<String, Wikitty> onBoth;
    private Criteria allCrit;

    @Before
    public void init() throws IOException, ArgumentsParserException {
        allCrit = Search.query().keyword("*").criteria();

        if (!FileUtils.getTempDirectory().exists()) {
            FileUtils.getTempDirectory().mkdir();
        }

        tempHome = new File(FileUtils.getTempDirectory().getCanonicalFile()
                + File.separator + "WikittyPublicationFallbackServiceTest");

        if (tempHome.exists()) {
            FileUtil.deleteRecursively(tempHome);

        }
        tempHome.mkdir();

        File starts = new File(tempHome.getAbsolutePath() + File.separator
                + "sub");
        if (starts.exists()) {
            FileUtil.deleteRecursively(starts);
        }
        starts.mkdir();

        // then initialise application config for wikitty proxy on service FS
        ApplicationConfig configFS = new ApplicationConfig();
        configFS.setOption(WikittyPublicationSynchronize.IS_RECURSION_OPTION,
                "true");
        configFS.setOption(
                WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS.getKey(),
                WikittyPublicationFileSystem.class.getName());

        String url = "file:///" + tempHome.getAbsolutePath() + "#sub";

        configFS.setOption(WikittyConfigOption.WIKITTY_SERVER_URL.getKey(), url);

        ApplicationConfig configLocal = new ApplicationConfig(
                "wikitty-publication-ws-test.properties");

        // configLocal = WikittyPublicationConfig.getConfig();

        String bdDir = tempHome + File.separator + "bd";

        configLocal.setOption("wikitty.data.directory", bdDir);
        configLocal.parse(null);
        // create two service
        proxyOnMain = new WikittyProxy(
                WikittyServiceFactory.buildWikittyService(configLocal));

        proxyOnFallBack = new WikittyProxy(
                WikittyServiceFactory.buildWikittyService(configFS));
        // initialize fall back service
        WikittyPublicationFallbackService fallback = new WikittyPublicationFallbackService(
                proxyOnMain.getWikittyService(), null);
        fallback.setFallbackService(proxyOnFallBack.getWikittyService());
        proxy = new WikittyProxy(fallback);

        onMain = new HashMap<String, Wikitty>();
        onBoth = new HashMap<String, Wikitty>();
        onFallBack = new HashMap<String, Wikitty>();

        // create wikitty for the test
        for (int i = 0; i < 10; i++) {

            Wikitty pubTextMain = new WikittyImpl();
            Wikitty pubTextFall = new WikittyImpl();

            pubTextMain
                    .addExtension(WikittyPubTextImpl.extensionWikittyPubText);
            pubTextFall
                    .addExtension(WikittyPubTextImpl.extensionWikittyPubText);

            WikittyPubTextHelper.setContent(pubTextMain, "content_main_" + i);
       
            WikittyPubTextHelper.setMimeType(pubTextMain,
                    "text/javascript");
            WikittyPubTextHelper.setName(pubTextMain, "pubtextOnMain_" + i);

            WikittyPubTextHelper.setContent(pubTextFall, "content_fall_" + i);

            WikittyPubTextHelper.setMimeType(pubTextFall,
                    "text/javascript");
            WikittyPubTextHelper.setName(pubTextFall, "pubtextOnFall_" + i);

            pubTextMain.addExtension(WikittyLabelImpl.extensions);
            pubTextFall.addExtension(WikittyLabelImpl.extensions);

            WikittyLabelHelper.addLabels(pubTextMain, "sub");
            WikittyLabelHelper.addLabels(pubTextFall, "sub");

            onFallBack.put(pubTextFall.getId(), pubTextFall);
            onMain.put(pubTextMain.getId(), pubTextMain);

        }

        for (int i = 0; i < 5; i++) {

            Wikitty pubTextMain = new WikittyImpl();

            pubTextMain
                    .addExtension(WikittyPubTextImpl.extensionWikittyPubText);

            WikittyPubTextHelper.setContent(pubTextMain, "content_onBoth_" + i);

            WikittyPubTextHelper.setMimeType(pubTextMain,
                    "text/javascript");
            WikittyPubTextHelper.setName(pubTextMain, "pubtextOnboth_" + i);

            pubTextMain.addExtension(WikittyLabelImpl.extensions);

            WikittyLabelHelper.addLabels(pubTextMain, "sub");

            onBoth.put(pubTextMain.getId(), pubTextMain);

        }

    }

    /**
     * delete directory after test
     */
    @After
    public void afterTest() {
        // remove directory
        FileUtil.deleteRecursively(tempHome);
    }

    /**
     * 
     */
    @Test
    public void testServiceCreated() {
        Assert.assertNotNull(proxyOnFallBack);
        Assert.assertNotNull(proxyOnMain);
        Assert.assertNotNull(proxy);

        Assert.assertTrue(((WikittyPublicationFallbackService) proxy
                .getWikittyService()).isFallBack());
    }

    public void putAllWikittyOnServices() {

        List<Wikitty> listFall = new LinkedList<Wikitty>();
        listFall.addAll(onFallBack.values());
        listFall.addAll(onBoth.values());

        proxyOnFallBack.storeWikitty(listFall);

        List<Wikitty> listmain = new LinkedList<Wikitty>();
        listmain.addAll(onMain.values());
        listmain.addAll(onBoth.values());

        proxyOnMain.storeWikitty(listmain);

    }

    @Test
    public void testSimpleFind() {
        putAllWikittyOnServices();

        int numfound = proxy.findAllByCriteria(allCrit).getNumFound();

        int totalOnBoth = onBoth.size() + onMain.size() + onFallBack.size();

        Assert.assertEquals(totalOnBoth, numfound);
    }

    @Test
    public void testMainServiceIsMoreImportantOne() {
        putAllWikittyOnServices();

        String id = (String) onBoth.keySet().toArray()[1];

        Assert.assertNotNull(id);

        String originalContent;

        Wikitty wikittFromFall = proxyOnFallBack.restore(id);
        
        System.out.println(wikittFromFall.toString());
        originalContent = WikittyPubTextHelper.getContent(wikittFromFall);

        Wikitty wikitty = proxy.restore(id);

        Assert.assertEquals(originalContent,
                WikittyPubTextHelper.getContent(wikitty));

        String newContent = "newContentOnlyOnFallBack";

        WikittyPubTextHelper.setContent(wikittFromFall, newContent);

        proxyOnFallBack.store(wikittFromFall);

        wikitty = proxy.restore(id);

        Assert.assertEquals(originalContent,
                WikittyPubTextHelper.getContent(wikitty));
    }

    @Test
    public void testMainServiceIsMoreImportantTwo() {
        putAllWikittyOnServices();

        String id = (String) onBoth.keySet().toArray()[1];

        Assert.assertNotNull(id);

        String originalContent;

        Wikitty wikittFromFall = proxyOnFallBack.restore(id);

        originalContent = WikittyPubTextHelper.getContent(wikittFromFall);

        // restore on main
        Wikitty wikitty = proxyOnMain.restore(id);

        // check that content are the same
        Assert.assertEquals(originalContent,
                WikittyPubTextHelper.getContent(wikitty));

        // write new content on wikitty
        String newContent = "newContentOnlyOnFallBack";

        WikittyPubTextHelper.setContent(wikitty, newContent);

        // Save the new version of the wikitty on main
        proxyOnMain.store(wikitty);

        wikitty = proxy.restore(id);
        // assert that we have the new content
        Assert.assertEquals(newContent,
                WikittyPubTextHelper.getContent(wikitty));

        wikittFromFall = proxyOnFallBack.restore(id);

        // assert that new content is not inside fallback
        Assert.assertNotSame(newContent,
                WikittyPubTextHelper.getContent(wikittFromFall));
    }

    /**
     * Test the save only appear on main
     */
    @Test
    public void testSave() {
        putAllWikittyOnServices();

        Wikitty wikitt = new WikittyImpl();

        wikitt.addExtension(WikittyPubTextImpl.extensionWikittyPubText);
        WikittyPubTextHelper.setContent(wikitt, "testSaveContent");

        WikittyPubTextHelper.setMimeType(wikitt, "text/javascript");
        WikittyPubTextHelper.setName(wikitt, "testSave");

        wikitt.addExtension(WikittyLabelImpl.extensions);

        WikittyLabelHelper.addLabels(wikitt, "sub");

        proxy.store(wikitt);

        Assert.assertNull(proxyOnFallBack.restore(wikitt.getId()));
        Assert.assertNotNull(proxyOnMain.restore(wikitt.getId()));

    }
    
    
    /**
     * Test save a wikitty that can be found on fallback 
     * test that the new version of the wikitty is on main
     * 
     */
    @Test
    public void testSaveWikittyFromFall() {
        putAllWikittyOnServices();
        
        String id = (String) onFallBack.keySet().toArray()[1];
        
        // assert the object is in the right service
        Assert.assertNotNull(proxyOnFallBack.restore(id));
        Assert.assertNull(proxyOnMain.restore(id));
        Assert.assertNotNull(proxy.restore(id));
    }
    


}


