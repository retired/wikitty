/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
                  package org.nuiton.wikitty.publication.synchro;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.ArgumentsParserException;
import org.nuiton.util.FileUtil;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyServiceFactory;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyLabelHelper;
import org.nuiton.wikitty.entities.WikittyLabelImpl;
import org.nuiton.wikitty.publication.WikittyPublicationFallbackService;
import org.nuiton.wikitty.publication.entities.WikittyPubTextHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubTextImpl;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.Search;

public class WikittyPublicationSynchronizeTest {

    
    protected File tempHome;
    protected WikittyProxy proxyOnClassic;
    protected WikittyProxy proxyOnFS;

    protected Map<String, Wikitty> onMain;
    protected Map<String, Wikitty> onFallBack;

    private Criteria allCrit;

    @Before
    public void init() throws IOException, ArgumentsParserException {
        allCrit = Search.query().keyword("*").criteria();

        if (!FileUtils.getTempDirectory().exists()) {
            FileUtils.getTempDirectory().mkdir();
        }

        tempHome = new File(FileUtils.getTempDirectory().getCanonicalFile()
                + File.separator + "WikittyPublicationFallbackServiceTest");

        if (tempHome.exists()) {
            FileUtil.deleteRecursively(tempHome);

        }
        tempHome.mkdir();

        File starts = new File(tempHome.getAbsolutePath() + File.separator
                + "sub");
        if (starts.exists()) {
            FileUtil.deleteRecursively(starts);
        }
        starts.mkdir();

        // then initialise application config for wikitty proxy on service FS
        ApplicationConfig configFS = new ApplicationConfig();
        configFS.setOption(WikittyPublicationSynchronize.IS_RECURSION_OPTION,
                "true");
        configFS.setOption(
                WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS.getKey(),
                WikittyPublicationFileSystem.class.getName());

        String url = "file:///" + tempHome.getAbsolutePath() + "#sub";

        configFS.setOption(WikittyConfigOption.WIKITTY_SERVER_URL.getKey(), url);

        ApplicationConfig configLocal = new ApplicationConfig(
                "wikitty-publication-ui/src/main/resources/wikitty-publication-ws-test.properties");

        // configLocal = WikittyPublicationConfig.getConfig();

        String bdDir = tempHome + File.separator + "bd";

        configLocal.setOption("wikitty.data.directory", bdDir);
        configLocal.parse(null);
        // create two service
        proxyOnClassic = new WikittyProxy(
                WikittyServiceFactory.buildWikittyService(configLocal));

        proxyOnFS = new WikittyProxy(
                WikittyServiceFactory.buildWikittyService(configFS));
        // initialize fall back service
        WikittyPublicationFallbackService fallback = new WikittyPublicationFallbackService(
                proxyOnClassic.getWikittyService(), null);
        fallback.setFallbackService(proxyOnFS.getWikittyService());
        

        onMain = new HashMap<String, Wikitty>();
        
        onFallBack = new HashMap<String, Wikitty>();

        // create wikitty for the test
        for (int i = 0; i < 10; i++) {

            Wikitty pubTextMain = new WikittyImpl();
            Wikitty pubTextFall = new WikittyImpl();

            pubTextMain
                    .addExtension(WikittyPubTextImpl.extensionWikittyPubText);
            pubTextFall
                    .addExtension(WikittyPubTextImpl.extensionWikittyPubText);

            WikittyPubTextHelper.setContent(pubTextMain, "content_main_" + i);
            WikittyPubTextHelper.setMimeType(pubTextMain,
                    "text/javascript");
            WikittyPubTextHelper.setName(pubTextMain, "pubtextOnMain_" + i);

            WikittyPubTextHelper.setContent(pubTextFall, "content_fall_" + i);
            WikittyPubTextHelper.setMimeType(pubTextFall,
                    "text/javascript");
            WikittyPubTextHelper.setName(pubTextFall, "pubtextOnFall_" + i);

            pubTextMain.addExtension(WikittyLabelImpl.extensions);
            pubTextFall.addExtension(WikittyLabelImpl.extensions);

            WikittyLabelHelper.addLabels(pubTextMain, "sub");
            WikittyLabelHelper.addLabels(pubTextFall, "sub");

            onFallBack.put(pubTextFall.getId(), pubTextFall);
            onMain.put(pubTextMain.getId(), pubTextMain);

        }

     

    }

    /**
     * delete directory after test
     */
    @After
    public void afterTest() {
        // remove directory
        FileUtil.deleteRecursively(tempHome);
    }


    public void putAllWikittyOnServices() {

        List<Wikitty> listFall = new LinkedList<Wikitty>();
        listFall.addAll(onFallBack.values());


        proxyOnFS.storeWikitty(listFall);

        List<Wikitty> listmain = new LinkedList<Wikitty>();
        listmain.addAll(onMain.values());


        proxyOnClassic.storeWikitty(listmain);

    }
    
    
    /*
     * Liste des tests à faire: 
     *  
     *  1) créer un wikitty service file system et un autre sur cajo
     *  créer des wikittys et les mettres dans un puis synchroniser avec l'autre
     *  et voir que tout est envoyé
     *  
     *  2) créer des nouveau sur l'un wikitty service, modifier des anciens
     *  et voir que seulement les anciens sont mis à jour
     *  
     *  3) supprimer et modifier, plus nouveau et voir que seulement ceux supprimer
     *  passent
     *  
     *  4) la même chose avec la gestion de la récursion pour update
     *  
     *  5) la même chose avec la gestion de la récursion pour existing
     *  
     *  6) la même chose avec la gestion de la récursion pour delete
     *  
     *  7) raccourci update
     *  
     *  8) raccourci existing
     *  
     *  9) raccourci delete
     *  
     *  
     */
    
}
