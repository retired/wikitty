/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;

public class WikittyFileUtilTest {

    /*
     * 1) test création du path à partir de wikitty
     * 
     * 2) test de la cohérence dans la création des paths à partir de label
     */

    /**
     * Test the label conversion to path
     */
    @Test
    public void testStringLabelToPath() {

        List<String> listLabel = new LinkedList<String>();
        listLabel.add("sub1");
        listLabel.add("sub2");
        listLabel.add("sub3");
        // construct label
        String label = StringUtil.join(listLabel,
                WikittyFileUtil.WIKITTY_LABEL_SEPARATOR, true);
        // contruct path
        String path = StringUtil.join(listLabel, File.separator, true);

        // convert label to path
        String resultPath = WikittyFileUtil.labelToPath(label);

        Assert.assertEquals(resultPath, path);
    }

    
    /**
     * Test the path creation from a label
     * @throws IOException
     */
    @Test
    public void testDirectoryCreationFromPath() throws IOException {

        List<String> listLabel = new LinkedList<String>();
        listLabel.add("sub1");
        listLabel.add("sub2");
        listLabel.add("sub3");
        // construct label
        String label = StringUtil.join(listLabel,
                WikittyFileUtil.WIKITTY_LABEL_SEPARATOR, true);

        // contruct path
        String path = StringUtil.join(listLabel, File.separator, true);

        File home = new File(FileUtils.getTempDirectory().getCanonicalFile()
                + File.separator + "wikittyFileUtilTest");
        
        // assert the temporary file does not exist
        if (home.exists()){
            FileUtil.deleteRecursively(home);
            
        }
        home.mkdir();

        
        File resultCreated = new File(home.getCanonicalPath()+File.separator+path);
        // assert the complete path not exist
        Assert.assertFalse(resultCreated.exists());
        // construct the directory
        WikittyFileUtil.createFilesFromLabelPath(home, label);
        // assert the diretories exists
        Assert.assertTrue(resultCreated.exists());
        
        FileUtil.deleteRecursively(home);
        }

}
