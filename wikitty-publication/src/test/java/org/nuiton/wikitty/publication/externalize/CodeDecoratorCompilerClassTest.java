/*
 * #%L
 * Wikitty :: publication
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.externalize;


public class CodeDecoratorCompilerClassTest {

       
   /*
    * Ce test sera pour tester la compilation d'un wikitty pub text 
    * en pub text compiled et son éxécution.
    * 
    * 1) créer un wikittyPubText avec du javascript, vérifier le code décorer
    * (externalize partie décoration)
    * 
    * 2) la même chose avec du code java 
    * (externalize partie décoration)
    * 
    * 3) faire la compilation du javascript et l'éxécution
    * (externalize partie décoration+compileHelper+classLoaderHelper)
    * 
    * 4) faire la compilation du java et l'éxécution 
    * (externalize partie décorations+compileHelper+classLoaderHelper)
    */
    
}
