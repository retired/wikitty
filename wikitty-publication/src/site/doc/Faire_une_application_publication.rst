.. -
.. * #%L
.. * Wikitty :: publication
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
==============================================
Faire une application avec Wikitty Publication
==============================================
:Authors: Manoël Fortun

Comment faire ?
---------------

Pour faire une application pour Wikitty Publication ce n'est pas compliqué.
Il y a deux solutions:

Depuis un système de fichier
****************************
L'idéal est d'utiliser le plugin maven prévu à cet effet.
Il faut un Pom élémentaire comme présenté dans la doc outils_publication, et 
d'initiliaser le repo local avec la commande wp:init et de bien respecter 
les endroits où mettre les fichiers::

  src/main/wp
       #toutes les pages
  src/main/ressources/images
       #stocker les images
  src/main/ressources/jar
       #stocker les jars

comme cela est fait dans l'exemple fournit dans Wikitty Publication.

Dans le cas où l'on développe sur un système de fichier, il faut faire attention 
aux extensions des fichiers auxquels corresponds leurs mimes types, selon le
mapping contenu dans l'application soit par défaut:

text/javascript=wp
text/html.javascript=htmljs
text/html.java=htmlja
image/jpeg=jpg
image/png=png
text/java=java
application/jar=jar
application/uml.jar=zargo

Les mime type déterminent les langages présents dans les fichiers et donc
comment ils seront éxécutés, pour en savoir plus voir la documentation sur
Wikitty Publication Web Application ou celles traitant de l'ajout de langage.


Directement dans le Navigateur
******************************
Une autre solution est de modifier et créer ses wikitty directement par 
l'intermédiaire de l'interface d'édition, comme dans un wiki.
Néanmoins cette solution ne permet que de créer des wikitty dont l'extension
existe déjà dans le WikittyService.

On peut ajouter les extentions que l'on veut au wikitty et remplir les champs, 
mais on est limité aux extensions déjà présente dans le wikitty service.

Interface d'édition:

 * /[contextData]/edit/[mandatory_args].action (avec contextData est ce qu'on veut, voir la
   partie sur le war pour savoir à quoi cela correspond)

Quoi mettre dans les WikittyPubTex ?
------------------------------------
Dans le WikittyPubText, le contenu doit être écrit dans le langage correspondant
au mime type.

Mais ce n'est pas tout, on peut rajouter des éléments issues du binding, qui 
est un mécanisme qui permet d'utiliser du java à l'intérieur du code.

Il est possible de faire des interfaces graphiques en html, soit en intégrant 
le hmtl directement dans le code dans une variable de retour, soit en utilisant
le mécanisme de filtre et des mime types composés.

par exemple le mime type:

 * text/html.javascript signifie que on a du html et que entre les balises <% %> ou <%= %> est du javascript

Voir partie sur l'ajout de langage et le fonctionnement des filtres pour l'évaluation.

Bindings
********
Le mécanisme de binding permet l'import de classe java, l'invocation de méthode 
et plein d'autre chose.

Import d'une classe (avec le script engine javascript) :
 
 * importPackage(org.nuiton.wikitty.entities);

Instanciation: 

 * var resource = new WikittyResourceImpl();

Invocation de méthode:

 * resource.setName(wpContext.getArgument("nom"));

Par défaut il y a un certain nombre d'objets disponible en binding:
    
 * wpEval, objet qui permet d'évaluer d'autre wikitty 
 * wpSubContext
 * wpPage, string correspondant au mandatoy args
 * wpWikitty, wikitty en cours d'évaluation
 * wpContext, context d'évaluation

Interface de wpContext::

    public interface PublicationContext {
  
        HttpServletRequest getRequest();
  
        HttpServletResponse getResponse();
  
        /**
         * the current wikitty proxy
         * @return
         */
        WikittyProxy getWikittyProxy();
  
        /**
         * add context to the url and parameter if necessary
         * @param url
         * @return
         */
        String makeUrl(String url);
  
        /**
         * the current wikitty service
         * @return
         */
        WikittyService getWikittyService();
  
        List<String> getMandatoryArguments();
  
        String getArgument(String name);
  
        /**
         * Get
         * @param name
         * @param defaultValue
         * @return
         */
        String getArgument(String name, String defaultValue);
  
        /**
         * return the actual return content tye for the page
         * @return
         */
        String getContentType();
  
        /**
         * Set the content type for the return page
         * @param contentType
         */
        void setContentType(String contentType);
  
        String toString();
  
        /**
         * the map of the arguments in the context
         * @return
         */
        Map<String,String> getArguments();
  
    }


Par exemple

Construit l'url de l'image en question pour l'affichage

 * <img src='<%=wpContext.makeUrl("/raw/wikittypubuml")%>'/>


Intégre le résultat de l'évaluation du wikittyPubText qui possède le nom Test

 * <p><%=wpEval.doAction(wpContext, "Test")%></p>


Détails de l'exemple
--------------------

Dans Wikitty Publication se trouve un exemple d'application dans 
/src/site/exemple, l'exemple est éclaté en deux "projet".

Le projet resourceEntity:

    resourceEntity/
    |-- changelog.txt
    |-- LICENSE.tx
    |-- LICENSE.txt
    |-- pom.xml
    |-- README.txt
    `-- src
        `-- main
        |-- java
        |-- resources
        `-- xmi
            |-- wp-resource.properties
            |-- wp-resource.zargo


ResourceEntity est un mini projet d'utilisation de Wikitty et création d'entity
et des nouvelles extensions de wikitty. Pour pouvoir utiliser resourceAppli
il faut contruire le jar de resourceEntity, puisque les entités sont utilisé 
à l'intérieur.

Le projet resourceAppli:

    resourceAppli/
    |-- pom.xml
    `-- src
        `-- main
        |-- resources
        |   |-- images
        |   |   `-- wikittypubuml.png
        |   `-- jar
        `-- wp
            |-- clearProxy.java
            |-- dummy.htmljs
            |-- Javahtml.htmlja
            |-- JavaView.java
            |-- resources.htmljs
            |-- SelfModify.htmljs
            |-- Test.java
            `-- view.htmljs

Ce projet est le "parfait" exemple d'utilisation, l'architecture à été initialisé
avec la commande wp:init. Pour le faire fonctionner, il faut rajouter le jar
correspondant au projet resourceEntity dans le dossier src/main/resources/jar.
Et lancer la commande wp:run.

L'application sera disponible à l'adresse:
 * http://localhost:8080/[contextData]/view/.action (avec contextData ce qu'on veut,
   voir la partie sur le war pour savoir à quoi cela correspond)

Fichier de l'application:

 * clearProxy.java, va supprimer tout les wikitty du proxy (sauf ceux de l'application)
 * dummy.htmljs, exemple d'utilisation de post
 * Javahtml.htmlja, intégration d'interface html avec du java
 * JavaView.java, exemple avec du java corps de méthode sans signature
 * resources.htmljs, permet de créer des resources Entity: intégration d'ihm, mécanisme post, utilisation des bindings, instanciation de java dans du javascript.
 * view.htmljs, permet de faire des réservations de ressource: intégration d'ihm, mécanisme post, utilisation des bindings, instanciation de java dans du javascript.
 * Test.java, dummy exemple
 * SelfModify.htmljs, un wikitty pub text qui se tripote le content.
