.. -
.. * #%L
.. * Wikitty :: publication
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
======================================================
Comment rajouter des langages dans Wikitty Publication
======================================================
:Authors:  Manoël Fortun

Un nouveau langage
------------------

Pour rajouter un nouveau langage il faut enregistrer dans le service ou dans 
le classPath le jar contenant le script engine correspondant. Il faut s'assurer
que l'engine peut être récupéré avec un mimeType de forme : text/[langage], avec
langage qui est égale au langage de script. Puisque la forme text/[truc] correspond
qu wikitty pub text dans le mapping des mimeType.


Comment marche le mapping mimeType <-> extension de fichier
-----------------------------------------------------------

Dans les ressources du module Wikitty Publication il y a un fichier de propriétés
nommé: "mimetype.properties" qui contient les mapping mimeType <-> extension.
Seulement ceux à l'intérieur sont considéré.

Pour en rajouter il suffit de rajouter des lignes.

Les mimeTypes commençant par text/ seront considérés comme des wikittyPubText.
Les mimeTypes composé comme: 
text/html.javascript=htmljs
Sont des mimeTypes liés au filtres:

Comment marche les filtres
--------------------------

Les filtres sont à utiliser en relation avec le mapping mime type - extension.
Ils permettent de définir des transformations du contenu d'un wikitty pub text.
Ceci est utilisé par exemple pour insérer du html dans le contenu des wikitty
pub text facilement, ou d'autre.

Pour en rajouter il suffit de reprendre le template et de le remplir selon
ses besoins.

Les Options de fitre:
*********************

    * OpeningTemplate: chaine d'ouverture du template
    * WriteString: instruction d'écriture 
    * StringDelim: délimiteur de chaine exemple "
    * ConcatChar: caractère de concaténation
    * EndingCar: caractère de fin d'instruction
    * ClosingWriterChar: chaine qui se place avant le caractère de fin et le délimiteur de chaine
    * OpeningWriterChar: chaine qui se place juste après l'instruction d'écriture et avant le délimiteur de chaine
    * ClosingTemplate: chaine de fermeture du template
    * Key: clé du mime type, définition des langages utilisé langageAutourBalise.LangageEntreBalise si le mime type du wikitty pub fini par la clé, ce template sera appliqué. Key sous forme [xxx.yyy]
 

Un exemple de fonctionnement
****************************

Contenu d'un wikitty pub text::
    
    <html>                              
    <%var name="bob"%>                                  
    <h1>Hello World <%=name%>!</h1>     
    </html>                         

Règle de remplacement avec le template::

    wpContext.setContentType("text/[yyy]")[EndingCar]                                                   
                                                                                                           
    [OpeningTemplate]<html>[StringDelim][ClosingWriterChar][EndingCar]

    [WriteString][OpeningWriterChar][StringDelim]bob[StringDelim][ClosingWriterChar][EndingCar]

    [WriteString][OpeningWriterChar][StringDelim]<h1>Hello World [StringDelim]
            [ConcatChar]name[ConcatChar][StringDelim]!</h1>[StringDelim][ClosingWriterChar][EndingCar]

    [WriteString][OpeningWriterChar][StringDelim]</html>[StringDelim][ClosingWriterChar][EndingCar][ClosingTemplate]

ce filtre sera actif quand on le mime type du wikittyPubText sera :
text/[xxx.yyy], après transformation par le filtre le mimetype sera text/[xxx]

Si on avait utilisé le filtre de base::

    wpContext.setContentType("text/html; charset=UTF-8");      
                                                               
    var wp_result="<html>";                                   
    var name= bob;                                        
    wp_result+="<h1>Hello World "+name+"!</h1>";                                                  
    wp_result+="</html>";                                      


ce filtre sera actif quand on le mime type du wikittyPubText sera :
text/html.javascript, après transformation par le filtre le mimetype sera application/javascript


