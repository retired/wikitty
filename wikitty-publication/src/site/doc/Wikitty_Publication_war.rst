.. -
.. * #%L
.. * Wikitty :: publication
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
===================================
Wikitty Publication Web Application
===================================
:Authors: Manoël Fortun


Wikitty Publication se déploie dans un serveur web et offre la possibilité de 
créer et d'utiliser des applications à l'intérieur. Tout cela en utilisant 
un navigateur web et en naviguant dans les différentes pages proposée.

Liste des pages
---------------

Il existe 4 pages différentes, ou action, avec chacune un rôle particulier.

Raw
***
Page qui ne fonctionne que pour les WikittyPubData et WikittyPubText.
Cette action renvois au navigateur le contenu du wikittyPub avec son mime type,
un wikittyPubData qui contient une image type : image/png sera renvoyé tel quel.
Un wikittyPubText qui contient un script text/javascript sera renvoyé tel quel 
aussi.

Edit
****
Page qui fonctionne pour tout les types de wikitty.
Permet l'édition de wikitty existant ou la création de nouveau wikitty.
On peut ajouter à un wikitty seulement les extensions déjà présente dans le 
WikittyService.


Eval
****
Page qui ne fonctionne que pour les WikittyPubData, WikittyPubText et 
WikittyPubTextCompiled.

Si cette page rencontre un WikittyPubData, elle fait la même chose que l'action
Raw.

Si c'est un WikittyPubTextCompiled donc un wikitty qui contient une classe java
spécifique, on appel la méthode eval de cette classe en lui passant le binding.
Un wikittyPubTextCompiled est un WikittyPubText qui a été compilé et qui n'est
donc plus évalué par un script engine dynamiquement, à la volé, mais on appel 
une méthode de la classe qui fait la même chose.

Si c'est un WikittyPubText en fonction du MimeType on ne fait pas la même chose:

 * text/java soit le MimeType du java, le contenu du wikitty est encapsulé dans une méthode "eval" prenant en paramètre les bindings, la classe est compilé et la méthode appelée, comme si le WikittyPubText était un WikittyPubTextCompiled
 * text/[xxx] ou [xxx] est un langage dont un script engine est disponible dans le classpath, on va déléguer au script engine le contenu du WikittyPubText et afficher dans une page le résultat de l'évaluation par le script engine.
 * text/[www.xxx] type mime qui correspond normalement à un filtre qui possède la "key" [www.xxx] le moteur d'évaluation appliquera le filtre correspondant avant normalement d'évaluer le résultat en fonction du mime type final normalement qui sera: text/[xxx] (les règles précédentes concernant les mime type seront appliquées)

View
****
Page qui liste tout les wikitty présent dans le WikittyService et qui permet de 
faire des recherches sur les wikitty.

Format des adresses
-------------------

Voici les adresses permettant d'accéder au différentes pages/action

 * /[contextData]/[contextApps]/[action]/[mandatory_args]?[args key=value]
ou
 * /[contextData]/[action]/[mandatory_args]?[args key=value]

Adresses sans contextApps disponible pour toute les actions.
Adresses avec contextApps disponible pour les actions : view, raw et eval.


action
******
Nom de la page.

mandatory_args
**************
Permet de retrouver le wikitty deux équivalences:

 * /eval/WikittyPubText.name:Wiki
 * /eval/Wiki

Pointer un wikitty en fonction d'un attribut de champ ou avec son id directement:

 * eval/elt_id:c9df9fd7-0714-4e1a-893c-0b37e0b8bc87 le wikitty avec l'id c9df9fd7-0714-4e1a-893c-0b37e0b8bc87

args key=value
**************
Arguments http get.


ContextData
***********

Le contextData sert pour charger des propriétés et charger le bon WikittyService.
Un WikittyService se charge avec des propriétés, l'endroit où il met sa base
de données, etc.

Par défaut l'application embarque des fichiers de propriétés pour charger des 
propriétés, on peut en rajouter des personnalisés. Si on trouve un fichier de 
propriétés dans le classPath de la forme: wikitty-publication-ws-[contextData].properties 
on le charge à la suite des propriétés par défaut écrasant les propriétés 
précédemment existante.

.. image:: multicontext.png

Ce diagramme montre bien le fonctionnement.

Si il n'y a pas de fichier de propriété correspondant au contextData, simplement 
un nouveau dossier de base de données sera créer.

De sorte que les adresses suivante ne pointent pas sur les mêmes données, isolant
dès lors les WikittyServices:

 * /wiki/chorem/eval/menu
 * /codelutin/chorem/eval/menu



ContextApps
***********

Tout WikittyPub possède aussi l'extension WikittyLabel, contextApps permet de 
retrouver le wikitty ciblé plus facilement. Un des labels du wikitty doit
commencer par contextApps. Cela permet aussi de faire que les liens vers les autres
wikitty que le wikittyPubText peut contenir doivent être des wikitty avec le
contextApps en label eux aussi.

Exemple :

 * /wiki/chorem/eval/menu

Le wikitty évalué, sera le wikittyPub qui a un attribut name égal à Menu
et qui possède un label qui commence par Chorem.
Si ce wikitty est un WikittyPubText qui contient un lien vers un autre wikitty
on cherchera un wikittyPub qui lui aussi a un label qui commence par Chorem.

Ce mécanisme permet d'éviter les collisions de noms et de pouvoir réutiliser
les noms et de s'assurer que c'est toujours le bon wikitty que l'on obtient.




