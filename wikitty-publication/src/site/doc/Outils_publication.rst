.. -
.. * #%L
.. * Wikitty :: publication
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
=====================
Outils de Publication
=====================
:Authors: Manoël Fortun

Wikitty Publication met à disposition divers outils pour faciliter le
développement d'application.

Synchronisation
---------------

La fonction Synchronisation est utilisable via un main, ou via ses méthodes, 
se référer à la javadoc pour son utilisation en dehors du main.

Le main nécessite des arguments, l'appelle peut avoir cette forme:

Utilisation complète
********************

 * ''wp sync [--norecursion] [--delete|--existing] [URI orgirine] [URI cible]''

Avec URI sous forme: 
   - file:///truc/machin/#label
   - hessian://www.adresse.com:8827/etc/etc#label
   - cajo://www.adresse.com:8827/etc/etc#label

 * [--norecursion] pour savoir si l'on s'occupe des sous labels du label ciblé.
 * [--delete] si l'on doit supprimer les wikitty qui se trouve sur l'uri cible et pas/plus sur l'uri origine
 * [--existing] si l'on doit seulement mettre à jour les wikitty en commun sur les deux uri
 * si aucune des deux option précédente on envois les nouveau wikitty, on met à jour et on supprime ceux qui ne sont plus là
 
La suppression n'est pas une vraie suppression elle se contente de supprimer le 
label ciblé du wikitty.

Les labels correspondent dans le cas d'une URI file au dossier "root".


Utilisation simplifié
*********************
Invocation aussi avec un main:

 * ''wp [update|commit] [--norecursion] [--delete|--existing] [label] [URI file]''

L'URI file est optionnelle, si pas précisée on va commit/update le dossier 
courrant.

Pour le commit update, on doit forcément préciser le label pour le wikitty 
service cible, cela pour permettre de commit ou update au bon endroit. 

Cette utilisation nécessite d'avoir utilisé la commande sync complète avec
un WikittyService origine sur file system, on l'on va commit/update avec le
dernier WikittyService utilisé en commande sync complète.


Fonction Externalize
--------------------
La fonction externalize est utilisable via un main, ou via ses méthodes, 
se référer à la javadoc pour son utilisation en dehors du main.

En appelant le main, sans aucun arguments, un WikittyService sur File system
est créer avec le répertoire courant comme label et donc tous les fichiers
seront convertit en wikitty pour être externalisé dans un jar.

Les WikittyPubData seront mit tel quel, alors que les WikittyPubText seront
compilé en WikittyPubTextCompiled en fonction de leurs mime type:
 * text/java, le contenu sera mit dans un une classe et dans une méthode eval qui prend en paramètre les bindings, cette classe sera compilé et ajouté dans le jar.
 * text/[xxx] ou [xxx] est un langage dont un script engine est disponible dans le classpath, on mettra le contenu dans une méthode eval qui contiendra le même corps que si on évaluait par le site
 * text/[www.xxx] type mime qui correspond normalement à un filtre qui possède la "key" [www.xxx] le moteur d'évaluation appliquera le filtre correspondant avant normalement d'externalize le résultat en fonction du mime type final normalement qui sera: text/[xxx] (les règles précédentes concernant les mime type seront appliquées)


Plugin maven
------------

Le plugin maven permet de d'utiliser naturellement les outils de synchronisation
et d'externalisation avec simplement en point d'entré un pom élémentaire

Pom élémentaire
***************

    <?xml version="1.0" encoding="UTF-8"?>
    <project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns="http://maven.apache.org/POM/4.0.0"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <modelVersion>4.0.0</modelVersion>
      <groupId>[groupID]</groupId>
      <artifactId>[artifactId]</artifactId>
      <version>[version]</version>
      <description>[description]</description>
      <build>
        <plugins>
           <plugin>
            <groupId>org.nuiton.wikitty</groupId>
            <artifactId>wp-maven-plugin</artifactId>
            <version>3.2-SNAPSHOT</version>
            <configuration>
                <applicationName>[applicationName]</applicationName>
                <wikittyServiceUrl>[wikittyServiceUrl]</wikittyServiceUrl>
                <serveurID>[serveurID]</serveurID>
                <uploadUrl>[uploadUrl]</uploadUrl>
                <publicationVersion>[publicationVersion]</publicationVersion>
            </configuration>
           </plugin>
        </plugins>
      </build>
    </project>

Les valeurs [groupID], [artifactId], [version], [description] se remplissent
selon l'application développée.

Les paramètres [applicationName], [wikittyServiceUrl], [serveurID], [uploadUrl],
et [publicationVersion] sont important pour l'utilisation du plugin.

 * [applicationName] sera le contextApps, le label de base des WikittyPub de l'application
 * [wikittyServiceUrl] sera l'url de syncrhonisation
 * [serveurID] l'id du serveur dans les setting de maven
 * [uploadUrl] url d'envoi pour le serveur
 * [publicationVersion] la version du war de Wikitty Publication à utiliser pour les tests locaux.

De plus si l'on déclare des dépendances dans ce pom, de façon "classique" par
exemple:

    <dependencies>
          <dependency>
        <groupId>commons-fileupload</groupId>
        <artifactId>commons-fileupload</artifactId>
        <version>1.2.2</version>
          </dependency>
    </dependencies>

Les dépendances seront automatiquement téléchargées et ajoutées dans le dossier
adéquat.

Goals Disponibles
*****************

 * mvn wp:init
 * mvn wp:run
 * mvn wp:deploy
 * mvn wp:update
 * mvn wp:jar
 * mvn wp:jar-deploy
 * mvn wp:clean

wp:init sert pour initialiser la bonne architecture de travail soit:

src/main/wp
       #toute les pages
src/main/ressources/images
       #stocker les images
src/main/ressources/jar
       #stoker les jars

wp:run pour lancer l'application sur un serveur local avec le war de Wikitty
Publication avec la version en paramètre. Adresse: 
 
 * http://localhost:8080/[contextData]/view/.action (avec contextData ce qu'on veut, voir la partie sur le war pour savoir à quoi cela correspond)


wp:deploy pour déployer l'application sur le WikittyService [wikittyServiceUrl] 
(utilisation de la synchronisation) avec la bonne architecture:

[applicationName].wp
       #toute les pages
[applicationName].ressources.images
       #stocker les images
[applicationName].ressources.jar
       #stoker les jars

wp:update pour mettre à jour sur le WikittyService [wikittyServiceUrl] 
(utilisation de la synchronisation)

wp:jar pour construire le jar de l'application à partir des fichiers de l'application
local. Jar qui respectera aussi la bonne architecture avec [applicationName]
en root label.

wp:jar-deploy sert pour envoyer le jar à l'url [uploadUrl] avec les informations
d'identification du serveur [serveurID]

wp:clean permet de nettoyer l'espace de travail local des fichiers de propriétés
créés par le WikittyService sur File System avec la commande wp:run

