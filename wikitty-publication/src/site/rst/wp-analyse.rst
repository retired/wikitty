.. -
.. * #%L
.. * Wikitty :: publication
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 CodeLutin, Benjamin Poussin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
===================
wikitty-publication
===================

But
===

Le but de la publication est de pouvoir créer des sites web en
stockant les informations nécessaires directement dans wikitty.

Contraintes
===========

La publication doit répondre à plusieurs contraintes:

* stockage dans wikitty
* développement en équipe aisé avec les outils habituels (éditeur,
  svn, maven, ...)
* gestion de l'authentification et des autorisations
* multi-langue
* multi-context (utilisation de plusieurs WikittyService par la même
  instance de publication)
* permettre le développement d'applications
* déplacement aisé d'applications d'un WikittyService vers un autre
  (partage d'application)
* mise en production d'applications ainsi créées hors de wikitty pour
  des besoins spécifiques

Les actions
===========

L'idée général est d'avoir quelques actions de base mappées sur un
path (ex: /view/ -> action view) Devant cette action on détermine sur
quel context on fait l'action (WikittyService). Le reste de l'url sont
des arguments pour l'action.

Les actions de base sont:

* ''login'' pour permettre de s'authentifier
* ''view'' pour permettre de recherche des wikitties d'afficher la
  liste des resultats et d'en voir un en détail
* search

	* vue list/détail

* ''edit'' permet de créer, ajouter des extensions à un wikitty,
  modifier les valeurs des champs, supprimer un objet

	* create
	* modif
	* delete

* ''raw'' permet de retourner la valeur d'un champs d'un wikitty dans
  un typemime précis
* ''eval'' permet d'évaluer un WikittyPubText
* ''admin'' Analyse et admin du WikittyService

	* import
	* export
	* synchro depuis un autre WikittyService (affichage des couples
      WikittyPub???/Label dispo pour ensuite synchroniser une
      application)
	* proxy stat
	* admin extension

Pour cela on défini deux objets de publication:

* WikittyPubText: pour les contenus textuels

	* name: String unique=true notNull=true
	* content: String multiline=true
	* mimetype: String
   * extension: String

* WikittyPubData: pour les contenues binaires

	* name: String unique=true notNull=true
	* content: Binary
	* mimetype: String
   * extension: String

Ensuite grâce à ces actions de bases on peut construire des
applications complètement contenu dans le WikittyService (code,
template, données)

Format des URL
==============

la syntaxe complète d'une url est::

  /[context]/[action]/[mandatory_args]?[args key=value]

* ''context'' est le context pour trouver le bon WikittyService
* ''action'' est le nom de l'action à effectuer, la classe gérant
  cette action est trouvée dans le fichier de configuration
* ''mandatory_args'' sont des arguments obligatoires pour l'action
* ''args'' sont des arguments optionels pour l'action sout forme de
  paramètre de requête

Context
=======

Lorsque la demande HTTP arrive sur le server un
WikittyPublicationContext est créé pour conservé le context de la
demande. Ce context permet de récupérer un WikittyProxy pointant sur
le bon WikittyService. Pour cela le fichier par defaut Wikitty est lu,
la variable wikitty.data.directory est surchargée pour lui ajouter le
context de l'url (pour différencier les espaces de stockage des
différents context). Ensuite s'il existe un fichier spécifique de
configuration de wikitty pour ce context il est lu pour permettre la
surcharge de configuration spécifiquement pour un context.  Les
fichiers de configuration spécifique sont trouvé grâce au pattern de
fichier trouvé dans la configuration de wikitty-publication.

Recherche des actions
=====================

L'action est retrouvée grâce au fichier de configuration de
wikitty-publication. On indique dans le fichier la classe héritant de
WikittyPublicationAction permettant de gérer l'action demandée.

Amélioration des URLs
=====================

Par défaut les actions recherchent ''WikittyPubText.name et
WikittyPubData.name s'il n'y a pas de = dans l'argument obligatoire.''

Par exemple les URLs suivantes sont équivalentes::

  /eval/WikittyPubText.name:Wiki
  /eval/Wiki
  
  /raw/WikittyPubText.name:WikiLogo
  /raw/WikiLogo

Action eval
===========

Eval permet d'évaluer un WikittyPubText en fonction de son typemime,
pour cela on utilise l'api javax.script. Plus il y a d'engine
disponible plus il y aura de langage supporté. Par défaut seul le
javascript (Rhino) est présent dans le JDK.

Automatiquement on a accès dans les scripts aux variables:

* ''WPContext'' qui est le context de publication
* ''nextUrl'' est en fait la concatenation des arguments obligatoires
  moins la page actuellement evaluée. ex:
  /eval/Wiki/AutrePage/EtEncore => /AutrePage/EtEncore

Action Login
============

Cette action si elle n'a pas d'argument affiche un formulaire pour
demander un login et un password. Si l'action à les arguments login et
password alors elle authentifie et crée le cookie nécessaire. En cas
d'échec ou de succès si dans les arguments on retrouve les actions à
faire on les faits sinon on affiche simplement le résultat.

exemple d'url d'authentification::

  /login?login=moi&password=xxxx&succes=/eval/Wiki/WikiHello&error=/eval/WikiBadLogin

Si on a l'argument logout de présent dans les paramètres alors un
logout est forcé. Par défaut si on demande un login et que
l'utilisateur était déjà loggué alors un logout est aussi fait avant
la nouvelle authentification.

exemple d'url de logout forcé::

  /login?logout&succes=/eval/Wiki/WikiHello

Authentification
================

Une fois loggué le token est conservé dans un cookie pour permettre
d'être authentifier pour plusieurs contexts (WikittyService) distinct
en même temps.

Développement d'application
===========================

Le but est de pouvoir facilement développer des applications avec ces
outils abituels (editor, svn, ...).  Pour cela on utilise
l'application d'extraction de WikittyPubText et WikittyPubData. Cette
application permet de récupérer l'ensemble de ces types d'objet en
fonction d'un Label qu'ils ont. Il faut donc que l'ensemble de ces
objets pour une application donnée est un Label en commun.

Si on a le WikittyPubText(''"Wiki", "<h1><%=...", "text/jsp"'') avec
le Label ''org.chorem'' il deviendra sur le système de fichier
/org/chorem/Wiki.jsp avec comme contenu le content du
WikittyPubText. On procède de la même manière pour les
WikittyPubData. L'application a un module de mapping entre le type
mime et l'extension des fichiers.

Les Labels recherchés sont récursif si besoin. Par exemple
''org.chorem.bonzoms'' est un sous Label de ''org.chorem. ''Si l'on
demande donc la récupération des WikittyPubText en récursif de
''org.chorem ''on récupérera aussi les WikittyPubText qui ont le Label
''org.chorem.bonzoms ''ou'' org.chorem.gepeto''.

L'application de synchronisation est capable de récupérer (checkout)
mais aussi de pousser (commit) les modifications.

Lors du commit il est possible de pousser les modifications vers un
autre context que le context initial. Par exemple si l'on récupère les
fichiers depuis le context 'chorem' on peut repousser les fichiers
dans le context 'chorem-test' et ainsi facilement faire des tests
avant de pousser en production les changements.

Il est ainsi possible de mettre les fichiers dans un svn classique si
on le souhaite.

commande de l'application de synchronisation:

* ''checkout [--recursion (true|false)] [url du WikittyService] [Label à extraire] [directory local d'accueil]''
* ''update [--recursion (true|false)] [--ws (url du WikittyService)] [répertoire à mettre à jour]''
* ''commit [--recursion (true|false)] [--ws (url du WikittyService)] [répertoire à pousser]''
* ''delete [--ws (url du WikittyService)] [répertoire ou fichier à supprimer]''
* ''relocate [nouvelle url du WikittyService par defaut] [directory a relocaliser]''

Il n'y a pas de add, tous les fichiers sont automatiquement commités.
L'action relocate recherche le fichier ''.wikitty-publication/source''
pour le modifier

Lors du checkout un fichier .wikitty-publication-sync/source est créé
dans le répertoire root du chekout contenant l'url source des fichiers
et le label extrait. Si lors d'un commit ou delete l'url n'est pas
indiqué, alors cette url sera utilisée par défaut. Dans chaque
répertoire de l'application lors du ''checkout/update/commit'' la
wikitty version de chaque fichier présent est mis dans le fichier
.wikitty-publication/versions. Ce fichier est sous la forme d'un
fichier de propriété avec en cle le nom du fichier/wikitty et valeur
la version. Cela permet de ne pas autoriser le commit si des
modifications ont été faite sur le serveur. Il faudra alors récupérer
la version du serveur et fusionner avec la version local.

Déploiement d'application
=========================

Il est bien sur possible d'utiliser l'application avec tous les
sources dans Wikitty. Mais il est aussi possible d'utiliser
l'application en extrayant les sources dans des fichiers et les
packagers dans un war. Dans ce cas l'application est figée et n'est
plus modifiable à l'exécution. Pour cela on utilise la servlet
WikittyPublicationExternalize qui au lieu d'aller chercher les
WikittyPubText et WikittyPubData dans le WikittyService va les
chercher sur le système de fichiers.

Dans le mode externalize les jsp et autres resources peuvent être
précompilées.

On peut donc avoir deux modes de mise en production suivant la taille
du site ou le moment de mise en production. Par exemple pour les sites
à fort volume la solution externalizée sera privilégiée ou pour les
sites qui souhaites mettre à deux endroit différent l'application et
les données pour des raisons de sécurites.

Au contraire lors d'une phase de prototypage ou si l'on souhaite
pouvoir modifier facilement l'application on utilisera le déploiement
avec l'application dans Wikitty.

Statistique d'accès au pages
============================

Le ''WikittyServiceAccesStat'' permet de mettre en place les stats sur
le nombre de fois qu'un wikitty a été restoré.  Lors d'un restore un
WikittyAccesStat(restoredWikittyId, userId, tokenId) est créé et
enregistré. Le service ''WikittyServiceAccesStat ''permet par
configuration de déterminer quel type de wikitty doit-être
monitoré. Pour monitorer l'utilisation d'une application il faut
configurer le service pour monitorer les WikittyPubText et
WikittyPubData.

Il est ensuite possible de sortir les stats:

* nombre d'accès à une page
* nombre d'accès à une page pour un utilisateur donné
* nombre d'accès à une page pour un utilisateur dans une session donnée
* nombre de pages accèdées par un utilisateur
* nombre de pages accèdées durant une session par un utilisateur

Il est ensuite possible de croiser ces stats avec les ids des pages
d'une application pour connaitre le nombre de solicitation d'un user à
une application.

Plugin Maven
============

Il serait intéressant d'avoir les goals suivant pour maven:

* ''wp-commit'' pour commiter les fichiers sur un WikittyService
* ''wp-run'' lance un conteneur de servlet avec le war
  wikitty-publication, pousse les fichiers dans ce service pour tester
  en local
* ''wp-verify ''essaie de compiler tout ce qui est compilable pour
  ressortir les erreurs
* ''wp-package'' qui package l'application en mode externalize, et
  précompile les jsp et autres resources

Application Wiki
================

Cette application commence par un WikittyPubText qui se nome Wiki. De
type text/jsp. Cette page est en fait le template général de
l'application (bandeau, menu, footer). Il contient des appels à
l'évaluation d'autres pages avec des paramètres. Lors de l'appel de
l'eval de cette page, il est possible d'indiquer aussi sur l'url une
autre page à évaluer.

contenu de la page::

  <html>
  ...
     <div id="bandeau"><img src="<%=wpContext.makeUrl("/raw/WikiLogo")%>"/></div>
     <div id="menu"><%=eval.doAction(wpContext, "/WikiMenu")%></div>
     <div id="content"><%=eval.doAction(wpContext, wpSubContext")%></div>
  ...
  </html>

Ici on affiche un Logo avec l'action ''raw''. On évalue ''WikiMenu''
pour afficher le menu. Et on évalue le reste de l'url qui sert de page
principale. S'il n'y a pas de suite à l'url alors par défaut on évalue
''WikiHello''.

L'url d'appel pourrait-être par exemple::

  /eval/Wiki/Bonzomes.list

Dans ce cas le content prendra le résultat de l'évalution de
''Bonzomes.list''.

Authorisation d'accès aux pages d'une application
=================================================

Une application est en fait stocké dans des Wikitties, une page de
l'application est un WikittyPubText, il suffit donc de mettre les bons
droits sur le wikitty de cette page pour permettre ou non aux
personnes d'y accéder. Les données ont déjà la possibillté d'avoir des
droits et ne sont retournés à l'utilisateur que s'il en a le droit.

[TODO] réflechir comment faire pour le mode externalize
