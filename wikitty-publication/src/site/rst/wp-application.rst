.. -
.. * #%L
.. * Wikitty :: publication
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
wikitty publication application
===============================
:Authors: Benjamin Poussin, Manoël Fortun

But
***

Ces spécifications ont pour but de présenter la façon de développer une 
application avec wikitty publication, avec un plug in maven chargé
d'utiliser les fonctionnalités de synchronisation, externalisation, etc de
wikitty publication.


Application sur File system vers un repo wikitty service
********************************************************

On introduit une nouvelle notion le nom de l'application ou contextApps ou
nomContext dans la suite, celà représentera le nom de l'application qui 
servira de label de base pour les applications stockées dans un wikitty service.

Sur un wikitty service on retrouvera donc:

nomContext.wp
       #toute les pages
nomContext.ressources.images
       #stocker les images
nomContext.ressources.jar
       #stoker les binaires

Sur un file system l'application sera ainsi:

src/main/wp
       #toute les pages
src/main/ressources/images
       #stocker les images
src/main/ressources/jar
       #stoker les binaires


Plug-in Maven, target roles
***************************

Le plug in maven pour wikitty publication devra fournir un certain nombre 
de tache pour permettre l'utilisation aisé de wikitty publication.

Ces commandes seraient:

 * mvn wp:init
 * mvn wp:run
 * mvn -Ptest wp:deploy
 * mvn -Ptest wp:update
 * mvn wp:jar
 * mvn wp:jar-deploy

wp:init servirait pour initialiser un repository correctement formé avec les 
dossier correctement placé, selon la "bonne" architecture de wikitty publication

wp:run pour lancer l'application sur un serveur local et l'essayer donc.

-Ptest wp:deploy pour déployer l'application sur un wikitty service distant(utilisation de la
synchronisation)

-Ptest wp:update pour mettre à jour le wikitty service distant (utilisation de la
synchronisation)

wp:jar pour construire le jar de l'application à partir des fichiers de l'application
local

wp:jar-deploy sert pour envoyer le jar par ssh sur un serveur donc passer l'adresse en option


L'utilisation du plug-in doit être la suivante:

1) création d'un fichier pom.xml dans un répertoire
2) utilisation de maven pour initialiser le répertoire de travail pour
   satisfaire les contraites de wikitty-publication
   "mvn wp:init"
3) ajout de fichier source (l'application)
4) ajout de dépendance dans le pom.xml (jar utile pour le code source)
   (qui seront ajouté à l'application en tant que mimetype
   "application/jar")
5) test de l'application "mvn wp:run"
6) déploiement du jar sur le wikitty-service defini dans la config du
   pom.xml comme environnement de test "mvn -Ptest wp:deploy"

7) les utilisateurs peuvent tester l'application et la modifier

8) récuperation des modifs faites par les utilisateurs
   "mvn -Ptest wp:update"
8) création d'un jar "mvn wp:jar"
9) deploiement du jar sur le wikitty-service defini dans la config du
   pom.xml comme env de prod "mvn wp:deploy-jar"


Discussion autour des jars
**************************
En mode de développement 'EDI' les jar sont ajouter dans le fichier
pom.xml, par contre en mode de développement 'wiki' les jar sont
pousser dans le wp comme des fichiers images (seul le type mime les
différencies).

Actuellement seul le 2eme mode (wiki) est possible, mais je pense qu'il
n'y a pas de code supplémentaire pour le 2eme.

Ce que je verrais:
- avoir un répertoire "resources/jars" qui contient les jars provenant du
  wiki (non référencé en tant que dépendance dans le pom.xml)
- lorsqu'on pousse l'appli vers un wikitty-service, le plugin maven
  créer un répertoire target/resources/jars dans lequel il copie les
  jars du répertoire jars et copie aussi les dépendances déclarer dans
  le pom.xml, puis pousse le tout dans le wikitty-service comme il faut

Lorsqu'on récupère une appli d'un wikitty-service:
- on récupère les jars et on les mets dans resources/jars
- le plugin maven regarde si certain de ces jars ne sont pas declarer
  dans le pom.xml, si c'est le cas, il les supprime pour qu'ils ne soient
  pas en double.
- C'est à la charge du développeur, de regarder les jars qu'il reste
  pour les remplacer par les bonnes dépendances maven (le but étant
  qu'il n'y ait pratiquement jamais de jar dans le répertoire
  resources/jars si le développeur fait bien sont travail).



