.. -
.. * #%L
.. * Wikitty :: publication
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
Spécifications wikitty publication site
=======================================
:Authors: Manoël Fortun


Contexte, Intérêt
+++++++++++++++++

Wikitty publication permet de construire une application et de la stocker dans
un wikitty, ensuite via l'interface web de wikitty publication l'application 
peut s'éxécuter. Le travail sur cette partie consiste donc en l'amélioration
du prototype qui avait été réaliser. Conserver les fonctionnalités existantes
et en ajouter de nouvelle 


Migration vers struts
+++++++++++++++++++++

Le prototype fonctionne actuellement simplement avec des jsp simple et tout
est géré à la main pas forcément de façon optimal. La première étape de 
l'amélioration de wikitty publication passe par la migration donc vers struts 2
en conservant le fonctionnement actuel.


Les actions
+++++++++++

Les actions sont les "pages" de base de wikitty publication permettant tout le
fonctionnement du concept.

Il y a les actions de type administration comme les actions view et edit, qui
permettent respectivement de voir les wikitty présent sur le wikitty service
et d'éditer les wikitty, d'en créer de nouveau.

La page view propose aussi un moteur de recherche pour retrouver des wikitty
dans le wikitty service.

Ensuite deux actions plus particulières qui sont le coeur de wikitty publication
l'action RAW et l'action EVAL, qui respectivement servent pour un rendu brute
du contenu d'un wikitty pub et l'évaluation du contenu d'un wikitty pub.

Concrétement le RAW renvoi le contenu d'un wikitty pub au navigateur selon le
mimeType de du wikitty Pub, une image png sera de cette façon renvoyé, un texte
sera renvoyé en brute.

L'EVAL est l'action qui se sert des scripts engine pour évaluer le contenu
d'un wikitty pub text et afficher le résultat de l'évaluation dans une page.
L'évaluation d'un wikittyPubData à le même effet qu'un raw.

Les urls des actions sont sous la forme :

 * /[contextData]/[contextApps]/[action]/[mandatory_args].action?[args key=value]

ou:

 * /[contextData]/[action]/[mandatory_args].action?[args key=value]


où action:

 * Raw
 * Edit
 * Eval
 * View

Args argument, classique d'une url
mandatory_args pour la recherche sur critéria et retrouver le wikitty concerné
par l'action. On peut placer directement le nom du wikittyPub, en ce cas si
contextApps est renseigné il permettra de rechercher un wikittyPub qui possède
le nom et contextApps comme début d'un de ses labels

ContextData sert pour charger les fichiers de propriété pour l'initialisation
des wikitty service, voir plus bas.

contextApps sert donc pour filtrer en amont sur les labels des wikitty,
si dans un wikitty pub text on fait appel à un eval d'un autre wikitty on 
cherchera en priorité les wikitty qui possèdent le même contextApps.

Ajout d'un mécanisme de login/logout
++++++++++++++++++++++++++++++++++++

Les couches sécurité des wikitty service s'occupe déjà d'un mécanisme de login
et d'une gestion des droits en fonction. Les utilisateurs sont stocker en tant
que wikitty dans la base et on peut attribué des droits à ces utilisateurs.
Ensuite via un wikitty service on peut se loger, sur une authentification on
récupére un jeton qui sera à passer pour chaque action, celà est géré par le
wikitty proxy qui encapsule le wikitty service.

Donc pour vérifier qu'une authentification s'est bien passé il faut regarder
dans le proxy si l'utilisateur est présent. Dans le cas de wikitty publication
au moment de l'action de login, délégué au proxy, on va stocker le jeton et
l'utilisateur dans la session.

Evidement le mécanisme de Logout lui nettoie simplement la session en la vidant
de ces informations.

Ensuite le fonction de la gestion de l'authentification se repose sur le
méchanisme d'intercepteur et de package de struts. On défini une pile
d'intercepteur personalisé qui en plus de la pile par defaut rajoute un
intercepteur d'authenfication qui vérifiera la présence des informations
utilisateur dans la session, et bloquera l'accès à la page demandé si
l'utilisateur n'est pas authentifié.

On définit que l'accès aux actions publication utilise cette pile d'intercepteur
qui gère l'authentification, c'est ainsi que fonctionne le méchanisme dans
wikitty publication avec struts

Lors de la redirection vers la page de login quand on tente d'accèder à une page
"protéger" on sauvegarde cette adresse pour que lors d'une authentification 
réussie on puisse se retrouver sur la page demandée

  /login.action?succes=/eval/Wiki/WikiHello

Et pour le logout de la même façon :

  [contextData]/logout.action?succes=/eval/Wiki/WikiHello

On pourrais imaginer que l'on puisse se loguer directement en donnant l'adresse
de login de cette façon:

   [contextData]/login.action?login=xxx&password=rrrr&succes=/eval/Wiki/WikiHello&error=/index

On doit pouvoir aussi pour le login et logout pouvoir préciser la page souhaitée
en cas d'erreur.


Amélioration des pages d'affichage et d'édition
+++++++++++++++++++++++++++++++++++++++++++++++

Les pages d'édition et d'affichage des wikitty étaient relativement simple, le
but étant une administration simple et efficace des wikitty. L'amélioration de
ces pages à consister en la correction des bugs présent déjà dans le prototype.

Par exemple le support pour les recherches dans wikitty dans la page view,
dans le prototype cette recherche était très limité et donc ne fonctionnait pas
très bien. De plus l'affichage des résultats nécessitait une amélioration.

Ensuite pour l'interface d'édition des wikitty j'ai intégré un décorateur de
Text Area qui permet une colorisation du contenu du champ, on peut choisir le
langage présent dans le champ pour avoir une colorisation, de l'indentation, des
outils de recherche/remplacer et d'autre, basiquement une sorte d'ide pour
faciliter le dévellopement de code dans les WikittyPubText.


Gestion, extension de contexte data
+++++++++++++++++++++++++++++++++++

A venir, explications sur le cloisonnement
même fichier de propriété etc
surcharge de propriété


Mécanisme de multicontext fallback Service
++++++++++++++++++++++++++++++++++++++++++

Cette fonctionnalité consiste en fait à l'encapsulation par un wikitty service
de deux autres wikitty service, quelque soit leurs natures.

Ce multi contexte éxécute ses recherches un wikitty service principal, et
compléte ses recherches si besoin avec les données du wikitty service dit
fallback.

De même l'écriture s'effectue sur le service principale, la restoration d'un
wikitty sur le principal et si il n'est pas dessus on va interroger le fallback
service.

Son utilisation n'est pas nécessairement limité à Wikitty Publication, il s'agit
finalement d'une implémentation différente de l'interface wikitty service. 

Ce mécanisme peut permettre la surcharge d'un wikitty, dans le sens ou on peut
modifier un wikitty qui aura été chargé depuis le fallback, mais à la sauvegarde
il sera restaurer depuis le wikitty principal, puisque celui ci est prioritaire.

On peut avoir ainsi un wikitty service statique, et un autre dynamique, par
exemple le wikitty service statique qui serait partagé et utilisé par d'autre
wikitty service multicontext, une base commune de wikitty.



