.. -
.. * #%L
.. * Wikitty :: publication
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
Wikitty Publication ui
----------------------
@author: Manoël Fortun

But
===

Pour le moment wikitty publication et ses script engine ne permettent pas de 
faire d'interface graphique, on peut seulement écrire du code qui sera exécuté
par le moteur.

Le but de cette partie est donc l'ajout de la possibilité de faire des
interfaces graphique dans wikitty publication afin de réellement pouvoir faire 
plus que l'affichage du résultat de l'évaluation d'un script, pouvoir atteindre
l'objectif de wikitty publication qui est la possibilité de développer des 
applications web complète.


Existant
========

Avec le script engine on peut définir le type de retour de l'évaluation du 
script, mais comme celui ci sera finalement affiché dans une page web, pour 
avoir des interfaces il faut que le résultat soit en html.

On retrouve dans les wikittyPubText des choses comme ça:

var result =
""+
"    <div class='menu'>\n"+
wpEval.doAction(wpContext, "WikiMenu")+
"    </div>\n"+
"    <h1>Bonjour Les Lutins,</h1>\n"+
"    Bienvenue sur le Wikitty Wiki\n";

wpContext.setContentType("text/html");
result;

Le html écrit dans des variables qui seront renvoyé en tant que résultat du 
sript, et on doit préciser le contentType du résultat pour que celui si soit 
correctement interprété.

A titre d'exemple dans xwiki quand on veut rajouter des éléments UI, on les 
écrit en html, mais c'est différent puisque dans xwiki on peut avoir plusieurs 
langage par "page" qui sont interprétés localement, ce que l'on peut pas faire 
dans wikitty publication, du moins pas avec le fonctionnement actuel.


Solution proposée
=================

Plutôt qu'un script engine pour le html qui permettrais l'intégration d'élements
des bindings, on va transformer les wikittyPubText qui contiennent des 
construction d'ui en wikittyPubText qui seront évalué par le script engine
correspondant au langage correspondant.

Par exemple :

<div class='menu'>
<%wpEval.doAction(wpContext, "WikiMenu")%>
</div>
<h1>Bonjour Les Lutins,</h1>
Bienvenue sur le Wikitty Wiki


va devenir :
wpContext.setContentType("text/html");
var result =
""+
"    <div class='menu'>\n"+
wpEval.doAction(wpContext, "WikiMenu")+
"    </div>\n"+
"    <h1>Bonjour Les Lutins,</h1>\n"+
"    Bienvenue sur le Wikitty Wiki\n";
result;

Et être finalement interprété par le script engine javascript.

Les éléments de binding ou de code seront glissé entre des "balises" type 
jsp: "<%= %> / <% %>" ce qui permettra de les isolers et de savoir quels morceau
"décorer". Entre les balises on doit pouvoir écrire du code dans le langage de
son choix, enfin le même pour toute la page. 

Pour spécifier que le code à été écrit dans un certain langage on va jouer avec
le mime type, par exmeple pour ce morceau de wikittyPubText, on pourra avoir un
mime type :

htmlp/js

Et une fois que l'on aura décoré le myme type sera application/javascript, la 
valeur après le "/" donnera le langage cible, et la valeur avant le "/" la façon
dont on doit interpréter le code "ui".

Pour converserver ces informations pour des wikittyPubText sur fileSystem ou
externaliser on pourra imaginer sauver celà dans l'extension du fichier par 
exemple : fichiertoto.htmlp.js
