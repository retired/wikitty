.. -
.. * #%L
.. * Wikitty :: publication
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
====================================
Modèle de données WikittyPublication
====================================

WikittyLabel
============

Tous les types WikittyPubText et WikittyPubData peuvent avoir en plus cette
extension, cela permet d'indiquer à quelle application l'objet appartient.

Un objet peut faire parti de plusieurs applications en même temps, par
exemple: une librairie binaire en version N, cela évite d'avoir N fois le binaire
de la même librairie dans le Storage.

WikittyPubText
==============

Représentation un contenu textuel qui peut-être évalué

:name: le nom de l'élément
:mimeType: le type du contenu, ce type est utilisé lors d'un eval pour
  retourner au navigateur le type de réponse
:content: le contenu textuel


WikittyPubTextCompiled
======================

Permet de stocker le résultat de la compilation d'un WikittyPubText, cet
objet est toujours associé à un WikittyPubText.

WikittyPubData
==============

Représentation un contenu binaire qui ne peut-être pas évalué

:name: le nom de l'élément
:mimeType: le type du contenu, ce type est utilisé lors d'un eval/raw pour
  retourner au navigateur le type de réponse
:content: le contenu binaire

