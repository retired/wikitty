Ceci est un exemple de d'utilisation de wikitty publication pour faire des applications

Le projet resourceEntity:

	resourceEntity/
	|-- changelog.txt
	|-- LICENSE.tx
	|-- LICENSE.txt
	|-- pom.xml
	|-- README.txt
	`-- src
	    `-- main
		|-- java
		|-- resources
		`-- xmi
		    |-- wp-resource.properties
		    |-- wp-resource.zargo


ResourceEntity est un mini projet d'utilisation de Wikitty et création d'entity
et des nouvelles extensions de wikitty. Pour pouvoir utiliser resourceAppli
il faut contruire le jar de resourceEntity, puisque les entités sont utilisé 
à l'intérieur.

Le projet resourceAppli:

	resourceAppli/
	|-- pom.xml
	`-- src
	    `-- main
		|-- resources
		|   |-- images
		|   |   `-- wikittypubuml.png
		|   `-- jar
		`-- wp
		    |-- clearProxy.java
		    |-- dummy.htmljs
		    |-- Javahtml.htmlja
		    |-- JavaView.java
		    |-- resources.htmljs
		    |-- SelfModify.htmljs
		    |-- Test.java
		    `-- view.htmljs

Ce projet est le "parfait" exemple d'utilisation, l'architecture à été initialisé
avec la commande wp:init. Pour le faire fonctionner, il faut rajouter le jar
correspondant au projet resourceEntity dans le dossier src/main/resources/jar.
Et lancer la commande wp:run.

L'application sera disponible à l'adresse:
 * http://localhost:8080/[contextData]/view/.action (avec contextData ce qu'on veut,
   voir la partie sur le war pour savoir à quoi cela correspond)

Fichier de l'application:

 * clearProxy.java, va supprimer tout les wikitty du proxy (sauf ceux de l'application)
 * dummy.htmljs, exemple d'utilisation de post
 * Javahtml.htmlja, intégration d'interface html avec du java
 * JavaView.java, exemple avec du java corps de méthode sans signature
 * resources.htmljs, permet de créer des resources Entity: intégration d'ihm, mécanisme post, utilisation des bindings, instanciation de java dans du javascript.
 * view.htmljs, permet de faire des réservations de ressource: intégration d'ihm, mécanisme post, utilisation des bindings, instanciation de java dans du javascript.
 * Test.java, dummy exemple
 * SelfModify.htmljs, un wikitty pub text qui se tripote le content.




