/*
 * #%L
 * Wikitty :: hessian server
 * %%
 * Copyright (C) 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;
import com.caucho.hessian.io.SerializerFactory;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;

/**
 * @author sletellier <letellier@codelutin.com>
 */
public class HessianTest {

    @Test
    public void testHessianBigDecimal() throws Exception {
        BigDecimal object = new BigDecimal("12474639.945458954");
        Hessian2Output os = new Hessian2Output(null);
        SerializerFactory factory = new SerializerFactory();
        os.setSerializerFactory(factory);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        os.init(buffer);
        os.writeObject(object);
        os.close();
        byte[] bytes = buffer.toByteArray();

        Hessian2Input is = new Hessian2Input(new ByteArrayInputStream(bytes));
        BigDecimal newObject = (BigDecimal) is.readObject();
        is.close();

        Assert.assertEquals(object, newObject);
    }
}
