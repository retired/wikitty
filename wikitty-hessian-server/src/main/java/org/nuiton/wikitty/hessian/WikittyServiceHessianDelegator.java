/*
 * #%L
 * Wikitty :: hessian server
 * %%
 * Copyright (C) 2010 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.hessian;

import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyServiceFactory;
import org.nuiton.wikitty.services.WikittyServiceDelegator;

/**
 * Hesian service delegator.
 * 
 * Based on empty constructor mandatory for some purpose (hessian), and
 * wikitty service factory to get delegated service.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class WikittyServiceHessianDelegator extends WikittyServiceDelegator {

    /** Configuration file name for application config. */
    protected static final String WIKITTY_HESSIAN_CONFIGURATION_FILE = "wikitty-hessian.properties";

    /**
     * Unique empty constructor.
     * 
     * @throws WikittyException if delegated service can't be obtained
     */
    public WikittyServiceHessianDelegator() throws WikittyException {

        super();

        try {
            // load configuration
            ApplicationConfig config = new ApplicationConfig();
            config.setConfigFileName(WIKITTY_HESSIAN_CONFIGURATION_FILE);
            config.parse(new String[0]);

            WikittyService service = WikittyServiceFactory.buildWikittyService(config);

            // set delegate service
            setDelegate(service);
        }
        catch (Exception eee) {
            throw new WikittyException("Can't get delegate proxy", eee);
        }
        
    }
}
