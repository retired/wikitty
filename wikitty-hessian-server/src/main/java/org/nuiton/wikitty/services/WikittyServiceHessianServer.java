/*
 * #%L
 * Wikitty :: hessian server
 * %%
 * Copyright (C) 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;


import com.caucho.hessian.server.HessianServlet;
import java.net.URL;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jetty.server.Connector;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyService;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.xml.XmlConfiguration;
import org.nuiton.wikitty.WikittyConfigOption;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceHessianServer extends WikittyServiceDelegator {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceHessianServer.class);

    public WikittyServiceHessianServer(ApplicationConfig config, WikittyService ws) {
        try {
            setDelegate(ws);

            // create and configure hessian servlet
            HessianServlet servlet = new HessianServlet();
            servlet.setHomeAPI(WikittyService.class);
            servlet.setHome(this);
            
            // create servlet container
            String jettyConfig = config.getOption(WikittyConfigOption.
                    WIKITTY_SERVER_CONFIG.getKey());
            // port and path from hessian server url in config
            String urlString = config.getOption(WikittyConfigOption.
                    WIKITTY_SERVER_URL.getKey());

            int port = 80;
            String path = "/";
            if (urlString != null && !"".equals(urlString)) {
                URL url = new URL(urlString);
                if (-1 != url.getPort()) {
                    port = url.getPort();
                }
                if (null != url.getPath()) {
                    path = url.getPath();
                    if (!path.endsWith("/")) {
                        path += "/";
                    }
                }
            }

            Server server;
            if (jettyConfig != null && !"".equals(jettyConfig)) {
                // use config file
                Resource fileserver_xml = Resource.newSystemResource(jettyConfig);
                XmlConfiguration configuration = new XmlConfiguration(fileserver_xml.getInputStream());
                server = (Server) configuration.configure();

                // change port as asked in wikitty url server
                boolean connectorPortFound = false;
                for (Connector connector : server.getConnectors()) {
                    if (connector instanceof SelectChannelConnector) {
                        connectorPortFound = true;
                        connector.setPort(port);
                    }
                }
                if (!connectorPortFound) {
                    Connector connector = new SelectChannelConnector();
                    connector.setPort(port);
                    server.addConnector(connector);
                }
            } else {
                // no config file
                server = new Server(port);
            }

            // put servlet in container
            ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
            context.setContextPath("/");
            server.setHandler(context);
            context.addServlet(new ServletHolder(servlet), path + "*");
            
            // start server
            server.start();
            server.join();

            log.info("Web server are running on port: " + port);
        } catch (Exception eee) {
            throw new WikittyException("Can't start wikitty server", eee);
        }
    }


}
