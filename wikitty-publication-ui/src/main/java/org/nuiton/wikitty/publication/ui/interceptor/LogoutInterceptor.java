/*
 * #%L
 * bow
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui.interceptor;

import java.util.Map;

import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.publication.ui.WikittyPublicationSession;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * Interceptor used to remove all trace of user in session. Used for login page
 */
public class LogoutInterceptor extends AbstractInterceptor {

    /**
     * 
     */
    private static final long serialVersionUID = -66045004020326043L;

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        Map<String, Object> session = ActionContext.getContext().getSession();
        // get wikittypublication context
        String contextData = StringUtil.split(invocation.getProxy()
                .getActionName(), "/")[0];
        // invalidate session for the context
        WikittyPublicationSession.invalidate(session, contextData);
        // next. 
        String result = invocation.invoke();
        return result;
    }
}
