/*
 * #%L
 * bow
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui.action;

import org.nuiton.wikitty.publication.ui.WikittyPublicationSession;




/**
 * Class used as an action logout
 * 
 * @author mfortun
 *
 */
public class PublicationActionLogout extends PublicationBaseAction {
    
    protected String success;
    
    private static final long serialVersionUID = 4806944250461551896L;    
       
    public String getSuccess() {
        return success;
    }
    
    public void setSuccess(String success) {
        this.success = success;
    }

    public String execute() {
        WikittyPublicationSession.invalidate(session, contextData);
        return SUCCESS;
    }
}
