/*
 * #%L
 * Wikitty :: publication-ui
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui.action;

import com.opensymphony.xwork2.ActionContext;
import org.apache.struts2.interceptor.SessionAware;
import org.nuiton.web.struts2.BaseAction;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.publication.PublicationContext;
import org.nuiton.wikitty.publication.entities.WikittyPubData;
import org.nuiton.wikitty.publication.entities.WikittyPubText;
import org.nuiton.wikitty.publication.ui.WikittyPublicationProxy;
import org.nuiton.wikitty.publication.ui.WikittyPublicationSession;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.Search;

import java.util.List;
import java.util.Map;


/**
 * Base action that wikitty publication will extend, needed to work within a
 * struts architecture.
 * 
 * @author mfortun
 * 
 */
public class PublicationBaseAction extends BaseAction implements
        SessionAware {

    /**
     * 
     */
    private static final long serialVersionUID = 1865905051843413141L;

    static final public int ARG_QUERY = 0;
    static final public String ARG_MIMETYPE = "mimetype";
    static final public String ARG_CONTENT_FIELD = "contentField";
    static final public String SEARCH_SEPARATOR = ":";
    final static protected String CONTEXT_ACTION_KEY = "action";

    static public String SEPARATOR = "/";


    protected Map<String, Object> session;
    
    protected String contextData;
    protected String contextApps;
    protected String args;
    
    

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public String getContextApps() {
        return contextApps;
    }

    public void setContextApps(String contextApps) {
        this.contextApps = contextApps;
    }

    public String getContextData() {
        return contextData;
    }

    public void setContextData(String context) {
        this.contextData = context;
    }
    
    

    public WikittyPublicationSession getWikittyPublicationSession() {
        WikittyPublicationSession result = WikittyPublicationSession
                .getWikittyPublicationSession(session, contextData);
        return result;
    }

    public WikittyPublicationProxy getWikittyPublicationProxy() {
        WikittyPublicationProxy result = getWikittyPublicationSession()
                .getProxy(contextData);
        return result;
    }

    @Override
    public void setSession(Map<String, Object> sess) {
        session = sess;
    }

    /**
     * Retourne le critere pour recherche l'objet sur lequel faire l'action. Le
     * nom du critere doit etre convenablement positionné avec la chaine qui a
     * permit la recherche
     * 
     * @param subContext
     * @return
     */
    protected Criteria searchCriteria(List<String> subContext) {
        Criteria result;
        if (subContext.size() <= 0) {
            result = null;
        } else {
            String searchString = subContext.get(ARG_QUERY);
            if (searchString.contains(SEARCH_SEPARATOR)) {
                // on a un field=value
                String[] arg = searchString.split(SEARCH_SEPARATOR);
                result = Search.query().eq(arg[0], arg[1])
                        .criteria(searchString);
            } else {
                // on a pas le champs, alors par defaut on recherche dans
                // WikittyPubText.name et WikittyPubData.name
              
                if (contextApps != null){
                   
                    Search mainRequest = Search.query();
                    Search subRoqu = mainRequest.or();
                    subRoqu.eq(WikittyPubText.FQ_FIELD_WIKITTYPUBTEXT_NAME,
                                    searchString)
                            .eq(WikittyPubData.FQ_FIELD_WIKITTYPUBDATA_NAME,
                                    searchString).criteria(searchString);
                    
                    
                    
                   // Search for wikitty pubdata or pub text with the name
                   // and their  
                    result = mainRequest
                            .exteq(WikittyLabel.EXT_WIKITTYLABEL)
                            .sw(WikittyLabel.FQ_FIELD_WIKITTYLABEL_LABELS,
                                    contextApps).criteria();
                    
                    
                }else{

                    result = Search
                            .query()
                            .or()
                            .eq(WikittyPubText.FQ_FIELD_WIKITTYPUBTEXT_NAME,
                                    searchString)
                            .eq(WikittyPubData.FQ_FIELD_WIKITTYPUBDATA_NAME,
                                    searchString).criteria(searchString);
                    
                }
                
            }
        }
        return result;
    }

    protected String getError(PublicationContext context) {
        context.setContentType("text/html");
        String result = String.format("<h1>bad query %s</h1>");
        return result;
    }

    public String getArgument(String key, String defaultValue) {
        Object temp = ActionContext.getContext().getParameters().get(key);

        String result = "";
        // TODO mfortun-2011-05-09 fix this with something clean
        if (temp == null) {
            result = defaultValue;
        } else {
            if (temp instanceof String[]) {
                for (String t : (String[]) temp) {
                    result += t;
                }
            } else {
                result = temp.toString();

            }

        }
        return result;
    }



}
