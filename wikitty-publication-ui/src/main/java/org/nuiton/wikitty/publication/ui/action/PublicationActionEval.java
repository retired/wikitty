/*
 * #%L
 * Wikitty :: publication-ui
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui.action;

import com.opensymphony.xwork2.ActionContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.ScriptEvaluator;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.publication.AbstractDecoredClass;
import org.nuiton.wikitty.publication.CodeDecorator;
import org.nuiton.wikitty.publication.EvalInterface;
import org.nuiton.wikitty.publication.MimeTypePubHelper;
import org.nuiton.wikitty.publication.PublicationContext;
import org.nuiton.wikitty.publication.WikittyPublicationConstant;
import org.nuiton.wikitty.publication.entities.WikittyPubData;
import org.nuiton.wikitty.publication.entities.WikittyPubDataHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubText;
import org.nuiton.wikitty.publication.entities.WikittyPubTextCompiled;
import org.nuiton.wikitty.publication.entities.WikittyPubTextCompiledHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubTextCompiledImpl;
import org.nuiton.wikitty.publication.entities.WikittyPubTextHelper;
import org.nuiton.wikitty.publication.externalize.CompileHelper;
import org.nuiton.wikitty.publication.externalize.JarUtil;
import org.nuiton.wikitty.publication.externalize.WikittyPublicationClassLoader;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.nuiton.wikitty.publication.ActionCodeDecorator;

/**
 * Java class
 * 
 * @author mfortun
 * 
 */
public class PublicationActionEval extends PublicationBaseAction implements
        ServletResponseAware, ServletRequestAware, PublicationContext,
        EvalInterface {

    public static final String RESULT_ACTION = "action";

    /**
     * 
     */
    private static final long serialVersionUID = -7649132751822833474L;
    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(PublicationActionEval.class);

    static protected String JAVA_TEMP_DIR_NAME = "PublicationJavaEvalDir";
    static protected String JAR_TEMP_DIR_NAME = "PublicationJarRepository";
    static protected String MD5_FILE = "JarMD5";

    protected String contentType;
    protected HttpServletResponse response;
    protected HttpServletRequest request;
    protected InputStream inputStream;
    protected WikittyPublicationClassLoader classloader;
    protected List<File> classPathSup;
    /** nom de l'action utilisateur a executer */
    protected String nextAction;

    @Override
    public void setServletResponse(HttpServletResponse arg0) {
        response = arg0;
    }

    @Override
    public void setServletRequest(HttpServletRequest arg0) {
        request = arg0;

    }

    public String getNextAction() {
        return nextAction;
    }

    public void setNextAction(String nextAction) {
        this.nextAction = nextAction;
    }

    @Override
    public String execute() throws Exception {
        String result = SUCCESS;

        // extract jars from wikitty service
        URL[] urls = getJarsDependency();

        // initialize classLoader
        // add jars to our custom classLoader
        classloader = new WikittyPublicationClassLoader(urls);
        // REALLY IMPORTANT put the classloader with the jarloaded

        // keep class loader
        ClassLoader oldClassLoader = Thread.currentThread().getContextClassLoader();
        // change class loader for action
        Thread.currentThread().setContextClassLoader(classloader);

        Object resultObject = doAction(this, getMandatoryArguments());

        if (RESULT_ACTION.equals(resultObject)) {
            result = RESULT_ACTION;
        }

        // restore class loader
//        Thread.currentThread().setContextClassLoader(oldClassLoader);

        log.fatal("Result eval is " + result);
        return result;
    }

    /**
     * extract jars from wikitty service, and return urls to thoses extracted
     * jars
     * 
     * @return urls to jars that must be add to classpath
     * @throws IOException
     * @throws MalformedURLException
     */
    protected URL[] getJarsDependency() throws IOException,
            MalformedURLException {
        // check for temporaryn dir
        if (!FileUtils.getTempDirectory().exists()) {
            FileUtils.getTempDirectory().mkdir();
        }

        File jarRepo;
        File md5File;
        // base criteria to found jars
        Search wikittyPubDataJarCrit = Search.query().eq(
                WikittyPubData.FQ_FIELD_WIKITTYPUBDATA_MIMETYPE,
                MimeTypePubHelper.JAR_TYPE);

        String jarRepoPath = FileUtils.getTempDirectory().getAbsolutePath()
                + File.separator + JAR_TEMP_DIR_NAME;

        // if contextApps set, update criteria
        if (contextApps != null) {
            // if application context is set sub dir
            jarRepoPath = jarRepoPath + File.separator + contextApps;
            // and complete criteria
            wikittyPubDataJarCrit.exteq(WikittyLabel.EXT_WIKITTYLABEL).sw(
                    WikittyLabel.FQ_FIELD_WIKITTYLABEL_LABELS, contextApps);

        }
        // dir that containt jars
        jarRepo = new File(jarRepoPath + File.separator);
        md5File = new File(jarRepo.getAbsolutePath() + File.separator
                + MD5_FILE);

        if (!jarRepo.exists()) {
            jarRepo.mkdir();
        }
        md5File.createNewFile();

        String md5ref = FileUtil.readAsString(md5File);

        // find jars
        PagedResult<Wikitty> jardatas = getWikittyProxy().findAllByCriteria(
                wikittyPubDataJarCrit.criteria());

        // construct md5 of the wikitty jars
        String sumMd5 = StringUtils.EMPTY;
        for (Wikitty w : jardatas) {
            sumMd5 += w.getId();
            sumMd5 += w.getVersion();
        }
        sumMd5 = StringUtil.encodeMD5(sumMd5);

        // if md5 not the same that mean that jars must be write on disk
        if (!sumMd5.equals(md5ref)) {
            FileUtil.writeString(md5File, sumMd5);
            for (Wikitty w : jardatas) {
                String name = WikittyPubDataHelper.getName(w);
                byte[] content = WikittyPubDataHelper.getContent(w);
                File tempJar = new File(jarRepo.getAbsolutePath()
                        + File.separator + name + ".jar");
                FileUtils.writeByteArrayToFile(tempJar, content);
            }

        }

        // collect urls of the jars loaded
        File[] jarFiles = jarRepo.listFiles(JarUtil.jarFilter);
        URL[] urls = new URL[1];
        classPathSup = new LinkedList<File>();
        classPathSup.add(jarRepo);
        urls[0] = jarRepo.toURI().toURL();

        if (jarFiles != null) {
            urls = new URL[jarFiles.length + 1];
            int i = 0;
            urls[i] = jarRepo.toURI().toURL();
            i++;
            for (File fj : jarFiles) {
                classPathSup.add(fj);
                urls[i] = fj.toURI().toURL();
                i++;
            }
        }
        return urls;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.nuiton.wikitty.publication.ui.action.EvalInterface#doAction(org.nuiton
     * .wikitty.publication.PublicationContext, java.util.List)
     */
    @Override
    public Object doAction(PublicationContext context, List<String> subContext) {
        log.info("path " + subContext);

        Object result;
        CodeDecorator decorator = new CodeDecorator();

        Criteria criteria = searchCriteria(subContext);

        if (criteria == null) {
            // rien a evaluer, on retourne une chaine vide
            result = "";
        } else {
            WikittyProxy proxy = context.getWikittyProxy();
            Wikitty w = proxy.findByCriteria(criteria);
            if (w == null || !w.hasExtension(WikittyPubText.EXT_WIKITTYPUBTEXT)) {

                /*
                 * If wikitty is not a wikitty pub text just return an error if
                 * wikitty is wikitty pub data do the same as raw action
                 */

                if (w != null && w.hasExtension(WikittyPubData.EXT_WIKITTYPUBDATA)) {

                    contentType = WikittyPubDataHelper.getMimeType(w);

                    inputStream = new ByteArrayInputStream(
                            WikittyPubDataHelper.getContent(w));
                    return WikittyPubDataHelper.getContent(w);
                }

                context.setContentType("text/plain");
                result = String.format("no data found for criteria '%s'",
                        criteria);
            } else if (WikittyPubTextHelper.getMimeType(w).equals(
                            MimeTypePubHelper.ACTION_TYPE)) {

                decorator = new ActionCodeDecorator();
                String name = WikittyPubTextHelper.getName(w);
                byte[] content;
                try {
                    content = compile(decorator, classPathSup, w);
                    classloader.addClass(name, content);
                    setNextAction(name);
                    result = RESULT_ACTION;
                    log.fatal(String.format("%s added in cl", name));
                } catch (IOException eee) {
                    log.debug("Error when processing the wikitty content");
                    // TODO Mfortun-2011-07-28 really handle exception
                    eee.printStackTrace();
                    result = getError(context);
                }
            } else if (w
                    .hasExtension(WikittyPubTextCompiled.EXT_WIKITTYPUBTEXTCOMPILED)
                    || WikittyPubTextHelper.getMimeType(w).equals(
                            MimeTypePubHelper.JAVA_TYPE)) {

                // if java type deleguate
                result = evalJava(context, subContext, decorator, criteria, w);

            } else {

                // check if wikitty is the kind that contain ui
                log.debug("Check if transformed needed");
                if (decorator.isTransformationNeeded(w)) {
                    log.debug("transformeneed");
                    try {
                        w = decorator.transformPubUIToPubText(w);
                    } catch (IOException e) {
                        log.debug("Error when processing the wikitty content");
                        // TODO Mfortun-2011-07-28 really handle exception
                        e.printStackTrace();
                        result = getError(context);
                    }
                }

                String content = WikittyPubTextHelper.getContent(w);

                if (content == null) {
                    result = getError(context);

                } else {

                    String mimetype = WikittyPubTextHelper.getMimeType(w);
                    // check if after decoration this is java
                    if (mimetype.equals(MimeTypePubHelper.JAVA_TYPE)) {
                        result = evalJava(context, subContext, decorator,
                                criteria, w);

                    } else {
                        // supprime de subcontext ce qui a ete utilise dans
                        // cette
                        // methode
                        subContext = new ArrayList<String>(subContext.subList(
                                1, subContext.size()));

                        Map<String, Object> bindings = new HashMap<String, Object>();
                        bindings.put(WikittyPublicationConstant.PAGE_NAME_VAR,
                                criteria.getName());
                        bindings.put(WikittyPublicationConstant.CONTEXT_VAR,
                                context);
                        bindings.put(WikittyPublicationConstant.SUBCONTEXT_VAR,
                                subContext);
                        bindings.put(WikittyPublicationConstant.WIKITTY_VAR, w);
                        bindings.put(WikittyPublicationConstant.EVAL_VAR, this);

                        result = ScriptEvaluator
                                .eval(classloader, criteria.getName(), content,
                                        mimetype, bindings);
                    }

                }
            }
        }

        inputStream = new ByteArrayInputStream(result.toString().getBytes());
        return result;
    }

    /**
     * Compile wikitty.content if needed and add it in
     * WikittyPubTextCompiled.byteCode in wikitty
     *
     * @param w
     * @return
     */
    protected byte[] compile(List<File> classPathSup, String name, String javaContent) throws IOException {
        if (!FileUtils.getTempDirectory().exists()) {
            FileUtils.getTempDirectory().mkdir();
        }

        File javaWikittyDir = new File(FileUtils.getTempDirectory()
                .getAbsolutePath() + File.separator + JAVA_TEMP_DIR_NAME);
        if (!javaWikittyDir.exists()) {
            javaWikittyDir.mkdir();
        }

        // compile
        File javaFile = new File(javaWikittyDir.getAbsolutePath()
                + File.separator + name + ".java");
        File classFile = new File(javaWikittyDir.getAbsolutePath()
                + File.separator + name + ".class");

        String content = null;
        if (javaFile.exists()) {
            content = FileUtil.readAsString(javaFile);
        }
        // compile only if the content has change if java already on the
        // FS
        if (content == null
                || !classFile.exists()
                || !StringUtil.encodeMD5(javaContent).equals(
                StringUtil.encodeMD5(content))
                ) {
            javaFile.createNewFile();
            FileUtil.writeString(javaFile, javaContent);

            PrintWriter writer = new PrintWriter(System.out);
            // compile
            CompileHelper.compile(classPathSup, javaWikittyDir,
                    javaFile, javaWikittyDir, writer);
        }

        byte[] wikittyByte = FileUtil.fileToByte(classFile);
        return wikittyByte;
    }
    
    /**
     * Compile wikitty.content if needed and add it in
     * WikittyPubTextCompiled.byteCode in wikitty
     *
     * @param w
     * @return
     */
    protected byte[] compile(CodeDecorator decorator, List<File> classPathSup, Wikitty w) throws IOException {
        String name = WikittyPubTextHelper.getName(w);
        // the java and the class are store insiede tmp directory
        // add extension and attribut

        String content = decorator.getCode(w);
        byte[] wikittyByte = compile(classPathSup, name, content);

        w.addExtension(WikittyPubTextCompiledImpl.extensionWikittyPubTextCompiled);
        WikittyPubTextCompiledHelper.setByteCode(w, wikittyByte);
        return wikittyByte;
    }

    /**
     * Method to evaluate java kind of wikittyPubText
     * 
     * @param context
     *            the context
     * @param subContext
     *            the subcontex
     * @param decorator
     *            instance codeDecorator
     * @param criteria
     *            the criteria
     * @param w
     *            the wikitty
     * @return the result of wikittyPubText content evaluation
     */
    protected Object evalJava(PublicationContext context,
            List<String> subContext, CodeDecorator decorator,
            Criteria criteria, Wikitty w) {

        Object result;
        // if wikitty pub text with java, need to transform it as
        // wikitty pub text compiled with java class bytecode
        if (WikittyPubTextHelper.getMimeType(w).equals(
                MimeTypePubHelper.JAVA_TYPE) ||
                WikittyPubTextHelper.getMimeType(w).equals(
                MimeTypePubHelper.ACTION_TYPE)) {
            try {
                compile(decorator, classPathSup, w);
            } catch (Exception e) {
                // TODO mfortun-2011-08-16 really handle exception
                if (log.isErrorEnabled()) {
                    log.error(
                            "Error while transforming wikittyPubText to wikittyPubTextCompiled wikitty:"
                                    + w, e);
                }

                result = getError(context);
            }

        }
        // evaluate the content of the wikittyPubTextCompiled, 
        // the byte code of a class that inherit of a specific abstract class
        byte[] content = WikittyPubTextCompiledHelper.getByteCode(w);
        if (content == null) {
            result = getError(context);
        } else {

            /*
             * if wikitty pub text compiled we load the class corresponding to
             * the byte code contained. And then invoke the eval method on it
             */
            String name = WikittyPubTextHelper.getName(w);

            // supprime de subcontext ce qui a ete utilise dans cette
            // methode
            subContext = new ArrayList<String>(subContext.subList(1,
                    subContext.size()));

            Map<String, Object> bindings = new HashMap<String, Object>();
            bindings.put(WikittyPublicationConstant.PAGE_NAME_VAR,
                    criteria.getName());
            bindings.put(WikittyPublicationConstant.CONTEXT_VAR, context);
            bindings.put(WikittyPublicationConstant.SUBCONTEXT_VAR, subContext);
            bindings.put(WikittyPublicationConstant.WIKITTY_VAR, w);
            bindings.put(WikittyPublicationConstant.EVAL_VAR, this);

            Class<? extends AbstractDecoredClass> clazz = (Class<? extends AbstractDecoredClass>) classloader
                    .addClass(name, content);

            try {
                result = clazz.newInstance().eval(bindings);
            } catch (Exception e) {
                // TODO Mfortun-2011-07-08 really handle exception
                e.printStackTrace();
                result = getError(context);
            }
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.nuiton.wikitty.publication.ui.action.EvalInterface#doAction(org.nuiton
     * .wikitty.publication.PublicationContext, java.lang.String)
     */
    @Override
    public Object doAction(PublicationContext context, String subContextAsText) {
        Object result;
        if (subContextAsText == null || "".equals(subContextAsText)) {
            result = getError(context);
        } else {
            if (subContextAsText.startsWith("/")) {
                subContextAsText = subContextAsText.substring(1);
            }
            String[] subContextArray = StringUtil.split(subContextAsText, "/");
            List<String> subContext = Arrays.asList(subContextArray);
            result = doAction(context, subContext);
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.nuiton.wikitty.publication.ui.action.EvalInterface#doAction(org.nuiton
     * .wikitty.publication.PublicationContext)
     */
    @Override
    public Object doAction(PublicationContext context) {
        Object result;
        if (context.getMandatoryArguments().size() <= 0) {
            result = getError(context);
        } else {
            result = doAction(context, context.getMandatoryArguments());
        }
        return result;
    }

    public String getMimeType() {
        return contentType;
    }

    public void setMimeType(String mimeType) {
        contentType = mimeType;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    @Override
    public HttpServletRequest getRequest() {
        return request;
    }

    @Override
    public String makeUrl(String url) {
        String finalUrl = url;
        if (!finalUrl.startsWith("/")) {
            finalUrl = "/" + finalUrl;
        }
        // TODO mforun-2011-05-09 rework on this method

        finalUrl = getRequest().getContextPath() + "/"
                + contextData + finalUrl;
        finalUrl = getResponse().encodeURL(finalUrl) + ".action";
        if (log.isInfoEnabled()) {
            log.info(String.format("transforme url from '%s' to '%s'", url,
                    finalUrl));
        }
        return finalUrl;
    }

    @Override
    public WikittyService getWikittyService() {
        return getWikittyPublicationProxy().getWikittyService();

    }

    @Override
    public List<String> getMandatoryArguments() {
        List<String> argsString = new ArrayList<String>();

        String[] argsTab = StringUtil.split(args, SEPARATOR);

        Collections.addAll(argsString, argsTab);
        return argsString;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public WikittyProxy getWikittyProxy() {
        return getWikittyPublicationProxy();
    }

    @Override
    public Map<String, String> getArguments() {
        Map<String, String> result = new HashMap<String, String>();

        for (String argElm : ActionContext.getContext().getParameters()
                .keySet()) {
            result.put(argElm, getArgument(argElm, ""));
        }

        return result;
    }

    @Override
    public String getArgument(String name) {
        return getArgument(name, StringUtils.EMPTY);
    }

}
