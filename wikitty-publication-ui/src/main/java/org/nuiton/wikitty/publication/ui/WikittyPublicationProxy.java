/*
 * #%L
 * Wikitty :: publication-ui
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui;

import java.util.HashMap;
import java.util.Map;

import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyServiceFactory;

/**
 * Proxy implementation for wikitty publication, that handle instance of the
 * wikitty service
 * 
 * @author mfortun
 * 
 */
public class WikittyPublicationProxy extends WikittyProxy {

    /**
     * 
     */
    private static final long serialVersionUID = -568462410130999972L;
    
    /**
     * Map to store instance of service, with contextData as key.
     * Used to avoid multiple instantiation of the same service.
     */
    static protected Map<String, WikittyService> mapService = new HashMap<String, WikittyService>();

    
    private WikittyPublicationProxy(ApplicationConfig config, WikittyService ws) {
        super(config, ws);
    }

    /**
     * return an instance of proxy service for a specific context with 
     * application config.
     * 
     * @param config the application config to instantiate service
     * @param context contextData for service
     * @return the instance of wikitty service
     */    
    static public WikittyPublicationProxy getInstance(String token,
            String context) {
        ApplicationConfig config = WikittyPublicationConfig
                .getInstance(context);
        WikittyService ws = getWikittyService(config, context);
        WikittyPublicationProxy result = new WikittyPublicationProxy(config, ws);
        result.setSecurityToken(token);

        return result;
    }

    /**
     * return an instance of wikitty service for a specific context with 
     * application config. If already instanciate return else create a new 
     * instance 
     * @param config the application config to instantiate service
     * @param context contextData for service
     * @return the instance of wikitty service
     */
    static protected WikittyService getWikittyService(ApplicationConfig config,
            String context) {
        if (!mapService.containsKey(context)) {
            synchronized (WikittyPublicationProxy.class) {
                if (!mapService.containsKey(context)) {
                    mapService.put(context,
                            WikittyServiceFactory.buildWikittyService(config));
                }
            }
        }
        return mapService.get(context);
    }

}
