/*
 * #%L
 * Wikitty :: publication-ui
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui;

import java.util.LinkedList;
import java.util.List;

/**
 * class used as a collection of option and param to fill edit page with
 * -codemirror css and script and supported langages
 * 
 * @author mfortun
 * 
 */
public class CodeMirrorWrapper {

    // /wikitty-publication/js/codemirror-ui/lib/CodeMirror-2.0/mode

    protected String servContext;
    protected String pathToCodeMirrorModeDir;
    protected String pathToCodeMirorUi;
    protected List<String> codeMirrorCssImport = new LinkedList<String>();
    protected List<String> codeMirrorScriptImport = new LinkedList<String>();

    protected List<SelectOption> langages = new LinkedList<SelectOption>();

    public CodeMirrorWrapper(String servContext, String pathToCodeMirorUi,
            String pathToCodeMirrorModeDire) {
        this.servContext = servContext;
        this.pathToCodeMirorUi = pathToCodeMirorUi;
        pathToCodeMirrorModeDir = pathToCodeMirorUi
                + pathToCodeMirrorModeDire;
        // TODO mfortun-2011-05-30 maybe change this

        codeMirrorScriptImport.add(servContext + pathToCodeMirorUi
                + "lib/CodeMirror-2.0/lib/codemirror.js");
        codeMirrorScriptImport.add(servContext + pathToCodeMirorUi
                + "js/codemirror-ui.js");

        codeMirrorScriptImport.add(servContext + pathToCodeMirrorModeDir
                + "/javascript/javascript.js");
        codeMirrorScriptImport.add(servContext + pathToCodeMirrorModeDir
                + "/clike/clike.js");
        codeMirrorScriptImport.add(servContext + pathToCodeMirrorModeDir
                + "/css/css.js");
        codeMirrorScriptImport.add(servContext + pathToCodeMirrorModeDir
                + "/diff/diff.js");
        codeMirrorScriptImport.add(servContext + pathToCodeMirrorModeDir
                + "/haskell/haskell.js");
        codeMirrorScriptImport.add(servContext + pathToCodeMirrorModeDir
                + "/htmlmixed/htmlmixed.js");
        codeMirrorScriptImport.add(servContext + pathToCodeMirrorModeDir
                + "/php/php.js");
        codeMirrorScriptImport.add(servContext + pathToCodeMirrorModeDir
                + "/stex/stex.js");

        codeMirrorScriptImport.add(servContext + pathToCodeMirrorModeDir
                + "/xml/xml.js");

        codeMirrorCssImport.add(servContext + pathToCodeMirorUi
                + "css/codemirror-ui.css");

        codeMirrorCssImport.add(servContext + pathToCodeMirorUi
                + "lib/CodeMirror-2.0/lib/codemirror.css");

        codeMirrorCssImport.add(servContext + pathToCodeMirrorModeDir
                + "/diff/diff.css");
        codeMirrorCssImport.add(servContext + pathToCodeMirrorModeDir
                + "/stex/stex.css");
        codeMirrorCssImport.add(servContext + pathToCodeMirrorModeDir
                + "/haskell/haskell.css");
        codeMirrorCssImport.add(servContext + pathToCodeMirrorModeDir
                + "/css/css.css");
        codeMirrorCssImport.add(servContext + pathToCodeMirrorModeDir + ""
                + "/clike/clike.css");
        codeMirrorCssImport.add(servContext + pathToCodeMirrorModeDir
                + "/javascript/javascript.css");
        codeMirrorCssImport.add(servContext + pathToCodeMirrorModeDir
                + "/xml/xml.css");

        langages.add(new SelectOption("clike", "clike", false));
        langages.add(new SelectOption("javascript", "javascript", false));
        langages.add(new SelectOption("css", "css", false));
        langages.add(new SelectOption("diff", "diff", false));
        langages.add(new SelectOption("haskell", "haskell", false));
        langages.add(new SelectOption("htmlmixed", "htmlmixed", false));
        langages.add(new SelectOption("stex", "stex", false));
        langages.add(new SelectOption("php", "php", false));
        langages.add(new SelectOption("xml", "xml", false));
    }

    public String modeForMime(String mime) {
        // TODO mfortun-2011-05-30 really implements this
        if (mime != null) {
            for (SelectOption opt : langages) {
                if (mime.endsWith(opt.value)) {
                    return opt.value;
                }

            }
        }
        return "javascript";
    }

    public String getPathToCodeMirorUiJs() {
        return servContext + pathToCodeMirorUi + "/js/";
    }

    public String getPathToCodeMirorUi() {
        return pathToCodeMirorUi;
    }

    public void setPathToCodeMirorUi(String pathToCodeMirorUi) {
        this.pathToCodeMirorUi = pathToCodeMirorUi;
    }

    public String getServContext() {
        return servContext;
    }

    public void setServContext(String servContext) {
        this.servContext = servContext;
    }

    public String getPathToCodeMirrorModeDir() {
        return pathToCodeMirrorModeDir;
    }

    public void setPathToCodeMirrorModeDir(String pathToCodeMirrorModeDir) {
        this.pathToCodeMirrorModeDir = pathToCodeMirrorModeDir;
    }

    public List<String> getCodeMirrorCssImport() {
        return codeMirrorCssImport;
    }

    public void setCodeMirrorCssImport(List<String> codeMirrorCssImport) {
        this.codeMirrorCssImport = codeMirrorCssImport;
    }

    public List<String> getCodeMirrorScriptImport() {
        return codeMirrorScriptImport;
    }

    public void setCodeMirrorScriptImport(List<String> codeMirrorScriptImport) {
        this.codeMirrorScriptImport = codeMirrorScriptImport;
    }

    public List<SelectOption> getLangages() {
        return langages;
    }

    public void setLangages(List<SelectOption> langages) {
        this.langages = langages;
    }
    
    

}
