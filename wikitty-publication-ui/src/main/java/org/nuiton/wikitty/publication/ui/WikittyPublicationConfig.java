/*
 * #%L
 * Wikitty :: publication-ui
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.ArgumentsParserException;
import org.nuiton.wikitty.WikittyConfig;
import org.nuiton.wikitty.WikittyConfigOption;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static org.nuiton.i18n.I18n.t;

/**
 * Specific configurations for wikitty publication web apps. Used to get
 * application for named contextData.
 * 
 * @author mfortun
 * 
 */
public class WikittyPublicationConfig {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyPublicationConfig.class);

    static final public String CONFIG_FILE_KEY = "wikitty.publication.config.pattern";
    static final public String DEFAULT_FILE_COMPLEMENT = "default";
    /**
     * Used to store already instanciate application config, with contextData as
     * key
     */
    static protected Map<String, ApplicationConfig> instanceMap = new HashMap<String, ApplicationConfig>();

    private WikittyPublicationConfig() {

    }

    /**
     * return a new instance of application config for contextData if any
     * 
     * @param Context
     *            contextData
     * @return application config for contextData
     */
    static public ApplicationConfig getConfig(String Context) {
        return getConfig(null, null, Context);
    }

    /**
     * return the application config requested corresponding with the
     * configfileName loading default properties, and load properties for
     * contextData if any.
     * 
     * @param props
     *            properties
     * @param configFilename
     * @param wsContext
     *            the contextData
     * @param args
     *            passed threw main
     * @return application config requested
     */
    static public ApplicationConfig getConfig(Properties props,
            String configFilename, String wsContext, String... args) {

        ApplicationConfig conf = new ApplicationConfig(Option.class, null,
                props, configFilename);
        try {
            conf.parse();
        } catch (ArgumentsParserException e) {
            e.printStackTrace();
        }

        String patternConfigFilename = conf.getOption(CONFIG_FILE_KEY);

        // load default configuration for all wikitty service
        String filename = String.format(patternConfigFilename,
                DEFAULT_FILE_COMPLEMENT);
        log.info(String.format("Try to load config file '%s'", filename));
        ApplicationConfig wsConfigDefault;

        wsConfigDefault = WikittyConfig.getConfig(filename);

        // change just data dir with context path
        String dataDir = wsConfigDefault
                .getOption(WikittyConfigOption.WIKITTY_DATA_DIR.getKey());
        wsConfigDefault.setOption(
                WikittyConfigOption.WIKITTY_DATA_DIR.getKey(), dataDir
                        + File.separator + wsContext);

        // read specific configuration with default config as default properties
        filename = String.format(patternConfigFilename, wsContext);
        log.info(String.format("Try to load config file '%s'", filename));
        ApplicationConfig wsConfig = null;
        try {
            wsConfig = new ApplicationConfig(
                    wsConfigDefault.getFlatOptions(false));
            wsConfig.setConfigFileName(filename);
            wsConfig.parse(null);
        } catch (ArgumentsParserException eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't load Wikitty Publication configuration", eee);
            }
        }

        return wsConfig;
    }

    
    /**
     * return application config for contextData if any or new
     * instance of application config if not created
     * 
     * @param Context
     *            contextData
     * @return application config for contextData
     */
    static public ApplicationConfig getInstance(String context) {
        if (!instanceMap.containsKey(context)) {
            synchronized (WikittyPublicationConfig.class) {
                if (!instanceMap.containsKey(context)) {
                    instanceMap.put(context, getConfig(context));
                }
            }
        }
        return instanceMap.get(context);
    }

    public enum Option implements ApplicationConfig.OptionDef {
        CONFIG_FILE(ApplicationConfig.CONFIG_FILE_NAME,
                t("wikitty-publication.config.configFileName.description"),
                "wikitty-publication.properties", String.class, false, false);

        public final String key;
        public final String description;
        public String defaultValue;
        public final Class<?> type;
        public boolean isTransient;
        public boolean isFinal;

        Option(String key, String description, String defaultValue,
                Class<?> type, boolean isTransient, boolean isFinal) {
            this.key = key;
            this.description = description;
            this.defaultValue = defaultValue;
            this.type = type;
            this.isFinal = isFinal;
            this.isTransient = isTransient;
        }

        public String getKey() {
            return key;
        }

        public Class<?> getType() {
            return type;
        }

        public String getDescription() {
            return description;
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        public boolean isTransient() {
            return isTransient;
        }

        public boolean isFinal() {
            return isFinal;
        }

        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        public void setTransient(boolean isTransient) {
            this.isTransient = isTransient;
        }

        public void setFinal(boolean isFinal) {
            this.isFinal = isFinal;
        }
    }

}
