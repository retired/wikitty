/*
 * #%L
 * Wikitty :: publication-ui
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui.action;

import org.nuiton.wikitty.entities.Wikitty;

import com.opensymphony.xwork2.ActionContext;

/**
 * Class for fragment action used to show a wikitty in the view page
 * @author mfortun
 *
 */
public class PublicationActionRestoreWikitty extends PublicationBaseAction {

    /**
     * 
     */
    private static final long serialVersionUID = 279959059921233642L;
    protected String id;
    protected Wikitty wikitty;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Wikitty getWikitty() {
        return wikitty;
    }

    public void setWikitty(Wikitty wikitty) {
        this.wikitty = wikitty;
    }

    static public PublicationActionRestoreWikitty getAction() {
        return (PublicationActionRestoreWikitty) ActionContext.getContext()
                .get(CONTEXT_ACTION_KEY);
    }

    @Override
    public String execute() throws Exception {

        wikitty = getWikittyPublicationProxy().restore(id);

        return SUCCESS;
    }

}
