/*
 * #%L
 * Wikitty :: publication-ui
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui.action;

import com.opensymphony.xwork2.ActionContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.FieldType.TYPE;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.publication.entities.WikittyPubData;
import org.nuiton.wikitty.publication.entities.WikittyPubDataHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubText;
import org.nuiton.wikitty.publication.entities.WikittyPubTextHelper;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.operators.Element;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


/**
 * 
 * Java class for the edit action that allow to create and edit
 * wikitty.
 * 
 * @author mfortun
 *
 */
public class PublicationActionEdit extends PublicationBaseAction {

    /**
     * 
     */
    private static final long serialVersionUID = -590087371230933701L;
    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(PublicationActionEdit.class);
    protected Wikitty wikitty;

    protected HashMap<String, Object> wikittyFieldMap;

    protected WikittyProxy proxy;

    protected boolean isDelete;
    protected boolean isStore;
    protected String wikittyId = "";
    protected String version = "";

    protected File uploadedFile;

    protected String uploadFileName;

    protected String uploadContentType;

   

    static public PublicationActionEdit getAction() {
        return (PublicationActionEdit) ActionContext.getContext().get(
                CONTEXT_ACTION_KEY);
    }

    @Override
    public String execute() throws Exception {
        
        
        proxy = getWikittyPublicationProxy();
        Map<String, Object> param = ActionContext.getContext().getParameters();
        formatArgs(param);
        
        isDelete = param.containsKey("delete");
        isStore = param.containsKey("store");

        wikittyId = getArgument("id", "");
        version = getArgument("version", "");

        doAction();

        args = Element.ELT_ID + ":" + wikitty.getId();

        return SUCCESS;
    }

    public void doAction() {
        if (isDelete) {
            log.info("delete requested");
            log.debug(wikitty);
            
            // on nous demande supprimer le wikitty, on l'efface et on
            // affichera un wikitty vide
            String id = wikittyId;
            proxy.delete(id);
            
            // apres un effacement on reprend l'edition d'un tout nouveau
            // wikitty
            wikitty = new WikittyImpl();
            wikittyId = wikitty.getId();

        } else {

            List<String> argsString = new ArrayList<String>();
            String[] argsTab = StringUtil.split(args, SEPARATOR);

            Collections.addAll(argsString, argsTab);
            // recherche du Wikitty a editer ou creation d'un nouveau si
            // necessaire
            Criteria criteria = searchCriteria(argsString);
            //System.out.println(criteria);
            if (criteria == null) {

                wikitty = new WikittyImpl();
            } else {
                wikitty = proxy.findByCriteria(criteria);
            }

            // si on ne retrouve pas le wikitty, mais qu'il vient d'etre cree
            // pour l'edition, on recree un wikitty avec ce meme identifiant
            if (wikitty == null && wikittyId !=null) {
                // c'est un nouvel objet, il n'a pas encore ete sauve, mais on
                // veut le faire
                String id = wikittyId;
                wikitty = new WikittyImpl(id);
            }

            if (wikitty == null) {

                // si le wikitty est null, et qu'on etait pas en edition
                // cela signifie qu'on ne retrouve pas le wikitty a editer
                // on creer un nouveau wikitty vide que l'on editera
                wikitty = new WikittyImpl();
            } else {

                // on met a jour le wikitty avec les infos trouvees dans les
                // arguments

                // ajout des extensions deja existante si necessaire
                String extensions = getArgument("extensions", null);

                if (extensions != null) {
                    String[] exts = StringUtil.split(extensions
                            .replace("[", "").replace("]", ""), ",");
                    for (String extName : exts) {

                        WikittyExtension ext = proxy
                                .restoreExtensionLastVersion(extName.trim());
                        if (ext != null) {
                            wikitty.addExtension(ext);
                        }
                    }
                }

                // ajout de l'extension demande par l'utilisateur
                String extName = getArgument("newExtension", null);
                if (extName != null && !"".equals(extName)) {
                    WikittyExtension ext = proxy
                            .restoreExtensionLastVersion(extName);
                    if (ext != null) {
                        wikitty.addExtension(ext);
                    }
                }

                for (Map.Entry<String, Object> field : wikittyFieldMap
                        .entrySet()) {
                    String key = field.getKey();
                    Object value = null;
                    if (key.contains(WikittyUtil.FQ_FIELD_NAME_SEPARATOR)) {
                        String ext = WikittyExtension.extractExtensionName(key);
                        String fieldName = WikittyExtension
                                .extractFieldName(key);

                        if (wikitty.hasField(ext, fieldName)) {
                            if (!"true".equals(getArgument("isNull-" + key,
                                    "false"))) {
                                value = field.getValue();
                            }

                            FieldType extFieldType = wikitty.getExtension(ext)
                                    .getFieldType(fieldName);
                            // allow collection string to be parse and save
                            if (extFieldType.isCollection()
                                    && extFieldType.getType() == TYPE.STRING
                                    && value != null) {

                                String valueString = value.toString();

                                valueString = new String(valueString.substring(
                                        1, valueString.length() - 1));

                                Collection<String> list = new ArrayList<String>();

                                String[] valuesString = StringUtil.split(
                                        valueString, ",");

                                for (String element : valuesString) {
                                    list.add(element.trim());
                                }

                                value = list;
                            }

                            // TODO mfortun-2011-05-27 find a better way to do
                            // that ?

                            // if extension is pub data, and the field try to
                            // set the name check if value is null, if so try to
                            // fill the name field with the file name value, if
                            // null, exception when try to set
                            if (ext.equals(WikittyPubData.EXT_WIKITTYPUBDATA)
                                    && fieldName
                                            .equals(WikittyPubData.FIELD_WIKITTYPUBDATA_NAME)
                                    && value == null) {

                                String extension = "."
                                        + FileUtil.extension(uploadFileName);
                                String fileName = FileUtil.basename(
                                        uploadFileName, extension);

                                value = fileName;

                            }

                            wikitty.setField(ext, fieldName, value);

                            // si w est un WikittyPubData on essai de mettre a
                            // jour si besoin les champs mimetype et name
                            // et extension
                            if (ext.equals(WikittyPubData.EXT_WIKITTYPUBDATA)
                                    && fieldName
                                            .equals(WikittyPubData.FIELD_WIKITTYPUBDATA_CONTENT)) {


                                if (null == WikittyPubDataHelper
                                        .getMimeType(wikitty)) {
                                    WikittyPubDataHelper.setMimeType(wikitty,
                                            uploadContentType);
                                }
                            }
                        }
                    } 

                    

                }
                if (isStore) {
                    log.info("store requested");
                    log.debug(wikitty);
                    // on nous demande la sauvegarde
                    wikitty = proxy.store(wikitty);
                }

            }
            
            


        }


    }

    public void setFile(File upload) {
        uploadedFile = upload;
    }

    public String getFileContentType() {
        return uploadContentType;
    }

    public void setFileContentType(String uploadContentType) {
        this.uploadContentType = uploadContentType;
    }

    public String getFileFileName() {
        return uploadFileName;
    }

    public void setFileFileName(String uploadFileName) {
        this.uploadFileName = uploadFileName;
    }

    public Wikitty getWikitty() {
        return wikitty;
    }

    public void setWikitty(Wikitty wikitty) {
        this.wikitty = wikitty;
    }

    public String getPostUrl() {

        return args + ".action";
    }

    protected Map<String, Object> formatArgs(Map<String, Object> args) {
        wikittyFieldMap = new HashMap<String, Object>();
        for (Entry<String, Object> en : args.entrySet()) {

            if (en.getKey().startsWith("Wikitty")) {

                String value = "";

                if (en.getValue() instanceof String[]) {
                    for (String occu : (String[]) en.getValue()) {
                        value += occu;
                    }
                } else {
                    value = String.valueOf(en.getValue());
                }

                if ("BINARY".equals(value) && uploadedFile != null) {
                    try {
                        wikittyFieldMap.put(en.getKey(),
                                FileUtil.fileToByte(uploadedFile));
                        uploadedFile.deleteOnExit();
                    } catch (IOException e) {
                        // TODO mfortun-2011-08-16 Exception not really handled
                        if (log.isErrorEnabled()){
                            log.error(
                                    "Error while reading File uploaded as wikittyPubDataContent ",
                                    e);
                        }
                    }
                } else {
                    wikittyFieldMap.put(en.getKey(), value);
                }

            }

        }
        return wikittyFieldMap;
    }




    public String getMimeType (){
        if (wikitty.hasExtension(WikittyPubText.EXT_WIKITTYPUBTEXT)){
            return WikittyPubTextHelper.getMimeType(wikitty);
        }
        return "";
    }
    
  

}
