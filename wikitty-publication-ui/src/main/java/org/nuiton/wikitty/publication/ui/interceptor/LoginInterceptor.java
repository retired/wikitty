/*
 * #%L
 * bow
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui.interceptor;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.entities.WikittyUser;
import org.nuiton.wikitty.publication.ui.WikittyPublicationSession;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * Interceptor used to redirect a non-logged user if he tries to access a page
 * where logging is mandatory
 */
public class LoginInterceptor extends AbstractInterceptor {
    private static final long serialVersionUID = -7520186185205372272L;

    protected String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        String result = null;
        
        
        Map<String, Object> session = ActionContext.getContext().getSession();
       
        // get wikittypublication context
        String context = StringUtil.split(
                invocation.getProxy().getActionName(), "/")[0];
       
        // get the session relative to the context 
        WikittyPublicationSession pubSession = WikittyPublicationSession
                .getWikittyPublicationSession(session,context);
        WikittyUser user = pubSession.getUser();

        // Construct redirect url.
        HttpServletRequest request = ServletActionContext.getRequest();
        String redirect = "/"+  context+request.getContextPath() + error ;
        redirect += "?success="+request.getServletPath();

        // If the user isn't logged in
        if (user == null) {
            ServletActionContext.getResponse().sendRedirect(redirect);
           
        } else {
            result = invocation.invoke();
        }
        return result;
    }

}
