package org.nuiton.wikitty.publication.ui;

import com.opensymphony.xwork2.ActionInvocation;
import java.io.File;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.ServletDispatcherResult;
import org.nuiton.util.FileUtil;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyPublicationResult extends ServletDispatcherResult {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyPublicationResult.class);
    private static final long serialVersionUID = 1L;

    private static final String JSP_DIR = "/WEB-INF/dynamic-jsp/";

    @Override
    public void doExecute(String finalLocation, ActionInvocation invocation) throws Exception {
        // FIXME poussin 20110907 ecrire la jsp sur le disque au bonne endroit
        // modifier finalLocation en fonction de cet endroit
        HttpServletRequest request = ServletActionContext.getRequest();
        String path = request.getSession().getServletContext().getRealPath(JSP_DIR);
        File jspdir = new File(path);
        jspdir.mkdirs();
        File jsp = new File(jspdir, finalLocation + ".jsp");

        String content = "<%@taglib prefix='s' uri='/struts-tags'%><html><body>Hello<form><s:textfield label='First' key='first' name='first' /></form></body></html>";

        FileUtil.writeString(jsp, content);

        log.fatal(String.format("jsp file %s writed ", jsp));

        finalLocation = JSP_DIR + finalLocation + ".jsp";

        super.doExecute(finalLocation, invocation);
    }


}
