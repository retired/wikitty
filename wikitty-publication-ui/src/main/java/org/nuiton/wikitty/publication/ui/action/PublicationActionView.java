/*
 * #%L
 * Wikitty :: publication-ui
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui.action;

import com.opensymphony.xwork2.ActionContext;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.publication.ui.WikittyPublicationProxy;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * class for the view action to list wikitty and search
 * 
 * @author mfortun
 *
 */
public class PublicationActionView extends PublicationBaseAction {

    protected PagedResult<Wikitty> pagedResult;
    protected Wikitty wikittyResult;
    protected String searchRequest = "*";
    protected int first = 1;
    protected int end = 100;

    protected String id;

    protected String search = "";
    protected String next = "";
    protected String previous = "";

    public PagedResult<Wikitty> getPagedResult() {
        return pagedResult;
    }

    public void setPagedResult(PagedResult<Wikitty> pagedResult) {
        this.pagedResult = pagedResult;
    }

    public Wikitty getWikittyResult() {
        return wikittyResult;
    }

    public void setWikittyResult(Wikitty wikittyResult) {
        this.wikittyResult = wikittyResult;
    }

    public String getSearchRequest() {
        return searchRequest;
    }

    public void setSearchRequest(String searchRequest) {
        this.searchRequest = searchRequest;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    /**
     * 
     */
    private static final long serialVersionUID = -4927699544115089591L;

    static public PublicationActionView getAction() {
        return (PublicationActionView) ActionContext.getContext().get(
                CONTEXT_ACTION_KEY);
    }

    @Override
    public String execute() throws Exception {
        List<String> argsString = new ArrayList<String>();
        String[] argsTab = StringUtil.split(args, SEPARATOR);

        Collections.addAll(argsString, argsTab);

        Criteria criteriaWikitty = searchCriteria(argsString);

        WikittyPublicationProxy proxy = getWikittyPublicationProxy();

        wikittyResult = proxy.findByCriteria(criteriaWikitty);

        int delta = end - first;
        /*
         * if first=1 and end=5 so next values will be first=6 and end=10
         */
        if (!"".equals(next)) {
            first = end + 1;
            end = first + delta;
        }
        if (!"".equals(previous)) {
            end = first - 1;
            first = end - delta;
        }

        // due to how indexes are adjust end and first value have to be strictly
        // positive
        if (end <= 0) {
            end = 1;
        }

        if (first <= 0) {
            first = 1;
        }

        if ("".equals(searchRequest)) {
            searchRequest = "*";
        }

        // wikittyResult = getWikittyPublicationProxy().restore(id);
        Criteria criteria;
        if (contextApps != null) {
            criteria = Search.query().keyword(searchRequest)
                    .exteq(WikittyLabel.EXT_WIKITTYLABEL)
                    .sw(WikittyLabel.FQ_FIELD_WIKITTYLABEL_LABELS, contextApps)
                    .criteria();
        } else {
            // search wikitties
            criteria = Search.query().keyword(searchRequest).criteria();
        }

        // criteria starts index at 0 so adjust search indexes
        criteria.setFirstIndex(first - 1);
        criteria.setEndIndex(end - 1);

        pagedResult = getWikittyPublicationProxy().findAllByCriteria(criteria);

        return SUCCESS;
    }

}
