/*
 * #%L
 * Wikitty :: publication-ui
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui;

import java.util.Locale;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.i18n.init.I18nInitializer;
import org.nuiton.util.FileUtil;

/**
 * Class use to set general properties and initialize needed component 
 * for the wikitty publication web application
 * @author mfortun
 *
 */
public class ApplicationListener implements ServletContextListener {

    private static final Log log = LogFactory.getLog(ApplicationListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        /*
         * needed to ensure that fileUtils used inside execution is set with the
         * correct encoding
         */
        FileUtil.ENCODING = "UTF-8";

        if (log.isInfoEnabled()) {
            log.info("Initializing I18n...");
        }

        I18nInitializer initializer = new DefaultI18nInitializer(
                "wikitty-publication");

        initializer.setMissingKeyReturnNull(true);

        I18n.init(initializer, Locale.FRENCH);

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

}
