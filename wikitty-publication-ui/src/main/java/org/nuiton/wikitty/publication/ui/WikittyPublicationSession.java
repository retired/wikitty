/*
 * #%L
 * Wikitty :: publication-ui
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.entities.WikittyUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.Map;

/**
 * Object store inside session to store information relative to the current
 * user, proxy etc.
 * 
 * @author mfortun
 * 
 */
public class WikittyPublicationSession implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3092501094068386098L;

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyPublicationSession.class);

    static final protected String WIKITTY_PUBLICATION_SESSION_KEY = WikittyPublicationSession.class
            .getSimpleName();

    static final protected String WIKITTY_PUBLICATION_SESSION_SEP = "_";

    protected String securityToken = null;
    transient protected WikittyUser user;
    transient protected WikittyPublicationProxy proxy;
    transient protected String context;

    public WikittyPublicationSession(String context) {
        this.context = context;
        proxy = WikittyPublicationProxy.getInstance(null, context);
    }

    /**
     * remove wikittypublicationSession from the httpsession
     * 
     * @param session
     */
    static public void invalidate(Map<String, Object> session,
            String contextData) {
        session.remove(WIKITTY_PUBLICATION_SESSION_KEY
                + WIKITTY_PUBLICATION_SESSION_SEP + contextData);
    }

    /**
     * return the wikittySession store in the http request for the context
     * 
     * @param request
     *            the current request
     * @param context
     *            the current contextData
     * @return the wikittyserssion
     */
    static public WikittyPublicationSession getWikittyPublicationSession(
            HttpServletRequest request, String context) {
        HttpSession session = request.getSession();
        WikittyPublicationSession result = getWikittyPublicationSession(
                session, context);
        return result;
    }

    /**
     * return the wikittySession store in the http sessions for the context
     * 
     * @param httpSession
     *            the current sessions
     * @param context
     *            the current contextData
     * @return the wikittyserssion
     */
    static public WikittyPublicationSession getWikittyPublicationSession(
            HttpSession httpSession, String contextData) {

        /*
         * The session is store with a key that containt context Data name the
         * ensure to not erase a session
         */
        WikittyPublicationSession result = (WikittyPublicationSession) httpSession
                .getAttribute(WIKITTY_PUBLICATION_SESSION_KEY
                        + WIKITTY_PUBLICATION_SESSION_SEP + contextData);
        if (result == null
                || (result.getContext() != null && !result.getContext().equals(
                        contextData))) {
            result = new WikittyPublicationSession(contextData);
            httpSession.setAttribute(WIKITTY_PUBLICATION_SESSION_KEY
                    + WIKITTY_PUBLICATION_SESSION_SEP + contextData, result);
        }
        return result;
    }

    /**
     * return the wikittySession store in sessions Map for the context
     * 
     * @param session
     *            the current sessions Map
     * @param context
     *            the current contextData
     * @return the wikittyserssion
     */
    static public WikittyPublicationSession getWikittyPublicationSession(
            Map<String, Object> session, String contextData) {

        /*
         * The session is store with a key that containt context Data name the
         * ensure to not erase a session
         */

        WikittyPublicationSession result = (WikittyPublicationSession) session
                .get(WIKITTY_PUBLICATION_SESSION_KEY
                        + WIKITTY_PUBLICATION_SESSION_SEP + contextData);
        if (result == null
                || (result.getContext() != null && !result.getContext().equals(
                        contextData))) {
            result = new WikittyPublicationSession(contextData);
            session.put(WIKITTY_PUBLICATION_SESSION_KEY
                    + WIKITTY_PUBLICATION_SESSION_SEP + contextData, result);
        }
        return result;
    }

    public void login(String context, String login, String password) {
        WikittyPublicationProxy proxy = getProxy(context);
        proxy.login(login, password);
        securityToken = proxy.getSecurityToken();
        user = proxy.getUser();

    }

    public WikittyPublicationProxy getProxy(String context) {
        if (proxy == null) {
            proxy = WikittyPublicationProxy.getInstance(securityToken, context);
        }
        return proxy;

    }

    public WikittyUser getUser() {
        return user;
    }

    public void setUser(WikittyUser user) {
        this.user = user;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

}
