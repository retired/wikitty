/*
 * #%L
 * Wikitty :: publication-ui
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui.action;

import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.entities.WikittyUser;
import org.nuiton.wikitty.entities.WikittyUserImpl;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.Search;

/**
 * Class mapped to the registered action, it create a wikittyuser with the
 * information send by the form
 * 
 * @author mfortun
 * 
 */
public class PublicationActionRegister extends PublicationBaseAction {
    private static final long serialVersionUID = 2204772861770399542L;
    protected String login;
    protected String password;
    protected String repeatPassword;





    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    protected boolean alreadyRegistered() {
        boolean result = true;

        WikittyProxy proxy = getWikittyPublicationProxy();
        Criteria criteria = Search.query()
                .eq(WikittyUser.FQ_FIELD_WIKITTYUSER_LOGIN, login).criteria();

        // If the user doesn't already exist
        if (proxy.findByCriteria(WikittyUser.class, criteria) == null) {
            result = false;
        }

        return result;
    }

    /**
     * Registers the new user and sends an email to confirm registration
     */
    public String execute() {
        String result = INPUT;
        

        if (login != null) {
            login = login.trim();
            if (password != null) {
                if (!password.equals(repeatPassword)) {
                    // TODO mfortun-2011-05-13 handle error message in jsp
                } else {
                    String md5 = StringUtil.encodeMD5(password);

                    // If the email address isn't already used
                    if (!alreadyRegistered()) {
                        WikittyProxy proxy = getWikittyPublicationProxy();

                        WikittyUser user = new WikittyUserImpl();
                        user.setLogin(login);
                        user.setPassword(md5);

                        WikittyUser userLoged = proxy.store(user);
                        // check if wikitty still exist
                        if (userLoged == null) {
                            // TODO mfortun-2011-05-13 handle error message
                            // in jsp
                        } else {
                            // try to login with information
                            proxy.login(login, md5);
                            // if logged it works
                            if (proxy.getLoggedInUser() != null) {

                                getWikittyPublicationSession().setUser(
                                        proxy.getLoggedInUser());

                                result = SUCCESS;
                            }
                        }
                    }
                }
            }
        }
    

        return result;
    }
}
