/*
 * #%L
 * bow
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.publication.ui.action;

import org.nuiton.util.StringUtil;

import com.opensymphony.xwork2.ActionContext;

/**
 * Class used as an action login, call the login method on the proxy with
 * login/password pass threw the login form
 * 
 * @author mfortun
 * 
 */
public class PublicationActionLogin extends PublicationBaseAction {
    private static final long serialVersionUID = 6891064800288772246L;
    protected String login;
    protected String password;

    protected String error;
    protected String success;

    static public PublicationActionLogin getAction() {
        return (PublicationActionLogin) ActionContext.getContext().get(
                CONTEXT_ACTION_KEY);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    /**
     * Method executed when struts invoke it and the struts action mapped to
     * this class
     */
    public String execute() {
        String result = INPUT;

        if (login != null) {
            login = login.trim();

            if (password != null) {
                String md5 = StringUtil.encodeMD5(password);

                getWikittyPublicationSession().login(contextData, login,
                        md5);
                result = SUCCESS;

            }
        }

        return result;
    }

}
