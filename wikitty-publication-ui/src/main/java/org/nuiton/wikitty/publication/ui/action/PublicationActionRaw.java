/*
 * #%L
 * Wikitty :: publication-ui
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.publication.ui.action;

import com.opensymphony.xwork2.ActionContext;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.publication.ui.WikittyPublicationProxy;
import org.nuiton.wikitty.publication.entities.WikittyPubData;
import org.nuiton.wikitty.publication.entities.WikittyPubDataHelper;
import org.nuiton.wikitty.publication.entities.WikittyPubText;
import org.nuiton.wikitty.publication.entities.WikittyPubTextHelper;
import org.nuiton.wikitty.search.Criteria;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Java class for the raw action that return wikittyPub simply with their
 * Mime Type.
 * 
 * @author mfortun
 *
 */
public class PublicationActionRaw extends PublicationBaseAction implements
        ServletResponseAware {

    /**
     * 
     */
    private static final long serialVersionUID = -2570662656931216123L;
    protected String mimeType;
    protected HttpServletResponse response;
    protected InputStream inputStream;

    static public PublicationActionRaw getAction() {
        return (PublicationActionRaw) ActionContext.getContext().get(
                CONTEXT_ACTION_KEY);
    }

    @Override
    public String execute() throws Exception {

        List<String> argsString = new ArrayList<String>();
        String[] argsTab = StringUtil.split(args, SEPARATOR);

        Collections.addAll(argsString, argsTab);

        Criteria criteria = searchCriteria(argsString);

        WikittyPublicationProxy proxy = getWikittyPublicationProxy();

        Wikitty w = proxy.findByCriteria(criteria);

        // return wikittyPubWith their mime type
        if (w.hasExtension(WikittyPubData.EXT_WIKITTYPUBDATA)) {
            mimeType = WikittyPubDataHelper.getMimeType(w);
            inputStream = new ByteArrayInputStream(
                    WikittyPubDataHelper.getContent(w));

        } else if (w.hasExtension(WikittyPubText.EXT_WIKITTYPUBTEXT)) {
            mimeType = WikittyPubTextHelper.getMimeType(w);
            inputStream = new ByteArrayInputStream(WikittyPubTextHelper
                    .getContent(w).getBytes());
        }


        return SUCCESS;
    }


    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public void setServletResponse(HttpServletResponse arg0) {
        response = arg0;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

}
