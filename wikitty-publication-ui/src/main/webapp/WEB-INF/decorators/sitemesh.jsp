<!--
  #%L
  bow
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2011 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<!--DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"-->

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page"
      xmlns:decorator="http://www.opensymphony.com/sitemesh/decorator">
    <head>
        <title>Wikitty Publication : <decorator:title default="" /></title>
        <decorator:head />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta name="description" content="Bookmarks on the web" />
        <s:url var="temporaryXml" action="temporaryXml" />
        <s:url var="permanentXml" action="permanentXml" />
        <s:url var="favicon" value="/img/favicon.png" />
        <link rel="icon" type="image/png" href="${favicon}" />


        <sj:head/>


    </head>
    <body id="page-home">
        <div id="wrap">
            <div id="page">
                <%@include file="/WEB-INF/jsp/header.jsp" %>
                <div id="main">
                    <decorator:body />
                </div>
            </div>
        </div>
        <%@include file="/WEB-INF/jsp/footer.jsp" %>
    </body>
</html>
