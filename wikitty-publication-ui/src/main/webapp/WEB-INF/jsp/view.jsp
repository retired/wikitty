<!--
  #%L
  Wikitty :: publication
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 CodeLutin, Benjamin Poussin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<%-- 
    Document   : view
    Created on : 7 dï¿½c. 2010, 04:18:13
    Author     : poussin
--%>

<%@page import="org.nuiton.wikitty.publication.entities.WikittyPubDataHelper"%>
<%@page import="org.nuiton.wikitty.publication.entities.WikittyPubData"%>
<%@page import="org.nuiton.wikitty.publication.entities.WikittyPubTextHelper"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.nuiton.wikitty.publication.entities.WikittyPubText"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page
    import="org.nuiton.wikitty.publication.ui.action.PublicationActionView"%>
<%@page import="org.nuiton.util.StringUtil"%>
<%@page import="org.nuiton.wikitty.search.Criteria"%>
<%@page import="org.nuiton.wikitty.search.Search"%>
<%@page
    import="org.nuiton.wikitty.publication.ui.WikittyPublicationSession"%>
<%@page import="org.nuiton.wikitty.search.PagedResult"%>
<%@page import="org.nuiton.wikitty.entities.Wikitty"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<sj:head jquerytheme="default"/>

<%
    PublicationActionView action = PublicationActionView
                .getAction();

        Wikitty wikit = action.getWikittyResult();

        if (wikit != null) {
%>


<h1><%=action.getText("publication.view.title")%></h1>


<pre><%=StringEscapeUtils.escapeHtml(String
                                .valueOf(wikit))%></pre>
<%
    }
%>

<p>
     <s:set var="contextData"><%=action.getContextData()%></s:set> <s:url
            var="urledit" action="%{#contextData}/edit/" />
        <a href="${urledit}">New wikitty</a>
</p>



<div id="wikittyInfo">
    <img  src="/img/indicator.gif"
        alt="Loading..." style="display: none" />
</div>




<form action="view.action" method="post">
    <div>
        <textarea title="Search" name="searchRequest"><%=action.getSearchRequest()%></textarea>
    </div>
    <s:textfield label="First" key="first" name="first" />
    <s:textfield label="End" key="end" name="end" />
    <input type="submit" name="search" value="<%=action.getText("publication.view.search")%>" />
    
    <pre> <%=action.getText("publication.view.founds")%>: <%=action.getPagedResult().getNumFound()%></pre>
    
    <br/>     


    <table border="1">
        <tr>
            <td></td>
            <td>Wikitty Id</td>
            <td>Version</td>
            <td>Extensions</td>
            <td>WikittyPub* Name</td>
        </tr>



        <%
            int i = 0;
                    for (Wikitty w : action.getPagedResult().getAll()) {
        %>
        <tr>
            <td>
                    
            
            <s:set var="wid"><%=w.getId()%></s:set> 
            <s:url var="urledit" action="%{#contextData}/edit/elt_id:%{#wid}" /> 
            <a     href="${urledit}">Edit</a> <s:url var="urlraw"
                    action="%{#contextData}/raw/elt_id:%{#wid}" /> 
                <s:url var="urlFragment" action="%{#contextData}/wikittyInfo" namespace="/fragment">
            
                </s:url>
                    
                    
                <sj:a targets="wikittyInfo" href="%{#urlFragment}">  view</sj:a>
                    
                <%
                if (w.hasExtension(WikittyPubText.EXT_WIKITTYPUBTEXT)
                            || w.hasExtension(WikittyPubData.EXT_WIKITTYPUBDATA)) {
                %>    
                    <a
                href="${urlraw}">Raw</a> <s:url var="urleval"
                    action="%{#contextData}/eval/elt_id:%{#wid}" /> <a
                href="${urleval}">Eval</a>
                <%
                    }
                %>

            </td>
            <td><%=w.getId()%></td>
            <td><%=w.getVersion()%></td>
            <td><%=w.getExtensionNames()%></td>
            <td>
            <%
                String currentWikittyName = StringUtils.EMPTY;
                            if (w.hasExtension(WikittyPubText.EXT_WIKITTYPUBTEXT)) {
                                currentWikittyName = WikittyPubTextHelper
                                        .getName(w);
                            } else if (w
                                    .hasExtension(WikittyPubData.EXT_WIKITTYPUBDATA)) {
                                currentWikittyName = WikittyPubDataHelper
                                        .getName(w);
                            }
            %>
            <%=currentWikittyName%>
            </td>
        </tr>
        <%
            i++;
                    }
        %>



    </table>
    <input type="submit" name="previous" value="
    <%=action.getText("publication.view.previous")%>" /> <input
        type="submit" name="next" value="<%=action.getText("publication.view.next")%>" />
</form>


      
      