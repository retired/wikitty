<%--
  #%L
  Wikitty :: publication
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 CodeLutin, Benjamin Poussin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>
<%-- 
    Document   : edit
    Created on : 6 dï¿½c. 2010, 18:32:18
    Author     : poussin
--%>

<%@page import="org.nuiton.i18n.I18n"%>
<%@page import="org.nuiton.wikitty.publication.ui.CodeMirrorWrapper"%>
<%@page import="org.nuiton.wikitty.publication.ui.SelectOption"%>
<%@page import="org.nuiton.wikitty.publication.ui.action.PublicationActionEdit"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="org.nuiton.wikitty.entities.FieldType"%>
<%@page import="java.util.Collection"%>
<%@page import="org.nuiton.wikitty.WikittyProxy"%>
<%@page import="org.nuiton.wikitty.WikittyService"%>
<%@page import="org.nuiton.wikitty.entities.WikittyExtension"%>
<%@page import="org.nuiton.wikitty.entities.Wikitty"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%

PublicationActionEdit action = PublicationActionEdit.getAction();

Wikitty wikitty = action.getWikitty(); 

CodeMirrorWrapper wrapper = new CodeMirrorWrapper(request.getContextPath(),"/js/codemirror-ui/", "lib/CodeMirror-2.0/mode");

%>





<% for (String uri : wrapper.getCodeMirrorCssImport()) { %>
<link rel="stylesheet" href="<%=uri%>">
<% } %>

<% for (String uri : wrapper.getCodeMirrorScriptImport()) { %>
<script src="<%=uri%>"></script>
<% } %>



<style type="text/css">
.CodeMirror {
border-top: 1px solid black; 
border-bottom: 1px solid black;
border-left: 1px solid black;
border-right: 1px solid black;
}</style>

<h1>Edit <%=wikitty.getId()%></h1>
<p>
    <s:set var="localContext"><%=action.getContextData()%></s:set>
    <s:url var="urledit" action="%{#localContext}/view/" />
    <a href="${urledit}">View</a>
    <s:url var="urledit" action="%{#localContext}/edit/" />
    <a href="${urledit}">New Wikitty</a>
</p>

<pre><%=StringEscapeUtils.escapeHtml(String.valueOf(wikitty))%></pre>

<script type="text/javascript">
<!--
var uiCodeMiror="";

function changeModeBy(select){
    uiCodeMiror.mirror.setOption("mode",select.value);
    uiCodeMiror.reindent();
}

//-->
</script>

 <form class="edit" action="<%=action.getPostUrl()%>" method="post" enctype="multipart/form-data" >
    <input type="hidden" name="id" value="<%=wikitty.getId()%>" />
    <input type="hidden" name="version" value="<%=wikitty.getVersion()%>" />
    <input type="hidden" name="extensions" value="<%=wikitty.getExtensionNames()%>"/>
   <fieldset>
       <legend><span class="legend">Extensions</span></legend>
       Current extension: <%=wikitty.getExtensionNames()%>
    <%
   WikittyProxy proxy = action.getWikittyPublicationProxy();
   Collection<String> allExt = proxy.getAllExtensionIds();
   if (allExt != null && allExt.size() > 0) {
  %>

        <select name="newExtension" size="1">
  <option value="" selected="true"></option>
  <%
       for (String extId : allExt) {
           String extName = WikittyExtension.computeName(extId);
           if (!wikitty.hasExtension(extName)) {
  %>
  <option value="<%=extName%>"><%=extName%></option>
  <%
           }
       }
  %>
        </select>
  <%
  }
   %>

        <input type="submit" name="addExtension" value="<%=action.getText("publication.edit.addExtension")%>" />
   </fieldset>

   <%
   int index = 0;
   for (WikittyExtension ext : wikitty.getExtensions()) {
       String extName = ext.getName();
   %>
   <fieldset>
       <legend><span class="legend"><%=extName%></span></legend>
       <%
       for (String fieldName : ext.getFieldNames()) {
           FieldType fieldType = wikitty.getExtension(extName).getFieldType(fieldName);
  %>
    <div>
        <label for="<%=extName%>.<%=fieldName%>" tabindex="<%=++index%>"><span class="label"><%=fieldName%></span></label>
  <%
           switch(fieldType.getType()) {
               case BINARY:
  %>

         <input type="hidden" name="<%=extName%>.<%=fieldName%>" value="BINARY" /> 
         <input type="file" name="File" label ="File"/>

        
  <%
                   break;
               case BOOLEAN:
                   boolean valueBool = wikitty.getFieldAsBoolean(ext.getName(), fieldName);
                   String checked = valueBool?"checked='true'":"";
  %>
        <input type="checkbox" name="<%=extName%>.<%=fieldName%>" value="true" <%=checked%>/>
  <%
                   break;
               default:
                   Object valueObject = wikitty.getFieldAsObject(ext.getName(), fieldName);
                   String valueString = "";
                   String checkedNull = "";
                   if (valueObject != null) {
                       valueString = String.valueOf(valueObject);
                   } else {
                       checkedNull = "checked='true'";
                   }

                   valueString = StringEscapeUtils.escapeHtml(valueString);
                   if (valueString.contains("\n") || "true".equals(fieldType.getTagValue("multiline"))) {
  %>


<select name="langageSelection" onchange="changeModeBy(this)">
<% for(SelectOption otpion  : wrapper.getLangages()) { %>
<option value="<%=otpion.getValue()%>" 
<%=otpion.getValue().
equalsIgnoreCase(wrapper.modeForMime(action.getMimeType()))?"selected":""%>
><%=otpion.getDesc()%></option>
<% } %>
</select>


        <textarea id="<%=extName%>.<%=fieldName%>" cols="80" rows="20" name="<%=extName%>.<%=fieldName%>"><%=valueString%></textarea>
          <script type="text/javascript">
<!--

var textarea = document.getElementById("<%=extName%>.<%=fieldName%>");

var codeMirrorOptions = {
        lineNumbers: true,
        matchBrackets: true,
        mode: "<%=wrapper.modeForMime(action.getMimeType())%>"
      };

var uiOptions = { path : '<%=wrapper.getPathToCodeMirorUiJs()%>', searchMode : 'popup' };

uiCodeMiror = new CodeMirrorUI(textarea,uiOptions,codeMirrorOptions);
//-->
</script>
  <%
                   } else {
  %>
        <input type="text" name="<%=extName%>.<%=fieldName%>" value="<%=valueString%>" />
  <%
                   }
  %>
        <input type="checkbox" name="isNull-<%=extName%>.<%=fieldName%>" value="true" <%=checkedNull%>/>(null)
  <%
           }
  %>
        <%=fieldType.toDefinition("")%>
    </div>
  <%
       }
  %>
  </fieldset>
  <%
   }
  %>

  <input type="submit" name="store" value="<%=action.getText("publication.edit.save") %>" />
  <input type="submit" name="delete" value="<%=action.getText("publication.edit.delete") %>" />
 </form>