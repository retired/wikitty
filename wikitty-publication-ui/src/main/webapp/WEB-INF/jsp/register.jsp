<%--
  #%L
  Wikitty :: publication-ui
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2011 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<div id="content">
    <div id="formFrame" class="fond">
        <h1>
            <s:text name="Register" />
        </h1>
        <s:url var="urlRegister" action="register"  namespace="/%{contextData}" />
        
        <s:form action="%{urlRegister}">
            <s:actionerror />
            <p>
                <s:set id="publication.login.login">
                    <s:text name="publication.login.login" />
                </s:set>
                <s:set id="publication.login.password">
                    <s:text name="publication.login.password" />
                </s:set>
                <s:set id="publication.login.repeatPassword">
                    <s:text name="publication.login.repeatPassword" />
                </s:set>
                <s:set id="publication.register.submit">
                    <s:text name="publication.register.submit" />
                </s:set>

                <s:textfield key="publication.login.login" name="login"
                    label="%{publication.login.login}" labelposition="top"
                    labelSeparator=" :" />
                <s:password key="publication.login.password" name="password"
                    label="%{publication.login.password}" labelposition="top"
                    labelSeparator=" :" />
                <s:password key="publication.login.repeatPassword"
                    label="%{publication.login.repeatPassword}" name="repeatPassword"
                    labelposition="top" labelSeparator=" :" />
                <s:submit key="publication.register.submit" name="submit"
                    value="%{publication.register.submit}" />
            </p>
        </s:form>
        <s:a action="login_input" id="loginLink">
            <s:text name="login" />
        </s:a>
    </div>

</div>