<%--
  #%L
  Wikitty :: publication
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 CodeLutin, Benjamin Poussin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>
<%-- 
    Document   : header
    Created on : 6 déc. 2010, 19:21:06
    Author     : poussin
--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<div id="header"
     xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
     xmlns:jsp="http://java.sun.com/JSP/Page">

  
    <div id="msg">
        <span id="actionmessageHeader">
            <s:actionmessage />
        </span>
        <span id="actionerrorHeader">
            <s:actionerror />
        </span>
    </div>
</div>
    