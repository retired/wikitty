<%--
  #%L
  Wikitty :: publication-ui
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2011 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>
<%@page import="org.nuiton.wikitty.publication.action.ui.PublicationActionLogin"%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<%
    String successValue = request.getParameter("success");
    String errorValue = request.getParameter("error");

   
    
    
    if (successValue == null || successValue == "") {
        successValue =  PublicationActionLogin.getAction().getContext()+"/view/.action";
    }

    //TODO mfortun-2011-05-13 find how to have default value configured by struts
    if (errorValue == null || errorValue == "") {
        errorValue = "login_input.action" + "?success="
                + request.getParameter("success");
    }
%>

        <s:url var="urlLogin" action="login" namespace="/%{contextData}" />
<s:form action="%{urlLogin}">
    <p>
        <s:set id="publication.login.login">
            <s:text name="publication.login.login" />
        </s:set>
        <s:set id="publication.login.password">
            <s:text name="publication.login.password" />
        </s:set>
        <s:set id="publication.login.submit">
            <s:text name="publication.login.submit" />
        </s:set>

        <input type="hidden" name="success" value="<%=successValue%>" /> <input
            type="hidden" name="error" value="<%=errorValue%>" />
        <s:textfield key="login" name="login" labelposition="top"
            labelSeparator=" :" label="%{publication.login.login}" />
        <br /> <br />
        <s:password key="password" name="password" labelposition="top"
            labelSeparator=" :" label="%{publication.login.password}" />
        <br /> <br />
        <s:submit key="publication.login.submit" name="submit"
            value="%{publication.login.submit}" />
    </p>
</s:form>