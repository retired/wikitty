/*
 * #%L
 * Wikitty :: publication-ui
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
function waitForStyles() {
  for (var i = 0; i < document.styleSheets.length; i++)
    if (/googleapis/.test(document.styleSheets[i].href))
      return document.body.className += " droid";
  setTimeout(waitForStyles, 100);
}
setTimeout(function() {
  if (/AppleWebKit/.test(navigator.userAgent) && /iP[oa]d|iPhone/.test(navigator.userAgent)) return;
  var link = document.createElement("LINK");
  link.type = "text/css";
  link.rel = "stylesheet";
  link.href = "http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans:bold";
  document.documentElement.getElementsByTagName("HEAD")[0].appendChild(link);
  waitForStyles();
}, 10);
