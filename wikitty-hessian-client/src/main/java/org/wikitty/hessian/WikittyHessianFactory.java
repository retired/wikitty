/*
 * #%L
 * Wikitty :: hessian client
 * %%
 * Copyright (C) 2010 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.wikitty.hessian;

import java.net.MalformedURLException;

import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyService;

import com.caucho.hessian.client.HessianProxyFactory;

/**
 * Wikitty hessian client factory.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class WikittyHessianFactory {

    /**
     * Get hessian wikitty service proxy on remove endpoint.
     * 
     * @param serviceEndpoint service endpoint (full url)
     * @return hessian proxy implementing {@code WikittyService}
     * @throws WikittyException if hessian proxy can't be created
     */
    public static WikittyService getWikittyService(String serviceEndpoint) throws WikittyException {
        return getService(WikittyService.class, serviceEndpoint);
    }

    /**
     * Get hessian service proxy on remove endpoint.
     * 
     * @param <S> service type
     * @param serviceInterface service interface
     * @param serviceEndpoint service endpoint (full url)
     * @return hessian proxy implementing {@code S}
     * @throws WikittyException if hessian proxy can't be created
     */
    public static <S> S getService(Class<S> serviceInterface, String serviceEndpoint) throws WikittyException {
        
        HessianProxyFactory factory = new HessianProxyFactory();
        // Fix : com.caucho.hessian.io.HessianProtocolException: '���' is an unknown code
        factory.setHessian2Request(true);
        // pour que les méthodes aux noms dupliquées fonctionnent (arguments different)
        factory.setOverloadEnabled(true);

        // service proxy
        S hessianProxy;
        try {
            hessianProxy = (S)factory.create(serviceInterface, serviceEndpoint);
        } catch (MalformedURLException eee) {
            throw new WikittyException("Can't create hessian proxy", eee);
        }

        return hessianProxy;
    }
}
