/*
 * #%L
 * Wikitty :: hessian client
 * %%
 * Copyright (C) 2010 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.wikitty.hessian.WikittyHessianFactory;

/**
 * This classe override all modification method (store, delete, clear) to send
 * modification to master. All read method are directly done localy.
 *
 * Slave is listener of master event and synchronize local data when event are
 * received
 *
 * Configuration:
 * <ul>
 * <li> master service url</li>
 * <li> master service xmpp room</li>
 * <li> sync state file</li>
 * <li> sync state intervale</li>
 * </ul>
 *
 * Il n'est pas necessaire que l'on sache exactement quel est le dernier event
 * jouer. Car on peut rejouer plusieurs fois l'event sans qu'il y ait de
 * probleme. Il faut juste que l'on ne loupe pas d'event.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceSlave
        extends WikittyServiceDelegator implements PacketListener {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceSlave.class);

    // Keep room addresse and pseudo to verify that messages event are not send by us
    protected String room;
    protected String pseudo;

    protected WikittyService master;
    protected ApplicationConfig config;

    protected long lastState = -1;

    protected RandomAccessFile syncStateFile = null;
    protected int syncIntervale = 300;
    protected long lastStateSaveDate = -1;

    public WikittyServiceSlave(ApplicationConfig config, WikittyService ws) {
        super(ws);
        this.config = config;

        File file = config.getOptionAsFile(WikittyConfigOption.
                WIKITTY_SLAVE_SYNC_STATE_FILE.getKey());
        if (!file.exists()) {
            throw new WikittyException(String.format(
                    "File state %s doesn't exists. You must initialise data"
                    + " correctly and create state file after that before used"
                    + " WikittyServiceSlave", file));
        }
        try {
            syncStateFile = new RandomAccessFile(file, "rwd"); // d to force sync
        } catch (FileNotFoundException eee) {
            throw new WikittyException("Can't create file to store slave state", eee);
        }

        syncIntervale = config.getOptionAsInt(WikittyConfigOption.
                WIKITTY_SLAVE_SYNC_STATE_INTERVALE.getKey());

        String masterURL = config.getOption(WikittyConfigOption.
                WIKITTY_SLAVE_MASTER_URL.getKey());

        // creation du WikittyService master
        master =  WikittyHessianFactory.getWikittyService(masterURL);

        // enregistrement en tant que listener sur le master
        initXMPP(config);
    }

    protected long getLastState() {
        if (lastState == -1) {
            try {
                // read state from file
                syncStateFile.seek(0);
                lastState = syncStateFile.readLong();
            } catch (IOException eee) {
                throw new WikittyException("Can't read state file", eee);
            }
        }
        return lastState;
    }

    protected void setLastState(long date) {
        lastState = date;
        // save state in file if necessary
        long now = System.currentTimeMillis();
        if (lastStateSaveDate + syncIntervale < now) {
            try {
                // write state to file
                syncStateFile.seek(0);
                syncStateFile.writeLong(lastState);
                lastStateSaveDate = now;
            } catch (IOException eee) {
                log.error("Can't write state file", eee);
            }
        }
    }

    /**
     * TODO poussin 20101117 reutilise le transporter plutot que de dupliquer le code.
     * Le probleme est de passer de la meilleur facon possible le xmpp server et la room
     * qui ne sont pas donnes par les memes cles de config. Il y a aussi l'historique
     * qu'il faut recuperer ici alors que dans le transporter qui est utilise pour les
     * client, il n'y a pas besoin d'historique. Le but est de pouvoir creer facilement
     * de nouveau transporter sans devoir modifier cette classe.
     *
     * Si persistent est vrai alors il faut toujours utilise le meme user id
     *
     * @param config
     */
    protected void initXMPP(ApplicationConfig config) {
        String server = config.getOption(WikittyConfigOption.
                WIKITTY_SLAVE_MASTER_XMPP_SERVER.getKey());

        // Keep them to verify that is not us notifications
        room = config.getOption(WikittyConfigOption.
                WIKITTY_SLAVE_MASTER_XMPP_ROOM.getKey());
        pseudo = WikittyUtil.getUniqueLoginName();
        try {
            if (log.isInfoEnabled()) {
                log.info("Try to connect to xmpp serveur " + server +
                     " with pseudo " + pseudo + " in room " + room);
            }
            XMPPConnection connection = new XMPPConnection(server);
            connection.connect();
            connection.loginAnonymously();

            DiscussionHistory history = new DiscussionHistory();
            // MUC must be archived
            long lastDate = getLastState();
            Date date = new Date(lastDate);
            history.setSince(date);
            
            // connection to the volatile room
            MultiUserChat muc = new MultiUserChat(connection, room);
            muc.join(pseudo, "", history, 4000);
            muc.addMessageListener(this);
        } catch (Exception eee) {
            throw new WikittyException("Can't connect to xmpp serveur", eee);
        }
    }
    /**
     * used for MUC message
     * @param packet
     */
    @Override
    public void processPacket(Packet packet) {

        // Dont listen own events
        String name = room + "/" + pseudo;
        if (!name.equals(packet.getFrom())) {

            Object event = packet.getProperty(XMPPNotifierTransporter.PROPERTY_EVENT_NAME);

            if (log.isDebugEnabled()) {
                log.debug("Receive message : " + event);
            }

            if (event instanceof WikittyEvent) {
                processRemoteEvent((WikittyEvent)event);
            }
        }
    }

    protected void processRemoteEvent(WikittyEvent event) {
        // rejoue l'event en local
        replay(null, Collections.singletonList(event), true);

        // marque cet event comme etant le dernier recu et rejouer
        setLastState(event.getTime());
    }

    //////////////////////////////////////////////////////
    //
    // W I K I T T Y   S E R V I C E   M E T H O D S
    //      method that must be done on master
    //////////////////////////////////////////////////////

    @Override
    public String login(String login, String password) {
        return master.login(login, password);
    }

    @Override
    public void logout(String securityToken) {
        master.logout(securityToken);
    }

    @Override
    public WikittyEvent store(String securityToken, Collection<Wikitty> wikitties, boolean force) {
        return master.store(securityToken, wikitties, force);
    }

    @Override
    public WikittyEvent storeExtension(String securityToken, Collection<WikittyExtension> exts) {
        return master.storeExtension(securityToken, exts);
    }

    @Override
    public WikittyEvent delete(String securityToken, Collection<String> ids) {
        return master.delete(securityToken, ids);
    }

    @Override
    public WikittyEvent deleteExtension(String securityToken, Collection<String> extNames) {
        return master.deleteExtension(securityToken, extNames);
    }

    @Override
    public WikittyEvent deleteTree(String securityToken, String wikittyId) {
        return master.deleteTree(securityToken, wikittyId);
    }

    @Override
    public WikittyEvent clear(String securityToken) {
        return master.clear(securityToken);
    }

}
