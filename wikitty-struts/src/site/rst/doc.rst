.. -
.. * #%L
.. * Wikitty :: struts
.. * %%
.. * Copyright (C) 2011 - 2012 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
=========================
Documentation Utilisateur
=========================
:Authors: Manoël Fortun

La taglib wikitty-struts est une tag lib pour facilité la création de page jsp
dans le cadre d'application web s'appuyant du un système de stockage wikitty.

Il y a deux possibilité d'utilisation pour cette tag lib, dans les deux cas il
faut importer les tags avec l'instruction:

* <%@ taglib prefix="ws" uri="/wikitty-struts" %>

Elle s'utilise ensuite normalement comme un taglib en fonction de ses besoins.

La TagLib fournit donc deux utilisations possibles, l'une pour l'édition 
complète d'un wikitty et l'autre pour l'intégration aisée de champ d'un wikitty
au sein d'un formulaire.

Les tags dans les deux types d'utilisations sont basiquement les mêmes, la mise
en place différe. 


--------------------
Edition d'un wikitty
--------------------

bases
+++++

Avec la tag lib on peut créer des formulaires d'édition de wikitty, la tag lib
fournit même le corps d'une action, abstraite, avec une seule méthode à 
implémenter, la méthode "getProxy()", qui doit permettre d'obtenir le 
wikittyProxy de l'application courante. Ensuite via un mapping classique
struts avec le formulaire généré (en lui donnant évidement l'adresse de l'action
mappée par struts) l'action remplira le wikitty avec les valeurs passées dans
le formulaire.

La création du formulaire passe par l'utilisation du tag **ws:form** :

* id, l'id que possédera le formulaire html généré
* name, le nom que possédera le formulaire html généré 
* wikitty, requit si entityBusiness non renseigné, wikitty objet de l'édition et donc du formulaire
* entityBusiness, requit si wikitty non renseigné, entityBusiness objet de l'édition et donc du formulaire
* proxy, wikitty proxy, requis
* action, action cible du formulaire
* redirect, champ caché pouvant être utilisé par l'action pour une redirection
* include, gestion des champs à afficher dans le formulaire voir plus bas
* exclude, gestion des champs à afficher dans le formulaire voir plus bas
* order, permet d'ordonner l'affichage des champs dans le formulaire 
* orderBefore flag vrai/faux pour savoir si l'ordonancement des champs passe avant la redéfinition, voir plus bas
* allowDelete flag vrai/faux pour savoir si l'on positionne un bouton de suppresion du wikitty


Sélection des champs
++++++++++++++++++++

La sélection des champs à afficher se fait grâce aux attributs **include** et
**exclude** du tag **ws:form**, par défaut si ils ne sont pas remplit, tout les
champs du wikitty seront inclus dans le formulaire.

Ensuite quoi qu'il advienne l'exclusion est prioritaire sur toute inclusion, 
si un champ apparait dans l'include et dans l'exclude, l'exclude sera 
prioritaire et le champ n'apparaitra pas.

L'inclusion exclu ceux qui ne sont pas nommé, si le champ include est renseigné
seulement les champs renseignés (sauf ceux expressément excluts par l'attribut
exclude), les autres seront simplement ignoré, puisque non inclus.

Pour nommer les champs et les inclures ou exclures il suffit de les lister
par leur nom complet (fully qualified name): "extension.champ"; on peut aussi
exclure ou inclure une extension compléte en remplaçant le nom du champ par 
le caractère "*" soit : "extension.\*". Pour lister les champs il suffit
juste des les séparer par des virgules.

Par exemple:

exclude="wikittyPubData.*" 
include="WikittyPubData.mimeType, WikittyPubText.name, WikittyPubtext.content"

Le champ wikittyPubData.mimeType ne sera pas inclus car il a été exclus par
l'exclusion de l'extension compléte, et donc seulement les champs name et content
de l'extension WikittyPubText seront inclus, même si le wikitty en possède 
d'autre ils seront ignoré, mais évidement préservé d'une éventuel modification
par le formulaire.

Le formulaire couplé à l'action ne modifie à la sauvegarde que les champs 
exposés dans le formulaire.


Redéfinition de l'affichage
+++++++++++++++++++++++++++

Par défaut en utilisant le tag **ws:form** les champs seront affichés comme
des simples champs texte, voir champs texte multilignes pour certain. La
redéfinition permet de forcer l'affichage des champs comme on pourrait le 
souhaiter, et pour celà à l'intérieur du tag **ws:form** on utilise les autres 
tag.

La redéfinition ne change rien à la politique d'inclusion/exclusion, un champ
non inclus/exclus même redéfini ne sera pas affiché.

Ces champs ont en commun certains attributs :

* id, id pour l'élément html correspondant généré
* name, name pour l'élément html correspondant généré
* label, label associé
* fqFieldName, requis, fully qualified name correspondant au champ redéfini

Les autres attributs sont spécifique à l'utilisation ou au type d'affichage
et seront présentés en fin de ce document.


Ordre d'affichage
+++++++++++++++++

La définition de l'ordre d'apparition des champs peut se spécifier simplement, 
par défaut l'ordre sera l'ordre rencontré dans le wikitty. Pour spéficier 
l'ordre on remplit l'attribut order comme on le ferait pour les attributs 
include ou exclude. 

Il y a aussi le flag orderBefore, qui permet de définir si l'on doit ordonner
les champs avant leurs redéfinitions, en ce cas seulement les champs spécifiés 
dans l'attribut **order**, ensuite les redéfinitions seront écrit, et enfin
si il reste des champs à écrire ils seront écrit.

Si l'orderBefore est faux, on écrit d'abord les redéfinitions, puis les champs
ordonnés dans l'order, et enfin les champs non traité. Un champ ne peut pas être
présent deux fois dans le formulaire, si il a été écrit avant la redéfinition
la redéfinition n'est pas prise en compte.


------------------
Champ d'un wikitty
------------------

La tag lib peut s'utiliser donc dans le cadre de création de formulaire 
quelconques s'appuyant sur des wikitty. On doit donc pouvoir intégrer les 
valeurs d'un champs wikitty simplement, sous la forme que l'on souhaite dans un
formulaire, un formulaire pas forcément d'édition de wikitty.

Pour se faire on se ressert simplement des tags de la taglib mais sans les 
inclurent dans un tag **ws:form**. 

Par contre cette utilisation nécessite de remplir des attributs supplémentaire
en plus de ceux génériques, ou ceux spécifiques à certain tag.

* businessEntity, nécessaire si attribut wikitty non renseigné, businessEntity cible
* wikitty, nécessaire si attribut businessEntity non renseigné, wikitty cible
* proxy, pas pour tous, mais nécessaire pour certain


--------------
Liste des tags
--------------

On reprendra ici les tag avec leurs fonctions et leurs champs spécifiques,
pour les champs communs se référer au reste du documents, ou à la TLD.

Commun
++++++

* ws:hidden permet d'insérer le champ en tant que champs caché
* ws:boolean permet d'insérer le champ en tant que checkbox 
* ws:textArea permet d'insérer le champ en tant que textArea
* ws:textField permet d'insérer le champ en tant que textField
* ws:date permet d'avoir un composant inteligent pour les dates **NON FONCTIONNEL**
* ws:selectFixed permet d'afficher un combo box avec des valeurs fixées à l'avance, sera sélectionnée celle correspondant au champs wikitty lié

Attributs spécifiques :

   * fixvalues, liste des valeurs à afficher dans le combobox, valeurs séparées par des virgules

* ws:selectCriteria permet d'afficher un combox box avec des wikitty, sera selectionnée le wikitty correspondant à l'id du wikitty du champ(utilisé donc dans le cadre de relation entre wikitty)

Attributs spécifiques :

   * descField, le champ wikitty à afficher dans la liste par défaut l'id
   * criteria, le criteria de recherche des wikitty

Spécifique à l'utilisation formulaire
+++++++++++++++++++++++++++++++++++++

* ws:form tag pour la création d'un formulaire d'édition des wikitty 

Spécifique à l'utilisation champ
++++++++++++++++++++++++++++++++

* ws:selectAssociation tag pour l'affichage de champ wikitty de type collection

Attributs spécifiques :

   * size taille du select si 1 l'objet html sera un combobox, sinon une liste de sélection 
   * multiple flag vrai/faux pour autoriser ou non la sélection multiple
   * descField, champ du wikitty à afficher dans la liste, dans le cas ou le champ est une collection de wikitty

* ws:select tag pour l'affichage de collection de wikitty en tant que list ou combobox html

Attributs spécifiques :

   * wikittyValues, collection de de wikitty à afficher (
   * entityValues, collection de d'entityBusiness à afficher 
   * size, taille du select si 1 l'objet html sera un combobox, sinon une liste de sélection
   * descField, champ du wikitty à afficher dans la liste

