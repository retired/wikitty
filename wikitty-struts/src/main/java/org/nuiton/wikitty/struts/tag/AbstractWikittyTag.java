/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts.tag;

import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.entities.BusinessEntity;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.struts.component.AbstractWikittyComponent;


public abstract class AbstractWikittyTag extends AbstractWikittyClosingTag {

    /**
     * 
     */
    private static final long serialVersionUID = -2804975141932256704L;
    protected Wikitty wikitty;
    protected BusinessEntity businessEntity;
    protected WikittyProxy proxy;
    protected String wikittyId;

    public AbstractWikittyTag() {
        super();
    }

    public String getWikittyId() {
        return wikittyId;
    }

    public void setWikittyId(String wikittyId) {
        this.wikittyId = wikittyId;
    }

    public Wikitty getWikitty() {
        return wikitty;
    }

    public void setWikitty(Wikitty wikitty) {
        this.wikitty = wikitty;
    }

    public BusinessEntity getBusinessEntity() {
        return businessEntity;
    }

    public void setBusinessEntity(BusinessEntity businessEntity) {
        this.businessEntity = businessEntity;
    }

    public WikittyProxy getProxy() {
        return proxy;
    }

    public void setProxy(WikittyProxy proxy) {
        this.proxy = proxy;
    }

    
    @Override
    protected void populateParams() {
        super.populateParams();
        AbstractWikittyComponent wikittyComponent = (AbstractWikittyComponent) component;
        wikittyComponent.setWikittyId(wikittyId);
        wikittyComponent.setWikitty(wikitty);
        wikittyComponent.setProxy(proxy);
        wikittyComponent.setBusinessEntity(businessEntity);
    }
    
    
}
