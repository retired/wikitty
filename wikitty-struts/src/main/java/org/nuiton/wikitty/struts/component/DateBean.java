/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.views.annotations.StrutsTag;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.WikittyUtil;

import com.opensymphony.xwork2.util.ValueStack;

@StrutsTag(name = "boolean", tldTagClass = "org.nuiton.wikitty.struts.tag.DateTag",
        description = "", allowDynamicAttributes = false)
public class DateBean extends AbstractWikittyComponentBean {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(DateBean.class);

    public static final String OPEN_TEMPLATE = "ws-date";

    protected Boolean enableHour = false;

    public DateBean(ValueStack stack, HttpServletRequest request,
            HttpServletResponse response) {
        super(stack, request, response);

    }

    @Override
    protected void evaluateExtraParams() {
        super.evaluateExtraParams();

        // if field defined as inclued
        // parse extname and field name
        String[] fieldsAccess = StringUtil.split(fqFieldName,
                WikittyUtil.FQ_FIELD_NAME_SEPARATOR);
        // add parametters to be use by the template

        Object val = getWikitty().getFieldAsDate(fieldsAccess[0],
                fieldsAccess[1]);
        val = val == null ? "" : val;

        addParameter("value", val);
        if (enableHour != null) {
            addParameter("enableHour", enableHour);
        }

    }

    @Override
    public String getDefaultOpenTemplate() {
        return OPEN_TEMPLATE;
    }

    @Override
    protected String getDefaultTemplate() {
        return EMPTY_TEMPLATE;
    }

    public Boolean getEnableHour() {
        return enableHour;
    }

    public void setEnableHour(Boolean enableHour) {
        this.enableHour = enableHour;
    }

}
