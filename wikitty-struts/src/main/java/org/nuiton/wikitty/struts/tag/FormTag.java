/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.components.Component;
import org.nuiton.wikitty.struts.component.FormTagBean;
import com.opensymphony.xwork2.util.ValueStack;

public class FormTag extends AbstractWikittyTag {

    /**
     * 
     */
    private static final long serialVersionUID = 258152544560583399L;
    protected String action;
    protected String redirect;
    protected String include;
    protected String exclude;
    protected String order;
    protected boolean allowDelete;
    protected boolean orderBefore;

    public boolean isOrderBefore() {
        return orderBefore;
    }

    public void setOrderBefore(boolean orderBefore) {
        this.orderBefore = orderBefore;
    }

    public String getInclude() {
        return include;
    }

    public void setInclude(String include) {
        this.include = include;
    }

    public String getExclude() {
        return exclude;
    }

    public void setExclude(String exclude) {
        this.exclude = exclude;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public boolean isAllowDelete() {
        return allowDelete;
    }

    public void setAllowDelete(boolean allowDelete) {
        this.allowDelete = allowDelete;
    }

    @Override
    public Component getBean(ValueStack stack, HttpServletRequest req,
            HttpServletResponse res) {
        return new FormTagBean(stack, req, res);

    }

    @Override
    protected void populateParams() {
        super.populateParams();
        FormTagBean wikittyTag = ((FormTagBean) component);
        wikittyTag.setAction(action);
        wikittyTag.setRedirect(redirect);
        wikittyTag.setOrder(order);
        wikittyTag.setOrderBefore(orderBefore);
        wikittyTag.setAllowDelete(allowDelete);
        wikittyTag.setInclude(include);
        wikittyTag.setExclude(exclude);
    }

    
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

}
