/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyTypes;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public abstract class AbstractWikittyEditAction extends ActionSupport {

    private static final long serialVersionUID = 1959245739866183821L;
    
    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(AbstractWikittyEditAction.class);

    public static final String PREFIX_WIKITTY = "Wikitty";

    /**
     * rajouter les attributs par défaut id tout ça
     * et ensuite on rajoute les méthodes de parsage des arguments
     * et qui les rajoute dans les wikitty field tout ça.
     * sauf les file parce que les files compliqué.
     */
    @Override
    public String execute() throws Exception {
        // restore basic element to know what to do        
        String id = getArgument("id", EMPTY);
        String redirect = getArgument("redirect", EMPTY);
        
        String delete =  getArgument("delete", EMPTY);
        String store =  getArgument("store", EMPTY);

        WikittyProxy proxy = getProxy();
        
        if (!StringUtils.isEmpty(delete)) {
            proxy.delete(id);

        } else if (!StringUtils.isEmpty(store)) {
            Map<String, Object> param = ActionContext.getContext().getParameters();
            Map<String, Object> wikittyFieldMap = formatArgs(param);

            Wikitty wikitty = proxy.restore(id);
            if (wikitty != null) {

                for (Entry<String, Object> entry : wikittyFieldMap.entrySet()){
                    //wikitty.setFqField(entry.getKey(), entry.getValue());

                    FieldType ftype = wikitty.getFieldType(entry.getKey());

                    Object value = entry.getValue();

                    // Patch to handle string collection
                    if (ftype.isCollection()
                            && ftype.getType() == WikittyTypes.STRING
                            && value != null) {

                        String valueString = value.toString();

                        valueString = new String(valueString.substring(
                                1, valueString.length() - 1));

                        Collection<String> list = new ArrayList<String>();

                        String[] valuesString = StringUtil.split(
                                valueString, ",");

                        for (String element : valuesString) {
                            list.add(element.trim());
                        }

                        value = list;
                    }
                    wikitty.setFqField(entry.getKey(), value);
                }
            }
            proxy.store(wikitty);

        }
        return SUCCESS;
    }
    
    public abstract WikittyProxy getProxy();

    protected Map<String, Object> formatArgs(Map<String, Object> args) {
        Map<String, Object> wikittyFieldMap = new HashMap<String, Object>();
        for (Entry<String, Object> en : args.entrySet()) {

            if (en.getKey().startsWith(PREFIX_WIKITTY)) {

                String value = EMPTY;

                if (en.getValue() instanceof String[]) {
                    for (String occu : (String[]) en.getValue()) {
                        value += occu;
                    }
                } else {
                    value = String.valueOf(en.getValue());
                }
                wikittyFieldMap.put(en.getKey(), value);
            }
        }
        return wikittyFieldMap;
    }
    
    
    public String getArgument(String key, String defaultValue) {
        Object temp = ActionContext.getContext().getParameters().get(key);
        // same method used inside wikitty publication base action
        String result = EMPTY;
        // TODO mfortun-2011-06-29 fix this with something clean
        if (temp == null) {
            result = defaultValue;
        } else {
            if (temp instanceof String[]) {
                for (String t : (String[]) temp) {
                    result += t;
                }
            } else {
                result = temp.toString();
            }
        }
        return result;
    }
}
