/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.BusinessEntity;
import org.nuiton.wikitty.entities.BusinessEntityImpl;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;

public class WikittyFieldHandler {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(WikittyFieldHandler.class);

    static public String ALL_EXTENSION_CAR = "*";
    static public String FIELD_SEPARATOR = ",";

    protected String wikittyId;
    protected Wikitty wikitty;
    protected BusinessEntity businessEntity;
    protected WikittyProxy proxy;
    protected String include = StringUtils.EMPTY;
    protected String exclude = StringUtils.EMPTY;
    protected String order = StringUtils.EMPTY;

    protected Boolean orderBefore = false;
    protected Set<String> fieldAdded;

    protected Map<String, List<String>> excludeMap;
    protected Map<String, List<String>> includeMap;
    // if include is enable it change how to construct result
    protected boolean includeEnable = false;

    public static final String WIKITTY_STACK_KEY = "wikitty_key";

    public Set<String> getFieldAdded() {
        return fieldAdded;
    }

    public void setFieldAdded(Set<String> fieldAdded) {
        this.fieldAdded = fieldAdded;
    }

    public WikittyFieldHandler() {
        fieldAdded = new HashSet<String>();
    }

    public String getWikittyId() {
        return wikittyId;
    }

    public void setWikittyId(String wikittyId) {
        this.wikittyId = wikittyId;
    }

    public Wikitty getWikitty() {
        if (wikitty == null) {
            if (StringUtils.isNotEmpty(wikittyId)) {
                wikitty = getProxy().restore(wikittyId);
            } else {
                wikitty = ((BusinessEntityImpl) getBusinessEntity()).getWikitty();
            }
        }
        return wikitty;
    }

    public void setWikitty(Wikitty wikitty) {
        this.wikitty = wikitty;
    }

    public BusinessEntity getBusinessEntity() {
        return businessEntity;
    }

    public void setBusinessEntity(BusinessEntity businessEntity) {
        this.businessEntity = businessEntity;
    }

    public Boolean getOrderBefore() {
        return orderBefore;
    }

    public void setOrderBefore(Boolean orderBefore) {
        this.orderBefore = orderBefore;
    }

    public WikittyProxy getProxy() {
        return proxy;
    }

    public void setProxy(WikittyProxy proxy) {
        this.proxy = proxy;
    }

    public String getInclude() {
        return include;
    }

    public void setInclude(String include) {
        this.include = include;
        // recalculate include map
        // delegate construction of map
        includeMap = constructIncludeExcludeMap(include);
        includeEnable = includeMap.size() != 0;

    }

    public String getExclude() {
        return exclude;
    }

    public void setExclude(String exclude) {
        this.exclude = exclude;
        // recalculate exclude map
        // delegate construction of map
        excludeMap = constructIncludeExcludeMap(exclude);
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    /*
     * Method called two times by wikitty tag
     */
    public Collection<ExtensionFieldStrutsBean> getWikittyField() {

        // if order before selected and none field add, prepare the field list
        // with another method
        if (orderBefore && (fieldAdded.size() == 0)) {
            return getFieldOrderedBefore();
        }

        Map<String, ExtensionFieldStrutsBean> mapField = new HashMap<String, ExtensionFieldStrutsBean>();

        /*
         * if no include defined, by default will select all wikitty field
         * except ones which are exclude
         * 
         * excluded fields are more important than included, if a field is
         * included and excluded, it will not appear in the result
         */

        for (WikittyExtension ext : getWikitty().getExtensions()) {
            String extName = ext.getName();

            /*
             * if extension excluded or not include while include is enable jump
             * to the next extension
             */
            if (!isIncluded(extName, ALL_EXTENSION_CAR)) {
                continue;
            }

            for (String fieldName : ext.getFieldNames()) {

                /*
                 * if field is excluded or field not included while include mode
                 * jump to the next field or if field allready added to the page
                 */
                if (!isIncluded(extName, fieldName)) {
                    continue;
                }

                // delegate construction of the element
                ExtensionFieldStrutsBean temp = constructExtensionFieldFromField(
                        extName, fieldName);

                // add element to the map
                log.debug("Add field : " + temp);
                mapField.put(temp.getName(), temp);
            }
        }

        String[] fieldOrder = StringUtil.split(order, FIELD_SEPARATOR);

        Collection<ExtensionFieldStrutsBean> result = mapField.values();

        /*
         * if an order is define parse order and construct ordering result if
         * all the field are not defined in the order just add the rest at the
         * end of the result list.
         */
        if (fieldOrder.length != 0 && !orderBefore) {

            List<ExtensionFieldStrutsBean> orderedResult = new LinkedList<ExtensionFieldStrutsBean>();

            // fieldname have the classic fq field name format like:
            // wikittyextension.name
            for (String orderIt : fieldOrder) {

                if (mapField.containsKey(orderIt)) {
                    orderedResult.add(mapField.remove(orderIt));
                }

            }

            orderedResult.addAll(mapField.values());

            result = orderedResult;
        }

        return result;
    }

    protected Collection<ExtensionFieldStrutsBean> getFieldOrderedBefore() {

        List<ExtensionFieldStrutsBean> result = new LinkedList<ExtensionFieldStrutsBean>();

        // hack TODO mfortun-2011-06-30 need to think again this orderBefore
        fieldAdded.add("DUMMY");

        log.debug("orderBefore enable with" + order);

        String[] fieldOrder = StringUtil.split(order, FIELD_SEPARATOR);

        /*
         * this method is used because ordering before is different. We parse
         * the list of the element we want ordered. If we see a * that mean all
         * the field have to be ordered now then add those field to the added
         * field
         */

        if (fieldOrder.length != 0) {
            for (String fieldit : fieldOrder) {

                if (!isIncluded(fieldit)) {
                    continue;
                }
                String extname = WikittyUtil
                        .getExtensionNameFromFQFieldName(fieldit);
                String field = WikittyUtil.getFieldNameFromFQFieldName(fieldit);
                // harvest all field of the extension
                if ("*".equals(field)) {

                    WikittyExtension ext = getWikitty().getExtension(extname);
                    for (String fieldNameIt : ext.getFieldNames()) {
                        ExtensionFieldStrutsBean temp = constructExtensionFieldFromField(
                                extname, fieldNameIt);
                        result.add(temp);
                        fieldAdded.add(temp.getName());
                    }

                } else {
                    ExtensionFieldStrutsBean temp = constructExtensionFieldFromField(
                            extname, field);
                    result.add(temp);
                    fieldAdded.add(temp.getName());
                }

            }
        }

        return result;
    }

    /**
     * construct map for exclude or include template
     * 
     * @param entry
     *            the attribut of the wikitty field tag exemple
     *            include="machin.*, truc.bob, truc.tt"
     * @return a map with key the string before the dot and a list with the
     *         value after the dot example: [machin:[*], truc:[tt,bob]]
     */
    protected Map<String, List<String>> constructIncludeExcludeMap(String entry) {

        String[] entryTab = StringUtil.split(entry, FIELD_SEPARATOR);

        Map<String, List<String>> result = new HashMap<String, List<String>>();

        // prepare list of excluded field and extension
        for (String excludeIt : entryTab) {
            // remove space from list of field
            excludeIt=excludeIt.trim();
            String[] extfield = StringUtil.split(excludeIt,
                    WikittyUtil.FQ_FIELD_NAME_SEPARATOR);
            if (extfield.length == 2) {
                List<String> listFields = result.get(extfield[0]);
                if (listFields == null) {
                    listFields = new LinkedList<String>();
                }
                listFields.add(extfield[1]);
                result.put(extfield[0], listFields);

            } else {
                // TODO mfortun-2011-06-24 exception
            }

        }
        return result;
    }

    public void addAddedField(String field) {
        log.debug("Field added : " + field);
        fieldAdded.add(field);
    }

    public boolean isIncluded(String fqFieldName) {

        String extName = WikittyUtil.getExtensionNameFromFQFieldName(fqFieldName);
        String fieldName = WikittyUtil.getFieldNameFromFQFieldName(fqFieldName);

        return this.isIncluded(extName, fieldName);
    }

    public boolean isIncluded(String extName, String fieldName) {

        // check if field is specifically exluded or if all the extention is
        boolean excluded = excludeMap.containsKey(extName)
                && (excludeMap.get(extName).contains(ALL_EXTENSION_CAR) || excludeMap
                        .get(extName).contains(fieldName));

               
        // check if field is specifically included or if all the extention is
        boolean notIncluded = includeEnable && ! ( includeMap.containsKey(extName) && ((includeMap
                .get(extName).contains(ALL_EXTENSION_CAR) || includeMap
                .get(extName).contains(fieldName))));
       
        // check if field allready added
        boolean added = fieldAdded.contains(extName
                + WikittyUtil.FQ_FIELD_NAME_SEPARATOR + fieldName);

        log.debug("extension: " + extName + "." + fieldName
                + " - notInclude:" + notIncluded + " exclude:" + excluded
                + " alreadyAdd:" + added);

        if (excluded || notIncluded || added) {
            return false;
        }

        return true;
    }

    protected ExtensionFieldStrutsBean constructExtensionFieldFromField(
            String extName, String fieldName) {

        ExtensionFieldStrutsBean result = new ExtensionFieldStrutsBean();

        FieldType fieldType = wikitty.getExtension(extName).getFieldType(
                fieldName);

        result.setName(extName + WikittyUtil.FQ_FIELD_NAME_SEPARATOR
                + fieldName);
        result.setLabel(result.getName());
        result.setValue(StringUtils.EMPTY);

        // set the field type.
        switch (fieldType.getType()) {
        case BINARY:
            result.setType("file");

            break;
        case BOOLEAN:
            boolean valueBool = getWikitty().getFieldAsBoolean(extName, fieldName);
            result.setType("boolean");
            result.setValue(valueBool);

            break;
        default:
            Object valueObject = getWikitty().getFieldAsObject(extName, fieldName);
            String valueString = StringUtils.EMPTY;

            if (valueObject != null) {
                valueString = String.valueOf(valueObject);
            }

            result.setValue(valueString);

            valueString = StringEscapeUtils.escapeHtml4(valueString);
            if (valueString.contains("\n")
                    || "true".equals(fieldType.getTagValue("multiline"))) {
                result.setType("textarea");

            } else {
                result.setType("textfield");

            }

        }
        return result;
    }
}
