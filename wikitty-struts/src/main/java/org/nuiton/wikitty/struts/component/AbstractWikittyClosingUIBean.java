/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.ClosingUIBean;

import com.opensymphony.xwork2.util.ValueStack;

public abstract class AbstractWikittyClosingUIBean extends ClosingUIBean {

    protected static final String EMPTY_TEMPLATE = "ws-empty";

    public AbstractWikittyClosingUIBean(ValueStack stack,
            HttpServletRequest request, HttpServletResponse response) {
        super(stack, request, response);
    }

    
    @Override
    protected void evaluateExtraParams() {
        super.evaluateExtraParams();
        addParameter("theme", getTheme());
        
    }
    
    @Override
    public String getTheme() {
        return "wikitty";
    }

}
