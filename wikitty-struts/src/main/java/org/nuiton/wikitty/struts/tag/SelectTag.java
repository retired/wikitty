/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts.tag;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.nuiton.wikitty.entities.BusinessEntity;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.struts.component.SelectBean;

import com.opensymphony.xwork2.util.ValueStack;

public class SelectTag extends AbstractWikittyClosingTag {

    protected Integer size;
    protected String descField;
    protected Collection<Wikitty> wikittyValues;
    protected Collection<? extends BusinessEntity> entityValues;

    /**
     * 
     */
    private static final long serialVersionUID = -3652505449437874066L;

    @Override
    public Component getBean(ValueStack stack, HttpServletRequest req,
            HttpServletResponse res) {
        return new SelectBean(stack, req, res);

    }

    /*
     * <ws:select name="userId" size="5" values="<%=action.getPartners()%>"
     * nameField="<%=VradiUser.FQ_FIELD_WIKITTYUSER_LOGIN%>"
     * descField="<%=VradiUser.FQ_FIELD_VRADIUSER_INFO%>"/>
     */
    @Override
    protected void populateParams() {
        super.populateParams();

        SelectBean select = (SelectBean) component;
        select.setSize(size);
        select.setDescField(descField);
        select.setEntityValues(entityValues);
        select.setWikittyValues(wikittyValues);

    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getDescField() {
        return descField;
    }

    public void setDescField(String descField) {
        this.descField = descField;
    }

    public Collection<Wikitty> getWikittyValues() {
        return wikittyValues;
    }

    public void setWikittyValues(Collection<Wikitty> wikittyValues) {
        this.wikittyValues = wikittyValues;
    }

    public Collection<? extends BusinessEntity> getEntityValues() {
        return entityValues;
    }

    public void setEntityValues(Collection<? extends BusinessEntity> entityValues) {
        this.entityValues = entityValues;
    }

}
