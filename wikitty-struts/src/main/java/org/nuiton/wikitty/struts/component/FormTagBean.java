/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.components.ClosingUIBean;
import org.apache.struts2.views.annotations.StrutsTag;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.struts.TagUseException;
import org.nuiton.wikitty.struts.WikittyFieldHandler;

import com.opensymphony.xwork2.util.ValueStack;

@StrutsTag(name = "Wikitty", tldTagClass = "org.nuiton.wikitty.struts.tag.FormTag", description = "", allowDynamicAttributes = false)
public class FormTagBean extends AbstractWikittyComponent {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(FormTagBean.class);

    public static final String OPEN_TEMPLATE = "ws-form";
    public static final String TEMPLATE = "ws-form-close";

    protected String action;
    protected String redirect;

    protected String include;
    protected String exclude;

    protected String order;
    protected Boolean orderBefore;

    protected Boolean allowDelete;

    public FormTagBean(ValueStack stack, HttpServletRequest request,
            HttpServletResponse response) {
        super(stack, request, response);
    }

    @Override
    protected void evaluateExtraParams() {
        super.evaluateExtraParams();

        if (wikitty == null && businessEntity == null) {
            log.info("wikitty and businessEntity not declared");

            throw new TagUseException(
                    "Tag must declare a valid Wikitty or businessEntity attribute");
        }

        /*
         * check for for fqfieldname list if respect regex 
         */
        
        if (include.length() != 0 && !include.matches(REGEX_LIST_FQFIELDNAME)) {
            log.debug("Include list set: " + include + " expected match: "
                    + REGEX_WIKITTY_FQFIELDNAME);
            throw new TagUseException(
                    "Include list field must be set with correct value: "
                            + REGEX_LIST_FQFIELDNAME);

        }
        
        if (order.length() != 0 && !order.matches(REGEX_LIST_FQFIELDNAME)) {
            log.debug("Order list set: " + order + " expected match: "
                    + REGEX_WIKITTY_FQFIELDNAME);
            throw new TagUseException(
                    "Order list field must be set with correct value: "
                            + REGEX_LIST_FQFIELDNAME);

        }

        if (exclude.length() != 0 && !exclude.matches(REGEX_LIST_FQFIELDNAME)) {
            log.debug("Exclude list set: " + exclude + " expected match: "
                    + REGEX_WIKITTY_FQFIELDNAME);
            throw new TagUseException(
                    "Exclude list field must be set with correct value: "
                            + REGEX_LIST_FQFIELDNAME);

        }

        /*
         * this methode is called two times: - first when the wikitty open tag
         * is red - second when the wikitty closing tag is red
         * 
         * for the first called this create an object wikittyfieldhandler that
         * will be store inside the stack and used by the included tag ( inside
         * the wikitty tags to store)
         */

        if (action != null) {
            addParameter("action", findString(action));
        }

        if (redirect != null) {
            addParameter("redirect", findString(redirect));
        }

        if (allowDelete != null) {
            addParameter("allowDelete", allowDelete);
        }

        if (orderBefore != null) {
            addParameter("orderBefore", orderBefore);
        }

        if (name == null || name.equals(StringUtils.EMPTY)) {
            name = "wikitty-form-" + getWikitty().getId();

        }

        addParameter("wikittyid", findString(getWikitty().getId()));
        // no uses finally:
        // addParameter("wikittyversion", findString(wikitty.getVersion()));
        // addParameter("wikittyextensions", findString(wikitty
        // .getExtensionNames().toString()));

        addParameter("name", name);

        Object temp = stack.getContext().get(
                WikittyFieldHandler.WIKITTY_STACK_KEY);
        WikittyFieldHandler handler;
        if (temp == null) {

            // construct wikitty field handler withh
            // all required param and put it in the stack
            handler = new WikittyFieldHandler();

            log.info(handler + " Added to the stack");

            stack.getContext().put(WikittyFieldHandler.WIKITTY_STACK_KEY,
                    handler);

            handler.setExclude(exclude);
            handler.setInclude(include);
            handler.setOrder(order);
            handler.setWikitty(getWikitty());
            handler.setProxy(proxy);
            handler.setOrderBefore(orderBefore);

        } else {
            // when the tag is closing remove the handler from the stack
            stack.getContext().remove(WikittyFieldHandler.WIKITTY_STACK_KEY);

            handler = (WikittyFieldHandler) temp;
        }
        // add field that have to be write inside the page
        log.info("add wikitty fields to the parametters");
        addParameter("wikittyfields", handler.getWikittyField());

    }

    public String getDefaultOpenTemplate() {
        return OPEN_TEMPLATE;
    }

    protected String getDefaultTemplate() {
        return TEMPLATE;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getInclude() {
        return include;
    }

    /**
     * set fqFieldName include list, value trimed
     * 
     * @param include
     */
    public void setInclude(String include) {
        this.include = include.trim();
    }

    public String getExclude() {
        return exclude;
    }

    /**
     * set fqfieldName exclude list, value trimed
     * 
     * @param exclude
     */
    public void setExclude(String exclude) {
        this.exclude = exclude.trim();
    }

    public String getOrder() {
        return order;
    }

    /**
     * set fqfieldName order list, value trimed
     * 
     * @param order
     */
    public void setOrder(String order) {
        this.order = order.trim();
    }

    public boolean isAllowDelete() {
        return allowDelete;
    }

    public void setAllowDelete(boolean allowDelete) {
        this.allowDelete = allowDelete;
    }

    public Boolean getOrderBefore() {
        return orderBefore;
    }

    public void setOrderBefore(Boolean orderBefore) {
        this.orderBefore = orderBefore;
    }

    public Boolean getAllowDelete() {
        return allowDelete;
    }

    public void setAllowDelete(Boolean allowDelete) {
        this.allowDelete = allowDelete;
    }

}
