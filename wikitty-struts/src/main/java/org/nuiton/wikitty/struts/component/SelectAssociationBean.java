/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts.component;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.views.annotations.StrutsTag;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.struts.Option;
import org.nuiton.wikitty.struts.WikittyFieldHandler;

import com.opensymphony.xwork2.util.ValueStack;


@StrutsTag(name = "boolean", tldTagClass = "org.nuiton.wikitty.struts.tag.SelectAssociationTag",
        description = "", allowDynamicAttributes = false)
public class SelectAssociationBean extends AbstractWikittyComponentBean {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory
            .getLog(SelectAssociationBean.class);

    public static final String OPEN_TEMPLATE = "ws-selectassociation";

    protected String descField;
    protected int size;
    protected Boolean multiple;

    public SelectAssociationBean(ValueStack stack, HttpServletRequest request,
            HttpServletResponse response) {
        super(stack, request, response);
    }

    
    @Override
    protected void evaluateExtraParams() {
        super.evaluateExtraParams();
        
        // the select constructed must be positive
        if (size <= 0 ){
            size = 1;
        }
        
        // add parameters
        addParameter("selectSize", size);

        if (multiple == null) {
            multiple = false;
        }
        addParameter("multiple", multiple);
        
        List<Option> listOption = new LinkedList<Option>();
        
        String ext = WikittyUtil.getExtensionNameFromFQFieldName(fqFieldName);
        String fieldName = WikittyUtil.getFieldNameFromFQFieldName(fqFieldName);
        
        String[] descriptor = StringUtil.split(descField,
                WikittyFieldHandler.FIELD_SEPARATOR);
        
        // iterate wikitty for result construction
        for (String id : getWikitty().getFieldAsList(ext, fieldName, String.class)){
            Wikitty wiki = proxy.restore(id);

            Option temp = new Option();
            temp.setValeur(wiki.getId());
            String desc = wiki.getId();
            
            // if attribute descripteur fill with something
            // extract from wikitty corresponding field value
            if (descriptor.length != 0) {
                desc = "";
                for (String descriptorIt : descriptor) {
                    String[] descTable = StringUtil
                            .split(descriptorIt, WikittyUtil.FQ_FIELD_NAME_SEPARATOR);

                    desc += wiki.getFieldAsWikitty(descTable[0],
                            descTable[1]);
                }
            }
            temp.setDescription(desc);
            listOption.add(temp);

        }
        
        addParameter("value", listOption);
    }
    
    
    public String getDescField() {
        return descField;
    }

    public void setDescField(String descField) {
        this.descField = descField;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getDefaultOpenTemplate() {
        return OPEN_TEMPLATE;
    }

    protected String getDefaultTemplate() {
        return EMPTY_TEMPLATE;
    }

}
