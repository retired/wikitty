/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts.component;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.views.annotations.StrutsTag;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.BusinessEntity;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.struts.Option;
import org.nuiton.wikitty.struts.TagUseException;
import org.nuiton.wikitty.struts.WikittyFieldHandler;

import com.opensymphony.xwork2.util.ValueStack;
@StrutsTag(name = "boolean", tldTagClass = "org.nuiton.wikitty.struts.tag.SelectTag",
        description = "", allowDynamicAttributes = false)
public class SelectBean extends AbstractWikittyClosingUIBean {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory
            .getLog(SelectBean.class);

    public static final String OPEN_TEMPLATE = "ws-select";

    protected Integer size;
    protected String descField;
    protected Collection<Wikitty> wikittyValues;
    protected Collection<? extends BusinessEntity> entityValues;

  

    public SelectBean(ValueStack stack, HttpServletRequest request,
            HttpServletResponse response) {
        super(stack, request, response);
    }

    @Override
    protected void evaluateExtraParams() {
        super.evaluateExtraParams();
        
        if (wikittyValues == null && entityValues == null){
            log.info("wikittyValues and entityValues null");
            throw new TagUseException("WikittyValues or entityValues must be define");
        }
        
        
        if (size == null || size <=0 ){
            size = 1;
        }
        
        addParameter("selectSize", size);
        
        List<Option> resultList = new LinkedList<Option>();
        
        String[] descriptor = StringUtil.split(descField,
                WikittyFieldHandler.FIELD_SEPARATOR);
        
        
        if (wikittyValues!=null){
            for (Wikitty wiki : wikittyValues){
                Option temp = new Option();
                temp.setValeur(wiki.getId());
                
                String desc = wiki.getId();
                
                // if attribute descripteur fill with something
                // extract from wikitty corresponding field value
                if (descriptor.length != 0) {
                    desc = "";
                    for (String descriptorIt : descriptor) {
                        String[] descTable = StringUtil
                                .split(descriptorIt, WikittyUtil.FQ_FIELD_NAME_SEPARATOR);

                        desc += wiki.getFieldAsWikitty(descTable[0],
                                descTable[1]);
                    }
                }
                temp.setDescription(desc);
                
                
                resultList.add(temp);
            }
        }
        
        
        if (entityValues!=null){
            for(BusinessEntity busi: entityValues){
                Option temp = new Option();
                temp.setValeur(busi.getWikittyId());
                String desc = busi.getWikittyId();
                
                // if attribute descripteur fill with something
                // extract from wikitty corresponding field value
                if (descriptor.length != 0) {
                    desc = "";
                    for (String descriptorIt : descriptor) {
                        String[] descTable = StringUtil
                                .split(descriptorIt, WikittyUtil.FQ_FIELD_NAME_SEPARATOR);

                        desc += busi.getField(descTable[0],
                                descTable[1]);
                    }
                }
                temp.setDescription(desc);
                
                resultList.add(temp);
            }
        }

        addParameter("value", resultList);

    }

    @Override
    public String getDefaultOpenTemplate() {
        return OPEN_TEMPLATE;

    }

    @Override
    protected String getDefaultTemplate() {
        return EMPTY_TEMPLATE;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getDescField() {
        return descField;
    }

    public void setDescField(String descField) {
        this.descField = descField;
    }

    public Collection<Wikitty> getWikittyValues() {
        return wikittyValues;
    }

    public void setWikittyValues(Collection<Wikitty> wikittyValues) {
        this.wikittyValues = wikittyValues;
    }

    public Collection<? extends BusinessEntity> getEntityValues() {
        return entityValues;
    }

    public void setEntityValues(Collection<? extends BusinessEntity> entityValues) {
        this.entityValues = entityValues;
    }
}
