/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.BusinessEntity;
import org.nuiton.wikitty.entities.BusinessEntityImpl;
import org.nuiton.wikitty.entities.Wikitty;

import com.opensymphony.xwork2.util.ValueStack;

public abstract class AbstractWikittyComponent extends
        AbstractWikittyClosingUIBean {

    protected String wikittyId;
    /**
     * Never used directly this attribute use the getter
     */
    protected Wikitty wikitty;
    /**
     * Never used directly this attribute use the getter
     */
    protected BusinessEntity businessEntity;
    /**
     * Never used directly this attribute use the getter
     */
    protected WikittyProxy proxy;

    static public String REGEX_EMPTY = " *";
    static public String REGEX_FIELD_SEP = ",";
    static public String REGEX_FIELD_JOKER="\\*";
    
    
    /**
     * Regex to check that field name are correct
     * " *\w+\.\w+ *"
     */
    static public String REGEX_WIKITTY_FQFIELDNAME = REGEX_EMPTY
            + WikittyUtil.extensionNamePattern
            + WikittyUtil.FQ_FIELD_NAME_SEPARATOR_REGEX
            + WikittyUtil.extensionNamePattern + REGEX_EMPTY;

    /**
     * regex to check joker field and fqfield name for field name
     * " *\w+\.(\w+|\*) *"
     */
    static public String REGEX_WIKITTY_FQFIELDNAME_EXT_JOKER = REGEX_EMPTY
    + WikittyUtil.extensionNamePattern
    + WikittyUtil.FQ_FIELD_NAME_SEPARATOR_REGEX
    + WikittyUtil.extensionNamePattern+"(|"+REGEX_FIELD_JOKER+")" + REGEX_EMPTY;
    
    /**
     * regex to check if field are list correctly
     * " *\w+\.\w+|\* *(, *\w+\.\w+|\* *)*"
     */
    static public String REGEX_LIST_FQFIELDNAME = REGEX_WIKITTY_FQFIELDNAME_EXT_JOKER + "("
            + REGEX_FIELD_SEP + REGEX_WIKITTY_FQFIELDNAME_EXT_JOKER + ")*";

    public AbstractWikittyComponent(ValueStack stack,
            HttpServletRequest request, HttpServletResponse response) {
        super(stack, request, response);
    }

    public WikittyProxy getProxy() {
        return proxy;
    }

    public String getWikittyId() {
        return wikittyId;
    }

    public Wikitty getWikitty() {
        if (wikitty == null) {
            if (StringUtils.isNotEmpty(getWikittyId())) {
                wikitty = getProxy().restore(getWikittyId());
            } else {
                wikitty = ((BusinessEntityImpl) getBusinessEntity()).getWikitty();
            }
        }
        return wikitty;
    }

    public BusinessEntity getBusinessEntity() {
        return businessEntity;
    }

    public void setWikittyId(String wikittyId) {
        this.wikittyId = wikittyId;
    }

    public void setWikitty(Wikitty wikitty) {
        this.wikitty = wikitty;
    }

    public void setBusinessEntity(BusinessEntity businessEntity) {
        this.businessEntity = businessEntity;
    }

    public void setProxy(WikittyProxy proxy) {
        this.proxy = proxy;
    }

}
