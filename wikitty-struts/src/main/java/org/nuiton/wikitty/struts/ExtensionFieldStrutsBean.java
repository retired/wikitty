/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts;

import java.util.Collection;
import java.util.LinkedList;

import org.apache.commons.lang3.StringUtils;


public class ExtensionFieldStrutsBean {

    
    protected String name;
    protected String type;
    protected Object value =StringUtils.EMPTY;
    protected String label;
    protected Collection<Option> listOption;
          
    public ExtensionFieldStrutsBean() {
        listOption = new LinkedList<Option>();
    }
    public Collection<Option> getListOption() {
        return listOption;
    }
    public void setListOption(Collection<Option> listOption) {
        this.listOption = listOption;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public Object getValue() {
        return value;
    }
    public void setValue(Object value) {
        this.value = value;
    }
        
    
    public void addOption(String val, String desc){
        Option opt= new Option();
        opt.setDescription(desc);
        opt.setValeur(val);
        this.listOption.add(opt);
    }
    @Override
    public String toString() {
        return "ExtensionFieldStrutsBean [name=" + name + ", type=" + type
                + ", value=" + value + ", label=" + label + ", listOption="
                + listOption + "]";
    }
    
    

    
}
