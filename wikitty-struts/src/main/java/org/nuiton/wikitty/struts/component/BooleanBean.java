/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.views.annotations.StrutsTag;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.WikittyUtil;

import com.opensymphony.xwork2.util.ValueStack;

@StrutsTag(name = "boolean", tldTagClass = "org.nuiton.wikitty.struts.tag.BooleanTag",
        description = "", allowDynamicAttributes = false)
public class BooleanBean extends AbstractWikittyComponentBean  {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(BooleanBean.class);
    
    public static final String OPEN_TEMPLATE = "ws-boolean";
    
    public BooleanBean(ValueStack stack, HttpServletRequest request,
            HttpServletResponse response) {
        super(stack, request, response);
    }
    
    @Override
    protected void evaluateExtraParams() {
        super.evaluateExtraParams();



            // parse extname and field name
            String[] fieldsAccess = StringUtil.split(fqFieldName, WikittyUtil.FQ_FIELD_NAME_SEPARATOR);
            // add parametters to be use by the template

            addParameter(
                    "value",
                    getWikitty().getFieldAsBoolean(fieldsAccess[0],
                            fieldsAccess[1]));

        
    }
    
    public String getDefaultOpenTemplate() {
        return OPEN_TEMPLATE;
    }

    protected String getDefaultTemplate() {
        return EMPTY_TEMPLATE;
    }
}
