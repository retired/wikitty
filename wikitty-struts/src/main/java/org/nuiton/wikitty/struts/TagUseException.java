/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts;

public class TagUseException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 5985271991430090753L;

    public TagUseException() {
    }

    public TagUseException(String message, Throwable cause) {
        super(message, cause);
    }

    public TagUseException(String message) {
        super(message);
    }

    public TagUseException(Throwable cause) {
        super(cause);
    }

}
