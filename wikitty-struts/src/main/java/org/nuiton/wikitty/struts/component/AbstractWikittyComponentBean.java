/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.entities.BusinessEntity;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.struts.TagUseException;
import org.nuiton.wikitty.struts.WikittyFieldHandler;
import com.opensymphony.xwork2.util.ValueStack;

public abstract class AbstractWikittyComponentBean extends
        AbstractWikittyComponent {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory
            .getLog(AbstractWikittyComponentBean.class);

    protected WikittyFieldHandler handler;
    protected String fqFieldName;

    protected AbstractWikittyComponentBean(ValueStack stack,
            HttpServletRequest request, HttpServletResponse response) {
        super(stack, request, response);

        handler = (WikittyFieldHandler) stack.getContext().get(
                WikittyFieldHandler.WIKITTY_STACK_KEY);

    }

    public boolean isIncluded(String fieldName) {
        if (handler != null) {
            return handler.isIncluded(fieldName);
        }
        return true;
    }

    @Override
    public WikittyProxy getProxy() {
        if (handler != null) {
            return handler.getProxy();
        }
        return super.getProxy();
    }

    @Override
    public Wikitty getWikitty() {
        if (handler != null) {
            return handler.getWikitty();
        }

        return super.getWikitty();
    }

    @Override
    public BusinessEntity getBusinessEntity() {
        if (handler != null) {
            return handler.getBusinessEntity();
        }
        return super.getBusinessEntity();
    }

    @Override
    public String getWikittyId() {
        if (handler != null) {
            return handler.getWikittyId();
        }
        return super.getWikittyId();
    }

    @Override
    protected void evaluateExtraParams() {
        super.evaluateExtraParams();

        if (handler == null && wikitty == null && businessEntity == null && wikittyId == null) {
            log.info("Handler not found in the stack and wikittyId, wikitty or businessEntity not declared");

            throw new TagUseException(
                    "Tag must declare wikittyId, Wikitty or businessEntity attribute if used outside ws:form tag");
        }

        if (!fqFieldName.matches(REGEX_WIKITTY_FQFIELDNAME)) {
            log.debug("fqFieldName: " + fqFieldName + " expected match"
                    + REGEX_WIKITTY_FQFIELDNAME);
            throw new TagUseException("fqFieldName must be valid: "
                    + REGEX_WIKITTY_FQFIELDNAME);
        }

        if (name != null && handler == null) {
            addParameter("name", name);
        } else {
            addParameter("name", fqFieldName);
        }

        /*
         * if (id!=null || id.equals("")) { id = name==null?fqFieldName:name; }
         * else { addParameter("name", fqFieldName); }
         */

        // check if field included
        // if so add the parametter included
        // and add the field to the addedfield (usefull if inside

        if (isIncluded(fqFieldName)) {
            addParameter("included", true);
            if (handler != null) {
                handler.addAddedField(fqFieldName);
            }
        }
    }

    public WikittyFieldHandler getHandler() {
        return handler;
    }

    public void setHandler(WikittyFieldHandler handler) {
        this.handler = handler;
    }

    public String getFqFieldName() {
        return fqFieldName;
    }

    /**
     * set the fqfieldname, value will be trimed
     * 
     * @param fqFieldName
     */
    public void setFqFieldName(String fqFieldName) {
        this.fqFieldName = fqFieldName.trim();
    }

}
