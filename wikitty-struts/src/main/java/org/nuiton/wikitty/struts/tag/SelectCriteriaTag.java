/*
 * #%L
 * Wikitty :: struts
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.struts.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.components.Component;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.struts.component.SelectCriteriaBean;
import com.opensymphony.xwork2.util.ValueStack;

public class SelectCriteriaTag extends AbstractWikittyTagComponent {

    /**
     * 
     */
    private static final long serialVersionUID = 2807198744532780915L;
    protected Criteria criteria;
    protected String descField;

    @Override
    public Component getBean(ValueStack stack, HttpServletRequest req,
            HttpServletResponse res) {
        return new SelectCriteriaBean(stack, req, res);

    }
    
    

    @Override
    protected void populateParams() {
        super.populateParams();
        SelectCriteriaBean fieldtag = ((SelectCriteriaBean) component);
        
        fieldtag.setCriteria(criteria);
        fieldtag.setDescField(descField);
    }
    

    public Criteria getCriteria() {
        return criteria;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
    }

    public String getDescField() {
        return descField;
    }

    public void setDescField(String descField) {
        this.descField = descField;
    }

}
