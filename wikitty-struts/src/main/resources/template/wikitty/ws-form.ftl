<#--
 #%L
 Wikitty :: struts
 %%
 Copyright (C) 2011 - 2012 CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Lesser Public License for more details.
 
 You should have received a copy of the GNU General Lesser Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/lgpl-3.0.html>.
 #L%
-->
<form 
<#include "/${parameters.templateDir}/${parameters.theme}/ws-commons.ftl" />
class="edit" action="${parameters.action}" method="post" enctype="multipart/form-data" >
<input type="hidden" name="id" value="${parameters.wikittyid}" />
<#if parameters.redirect??>
<input type="hidden" name="redirect" value="${parameters.redirect}" />
</#if><#t/>
<#if parameters.orderBefore?? && parameters.orderBefore==true >
<#include "/${parameters.templateDir}/${parameters.theme}/ws-form-commons.ftl" />
</#if><#t/>
