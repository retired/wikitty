<#--
 #%L
 Wikitty :: struts
 %%
 Copyright (C) 2011 - 2012 CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Lesser Public License for more details.
 
 You should have received a copy of the GNU General Lesser Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/lgpl-3.0.html>.
 #L%
-->
<#assign aKeys = parameters.wikittyfields><#t/>
    <#list aKeys as aKey><#t/>
<#--<#if  aKey.type=="file">
<input type="hidden" name="${aKey.name}" value="BINARY" />
${aKey.label} : <input type="file" name="File" label ="File"/>
</#if>-->
<#if  aKey.type=="hidden">
<input
<#include "/${parameters.templateDir}/${parameters.theme}/ws-commons.ftl" />
 type="hidden" name="${aKey.name}" value="${aKey.value}" />
</#if>
<#if  aKey.type=="textarea" >
${aKey.label} : <textarea 
<#include "/${parameters.templateDir}/${parameters.theme}/ws-commons.ftl" />
 cols="80" rows="20" name="${aKey.name}">${aKey.value}</textarea>
</#if>
<#if  aKey.type=="textfield" >
${aKey.label} : <input
<#include "/${parameters.templateDir}/${parameters.theme}/ws-commons.ftl" />
 type="text" name="${aKey.name}" value="${aKey.value}" >
</#if>
<#if  aKey.type=="boolean" >                            
${aKey.label} : <input 
<#include "/${parameters.templateDir}/${parameters.theme}/ws-commons.ftl" />
 type="checkbox" name="${aKey.name}" value="true" 
	<#if  aKey.value=="true" >   
		checked='true'
	</#if> 
 />                          
</#if>      
<#--

-->
<#if  aKey.type=="select" >
${aKey.label} :
<select
<#include "/${parameters.templateDir}/${parameters.theme}/ws-commons.ftl" />
 name="${aKey.name}" size="1">
<#assign optionKeys = aKey.listOption><#t/>
    <#list optionKeys as optionKey><#t/>
	<option value="${optionKey.valeur}"
	<#if  optionKey.valeur==aKey.value >
		selected
	</#if>
	> ${optionKey.description} </option>
	</#list><#t/> 
</select>
</#if>
</br>
</#list><#t/> 
