<#--
 #%L
 Wikitty :: struts
 %%
 Copyright (C) 2011 - 2012 CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Lesser Public License for more details.
 
 You should have received a copy of the GNU General Lesser Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/lgpl-3.0.html>.
 #L%
-->
<#if parameters.included?? && parameters.included==true >
<#include "/${parameters.templateDir}/${parameters.theme}/ws-label-commons.ftl" /> 
<input 
<#include "/${parameters.templateDir}/${parameters.theme}/ws-commons.ftl" />

<#if parameters.password?? && parameters.password==true ><#t/>
type="password"
<#else><#t/>
type="text"
</#if><#t/>
 value="${parameters.value}" ><#t/> 
</#if><#t/> 
