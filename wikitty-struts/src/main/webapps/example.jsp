<%--
  #%L
  Wikitty :: struts
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="org.nuiton.wikitty.entities.Wikitty"%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="ws" uri="/wikitty-struts"%>


<%

// temp action inherit of the AbstractWikittyEditAction
TempAction action = TempAction.getAction();

Wikitty wikitty = action.getWikitty();

%>

<% if (wikitty != null) { %>
<pre><%=StringEscapeUtils.escapeHtml(String.valueOf(wikitty))%></pre>
<% } %>

<ws:form wikitty="<%=wikitty%>"
	proxy="<%=action.getProxy()%>" 
	action="/wikitty-publication/temp.action" allowDelete="true"
	exclude="WikittyToken.date"
	
>	
	
	<ws:selectCriteria fqFieldName="WikittyToken.user" label="user"  criteria="<%=action.getCrit() %>"/>
	<ws:date fqFieldName="WikittyToken.date" label="dateuh"/>
	<ws:selectFixed fqFieldName="WikittyUser.password" fixvalues="password,truc,passwordPourri" label="motdepasse"/>
	
	<!-- exclude="WikittyToken.date"
	<s:url var="urlFragment" action="temp" />
	<sj:submit targets="wikittyInfo" href="%{#urlFragment}"> </sj:submit> -->
</ws:form>







