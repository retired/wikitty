/*
 * #%L
 * Wikitty :: wikitty-jdbc
 * %%
 * Copyright (C) 2009 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbcp.managed.BasicManagedDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

/**
 * Extends BasicManagedDataSource to permit setWhenExhaustedAction configuration
 * on GenericObjectPool internal field and change default action to Grow when
 * exhausted
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyManagedDataSource extends BasicManagedDataSource {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyManagedDataSource.class);

    /**
     * nombre de connexions actives simultanement qui affiche un message
     * lorsqu'un message est affiche ce nombre est multiplie par 2 pour le
     * prochain message
     */
    protected int warnConnectionCount = 10;
    protected byte whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
    
    /**
     * 
     * @return whenExhaustedAction
     * 
     * @see GenericObjectPool#WHEN_EXHAUSTED_BLOCK
     * @see GenericObjectPool#WHEN_EXHAUSTED_FAIL
     * @see GenericObjectPool#WHEN_EXHAUSTED_GROW
     */
    public byte getWhenExhaustedAction() {
        byte result = whenExhaustedAction;
        if (connectionPool != null) {
            connectionPool.getWhenExhaustedAction();
        }
        return result;
    }
    
    /**
     * Change whenExhaustedAction of connectionPool
     * 
     * @param whenExhaustedAction 
     * @see GenericObjectPool#WHEN_EXHAUSTED_BLOCK
     * @see GenericObjectPool#WHEN_EXHAUSTED_FAIL
     * @see GenericObjectPool#WHEN_EXHAUSTED_GROW
     */
    public void setWhenExhaustedAction(byte whenExhaustedAction) {
        this.whenExhaustedAction = whenExhaustedAction;
        if (connectionPool != null) {
            connectionPool.setWhenExhaustedAction(whenExhaustedAction);
        }
    }
    
    @Override
    protected void createConnectionPool() {
        super.createConnectionPool();
        connectionPool.setWhenExhaustedAction(whenExhaustedAction);
    }

    @Override
    public Connection getConnection() throws SQLException {
        Connection result = super.getConnection();
        
        int active = connectionPool.getNumActive();
        if (active > warnConnectionCount) {
            int idle = connectionPool.getNumIdle();
            warnConnectionCount = warnConnectionCount * 2;
            
            log.warn(String.format(
                    "Too many database connection open active:%s idle:%s",
                    active, idle));
        }
        
        return result;
    }
        
}
