/*
 * #%L
 * Wikitty :: wikitty-jdbc
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.jdbc;

import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_FIELDTYPE;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_ID;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_NAME;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_REQUIRES;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_TAGVALUES;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_VERSION;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_CLEAR_EXTENSION;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_DELETE_EXTENSION_ADMIN;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_DELETE_EXTENSION_DATA;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_CREATION_EXTENSION_ADMIN;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_CREATION_EXTENSION_DATA;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_INSERT_EXTENSION_ADMIN;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_INSERT_EXTENSION_DATA;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_SELECT;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_SELECT_EXTENSION_ADMIN;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_SELECT_WHERE;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_SELECT_WHERE_REQUIRES;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.TABLE_EXTENSION_ADMIN;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.TABLE_EXTENSION_DATA;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.storage.WikittyExtensionStorage;
import org.nuiton.wikitty.services.WikittyEvent;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.WikittyUtil;

/**
 *
 * @author morin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyExtensionStorageJDBC implements WikittyExtensionStorage {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static protected Log log = LogFactory.getLog(WikittyExtensionStorageJDBC.class);
    
    /** Properties file */
    protected final Properties jdbcQuery;
    protected ApplicationConfig config;
    
    /** cache for extension key: ext id (extname[extversion]) value: WikittyExtension */
    protected Map<String, WikittyExtension> extensionCache =
            new HashMap<String, WikittyExtension>();
    
    /** cache for last extension version; key: extName value: extVersion */
    transient protected Map<String, String> lastVersion = null;
    
    public WikittyExtensionStorageJDBC(ApplicationConfig config) {
        this.config = config;
        jdbcQuery = WikittyJDBCUtil.loadQuery(config);
        
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }
            
            createDatabase(tx);
            
            if (txBeginHere) {
                tx.commit();
            }
        } catch (WikittyException eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw eee;
        }
    }

    protected void createDatabase(WikittyTransaction tx) {
        Connection connection  = WikittyJDBCUtil.getConnection(tx, config);
        try {
            Statement statement = connection.createStatement();
            if (!WikittyJDBCUtil.tableExist(connection, TABLE_EXTENSION_ADMIN)) {
                if (log.isInfoEnabled()) {
                    log.info("try to create extension database");
                }
                statement.execute(jdbcQuery.getProperty(QUERY_CREATION_EXTENSION_ADMIN));
            }
            if (!WikittyJDBCUtil.tableExist(connection, TABLE_EXTENSION_DATA)) {
                statement.execute(jdbcQuery.getProperty(QUERY_CREATION_EXTENSION_DATA));
            }
        } catch (Exception eee) {
            throw new WikittyException("Can't create table for extension storage", eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    @Override
    public WikittyEvent store(WikittyTransaction tx,
            Collection<WikittyExtension> extensions)
            throws WikittyException {
        WikittyEvent result = new WikittyEvent(this);
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            for (WikittyExtension ext : extensions) {
                // extension id is extension name with version
                String id = ext.getId();
                //select all the data with the id "id"
                String query = String.format(jdbcQuery.getProperty(QUERY_SELECT_WHERE),
                        COL_VERSION, TABLE_EXTENSION_ADMIN, COL_ID);
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, id);
                ResultSet versionResultSet = statement.executeQuery();
                
                //if the data is not already recorded
                if (!versionResultSet.next()) {
                    // FIXME poussin 20101216 make copy before add to result to prevent side effect
                    result.addExtension(ext);
                    WikittyJDBCUtil.doQuery(connection,
                            jdbcQuery.getProperty(QUERY_INSERT_EXTENSION_ADMIN),
                            ext.getId(),
                            ext.getName(),
                            ext.getVersion(),
                            StringUtils.join(ext.getRequires(), ","),
                            WikittyUtil.tagValuesToString(ext.getTagValues()));
                    for (String fieldName : ext.getFieldNames()) {
                        FieldType type = ext.getFieldType(fieldName);
                        WikittyJDBCUtil.doQuery(connection,
                                jdbcQuery.getProperty(QUERY_INSERT_EXTENSION_DATA),
                                ext.getId(), fieldName, type.toDefinition(fieldName));
                    }
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("The extension is found " + id);
                    }
                }
            }

            // FIXME poussin 20100114 put that in abstractService after commit, because must remove cache only when tx is commited
            // modification in extension, remove the cache
            lastVersion = null;

            return result;
        } catch (SQLException eee) {
            throw new WikittyException("Can't store extension", eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    @Override
    public WikittyEvent delete(WikittyTransaction tx, Collection<String> extNames) {
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            WikittyEvent result = new WikittyEvent(this);

            for (String name : extNames) {
                // # is ESCAPE caractere (see wikitty-jdbc-query file)
                // we escape [ and ] because some database use it as special char
                name = name + "#[%#]";

                // select all wikitty id for event to contains
                // id instead of only name (usefull for cache to remove cached
                // extension id)
                String q = String.format(jdbcQuery.getProperty(QUERY_SELECT_EXTENSION_ADMIN), COL_ID);
                PreparedStatement statement = connection.prepareStatement(q);
                statement.setString(1, name);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    String id = resultSet.getString(COL_ID);
                    result.addDeletedExtension(id);
                }

                // delete extension in admin and data
                WikittyJDBCUtil.doQuery(connection, jdbcQuery.getProperty(
                        QUERY_DELETE_EXTENSION_DATA), name);
                WikittyJDBCUtil.doQuery(connection, jdbcQuery.getProperty(
                        QUERY_DELETE_EXTENSION_ADMIN), name);
            }
            
            // FIXME echatellier 20110221 : clear cache !!!
            lastVersion = null;

            return result;
        } catch (SQLException eee) {
            throw new WikittyException("Can't delete extension", eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    @Override
    public boolean exists(WikittyTransaction tx, String id) {
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            //select the data with teh id "id" in the admin table
            String q = String.format(jdbcQuery.getProperty(QUERY_SELECT_WHERE),
                    COL_ID, TABLE_EXTENSION_ADMIN, COL_ID);
            PreparedStatement statement = connection.prepareStatement(q);
            statement.setString(1, id);
            ResultSet resultSet = statement.executeQuery();

            boolean result = resultSet.next();
            return result;
        } catch (SQLException eee) {
            throw new WikittyException("Can't test extension existance",eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    @Override
    public List<String> getAllExtensionIds(WikittyTransaction tx) {
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            List<String> result = new ArrayList<String>();
            Statement statement = connection.createStatement();
            //get all extensions names and versions
            ResultSet resultSet = statement.executeQuery(
                    String.format(jdbcQuery.getProperty(QUERY_SELECT),
                    COL_ID, TABLE_EXTENSION_ADMIN));

            while (resultSet.next()) {
                String id = resultSet.getString(COL_ID);
                result.add(id);
            }
            return result;
        } catch (SQLException eee) {
            throw new WikittyException("Can't retrieve all extensions id", eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    @Override
    public List<String> getAllExtensionsRequires(WikittyTransaction tx,
            String extensionName) {
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            List<String> result = new ArrayList<String>();
            String q = String.format(jdbcQuery.getProperty(QUERY_SELECT_WHERE_REQUIRES),
                    COL_ID, extensionName);
            
            PreparedStatement statement = connection.prepareStatement(q);
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                String id = resultSet.getString(COL_ID);
                result.add(id);
            }
            
            return result;
        } catch (SQLException eee) {
            throw new WikittyException(String.format(
                    "Can't retrieve all extensions required by %s", extensionName), eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    /**
     * return last version available for specified extension name
     * @param tx ???
     * @param extName name of extension
     * @return last version availble for this version, or null if extension
     * doesn't exist
     */
    @Override
    public String getLastVersion(WikittyTransaction tx,
            String extName) {
        if (lastVersion == null) {
            // create cache for futur call
            Map<String, String> tmp = new HashMap<String, String>();
            Connection connection = WikittyJDBCUtil.getConnection(tx, config);
            try {
                Statement statement = connection.createStatement();
                //get all extensions names and versions
                ResultSet resultSet = statement.executeQuery(
                        String.format(jdbcQuery.getProperty(QUERY_SELECT),
                        COL_NAME + "," + COL_VERSION, TABLE_EXTENSION_ADMIN));
                while (resultSet.next()) {
                    String name = resultSet.getString(COL_NAME);
                    String version = resultSet.getString(COL_VERSION);
                    String prevVersion = tmp.get(name);
                    //if the version or this row is greater than the versions of teh already visited rows
                    if (prevVersion == null ||
                            WikittyUtil.versionGreaterThan(version, prevVersion)) {
                        tmp.put(name, version);
                    }
                }
                lastVersion = tmp;
            } catch (SQLException eee) {
                throw new WikittyException("Can't get last version of extensions", eee);
            } finally {
                WikittyJDBCUtil.closeQuietly(connection);
            }
        }
        String result = lastVersion.get(extName);
        return result;
    }

    @Override
    public WikittyExtension restore(WikittyTransaction tx, String name, String version)
            throws WikittyException {
        String id = WikittyExtension.computeId(name, version);
        WikittyExtension result = extensionCache.get(id);
        if (result == null) {
            Connection connection = WikittyJDBCUtil.getConnection(tx, config);
            try {

                //get the data with the id "id" in the admin table
                String q = String.format(jdbcQuery.getProperty(QUERY_SELECT_WHERE),
                        "*", TABLE_EXTENSION_ADMIN, COL_ID);
                PreparedStatement statement = connection.prepareStatement(q);
                statement.setString(1, id);
                ResultSet adminResultSet = statement.executeQuery();

                if (adminResultSet.next()) {
                    String extName = adminResultSet.getString(COL_NAME);
                    String extVersion = adminResultSet.getString(COL_VERSION);
                    
                    LinkedHashMap<String, FieldType> fieldTypes =
                            new LinkedHashMap<String, FieldType>();

                    //get the data with the id "id" in the data table
                    String qdata = String.format(jdbcQuery.getProperty(QUERY_SELECT_WHERE),
                            "*", TABLE_EXTENSION_DATA, COL_ID);
                    PreparedStatement sta = connection.prepareStatement(qdata);
                    sta.setString(1, id);
                    ResultSet dataResultSet = sta.executeQuery();

                    while (dataResultSet.next()) {
                        String fieldDef = dataResultSet.getString(COL_FIELDTYPE);
                        FieldType fieldType = new FieldType();
                        String fieldName = WikittyUtil.parseField(fieldDef, fieldType);
                        fieldTypes.put(fieldName, fieldType);
                    }
                    
                    String extTagValues = adminResultSet.getString(COL_TAGVALUES);
                    Map<String, String> tagValues = WikittyUtil.tagValuesToMap(extTagValues);
                    String extRequires = adminResultSet.getString(COL_REQUIRES);
                    result = new WikittyExtension(
                            extName, extVersion, tagValues, extRequires, fieldTypes);

                    // synchronisation pour supporter le multithreading
                    synchronized (extensionCache) {
                        // FIXME poussin 20100114 put that in abstractService after commit, because must in cache only when tx is commited
                        extensionCache.put(id, result);
                    }
                }
                
            } catch (SQLException eee) {
                throw new WikittyException(String.format("Can't load extension %s", id), eee);
            } finally {
                WikittyJDBCUtil.closeQuietly(connection);
            }
        }
        return result;
    }

    @Override
    public WikittyEvent clear(WikittyTransaction tx) {
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            lastVersion = null;
            WikittyJDBCUtil.doQuery(connection, jdbcQuery.getProperty(QUERY_CLEAR_EXTENSION));
            WikittyEvent result = new WikittyEvent(this);
            result.addType(WikittyEvent.WikittyEventType.CLEAR_EXTENSION);
            return result;
        } catch (Exception eee) {
            throw new WikittyException("Can't clear all extension", eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }
}
