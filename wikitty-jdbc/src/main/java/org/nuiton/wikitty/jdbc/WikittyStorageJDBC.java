/*
 * #%L
 * Wikitty :: wikitty-jdbc
 * %%
 * Copyright (C) 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_BINARY_VALUE;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_BOOLEAN_VALUE;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_DATE_VALUE;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_DELETION_DATE;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_EXTENSION;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_FIELDNAME;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_ID;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_NUMBER_VALUE;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_TEXT_VALUE;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.COL_VERSION;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_CLEAR_WIKITTY;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_CREATION_WIKITTY_ADMIN;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_CREATION_WIKITTY_DATA_ALTER_BINARY;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_CREATION_WIKITTY_DATA_TEST_BINARY;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_CREATION_WIKITTY_DATA;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_CREATION_WIKITTY_DATA_NO_BINARY;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_DELETE_WIKITTY_ADMIN;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_DELETE_WIKITTY_DATA;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_INSERT_WIKITTY_ADMIN;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_INSERT_WIKITTY_DATA;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_SELECT_NOTDELETED;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_COUNT_ACTIVE_WIKITTY;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_COUNT_DELETED_WIKITTY;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_SELECT_WHERE;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_SELECT_TWO_WHERE;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_SELECT_WHERE_NOTDELETED;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.QUERY_UPDATE_WIKITTY_ADMIN;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.TABLE_WIKITTY_ADMIN;
import static org.nuiton.wikitty.jdbc.WikittyJDBCUtil.TABLE_WIKITTY_DATA;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.io.IOUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfig;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.storage.WikittyExtensionStorage;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.WikittyObsoleteException;
import org.nuiton.wikitty.services.WikittyEvent;
import org.nuiton.wikitty.storage.WikittyStorage;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.WikittyTypes;

/**
 *
 * @author morin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyStorageJDBC implements WikittyStorage {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static protected Log log = LogFactory.getLog(WikittyStorageJDBC.class);

    /** Properties file */
    protected final Properties jdbcQuery;
    protected ApplicationConfig config;

    /** used to parse list field from hbase data. ex: extension.fieldname[11/15] */
    static final private Pattern listFieldPattern =
            Pattern.compile("(.*)\\[(\\d+)/(\\d+)\\]");
    
    protected WikittyExtensionStorage extensionStorage;

    public WikittyStorageJDBC(ApplicationConfig config, WikittyExtensionStorage extensionStorage) {
        this.config = config;
        this.extensionStorage = extensionStorage;
        jdbcQuery = WikittyJDBCUtil.loadQuery(config);
        
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            checkTableOrCreation(tx);

            if (txBeginHere) {
                tx.commit();
            }

        } catch (WikittyException eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw eee;
        }
    }

    /**
     * test table existance or create them if necessary
     */
    protected void checkTableOrCreation(WikittyTransaction tx) {
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            Statement statement = connection.createStatement();
            if (!WikittyJDBCUtil.tableExist(connection, WikittyJDBCUtil.TABLE_WIKITTY_ADMIN)) {
                if (log.isInfoEnabled()) {
                    log.info("try to create wikitty database");
                }
                statement.execute(jdbcQuery.getProperty(QUERY_CREATION_WIKITTY_ADMIN));
            }
            if (!WikittyJDBCUtil.tableExist(connection, WikittyJDBCUtil.TABLE_WIKITTY_DATA)) {

                // Check if database support blob
                String supportedBinaryType = WikittyJDBCUtil.getSupportedBinaryType(connection);
                if (supportedBinaryType == null) {
                    log.fatal("Can add column to store binary field. You can't use binary");
                    statement.execute(jdbcQuery.getProperty(QUERY_CREATION_WIKITTY_DATA_NO_BINARY));
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("Creating wikitty data with '" + supportedBinaryType + "' type for binary");
                    }
                    String request = String.format(jdbcQuery.getProperty(QUERY_CREATION_WIKITTY_DATA), supportedBinaryType);
                    statement.execute(request);
                }
            }
        } catch (Exception eee) {
            throw new WikittyException("Can't create table for wikitty storage", eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    protected String getColName(WikittyTypes type) {
        String result;
        switch(type) {
            case BINARY:
                result = COL_BINARY_VALUE;
                break;
            case BOOLEAN:
                result = COL_BOOLEAN_VALUE;
                break;
            case DATE:
                result = COL_DATE_VALUE;
                break;
            case NUMERIC:
                result = COL_NUMBER_VALUE;
                break;
            case STRING:
            case WIKITTY:
            default:
                result = COL_TEXT_VALUE;
                break;
        }
        return result;
    }

    @Override
    public WikittyEvent store(WikittyTransaction tx,
            Collection<Wikitty> wikitties, boolean force) throws WikittyException {
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            WikittyEvent result = new WikittyEvent(this);
            for (Wikitty wikitty : wikitties) {
                String query = String.format(jdbcQuery.getProperty(QUERY_SELECT_TWO_WHERE),
                        COL_VERSION, COL_DELETION_DATE, TABLE_WIKITTY_ADMIN, COL_ID);
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, wikitty.getWikittyId());
                ResultSet versionResultSet = statement.executeQuery();

                String extensionList = "";
                boolean wikittyAlreadyExists = versionResultSet.next();
                String actualVersion = null;
                Date deletionDate = null;
                String requestedVersion = wikitty.getWikittyVersion();

                if (wikittyAlreadyExists) {
                    actualVersion = versionResultSet.getString(COL_VERSION);
                    deletionDate = versionResultSet.getDate(COL_DELETION_DATE);
                }
         
                // compute new version, but not change wikitty
                String newVersion = null;
                if (force) {
                    // requestedVersion is never null
                    // versionGreaterThan support null for actualVersion
                    // and in this case return true
                    // see javadoc for more explanation
                    if (WikittyUtil.versionGreaterThan(requestedVersion, actualVersion)) {
                        newVersion = requestedVersion;
                    } else {
                        // else take actualVersion as base version
                        newVersion = WikittyUtil.incrementMajorRevision(actualVersion);
                    }
                } else {
                    if (WikittyUtil.versionEquals(actualVersion, requestedVersion) && deletionDate == null) {
                        // wikitty is not modified, do nothing
                        continue;
                    } else if (WikittyUtil.versionGreaterThan(actualVersion, requestedVersion)) {
                        throw new WikittyObsoleteException(String.format(
                                "Your wikitty '%s' is obsolete", wikitty.getWikittyId()));
                    } else {
                        newVersion = WikittyUtil.incrementMajorRevision(actualVersion);
                    }
                }

                if (wikittyAlreadyExists) {
                    WikittyJDBCUtil.doQuery(connection,
                            jdbcQuery.getProperty(QUERY_DELETE_WIKITTY_DATA),
                            wikitty.getWikittyId());
                }
                else {
                    WikittyJDBCUtil.doQuery(connection,
                            jdbcQuery.getProperty(QUERY_INSERT_WIKITTY_ADMIN),
                            wikitty.getWikittyId(), newVersion, "");
                }
                
                
                for (WikittyExtension ext : wikitty.getExtensions()) {
                    extensionList += "," + ext.getId();
                    for (String fieldName : ext.getFieldNames()) {
                        FieldType type = ext.getFieldType(fieldName);
                        if (type.isCollection()) {
                            List<Object> list = wikitty.getFieldAsList(ext.getName(),
                                    fieldName, Object.class);
                            if (list != null) {
                                for (int i = 0; i < list.size(); i++) {
                                    Object value = list.get(i);
                                    String colName = getColName(type.getType());
                                    String q = String.format(jdbcQuery.getProperty(
                                            QUERY_INSERT_WIKITTY_DATA), colName);
                                    WikittyJDBCUtil.doQuery(connection, q,
                                            wikitty.getWikittyId(),
                                            ext.getName() + "." + fieldName + "[" + i + "/" + list.size() + "]",
                                            value);
                                }
                            } else {
                                if (type.isNotNull()) {
                                    throw new WikittyException(String.format(
                                            "Field %s in extension %s can't be null",
                                            fieldName, ext.getName()));
                                }
                            }
                        } else {
                            Object value = wikitty.getFieldAsObject(ext.getName(), fieldName);
                            if (value != null) {
                                String colName;
                                if (type.isCrypted()) {
                                    value = wikitty.getFieldAsBytes(ext.getName(), fieldName);
                                    value = WikittyUtil.crypt(type.getCryptAlgo(),
                                            WikittyConfig.getCryptPassword(config, type.getCryptPassword()),
                                            (byte[])value);
                                    colName = getColName(WikittyTypes.BINARY);
                                } else {
                                    colName = getColName(type.getType());
                                }
                                String q = String.format(jdbcQuery.getProperty(
                                        QUERY_INSERT_WIKITTY_DATA), colName);
                                WikittyJDBCUtil.doQuery(connection, q,
                                        wikitty.getWikittyId(),
                                        ext.getName() + "." + fieldName,
                                        value);
                            } else {
                                if (type.isNotNull()) {
                                    throw new WikittyException(String.format(
                                            "Field %s in extension %s can't be null",
                                            fieldName, ext.getName()));
                                }
                            }
                        }
                    }
                }

                if (extensionList.length() > 0) {
                    // delete first ','
                    extensionList = extensionList.substring(1);
                }

                // update extensions in wikitty object
                // force all time deletion date to null
                String q = jdbcQuery.getProperty(QUERY_UPDATE_WIKITTY_ADMIN);
                WikittyJDBCUtil.doQuery(connection, q, newVersion, extensionList,
                        null, wikitty.getWikittyId());

                // make a clone to prevent side effect
                Wikitty newWikitty = wikitty.clone();
                newWikitty.setWikittyVersion(newVersion);
                newWikitty.clearDirty();
                result.addWikitty(newWikitty);
            }

            return result;
        } catch (WikittyException eee) {
            throw eee;
        } catch (Exception eee) {
            throw new WikittyException("Can't store wikitty", eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    @Override
    public boolean exists(WikittyTransaction tx, String id) {
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            //select the data with the id "id" in the admin table
            String q = String.format(jdbcQuery.getProperty(QUERY_SELECT_WHERE), COL_ID,
                    TABLE_WIKITTY_ADMIN, COL_ID);
            PreparedStatement statement = connection.prepareStatement(q);
            statement.setString(1, id);
            ResultSet resultSet = statement.executeQuery();
            // return true if the query has a result
            boolean result = resultSet.next();
            return result;
        } catch (SQLException eee) {
            throw new WikittyException("Can't test wikitty existance", eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    @Override
    public boolean isDeleted(WikittyTransaction tx, String id) {
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            //select the data with the id "id" in the admin table
            String q = String.format(jdbcQuery.getProperty(QUERY_SELECT_WHERE),
                    COL_DELETION_DATE, TABLE_WIKITTY_ADMIN, COL_ID);
            PreparedStatement statement = connection.prepareStatement(q);
            statement.setString(1, id);
            ResultSet resultSet = statement.executeQuery();
            boolean result;
            if(resultSet.next()) {
                result = resultSet.getDate(COL_DELETION_DATE) != null;
                return result;
            } else {
                throw new WikittyException(String.format(
                            "Wikitty with id '%s' doesn't exists", id));
            }
        } catch (SQLException eee) {
            throw new WikittyException("Can't test if wikitty is already deleted", eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    @Override
    public Wikitty restore(WikittyTransaction tx,
            String id, String... fqFieldName) throws WikittyException {
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            //select the data with the id "id" in the admin table
            String q = String.format(jdbcQuery.getProperty(
                    QUERY_SELECT_WHERE_NOTDELETED), "*", TABLE_WIKITTY_ADMIN, COL_ID);
            PreparedStatement statement = connection.prepareStatement(q);
            statement.setString(1, id);
            ResultSet adminResultSet = statement.executeQuery();

            if (adminResultSet.next()) {
                String version = adminResultSet.getString(COL_VERSION);
                String extensionList = adminResultSet.getString(COL_EXTENSION);
                //select the data with the id "id" in the data table
                String qdata = String.format(jdbcQuery.getProperty(QUERY_SELECT_WHERE),
                        "*", TABLE_WIKITTY_DATA, COL_ID);
                PreparedStatement sta = connection.prepareStatement(qdata);
                sta.setString(1, id);
                ResultSet dataResultSet = sta.executeQuery();

                Wikitty result = constructWikitty(tx, id, version, extensionList,
                        dataResultSet, fqFieldName);
                return result;
            } else {
                throw new WikittyException(String.format(
                        "Can't restore wikitty '%s'", id));
            }
        } catch (SQLException eee) {
            throw new WikittyException(String.format(
                    "Can't restore wikitty '%s'", id), eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    @Override
    public WikittyEvent delete(WikittyTransaction tx, Collection<String> ids) throws WikittyException {
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            WikittyEvent result = new WikittyEvent(this);
            Date now = new Date();

            for (String id : ids) {
                if (exists(tx, id) && !isDeleted(tx, id)) {
                    // addVersionUpdate delete date field
                    WikittyJDBCUtil.doQuery(connection, jdbcQuery.getProperty(
                            QUERY_DELETE_WIKITTY_ADMIN), id);
                    result.addRemoveDate(id, now);
                }
            }

            return result;
        } catch (SQLException eee) {
            throw new WikittyException("Can't delete wikitty", eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    @Override
    public void scanWikitties(WikittyTransaction tx, Scanner scanner) {
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            Statement statement = connection.createStatement();
            
            // get all wikitties
            // fails with QUERY_SELECT
            ResultSet resultSet = statement.executeQuery(
                    String.format(jdbcQuery.getProperty(QUERY_SELECT_NOTDELETED),
                    COL_ID, TABLE_WIKITTY_ADMIN));
    
            while (resultSet.next()) {
                String id = resultSet.getString(COL_ID);
                //Wikitty wikitty = restore(transaction, id);
                scanner.scan(id);
            }
        } catch (SQLException eee) {
            throw new WikittyException("Can't scan whole wikitty", eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    /**
     * Create Wikitty from jdbc tables.
     * 
     * @param id the id of the wikitty to restore
     * @param version the version of the wikitty to restore
     * @param extensionList the list of the extensions of the wikitty to restore
     * @param resultSet the ResultSet as the result of the selection of the in the data table
     * @param fqFieldName minimum field to restore
     * @return
     */
    protected Wikitty constructWikitty(WikittyTransaction tx,
            String id, String version, String extensionList,
            ResultSet resultSet, String... fqFieldName) throws SQLException {
        Set<String> acceptedField = new HashSet<String>(Arrays.asList(fqFieldName));
        Wikitty result = new WikittyImpl(id);
        result.setWikittyVersion(version);
        if (extensionList != null && !"".equals(extensionList)) {
            for (String ext : extensionList.split(",")) {
                String extName = WikittyExtension.computeName(ext);
                String extVersion = WikittyExtension.computeVersion(ext);

                WikittyExtension extension =
                        extensionStorage.restore(tx, extName, extVersion);
                result.addExtension(extension);
            }
        }

        // load field
        Map<String, Object[]> listFieldMap = new HashMap<String, Object[]>();
        while (resultSet.next()) {
            // fqfieldName fully qualified fieldName (extention.fieldname)
            String fqfieldName = resultSet.getString(COL_FIELDNAME);
            // si le champs est dans la base sur l'objet mais non declarer sur
            // l'extension, on ne charge pas ce champs (ceci arrive si, quelqu'un 
            // a modifier une extension, sans modifier sa version :(
            if (!result.hasField(fqfieldName)) {
                if (log.isErrorEnabled()) {
                    log.error(String.format("Stored field not found in this"
                            + " extension version, perhaps you have change"
                            + " extension without change version", fqfieldName));
                }
            } else if (isAcceptedField(acceptedField, fqfieldName)) {
                FieldType type = result.getFieldType(fqfieldName);
                Object value = null;
                WikittyTypes fieldType = type.getType();
                if (type.isCrypted()) {
                    fieldType = WikittyTypes.BINARY;
                }
                switch (fieldType) {
                    case BINARY:
                        InputStream blob = resultSet.getBinaryStream(COL_BINARY_VALUE);
                        try {
                            value = IOUtils.toByteArray(blob);
                        } catch (IOException eee) {
                            throw new WikittyException("Can't read blob stream for database", eee);
                        }
                        break;
                    case BOOLEAN:
                        value = resultSet.getBoolean(COL_BOOLEAN_VALUE);
                        break;
                    case DATE:
                        java.sql.Timestamp timestamp = resultSet.getTimestamp(COL_DATE_VALUE);
                        if(timestamp != null) {
                            value = new java.util.Date(timestamp.getTime());
                        }
                        break;
                    case NUMERIC:
                        value = resultSet.getBigDecimal(COL_NUMBER_VALUE);
                        break;
                    case STRING:
                        value = resultSet.getString(COL_TEXT_VALUE);
                        break;
                    case WIKITTY:
                        value = resultSet.getString(COL_TEXT_VALUE);
                        break;
                    default:
                        value = resultSet.getString(COL_TEXT_VALUE);
                        break;
                }

                if (type.isCrypted()) {
                    value = WikittyUtil.decrypt(type.getCryptAlgo(),
                            WikittyConfig.getCryptPassword(config, type.getCryptPassword()),
                            (byte[])value);
                }

                if (type.isCollection()) {
                    // for list just stock array of element in map
                    Matcher match = listFieldPattern.matcher(fqfieldName);
                    if (match.find()) {
                        fqfieldName = match.group(1);
                        int index = Integer.parseInt(match.group(2));
                        Object[] array = listFieldMap.get(fqfieldName);
                        if (array == null) {
                            int size = Integer.parseInt(match.group(3));
                            array = new Object[size];
                            listFieldMap.put(fqfieldName, array);
                        }
                        array[index] = value;
                    } else {
                        if(log.isErrorEnabled()) {
                            log.error(String.format(
                                    "Can't read list field correctly '%s'", fqfieldName));
                        }
                    }
                } else {
                    result.setFqField(fqfieldName, value);
                }
            }
        }

        // add fieldList in wikitty
        for (String fieldName : listFieldMap.keySet()) {
            Object[] array = listFieldMap.get(fieldName);
            FieldType type = result.getFieldType(fieldName);
            
            // EC20100623 check this Set/List depending on unique constraints
            if (type.isUnique()) {
                Set set = new HashSet(Arrays.asList(array));
                result.setFqField(fieldName, set);
            }
            else {
                List list = new ArrayList(Arrays.asList(array));
                result.setFqField(fieldName, list);
            }
        }

        return result;
    }

    /**
     * Test if fqfieldName is in acceptedField
     * @param acceptedField list of all accepted field
     * @param fqfieldName fully qualified field name with potential [n/m] at end
     * @return if fqfieldName without potential [n/m] is in acceptedField or if acceptedField is empty
     */
    protected boolean isAcceptedField(Set<String> acceptedField, String fqfieldName) {
        boolean result = acceptedField.isEmpty();
        if (!result) {
            int crochet = fqfieldName.indexOf("[");
            if (crochet != -1) {
                fqfieldName = fqfieldName.substring(0, crochet);
            }

            result = acceptedField.contains(fqfieldName);
        }
        return result;
    }

    @Override
    public WikittyEvent clear(WikittyTransaction tx) {
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            WikittyJDBCUtil.doQuery(connection, jdbcQuery.getProperty(QUERY_CLEAR_WIKITTY));
            WikittyEvent result = new WikittyEvent(this);
            result.addType(WikittyEvent.WikittyEventType.CLEAR_WIKITTY);
            return result;
        } catch (SQLException eee) {
            throw new WikittyException("Can't clear wikitty data", eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }
    }

    @Override
    public DataStatistic getDataStatistic(WikittyTransaction tx) {
        DataStatistic result = null;
        Connection connection = WikittyJDBCUtil.getConnection(tx, config);
        try {
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(
                    jdbcQuery.getProperty(QUERY_COUNT_ACTIVE_WIKITTY));
            if (resultSet.next()) {
                long activeWikitties = resultSet.getLong(1);

                resultSet = statement.executeQuery(
                        jdbcQuery.getProperty(QUERY_COUNT_DELETED_WIKITTY));
                if (resultSet.next()) {
                    long deletedWikitties = resultSet.getLong(1);
                    
                    result = new DataStatistic(activeWikitties, deletedWikitties);
                }
            }
        } catch (SQLException eee) {
            log.warn("Can't retrieve statisticn data", eee);
        } finally {
            WikittyJDBCUtil.closeQuietly(connection);
        }

        if (result == null) {
            result = new DataStatistic();
        }
        return result;
    }
}
