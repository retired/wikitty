/*
 * #%L
 * Wikitty :: wikitty-jdbc
 * %%
 * Copyright (C) 2010 - 2016 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import javax.sql.XADataSource;

import javax.transaction.TransactionManager;
import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.beanutils.BeanUtils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.services.WikittyTransaction;

/**
 *
 * @author morin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyJDBCUtil {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyJDBCUtil.class);

    /** TODO sletellier 20110506 : reference all binary types existing */
    /** reference all binary type known for bd, wikitty will search if type is supported for using it */
    static final public String[] BINARY_TYPES = new String[]{"bytea", "blob"};

    /** extension list column in the wikitty_admin table */
    static final public String COL_EXTENSION  = "extension_list";
    /** version column in the admin tables */
    static final public String COL_VERSION = "version";
    /** id column in all the tables */
    static final public String COL_ID = "id";
    /** name column in the extension_admin table */
    static final public String COL_NAME = "name";
    /** requires column in the extension_admin table */
    static final public String COL_REQUIRES = "requires";
    /** tagvalues column in the extension_admin table */
    static final public String COL_TAGVALUES = "tagvalues";
    /** field name column in the data tables */
    static final public String COL_FIELDNAME = "fieldName";
    /** field type column in the extension_data table */
    static final public String COL_FIELDTYPE = "fieldType";
    /** boolean value column in the wikitty_data table */
    static final public String COL_BINARY_VALUE = "binaryValue";
    /** boolean value column in the wikitty_data table */
    static final public String COL_BOOLEAN_VALUE = "booleanValue";
    /** number value column in the wikitty_data table */
    static final public String COL_NUMBER_VALUE = "numberValue";
    /** text value column in the wikitty_data table */
    static final public String COL_TEXT_VALUE = "textValue";
    /** date value column in the wikitty_data table */
    static final public String COL_DATE_VALUE = "dateValue";
    /** deletion date column in wikitty_admin table */
    static final public String COL_DELETION_DATE = "deletionDate";

    /** basic selection without where clause query property name */
    static final public String QUERY_SELECT = "jdbc.queries.select";
    /** basic selection without where clause query property name */
    static final public String QUERY_SELECT_NOTDELETED = "jdbc.queries.select.notdeleted";
    /** count number of active (not deleted wikitties) */
    static final public String QUERY_COUNT_ACTIVE_WIKITTY = "jdbc.queries.select.count.active.wikitties";
    /** count number of deleted */
    static final public String QUERY_COUNT_DELETED_WIKITTY = "jdbc.queries.select.count.deleted.wikitties";
    /** basic selection with where clause query property name */
    static final public String QUERY_SELECT_WHERE = "jdbc.queries.select.where";
    /** basic selection with where clause query property name */
    static final public String QUERY_SELECT_TWO_WHERE = "jdbc.queries.select.two.where";
    /** basic selection with where clause like for requires */
    static final public String QUERY_SELECT_WHERE_REQUIRES = "jdbc.queries.select.requires";
    /** not deleted data selection with where clause query property name */
    static final public String QUERY_SELECT_WHERE_NOTDELETED = "jdbc.queries.select.where.notdeleted";

    /** wikitty_admin table creation query property name */
    static final public String QUERY_CREATION_WIKITTY_ADMIN =
            "jdbc.queries.creation.wikitty.admin";
    /** wikitty_data column binary test exits query property name */
    static final public String QUERY_CREATION_WIKITTY_DATA_TEST_BINARY =
            "jdbc.queries.creation.wikitty.data.test.binary";
    /** wikitty_data column binary creation with alter query property name */
    static final public String QUERY_CREATION_WIKITTY_DATA_ALTER_BINARY =
            "jdbc.queries.creation.wikitty.data.alter.binary";
    /** wikitty_data table creation query property name */
    static final public String QUERY_CREATION_WIKITTY_DATA =
            "jdbc.queries.creation.wikitty.data";
    static final public String QUERY_CREATION_WIKITTY_DATA_NO_BINARY =
            "jdbc.queries.creation.wikitty.data.no.binary";
    /** insertion in the admin table query property name */
    static final public String QUERY_INSERT_WIKITTY_ADMIN = "jdbc.queries.insert.wikitty.admin";
    /** update in the admin table query property name */
    static final public String QUERY_UPDATE_WIKITTY_ADMIN = "jdbc.queries.update.wikitty.admin";
    /** insertion in the data table query property name */
    static final public String QUERY_INSERT_WIKITTY_DATA = "jdbc.queries.insert.wikitty.data";
    /** deletion in the admin table query property name */
    static final public String QUERY_DELETE_WIKITTY_ADMIN = "jdbc.queries.delete.wikitty.admin";
    /** deletion in the data table query property name */
    static final public String QUERY_DELETE_WIKITTY_DATA = "jdbc.queries.delete.wikitty.data";

    /** clear extension query property name */
    static final public String QUERY_CLEAR_EXTENSION = "jdbc.queries.clear.extension";
    /** clear wikitty query property name */
    static final public String QUERY_CLEAR_WIKITTY = "jdbc.queries.clear.wikitty";

    /** clear extension query property name */
    static final public String QUERY_SELECT_EXTENSION_ADMIN = "jdbc.queries.select.extension.admin";
    static final public String QUERY_DELETE_EXTENSION_ADMIN = "jdbc.queries.delete.extension.admin";
    static final public String QUERY_DELETE_EXTENSION_DATA = "jdbc.queries.delete.extension.data";

    /** extension_admin table creation query property name */
    static final public String QUERY_CREATION_EXTENSION_ADMIN =
            "jdbc.queries.creation.extension.admin";
    /** extension_data table creation query property name */
    static final public String QUERY_CREATION_EXTENSION_DATA =
            "jdbc.queries.creation.extension.data";
    /** insertion in the admin table query property name */
    static final public String QUERY_INSERT_EXTENSION_ADMIN =
            "jdbc.queries.insert.extension.admin";
    /** insertion in the data table query property name */
    static final public String QUERY_INSERT_EXTENSION_DATA =
            "jdbc.queries.insert.extension.data";

    /** admin table name */
    static protected String TABLE_WIKITTY_ADMIN = "wikitty_admin";
    /** data table name */
    static protected String TABLE_WIKITTY_DATA = "wikitty_data";
    /** admin table name */
    static protected String TABLE_EXTENSION_ADMIN = "extension_admin";
    /** data table name */
    static protected String TABLE_EXTENSION_DATA = "extension_data";


    /**
     * Loads the properties from configuration file, one or more properties
     * can be load default load {@code wikitty-jdbc-config.properties} file.
     *
     * @param config custom properties to override default configuration
     * @return the properties for the queries
     */
    public static synchronized Properties loadQuery(ApplicationConfig config) {
        Properties result = null;

        // list of all properties file to load
        List<String> wikittyQueryFiles = config.getOptionAsList(
                WikittyConfigOption.WIKITTY_STORAGE_JDBC_QUERY_FILE.getKey())
                .getOption();

        for (String file : wikittyQueryFiles) {
            // create new Properties with result as parent
            result = new Properties(result);
            InputStream streamQuery = null;
            try {
                // queries
                URL url = ClassLoader.getSystemResource(file);
                if (url == null) {
                    ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
                    url = contextClassLoader.getResource(file);
                }

                if (log.isInfoEnabled()) {
                    log.info("Reading resource from: " + url);
                }
                // url can't be null
                streamQuery = url.openStream();
                result.load(streamQuery);
            } catch (IOException eee) {
                throw new WikittyException(String.format(
                        "Unable to load property file '%s'",
                        file), eee);
            } finally {
                IOUtils.closeQuietly(streamQuery);
            }
        }


        return result;
    }

    private static Map<String, WikittyManagedDataSource> dataSources =
        new HashMap<String, WikittyManagedDataSource>();

    /**
     * Get a new connection instance (i.e. it opens a new transaction) plug on
     * JTA.
     * 
     * This connection is not configured, returned connection is just create connection
     * Don't use this method, use getConnection that change auto commit to false
     * 
     * This connection must be close be the asker after used
     * 
     * @param config configuration
     * @return a new Connection (db transaction)
     */
    public static synchronized Connection getConnectionUnconfigured(
            WikittyTransaction tx, ApplicationConfig config) {
        if (tx == null) {
            throw new IllegalArgumentException("WikittyTransaction must not be null");
        }

        String driver = config.getOption(
                WikittyConfigOption.WIKITTY_STORAGE_JDBC_DRIVER.getKey());
        String host = config.getOption(
                WikittyConfigOption.WIKITTY_STORAGE_JDBC_URL.getKey());
        String username = config.getOption(
                WikittyConfigOption.WIKITTY_STORAGE_JDBC_LOGIN.getKey());
        String password = config.getOption(
                WikittyConfigOption.WIKITTY_STORAGE_JDBC_PASSWORD.getKey());
        
        String xaDataSourceClassName = config.getOption(
                WikittyConfigOption.WIKITTY_STORAGE_JDBC_XADATASOURCE.getKey());
        try {
            String jdbcUrl = String.format("%s:%s@%s", username, password, host);
            
            if (!dataSources.containsKey(jdbcUrl)) {
                if (log.isInfoEnabled()) {
                    String jdbcUrlForLog = String.format("%s:%s@%s", username, "********", host);
                    log.info("Creating BasicManagedDataSource for: " + jdbcUrlForLog);
                }

                // Create datasource
                WikittyManagedDataSource dataSource = new WikittyManagedDataSource();

                // if xadatasource
                if(StringUtils.isNotEmpty(xaDataSourceClassName)) {
                    XADataSource xaDataSource = (XADataSource)
                            Class.forName(xaDataSourceClassName).newInstance();

                    BeanMap beanMap = new BeanMap(xaDataSource);
                    Set<?> fields = beanMap.keySet();

                    String keyStart =
                            WikittyConfigOption.WIKITTY_STORAGE_JDBC_XADATASOURCE.getKey()
                            + "." + xaDataSourceClassName + ".";
                    
                    // Inject properties in xadatasource
                    for(Entry<Object, Object> properties : config.getFlatOptions().entrySet()) {
                        String propertyName = (String) properties.getKey();
                        if (propertyName.startsWith(keyStart)) {
                            propertyName = propertyName.replaceFirst(keyStart, "");
                            if(fields.contains(propertyName)) {
                                String propertyValue = (String) properties.getValue();
                                BeanUtils.setProperty(xaDataSource, propertyName, propertyValue);
                            } else {
                                log.warn("Invalid property " + propertyName + " for XADatasource " + Arrays.toString(fields.toArray()));
                            }
                        }                        
                    }
                    dataSource.setXaDataSourceInstance(xaDataSource);
                } else {
                    log.warn("No xadatasource is used, data integrity is not guarantee");
                }

                // else standard datasource
                dataSource.setDriverClassName(driver);
                dataSource.setUrl(host);
                dataSource.setUsername(username);
                dataSource.setPassword(password);

                TransactionManager tm = tx.getTransactionManager();
                dataSource.setTransactionManager(tm);
                
                dataSources.put(jdbcUrl, dataSource);
            }
            
            WikittyManagedDataSource dataSource = dataSources.get(jdbcUrl);
            Connection connection = dataSource.getConnection();

            return connection;
            
        } catch(Exception eee) {

            String msg = String.format(
                    "Can't connect to database %s %s with login %s",
                    driver, host, username);

            log.error(msg, eee);
            throw new WikittyException(msg, eee);
        }
    }

    /**
     * Closes a connection.
     *
     * @param connection the connection to close
     */
    public static void closeQuietly(Connection connection) {
        try {
            if (connection != null) {
                // delegate close can throw NPE
                // so this is necessary to catch all Exceptions
                connection.close();
            }
        } catch (Exception e) {
            log.error("Exception while closing connection", e);
        }
    }

    /**
     * Get a new connection instance (i.e. it opens a new transaction).
     * This connection must be close be the asker after used
     *
     * @return a new Connection (db transaction)
     * @throws WikittyException if the connection fails
     */
    public static synchronized Connection getConnection(
            WikittyTransaction tx, ApplicationConfig config) {
        try {
            Connection connection = getConnectionUnconfigured(tx, config);
            if (connection.getAutoCommit()) {
                connection.setAutoCommit(false);
            }
            return connection;
        } catch(SQLException eee) {
            throw new WikittyException(
                    "Can't set connection auto commit to false", eee);
        }
    }

    public static boolean tableExist(Connection connection, String tableName) throws SQLException {

        // In Postgres, tablenames are in lower case, in H2, are in upper case, so we test both
        DatabaseMetaData metaData = connection.getMetaData();

        boolean existInLowerCase = metaData.getTables(null, null, tableName, null).next();
        boolean existInUpperCase = metaData.getTables(null, null, tableName.toUpperCase(), null).next();

        return existInLowerCase || existInUpperCase;
    }

    public static String getSupportedBinaryType(Connection connection) throws SQLException {

        ResultSet typeInfo = connection.getMetaData().getTypeInfo();
        while (typeInfo.next()) {
            String type = typeInfo.getString("TYPE_NAME");
            // In Postgres, types are in lower case, in H2, are in upper case, so we test both
            if (ArrayUtils.contains(BINARY_TYPES, type.toLowerCase())) {
                return type;
            }
        }
        return null;
    }

    // REMOVED because, we must used WikittyTransaction with jta management
//    /**
//     * Closes a connection (i.e. transaction) and commit data.
//     *
//     * @param connection the connection to close and commit
//     */
//    public static void commitJDBCConnection(Connection connection) {
//        try {
//            connection.commit();
//        } catch(SQLException eee) {
//            throw new WikittyException("Can't commit transaction", eee);
//        } finally {
//            try {
//                connection.close();
//            }  catch(SQLException eee) {
//                throw new WikittyException("Can't close connection", eee);
//            }
//        }
//    }
//
//    /**
//     * Closes a connection (i.e. transaction) and rollback data.
//     *
//     * @param connection the connection to close and rollback
//     */
//    public static void rollbackJDBCConnection(Connection connection) {
//        try {
//            connection.rollback();
//        } catch(SQLException eee) {
//            throw new WikittyException("Can't rollback transaction", eee);
//        } finally {
//            try {
//                connection.close();
//            }  catch(SQLException eee) {
//                throw new WikittyException("Can't close connection", eee);
//            }
//        }
//    }

    /**
     * Execute query.
     *
     * @param connection connection to use
     * @param query sql query to do
     * @param args arguments for the query
     */
    static public void doQuery(Connection connection, String query, Object ... args) throws SQLException {
        PreparedStatement sta = connection.prepareStatement(query);
        for (int i=0; i<args.length; i++) {
            if (args[i] instanceof Date) {
                // force for java.util.Date to TIMESTAMP, because some driver
                // (Postgresql) don't support it naturaly
                sta.setObject(i + 1, args[i], Types.TIMESTAMP);
            } else {
                sta.setObject(i + 1, args[i]);
            }
        }
        sta.execute();
    }
}
