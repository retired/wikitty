/*
 * #%L
 * Wikitty :: wikitty-jdbc
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty;

import org.nuiton.wikitty.jdbc.WikittyExtensionStorageJDBC;
import org.nuiton.wikitty.jdbc.WikittyStorageJDBC;
import org.nuiton.wikitty.services.WikittyServiceStorage;
import org.nuiton.wikitty.storage.WikittyExtensionStorage;
import org.nuiton.wikitty.storage.WikittySearchEngine;
import org.nuiton.wikitty.storage.WikittyStorage;
import org.nuiton.wikitty.storage.solr.WikittySearchEngineSolr;

/**
 * Wikitty client test over JDBC implementation.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class WikittyClientJdbcTest extends WikittyClientTest {

    @Override
    protected WikittyClient getWikittyClient() {

        WikittyExtensionStorage extStorage = new WikittyExtensionStorageJDBC(wikittyConfig);
        WikittyStorage wikittyStorage = new WikittyStorageJDBC(wikittyConfig, extStorage);
        WikittySearchEngine searchEngine = new WikittySearchEngineSolr(wikittyConfig, extStorage);

        WikittyServiceStorage wikittyService = new WikittyServiceStorage(wikittyConfig,
                extStorage, wikittyStorage, searchEngine);
        WikittyClient client = new WikittyClient(wikittyConfig, wikittyService);
        return client;
    }
}
