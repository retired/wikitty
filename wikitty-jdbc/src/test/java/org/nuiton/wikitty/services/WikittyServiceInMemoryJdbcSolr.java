/*
 * #%L
 * Wikitty :: wikitty-jdbc
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import java.io.File;
import java.util.UUID;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.core.RAMDirectoryFactory;
import org.nuiton.wikitty.WikittyConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.jdbc.WikittyExtensionStorageJDBC;
import org.nuiton.wikitty.jdbc.WikittyStorageJDBC;
import org.nuiton.wikitty.storage.solr.WikittySearchEngineSolr;

/**
 * In memory implementation that use in memory h2 and in memory solr
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceInMemoryJdbcSolr extends WikittyServiceStorage {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceInMemoryJdbcSolr.class);

    public WikittyServiceInMemoryJdbcSolr() {
        super(WikittyConfig.getConfig(), null, null, null);

        // we use unique db name (this permit to use simultaneously many
        // WikittyServiceInMemoryJdbcSolr)
        String dbName = "wikitty-tx-" + UUID.randomUUID().toString();
        config.setOption(WikittyConfigOption.
                WIKITTY_STORAGE_JDBC_URL.getKey(),
                "jdbc:h2:mem:" + dbName);
        // solr meme en RAMDirectoryFactory peut creer des fichiers si
        // la config est mauvaise, pour prevenir tous problemes on fixe un
        // repertoire unique si jamais ca arrive pour eviter les problemes
        config.setOption(WikittyConfigOption.WIKITTY_DATA_DIR.getKey(),
                config.getOption("java.io.tmpdir") + File.separator + dbName);
        config.setOption(WikittyConfigOption.
                WIKITTY_SEARCHENGINE_SOLR_DIRECTORY_FACTORY.getKey(),
                RAMDirectoryFactory.class.getName());
        // others defaults value in config normaly is correct
        // - WIKITTY_STORAGE_JDBC*
        // - WIKITTY_STORAGE_JDBC_XADATASOURCE*

        extensionStorage = new WikittyExtensionStorageJDBC(config);
        wikittyStorage = new WikittyStorageJDBC(config, extensionStorage);
        searchEngine = new WikittySearchEngineSolr(config, extensionStorage);
    }

}
