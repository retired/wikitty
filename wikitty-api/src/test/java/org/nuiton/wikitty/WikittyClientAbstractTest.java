/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.addons.WikittyImportExportService;
import org.nuiton.wikitty.addons.WikittyImportExportService.FORMAT;
import org.nuiton.wikitty.entities.ExtensionFactory;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyTypes;
import org.nuiton.wikitty.services.WikittyServiceDelegator;
import org.nuiton.wikitty.services.WikittyServiceInMemory;
import org.nuiton.wikitty.test.CatalogNode;
import org.nuiton.wikitty.test.CatalogNodeImpl;
import org.nuiton.wikitty.test.Category;
import org.nuiton.wikitty.test.CategoryImpl;
import org.nuiton.wikitty.test.Product;
import org.nuiton.wikitty.test.ProductImpl;

/**
 * Abstract test for wikitty client.
 * 
 * Just init object, and load defaut and import data. Asserts are subclassed.
 * 
 * Abstract test defini and use to data type:
 * - Business entity : generated from modele
 * - Manual extension : movies
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public abstract class WikittyClientAbstractTest {

    static private Log log = LogFactory.getLog(WikittyClientAbstractTest.class);

    protected static ApplicationConfig wikittyConfig;

    protected WikittyClient wikittyClient;

    protected static DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH);

    /**
     * Certains test utilisent des fonctionnalités avancés des moteurs de
     * recherche et de facetisation et ne peuvent pas fonctionner si
     * l'instance in memory ne supporte pas la fonctionnalité testée.
     */
    protected void assumeTrueSearchEngineCanRunTest() {
        
        // improve imbricated service detection
        WikittyService ws = wikittyClient.getWikittyService();
        while (ws instanceof WikittyServiceDelegator) {
            ws = ((WikittyServiceDelegator)ws).getDelegate();
        }

        boolean isInMomory = ws instanceof WikittyServiceInMemory;
        if (isInMomory) {
            log.warn("Not yet implemented in memory, skipping");
        }
        Assume.assumeTrue(!isInMomory);
    }

    /**
     * Init {@link ApplicationConfig} class.
     */
    @BeforeClass
    public static void setUpConfig() {
        // can't call parse method here
        wikittyConfig = new ApplicationConfig(
                WikittyConfigOption.class, WikittyConfigAction.class,
                null, null);
    }
    
    /**
     * Override to method in sub tests to change wikitty client implementation.
     * 
     * @return wikitty client implementation to use in current test case instance
     */
    protected WikittyClient getWikittyClient() {
        WikittyService wikittyService = new WikittyServiceInMemory(wikittyConfig);
        WikittyClient client = new WikittyClient(wikittyConfig, wikittyService);
        return client;
    }

    /**
     * Clear all data and add some test data for all tests.
     * 
     * After data init, there is in wikitty storage:
     * <ul>
     * <li>5 categories
     * <li>4 books
     * <li>10 catalog node
     * <li>3 movies
     * <li>total = 22 wikitties
     * </ul>
     * 
     * @throws ParseException 
     */
    @Before
    public void setUpTestData() throws ParseException {
        wikittyClient = getWikittyClient();
        wikittyClient.clear();
        addTestDataInClient(wikittyClient); // add 21
    }

    /** Manual extension name : media. */
    protected static final String MEDIA_EXTENSION_NAME = "Media";
    /** Manual extension media. */
    protected static final WikittyExtension MEDIA_EXTENSION =
            ExtensionFactory.create(MEDIA_EXTENSION_NAME, "6.0")
            .addField("type", WikittyTypes.STRING)
            .extension();
    protected static final String MOVIE_EXTENSION_NAME = "Movies";
    /** Manual extension movies. */
    protected static final WikittyExtension MOVIE_EXTENSION =
            ExtensionFactory.create(MOVIE_EXTENSION_NAME, "2.0", MEDIA_EXTENSION_NAME)
            .addField("name", WikittyTypes.STRING)
            .addField("authors", WikittyTypes.STRING).maxOccur(Integer.MAX_VALUE)
            .addField("category", WikittyTypes.WIKITTY)
            .addField("date", WikittyTypes.DATE)
            .extension();

    /**
     * Add some wikitty in client.
     * 
     * In test case, product are books, movies structured in a media library.
     * 
     * @param client wikitty client
     * @throws ParseException 
     */
    protected void addTestDataInClient(WikittyClient client) throws ParseException {

        // ###########################
        // add some business wikitties
        // ###########################

        // Categories (5)
        Category sf = new CategoryImpl("sf", "science fiction");
        Category history = new CategoryImpl("history", "history");
        Category society = new CategoryImpl("society", "société");
        Category fantastic = new CategoryImpl("fan", "fantastique");
        Category fantaisie = new CategoryImpl("hf", "heroique/fantaisie");
        client.store(sf, society, fantastic, fantaisie, history);

        // Product (multiple inheritance) (4)
        Product book42 = new ProductImpl("Answer to life the universe and everything");
        book42.setWikittyVersion("2.0");
        book42.setPrice(4200);
        book42.setPicturePrice(420);
        book42.setPriceFromProduct(42);
        book42.addColors("white", "black");
        book42.setDate(df.parse("December 25, 1983"));
        book42.setCategory(sf.getWikittyId());

        Product bookIndignez = new ProductImpl("Indignez-vous !");
        bookIndignez.setPrice(1);
        bookIndignez.setPicturePrice(2);
        bookIndignez.setPriceFromProduct(15);
        bookIndignez.setCategory(sf.getWikittyId());
        bookIndignez.addColors("white");
        bookIndignez.setDate(df.parse("April 12, 2011"));
        bookIndignez.setCategory(society.getWikittyId());

        Product bookLotr = new ProductImpl("Le seigneur des anneaux");
        bookLotr.setPrice(11);
        bookLotr.setPicturePrice(12);
        bookLotr.setPriceFromProduct(100);
        bookLotr.setCategory(sf.getWikittyId());
        bookLotr.setDate(df.parse("July 9, 1953"));
        bookLotr.addColors("red", "blue");

        Product bookLan = new ProductImpl("Lanfeust");
        bookLan.setWikittyVersion("3.0");
        bookLan.setPrice(0);
        bookLan.setPicturePrice(5);
        bookLan.setPriceFromProduct(13);
        bookLan.setCategory(fantastic.getWikittyId());
        bookLan.addColors("red", "yellow");
        bookLan.setDate(df.parse("January 12, 2002"));
        client.store(book42, bookIndignez, bookLotr, bookLan);

        // #########################
        // add some manual wikitties
        // #########################
        client.storeExtension(MOVIE_EXTENSION);

        // create wikitty movies (3)
        Wikitty dieHardMovie = new WikittyImpl();
        dieHardMovie.addExtension(MEDIA_EXTENSION);
        dieHardMovie.addExtension(MOVIE_EXTENSION);
        dieHardMovie.setField(MOVIE_EXTENSION_NAME, "name", "Die hard 4");
        dieHardMovie.addToField(MOVIE_EXTENSION_NAME, "authors", "Willis");
        dieHardMovie.setField(MOVIE_EXTENSION_NAME, "date", "04/02/2009");

        Wikitty edgarMovie = new WikittyImpl();
        edgarMovie.addExtension(MEDIA_EXTENSION);
        edgarMovie.addExtension(MOVIE_EXTENSION);
        edgarMovie.setField(MOVIE_EXTENSION_NAME, "name", "J. Edgar");
        edgarMovie.addToField(MOVIE_EXTENSION_NAME, "authors", "Eastwood");
        edgarMovie.setField(MOVIE_EXTENSION_NAME, "date", "25/12/2011");

        Wikitty dnrMovie = new WikittyImpl();
        dnrMovie.addExtension(MEDIA_EXTENSION);
        dnrMovie.addExtension(MOVIE_EXTENSION);
        dnrMovie.setField(MOVIE_EXTENSION_NAME, "name", "The Dark Knight Rises");
        dnrMovie.addToField(MOVIE_EXTENSION_NAME, "authors", "Nolan");
        dnrMovie.setField(MOVIE_EXTENSION_NAME, "date", "13/03/2012");

        client.store(dieHardMovie, edgarMovie, dnrMovie);


        // ########################################################
        // build tree node with previous wikitties attached into it
        // ########################################################
        
        // Create tree as following : (10)
        // Catalog
        // |_ Media
        // |  |_ Books
        // |  |  |_ Bande dessinées (1)
        // |  |  |_ Nouvelles (1)
        // |  |  |_ Roman (2)
        // |  |_ Movies
        // |  |  |_ Action (2)
        // |  |  |_ Biopic (1)
        // |_ Everything else
        CatalogNode catalogNode = new CatalogNodeImpl("Catalog");
        CatalogNode mediaNode = new CatalogNodeImpl("Media");
        mediaNode.setParent(catalogNode.getWikittyId());
        CatalogNode bookNode = new CatalogNodeImpl("Books");
        bookNode.setParent(mediaNode.getWikittyId());
        CatalogNode bdNode = new CatalogNodeImpl("Bande dessinées");
        bdNode.setParent(bookNode.getWikittyId());
        bdNode.addAttachment(bookLan.getWikittyId());
        CatalogNode newsNode = new CatalogNodeImpl("Nouvelles");
        newsNode.setParent(bookNode.getWikittyId());
        newsNode.addAttachment(bookIndignez.getWikittyId());
        CatalogNode romanNode = new CatalogNodeImpl("Roman");
        romanNode.setParent(bookNode.getWikittyId());
        romanNode.addAttachment(book42.getWikittyId(), bookLotr.getWikittyId());
        CatalogNode moviesNode = new CatalogNodeImpl("Movies");
        moviesNode.setParent(mediaNode.getWikittyId());
        CatalogNode actionNode = new CatalogNodeImpl("Action");
        actionNode.setParent(moviesNode.getWikittyId());
        actionNode.addAttachment(dieHardMovie.getWikittyId(), dnrMovie.getWikittyId());
        CatalogNode biopicNode = new CatalogNodeImpl("Biopic");
        biopicNode.setParent(moviesNode.getWikittyId());
        biopicNode.addAttachment(edgarMovie.getWikittyId());
        CatalogNode otherNode = new CatalogNodeImpl("Everything else");
        otherNode.setParent(catalogNode.getWikittyId());
        client.store(catalogNode, mediaNode, bookNode, bdNode, newsNode,
                romanNode, moviesNode, actionNode, biopicNode, otherNode);
    }

    /**
     * Import books from csv files (13 books)
     * 
     * @throws IOException 
     */
    protected void importBooks() throws IOException {
        WikittyImportExportService ieService = new WikittyImportExportService(wikittyClient);

        InputStreamReader reader = null;
        try {
            InputStream is = WikittyClientAbstractTest.class.getResourceAsStream("/csv/importbooks.csv");
            reader = new InputStreamReader(is);
            ieService.syncImport(FORMAT.CSV, reader);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    /**
     * Import books from csv files.
     * 
     * @throws IOException 
     */
    protected void importClients() throws IOException {
        
        // client and tag extensions (used in some imports)
        wikittyClient.storeExtension(new WikittyExtension("Client", "1.0",
                        WikittyUtil.buildFieldMapExtension(
                            "String name")));
        wikittyClient.storeExtension(new WikittyExtension("Tag", "1.0",
                        WikittyUtil.buildFieldMapExtension(
                            "String tags")));
        
        WikittyImportExportService ieService = new WikittyImportExportService(wikittyClient);

        String[] importFiles = {
                "/csv/importclient.csv",
                "/csv/importtree.csv",
                "/csv/importtree2.csv"};
        for (String importFile : importFiles) {
            InputStreamReader reader = null;
            try {
                InputStream is = WikittyClientAbstractTest.class.getResourceAsStream(importFile);
                reader = new InputStreamReader(is);
                ieService.syncImport(FORMAT.CSV, reader);
            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
        }
    }
}
