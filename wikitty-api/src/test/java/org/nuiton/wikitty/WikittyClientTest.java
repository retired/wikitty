/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.wikitty.addons.WikittyI18nTestUtil;
import org.nuiton.wikitty.addons.WikittyI18nUtil;
import org.nuiton.wikitty.addons.WikittyImportExportService;
import org.nuiton.wikitty.addons.WikittyImportExportService.FORMAT;
import org.nuiton.wikitty.addons.WikittyLabelUtil;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.entities.ElementField;
import org.nuiton.wikitty.entities.ExtensionFactory;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyAuthorisation;
import org.nuiton.wikitty.entities.WikittyAuthorisationImpl;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyGroup;
import org.nuiton.wikitty.entities.WikittyGroupImpl;
import org.nuiton.wikitty.entities.WikittyI18n;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.entities.WikittyLabelImpl;
import org.nuiton.wikitty.entities.WikittyTreeNode;
import org.nuiton.wikitty.entities.WikittyTreeNodeHelper;
import org.nuiton.wikitty.entities.WikittyTreeNodeImpl;
import org.nuiton.wikitty.entities.WikittyTypes;
import org.nuiton.wikitty.entities.WikittyUserImpl;
import org.nuiton.wikitty.query.FacetSortType;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryParser;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.query.WikittyQueryResultTreeNode;
import org.nuiton.wikitty.query.function.FunctionAvg;
import org.nuiton.wikitty.query.function.FunctionCount;
import org.nuiton.wikitty.query.function.FunctionFieldValue;
import org.nuiton.wikitty.query.function.FunctionMax;
import org.nuiton.wikitty.query.function.FunctionMin;
import org.nuiton.wikitty.query.function.FunctionSum;
import org.nuiton.wikitty.services.WikittyEvent;
import org.nuiton.wikitty.test.CatalogNode;
import org.nuiton.wikitty.test.Category;
import org.nuiton.wikitty.test.Product;

/**
 * Wikitty client test to test for client use.
 * 
 * This test is designed to be overridden and change {@link WikittyClient}
 * implementation.
 * 
 * Test begin with 'testStorage' focus on store/restore.
 * Test begin with 'testFind' or 'testSearch' focus on search engine.
 *  
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class WikittyClientTest extends WikittyClientAbstractTest {

    static private Log log = LogFactory.getLog(WikittyClientTest.class);

    @Test
    public void testStorageRestoreBasics() {
        // create basic wikitty
        Wikitty w = new WikittyImpl();
        w.addExtension(MEDIA_EXTENSION);
        w.addExtension(MOVIE_EXTENSION);

        String id = w.getWikittyId();
        wikittyClient.store(w);
        w = wikittyClient.restore(id);
        Assert.assertTrue(w.hasExtension(MOVIE_EXTENSION_NAME));
        Assert.assertTrue(w.hasField(MOVIE_EXTENSION_NAME, "name"));
        Assert.assertTrue(w.hasField(MOVIE_EXTENSION_NAME, "authors"));
        Assert.assertTrue(w.hasField(MOVIE_EXTENSION_NAME, "category"));
        Assert.assertTrue(w.hasField(MOVIE_EXTENSION_NAME, "date"));
    }

    @Test
    public void testStorageExtensionMethod() {
        // store extension
        List<String> extIds = wikittyClient.getAllExtensionIds();
        log.debug("extIds: " + extIds);
        Assert.assertEquals(8, extIds.size());
        Assert.assertTrue(extIds.contains(MOVIE_EXTENSION_NAME+"[2.0]"));
        WikittyExtension ext = wikittyClient.restoreExtension(MOVIE_EXTENSION_NAME+"[2.0]");
        Assert.assertEquals(MOVIE_EXTENSION_NAME, ext.getName());
        Assert.assertEquals("2.0", ext.getVersion());
        Assert.assertEquals(MOVIE_EXTENSION, ext);
    }

    /**
     * Test que la suppression d'une extension est possible.
     */
    @Test
    public void testStorageExtensionDelete() {
        // new one
        WikittyExtension volatileExt = new WikittyExtension("VolatileExt",
                "4.0", // version
                WikittyUtil.buildFieldMapExtension( // building field map
                        "String name unique=\"true\""));
        wikittyClient.storeExtension(volatileExt);

        // test existance
        List<String> extIds = wikittyClient.getAllExtensionIds();
        Assert.assertTrue(extIds.contains("VolatileExt[4.0]"));
        
        // delete
        wikittyClient.deleteExtension("VolatileExt");
        
        // restest existance
        extIds = wikittyClient.getAllExtensionIds();
        Assert.assertFalse(extIds.contains("VolatileExt[4.0]"));
    }

    /**
     * Test que la suppression echoue si l'extension est utilisee.
     */
    @Test(expected=WikittyException.class)
    public void testStorageExtensionDeleteUsed() {
        // try to delete it (throws WikittyException)
        wikittyClient.deleteExtension(MOVIE_EXTENSION_NAME);
    }

    @Test
    public void testStorageExtensionRequires() {
        wikittyClient.storeExtension(MEDIA_EXTENSION);
        wikittyClient.storeExtension(MOVIE_EXTENSION);
        List<String> extIds = wikittyClient.getAllExtensionsRequires(MEDIA_EXTENSION_NAME);
        log.debug("extIds: " + extIds);
        Assert.assertEquals(1, extIds.size());

        WikittyExtension ext = wikittyClient.restoreExtension(extIds.get(0));
        Assert.assertEquals(MOVIE_EXTENSION_NAME, ext.getName());
        Assert.assertEquals("2.0", ext.getVersion());
        Assert.assertEquals(MOVIE_EXTENSION, ext);
    }

    /**
     * Test seulement la methode restore().
     * 
     * @throws ParseException 
     */
    @Test
    public void testStorageRestoreLists() throws ParseException {

        // create some wikitty
        Wikitty gf1Movie = new WikittyImpl();
        gf1Movie.addExtension(MEDIA_EXTENSION);
        gf1Movie.addExtension(MOVIE_EXTENSION);
        gf1Movie.setField(MOVIE_EXTENSION_NAME, "name", "The godfather");
        gf1Movie.addToField(MOVIE_EXTENSION_NAME, "authors", "Coppola");
        gf1Movie.setField(MOVIE_EXTENSION_NAME, "date", WikittyUtil.formatDate(df.parse("March 15, 1972")));

        Wikitty gf2Movie = new WikittyImpl();
        gf2Movie.addExtension(MEDIA_EXTENSION);
        gf2Movie.addExtension(MOVIE_EXTENSION);
        gf2Movie.setField(MOVIE_EXTENSION_NAME, "name", "The godfather 2");
        gf2Movie.addToField(MOVIE_EXTENSION_NAME, "authors", "Coppola");
        gf2Movie.setField(MOVIE_EXTENSION_NAME, "date", WikittyUtil.formatDate(df.parse("October 12, 1974")));
        
        Wikitty gf3Movie = new WikittyImpl();
        gf3Movie.addExtension(MEDIA_EXTENSION);
        gf3Movie.addExtension(MOVIE_EXTENSION);
        gf3Movie.setField(MOVIE_EXTENSION_NAME, "name", "The godfather 3");
        gf3Movie.addToField(MOVIE_EXTENSION_NAME, "authors", "Coppola");
        gf3Movie.setField(MOVIE_EXTENSION_NAME, "date", WikittyUtil.formatDate(df.parse("July 30, 1990")));

        wikittyClient.store(gf1Movie, gf2Movie, gf3Movie);

        List<String> ids = new ArrayList<String>();
        ids.add(gf1Movie.getWikittyId());
        ids.add(gf2Movie.getWikittyId());
        ids.add(gf3Movie.getWikittyId());

        List<Wikitty> wikitties = wikittyClient.restore(ids);

        Assert.assertEquals("The godfather", wikitties.get(0).getFieldAsString(MOVIE_EXTENSION_NAME, "name"));
        Assert.assertEquals("The godfather 2", wikitties.get(1).getFieldAsString(MOVIE_EXTENSION_NAME, "name"));
        Assert.assertEquals("The godfather 3", wikitties.get(2).getFieldAsString(MOVIE_EXTENSION_NAME, "name"));
    }

    @Test
    public void testStorageRestoreSingle() throws Exception {
        Wikitty w = new WikittyImpl();
        String id = w.getWikittyId();
        wikittyClient.store(w);
        w = wikittyClient.restore(id);
        Assert.assertEquals(id, w.getWikittyId());
    }

    @Test
    public void testStorageRestoreNull() throws Exception {
        Wikitty result = wikittyClient.store((Wikitty)null);
        Assert.assertNull(result);
    }

    @Test
    public void testFieldConstraint() throws Exception {
        // Store ext
        WikittyExtension ext = new WikittyExtension("TestConstraint", "1",
            WikittyUtil.buildFieldMapExtension(
                "String id notNull=true",
                "String ext[0-n] unique=true",
                "String other[0-n] unique=true notNull=true"
        ));
        wikittyClient.storeExtension(ext);
        
        // store wikitty
        Wikitty w = new WikittyImpl();
        w.addExtension(ext);

        try {
            w = wikittyClient.store(w);
            Assert.fail("not null contraint don't work on String");
        } catch (WikittyException eee) {
//            eee.printStackTrace();
            // ok id must not be null
        }

        w.setField("TestConstraint", "id", "toto");
        try {
            w = wikittyClient.store(w);
            Assert.fail("not null contraint don't work in Collection");
        } catch (WikittyException eee) {
//            eee.printStackTrace();
            // ok id must not be null
        }

        w.addToField("TestConstraint", "other", "titi");
        w = wikittyClient.store(w);

        w.addToField("TestConstraint", "ext", "tata");
        w.addToField("TestConstraint", "ext", "titi"); // titi ne doit pas s'ajouter
        w = wikittyClient.store(w);

        Wikitty w2 = wikittyClient.restore(w.getWikittyId());
        w2.addToField("TestConstraint", "ext", "tata"); // tata ne doit pas s'ajouter
        w2.addToField("TestConstraint", "ext", "toto");

        wikittyClient.store(w2);
        Wikitty w3 = wikittyClient.restore(w.getWikittyId());

        Set<String> set = new HashSet<String>(Arrays.asList("tata", "titi", "toto"));
        Assert.assertEquals(set, w3.getFieldAsSet("TestConstraint", "ext", String.class));
    }

    @Test
    public void testSearchByExampleFacet() throws Exception {
        // create some wikitty
        Wikitty gf1Movie = new WikittyImpl();
        gf1Movie.addExtension(MEDIA_EXTENSION);
        gf1Movie.addExtension(MOVIE_EXTENSION);
        gf1Movie.setField(MEDIA_EXTENSION_NAME, "type", "movie");
        gf1Movie.setField(MOVIE_EXTENSION_NAME, "name", "The godfather");
        gf1Movie.addToField(MOVIE_EXTENSION_NAME, "authors", "Coppola");
        gf1Movie.setField(MOVIE_EXTENSION_NAME, "date", WikittyUtil.formatDate(df.parse("March 15, 1972")));

        Wikitty gf2Movie = new WikittyImpl();
        gf2Movie.addExtension(MEDIA_EXTENSION);
        gf2Movie.addExtension(MOVIE_EXTENSION);
        gf2Movie.setField(MEDIA_EXTENSION_NAME, "type", "movie");
        gf2Movie.setField(MOVIE_EXTENSION_NAME, "name", "The godfather 2");
        gf2Movie.addToField(MOVIE_EXTENSION_NAME, "authors", "Coppola");
        gf2Movie.setField(MOVIE_EXTENSION_NAME, "date", WikittyUtil.formatDate(df.parse("October 12, 1974")));

        Wikitty gf3Movie = new WikittyImpl();
        gf3Movie.addExtension(MEDIA_EXTENSION);
        gf3Movie.addExtension(MOVIE_EXTENSION);
        gf3Movie.setField(MEDIA_EXTENSION_NAME, "type", "movie");
        gf3Movie.setField(MOVIE_EXTENSION_NAME, "name", "Fly me to the moon");
        gf3Movie.addToField(MOVIE_EXTENSION_NAME, "authors", "Pixar");
        gf3Movie.setField(MOVIE_EXTENSION_NAME, "date", WikittyUtil.formatDate(df.parse("October 12, 1974")));

        wikittyClient.store(gf1Movie, gf2Movie, gf3Movie);

        // search test
        Wikitty w = new WikittyImpl();
        w.addExtension(MEDIA_EXTENSION);
        w.addExtension(MOVIE_EXTENSION);
        w.setField(MEDIA_EXTENSION_NAME, "type", "movie");
        WikittyQuery query = new WikittyQueryMaker().wikitty(w).end();
        query.addFacetField(new ElementField(MOVIE_EXTENSION_NAME,"authors"));
        query.addFacetField(new ElementField(MOVIE_EXTENSION_NAME,"date"));
        WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);

        Assert.assertTrue(result.getFacetNames().contains(MOVIE_EXTENSION_NAME + ".date"));
        Assert.assertTrue(result.getFacetNames().contains(MOVIE_EXTENSION_NAME + ".authors"));

        // with must have 2 topic: Pixar and Coppola
        Assert.assertEquals(2, result.getTopic(MOVIE_EXTENSION_NAME + ".authors").size());
        Assert.assertEquals("Coppola", result.getTopic(MOVIE_EXTENSION_NAME + ".authors").get(0).getTopicName());
        Assert.assertEquals(2, result.getTopic(MOVIE_EXTENSION_NAME + ".authors").get(0).getCount());
        Assert.assertEquals("Pixar", result.getTopic(MOVIE_EXTENSION_NAME + ".authors").get(1).getTopicName());
        Assert.assertEquals(1, result.getTopic(MOVIE_EXTENSION_NAME + ".authors").get(1).getCount());
        // with must have 2 topic: March 15, 1972 and October 12, 1974
        Assert.assertEquals(2, result.getTopic(MOVIE_EXTENSION_NAME + ".date").size());
    }
    
    @Test
    public void testSearchExtensionFacetExplicitly() throws Exception {
        // essai de facettiser sur les extensions
        WikittyQuery query = new WikittyQueryMaker().keyword("*").end();
        query.setOffset(0);
        query.setLimit(0);
        query.setFacetExtension(true);
        WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(1, result.getFacetNames().size());
        //TODO echatellier 20120112 : revue this code
        //Assert.assertNotNull(result.getFacets().get(Element.EXTENSION));
        // is more convenient, but wont work
        Assert.assertNotNull(result.getFacets().get(Element.EXTENSION.getValue()));
    }

    @Test
    public void testSearchExtensionFacet() throws Exception {
        // essai de facettiser sur les extensions
        WikittyQuery query = new WikittyQueryMaker().keyword("*").end();
        query.setOffset(0);
        query.setLimit(0);
        query.setFacetField(Element.EXTENSION);
        WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(1, result.getFacetNames().size());
        //TODO echatellier 20120112 : revue this code
        //Assert.assertNotNull(result.getFacets().get(Element.EXTENSION));
        // is more convenient, but wont work
        Assert.assertNotNull(result.getFacets().get(Element.EXTENSION.getValue()));
    }

    /**
     * Test qu'une recherche fonctionne et que la même recherche apres
     * suppression ne trouve plus l'element.
     * 
     * @throws ParseException 
     */
    @Test
    public void testStorageFindDelete() throws ParseException {

        // create some wikitty
        Wikitty gf1Movie = new WikittyImpl();
        gf1Movie.addExtension(MEDIA_EXTENSION);
        gf1Movie.addExtension(MOVIE_EXTENSION);
        gf1Movie.setField(MEDIA_EXTENSION_NAME, "type", "movie");
        gf1Movie.setField(MOVIE_EXTENSION_NAME, "name", "The godfather");
        gf1Movie.addToField(MOVIE_EXTENSION_NAME, "authors", "Coppola");
        gf1Movie.setField(MOVIE_EXTENSION_NAME, "date", WikittyUtil.formatDate(df.parse("March 15, 1972")));
        String oldVersion = gf1Movie.getWikittyVersion();

        Wikitty newWik = wikittyClient.store(gf1Movie);
        Assert.assertTrue(WikittyUtil.versionGreaterThan(newWik.getWikittyVersion(), oldVersion));

        // search test
        Wikitty w = new WikittyImpl();
        w.addExtension(MEDIA_EXTENSION);
        w.addExtension(MOVIE_EXTENSION);
        w.setField(MOVIE_EXTENSION_NAME, "name", "The godfather");
        WikittyQuery query = new WikittyQueryMaker().wikitty(w).end();
        Wikitty resultFind = wikittyClient.findByQuery(Wikitty.class, query);

        Assert.assertEquals(w.getFieldAsString(MOVIE_EXTENSION_NAME, "name"),
                resultFind.getFieldAsString(MOVIE_EXTENSION_NAME, "name"));

        Assert.assertEquals(gf1Movie.getWikittyId(), resultFind.getWikittyId());
        // test equals implantation method
        Assert.assertEquals(gf1Movie, resultFind);

        Assert.assertEquals(gf1Movie.getFieldAsString(MOVIE_EXTENSION_NAME, "name"),
                resultFind.getFieldAsString(MOVIE_EXTENSION_NAME, "name"));
        Assert.assertEquals(gf1Movie.getFieldAsDate(MOVIE_EXTENSION_NAME, "date"),
                resultFind.getFieldAsDate(MOVIE_EXTENSION_NAME, "date"));
        Assert.assertEquals(gf1Movie.getFieldAsSet(MOVIE_EXTENSION_NAME, "authors", String.class),
                resultFind.getFieldAsSet(MOVIE_EXTENSION_NAME, "authors", String.class));

        // test to find deleted wikitty
        // test if solr index is coherent with database
        WikittyQuery query1 = new WikittyQueryMaker().ideq(gf1Movie.getWikittyId()).end();
        Wikitty searchedWikitty1 = wikittyClient.findByQuery(Wikitty.class, query1);
        Assert.assertNotNull(searchedWikitty1);

        wikittyClient.delete(gf1Movie.getWikittyId());
        Wikitty deletedObject = wikittyClient.restore(gf1Movie.getWikittyId());
        Assert.assertNull(deletedObject);

        // test to find deleted wikitty
        // test if solr index is coherent with database   
        Wikitty searchedWikitty2 = wikittyClient.findByQuery(Wikitty.class, query1);
        Assert.assertNull(searchedWikitty2);
    }

    @Test
    public void testStorageAndLabel() throws Exception {

        // create some wikitty to permit search test
        WikittyQuery query1 = new WikittyQueryMaker().exteq(MOVIE_EXTENSION_NAME).end();
        List<Wikitty> wikitties = wikittyClient.findAllByQuery(Wikitty.class, query1).getAll();
        long ts = new Date().getTime();

        // labelisation test
        Wikitty w1 = wikitties.get(0);
        WikittyLabelUtil.addLabel(wikittyClient, w1.getWikittyId(), "titi"+ts);
        WikittyLabelUtil.addLabel(wikittyClient, w1.getWikittyId(), "toto"+ts);

        Wikitty w2 = wikitties.get(1);
        WikittyLabelUtil.addLabel(wikittyClient, w2.getWikittyId(), "tata"+ts);
        WikittyLabelUtil.addLabel(wikittyClient, w2.getWikittyId(), "titi"+ts);

        Wikitty w3 = wikitties.get(2);
        WikittyLabelUtil.addLabel(wikittyClient, w3.getWikittyId(), "tutu"+ts);
        WikittyLabelUtil.addLabel(wikittyClient, w3.getWikittyId(), "titi"+ts);

        Wikitty wt = WikittyLabelUtil.findByLabel(wikittyClient, "toto"+ts );
        Assert.assertEquals(w1, wt);

        Set<String> labels = WikittyLabelUtil.findAllAppliedLabels(wikittyClient, w2.getWikittyId());
        Assert.assertEquals(new HashSet<String>(Arrays.asList("tata"+ts, "titi"+ts)), labels);
    }

    @Test
    public void testStorageAndClassification() throws Exception {
        assumeTrueSearchEngineCanRunTest(); // not supported

        // create some wikitty to permit search test
        WikittyQuery query1 = new WikittyQueryMaker().exteq(MOVIE_EXTENSION_NAME).end();
        List<Wikitty> wikitties = wikittyClient.findAllByQuery(Wikitty.class, query1).getAll();
        List<Wikitty> wikittyNodes = new ArrayList<Wikitty>();

        WikittyTreeNodeImpl root = new WikittyTreeNodeImpl();
        wikittyNodes.add(root.getWikitty());
        root.setName("MyCategoryRoot");
        for ( int i = 0; i < 3; i++ ) {
            WikittyTreeNodeImpl leaf = new WikittyTreeNodeImpl();
            wikittyNodes.add( leaf.getWikitty() );
            leaf.setName( "cat-"+i );
            leaf.setParent( root.getWikittyId() );
            // root.addChild( leaf.getWikittyIdNNN() );

            for ( int j = 0; j < 5; j++ ) {
                WikittyTreeNodeImpl subLeaf = new WikittyTreeNodeImpl();
                subLeaf.setName( "subcat-"+i+"-"+j );
                subLeaf.setParent( leaf.getWikittyId() );
                wikittyNodes.add( subLeaf.getWikitty() );
                // leaf.addChild( subLeaf.getWikittyIdNNN() );
            }
        }
        wikittyClient.storeWikitty(wikittyNodes); // store treeNodes.

        Wikitty dieHard = wikitties.get(0);
        Wikitty edgar = wikitties.get(1);
        Wikitty knight = wikitties.get(2);

        assign( dieHard, root, "cat-1/subcat-1-4" );
        assign( edgar, root, "cat-1" );
        assign( knight, root, "cat-2/subcat-2-4" );

        WikittyQueryResultTreeNode<WikittyTreeNode> t = wikittyClient.findTreeNode(
                WikittyTreeNode.class,
                root.getWikitty().getWikittyId(), 0, false, null);
        Assert.assertEquals("MyCategoryRoot", t.getObject().getName());
    }

    /**
     * @deprecated remove this undocumented method
     */
    @Deprecated
    protected void assign(Wikitty wikitty, WikittyTreeNodeImpl root, String path) {
        String[] nodeNames = path.split("/");
        WikittyTreeNodeImpl currentNode = root;
        outerloop : for( String nodeName : nodeNames ) {
            WikittyTreeNodeImpl node = new WikittyTreeNodeImpl();
            node.setName(nodeName);

            WikittyQuery query = new WikittyQueryMaker().wikitty(node.getWikitty()).end();
            List<String> wikittiesId = wikittyClient.findAllByQuery(query).getAll();
            List<Wikitty> wikitties = wikittyClient.restore(wikittiesId);
            for ( Wikitty child : wikitties ) {
                if (!child.hasExtension(WikittyTreeNode.EXT_WIKITTYTREENODE)) {
                    continue;
                }
                node = new WikittyTreeNodeImpl(child);
                if ( node.getName().equals(nodeName) ) {
                    currentNode = node;
                    continue outerloop;
                }
            }
            Assert.fail( "Unable to find node " + nodeName + " inside " + currentNode.getName() );
        }
        currentNode.addAttachment(wikitty.getWikittyId());
        wikittyClient.store(currentNode.getWikitty());
    }

    /**
     * Test de la methode cast du wikitty client.
     */
    @Test
    public void testStorageCastTo() {
        WikittyTreeNode node = new WikittyTreeNodeImpl();
        node.setName("nodeName");

        // cast, different business object, same wikitty
        WikittyLabel label = wikittyClient.castTo(WikittyLabel.class, node);
        label.addLabels("testlabel");

        label = wikittyClient.store(label);
        String wikittyId = label.getWikittyId();

        WikittyTreeNode node2 = wikittyClient.restore(WikittyTreeNode.class, wikittyId);
        Assert.assertEquals("nodeName", node2.getName());

        WikittyLabel label2 = wikittyClient.castTo(WikittyLabel.class, node);
        List<String> labels = new ArrayList<String>(label2.getLabels());
        Assert.assertFalse(labels.isEmpty());
        Assert.assertEquals("testlabel", labels.get(0));
    }

    /**
     * Test les differents cas du forcage de version lors du store.
     */
    @Test
    public void testStorageVersionForce() {
        // store 1
        Wikitty myWikitty = new WikittyImpl();
        myWikitty.addExtension(WikittyTreeNodeImpl.extensions);
        WikittyTreeNodeHelper.setName(myWikitty, "name");
        myWikitty = wikittyClient.store(myWikitty);
        Assert.assertEquals("1.0", myWikitty.getWikittyVersion());

        // store 2 : no modification
        myWikitty = wikittyClient.store(myWikitty);
        Assert.assertEquals("1.0", myWikitty.getWikittyVersion());

        // store 3 : modification
        WikittyTreeNodeHelper.setName(myWikitty, "new name");
        myWikitty = wikittyClient.store(myWikitty);
        Assert.assertEquals("2.0", myWikitty.getWikittyVersion());

        // store 4 : new wikitty with same wikitty id (obsolete)
        Wikitty myNewWikitty = new WikittyImpl(myWikitty.getWikittyId());
        myNewWikitty.addExtension(WikittyTreeNodeImpl.extensions);
        WikittyTreeNodeHelper.setName(myNewWikitty, "new wikitty");
        try {
            myWikitty = wikittyClient.store(myNewWikitty);
            Assert.fail("Test must throw WikittyObsoleteException");
        }
        catch (WikittyObsoleteException ex) {
            if (log.isTraceEnabled()) {
                log.trace("Wikitty obsolete", ex);
            }
        }

        // store 4 : same but with force (increased by force)
        WikittyEvent event = wikittyClient.getWikittyService().store(
                null, Collections.singletonList(myWikitty), true);
        event.update(myWikitty);
        Assert.assertEquals("3.0", myWikitty.getWikittyVersion());

        // store 5 : fix version (not increased by force)
        myNewWikitty.setWikittyVersion("11.0");
        event = wikittyClient.getWikittyService().store(
                null, Collections.singletonList(myNewWikitty), true);
        event.update(myNewWikitty);
        Assert.assertEquals("11.0", myNewWikitty.getWikittyVersion());
    }
    
    /**
     * Test qu'une sauvegarde de wikitty précédemment supprimé fonctionne.
     */
    @Test
    public void testStoragePreviouslyDeleted() {
        // store 1
        Wikitty myWikitty = new WikittyImpl();
        myWikitty.addExtension(WikittyTreeNodeImpl.extensions);
        WikittyTreeNodeHelper.setName(myWikitty, "name");

        myWikitty = wikittyClient.store(myWikitty);

        // delete
        wikittyClient.delete(myWikitty.getWikittyId());
        Wikitty restoredWikitty = wikittyClient.restore(myWikitty.getWikittyId());
        Assert.assertNull(restoredWikitty);

        // store again
        myWikitty = wikittyClient.store(myWikitty);
        restoredWikitty = wikittyClient.restore(myWikitty.getWikittyId());
        Assert.assertNotNull(restoredWikitty);
    }

    @Test
    public void testStorageBinaryField() {
        String extName = "BinaryExt";
        byte[] bytes = "Coucou le monde".getBytes();

        WikittyExtension BinaryExt = new WikittyExtension(extName,
                                     "1.0", // version
                                     WikittyUtil.buildFieldMapExtension( // building field map
                                             "String name unique=\"true\"",
                                             "Binary content"));
        Wikitty w = new WikittyImpl();
        w.addExtension(BinaryExt);
        w.setField(extName, "name", "LeBin");
        w.setField(extName, "content", bytes);

        w = wikittyClient.store(w);

        Wikitty restoredWikitty = wikittyClient.restore(w.getWikittyId());
        Assert.assertNotNull(restoredWikitty);
        Assert.assertEquals("LeBin", restoredWikitty.getFieldAsString(extName, "name"));
        Assert.assertTrue(Arrays.equals(bytes, restoredWikitty.getFieldAsBytes(extName, "content")));
    }

    @Test
    public void testStoreUnmodifiedEntity() {
        WikittyLabel wikitty1 = new WikittyLabelImpl();
        WikittyLabel wikitty2 = new WikittyLabelImpl();

        List<WikittyLabel> toStore = new ArrayList<WikittyLabel>();
        Collections.addAll(toStore, wikitty1, wikitty2);
        List<WikittyLabel> stored = wikittyClient.store(toStore);

        stored.get(0).addLabels("lbl");

        stored = wikittyClient.store(toStore);
        Assert.assertEquals(Collections.singleton("lbl"), stored.get(0).getLabels());
    }

    /**
     * Test a query with query maker.
     */
    @Test
    public void testFindByQueryMaker() {
        WikittyQuery query = new WikittyQueryMaker().and()
        .like(Product.ELEMENT_FIELD_PRODUCT_NAME, "*universe*")
        .end();
        
        Product product = wikittyClient.findByQuery(Product.class, query);
        Assert.assertEquals(4662, product.totalPrice());
    }

    /**
     * Test a query with query maker without result.
     */
    @Test
    public void testFindByQueryMakerNoneFound() {
        WikittyQuery query = new WikittyQueryMaker().and()
        .like(Element.ALL_FIELD, "toto")
        .end();
        
        Product product = wikittyClient.findByQuery(Product.class, query);
        Assert.assertNull(product);
    }

    /**
     * Test a query with query maker multiple.
     */
    @Test
    public void testFindByQueryMakerMuliple() {
        WikittyQuery query = new WikittyQueryMaker().and()
        .like(Element.ALL_FIELD, "*everything*")
        .end();
        query.setLimit(0); // just count
        
        WikittyQueryResult results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(2, results.getTotalResult());
        Assert.assertTrue(results.getAll().isEmpty());
    }

    /**
     * Test a query with query parser.
     */
    @Test
    public void testFindQueryParser() {
        WikittyQuery query = WikittyQueryParser.parse(
            "ProductPicture.price=420");
        Product product = wikittyClient.findByQuery(Product.class, query);
        Assert.assertEquals(420, product.getPicturePrice());
    }

    /**
     * Test a query with query parser multiples.
     */
    @Test
    public void testFindQueryParserMuliple() {
        WikittyQuery query = WikittyQueryParser.parse(
            "Product.price=[0 TO 99] Product.colors!=red");
        WikittyQueryResult<String> product = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(2, product.getTotalResult());
    }

    /**
     * Test la recherche sur les extensions et wikitty crée manuelement.
     */
    @Test
    public void testFindQueryOnMovies() {
        // The Dark Knight Rises
        // Die hard 4
        WikittyQuery query = new WikittyQueryMaker().eq(MOVIE_EXTENSION_NAME + ".name", "*ar?*").end();
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(2, results.getAll().size());
    }

    /**
     * Test le resultat attendu d'un export XML.
     */
    @Test
    public void testExportXml() {
        WikittyImportExportService ieService = new WikittyImportExportService(wikittyClient);
        WikittyQuery query = new WikittyQueryMaker().eq(Element.EXTENSION, WikittyTreeNode.EXT_WIKITTYTREENODE).end();
        String xmlExport = ieService.syncExportAllByQuery(FORMAT.XML, query);

        // extension definition is present in xml export
        Assert.assertTrue("#### xmlExport ####\n" + xmlExport + "\n#############",
                xmlExport.contains("<extension name='WikittyTreeNode' version='4.0'>"));
        // some data too
        Assert.assertTrue("#### xmlExport ####\n" + xmlExport + "\n#############",
                xmlExport.contains("<WikittyTreeNode.name>Books</WikittyTreeNode.name>"));
        Assert.assertTrue("#### xmlExport ####\n" + xmlExport + "\n#############",
                xmlExport.contains("extensions='WikittyTreeNode[4.0],CatalogNode[2.0]'"));
        Assert.assertTrue("#### xmlExport ####\n" + xmlExport + "\n#############",
                xmlExport.contains("<WikittyTreeNode.attachment>"));
    }

    /**
     * Import le fichier importclient.xml qui correspond aux mêmes données
     * que les fichiers importclient.csv et importtree*.csv.
     * 
     * Après l'import on se retourve donc normalement avec les mêmes données
     * que si l'on avait appelé la methode {@link #importClients()}.
     * @throws IOException
     */
    @Test
    public void testImportXml() throws IOException {
        WikittyImportExportService ieService = new WikittyImportExportService(wikittyClient);
        InputStream is = WikittyClientTest.class.getResourceAsStream("/xml/importclient.xml");
        InputStreamReader reader = new InputStreamReader(is);
        ieService.syncImport(FORMAT.XML, reader);
        reader.close();

        Wikitty wClient = wikittyClient.restore("fbcc8aed-7f67-4e3c-a9aa-221373765f8d");
        Assert.assertEquals("Entreprise dupont", wClient.getFieldAsString("Client", "name"));
        
        WikittyTreeNode node = wikittyClient.restore(WikittyTreeNode.class, "0d13cb0b-bc06-431c-9438-7bcb357f45da");
        Assert.assertEquals("MySubNode", node.getName());
    }

    /**
     * Test le resultat attendu d'un export CSV.
     */
    @Test
    public void testExportCSV() {
        assumeTrueSearchEngineCanRunTest(); // facets

        WikittyImportExportService ieService = new WikittyImportExportService(wikittyClient);
        WikittyQuery query = new WikittyQueryMaker()
                .eq(Element.EXTENSION, WikittyTreeNode.EXT_WIKITTYTREENODE).end();
        String csvExport = ieService.syncExportAllByQuery(FORMAT.CSV, query);
        
        // extension definition is present in xml export
        Assert.assertTrue("#### csvExport ####\n" + csvExport + "\n#############",
                csvExport.startsWith("\"Wikitty.Id\",\"Wikitty.Ext\",\"WikittyTreeNode.name\",\"WikittyTreeNode.attachment\",\"WikittyTreeNode.parent\""));
        // some data too
        Assert.assertTrue("#### csvExport ####\n" + csvExport + "\n#############",
                csvExport.contains("\"WikittyTreeNode,CatalogNode\",\"Everything else\",,"));
        Assert.assertTrue("#### csvExport ####\n" + csvExport + "\n#############",
                csvExport.contains("\"WikittyTreeNode,CatalogNode\",\"Catalog\",,"));
        Assert.assertTrue("#### csvExport ####\n" + csvExport + "\n#############",
                csvExport.contains("\"WikittyTreeNode,CatalogNode\",\"Books\",,"));
        Assert.assertTrue("#### csvExport ####\n" + csvExport + "\n#############",
                csvExport.contains("Nouvelles"));
    }

    /**
     * Test l'import csv des livres.
     * @throws IOException 
     */
    @Test
    public void testImportCSVBooks() throws IOException {
        importBooks(); // 13 books

        WikittyQuery query = new WikittyQueryMaker().eq(Element.EXTENSION, Product.EXT_PRODUCT).end();
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(4 + 13, results.getTotalResult());
    }

    /**
     * Test dans l'import csv que les request de liaisons d'elements
     * fonctionne.
     * 
     * Les livres sont liés à des categories.
     * 
     * @throws IOException 
     */
    @Test
    public void testImportCSVBooksLinks() throws IOException {
        importBooks();

        WikittyQuery query = new WikittyQueryMaker().eq(Product.ELEMENT_FIELD_PRODUCT_NAME, "Da vinci code").end();
        Product product = wikittyClient.findByQuery(Product.class, query);
        String categoryId = product.getCategory();
        Category category = wikittyClient.restore(Category.class, categoryId);
        Assert.assertEquals("science fiction", category.getName());
    }
    
    /**
     * Test les requetage après l'import client.
     * 
     * @throws IOException 
     */
    @Test
    public void testImportClients() throws IOException {
        importClients();

        // test extension support
        WikittyQuery query = new WikittyQueryMaker().eq(Element.EXTENSION, "Tag").end();
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(1, results.getTotalResult());

        // test normal import
        WikittyQuery query2 = new WikittyQueryMaker().eq("Client.name", "Entreprise dupont").end();
        WikittyQueryResult<String> results2 = wikittyClient.findAllByQuery(query2);
        Assert.assertEquals(1, results2.getTotalResult());

        // import attachment id that not exist, must be not imported
        WikittyQuery query3 = new WikittyQueryMaker().eq(WikittyTreeNode.ELEMENT_FIELD_WIKITTYTREENODE_NAME, "MyTreeNode").end();
        WikittyQueryResult<WikittyTreeNode> results3 = wikittyClient.findAllByQuery(WikittyTreeNode.class, query3);
        Assert.assertEquals(1, results3.getTotalResult());
        WikittyTreeNode myTreeNode = results3.get(0);
        Assert.assertEquals(1, myTreeNode.getAttachment().size());

        // test des requetes imbriquées
        WikittyQuery query4 = new WikittyQueryMaker().eq(WikittyTreeNode.ELEMENT_FIELD_WIKITTYTREENODE_PARENT, myTreeNode.getWikittyId()).end();
        WikittyQueryResult<WikittyTreeNode> results4 = wikittyClient.findAllByQuery(WikittyTreeNode.class, query4);
        Assert.assertEquals(1, results4.getTotalResult());
        WikittyTreeNode mySubNode = results4.get(0);
        Assert.assertEquals("MySubNode", mySubNode.getName());
    }

    /**
     * Test que faire l'import après avoir modifier un des wikitty ecrase les
     * données.
     * 
     * @throws IOException 
     */
    @Test
    public void testImportTwice() throws IOException {
        // first import
        importClients();

        // modify one
        Wikitty w = wikittyClient.restore("fbcc8aed-7f67-4e3c-a9aa-221373765f8d");
        Assert.assertNotNull("Erreur d'import de fichier csv", w);
        w.setField("Client", "name", "titi");
        wikittyClient.store(w);

        // second import sould fail
        importClients();

        // aa
        w = wikittyClient.restore("fbcc8aed-7f67-4e3c-a9aa-221373765f8d");
        Assert.assertEquals("Entreprise dupont", w.getFieldAsString("Client", "name"));
    }

    /**
     * Test i18n in wikitty.
     */
    @Test
    public void testI18n() {
        // creation d'un label pour l'utiliser pour l'i18n
        WikittyLabel label = new WikittyLabelImpl();
        Wikitty labelWikitty = wikittyClient.castTo(Wikitty.class, label);
        WikittyExtension ext = labelWikitty.getExtension(WikittyLabel.EXT_WIKITTYLABEL);

        // recuperation de support i18n pour l'extension WikittyLabel
        WikittyI18n i18n = WikittyI18nUtil.getI18n(wikittyClient, ext);

        // ajout d'une traduction pour le francais
        i18n.setTranslation("fr", WikittyLabel.FIELD_WIKITTYLABEL_LABELS, "étiquette");
        wikittyClient.store(i18n);

        String trad = i18n.getTranslation("fr", WikittyLabel.FIELD_WIKITTYLABEL_LABELS);
        Assert.assertEquals("étiquette", trad);
        Assert.assertEquals(1, i18n.getLang().size());
        Assert.assertEquals("fr", i18n.getLang().iterator().next());
        Assert.assertEquals("[fr:\"labels\"=\"étiquette\"]", i18n.getTranslations());

        // ajout d'une traduction pour l'espagnole
        i18n.setTranslation("es", WikittyLabel.FIELD_WIKITTYLABEL_LABELS, "etiqueta");

        Assert.assertEquals(2, i18n.getLang().size());

        // on force le vidage du cache pour voir si le parsing se passe bien
        WikittyI18nTestUtil.cleanCache(i18n);
        trad = i18n.getTranslation("fr", WikittyLabel.FIELD_WIKITTYLABEL_LABELS);
        Assert.assertEquals("étiquette", trad);
    }

    /**
     * Test d'ajout de labels sur un wikitty et de recherche par label.
     */
    @Test
    public void testFindAllByLabel() {
        WikittyExtension ext = ExtensionFactory.create("test", "1")
                .addField("toto", WikittyTypes.NUMERIC).extension();
        Wikitty w = new WikittyImpl();
        String id = w.getWikittyId();
        w.addExtension(ext);
        w.setField(ext.getName(), "toto", 42);
        wikittyClient.store(w);
        WikittyLabelUtil.addLabel(wikittyClient, id, "hello");
        WikittyQueryResult<Wikitty> wikitties = WikittyLabelUtil.findAllByLabel(wikittyClient, "hello", 0, 1);
        Assert.assertEquals(wikitties.get(0), w);
    }

    /**
     * Test de ' comme chaine de caractere
     */
    @Test
    public void testQueryParserSimpleQuote() throws IOException {
        importBooks();
        { // avec ' escape
            WikittyQuery query = WikittyQueryParser.parse("Product.name=\"Harry Potter et le Prisonnier d'Azkaban\"");
            WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
            Assert.assertEquals(1, results.getTotalResult());
        }
        { // avec ' escape
            WikittyQuery query = WikittyQueryParser.parse("Product.name='Harry Potter et le Prisonnier d\\'Azkaban'");
            WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
            Assert.assertEquals(1, results.getTotalResult());
        }
    }
    /**
     * Test de limit et offset dans le parser.
     */
    @Test
    public void testQueryParseOffsetAndLimit() {
        {
            // 22 in init db
            WikittyQuery query = WikittyQueryParser.parse("TRUE");
            WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
            Assert.assertEquals(22, results.getTotalResult());
        }
        {
            // 22 in init db
            WikittyQuery query = WikittyQueryParser.parse("TRUE #offset 2");
            WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
            Assert.assertEquals(22, results.getTotalResult());
            Assert.assertEquals(20, results.size());
        }
        {
            // 22 in init db
            WikittyQuery query = WikittyQueryParser.parse("TRUE #limit 10");
            WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
            Assert.assertEquals(22, results.getTotalResult());
            Assert.assertEquals(10, results.size());
        }
        {
            // 22 in init db
            WikittyQuery query = WikittyQueryParser.parse("TRUE #offset 2 #limit 10");
            WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
            Assert.assertEquals(22, results.getTotalResult());
            Assert.assertEquals(10, results.size());
        }
    }



    /**
     * Test eq() operator.
     */
    @Test
    public void testQueryMakerEq() {
        // test strict equals
        WikittyQuery query = new WikittyQueryMaker().eq(Product.ELEMENT_FIELD_PRODUCT_PRICE, 42).end();
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(1, results.getTotalResult());
        Assert.assertEquals(1, results.getAll().size());

        Product resultP = results.peek();
        Assert.assertEquals("Answer to life the universe and everything", resultP.getName());
        Assert.assertEquals(42, resultP.getPriceFromProduct());

        //Test using pattern matching
        WikittyQuery query2 = new WikittyQueryMaker().eq(Product.ELEMENT_FIELD_PRODUCT_NAME, "Indign*").end();
        WikittyQueryResult<Product> results2 = wikittyClient.findAllByQuery(Product.class, query2);
        Assert.assertEquals(1, results2.getTotalResult());
        Assert.assertEquals(1, results2.getAll().size());

        //Test using pattern matching
        WikittyQuery query3 = new WikittyQueryMaker().eq(Product.ELEMENT_FIELD_PRODUCT_NAME, "Indign*").end();
        WikittyQueryResult<Product> results3 = wikittyClient.findAllByQuery(Product.class, query3);
        Assert.assertEquals(1, results3.getTotalResult());
        Assert.assertEquals(1, results3.getAll().size());
    }
    
    /**
     * Test eq() operator.
     */
    @Test
    public void testQueryMakerEqFqf() {
        // test strict equals
        WikittyQuery query = new WikittyQueryMaker().eq(Product.FQ_FIELD_PRODUCT_PRICE, 42).end();
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(1, results.getTotalResult());
        Assert.assertEquals(1, results.getAll().size());

        Product resultP = results.peek();
        Assert.assertEquals("Answer to life the universe and everything", resultP.getName());
        Assert.assertEquals(42, resultP.getPriceFromProduct());

        //Test using pattern matching
        WikittyQuery query2 = new WikittyQueryMaker().eq(Product.FQ_FIELD_PRODUCT_NAME, "Indign*").end();
        WikittyQueryResult<Product> results2 = wikittyClient.findAllByQuery(Product.class, query2);
        Assert.assertEquals(1, results2.getTotalResult());
        Assert.assertEquals(1, results2.getAll().size());

        //Test using pattern matching
        WikittyQuery query3 = new WikittyQueryMaker().eq(Product.FQ_FIELD_PRODUCT_NAME, "Indign*").end();
        WikittyQueryResult<Product> results3 = wikittyClient.findAllByQuery(Product.class, query3);
        Assert.assertEquals(1, results3.getTotalResult());
        Assert.assertEquals(1, results3.getAll().size());
    }

    /**
     * Test neq() operator.
     */
    @Test
    public void testQueryMakerNeq() {
        // 22 wikitty in init
        // only one with price == 42
        // test strict equals
        WikittyQuery query = new WikittyQueryMaker().ne(Product.FQ_FIELD_PRODUCT_PRICE, 42).end();
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(21, results.getTotalResult());
        Assert.assertEquals(21, results.getAll().size());
    }

    /**
     * Test between.
     */
    @Test
    public void testQueryMakerBw() {

        // test strict equals
        WikittyQuery query = new WikittyQueryMaker().bw(Product.FQ_FIELD_PRODUCT_PRICE, 14, 99).end();
        query.setLimit(0);
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(2, results.getTotalResult());
        Assert.assertEquals(0, results.getAll().size());
    }

    @Test
    public void testQueryMakerLt() {
        WikittyQuery query = new WikittyQueryMaker().lt(Product.FQ_FIELD_PRODUCT_PRICE, 15).end();
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(1, results.getTotalResult());
        Assert.assertEquals(1, results.getAll().size());
    }

    @Test
    public void testQueryParserLe() {
        WikittyQuery query = WikittyQueryParser.parse("Product.price <= 15");
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(2, results.getTotalResult());
        Assert.assertEquals(2, results.getAll().size());
    }

    @Test
    public void testQueryMakerGt() {
        WikittyQuery query = new WikittyQueryMaker().gt(Product.FQ_FIELD_PRODUCT_PRICE, 15).end();
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(2, results.getTotalResult());
        Assert.assertEquals(2, results.getAll().size());
    }

    @Test
    public void testQueryParserGe() {
        WikittyQuery query = WikittyQueryParser.parse("Product.price >= 15");
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(3, results.getTotalResult());
        Assert.assertEquals(3, results.getAll().size());
    }

    @Test
    public void testQueryMakerContainsAll() throws IOException {
        importBooks(); // add 13 livres
        {
            WikittyQuery query = new WikittyQueryMaker()
                    .containsAll(Product.ELEMENT_FIELD_PRODUCT_COLORS, Arrays.asList("white", "black")).end();
            WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
            Assert.assertEquals(2, results.getTotalResult());
            Assert.assertEquals(2, results.getAll().size());
        }

        // with empty list, result must contains nothing
        // http://www.nuiton.org/issues/3735
        // Anomalie #3735: containsOne and containsAll with an empty collection returns all objects but no object should have been returned
        {
            WikittyQuery query = new WikittyQueryMaker()
                    .containsAll(Product.ELEMENT_FIELD_PRODUCT_COLORS, Arrays.asList()).end();
            WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
            Assert.assertEquals(0, results.getTotalResult());
            Assert.assertEquals(0, results.getAll().size());
        }

    }

    @Test
    public void testQueryParserContainsAll() throws IOException {
        importBooks(); // add 13 livres
        WikittyQuery query = WikittyQueryParser.parse("Product.colors=[white, black]");
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(2, results.getTotalResult());
        Assert.assertEquals(2, results.getAll().size());
    }

    @Test
    public void testQueryMakerContainsOne() throws IOException {
        importBooks(); // add 13 livres
        {
            WikittyQuery query = new WikittyQueryMaker()
                    .containsOne(Product.ELEMENT_FIELD_PRODUCT_COLORS, Arrays.asList("white", "black")).end();
            WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
            Assert.assertEquals(9, results.getTotalResult());
            Assert.assertEquals(9, results.getAll().size());
        }

        // with empty list, result must contains nothing
        // http://www.nuiton.org/issues/3735
        // Anomalie #3735: containsOne and containsAll with an empty collection returns all objects but no object should have been returned
        {
            WikittyQuery query = new WikittyQueryMaker()
                    .containsOne(Product.ELEMENT_FIELD_PRODUCT_COLORS, Arrays.asList()).end();
            WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
            Assert.assertEquals(0, results.getTotalResult());
            Assert.assertEquals(0, results.getAll().size());
        }

    }

    @Test
    public void testQueryParserContainsOne() throws IOException {
        importBooks(); // add 13 livres
        WikittyQuery query = WikittyQueryParser.parse("Product.colors={white, black}");
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(9, results.getTotalResult());
        Assert.assertEquals(9, results.getAll().size());
    }
    
    @Test
    public void testQueryParserExteq() throws IOException {
        importBooks(); // add 13 livres
        WikittyQuery query = WikittyQueryParser.parse("extension=Product");
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(17, results.getTotalResult());
        Assert.assertEquals(17, results.getAll().size());
    }

    @Test
    public void testQueryMakerIdeq() throws IOException {
        importBooks(); // pour un wid connu
        WikittyQuery query = new WikittyQueryMaker().ideq("db9dc782-e650-4fd4-83ac-3c1c5c136cde").end();
        Product p = wikittyClient.findByQuery(Product.class, query);
        Assert.assertEquals("Da vinci code", p.getName());
    }
    
    @Test
    public void testQueryParserIdeq() throws IOException {
        importBooks(); // pour un wid connu
        WikittyQuery query = WikittyQueryParser.parse("id=db9dc782-e650-4fd4-83ac-3c1c5c136cde");
        Product p = wikittyClient.findByQuery(Product.class, query);
        Assert.assertEquals("Da vinci code", p.getName());
    }

    @Test
    public void testQueryMakerIdneq() throws IOException {
        // + 22 in init db
        importBooks(); // 13 importé = 35
        // -1 avec cet id
        WikittyQuery query = new WikittyQueryMaker().idne("db9dc782-e650-4fd4-83ac-3c1c5c136cde").end();
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(34, results.getTotalResult());
    }
    
    @Test
    public void testQueryParserIdneq() throws IOException {
        // + 22 in init db
        importBooks(); // 13 importé = 35
        // -1 avec cet id
        WikittyQuery query = WikittyQueryParser.parse("id!=db9dc782-e650-4fd4-83ac-3c1c5c136cde");
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(34, results.getTotalResult());
    }
    
    @Test
    public void testQueryMakerUnlike() throws IOException {
        // + 22 in init db
        importBooks(); // 13 importé = 35
        // -6 HP
        WikittyQuery query = new WikittyQueryMaker().unlike(Product.ELEMENT_FIELD_PRODUCT_NAME, "*Potter*").end();
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(29, results.getTotalResult()); // -6 HP = 11
    }
    
    @Test
    public void testQueryParserUnlike() throws IOException {
        // + 22 in init db
        importBooks(); // 13 importé = 35
        // -6 HP
        WikittyQuery query = WikittyQueryParser.parse("Product.name UNLIKE *Potter*");
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(29, results.getTotalResult()); // -6 HP = 11
    }
    
    @Test
    public void testQueryMakerLike() throws IOException {
        importBooks();
        WikittyQuery query = new WikittyQueryMaker().like(Product.ELEMENT_FIELD_PRODUCT_NAME, "*potter*").end();
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(6, results.getTotalResult());
    }
    
    @Test
    public void testQueryParserLike() throws IOException {
        importBooks();
        WikittyQuery query = WikittyQueryParser.parse("Product.name LIKE *potter*");
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(6, results.getTotalResult());
    }

    @Test
    public void testQueryMakerSw() throws IOException {
        importBooks();
        WikittyQuery query = new WikittyQueryMaker().sw(Product.ELEMENT_FIELD_PRODUCT_NAME, "Harry").end();
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(6, results.getTotalResult());
    }

    @Test
    public void testQueryParserEw() throws IOException {
        importBooks();
        WikittyQuery query = WikittyQueryParser.parse("Product.name=*sorciers");
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(1, results.getTotalResult());
    }

    @Test
    public void testQueryMakerNotsw() throws IOException {
        // + 22 in init db
        importBooks(); // 13 importé = 35
        // -6 HP
        WikittyQuery query = new WikittyQueryMaker().notsw(Product.ELEMENT_FIELD_PRODUCT_NAME, "Harry").end();
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(29, results.getTotalResult());
    }

    @Test
    public void testQueryParserNotsw() throws IOException {
        // + 22 in init db
        importBooks(); // 13 importé = 35
        // -6 HP
        WikittyQuery query = WikittyQueryParser.parse("Product.name!=Harry*");
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(29, results.getTotalResult());
    }

    @Test
    public void testQueryMakerNotew() throws IOException {
        // + 22 in init db
        importBooks(); // 13 importé = 35
        // -1 sorciers
        WikittyQuery query = new WikittyQueryMaker().notew(Product.ELEMENT_FIELD_PRODUCT_NAME, "sorciers").end();
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(34, results.getTotalResult());
    }

    @Test
    public void testQueryParserNotew() throws IOException {
        // + 22 in init db
        importBooks(); // 13 importé = 35
        // -1 sorciers
        WikittyQuery query = WikittyQueryParser.parse("Product.name!=*sorciers");
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(34, results.getTotalResult());
    }

    @Test
    public void testQueryMakerKeyword() throws IOException {
        importBooks();
        WikittyQuery query = new WikittyQueryMaker().keyword("potter").end();
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(6, results.getTotalResult());
        
        WikittyQuery query2 = new WikittyQueryMaker().keyword("yellow").end();
        WikittyQueryResult<Product> results2 = wikittyClient.findAllByQuery(Product.class, query2);
        Assert.assertEquals(2, results2.getTotalResult());
    }

    @Test
    public void testQueryParserKeyword() throws IOException {
        importBooks();
        WikittyQuery query = WikittyQueryParser.parse("potter");
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(6, results.getTotalResult());
        
        WikittyQuery query2 = WikittyQueryParser.parse("yellow");
        WikittyQueryResult<Product> results2 = wikittyClient.findAllByQuery(Product.class, query2);
        Assert.assertEquals(2, results2.getTotalResult());
    }

    @Test
    public void testQueryMakerIsNull() throws IOException {
        importBooks();
        WikittyQuery query = new WikittyQueryMaker().isNull(Product.ELEMENT_FIELD_PRODUCT_CATEGORY).end();
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(2, results.getTotalResult());
    }
    
    @Test
    public void testQueryParserIsNull() throws IOException {
        importBooks();
        WikittyQuery query = WikittyQueryParser.parse("Product.category=NULL");
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(2, results.getTotalResult());
    }

    @Test
    public void testQueryMakerIsNotNull() throws IOException {
        importBooks();
        WikittyQuery query = new WikittyQueryMaker().isNotNull(Product.ELEMENT_FIELD_PRODUCT_CATEGORY).end();
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(15, results.getTotalResult());
    }

    @Test
    public void testQueryParserIsNotNull() throws IOException {
        importBooks();
        WikittyQuery query = WikittyQueryParser.parse("Product.category!=NULL");
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(15, results.getTotalResult());
    }

    @Test
    public void testQueryMakerFalse() {
        WikittyQuery query = new WikittyQueryMaker().rFalse().end();
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(0, results.getTotalResult());
    }
    
    @Test
    public void testQueryMakerTrue() {
        // 22 in init db
        WikittyQuery query = new WikittyQueryMaker().rTrue().end();
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(22, results.getTotalResult());
    }

    @Test
    public void testQueryParseTrue() {
        // 22 in init db
        WikittyQuery query = WikittyQueryParser.parse("TRUE");
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(22, results.getTotalResult());
    }
    
    @Test
    public void testQueryParseFalse() {
        WikittyQuery query = WikittyQueryParser.parse("FALSE");
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(0, results.getTotalResult());
    }

    @Test
    public void testQueryMakerAnd() throws IOException {
        importBooks();
        WikittyQuery query = new WikittyQueryMaker().and()
                .sw(Product.FQ_FIELD_PRODUCT_NAME, "Harry")
                .notew(Product.ELEMENT_FIELD_PRODUCT_NAME, "sorciers").end();
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(5, results.getTotalResult());
    }

    @Test
    public void testQueryParserAnd() throws IOException {
        importBooks();
        WikittyQuery query = WikittyQueryParser.parse("Product.name=Harry* Product.name!=*sorciers");
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(5, results.getTotalResult());
    }

    @Test
    public void testQueryMakerOr() throws IOException {
        importBooks();
        WikittyQuery query = new WikittyQueryMaker().or()
                .sw(Product.FQ_FIELD_PRODUCT_NAME, "Harry")
                .like(Product.ELEMENT_FIELD_PRODUCT_NAME, "*code*").end();
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(7, results.getTotalResult());
    }

    @Test
    public void testQueryParserOr() throws IOException {
        importBooks();
        WikittyQuery query = WikittyQueryParser.parse("Product.name=Harry* OR Product.name LIKE *code*");
        WikittyQueryResult<Product> results = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertEquals(7, results.getTotalResult());
    }

    @Test
    public void testQueryMakerNot() {
        // 22 in db (4 products)
        WikittyQuery query = new WikittyQueryMaker().not().exteq(Product.EXT_PRODUCT).end();
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(18, results.getTotalResult());
    }

    /**
     * Test avec des requettes imbriquées.
     * 
     * @throws IOException 
     */
    @Test
    public void testQueryMakerSelect() throws IOException {
        importBooks();
        
        // les livres dont le prix est entre 0 et 75
        // et qui appartiennet a une catégory nommé "history"
        WikittyQuery query = new WikittyQueryMaker().and()
                .bw(Product.ELEMENT_FIELD_PRODUCT_PRICE, 0, 75)
                .containsOne(Product.ELEMENT_FIELD_PRODUCT_CATEGORY)
                    .select(Element.ID)
                        .eq(Category.FQ_FIELD_CATEGORY_CODE, "history").end();
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(3, results.getTotalResult());
    }

    /**
     * Test avec des requettes imbriquées.
     * 
     * @throws IOException 
     */
    @Test
    public void testQueryParserSelect() throws IOException {
        importBooks();

        // les livres dont le prix est entre 0 et 75
        // et qui appartiennet a une catégory nommé "history"
        WikittyQuery query = WikittyQueryParser.parse("Product.price=[0 TO 75] AND Product.category={SELECT id WHERE Category.code = history}");
        WikittyQueryResult<String> results = wikittyClient.findAllByQuery(query);
        Assert.assertEquals(3, results.getTotalResult());
    }

    @Test
    public void testSearchFacetSingleField() throws Exception {
        importBooks();

        WikittyQuery query = new WikittyQueryMaker().exteq(Product.EXT_PRODUCT).end();
        query.setLimit(0);
        query.addFacetField(Product.ELEMENT_FIELD_PRODUCT_CATEGORY);
        query.setFacetSort(FacetSortType.count);
        WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);

        // les resultats sont répartit en 4 categories
        Assert.assertEquals(4, result.getTopic(Product.ELEMENT_FIELD_PRODUCT_CATEGORY).size());
        Assert.assertEquals(10, result.getTopic(Product.ELEMENT_FIELD_PRODUCT_CATEGORY).get(0).getCount());
        Assert.assertEquals(3, result.getTopic(Product.ELEMENT_FIELD_PRODUCT_CATEGORY).get(1).getCount());
        Assert.assertEquals(1, result.getTopic(Product.ELEMENT_FIELD_PRODUCT_CATEGORY).get(2).getCount());
        Assert.assertEquals(1, result.getTopic(Product.ELEMENT_FIELD_PRODUCT_CATEGORY).get(3).getCount());
    }

    @Test
    public void testQueryMarkerWilcardEquals() {
        WikittyQuery query1 = new WikittyQueryMaker().eq(Product.FQ_FIELD_PRODUCT_NAME, "Lanfeust").end();
        WikittyQueryResult<Product> results1 = wikittyClient.findAllByQuery(Product.class, query1);
        Assert.assertEquals(1, results1.getTotalResult());

        // ???13
        WikittyQuery query4 = new WikittyQueryMaker().eq("*.name." + WikittyTypes.STRING, "Lanfeust").end();
        WikittyQueryResult<Product> results4 = wikittyClient.findAllByQuery(Product.class, query4);
        Assert.assertEquals(1, results4.getTotalResult());

        // ???
        WikittyQuery query3 = new WikittyQueryMaker().eq("Product.name." + WikittyTypes.STRING, "Lanfeust").end();
        WikittyQueryResult<Product> results3 = wikittyClient.findAllByQuery(Product.class, query3);
        Assert.assertEquals(1, results3.getTotalResult());

        WikittyQuery query2 = new WikittyQueryMaker().eq("*" + WikittyUtil.FQ_FIELD_NAME_SEPARATOR + Product.FIELD_PRODUCT_NAME, "Lanfeust").end();
        WikittyQueryResult<Product> results2 = wikittyClient.findAllByQuery(Product.class, query2);
        Assert.assertEquals(1, results2.getTotalResult());
    }

    @Test
    public void testQueryParserWilcardEquals() {
        WikittyQuery query1 = WikittyQueryParser.parse("Product.name=Lanfeust");
        WikittyQueryResult<Product> results1 = wikittyClient.findAllByQuery(Product.class, query1);
        Assert.assertEquals(1, results1.getTotalResult());

        WikittyQuery query2 = WikittyQueryParser.parse("*.name=Lanfeust");
        WikittyQueryResult<Product> results2 = wikittyClient.findAllByQuery(Product.class, query2);
        Assert.assertEquals(1, results2.getTotalResult());

        // ???
        WikittyQuery query3 = WikittyQueryParser.parse("Product.name.STRING=Lanfeust");
        WikittyQueryResult<Product> results3 = wikittyClient.findAllByQuery(Product.class, query3);
        Assert.assertEquals(1, results3.getTotalResult());

        // ???
        WikittyQuery query4 = WikittyQueryParser.parse("*.name.STRING=Lanfeust");
        WikittyQueryResult<Product> results4 = wikittyClient.findAllByQuery(Product.class, query4);
        Assert.assertEquals(1, results4.getTotalResult());
    }

    @Test
    public void testQueryMakerLowerCaseSearch() {
        WikittyQuery query1 = new WikittyQueryMaker().eq(Product.FQ_FIELD_PRODUCT_NAME, "Lanfeust").end();
        WikittyQueryResult<Product> results1 = wikittyClient.findAllByQuery(Product.class, query1);
        Assert.assertEquals(1, results1.getTotalResult());
        
        WikittyQuery query2 = new WikittyQueryMaker().eq(Product.FQ_FIELD_PRODUCT_NAME, "lanfeust").end();
        WikittyQueryResult<Product> results2 = wikittyClient.findAllByQuery(Product.class, query2);
        Assert.assertEquals(0, results2.getTotalResult());
        
        WikittyQuery query3 = new WikittyQueryMaker().like(Product.FQ_FIELD_PRODUCT_NAME, "Lanfeust").end();
        WikittyQueryResult<Product> results3 = wikittyClient.findAllByQuery(Product.class, query3);
        Assert.assertEquals(1, results3.getTotalResult());
        
        WikittyQuery query4 = new WikittyQueryMaker().like(Product.FQ_FIELD_PRODUCT_NAME, "lanfeust").end();
        WikittyQueryResult<Product> results4 = wikittyClient.findAllByQuery(Product.class, query4);
        Assert.assertEquals(1, results4.getTotalResult());
    }

    @Test
    public void testQueryParserLowerCaseSearch() {
        WikittyQuery query1 = WikittyQueryParser.parse("Product.name=Lanfeust");
        WikittyQueryResult<Product> results1 = wikittyClient.findAllByQuery(Product.class, query1);
        Assert.assertEquals(1, results1.getTotalResult());
        
        WikittyQuery query2 = WikittyQueryParser.parse("Product.name=lanfeust");
        WikittyQueryResult<Product> results2 = wikittyClient.findAllByQuery(Product.class, query2);
        Assert.assertEquals(0, results2.getTotalResult());
        
        WikittyQuery query3 = WikittyQueryParser.parse("Product.name LIKE Lanfeust");
        WikittyQueryResult<Product> results3 = wikittyClient.findAllByQuery(Product.class, query3);
        Assert.assertEquals(1, results3.getTotalResult());
        
        WikittyQuery query4 = WikittyQueryParser.parse("Product.name LIKE lanfeust");
        WikittyQueryResult<Product> results4 = wikittyClient.findAllByQuery(Product.class, query4);
        Assert.assertEquals(1, results4.getTotalResult());
    }

    /**
     * Test sort on wildcard fields.
     */
    @Test
    public void testSearchWithSort1() {
        // FIXME poussin 20120202 comment savoir que les objets sont bien trie ?
        // Il y a toute sorte d'extension avec *.name et on ne recupere que
        // les ID.
        WikittyQuery query1 = WikittyQueryParser.parse("*.name=*");
        query1.setSortAscending(new ElementField("*.name"));
        WikittyQueryResult<String> results1 = wikittyClient.findAllByQuery(query1);
        Assert.assertEquals(22, results1.getTotalResult());
    }
    
    /**
     * Creer une nouvelle extension avec des données non ordonnées et fait
     * une recherche triée dessus.
     */
    @Test
    public void testSearchWithSort2() {
        //Create ext sortable
        String sortableExtName = "sortable";
        String numFieldName = "num";
        WikittyExtension sortable = ExtensionFactory.create(sortableExtName, "1.0")
                .addField(numFieldName, WikittyTypes.NUMERIC)
                .extension();
        wikittyClient.storeExtension(sortable);

        List<Integer> expected = new ArrayList<Integer>();
        expected.add(10);
        expected.add(1);
        expected.add(7);

        //Create wikitty sortable
        for (Integer i : expected) {
            Wikitty w = new WikittyImpl();
            w.addExtension(sortable);
            w.setField(sortableExtName, numFieldName, i);
            wikittyClient.store(w);

        }
        Collections.sort(expected);
        WikittyQuery query2 = new WikittyQueryMaker().exteq(sortableExtName).end();
        query2.setSortAscending(new ElementField(WikittyUtil.getFQFieldName(sortableExtName, numFieldName)));
        WikittyQueryResult<Wikitty> results2 = wikittyClient.findAllByQuery(Wikitty.class, query2);

        List<Integer> resulted = new ArrayList<Integer>();
        for (Wikitty w : results2.getAll()) {
            resulted.add(w.getFieldAsInt(sortableExtName, numFieldName));
        }
        Assert.assertEquals(expected, resulted);
    }

    /**
     * Test sort on test data.
     */
    @Test
    public void testSearchWithSortTestData() {
        WikittyQuery query1 = WikittyQueryParser.parse("Product.price <= 100");
        query1.setSortDescending(Product.ELEMENT_FIELD_PRODUCT_PRICE);
        WikittyQueryResult<Product> results1 = wikittyClient.findAllByQuery(Product.class, query1);
        Assert.assertEquals(100, results1.get(0).getPriceFromProduct());
        Assert.assertEquals(42, results1.get(1).getPriceFromProduct());
        Assert.assertEquals(15, results1.get(2).getPriceFromProduct());
        Assert.assertEquals(13, results1.get(3).getPriceFromProduct());
    }

    /**
     * Test les recherches avec les accents et les charactères spéciaux.
     * 
     * FIXME echatellier 20120131 fix test (fails with solr analyzer)
     */
    @Test
    public void testSearchWithAccent() {
        WikittyGroupImpl group = new WikittyGroupImpl();
        group.setName("coucou");
        WikittyGroupImpl groupAccent = new WikittyGroupImpl();
        groupAccent.setName("çéçà");

        wikittyClient.store(group, groupAccent);

        {
            // sans accent
            WikittyQuery query = new WikittyQueryMaker().keyword("coucou").end();
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);
            Assert.assertEquals(1, result.size());
        }
        {
            // avec accent
            WikittyQuery query = new WikittyQueryMaker().keyword("çéçà").end();
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);
            Assert.assertEquals(1, result.size());
        }
        {
            // avec accent specifiquement sur le champs name
            WikittyQuery query = new WikittyQueryMaker().eq(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, "çéçà").end();
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);
            Assert.assertEquals(1, result.size());
        }
        {
            // avec accent specifiquement sur le champs name mais sans accent pour la recherche
            // specifique à solr
            WikittyQuery query = new WikittyQueryMaker().eqIgnoreCaseAndAccent(
                    WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, "ceca").end();
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);
            Assert.assertEquals(1, result.size());
        }
        {
            // avec accent specifiquement sur le champs name
            // mais sans accent pour la recherche
            // et et majuscule
            WikittyQuery query = new WikittyQueryMaker().eqIgnoreCaseAndAccent(
                    WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, "CECA").end();
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);
            Assert.assertEquals(1, result.size());
        }
        {
            // avec accent specifiquement sur le champs name
            // mais sans accent pour la recherche
            // et et majuscule
            WikittyQuery query = new WikittyQueryMaker().like(
                    WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, "ceca").end();
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);
            Assert.assertEquals(1, result.size());
        }
    }

    /**
     * Test de la selection et de la pagination.
     * @throws IOException 
     */
    @Test
    public void testPaginedSearchSelect() throws IOException {
        importBooks();

        // first
        WikittyQuery query1 = new WikittyQueryMaker()
            .select(Product.ELEMENT_FIELD_PRODUCT_PRICE)
            .exteq(Product.EXT_PRODUCT).end();
        query1.setSortAscending(Product.ELEMENT_FIELD_PRODUCT_PRICE);
        query1.setOffset(0);
        query1.setLimit(2);
        WikittyQueryResult<Double> results1 = wikittyClient.findAllByQuery(Double.class, query1);
        Assert.assertEquals(17, results1.getTotalResult());
        Assert.assertEquals(2, results1.getAll().size());
        Assert.assertEquals(Double.valueOf(13), results1.get(0));

        // second
        // FIXME echatellier 20120201 setLimit -1 us buggy
        /*query1.setOffset(0);
        query1.setLimit(WikittyQuery.MAX);
        results1 = wikittyClient.findAllByQuery(query1);
        Assert.assertEquals(17, results1.getTotalResult());
        Assert.assertEquals(17, results1.getAll().size());*/
        
        // third
        query1.setOffset(0);
        query1.setLimit(0);
        results1 = wikittyClient.findAllByQuery(Double.class, query1);
        Assert.assertEquals(17, results1.getTotalResult());
        Assert.assertEquals(0, results1.getAll().size());
    }

    @Test
    public void testQueryMarkerSelectSortAndAggregate() throws Exception {
        WikittyExtension ext = new WikittyExtension("Test",
                                     "1.0", // version
                                     WikittyUtil.tagValuesToMap(" version=\"1.0\""), // tag/values
                                     (List<String>)null,
                                     WikittyUtil.buildFieldMapExtension( // building field map
                                             "Numeric number",
                                             "String string",
                                             "Date date"));
        Wikitty w1 = new WikittyImpl();
        w1.addExtension(ext);
        w1.setField("Test", "number", 5);
        w1.setField("Test", "date", WikittyUtil.parseDate("02/05/1975"));

        Wikitty w2 = new WikittyImpl();
        w2.addExtension(ext);
        w2.setField("Test", "number", -4);
        w2.setField("Test", "date", WikittyUtil.parseDate("19830606"));

        Wikitty w3 = new WikittyImpl();
        w3.addExtension(ext);
        w3.setField("Test", "number", 10);
        w3.setField("Test", "date", WikittyUtil.parseDate("21/05/2002"));

        Wikitty w4 = new WikittyImpl();
        w4.addExtension(ext);
        w4.setField("Test", "number", 1);
        w4.setField("Test", "date", WikittyUtil.parseDate("05/01/2012"));

        wikittyClient.store(w1, w2, w3, w4);

        {
            WikittyQuery q = new WikittyQueryMaker().select("Test.number").end();
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(q);
            System.out.println("q: " + result);
        }
        {
            WikittyQuery q = new WikittyQueryMaker().select("Test.number").end()
                    .addSortDescending(new ElementField("Test", "number"));
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(q);
            System.out.println("q: " + result);
        }
        {
            WikittyQuery q = new WikittyQueryMaker().select("Test.date").end();
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(q);
            System.out.println("q: " + result);
        }
        {
            WikittyQuery q = new WikittyQueryMaker().select("Test.date").end()
                    .addSortDescending(new ElementField("Test", "date"));
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(q);
            System.out.println("q: " + result);
        }

        // test aggregate
        {
            WikittyQuery q = new WikittyQueryMaker()
                    .select(new FunctionAvg(null, new FunctionFieldValue(null, "Test.number")))
                    .where().exteq("Test").end();
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(q);
            System.out.println("q: " + result);
            Assert.assertEquals("3", result.peek());
        }
        {
            WikittyQuery q = new WikittyQueryMaker()
                    .select(new FunctionCount(null, new FunctionFieldValue(null, "Test.number")))
                    .where().exteq("Test").end();
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(q);
            System.out.println("q: " + result);
            Assert.assertEquals("4", result.peek());
        }
        {
            WikittyQuery q = new WikittyQueryMaker()
                    .select(new FunctionMax(null, new FunctionFieldValue(null, "Test.number")))
                    .where().exteq("Test").end();
            WikittyQueryResult<Double> result = wikittyClient.findAllByQuery(Double.class, q);
            System.out.println("q: " + result);
            Assert.assertEquals(Double.valueOf(10), result.peek());
        }
        {
            WikittyQuery q = new WikittyQueryMaker()
                    .select(new FunctionMax(null, new FunctionFieldValue(null, "Test.number")))
                    .where().exteq("Test").end()
                    .addSortDescending(new ElementField("Test", "number"));
            WikittyQueryResult<Double> result = wikittyClient.findAllByQuery(Double.class, q);
            System.out.println("q: " + result);
            Assert.assertEquals(Double.valueOf(10), result.peek());
        }
        {
            WikittyQuery q = new WikittyQueryMaker()
                    .select(new FunctionMin(null, new FunctionFieldValue(null, "Test.number")))
                    .where().exteq("Test").end();
            WikittyQueryResult<Double> result = wikittyClient.findAllByQuery(Double.class, q);
            System.out.println("q: " + result);
            Assert.assertEquals(Double.valueOf(-4.0), result.peek());
        }
        {
            WikittyQuery q = new WikittyQueryMaker()
                    .select(new FunctionMin(null, new FunctionFieldValue(null, "Test.number")))
                    .where().exteq("Test").end()
                    .addSortDescending(new ElementField("Test", "number"));
            WikittyQueryResult<Double> result = wikittyClient.findAllByQuery(Double.class, q);
            System.out.println("q: " + result);
            Assert.assertEquals(Double.valueOf(-4.0), result.peek());
        }
        {
            WikittyQuery q = new WikittyQueryMaker()
                    .select(new FunctionSum(null, new FunctionFieldValue(null, "Test.number")))
                    .where().exteq("Test").end();
            WikittyQueryResult<Double> result = wikittyClient.findAllByQuery(Double.class, q);
            System.out.println("q: " + result);
            Assert.assertEquals(Double.valueOf(12), result.peek());
        }

    }

    @Test
    public void testQueryMakerFacetOnGroup() throws Exception {
        // for id for easy debugging
        WikittyGroupImpl g1 = new WikittyGroupImpl(new WikittyImpl("g1"));
        g1.setName("Group1");
        WikittyGroupImpl g2 = new WikittyGroupImpl(new WikittyImpl("g2"));
        g2.setName("GROUP");
        WikittyGroupImpl g3 = new WikittyGroupImpl(new WikittyImpl("g3"));
        g3.setName("Group3");
        WikittyGroupImpl g4 = new WikittyGroupImpl(new WikittyImpl("g4"));
        g4.setName("GROUP");

        wikittyClient.store(g1, g2, g3, g4);

        {
            WikittyQuery query = new WikittyQueryMaker()
                    .eq(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, "Group*")
                    .end().addFacetField(WikittyGroup.ELEMENT_FIELD_WIKITTYGROUP_NAME);

            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);

            System.out.println("testFacet" + result);
            Assert.assertEquals(2, result.size());
            Assert.assertEquals(2, result.getFacets().get(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME).size());
        }
        {
            WikittyQuery query = new WikittyQueryMaker()
                    .eq(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, "GROUP")
                    .end().addFacetField(WikittyGroup.ELEMENT_FIELD_WIKITTYGROUP_NAME);

            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);

            System.out.println("testFacet" + result);
            Assert.assertEquals(2, result.size());
            Assert.assertEquals(1, result.getFacets().get(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME).size());
            Assert.assertEquals(2, result.getFacets().get(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME).get(0).getCount());
        }
        {
            WikittyQuery query = new WikittyQueryMaker()
                    .eq(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, "GROUP")
                    .end().addFacetField(WikittyGroup.ELEMENT_FIELD_WIKITTYGROUP_NAME)
                    .setLimit(0);

            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);

            System.out.println("testFacet" + result);
            Assert.assertEquals(0, result.size());
            Assert.assertEquals(1, result.getFacets().get(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME).size());
            Assert.assertEquals(2, result.getFacets().get(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME).get(0).getCount());
        }
    }

    @Test
    public void testQueryMakerEqOnGroup() throws Exception {
        // for id for easy debugging
        WikittyGroupImpl g1 = new WikittyGroupImpl(new WikittyImpl("g1"));
        g1.setName("Group1");
        WikittyGroupImpl g2 = new WikittyGroupImpl(new WikittyImpl("g2"));
        g2.setName("GROUP2");

        wikittyClient.store(g1, g2);

        {
            WikittyQuery query = new WikittyQueryMaker()
                    .eq(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, "Group1")
                    .end();

            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);

            System.out.println("testEq" + result.getAll());
            Assert.assertEquals(1, result.size());
            Assert.assertEquals(g1.getWikittyId(), result.peek());
        }

        {
            WikittyQuery query = new WikittyQueryMaker()
                    .eq(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, "Group*")
                    .end();

            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);

            System.out.println("testEq" + result.getAll());
            Assert.assertEquals(1, result.size());
            Assert.assertEquals(g1.getWikittyId(), result.peek());
        }

        {
            WikittyQuery query = new WikittyQueryMaker()
                    .eq(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, "G*")
                    .end();

            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);

            System.out.println("testEq" + result.getAll());
            Assert.assertEquals(2, result.size());
        }

        {
            WikittyQuery query = new WikittyQueryMaker()
                    .like(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, "*oup*")
                    .end();

            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);

            System.out.println("testLike" + result);
            Assert.assertEquals(2, result.size());
        }
        {
            WikittyQuery query = new WikittyQueryMaker()
                    .like("*", "Grou*")
                    .end();

            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);

            System.out.println("testLike" + result);
            Assert.assertEquals(2, result.size());
        }
        {
            WikittyQuery query = new WikittyQueryMaker()
                    .keyword("Gro")
                    .end();

            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);

            System.out.println("testKeyword:" + result);
            Assert.assertEquals(2, result.size());
        }

    }
        
    @Test
    public void testQueryMarkerSelect() throws Exception {
        // force id for easy debugging
        WikittyImpl w = new WikittyImpl("g1");
        WikittyGroupImpl g1 = new WikittyGroupImpl(w);
        g1.setName("Group1");
        w = new WikittyImpl("g2");
        WikittyGroupImpl g2 = new WikittyGroupImpl(w);
        g2.setName("GROUP2");

        w = new WikittyImpl("l1");
        WikittyLabelImpl l1 = new WikittyLabelImpl(w);
        l1.addLabels(g1.getName());

        wikittyClient.store(g1, g2, l1);

        {
            WikittyQuery query = new WikittyQueryMaker()
                    .select(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME)
                    .where()
                    .exteq(WikittyGroup.EXT_WIKITTYGROUP)
                    .end();

            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);

            System.out.println(result.getAll());
            Assert.assertEquals(2, result.size());
        }

        {
            WikittyQuery query = new WikittyQueryMaker()
                    .and()
                    .exteq(WikittyGroup.EXT_WIKITTYGROUP)
                    .containsOne(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME)
                    .select(WikittyLabel.FQ_FIELD_WIKITTYLABEL_LABELS)
                    .end();

            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);

            System.out.println(result.getAll());
            Assert.assertEquals(1, result.getTotalResult());
        }
    }

    @Test
    public void testQueryParserOnTree() throws Exception {
        assumeTrueSearchEngineCanRunTest(); // tree nodes ?

        WikittyQueryParser parser = new WikittyQueryParser();
        parser.addAlias("MyAlias\\((.*), (.*)\\)",
                "id={SELECT WikittyTreeNode.attachment"
                + " WHERE (rootNode={SELECT ID WHERE (WikittyTreeNode.name=$1)}"
                + " AND pathNode={SELECT ID WHERE (WikittyTreeNode.name=$2)})}");

        WikittyQuery q1 = parser.parseQuery(
                "WikittyGroup.name=Group* AND MyAlias(OtherTree, OtherBranch)");

        WikittyQuery q2 = parser.parseQuery(
                "WikittyGroup.name=Group* AND id={SELECT WikittyTreeNode.attachment"
                + " WHERE (rootNode={SELECT ID WHERE (WikittyTreeNode.name=OtherTree)}"
                + " AND pathNode={SELECT ID WHERE (WikittyTreeNode.name=OtherBranch)})}");

        System.out.println("Q1:" + q1);
        System.out.println("Q2:" + q2);
        Assert.assertEquals(q2, q1);

        WikittyGroupImpl g1 = new WikittyGroupImpl(new WikittyImpl("g1"));
        g1.setName("Group1");
        WikittyGroupImpl g2 = new WikittyGroupImpl(new WikittyImpl("g2"));
        g2.setName("GROUP2");
        WikittyGroupImpl g3 = new WikittyGroupImpl(new WikittyImpl("g3"));
        g3.setName("Group3");
        WikittyGroupImpl g4 = new WikittyGroupImpl(new WikittyImpl("g4"));
        g4.setName("group4");

        WikittyTreeNode root = new WikittyTreeNodeImpl(new WikittyImpl("root"));
        root.setName("OtherTree");

        WikittyTreeNode node = new WikittyTreeNodeImpl(new WikittyImpl("node1"));
        node.setName("node1");
        node.setParent(root.getWikittyId());
        node.addAttachment(g1.getWikittyId(), g2.getWikittyId());

        WikittyTreeNode branch = new WikittyTreeNodeImpl(new WikittyImpl("branch"));
        branch.setName("OtherBranch");
        branch.setParent(node.getWikittyId());

        WikittyTreeNode leaf = new WikittyTreeNodeImpl(new WikittyImpl("leaf"));
        leaf.setName("leaf");
        leaf.setParent(branch.getWikittyId());
        leaf.addAttachment(g3.getWikittyId(), g4.getWikittyId());

        wikittyClient.store(g1, g2, g3, g4, root, node, branch, leaf);

        {
            WikittyQuery q0 = parser.parseQuery(
                    "rootNode={SELECT ID WHERE (WikittyTreeNode.name=OtherTree)} AND pathNode={SELECT ID WHERE (WikittyTreeNode.name=OtherBranch)}");
            System.out.println("qO: " + q0);
            WikittyQueryResult<String> q0Result = wikittyClient.findAllByQuery(q0);
            System.out.println("q0Result:" + q0Result);
        }
        {
            WikittyQuery q0 = parser.parseQuery(
                    "SELECT WikittyTreeNode.attachment WHERE (rootNode={SELECT ID WHERE (WikittyTreeNode.name=OtherTree)} AND pathNode={SELECT ID WHERE (WikittyTreeNode.name=OtherBranch)})")
                    .addFacetField(WikittyTreeNode.ELEMENT_FIELD_WIKITTYTREENODE_ATTACHMENT);
            System.out.println("qO: " + q0);
            WikittyQueryResult<String> q0Result = wikittyClient.findAllByQuery(q0);
            System.out.println("q0Result:" + q0Result);
        }
        {
            WikittyQuery q0 = parser.parseQuery(
                    "SELECT WikittyTreeNode.attachment WHERE (ID={branch, leaf})")
                    .addFacetField(WikittyTreeNode.ELEMENT_FIELD_WIKITTYTREENODE_ATTACHMENT);
            System.out.println("qO: " + q0);
            WikittyQueryResult<String> q0Result = wikittyClient.findAllByQuery(q0);
            System.out.println("q0Result:" + q0Result);
        }
        {
            WikittyQuery q0 = parser.parseQuery(
                    "ID={branch, leaf}").addFacetField(WikittyTreeNode.ELEMENT_FIELD_WIKITTYTREENODE_ATTACHMENT);
            System.out.println("qO: " + q0);
            WikittyQueryResult<String> q0Result = wikittyClient.findAllByQuery(q0);
            System.out.println("q0Result:" + q0Result);
        }
        {
            WikittyQuery q0 = parser.parseQuery(
                    "WikittyGroup.name=Group*");
            System.out.println("q0: " + q0);
            WikittyQueryResult<String> q0Result = wikittyClient.findAllByQuery(q0);
            System.out.println("q0Result:" + q0Result);
        }

        WikittyQueryResult<String> q1Result = wikittyClient.findAllByQuery(q1);
        WikittyQueryResult<String> q2Result = wikittyClient.findAllByQuery(q2);

        System.out.println("q2Result: " + q2Result);
        
        Assert.assertEquals(q2Result.getAll(), q1Result.getAll());
        Assert.assertEquals(1, q2Result.size());
        Assert.assertEquals(Collections.singletonList(g3.getWikittyId()), q2Result.getAll());
    }

    /**
     * Test la recherche full text.
     */
    @Test
    public void testSearchEngineFullTextSearch() {
        // for id for easy debugging
        WikittyImpl w = new WikittyImpl("label");
        WikittyLabelImpl l = new WikittyLabelImpl(w);
        l.addLabels("label");
        w = new WikittyImpl("LABEL");
        WikittyLabelImpl l2 = new WikittyLabelImpl(w);
        l2.addLabels("OTHER LABEL");
        w = new WikittyImpl("TATA");
        WikittyUserImpl u = new WikittyUserImpl(w);
        u.setLogin("tata");

        List<Wikitty> toStore = new ArrayList<Wikitty>();
        toStore.add(l.getWikitty());
        toStore.add(l2.getWikitty());
        toStore.add(u.getWikitty());

        wikittyClient.storeWikitty(toStore);

        {
            WikittyQuery query = new WikittyQueryMaker().keyword("lab*").end();
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);
            System.out.println(result.getAll());
            Assert.assertEquals(2, result.getTotalResult());
        }
        {
            WikittyQuery query = new WikittyQueryMaker().and()
                .exteq(WikittyLabel.EXT_WIKITTYLABEL)
                .keyword("*a*").end();
            WikittyQueryResult<String> result = wikittyClient.findAllByQuery(query);
            System.out.println(result.getAll());
            Assert.assertEquals(2, result.getTotalResult());
        }
    }

    /**
     * Test que lors de la reindexation les noeuds indexés le sont
     * correctement si leur attachement ne sont pas encore
     * présent dans l'index.
     * 
     * Stocke les attachements avant le noeud.
     */
    @Test
    public void testReindexWithAttachement() {

        // store attachement
        Wikitty attach1 = new WikittyImpl("att1");
        Wikitty attach2 = new WikittyImpl("att2");
        List<Wikitty> toStore = new ArrayList<Wikitty>();
        toStore.add(attach1);
        toStore.add(attach2);
        wikittyClient.storeWikitty(toStore);

        // store first node
        Wikitty treeNode = new WikittyImpl();
        WikittyTreeNode treeNodeImpl = new WikittyTreeNodeImpl(treeNode);
        treeNodeImpl.setName("root");
        treeNodeImpl.addAttachment(attach1.getWikittyId());
        treeNodeImpl.addAttachment(attach2.getWikittyId());
        wikittyClient.store(treeNode);

        // set resync
        wikittyClient.syncSearchEngine();
        
        // search wikitty with attachement
        WikittyQuery query = new WikittyQueryMaker().and()
                .exteq(WikittyTreeNodeImpl.EXT_WIKITTYTREENODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "root").end();
        WikittyQueryResult<Wikitty> result = wikittyClient.findAllByQuery(Wikitty.class, query);
        Assert.assertEquals(1, result.getTotalResult());
        WikittyTreeNode resultTreeNode = new WikittyTreeNodeImpl(result.get(0));
        Assert.assertEquals(2, resultTreeNode.getAttachment().size());
    }
    
    /**
     * Test que lors de la reindexation les noeuds indexés le sont
     * correctement si leur attachement ne sont pas encore
     * présent dans l'index.
     * 
     * Stocke le noeud avant les attachements.
     */
    @Test
    public void testReindexWithAttachementOrdering() {
        
        // store first node
        Wikitty treeNode = new WikittyImpl();
        WikittyTreeNode treeNodeImpl = new WikittyTreeNodeImpl(treeNode);
        treeNodeImpl.setName("root");
        treeNode = wikittyClient.store(treeNode);
        treeNodeImpl = new WikittyTreeNodeImpl(treeNode);
        
        // store attachement
        Wikitty attach1 = new WikittyImpl("att1");
        Wikitty attach2 = new WikittyImpl("att2");
        treeNodeImpl.addAttachment(attach1.getWikittyId());
        treeNodeImpl.addAttachment(attach2.getWikittyId());
        List<Wikitty> toStore = new ArrayList<Wikitty>();
        toStore.add(attach1);
        toStore.add(attach2);
        toStore.add(treeNode);
        wikittyClient.storeWikitty(toStore);

        // set resync
        wikittyClient.syncSearchEngine();
        
        // search wikitty with attachement
        WikittyQuery query = new WikittyQueryMaker().and()
                .exteq(WikittyTreeNodeImpl.EXT_WIKITTYTREENODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "root").end();
        WikittyQueryResult<Wikitty> result = wikittyClient.findAllByQuery(Wikitty.class, query);
        Assert.assertEquals(1, result.getTotalResult());
        WikittyTreeNode resultTreeNode = new WikittyTreeNodeImpl(result.get(0));
        Assert.assertEquals(2, resultTreeNode.getAttachment().size());
    }

    /**
     * Test que lorsqu'un document solr est supprimé, il l'est réelement
     * car le proxy gere les retours null pour des documents qui n'existe
     * plus dans le base, mais les recherches continue de les trouver.
     */
    @Test
    public void testSolrDeleteDocument() {
        // store new wikitty
        Wikitty toDeleteWikitty = new WikittyImpl("wikkitytodelete");
        wikittyClient.store(toDeleteWikitty);

        // look for it
        WikittyQuery query = new WikittyQueryMaker().ideq("wikkitytodelete").end();
        Assert.assertEquals(1, wikittyClient.findAllByQuery(Collections.singletonList(query)).get(0).getTotalResult());
        Assert.assertEquals(1, wikittyClient.findAllByQuery(query).getTotalResult());

        // delete it
        wikittyClient.delete(Collections.singleton(toDeleteWikitty.getWikittyId()));

        // try to look for it after deletion
        Assert.assertEquals(0, wikittyClient.findAllByQuery(query).getTotalResult());
        Assert.assertEquals(0, wikittyClient.findAllByQuery(
                Collections.singletonList(query)).get(0).getTotalResult());
    }

    /**
     * Test que lorsque plus de 10 documents solr sont supprimés, il le sont
     * réelement car par défault, solr limte les resultats à 10.
     */
    @Test
    public void testSolrDeleteMoreThan10Documents() {
        final int NB_DOCS_TO_DELETE = 37;
        List<WikittyLabel> toDelete = new ArrayList<WikittyLabel>();

        for (int i = 0;i < NB_DOCS_TO_DELETE;i++) {
            WikittyLabel toDeleteEntity = new WikittyLabelImpl();

            // store new wikitty
            toDeleteEntity.addLabels("toDeleteEntity" + i);
            toDelete.add(toDeleteEntity);
        }

        // Store all
        wikittyClient.store(toDelete);

        // look for it
        WikittyQuery query = new WikittyQueryMaker()
                .exteq(WikittyLabel.EXT_WIKITTYLABEL).end();
        Assert.assertEquals(NB_DOCS_TO_DELETE, wikittyClient.findAllByQuery(
                Collections.singletonList(query)).get(0).getTotalResult());
        Assert.assertEquals(NB_DOCS_TO_DELETE, wikittyClient.findAllByQuery(query).getTotalResult());
        Assert.assertEquals(NB_DOCS_TO_DELETE, wikittyClient.findAllByQuery(WikittyLabel.class, query).getTotalResult());

        // delete it
        wikittyClient.delete(toDelete);

        // try to look for it after deletion
        Assert.assertEquals(0, wikittyClient.findAllByQuery(
                Collections.singletonList(query)).get(0).getTotalResult());
        Assert.assertEquals(0, wikittyClient.findAllByQuery(query).getTotalResult());
        Assert.assertEquals(0, wikittyClient.findAllByQuery(WikittyLabel.class, query).getTotalResult());
    }
    
    /**
     * Test le nombre d'attachment (fichiers attachés) au noeud des arbres.
     */
    @Test
    public void testSearchEngineCountAttachment() {
        assumeTrueSearchEngineCanRunTest(); // trees

        WikittyQuery query = new WikittyQueryMaker().and()
                .exteq(WikittyTreeNodeImpl.EXT_WIKITTYTREENODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "Catalog").end();
        CatalogNode catalogNode = wikittyClient.findAllByQuery(CatalogNode.class, query).peek();
        query = new WikittyQueryMaker().and()
                .exteq(WikittyTreeNodeImpl.EXT_WIKITTYTREENODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "Books").end();
        CatalogNode bookNode = wikittyClient.findAllByQuery(CatalogNode.class, query).peek();
        query = new WikittyQueryMaker().and()
                .exteq(WikittyTreeNodeImpl.EXT_WIKITTYTREENODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "Bande dessinées").end();
        CatalogNode bdNode = wikittyClient.findAllByQuery(CatalogNode.class, query).peek();
        query = new WikittyQueryMaker().and()
                .exteq(WikittyTreeNodeImpl.EXT_WIKITTYTREENODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "Roman").end();
        CatalogNode romanNode = wikittyClient.findAllByQuery(CatalogNode.class, query).peek();

        WikittyQueryResultTreeNode<String> treeNodeResult =
                wikittyClient.findAllIdTreeNode(catalogNode.getWikittyId(), 0, true, null);
        Assert.assertEquals(7, treeNodeResult.getAttCount());

        WikittyQueryResultTreeNode<String> children =
                wikittyClient.findAllIdTreeNode(bookNode.getWikittyId(), 1, true, null);
        if (log.isDebugEnabled()) {
            log.debug("Children : " + children);
        }

        Assert.assertEquals(3, children.getChildCount());
        Assert.assertEquals(1, children.getChild(bdNode.getWikittyId()).getAttCount());
        Assert.assertEquals(2, children.getChild(romanNode.getWikittyId()).getAttCount());
    }
    
    /**
     * Test le nombre d'attachment (fichiers attachés) au noeud des arbres
     * avec un filtre cette fois.
     */
    @Test
    public void testSearchEngineCountAttachmentFiltered() {
        assumeTrueSearchEngineCanRunTest(); // trees

        WikittyQuery query = new WikittyQueryMaker().and()
                .exteq(CatalogNode.EXT_CATALOGNODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "Catalog").end();
        CatalogNode catalogNode = wikittyClient.findAllByQuery(CatalogNode.class, query).peek();

        // on filtre par category (society)
        WikittyQuery filter = WikittyQueryParser.parse("Product.category={SELECT id WHERE Category.code = society}");

        WikittyQueryResultTreeNode<String> treeNodeResult = wikittyClient.findAllIdTreeNode(catalogNode.getWikittyId(),
                0, true, filter);
        // parmis les 7 du test precedent, seulement 1 dans la category society
        Assert.assertEquals(1, treeNodeResult.getAttCount()); 
    }
    
    /**
     * Test le nombre d'attachment (fichiers attachés) au noeud des arbres.
     */
    @Test
    public void testSearchEngineCountAttachmentAfterDeletion() {
        assumeTrueSearchEngineCanRunTest(); // trees

        // delete Lanfeust book, attached to tree
        WikittyQuery query = new WikittyQueryMaker().and()
                .exteq(Product.EXT_PRODUCT)
                .eq(Product.FQ_FIELD_PRODUCT_NAME, "Lanfeust").end();
        String bookId = wikittyClient.findByQuery(query);
        wikittyClient.delete(bookId);

        query = new WikittyQueryMaker().and()
                .exteq(CatalogNode.EXT_CATALOGNODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "Catalog").end();
        CatalogNode catalogNode = wikittyClient.findAllByQuery(CatalogNode.class, query).peek();
        WikittyQueryResultTreeNode<String> treeNodeResult =
                wikittyClient.findAllIdTreeNode(catalogNode.getWikittyId(), 0, true, null);
        Assert.assertEquals(6, treeNodeResult.getAttCount()); // one deletion
    }
    
    /**
     * Test le nombre d'attachment (fichiers attachés) au noeud des arbres.
     * 
     * @throws IOException 
     */
    @Test
    public void testSearchEngineCountAttachmentMultipleSameNode() throws IOException {
        assumeTrueSearchEngineCanRunTest(); // trees

        importBooks();

        // delete Lanfeust book, attached to tree
        WikittyQuery query = new WikittyQueryMaker().and()
                .exteq(CatalogNode.EXT_CATALOGNODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "Action").end();
        CatalogNode actionNode = wikittyClient.findAllByQuery(CatalogNode.class, query).peek();
        
        query = new WikittyQueryMaker().and()
                .exteq(CatalogNode.EXT_CATALOGNODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "Everything else").end();
        CatalogNode eeNode = wikittyClient.findAllByQuery(CatalogNode.class, query).peek();
        
        // 4 attachement en plus, mais sur seulement 2 livres
        actionNode.addAttachment("db9dc782-e650-4fd4-83ac-3c1c5c136cde");
        actionNode.addAttachment("584adc1e-726d-4348-9a57-77153d245b34");
        eeNode.addAttachment("db9dc782-e650-4fd4-83ac-3c1c5c136cde");
        eeNode.addAttachment("584adc1e-726d-4348-9a57-77153d245b34");
        wikittyClient.store(actionNode, eeNode);

        query = new WikittyQueryMaker().and()
                .exteq(CatalogNode.EXT_CATALOGNODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "Catalog").end();
        CatalogNode catalogNode = wikittyClient.findAllByQuery(CatalogNode.class, query).peek();
        WikittyQueryResultTreeNode<String> treeNodeResult =
                wikittyClient.findAllIdTreeNode(catalogNode.getWikittyId(), 0, true, null);
        Assert.assertEquals(9, treeNodeResult.getAttCount()); // 7 + 2 nouveaux
    }
    
    /**
     * Test le nombre d'attachment (fichiers attachés) au noeud des arbres.
     * 
     * @throws IOException 
     */
    @Test
    public void testSearchEngineCountAttachmentNodeMove() throws IOException {
        assumeTrueSearchEngineCanRunTest(); // trees

        // delete Lanfeust book, attached to tree
        WikittyQuery query = new WikittyQueryMaker().and()
                .exteq(CatalogNode.EXT_CATALOGNODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "Action").end();
        CatalogNode actionNode = wikittyClient.findAllByQuery(CatalogNode.class, query).peek();

        query = new WikittyQueryMaker().and()
                .exteq(CatalogNode.EXT_CATALOGNODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "Everything else").end();
        CatalogNode eeNode = wikittyClient.findAllByQuery(CatalogNode.class, query).peek();

        // move action node
        actionNode.setParent(eeNode.getWikittyId());
        wikittyClient.store(actionNode);

        query = new WikittyQueryMaker().and()
                .exteq(CatalogNode.EXT_CATALOGNODE)
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_NAME, "Media").end();
        CatalogNode mediaNode = wikittyClient.findAllByQuery(CatalogNode.class, query).peek();
        WikittyQueryResultTreeNode<String> treeNodeResult =
                wikittyClient.findAllIdTreeNode(mediaNode.getWikittyId(), 0, true, null);
        Assert.assertEquals(5, treeNodeResult.getAttCount()); // 7 before move

        treeNodeResult = wikittyClient.findAllIdTreeNode(eeNode.getWikittyId(), 0, true, null);
        Assert.assertEquals(2, treeNodeResult.getAttCount()); // 0 before move
    }

    /**
     * Test time query duration
     */
    @Test
    public void testTimeQuery() {
        WikittyQuery query = WikittyQueryParser.parse(
            "ProductPicture.price=*");
        WikittyQueryResult<Product> result = wikittyClient.findAllByQuery(Product.class, query);
        Assert.assertTrue(0 < result.getTimeQuery());
        Assert.assertTrue(0 < result.getTimeConvertion());
    }



    @Test
    public void testCryptAndPatternField() throws Exception {
        WikittyExtension ext = new WikittyExtension("UserTest",
                "1.0", // version
                WikittyUtil.tagValuesToMap(" version=\"2.0\""), // tag/values
                (List)null,
                WikittyUtil.buildFieldMapExtension( // building field map
                "String login pattern=\\w+",
                "String password crypt=Blowfish:xxxxxxxx"));

        Wikitty w = new WikittyImpl();
        w.addExtension(ext);
        w.setField("UserTest", "login", "moi");
        w.setField("UserTest", "password", "monpassword");

        w = wikittyClient.store(w);
        {
            WikittyQuery q = new WikittyQueryMaker()
                    .eq(Element.get("UserTest.login"), "moi").end();
            WikittyQueryResult<Wikitty> r =
                    wikittyClient.findAllByQuery(Wikitty.class, q);
            Assert.assertEquals(1, r.size());
// FIXME poussin 20150720 find why this test faild :(
//            Assert.assertEquals("monpassword",
//                    r.peek().getFieldAsString("UserTest", "password"));
        }

        {
            WikittyQuery q = new WikittyQueryMaker()
                    .eq(Element.get("UserTest.password"), "monpassword").end();
            WikittyQueryResult<Wikitty> r =
                    wikittyClient.findAllByQuery(Wikitty.class, q);
            // les champs cryptes ne sont pas indexe et on ne peut donc pas
            // faire de recherche dessus
            Assert.assertEquals(0, r.size());
        }

        w= new WikittyImpl();
        w.addExtension(ext);
        w.setField("UserTest", "login", "++IllegaleLogin++");
        w.setField("UserTest", "password", "monpassword");

        try {
            w = wikittyClient.store(w);
            Assert.assertTrue(false);
        } catch(WikittyException eee) {
            // le storage doit echouer car le pattern ne permet pas les +
            Assert.assertTrue(true);
        }
    }

    @Test
    public void testPreloadSimple() throws Exception {
        WikittyUserImpl user = new WikittyUserImpl();
        user.setLogin("moi");

        WikittyAuthorisationImpl auth = new WikittyAuthorisationImpl();
        auth.setOwner(user.getWikittyId());
        wikittyClient.store(user, auth);

        Wikitty authRestored = wikittyClient.restore(auth.getWikittyId());
        Assert.assertNull(authRestored.getPreloaded().get(user.getWikittyId()));

        wikittyClient.preload(Arrays.asList(authRestored), "WikittyAuthorisation.owner");
        Assert.assertNotNull(authRestored.getPreloaded().get(user.getWikittyId()));
    }

    @Test
    public void testPreloadBoucle() throws Exception {
        WikittyUserImpl user = new WikittyUserImpl();
        user.setLogin("moi");

        WikittyAuthorisationImpl auth = new WikittyAuthorisationImpl();
        auth.setOwner(user.getWikittyId());
        auth.addParent(auth.getWikittyId());

        wikittyClient.store(auth, user);

        Wikitty authRestored = wikittyClient.restore(auth.getWikittyId());
        Assert.assertNull(authRestored.getPreloaded().get(user.getWikittyId()));

        wikittyClient.preload(Arrays.asList(authRestored), "WikittyAuthorisation.parent,WikittyAuthorisation.parent,WikittyAuthorisation.owner");
        // parent doit avoir ete restorer
        Assert.assertNotNull(authRestored.getPreloaded().get(auth.getWikittyId()));
        // mais owner non, car on le demande apres deux fois parent et vu que
        // parent a deja ete traiter vu qu'on boucle sur nous meme, owner ne
        // sera pas preloade (comportement normale)
        Assert.assertNull(authRestored.getPreloaded().get(user.getWikittyId()));
    }

    @Test
    public void testPreloadCollection() throws Exception {
        WikittyLabelImpl l1 = new WikittyLabelImpl();
        l1.addLabels("l1");
        WikittyLabelImpl l2 = new WikittyLabelImpl();
        l2.addLabels("l2");
        WikittyLabelImpl l3 = new WikittyLabelImpl();
        l3.addLabels("l3");

        WikittyGroupImpl group = new WikittyGroupImpl();
        group.addMembers(l1.getWikittyId(), l2.getWikittyId(), l3.getWikittyId());

        wikittyClient.store(group, l1, l2, l3);

        Wikitty groupRestored = wikittyClient.restore(group.getWikittyId());
        Assert.assertNull(groupRestored.getPreloaded().get(l1.getWikittyId()));
        Assert.assertNull(groupRestored.getPreloaded().get(l2.getWikittyId()));
        Assert.assertNull(groupRestored.getPreloaded().get(l3.getWikittyId()));

        wikittyClient.preload(Arrays.asList(groupRestored), "WikittyGroup.members");
        Assert.assertNotNull(groupRestored.getPreloaded().get(l1.getWikittyId()));
        Assert.assertNotNull(groupRestored.getPreloaded().get(l2.getWikittyId()));
        Assert.assertNotNull(groupRestored.getPreloaded().get(l3.getWikittyId()));
    }

    @Test
    public void testRestoreStoreWithPreload() throws Exception {
        WikittyUserImpl moi = new WikittyUserImpl();
        moi.setLogin("moi");

        WikittyAuthorisationImpl auth = new WikittyAuthorisationImpl();
        auth.setOwner(moi.getWikittyId());
        wikittyClient.store(moi, auth);

        Wikitty authRestored = wikittyClient.restore(auth.getWikittyId(),
                "WikittyAuthorisation.owner");
        // on verifie que moi est bien present dans les preload
        Assert.assertNotNull(authRestored.getPreloaded().get(moi.getWikittyId()));

        WikittyUserImpl other = new WikittyUserImpl();
        other.setLogin("other");
//        WikittyAuthorisationHelper.setOwner(authRestored, other);
        authRestored.setFqField(
                WikittyAuthorisation.FQ_FIELD_WIKITTYAUTHORISATION_OWNER, other);
        // on verifie que si on passe un Wikitty en parametre du set, il est
        // bien ajoute au preload, meme s'il n'est pas encore store
        Assert.assertNotNull(authRestored.getPreloaded().get(other.getWikittyId()));

        WikittyUserImpl unknown = new WikittyUserImpl();
        other.setLogin("unknown");
        wikittyClient.store(unknown);

        authRestored.setFqField(
                WikittyAuthorisation.FQ_FIELD_WIKITTYAUTHORISATION_OWNER, unknown.getWikittyId());
        // on verifie que si on passe un id, on a plus rien 
        Assert.assertNull(authRestored.getPreloaded().get(unknown.getWikittyId()));
        Assert.assertNull(authRestored.getFieldAsWikitty(
                WikittyAuthorisation.EXT_WIKITTYAUTHORISATION,
                WikittyAuthorisation.FIELD_WIKITTYAUTHORISATION_OWNER, false));

        authRestored = wikittyClient.store(authRestored);
        // mais qu'apres un store on l'a a nouveau
        Assert.assertNotNull(authRestored.getPreloaded().get(unknown.getWikittyId()));
        Assert.assertNotNull(authRestored.getFieldAsWikitty(
                WikittyAuthorisation.EXT_WIKITTYAUTHORISATION,
                WikittyAuthorisation.FIELD_WIKITTYAUTHORISATION_OWNER, false));

    }


}
