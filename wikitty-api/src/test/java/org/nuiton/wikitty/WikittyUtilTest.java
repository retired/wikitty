/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.entities.BusinessEntity;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyField;
import org.nuiton.wikitty.entities.WikittyGroupImpl;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.entities.WikittyLabelImpl;
import org.nuiton.wikitty.services.WikittyServiceInMemory;

import java.beans.PropertyChangeListener;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Test on WikittyUtil class.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyUtilTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyUtilTest.class);

    @Test
    public void testToDate() throws Exception {
        {
            Date d = new Date();
            String ds = WikittyUtil.toString(d);
            Date d2 = WikittyUtil.toDate(ds);
            Assert.assertEquals(d, d2);
        }
        {
            Date d = WikittyUtil.toDate("20101231");
            Date d3y = WikittyUtil.toDate("20131231");
            Date j1 = WikittyUtil.toDate("20100101");
            Date smd = WikittyUtil.toDate("20100502");


            Assert.assertEquals(smd, WikittyUtil.toDate("20101231=????0502"));
            Assert.assertEquals(d3y, WikittyUtil.toDate("20101231+3YEARS"));
            Assert.assertEquals(j1, WikittyUtil.toDate("20101231/YEAR"));
        }

        //Test all date patterns
        {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
            Date d = format.parse("26/03/13");
            Assert.assertEquals(d, WikittyUtil.toDate("26/03/13"));
        }
        {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy hh:mm");
            Date d = format.parse("26/03/13 20:51");
            Date result = WikittyUtil.toDate("26/03/2013 20:51");
            Assert.assertEquals(d, result);
        }
        {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Date d = format.parse("26/03/2013");
            Assert.assertEquals(d, WikittyUtil.toDate("26/03/2013"));
        }
        {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");
            Date d = format.parse("26/03/2013 20:51");
            Assert.assertEquals(d, WikittyUtil.toDate("26/03/2013 20:51"));
        }
        {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy");
            Date d = format.parse("26-03-13");
            Assert.assertEquals(d, WikittyUtil.toDate("26-03-13"));
        }
        {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy hh:mm");
            Date d = format.parse("26-03-13 20:51");
            Assert.assertEquals(d, WikittyUtil.toDate("26-03-2013 20:51"));
        }
        {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            Date d = format.parse("26-03-2013");
            Assert.assertEquals(d, WikittyUtil.toDate("26-03-2013"));
        }
        {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm");
            Date d = format.parse("26-03-2013 20:51");
            Assert.assertEquals(d, WikittyUtil.toDate("26-03-2013 20:51"));
        }
        {
            SimpleDateFormat format = new SimpleDateFormat("yyMMdd");
            Date d = format.parse("130326");
            Assert.assertEquals(d, WikittyUtil.toDate("130326"));
        }

        // les syntaxes suivantes sont impossible car on ne peut pas faire
        // la difference entre dd-MM-yy et yy-MM-dd. On prend le parti de
        // supporter le 1er et pas le 2eme
//        {
//            SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd");
//            Date d = format.parse("13-03-26");
//            Assert.assertEquals(d, WikittyUtil.toDate("13-03-26"));
//        }
//        {
//            SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd hh:mm");
//            Date d = format.parse("13-03-26 20:51");
//            Assert.assertEquals(d, WikittyUtil.toDate("13-03-26 20:51"));
//        }
//        {
//            SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd'T' hh:mm:ss'Z'");
//            Date d = format.parse("13-03-26'T' 20:51:00'Z'");
//            Assert.assertEquals(d, WikittyUtil.toDate("13-03-26'T' 20:51:00'Z'"));
//        }
    }

    @Test
    public void testFormat() {
        WikittyGroupImpl group = new WikittyGroupImpl();
        group.setName("Mon Groupe");

        // test un cas passant
        String result = WikittyUtil.format(
                "Group Name = %"+WikittyGroupImpl.FQ_FIELD_WIKITTYGROUP_NAME+"$s",
                group.getWikitty());
        Assert.assertEquals("Group Name = Mon Groupe", result);

        // test un cas passant
        result = WikittyUtil.format(
                "Group Name = %"+WikittyGroupImpl.FQ_FIELD_WIKITTYGROUP_NAME+"|noname$s",
                group.getWikitty());
        Assert.assertEquals("Group Name = Mon Groupe", result);

        // test un cas passant avec deux fois le meme champs
        result = WikittyUtil.format(
                "Group Name = %"+WikittyGroupImpl.FQ_FIELD_WIKITTYGROUP_NAME+"$s(%"+WikittyGroupImpl.FQ_FIELD_WIKITTYGROUP_NAME+"$s)",
                group.getWikitty());
        Assert.assertEquals("Group Name = Mon Groupe(Mon Groupe)", result);

        // test un cas ou le champs demande n'existe pas dans le wikitty (sans remplacement)
        result = WikittyUtil.format(
                "Group Name = %"+WikittyGroupImpl.FQ_FIELD_WIKITTYGROUP_NAME+"toto$s",
                group.getWikitty());
        Assert.assertEquals("Group Name = WikittyGroup.nametoto", result);

        // test un cas ou le champs demande n'existe pas dans le wikitty (sans remplacement)
        // avec deux fois le meme champs
        result = WikittyUtil.format(
                "Group Name = %"+WikittyGroupImpl.FQ_FIELD_WIKITTYGROUP_NAME+"toto$s(%"+WikittyGroupImpl.FQ_FIELD_WIKITTYGROUP_NAME+"tata$s)",
                group.getWikitty());
        Assert.assertEquals("Group Name = WikittyGroup.nametoto(WikittyGroup.nametata)", result);

        group = new WikittyGroupImpl();
        // test un cas ou le champs demande n'existe pas dans le wikitty (avec remplacement vide)
        result = WikittyUtil.format(
                "Group Name = %"+WikittyGroupImpl.FQ_FIELD_WIKITTYGROUP_NAME+"|$s",
                group.getWikitty());
        Assert.assertEquals("Group Name = ", result);

        // test un cas ou le champs demande n'existe pas dans le wikitty (avec remplacement
        result = WikittyUtil.format(
                "Group Name = %"+WikittyGroupImpl.FQ_FIELD_WIKITTYGROUP_NAME+"|noname$s",
                group.getWikitty());
        Assert.assertEquals("Group Name = noname", result);
    }

    /**
     * Test of normalizeVersion method, of class WikittyUtil.
     */
    @Test
    public void testNormalizeVersion() {
        {
            String version = null;
            String expResult = "0.0";
            String result = WikittyUtil.normalizeVersion(version);
            Assert.assertEquals(expResult, result);
        }
        {
            String version = "";
            String expResult = "0.0";
            String result = WikittyUtil.normalizeVersion(version);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of versionEquals method, of class WikittyUtil.
     */
    @Test
    public void testVersionEquals() {
        {
            String v1 = "0.4";
            String v2 = "0.4";
            boolean expResult = true;
            boolean result = WikittyUtil.versionEquals(v1, v2);
            Assert.assertEquals(expResult, result);
        }
        {
            String v1 = "0.4.0";
            String v2 = "0.4";
            boolean expResult = false;
            boolean result = WikittyUtil.versionEquals(v1, v2);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of versionGreaterThan method, of class WikittyUtil.
     */
    @Test
    public void testVersionGreaterThan() {
        String v1 = "4.5";
        String v2 = "4.4";
        boolean expResult = true;
        boolean result = WikittyUtil.versionGreaterThan(v1, v2);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of incrementMinorRevision method, of class WikittyUtil.
     */
    @Test
    public void testIncrementMinorRevision() {
        {
            String v = "";
            String expResult = "0.1";
            String result = WikittyUtil.incrementMinorRevision(v);
            Assert.assertEquals(expResult, result);
        }
        {
            String v = "0";
            String expResult = "0.1";
            String result = WikittyUtil.incrementMinorRevision(v);
            Assert.assertEquals(expResult, result);
        }
        {
            String v = "1.0";
            String expResult = "1.1";
            String result = WikittyUtil.incrementMinorRevision(v);
            Assert.assertEquals(expResult, result);
        }
        {
            String v = "1.4";
            String expResult = "1.5";
            String result = WikittyUtil.incrementMinorRevision(v);
            Assert.assertEquals(expResult, result);
        }
        {
            String v = "1.9";
            String expResult = "1.10";
            String result = WikittyUtil.incrementMinorRevision(v);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test la difference de temps entre differente methode d'increment de version
     * qui est sous forme texte.
     *
     * <ul>
     * <li> parsing Integer + 1 (757ms / 3 millions d'appels)</li>
     * <li> switch sur les caracteres (419ms / 3 millions d'appels)</li>
     * <li> appel d'une methode d'increment (462ms / 3 millions d'appels)</li>
     * </ul>
     */
    @Test
    public void testPerfIncrementMinorRevision() {
        int MAX = 3000000;
        String v = "0";

        long time = System.currentTimeMillis();
        for (int cpt = 0; cpt < MAX; cpt++) {
            int i = Integer.parseInt(v) + 1;
            v = String.valueOf(i);
        }
        long timeParse = System.currentTimeMillis() - time;

        v = "0";
        time = System.currentTimeMillis();
        for (int cpt = 0; cpt < MAX; cpt++) {
            v = inc(v);
        }
        long timeCall = System.currentTimeMillis() - time;

        v = "0";
        time = System.currentTimeMillis();
        for (int cpt = 0; cpt < MAX; cpt++) {
            char[] c = v.toCharArray();
            boolean retenue = true;
            for (int i = c.length - 1; retenue && i >= 0; i--) {
                switch (c[i]) {
                case '0':
                    c[i] = '1';
                    retenue = false;
                    break;
                case '1':
                    c[i] = '2';
                    retenue = false;
                    break;
                case '2':
                    c[i] = '3';
                    retenue = false;
                    break;
                case '3':
                    c[i] = '4';
                    retenue = false;
                    break;
                case '4':
                    c[i] = '5';
                    retenue = false;
                    break;
                case '5':
                    c[i] = '6';
                    retenue = false;
                    break;
                case '6':
                    c[i] = '7';
                    retenue = false;
                    break;
                case '7':
                    c[i] = '8';
                    retenue = false;
                    break;
                case '8':
                    c[i] = '9';
                    retenue = false;
                    break;
                default:
                    c[i] = '0';
                    break;
                }
            }
            if (retenue) {
                v = "1" + String.valueOf(c);
            } else {
                v = String.valueOf(c);
            }
        }
        long timeSwitch = System.currentTimeMillis() - time;

        log.info("Inc parse: " + timeParse + " call: " + timeCall + " switch: "
                + timeSwitch + " (" + v + ")");
    }

    protected String inc(String v) {
        char[] c = v.toCharArray();
        boolean retenue = true;
        for (int i = c.length - 1; retenue && i >= 0; i--) {
            switch (c[i]) {
            case '0':
                c[i] = '1';
                retenue = false;
                break;
            case '1':
                c[i] = '2';
                retenue = false;
                break;
            case '2':
                c[i] = '3';
                retenue = false;
                break;
            case '3':
                c[i] = '4';
                retenue = false;
                break;
            case '4':
                c[i] = '5';
                retenue = false;
                break;
            case '5':
                c[i] = '6';
                retenue = false;
                break;
            case '6':
                c[i] = '7';
                retenue = false;
                break;
            case '7':
                c[i] = '8';
                retenue = false;
                break;
            case '8':
                c[i] = '9';
                retenue = false;
                break;
            default:
                c[i] = '0';
                break;
            }
        }
        if (retenue) {
            v = "1" + String.valueOf(c);
        } else {
            v = String.valueOf(c);
        }
        return v;
    }

    /**
     * Test of incrementMajorRevision method, of class WikittyUtil.
     */
    @Test
    public void testIncrementMajorRevision() {
        {
            String v = "";
            String expResult = "1.0";
            String result = WikittyUtil.incrementMajorRevision(v);
            Assert.assertEquals(expResult, result);
        }
        {
            String v = "0";
            String expResult = "1.0";
            String result = WikittyUtil.incrementMajorRevision(v);
            Assert.assertEquals(expResult, result);
        }
        {
            String v = "1.5";
            String expResult = "2.0";
            String result = WikittyUtil.incrementMajorRevision(v);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of toBigDecimal method, of class WikittyUtil.
     */
    @Test
    public void testToBigDecimal() {
        Object value = null;
        BigDecimal expResult = new BigDecimal(0);
        BigDecimal result = WikittyUtil.toBigDecimal(value);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of toBoolean method, of class WikittyUtil.
     */
    @Test
    public void testToBoolean() {
        Object value = null;
        boolean expResult = false;
        boolean result = WikittyUtil.toBoolean(value);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class WikittyUtil.
     */
    @Test
    public void testToString() {
        Object value = null;
        String expResult = null;
        String result = WikittyUtil.toString(value);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of toDate method, of class WikittyUtil.
     */
    @Test
    public void testToDateNull() {
        Object value = null;
        Date expResult = null;
        Date result = WikittyUtil.toDate(value);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of toWikitty method, of class WikittyUtil.
     */
    @Test
    public void testToWikitty() {
        Object value = null;
        String expResult = null;
        String result = WikittyUtil.toWikitty(value);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of toList method, of class WikittyUtil.
     */
    @Test
    public void testToList() {
        Object value = null;
        Class<Object> clazz = Object.class;
        List<Object> expResult = null;
        List<Object> result = WikittyUtil.toList(value, clazz);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getClass method, of class WikittyUtil.
     */
    @Test
    public void testGetClass() {
        Object value = null;
        Class expResult = null;
        Class result = WikittyUtil.getClass(value);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of cast method, of class WikittyUtil.
     */
    @Test
    public void testCast() {
        {
            Object obj = null;
            Class<Object> clazz = null;
            Object expResult = null;
            Object result = WikittyUtil.cast(obj, clazz);
            Assert.assertEquals(expResult, result);
        }
        {
            Object obj = "Toto";
            Class<Object> clazz = Object.class;
            Object expResult = "Toto";
            Object result = WikittyUtil.cast(obj, clazz);
            Assert.assertEquals(expResult, result);
        }
        {
            Object obj = new Object();
            Class<String> clazz = String.class;
            try {
                WikittyUtil.cast(obj, clazz);
                Assert.fail("cast object to string !!!");
            } catch (Exception eee) {
                // ok
            }
        }
    }

    @Test
    public void testUID() throws Exception {
        for (int i = 0; i < 8; i++) {
            String uid = WikittyUtil.genUID();
            log.info("uid = " + uid);
        }
    }

    @Test
    public void testWikittyExtensionBuild() {
        String extName = "Produit";

        LinkedHashMap<String, FieldType> fields = WikittyUtil
                .buildFieldMapExtension("String name", "Numeric amount",
                        "Date buildDate");

        Assert.assertNotNull(fields.get("name"));
        Assert.assertNotNull(fields.get("amount"));
        Assert.assertNotNull(fields.get("buildDate"));

        WikittyExtension ext = new WikittyExtension(extName, "1", fields);

        Assert.assertNotNull(ext.getFieldType("name"));
        Assert.assertNotNull(ext.getFieldType("amount"));
        Assert.assertNotNull(ext.getFieldType("buildDate"));
    }

    @Test
    public void testWikittyExtensionTagValue() {
        String accentValue = "Avèc_dés_açcents";
        String floatValue = "3.3";
        String dateValue = "2009-12-10T12:45:31:551Z";
        String whitespaceCharacters = "ertyuio\n   <df ds \r\rf sf >sd f\n\tdf";

        LinkedHashMap<String, FieldType> fields = WikittyUtil
                .buildFieldMapExtension("String name test=" + accentValue,
                        "Numeric amount defaultValue=" + floatValue,
                        "Date buildDate now=" + dateValue);

        FieldType fieldName = fields.get("name");
        Assert.assertEquals(accentValue, fieldName.getTagValue("test"));
        Assert.assertEquals(floatValue,
                fields.get("amount").getTagValue("defaultValue"));
        Assert.assertEquals(dateValue,
                fields.get("buildDate").getTagValue("now"));

        fieldName.addTagValue("whitespaceCharacters", whitespaceCharacters);
        String toDefinition = fieldName.toDefinition("name");
        WikittyUtil.parseField(toDefinition, fieldName);
    }

    // use in testNewInstance
    static class MonLabel extends WikittyLabelImpl {
        /** serialVersionUID. */
        private static final long serialVersionUID = 1L;
    }

    /** 
     * Check that a date is the same after parse and format.
     * @throws Exception 
     */
    @Test
    public void testDateConsistency() throws Exception {
        Date date = WikittyUtil.parseDate("30/01/2009");
        Date dateFormatedAndParsed = WikittyUtil.parseDate(WikittyUtil
                .formatDate(date));
        Assert.assertEquals(date, dateFormatedAndParsed);

        date = new Date();
        dateFormatedAndParsed = WikittyUtil.parseDate(WikittyUtil
                .formatDate(date));
        Assert.assertEquals(date, dateFormatedAndParsed);

    }

    @Test
    public void testParseDate() throws ParseException {
        String date = "1982-01-22T23:00:00.000+0000Z";
        Date date2 = WikittyUtil.parseDate(date);
        Assert.assertNotNull(date2);
    }

    @Test
    public void testGetWikitty() {

        WikittyLabel label = new WikittyLabelImpl();
        label.addLabels("Test");

        String wikittyId = label.getWikittyId();

        Wikitty labelWikitty = WikittyUtil.getWikitty(label);

        Assert.assertEquals(wikittyId, labelWikitty.getWikittyId());
        Assert.assertEquals(Collections.singleton("Test"),labelWikitty.getFieldAsObject(WikittyLabel.EXT_WIKITTYLABEL,WikittyLabel.FIELD_WIKITTYLABEL_LABELS));

        ApplicationConfig config = WikittyConfig.getConfig();
        WikittyServiceInMemory ws = new WikittyServiceInMemory(config);

        ws.store(null, Collections.singleton(labelWikitty), false);

        LabelDTO dto = new LabelDTO(wikittyId);
        dto.addLabels("toto");
        dto.setWikittyVersion("2.0");

        Wikitty w = WikittyUtil.getWikitty(ws, null, dto);
        WikittyLabel l = new WikittyLabelImpl(w);

        Assert.assertEquals(dto.getWikittyId(), l.getWikittyId());
        Assert.assertEquals(dto.getWikittyVersion(), l.getWikittyVersion());
        Assert.assertEquals(dto.getLabels(), l.getLabels());
    }

    @Test
    public void testCopyBean() throws Exception {
        WikittyLabel source = new WikittyLabelImpl(); // version 0.1 (init)
        source.addLabels("toto"); // changes : new version is 0.2
        WikittyLabel dest = new WikittyLabelImpl(); // version 0.1 (init)
        
        WikittyUtil.copyBean(source, dest);

        Assert.assertFalse(source.getWikittyId().equals(dest.getWikittyId()));
        Assert.assertEquals(source.getWikittyVersion(), dest.getWikittyVersion());
        Assert.assertEquals(source.getLabels(), dest.getLabels());
    }

    @Test
    public void testCopyFrom() throws Exception {
        {
//            System.out.println("Dto => W");
            WikittyLabel source = new LabelDTO(WikittyUtil.genUID());
            source.addLabels("toto");
            source.addLabels("titi");
            WikittyLabel dest = new WikittyLabelImpl();

            dest.copyFrom(source);
            Assert.assertFalse(source.getWikittyId().equals(dest.getWikittyId()));
            Assert.assertEquals(source.getWikittyVersion(), dest.getWikittyVersion());
            Assert.assertEquals(source.getLabels(), dest.getLabels());
        }
        {
//            System.out.println("W => Dto");
            WikittyLabel source = new WikittyLabelImpl();
            source.addLabels("toto");
            source.addLabels("titi");
            WikittyLabel dest = new LabelDTO(WikittyUtil.genUID());

            dest.copyFrom(source);
            Assert.assertFalse(source.getWikittyId().equals(dest.getWikittyId()));
            Assert.assertEquals(source.getWikittyVersion(), dest.getWikittyVersion());
            Assert.assertEquals(source.getLabels(), dest.getLabels());
        }
    }

    @Test
    public void testClone() throws Exception {
        {
            WikittyLabel source = new WikittyLabelImpl();
            source.setWikittyVersion("1.0");
            source.addLabels("toto");
            source.addLabels("titi");

            Wikitty labelWikitty = WikittyUtil.getWikitty(source);
            Wikitty cloned = WikittyUtil.clone(labelWikitty, false);

            WikittyLabelImpl labelCloned = new WikittyLabelImpl(cloned);

            Assert.assertTrue(labelWikitty.getWikittyId().equals(cloned.getWikittyId()));
            Assert.assertEquals(labelWikitty.getWikittyVersion(), cloned.getWikittyVersion());
            Assert.assertTrue(source.getWikittyId().equals(labelCloned.getWikittyId()));
            Assert.assertEquals(source.getWikittyVersion(), labelCloned.getWikittyVersion());
            Assert.assertEquals(source.getLabels(), labelCloned.getLabels());
        }
        {
            WikittyLabel source = new WikittyLabelImpl();
            source.setWikittyVersion("1.0");
            source.addLabels("toto");
            source.addLabels("titi");

            Wikitty labelWikitty = WikittyUtil.getWikitty(source);
            Wikitty cloned = WikittyUtil.clone(labelWikitty, true);

            WikittyLabelImpl labelCloned = new WikittyLabelImpl(cloned);

            Assert.assertFalse(labelWikitty.getWikittyId().equals(cloned.getWikittyId()));
            Assert.assertNotSame(labelWikitty.getWikittyVersion(), cloned.getWikittyVersion());
            Assert.assertFalse(source.getWikittyId().equals(labelCloned.getWikittyId()));
            Assert.assertNotSame(source.getWikittyVersion(), labelCloned.getWikittyVersion());
            Assert.assertEquals(source.getLabels(), labelCloned.getLabels());
        }
    }

    @Test
    public void testCrypt() throws Exception {
        String[] algos = {"Blowfish"};
        String password = "password";
        byte[] value = "Le message en clair".getBytes();

        for (String algo : algos) {
            byte[] valueCrypt = WikittyUtil.crypt(algo, password, value);
            Assert.assertFalse(Arrays.equals(value, valueCrypt));

            byte[] valueDecrypt = WikittyUtil.decrypt(algo, password, valueCrypt);
            Assert.assertArrayEquals(value, valueDecrypt);
        }
    }

    static public class LabelDTO implements WikittyLabel {

        protected String wikittyId;
        protected String wikittyVersion = "0.0";
        @WikittyField(fqn="WikittyLabel.labels")
        protected Set<String> labels = new LinkedHashSet<String>();

        public LabelDTO(String wikittyId) {
            this.wikittyId = wikittyId;
        }

        @Override
        public String getWikittyId() {
            return wikittyId;
        }

        @Override
        public String getWikittyVersion() {
            return wikittyVersion;
        }

        @Override
        public void setWikittyVersion(String version) {
            this.wikittyVersion = version;
        }

        /**
         * getLabels :
         * @return Set&lt;String&gt;
         */
        @Override
        public Set<String> getLabels() {
            return labels;
        }

        @Override
        public void setLabels(Set<String> labels) {
            this.labels = new LinkedHashSet<String>(labels);
        }

        @Override
        public void addAllLabels(Collection<String> labels) {
            this.labels.addAll(labels);
        }

        /**
         * addLabels :
         * @param element
         */
        @Override
        public void addLabels(String... element) {
            for (String v : element) {
                labels.add(v);
            }
        }

        /**
         * removeLabels :
         * @param element
         */
        @Override
        public void removeLabels(String... element) {
            for (String v : element) {
                labels.remove(v);
            }
        }

        /**
         * clearLabels :
         */
        @Override
        public void clearLabels() {
            labels.clear();
        }

        @Override
        public void copyFrom(BusinessEntity source) {
            try {
                BeanUtils.copyProperties(this, source);
            } catch (Exception eee) {
                throw new WikittyException(String.format(
                        "Can't copy source object %s", source), eee);
            }
        }

        @Override
        public Collection<String> getExtensionFields(String ext) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Collection<String> getStaticExtensionNames() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Collection<String> getExtensionNames() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Object getField(String ext, String fieldName) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Object getFieldAsObject(String ext, String fieldName) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void setField(String ext, String fieldName, Object value) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

    }
}
