/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.test;

import org.nuiton.wikitty.entities.BusinessEntityImpl;
import org.nuiton.wikitty.entities.Wikitty;

/**
 * Category entity implementation.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class CategoryImpl extends CategoryAbstract {

    private static final long serialVersionUID = 7003155914532532322L;

    public CategoryImpl() {

    }
    
    public CategoryImpl(String code, String name) {
        this();
        setCode(code);
        setName(name);
    }

    public CategoryImpl(Wikitty wikitty) {
        super(wikitty);
    }

    public CategoryImpl(BusinessEntityImpl businessEntityImpl) {
        super(businessEntityImpl.getWikitty());
    }

} //CategoryEntityImpl
