/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.test;

import org.nuiton.wikitty.entities.BusinessEntityImpl;
import org.nuiton.wikitty.entities.Wikitty;

/**
 * Product entity implementation.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ProductImpl extends ProductAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = 3876446488053147070L;

    /**
     * Empty constructor.
     */
    public ProductImpl() {

    }
    
    /**
     * Constructor with name.
     * 
     * @param name name
     */
    public ProductImpl(String name) {
        this();
        setName(name);
    }

    /**
     * @param businessEntityImpl
     */
    public ProductImpl(BusinessEntityImpl businessEntityImpl) {
        super(businessEntityImpl);
    }

    /**
     * @param wikitty
     */
    public ProductImpl(Wikitty wikitty) {
        super(wikitty);
    }

    /*
     * @see org.nuiton.wikitty.test.ProductEntity#totalPrice()
     */
    @Override
    public int totalPrice() {
        return getPrice() + getPicturePrice() + getPriceFromProduct();
    }
}
