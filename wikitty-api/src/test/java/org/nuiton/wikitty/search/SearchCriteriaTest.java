/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.search;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.operators.And;
import org.nuiton.wikitty.search.operators.AssociatedRestriction;
import org.nuiton.wikitty.search.operators.Between;
import org.nuiton.wikitty.search.operators.BinaryOperator;
import org.nuiton.wikitty.search.operators.Equals;
import org.nuiton.wikitty.search.operators.Or;
import org.nuiton.wikitty.search.operators.Restriction;
import org.nuiton.wikitty.search.operators.RestrictionName;
import org.nuiton.wikitty.search.Search;

/**
 * @deprecated since 3.4, will be removed in 4
 */
@Deprecated
public class SearchCriteriaTest {

    static private Log log = LogFactory.getLog(SearchCriteriaTest.class);

    String elementName = "ELM_NAME",
        value = "Value";
    
    @Test
    public void testBinaryOperators() throws Exception {
        checkSearch( q().eq(elementName, value), RestrictionName.EQUALS );
        checkSearch( q().neq(elementName, value), RestrictionName.NOT_EQUALS );
        checkSearch( q().lt(elementName, value), RestrictionName.LESS );
        checkSearch( q().le(elementName, value), RestrictionName.LESS_OR_EQUAL );
        checkSearch( q().gt(elementName, value), RestrictionName.GREATER );
        checkSearch( q().ge(elementName, value), RestrictionName.GREATER_OR_EQUAL );
        checkSearch( q().sw(elementName, value), RestrictionName.STARTS_WITH );
        checkSearch( q().ew(elementName, value), RestrictionName.ENDS_WITH );
    }
    
    @Test
    public void testBetween() throws Exception {
        Criteria criteria = q().bw(elementName, "from", "to").criteria();
        Restriction restriction = criteria.getRestriction();
        Assert.assertTrue( restriction instanceof Between );
        Between between = (Between) restriction;
        Assert.assertEquals( "from", between.getMin() );
        Assert.assertEquals( "to", between.getMax() );
    }
    
    @Test
    public void testAnd() throws Exception {
        Restriction dto = q()
        .eq(elementName + "0", value + "0")
        .gt(elementName + "1", value + "1")
        .ew(elementName + "2", value + "2")
        .criteria().getRestriction();
        Assert.assertTrue( dto instanceof And );
        And and = (And) dto;
        Restriction[] dtos = and.getRestrictions().toArray(new Restriction[]{});
        checkRestriction(dtos[0], RestrictionName.EQUALS, elementName + "0", value + "0");
        checkRestriction(dtos[1], RestrictionName.GREATER, elementName + "1", value + "1");
        checkRestriction(dtos[2], RestrictionName.ENDS_WITH, elementName + "2", value + "2");
    }
    
    @Test
    public void testOr() throws Exception {
        String[] elementNames = new String[4];
        String[] values = new String[4];
        
        for (int i = 0; i < elementNames.length; i++) {
            elementNames[i] = elementName + i;
            values[i] = value + i;
        }

        Search q = q().eq(elementNames[0], values[0]);
        Search qor = q.or()
        .gt(elementNames[1], values[1])
        .ew(elementNames[2], values[2])
        .lt(elementNames[3], values[3]);
        Restriction dto = q.criteria().getRestriction();
        Assert.assertTrue( "Restriction is " + dto.getClass().getName(), dto instanceof And );
        And and = (And) dto;
        Restriction[] dtos = and.getRestrictions().toArray(new Restriction[]{});
        checkRestriction(dtos[0], RestrictionName.EQUALS, elementName + "0", value + "0");
        Assert.assertTrue( "Restriction is " + dtos[1].getClass().getName(), dtos[1] instanceof Or );
        Or or = (Or) dtos[1];
        dtos = or.getRestrictions().toArray(new Restriction[]{});
        checkRestriction(dtos[0], RestrictionName.GREATER, elementName + "1", value + "1");
        checkRestriction(dtos[1], RestrictionName.ENDS_WITH, elementName + "2", value + "2");
        checkRestriction(dtos[2], RestrictionName.LESS, elementName + "3", value + "3");
    }
    
    @Test
    public void testAssociated() throws Exception {

        /* Following request mean is :
         * Looking for any product which price is between 15 and 25,
         * and name starts with 'Paint',
         * and with an associated category which name is 'Hardware'
         */
        
        Restriction dto = q()
        .bw( "Product.price", "15", "25")
        .sw( "Product.name", "Paint")
        .associated( "Product.category" )
            .eq( "Category.name", "Hardware" )
        .criteria().getRestriction();
        
        Assert.assertTrue( "Restriction is " + dto.getClass().getName(), dto instanceof AssociatedRestriction );
        AssociatedRestriction ass = (AssociatedRestriction) dto;
        Restriction embedded = ass.getRestriction();
        Assert.assertTrue( "Restriction is " + dto.getClass().getName(), embedded instanceof Equals );
        checkRestriction(embedded, RestrictionName.EQUALS, "Category.name", "Hardware");
        Assert.assertEquals( "Product.category", ass.getElement().getName() );
    }

    @Test
    public void testXML() throws Exception {
        Criteria criteria = q().bw(elementName, "from", "to").criteria();

        // Serialize
        String xml = Criteria.toXML(criteria);
        log.debug("[XML] " + xml);
        Assert.assertNotNull(xml);

        // Deserialize
        criteria = Criteria.fromXML(xml);
        Assert.assertNotNull(criteria);
    }

    /*
     * -============ private test utils operations ============-
     */
    protected static Search q() {
        return Search.query();
    }

    protected void checkSearch( Search search, RestrictionName name ) throws Exception {
        Restriction dto = search.criteria().getRestriction();
        checkRestriction( dto, name, elementName, value );
    }

    protected void checkRestriction(Restriction dto, RestrictionName name,
            String element, String value) throws Exception {
        Assert.assertTrue( dto instanceof BinaryOperator );
        BinaryOperator bop = (BinaryOperator) dto;
        Assert.assertEquals( name, dto.getName() );
        Assert.assertEquals( element, bop.getElement().getName() );
        Assert.assertEquals( value, bop.getValue() );
    }

}
