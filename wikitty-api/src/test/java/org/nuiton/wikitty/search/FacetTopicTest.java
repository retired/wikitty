/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/*
 * Copyright (c) 2011 poussin. All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.nuiton.wikitty.search;

import java.util.TreeSet;
import org.junit.Test;

/**
 *
 * @author poussin
 * @deprecated since 3.4, will be removed in 4
 */
@Deprecated
public class FacetTopicTest {
    
    /**
     * Test of toString method, of class FacetTopic.
     */
    @Test
    public void testNameComparator() {
        TreeSet<FacetTopic> countSort = new TreeSet<FacetTopic>(new FacetTopicCountComparator());
        TreeSet<FacetTopic> nameSort = new TreeSet<FacetTopic>(new FacetTopicNameComparator(false));
        TreeSet<FacetTopic> nameIgnoreCaseSort = new TreeSet<FacetTopic>(new FacetTopicNameComparator(true));
        
        FacetTopic a5 = new FacetTopic("test", "a", 5);
        FacetTopic b4 = new FacetTopic("test", "b", 4);
        FacetTopic c3 = new FacetTopic("test", "c", 3);
        FacetTopic d2 = new FacetTopic("test", "d", 2);
        FacetTopic e1 = new FacetTopic("test", "e", 1);
        FacetTopic A0 = new FacetTopic("test", "A", 0);
        
        
        countSort.add(a5);
        nameSort.add(a5);
        nameIgnoreCaseSort.add(a5);
        
        countSort.add(b4);
        nameSort.add(b4);
        nameIgnoreCaseSort.add(b4);

        countSort.add(c3);
        nameSort.add(c3);
        nameIgnoreCaseSort.add(c3);

        countSort.add(d2);
        nameSort.add(d2);
        nameIgnoreCaseSort.add(d2);

        countSort.add(e1);
        nameSort.add(e1);
        nameIgnoreCaseSort.add(e1);

        countSort.add(A0);
        nameSort.add(A0);
        nameIgnoreCaseSort.add(A0);

        //TODO poussin 20110418 faire de vrai assert, mais comme faire au plus simple ?
        System.out.println("count: " + countSort);
        System.out.println("nameSort: " + nameSort);
        System.out.println("nameIgnoreCaseSort: " + nameIgnoreCaseSort);
    }
}
