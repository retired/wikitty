/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.addons.importexport;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.util.StringUtil;

/**
 * Test for CSV import export class
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ImportExportCSVTest {

    /**
     * Test que le pattern match les bonnes choses.
     */
    @Test
    public void testQueryPattern() {

        ImportExportCSV task = new ImportExportCSV();
        Pattern pattern = task.queryPattern;
        
        Matcher m = pattern.matcher("rtet");
        Assert.assertFalse(m.find());
        
        m = pattern.matcher("Client=4e2e4a93-c412-46b8-beb9-e76e0f16740d");
        Assert.assertFalse(m.find());
        
        m = pattern.matcher("Client=4e2e4a93-c412-46b8-beb9-e76e0f16740d");
        Assert.assertFalse(m.find());
        
        m = pattern.matcher("Client.name=toto");
        Assert.assertTrue(m.find());
        Assert.assertEquals("Client.name", m.group(1));
        Assert.assertEquals("Client", m.group(2));
        Assert.assertEquals("name", m.group(3));
        Assert.assertEquals("toto", m.group(4));
        
        m = pattern.matcher("Client.name=toto");
        Assert.assertTrue(m.find());
        Assert.assertEquals("toto", m.group(6));
        
        m = pattern.matcher("Client.name=\"toto\"");
        Assert.assertTrue(m.find());
        Assert.assertEquals("toto", m.group(5));
        
        m = pattern.matcher("Client.name=\"toto");
        Assert.assertFalse(m.find());
        
        m = pattern.matcher("Client.name=\"to\"to\"");
        Assert.assertTrue(m.find());
        Assert.assertEquals("to\"to", m.group(5));
        
        m = pattern.matcher("Client.name=toto\"");
        Assert.assertFalse(m.find());
    }
    
    /**
     * Test de parsing des valeurs multiples wikitty.
     */
    @Test
    public void testMultipleFieldsParsing() {
        String test = "(sdfsdf),(dfsdf),(fdsfsdfg),(sdfsdfqs)";
        String[] result = StringUtil.split(test);
        Assert.assertEquals(4, result.length);
        Assert.assertEquals("(sdfsdf)", result[0]);
        Assert.assertEquals("sdfsdf", result[0].substring(1, result[0].length() -1));
        Assert.assertEquals("(dfsdf)", result[1]);
        Assert.assertEquals("(fdsfsdfg)", result[2]);
        Assert.assertEquals("(sdfsdfqs)", result[3]);
        
        test = "(sdf\",\"sdf),(df()sdf),(fds\"(),()\"fsdfg),(sdfsdfqs)";
        result = StringUtil.split(test);
        Assert.assertEquals(4, result.length);
        Assert.assertEquals("(sdf\",\"sdf)", result[0]);
        Assert.assertEquals("(df()sdf)", result[1]);
        Assert.assertEquals("(fds\"(),()\"fsdfg)", result[2]);
        Assert.assertEquals("(sdfsdfqs)", result[3]);
        
        test = "toto ?";
        result = StringUtil.split(test);
        Assert.assertEquals(1, result.length);
        Assert.assertEquals("toto ?", result[0]);
    }
}
