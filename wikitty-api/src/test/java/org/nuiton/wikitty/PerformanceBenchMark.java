/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyTypes;

/**
 * This class is <strong>NOT</strong> a test. It's a benchlark that
 * computes values and log them.  This class is named properly to
 * prevent any test Runner to run it.
 *
 * @author ruchaud
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class PerformanceBenchMark extends WikittyClientAbstractTest {

    protected final static Log log = LogFactory.getLog(PerformanceBenchMark.class);

    protected final static int WIKITTY_SIZE = 10000;

    @Test
    public void testPerformanceWikitty() throws Exception {
        testPerformanceWikitty(3);
        testPerformanceWikitty(50);
        testPerformanceWikitty(100);
    }

    protected void testPerformanceWikitty(int fieldSize) {
        WikittyExtension extension = new WikittyExtension("Performance" + fieldSize);
        for (int i = 0; i < fieldSize; i++) {
            FieldType fieldType = new FieldType(WikittyTypes.STRING, 1, 1);
            extension.addField("field" + i, fieldType);
        }
        wikittyClient.storeExtension(Arrays.asList(extension));

        List<Wikitty> wikitties = new ArrayList<Wikitty>(WIKITTY_SIZE);
        for (int i = 0; i < WIKITTY_SIZE; i++) {
            Wikitty wikitty = new WikittyImpl("Performance" + fieldSize + "id" + i);
            wikitty.addExtension(extension);
            for (int j = 0; j < fieldSize; j++) {
                wikitty.setField("Performance" + fieldSize, "field" + j, "value" + j);
            }
            wikitties.add(wikitty);
        }
        long start = System.currentTimeMillis();
        wikittyClient.storeWikitty(wikitties);
        long end = System.currentTimeMillis();
        log.info("[Performance" + fieldSize + "] Write times : " + ((end - start) / 1000) + "s");

        Random random = new Random();
        start = System.currentTimeMillis();
        for (int i = 0; i < WIKITTY_SIZE; i++) {
            int nextInt = random.nextInt(WIKITTY_SIZE);
            wikittyClient.restore(Arrays.asList("Performance" + fieldSize + "id" + nextInt));
        }
        end = System.currentTimeMillis();
        log.info("[Performance" + fieldSize + "] Read times : " + ((end - start) / 1000) + "s");
    }
}
