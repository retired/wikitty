/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.storage;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyGroup;
import org.nuiton.wikitty.entities.WikittyGroupImpl;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.entities.WikittyLabelImpl;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittySearchEngineInMemoryTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittySearchEngineInMemoryTest.class);

    @Test
    public void testfindAllByQuery() throws Exception {

        // initialisation des données
        // 2 label: l1("toutou", "titi") et l2()
        // 3 group: g1("MonGroup1", [l1]), g2("MonGroup2", [l2]), g3("MonGroup3", [l1,l2])
        
        WikittyLabelImpl l1 = new WikittyLabelImpl(new WikittyImpl("l1"));
        l1.addLabels("toutou");
        l1.addLabels("titi");
        
        WikittyLabelImpl l2 = new WikittyLabelImpl(new WikittyImpl("l2"));

        WikittyGroupImpl g1 = new WikittyGroupImpl(new WikittyImpl("g1"));
        g1.setName("MonGroup1");
        g1.addMembers(l1.getWikittyId());

        WikittyGroupImpl g2 = new WikittyGroupImpl(new WikittyImpl("g2"));
        g2.setName("MonGroup2");
        g2.addMembers(l2.getWikittyId());

        WikittyGroupImpl g3 = new WikittyGroupImpl(new WikittyImpl("g3"));
        g3.setName("MonGroup3");
        g3.addMembers(l1.getWikittyId());
        g3.addMembers(l2.getWikittyId());


        Collection<Wikitty> wikitties = new LinkedList<Wikitty>();
        wikitties.add(l1.getWikitty());
        wikitties.add(l2.getWikitty());
        wikitties.add(g1.getWikitty());
        wikitties.add(g2.getWikitty());
        wikitties.add(g3.getWikitty());

        WikittyStorageInMemory storage = new WikittyStorageInMemory();
        storage.store(null, wikitties, false);

        WikittySearchEngineInMemory se = new WikittySearchEngineInMemory(storage);

        {
            // recherche des labels
            WikittyQuery q = new WikittyQueryMaker()
                    .eq(WikittyLabel.FQ_FIELD_WIKITTYLABEL_LABELS, "toutou").end();
            WikittyQueryResult<String> result = se.findAllByQuery(null, q).convertMapToSimple();
            // seul l1 doit etre retrouve
            Assert.assertEquals(1, result.size());
            Assert.assertEquals(l1.getWikittyId(), result.peek());
        }

        {
            // recherche des du keyword 'i'
            WikittyQuery q = new WikittyQueryMaker().keyword("i").end();
            WikittyQueryResult<String> result = se.findAllByQuery(null, q).convertMapToSimple();
            // seul l1 doit etre retrouve
            Assert.assertEquals(1, result.size());
            Assert.assertEquals(l1.getWikittyId(), result.peek());
        }

        {
            // recherche des du keyword 'ou'
            WikittyQuery q = new WikittyQueryMaker().keyword("ou").end();
            WikittyQueryResult<String> result = se.findAllByQuery(null, q).convertMapToSimple();
            // l1 et g1, g2, g3 doivent etre retrouve
            Assert.assertEquals(4, result.size());
            Assert.assertTrue(result.getAll().containsAll(Arrays.asList(
                    l1.getWikittyId(), g1.getWikittyId(), g2.getWikittyId(), g3.getWikittyId())));
        }

        {
            // recherche des du keyword 'ou'
            WikittyQuery q = new WikittyQueryMaker()
                    .and().exteq(WikittyGroup.EXT_WIKITTYGROUP).keyword("ou").end();
            WikittyQueryResult<String> result = se.findAllByQuery(null, q).convertMapToSimple();
            // g1, g2, g3 doivent etre retrouve mais pas l1 (pas la bonne extension)
            Assert.assertEquals(3, result.size());
            Assert.assertTrue(result.getAll().containsAll(Arrays.asList(
                    g1.getWikittyId(), g2.getWikittyId(), g3.getWikittyId())));
        }

        {
            // recherche des labels
            WikittyQuery q = new WikittyQueryMaker()
                    .isNull(WikittyLabel.FQ_FIELD_WIKITTYLABEL_LABELS).end();
            WikittyQueryResult<String> result = se.findAllByQuery(null, q).convertMapToSimple();
            // seul l2 doit etre retrouve
            Assert.assertEquals(1, result.size());
            Assert.assertEquals(l2.getWikittyId(), result.peek());
        }

        {
            WikittyQuery q = new WikittyQueryMaker()
                    .like(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, "*P2").end();
            WikittyQueryResult<String> result = se.findAllByQuery(null, q).convertMapToSimple();
            // seul g2 doit etre retrouve
            Assert.assertEquals(1, result.size());
            Assert.assertEquals(g2.getWikittyId(), result.peek());
        }

        {
            WikittyQuery q = new WikittyQueryMaker()
                    .ew(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, "p2").end();
            WikittyQueryResult<String> result = se.findAllByQuery(null, q).convertMapToSimple();
            // seul g2 doit etre retrouve
            Assert.assertEquals(1, result.size());
            Assert.assertEquals(g2.getWikittyId(), result.peek());
        }

        { // test join
            WikittyQuery q = new WikittyQueryMaker()
                    .containsOne(WikittyGroup.FQ_FIELD_WIKITTYGROUP_MEMBERS)
                      .select(Element.ID)
                        .eq(WikittyLabel.FQ_FIELD_WIKITTYLABEL_LABELS, "titi")
                    .end();
            WikittyQueryResult<String> result = se.findAllByQuery(null, q).convertMapToSimple();
            // g1, g3 doivent etre retrouve via le join
            Assert.assertEquals(2, result.size());
            Assert.assertTrue(result.getAll().containsAll(Arrays.asList(
                    g1.getWikittyId(), g3.getWikittyId())));
        }

        { // test join
            WikittyQuery q = new WikittyQueryMaker()
                    .or()
                      .ideq(g2.getWikittyId())
                      .containsOne(WikittyGroup.FQ_FIELD_WIKITTYGROUP_MEMBERS)
                        .select(Element.ID)
                          .eq(WikittyLabel.FQ_FIELD_WIKITTYLABEL_LABELS, "titi")
                    .end();
            WikittyQueryResult<String> result = se.findAllByQuery(null, q).convertMapToSimple();
            // g1, g2, g3 doivent etre retrouve (g2 via ideq, et g1, g3 via le join)
            Assert.assertEquals(3, result.size());
            Assert.assertTrue(result.getAll().containsAll(Arrays.asList(
                    g1.getWikittyId(), g2.getWikittyId(), g3.getWikittyId())));
        }

        { // test join
            WikittyQuery q = new WikittyQueryMaker()
                    .and()
                      .not()
                        .ideq(g1.getWikittyId())
                      .containsOne(WikittyGroup.FQ_FIELD_WIKITTYGROUP_MEMBERS)
                        .select(Element.ID)
                          .eq(WikittyLabel.FQ_FIELD_WIKITTYLABEL_LABELS, "titi")
                    .end();
            WikittyQueryResult<String> result = se.findAllByQuery(null, q).convertMapToSimple();
            // g3 doit etre retrouve via le join, et g1 est exclue via la not(ideq)
            Assert.assertEquals(1, result.size());
            Assert.assertTrue(result.getAll().containsAll(Arrays.asList(
                    g3.getWikittyId())));
        }

        { // select (et donc les facets)
            WikittyQuery q = new WikittyQueryMaker()
                    .select(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME)
                    .eq(WikittyGroup.FQ_FIELD_WIKITTYGROUP_MEMBERS, l1)
                    .end();
            WikittyQueryResult<String> result = se.findAllByQuery(null, q).convertMapToSimple();
            System.out.println("dd:" + result.getAll());
            // g3 doit etre retrouve via le join, et g1 est exclue via la not(ideq)
            Assert.assertEquals(2, result.size());
            Assert.assertTrue(result.getAll().containsAll(Arrays.asList(
                    g1.getName(), g3.getName())));
        }

        // TODO poussin 20111228 do more test to test all possible condition

    }

    @Test
    public void findAllChildrenCount() throws Exception {
        // don't test it not yet implemented :(
    }

}

