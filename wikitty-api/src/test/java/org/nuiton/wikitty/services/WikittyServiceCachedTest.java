/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.WikittyClientTest;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.entities.BusinessEntity;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyUser;
import org.nuiton.wikitty.entities.WikittyUserImpl;
import org.nuiton.wikitty.test.Product;

/**
 * Re passe les tests du wikitty client en ajoutant une couche de de cache dans
 * les services.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceCachedTest extends WikittyClientTest {

    /**
     * Override to method in sub tests to change wikitty client implementation.
     * 
     * @return wikitty client implementation to use in current test case instance
     */
    @Override
    protected WikittyClient getWikittyClient() {
        wikittyConfig.setOption(WikittyConfigOption
                .JCS_DEFAULT_CACHEATTRIBUTES_MAXOBJECTS.getKey(), "10");

        WikittyService wikittyService = new WikittyServiceInMemory(wikittyConfig);
        wikittyService = new WikittyServiceCached(wikittyConfig, wikittyService, new WikittyCacheJCS(wikittyConfig));
        WikittyClient client = new WikittyClient(wikittyConfig, wikittyService);
        return client;
    }

    /**
     * Test 
     * @throws Exception
     */
    @Test
    public void testCache() throws Exception {

        List<BusinessEntity> toStore = new ArrayList<BusinessEntity>();
        List<String> toRestore = new ArrayList<String>();

        WikittyUser u1 = new WikittyUserImpl();
        WikittyUser u2 = new WikittyUserImpl();
        WikittyUser u3 = new WikittyUserImpl();
        WikittyUser u4 = new WikittyUserImpl();
        WikittyUser u5 = new WikittyUserImpl();

        toStore.add(u1);
        toStore.add(u2);
        toStore.add(u3);
        toStore.add(u4);
        toStore.add(u5);

        toStore = wikittyClient.store(toStore);

        u1 = wikittyClient.restore(WikittyUser.class, u1.getWikittyId());
        u2 = wikittyClient.restore(WikittyUser.class, u2.getWikittyId());
        u3 = wikittyClient.restore(WikittyUser.class, u3.getWikittyId());
        u4 = wikittyClient.restore(WikittyUser.class, u4.getWikittyId());
        u5 = wikittyClient.restore(WikittyUser.class, u5.getWikittyId());
        u1 = wikittyClient.restore(WikittyUser.class, u1.getWikittyId());
        u2 = wikittyClient.restore(WikittyUser.class, u2.getWikittyId());
        u3 = wikittyClient.restore(WikittyUser.class, u3.getWikittyId());

        toRestore.add(u1.getWikittyId());
        toRestore.add(u2.getWikittyId());
        toRestore.add(u3.getWikittyId());
        toRestore.add(u4.getWikittyId());
        toRestore.add(u5.getWikittyId());

        wikittyClient.restore(WikittyUser.class, toRestore);
    }

    /**
     * Setting a field value doesn't corrupt cache.
     * 
     * @throws IOException 
     */
    @Test
    public void testCacheRestore() throws IOException {
        importBooks(); // to get a known id
        Product book = wikittyClient.restore(Product.class, "4d221e31-ff9b-44f0-8545-f9884435f30d");
        Assert.assertNotNull(book);

        // we set the value of a field
        book.setName("My new name");

        // now let's suppose, the user cancel its modification
        // we don't have call store()
        book = wikittyClient.restore(Product.class, "4d221e31-ff9b-44f0-8545-f9884435f30d");

        // the remaining wikitty should hold old value
        Assert.assertEquals("Harry Potter à l'école des sorciers", book.getName());
    }

    /**
     * Same as testCacheRestore() using methods that restore multiple ids.
     * 
     * @throws IOException 
     */
    @Test
    public void testCacheRestoreMultipleIds() throws IOException {
        importBooks(); // to get a known id
        // now, let's do the same test, just by using others restore() available
        List<String> idsToRestore = new ArrayList<String>();
        idsToRestore.add("4d221e31-ff9b-44f0-8545-f9884435f30d");

        List<Product> otherWikitties = wikittyClient.restore(Product.class, idsToRestore);
        Product book = otherWikitties.get(0);

        // we set the value of a field
        book.setName("My new name");

        // now let's suppose, the user cancel its modification
        // we don't have call store()
        otherWikitties = wikittyClient.restore(Product.class, idsToRestore);
        book = otherWikitties.get(0);

        // the remaining wikitty should hold old value
        Assert.assertEquals("Harry Potter à l'école des sorciers", book.getName());
    }
    
    /**
     * Test que deux restore consecutif retourne la même instance (cache).
     */
    @Test
    public void testRestoreNoCopyPolicy() {
        // restoring two times the same wikitty should produces two different copies
        Wikitty anotherWikitty = wikittyClient.restore("4d221e31-ff9b-44f0-8545-f9884435f30d");
        Wikitty yetAnotherWikitty = wikittyClient.restore("4d221e31-ff9b-44f0-8545-f9884435f30d");

        Assert.assertSame(anotherWikitty, yetAnotherWikitty); // same reference
    }
}
