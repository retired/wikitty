/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.WikittyClientTest;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.entities.Wikitty;

/**
 * Same test as {@link WikittyServiceCachedTest} but with different cache policy.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Ignore
public class WikittyServiceCachedCopyTest extends WikittyClientTest {

    /**
     * Override to method in sub tests to change wikitty client implementation.
     * 
     * @return wikitty client implementation to use in current test case instance
     */
    @Override
    protected WikittyClient getWikittyClient() {
        wikittyConfig.setOption(
                WikittyConfigOption.WIKITTY_CACHE_RESTORE_COPIES.getKey(),
                "true");

        WikittyService wikittyService = new WikittyServiceInMemory(wikittyConfig);
        wikittyService = new WikittyServiceCached(wikittyConfig, wikittyService, new WikittyCacheJCS(wikittyConfig));
        WikittyClient client = new WikittyClient(wikittyConfig, wikittyService);
        return client;
    }

    /**
     * This test is the same but service cache policy is changed.
     * 
     * Inverse of test {@link WikittyServiceCachedTest#testRestoreNoCopyPolicy()} test
     */
    @Test
    public void testRestoreAllwaysCopyPolicy() {
        // restoring two times the same wikitty should produces two different copies
        Wikitty anotherWikitty = wikittyClient.restore("4d221e31-ff9b-44f0-8545-f9884435f30d");
        Wikitty yetAnotherWikitty = wikittyClient.restore("4d221e31-ff9b-44f0-8545-f9884435f30d");

        Assert.assertEquals(anotherWikitty, yetAnotherWikitty);
        Assert.assertNotSame(anotherWikitty, yetAnotherWikitty); // two different objects
    }
}
