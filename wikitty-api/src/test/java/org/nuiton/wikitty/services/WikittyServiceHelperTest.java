/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyServiceFactory;
import org.nuiton.wikitty.storage.WikittyExtensionStorageInMemory;
import org.nuiton.wikitty.storage.WikittySearchEngineInMemory;
import org.nuiton.wikitty.storage.WikittyStorageInMemory;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceHelperTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceHelperTest.class);

    @Test
    public void testBuild() throws Exception {
        ApplicationConfig config = WikittyConfig.getConfig();
        config.setOption(WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS.getKey(),
                WikittyServiceInMemory.class.getName() + "," +
                WikittyServiceNotifier.class.getName() + "," +
                WikittyServiceCached.class.getName());
        WikittyService ws = WikittyServiceFactory.buildWikittyService(config);

        Assert.assertEquals(WikittyServiceCached.class, ws.getClass());
        ws = ((WikittyServiceCached)ws).getDelegate();
        Assert.assertEquals(WikittyServiceNotifier.class, ws.getClass());
        ws = ((WikittyServiceNotifier)ws).getDelegate();
        Assert.assertEquals(WikittyServiceInMemory.class, ws.getClass());
    }

    @Test
    public void testSampleConfig() throws Exception {
        {
            ApplicationConfig config = WikittyConfig.getConfig(
                    "wikitty-config-sample-inmemory.properties");
            // surcharge certain option qui ont besoin d'autre module que api
            config.setOption(WikittyConfigOption.WIKITTY_WIKITTYSERVICESTORAGE_COMPONENTS.getKey(),
                    WikittyStorageInMemory.class.getName()
                    + "," + WikittyExtensionStorageInMemory.class.getName()
                    + "," + WikittySearchEngineInMemory.class.getName());
            WikittyService ws = WikittyServiceFactory.buildWikittyService(config);

            Assert.assertEquals(WikittyServiceAuthorisation.class, ws.getClass());
            ws = ((WikittyServiceAuthorisation) ws).getDelegate();
            Assert.assertEquals(WikittyServiceAuthentication.class, ws.getClass());
            ws = ((WikittyServiceAuthentication) ws).getDelegate();
            Assert.assertEquals(WikittyServiceCached.class, ws.getClass());
            ws = ((WikittyServiceCached) ws).getDelegate();
            Assert.assertEquals(WikittyServiceNotifier.class, ws.getClass());
            ws = ((WikittyServiceNotifier) ws).getDelegate();
            Assert.assertEquals(WikittyServiceStorage.class, ws.getClass());
        }
    }

}
