/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;
import java.util.Collections;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfig;
import org.nuiton.wikitty.entities.WikittyLabel;
import org.nuiton.wikitty.entities.WikittyLabelImpl;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceTransactionTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceTransactionTest.class);

    protected ApplicationConfig getConfig() {
        ApplicationConfig result = WikittyConfig.getConfig();
        return result;
    }

    @Test
    public void testTransaction() throws Exception {
        ApplicationConfig config = getConfig();
        WikittyServiceInMemory ws = new WikittyServiceInMemory(config);
        WikittyServiceTransaction tx = new WikittyServiceTransaction(config, ws);

        // ajout d'objet hors transaction
        WikittyLabel label = new WikittyLabelImpl();
        label.addLabels("coucou");

        Wikitty labelWikitty = WikittyUtil.getWikitty(ws,null,label);

        ws.store(null, Collections.singleton(labelWikitty), false);

        // on doit le retrouver dans la transation (meme si elle a ete ouverte avant)
        Wikitty wTx = WikittyServiceEnhanced.restore(tx, null, label.getWikittyId());
        Assert.assertEquals(labelWikitty, wTx);

        // on creer un objet dans la transaction
        WikittyLabel labelTx = new WikittyLabelImpl();
        labelTx.addLabels("coucouTx");
        Wikitty labelWikittyTx = WikittyUtil.getWikitty(tx, null, labelTx);
        Assert.assertNotNull(labelWikittyTx);
        tx.store(null, Collections.singleton(labelWikittyTx), false);

        //we have to find it in the transaction
        Wikitty w = WikittyServiceEnhanced.restore(tx, null, labelTx.getWikittyId());
        Assert.assertEquals(labelWikittyTx, w);

        // on ne doit pas le retrouver hors de la transation
        w = WikittyServiceEnhanced.restore(ws, null, labelTx.getWikittyId());
        Assert.assertEquals(null, w);

        // on commit, du coup on doit retrouver l'objet
        tx.commit(null);
        w = WikittyServiceEnhanced.restore(ws, null, labelTx.getWikittyId());
        Assert.assertEquals(labelWikittyTx, w);

    }

}
