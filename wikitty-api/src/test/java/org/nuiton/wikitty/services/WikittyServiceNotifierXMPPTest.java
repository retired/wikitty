/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import java.util.Date;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyUtil;

/**
 * Wikitty service test based on XMPP transporter.
 * 
 * Test disabled, need to fix http://www.nuiton.org/issues/1060 before
 * to add embedded xmpp server available in test.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Ignore
public class WikittyServiceNotifierXMPPTest {

    @Test
    public void testXMPP() throws Exception {
        String server = "im.codelutin.com";
        String room = "test@conference.im.codelutin.com";
        ApplicationConfig config = WikittyConfig.getConfig();
        config.setOption(WikittyConfigOption.WIKITTY_EVENT_TRANSPORTER_XMPP_SERVER.getKey(), server);
        config.setOption(WikittyConfigOption.WIKITTY_EVENT_TRANSPORTER_XMPP_ROOM.getKey(), room);

        // Envoi d'un message avec le transporter normal
        WikittyServiceNotifier.RemoteNotifierTransporter transporteur =
                new XMPPNotifierTransporter(config);
        WikittyServiceNotifier wsn = new WikittyServiceNotifier(config, null, transporteur);
        WikittyEvent event = new WikittyEvent("test");
        event.addRemoveDate("theId", new Date());

        transporteur.sendMessage(event);


        // essaie de recuperation du message

        XMPPConnection connection = new XMPPConnection(server);
        connection.connect();
        connection.loginAnonymously();

        MultiUserChat muc = new MultiUserChat(connection, room);
        String pseudo = WikittyUtil.getUniqueLoginName();
        System.out.println("pseudo: " + pseudo);

        DiscussionHistory history = new DiscussionHistory();
        history.setMaxStanzas(0);
        muc.join(pseudo, "", history, 4000);

        muc.addMessageListener(new PacketListener() {

            @Override
            public void processPacket(Packet packet) {
                System.out.println("ext: " + packet.getExtensions());
                System.out.println("prop: " + packet.getPropertyNames());
                Object event = packet.getProperty(XMPPNotifierTransporter.PROPERTY_EVENT_NAME);
                System.out.println("event " + event + " PACKET: " +
                        " xml: " + packet.toXML());
            }
        });

        // Discover information about the room roomName@conference.myserver
//        RoomInfo info = MultiUserChat.getRoomInfo(connection, room);
//        System.out.println("Number of occupants:" + info.getOccupantsCount());
//        System.out.println("Room Subject:" + info.getSubject());

//        Thread t = new Thread();
//        Thread.currentThread().sleep(1000*60);
    }

}
