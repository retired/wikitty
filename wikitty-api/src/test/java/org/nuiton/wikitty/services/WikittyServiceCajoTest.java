/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import java.rmi.ConnectException;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyServiceFactory;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyImpl;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceCajoTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceCajoTest.class);

    static final protected String url = "http://localhost:1198/ws";

    protected WikittyService startServer() throws Exception {
        Properties props = new Properties();
        props.setProperty(WikittyConfigOption.WIKITTY_SERVER_URL.getKey(),
                url);
        props.setProperty(WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS.getKey(),
                WikittyServiceInMemory.class.getName() + "," +
                WikittyServiceCajoServer.class.getName());
        ApplicationConfig config = WikittyConfig.getConfig(props);
        WikittyService result = WikittyServiceFactory.buildWikittyService(config);
        return result;
    }

    protected void stopServer(WikittyService ws) {
        if (ws instanceof WikittyServiceCajoServer) {
            ((WikittyServiceCajoServer)ws).stop();
        }
    }

    protected WikittyService getClient() throws Exception {
        Properties props = new Properties();
        props.setProperty(WikittyConfigOption.WIKITTY_SERVER_URL.getKey(),
                url);
        props.setProperty(WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS.getKey(),
                WikittyServiceCajoClient.class.getName());
        ApplicationConfig config = WikittyConfig.getConfig(props);
        WikittyService result = WikittyServiceFactory.buildWikittyService(config);
        return result;
    }


    @Test
    public void testCajo() throws Exception {

        // this test always fails on jenkins
        try {
            WikittyService server = startServer();
            WikittyService client = getClient();
    
            WikittyProxy proxy = new WikittyProxy(client);
    
            Wikitty w = new WikittyImpl();
            proxy.store(w);
    
            List<Wikitty> list = server.restore(null, Collections.singletonList(w.getWikittyId()));
            Assert.assertEquals(1, list.size());
    
            stopServer(server);
        }
        catch (Exception ex) {
            if (ex.getCause() instanceof ConnectException) {
                if (log.isErrorEnabled()) {
                    log.error("Cajo test failed !!!", ex);
                }
            }
            else {
                throw ex;
            }
        }
    }

}
