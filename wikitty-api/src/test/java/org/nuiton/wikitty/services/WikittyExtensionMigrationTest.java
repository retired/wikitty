/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import java.util.Date;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.WikittyConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyExtensionMigrationTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyExtensionMigrationTest.class);

    static protected String extensionName = "Dummy";

    @Test
    public void testMigration() throws Exception {

        WikittyExtension ext1 = new WikittyExtension(extensionName,
                "1.0", // version
                WikittyUtil.tagValuesToMap(" version=\"1.0\""), // tag/values
                (List)null,
                WikittyUtil.buildFieldMapExtension( // building field map
                "Numeric number",
                "String string",
                "Date date"));

        WikittyExtension ext2 = new WikittyExtension(extensionName,
                "2.0", // version
                WikittyUtil.tagValuesToMap(" version=\"1.0\""), // tag/values
                (List<String>)null,
                WikittyUtil.buildFieldMapExtension( // building field map
                "Numeric number",
                "String string",
                "Date date"));

        ApplicationConfig config = WikittyConfig.getConfig();
        String key = WikittyConfigOption.WIKITTY_MIGRATION_CLASS.getKey() + extensionName;
        config.setOption(key, DummyMigration.class.getName());
        WikittyService service = new WikittyServiceInMemory(config);
        WikittyClient client = new WikittyClient(config, service);

        WikittyImpl w1 = new WikittyImpl("w1");
        w1.addExtension(ext1);
        w1.setField(extensionName, "string", "coucou");

        client.store(w1);

        client.storeExtension(ext2);

        Wikitty w2 = client.restore(w1.getWikittyId());
        Assert.assertEquals("hello", w2.getFieldAsString(extensionName, "string"));
    }

    public static class DummyMigration extends WikittyExtensionMigrationRename {

        @Override
        public Wikitty migrate(WikittyService service, Wikitty oldWikitty,  Wikitty newWikitty, WikittyExtension oldExt, WikittyExtension newExt) {
            System.out.println(String.format("migrate %s(%s) oldExt %s newExt %s",
                    oldWikitty.getWikittyId(), oldWikitty.getExtension(extensionName).getId(),
                    oldExt.getId(), newExt.getId()));

            Wikitty result = super.migrate(service, oldWikitty, newWikitty, oldExt, newExt);
            result.setField(extensionName, "string", "hello");
            result.setField(extensionName, "date", new Date());
            
            System.out.println(String.format("done %s(%s)",
                    result.getWikittyId(), result.getExtension(extensionName).getId()));

            return result;
        }

    }
}
