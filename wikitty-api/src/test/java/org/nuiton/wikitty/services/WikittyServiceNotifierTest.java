/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;


import java.util.EnumSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.wikitty.WikittyService.ServiceListenerType;
import org.nuiton.wikitty.services.WikittyEvent;
import org.nuiton.wikitty.services.WikittyListener;
import org.nuiton.wikitty.services.WikittyServiceNotifier;

/**
 * Test si la notification par event fonctionne bien (les bons types d'event
 * sont envoyes et recus.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceNotifierTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceNotifierTest.class);

    protected EnumSet<WikittyEvent.WikittyEventType> lastEvent = null;
    protected int nbEvent = 0;

    /**
     * Test si les events sont bien lever et bien recu
     * @throws Exception
     */
    @Test
    public void testEvent() throws Exception {
        WikittyServiceNotifier wsn = new WikittyServiceNotifier(null, null, null);
        Listener l = new Listener();

        // test d'envoi et de bonne reception
        wsn.addWikittyServiceListener(l, ServiceListenerType.ALL);
        sendEvent(wsn, true);

        // si on enleve le listener, plus aucun event ne doit arriver
        wsn.removeWikittyServiceListener(l, ServiceListenerType.ALL);
        sendEvent(wsn, false);

        // donc au total seulement 5 events on du etre envoye
        Assert.assertEquals(6, nbEvent);

    }

    /**
     * Envoi tous les events possible. Permet de tester la methode process et
     * la method fireEvent de WikittyServiceNotifier vu que la premiere
     * appelle la deuxieme
     * 
     * @param wsn
     */
    protected void sendEvent(WikittyServiceNotifier wsn, boolean hasListener) throws Exception {
        {
            WikittyEvent event = new WikittyEvent("test");
            event.addType(WikittyEvent.WikittyEventType.PUT_WIKITTY);
            wsn.processRemoteEvent(event);
            wsn.getEventThread().waitFor(event.getTime());
            if (hasListener) {
                Assert.assertEquals(
                        EnumSet.of(WikittyEvent.WikittyEventType.PUT_WIKITTY),
                        lastEvent);
            } else {
                Assert.assertEquals(null, lastEvent);
            }
            lastEvent = null;
        }
        {
            WikittyEvent event = new WikittyEvent("test");
            event.addType(WikittyEvent.WikittyEventType.REMOVE_WIKITTY);
            wsn.processRemoteEvent(event);
            wsn.getEventThread().waitFor(event.getTime());
            if (hasListener) {
            Assert.assertEquals(
                    EnumSet.of(WikittyEvent.WikittyEventType.REMOVE_WIKITTY),
                    lastEvent);
            } else {
                Assert.assertEquals(null, lastEvent);
            }
            lastEvent = null;
        }
        {
            WikittyEvent event = new WikittyEvent("test");
            event.addType(WikittyEvent.WikittyEventType.CLEAR_WIKITTY);
            wsn.processRemoteEvent(event);
            wsn.getEventThread().waitFor(event.getTime());
            if (hasListener) {
            Assert.assertEquals(
                    EnumSet.of(WikittyEvent.WikittyEventType.CLEAR_WIKITTY),
                    lastEvent);
            } else {
                Assert.assertEquals(null, lastEvent);
            }
            lastEvent = null;
        }
        {
            WikittyEvent event = new WikittyEvent("test");
            event.addType(WikittyEvent.WikittyEventType.PUT_EXTENSION);
            wsn.processRemoteEvent(event);
            wsn.getEventThread().waitFor(event.getTime());
            if (hasListener) {
                Assert.assertEquals(
                        EnumSet.of(WikittyEvent.WikittyEventType.PUT_EXTENSION),
                        lastEvent);
            } else {
                Assert.assertEquals(null, lastEvent);
            }
            lastEvent = null;
        }
        {
            WikittyEvent event = new WikittyEvent("test");
            event.addType(WikittyEvent.WikittyEventType.REMOVE_EXTENSION);
            wsn.processRemoteEvent(event);
            wsn.getEventThread().waitFor(event.getTime());
            if (hasListener) {
                Assert.assertEquals(
                        EnumSet.of(WikittyEvent.WikittyEventType.REMOVE_EXTENSION),
                        lastEvent);
            } else {
                Assert.assertEquals(null, lastEvent);
            }
            lastEvent = null;
        }
        {
            WikittyEvent event = new WikittyEvent("test");
            event.addType(WikittyEvent.WikittyEventType.CLEAR_EXTENSION);
            wsn.processRemoteEvent(event);
            wsn.getEventThread().waitFor(event.getTime());
            if (hasListener) {
            Assert.assertEquals(
                    EnumSet.of(WikittyEvent.WikittyEventType.CLEAR_EXTENSION),
                    lastEvent);
            } else {
                Assert.assertEquals(null, lastEvent);
            }
            lastEvent = null;
        }
    }

    /**
     * Class listener des events, check la bonne reception
     */
    class Listener implements WikittyListener {

        @Override
        public void putWikitty(WikittyEvent event) {
            nbEvent++;
            Assert.assertEquals(
                    EnumSet.of(WikittyEvent.WikittyEventType.PUT_WIKITTY),
                    event.getType());
            lastEvent = event.getType();
        }

        @Override
        public void removeWikitty(WikittyEvent event) {
            nbEvent++;
            Assert.assertEquals(
                    EnumSet.of(WikittyEvent.WikittyEventType.REMOVE_WIKITTY),
                    event.getType());
            lastEvent = event.getType();
        }

        @Override
        public void clearWikitty(WikittyEvent event) {
            nbEvent++;
            Assert.assertEquals(
                    EnumSet.of(WikittyEvent.WikittyEventType.CLEAR_WIKITTY),
                    event.getType());
            lastEvent = event.getType();
        }

        @Override
        public void putExtension(WikittyEvent event) {
            nbEvent++;
            Assert.assertEquals(
                    EnumSet.of(WikittyEvent.WikittyEventType.PUT_EXTENSION),
                    event.getType());
            lastEvent = event.getType();
        }

        @Override
        public void removeExtension(WikittyEvent event) {
            nbEvent++;
            Assert.assertEquals(
                    EnumSet.of(WikittyEvent.WikittyEventType.REMOVE_EXTENSION),
                    event.getType());
            lastEvent = event.getType();
        }

        @Override
        public void clearExtension(WikittyEvent event) {
            nbEvent++;
            Assert.assertEquals(
                    EnumSet.of(WikittyEvent.WikittyEventType.CLEAR_EXTENSION),
                    event.getType());
            lastEvent = event.getType();
        }

    }
}
