/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query;

import java.util.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyImpl;

/**
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyQueryTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyQueryTest.class);

    @Test
    public void testParseAliasAndTree() throws Exception {
        WikittyQueryParser parser = new WikittyQueryParser();
        System.out.println("---------------------------------------------------------------");

        WikittyQueryParser.parse("Select WikittyGroup.name Where WikittyGroup.name=Group*");
        System.out.println("***************************************************************");

        WikittyQueryParser.parse("Select WikittyGroup.name Where WikittyGroup.name=Group*");

        System.out.println("---------------------------------------------------------------");
        parser.addAlias("MyAlias\\((.*), (.*)\\)",
                "id={SELECT WikittyTreeNode.attachment"
                + " WHERE (rootNode={SELECT ID WHERE (WikittyTreeNode.name=$1)}"
                + " AND pathNode={SELECT ID WHERE (WikittyTreeNode.name=$2)})}");

        WikittyQueryParser.parse("WikittyGroup.name=Group*");
        WikittyQueryParser.parse("WikittyGroup.name=Group* AND id=toto");

        WikittyQuery q1 = parser.parseQuery(
                "WikittyGroup.name=Group* AND MyAlias(OtherTree, OtherBranch)");

        WikittyQuery q2 = parser.parseQuery(
                "WikittyGroup.name=Group* AND id={SELECT WikittyTreeNode.attachment"
                + " WHERE (rootNode={SELECT ID WHERE (WikittyTreeNode.name=OtherTree)}"
                + " AND pathNode={SELECT ID WHERE (WikittyTreeNode.name=OtherBranch)})}");

        System.out.println("Q1:" + q1);
        System.out.println("Q2:" + q2);
        Assert.assertEquals(q2, q1);
    }

    @Test
    public void testParseEachElement() throws Exception {
        Wikitty w = new WikittyImpl();

        {
            WikittyQuery query = new WikittyQueryMaker().and().rFalse().rFalse().end();
            String queryString = query.getCondition().toString();
            System.out.println("queryString:" + queryString);
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }

        {
            WikittyQuery query = new WikittyQueryMaker().bw("ext.field", 0, 10).end();
            String queryString = query.getCondition().toString();
            System.out.println("queryString:" + queryString);
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().containsAll("ext.field", new Date(), new Date()).end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().extContainsAll("ext1", "ext2").end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().eq("ext.field", w).end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().exteq("ext").end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().rFalse().end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().gt("ext.field", 13.3).end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().ge("ext.field", 20.5).end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().keyword("value").end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().lt("ext.field", new Date()).end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().le("ext.field", 30).end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().like("ext.field", "truc").end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().ne("ext.field", w).end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().isNotNull("ext.field").end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
       }
        {
            WikittyQuery query = new WikittyQueryMaker().isNull("ext.field").end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().rTrue().end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().unlike("ext.field", "titi").end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().sw("ext.field", "debut").end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
       }
        {
            WikittyQuery query = new WikittyQueryMaker().ew("ext.field", "fin").end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().notsw("ext.field", "pasdebut").end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().notew("ext.field", "pasfin").end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().not().rTrue().end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().rFalse().end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().or().rFalse().rTrue().end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().eq("ext.field", w).end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker()
                    .not()
                      .and()
                        .eq("ext.field", 1)
                        .eq("ext.field", 2)
                      .close()
                    .end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
        {
            WikittyQuery query = new WikittyQueryMaker().eq("ext.field", "truc").end();
            String queryString = query.getCondition().toString();
            WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
            String queryParsedString = queryParsed.getCondition().toString();
            System.out.println("queryString:" + queryString);
            System.out.println("queryParsed:" + queryParsedString);
            Assert.assertEquals(query, queryParsed);
            Assert.assertEquals(queryString, queryParsedString);
        }
    }

    @Test
    public void testParse() throws Exception {
        Wikitty w = new WikittyImpl();

        // creation d'une query avec toutes les conditions possibles
        // et tous les types possibles (string, boolean, Wikitty, Date)
        WikittyQueryMaker cc = new WikittyQueryMaker();
        // on ecrit pas la requete en flux pour savoir s'il y une erreur a dans
        // quelle methode elle arrive
        cc.and();
        cc.bw("ext.field", 0, 10);
        cc.containsAll("ext.field", new Date(), new Date());
        cc.extContainsAll("ext1", "ext2");
        cc.eq("ext.field", w);
        cc.exteq("ext");
        cc.rFalse();
        cc.gt("ext.field", 13.3);
        cc.ge("ext.field", 20.5);
        cc.keyword("value");
        cc.lt("ext.field", new Date());
        cc.le("ext.field", 30);
        cc.like("ext.field", "truc");
        cc.ne("ext.field", w);
        cc.isNotNull("ext.field");
        cc.isNull("ext.field");
        cc.rTrue();
        cc.unlike("ext.field", "titi");
        cc.sw("ext.field", "debut");
        cc.ew("ext.field", "fin");
        cc.notsw("ext.field", "pasdebut");
        cc.notew("ext.field", "pasfin");
        cc.not();
          cc.rFalse();
        cc.or();
          cc.eq("ext.field", w);
          cc.not()
            .and()
              .eq("ext.field", 1)
              .eq("ext.field", 2)
            .close();
          cc.eq("ext.field", "truc");
        cc.close();
        WikittyQuery query = cc.end();

        // transformation en string (test du visiteur to String)
        String queryString = query.getCondition().toString();
        System.out.println("queryString:" + queryString);

        // test de la copie de query
        WikittyQuery queryCopy = query.copy();
        System.out.println("queryCopy:" + queryCopy.getCondition());

        // parsing du query genere
        WikittyQuery queryParsed = WikittyQueryParser.parse(queryString);
        System.out.println("queryParsed:" + queryParsed.getCondition());

        String queryCopyString = queryCopy.getCondition().toString();
        System.out.println("queryCopyString:" + queryCopyString);

        // parsing du query genere a partir de la copy
        WikittyQuery queryCopyParsed = WikittyQueryParser.parse(queryCopyString);
        System.out.println("queryParsed:" + queryParsed.getCondition());

        // test d'equalite sur les 2 query
        Assert.assertEquals(query, queryCopy);

        // test d'equalite sur les representation string des 2 query
        Assert.assertEquals(queryParsed.toString(), queryCopyParsed.toString());
    }

    /**
     * Les requettes qui commencent par un espace provoque une erreur de parsing.
     */
    @Test
    public void testParseSpaceStart() {
        WikittyQueryMaker cc = new WikittyQueryMaker();
        cc.parse(" xxx");
    }
}
