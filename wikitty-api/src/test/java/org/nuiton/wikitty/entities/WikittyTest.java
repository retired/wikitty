/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.entities;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyUtil;

/**
 * Tests on wikitty object only.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class WikittyTest {

    private final static Log log  = LogFactory.getLog(WikittyTest.class);

    protected static final String EXTNAME = "wikittyExt";
    protected static final String EXTREQUIRED = "wikittyRequired";
    protected static final String EXTREQUIRES = "wikittyRequires";

    //protected static final WikittyExtension EXT_REQUIRED =
    //        createExtension(EXTREQUIRED, null);
        
    //protected static final WikittyExtension EXT_REQUIRES =
    //        createExtension(EXTREQUIRES, EXTREQUIRED);
        
    protected static final WikittyExtension EXT_TEST =
            createExtension(EXTNAME, null,
                new FieldType(WikittyTypes.STRING, 0, 1),
                new FieldType(WikittyTypes.NUMERIC, 1, 1),
                new FieldType(WikittyTypes.DATE, 1, 1)
            );

    protected static WikittyExtension createExtension(String extName, String requires,
            FieldType... types) {
        LinkedHashMap<String, FieldType> fieldsMap = new LinkedHashMap<String, FieldType>();
        for ( int i = 0; i < types.length; i++ ) {
            fieldsMap.put( "fieldName" + i,  types[i] );
        }
        return new WikittyExtension(extName, "1", null, requires, fieldsMap);
    }

    /**
     * Create a wikitty instance used in current test.
     * 
     * @return valid wikitty
     */
    protected Wikitty createBasicWikitty() {
        WikittyExtension ext = ExtensionFactory.create(EXTNAME, "1")
            .addField("name", WikittyTypes.STRING)
            .extension();
        Wikitty w = new WikittyImpl();
        w.addExtension(ext);
        w.setField(EXTNAME, "name", "foobar");
        return w;
    }

    /**
     * Wikitty id must never be null.
     */
    @Test
    public void testWikittyId() {
        Wikitty w = new WikittyImpl();
        // as soon as a wikitty object is created, it got an ID.
        Assert.assertNotNull("Wikitty should got an ID", w.getWikittyId() );
        
        Wikitty w2 = new WikittyImpl( w.getWikittyId() );
        // two wikitty with a similar id are equals.
        Assert.assertEquals("Wikitty with same ID must be equals", w, w2);
    }

    /**
     * Test sur les extensions d'un wikitty.
     */
    @Test
    public void testWikittyExtension() {
        
        Wikitty wikitty = new WikittyImpl();
        
        // null extension should be ignored
        WikittyExtension ext = new WikittyExtension( "invalidExt", "1", null );
        wikitty.addExtension(ext);
        // yes we want to handle empty extension
        Assert.assertTrue( wikitty.hasExtension("invalidExt") );
        
        // empty extension should be ignored (?)
        ext = new WikittyExtension( "invalideExt2", "1", new LinkedHashMap<String, FieldType>() );
        wikitty.addExtension(ext);
        Assert.assertFalse( wikitty.hasExtension("invalidExt2") );
        
        // test requires extension
        ext = new WikittyExtension( "master", "1", null);
        wikitty.addExtension(ext);
        Assert.assertTrue( wikitty.hasExtension("master") );

        ext = new WikittyExtension( "slave", "1", null, "master", null);
        wikitty.addExtension(ext);
        Assert.assertTrue( wikitty.hasExtension("slave") );

        ext = new WikittyExtension("badslave", "1", null, "nomaster", null);
        try {
            wikitty.addExtension(ext);
            Assert.assertFalse(true);
        } catch (Exception eee) {
            // add extension that depend another not in wikitty must throw exception
            Assert.assertTrue(true);
        }
        Assert.assertFalse( wikitty.hasExtension("badslave") );

        // test data ...
        String testExtName = "testExt";
        ext = ExtensionFactory.create(testExtName, "1")
            .addField("fieldName0", WikittyTypes.NUMERIC)
            .addField("fieldName1", WikittyTypes.STRING)
            .addField("fieldName2", WikittyTypes.NUMERIC)
            .extension();
        Wikitty w = new WikittyImpl();
        Assert.assertFalse( w.hasExtension(testExtName) );
        w.addExtension(ext);
        Assert.assertTrue( w.hasExtension(testExtName) );
        for ( int i = 0; i < 3; i++ ) {
            String fieldName = "fieldName" + i;
            Assert.assertTrue( "Field " + fieldName + " not found"
                    , w.hasField(testExtName, fieldName) );
        }
        
        // test add an other extension ...
        WikittyExtension ext2 = ExtensionFactory.create("otherExt", "1")
                .addField("fieldName0", WikittyTypes.NUMERIC)
                .addField("fieldName1", WikittyTypes.STRING)
                .addField("fieldName2", WikittyTypes.NUMERIC)
                .addField("fieldName3", WikittyTypes.NUMERIC)
                .addField("fieldName4", WikittyTypes.NUMERIC)
                .extension();

        w.addExtension(ext2);
        for ( int i = 0; i < 5; i++ ) {
            String fieldName = "fieldName" + i;
            if  ( i < 3 )  { // fields in the first implem should be kept
                Assert.assertTrue( "Field " + fieldName + " not found"
                    , w.hasField(testExtName, fieldName) );
            }
            Assert.assertTrue( "Field " + fieldName + " not found",
                    w.hasField("otherExt", fieldName) );
        }
    }
    
    @Test
    public void testFieldAssigment() throws Exception {
        Wikitty w = createBasicWikitty();
        
        // test assignment on a non-existing extension.    
        try {
            w.setField("UNKNOWN", "fieldName", "fieldValue");
            Assert.fail("Should throw a runtime exception !");
        } catch (RuntimeException e) {
            // OK !
        }
        
        // test assignment on a non-existing field.    
        try {
            w.setField(EXTNAME, "non-existing-fieldName", "fieldValue");
            Assert.fail( "Should throw a runtime exception !"  );
        } catch (RuntimeException e) {
            // OK !
        }

        w.addExtension(EXT_TEST);
        
        // basic valid assignment ...
        String stringValue = "A Test Value";
        w.setField(EXTNAME, "fieldName0", stringValue);
        Assert.assertEquals(stringValue,  w.getFieldAsString(EXTNAME, "fieldName0") );
        
        int intValue = 123456;
        w.setField(EXTNAME, "fieldName1", intValue);
        Assert.assertEquals(intValue,  w.getFieldAsInt(EXTNAME, "fieldName1") );
        
        Date dateValue = new Date();
        w.setField(EXTNAME, "fieldName2", dateValue);
        Assert.assertEquals(dateValue,  w.getFieldAsDate(EXTNAME, "fieldName2") );
        
        // null or empty assignment ...
        for ( int i = 0; i < 3; i++ ) {
            w.setField(EXTNAME, "fieldName" + i, null);
        }
    }
    
    @Test
    public void testFieldCastRules() throws Exception {
        Wikitty w = createBasicWikitty();
        w.addExtension(EXT_TEST);
        
        // casted assignment ...
        final String stringValue = "Hello";
        // test any object => string type (casted as its toString() value) 
        w.setField(EXTNAME, "fieldName0", new Object() {
            public String toString() {
                return stringValue;
            }
        });
        // test java.lang.Integer => int
        w.setField(EXTNAME, "fieldName1", Integer.valueOf(123));
        Assert.assertEquals(123,  w.getFieldAsInt(EXTNAME, "fieldName1") );
        // test String => int
        w.setField(EXTNAME, "fieldName1", "123");
        Assert.assertEquals(123,  w.getFieldAsInt(EXTNAME, "fieldName1") );
        // test String => Date
        Calendar cal = Calendar.getInstance(Locale.ROOT);
        cal.set(1982, 0, 23, 0, 0, 0); cal.set( Calendar.MILLISECOND, 0 );
        w.setField( EXTNAME, "fieldName2", WikittyUtil.formatDate(cal.getTime()) );
        Assert.assertEquals(cal.getTime(),  w.getFieldAsDate(EXTNAME, "fieldName2") );
    }
    
    @Test
    public void testFieldBoundsManagement() throws Exception {
        String fieldName = "fieldName0";
        WikittyExtension ext = ExtensionFactory.create(EXTNAME, "1")
            .addField(fieldName, WikittyTypes.NUMERIC)
                .maxOccur(FieldType.NOLIMIT)
            .extension();
        Wikitty w = new WikittyImpl();
        w.addExtension(ext);
        
        
        // test setting a (primitive) value to the field ...
        try { 
            w.setField(EXTNAME, fieldName, 0);
            // OK, to simplify auto migration, if assign number to list
            // now number is automaticaly converted to Collections.singletonList(number)
        } catch (WikittyException e) {
            Assert.fail("setting an int to a list of int is forbidden !");
        }
        
        // test adding elements to the field ...
        for ( int i = 1; i < 10; i++ ) { 
            w.addToField(EXTNAME, fieldName, i);
        }
        int z = 0;
        for ( int value : w.getFieldAsList(EXTNAME, fieldName, Integer.class) ) {
            Assert.assertEquals( "element " + z + " failed",
                    z, value );
            z++;
        }
        
        // test set a list as the field value ...
        Integer[] listInt = new Integer[] {4, 2, 8, 9, 5, -12, Integer.MIN_VALUE, Integer.MAX_VALUE };
        w.setField( EXTNAME, fieldName, Arrays.asList(listInt) );
        z = 0;
        for ( int value : w.getFieldAsList(EXTNAME, fieldName, Integer.class) ) {
            Assert.assertEquals( listInt[z++].intValue(), value );
        }
        
        // test remove an element ...
        w.removeFromField( EXTNAME, fieldName, Integer.MIN_VALUE );
        List<Integer> values = w.getFieldAsList(EXTNAME, fieldName, Integer.class);
        z = 0;
        for ( int value : w.getFieldAsList(EXTNAME, fieldName, Integer.class) ) {
            Assert.assertEquals( 
                    // on the last element, we expect MAX_VALUE because MIN_VALUE was deleted.
                    z == values.size() - 1 ? Integer.MAX_VALUE : listInt[z++].intValue()
                    , value );
        }
        
        // test clear field ...
        w.clearField(EXTNAME, fieldName);
        values = w.getFieldAsList(EXTNAME, fieldName, Integer.class);
        Assert.assertTrue( values.isEmpty() );
    }
    
    public static class PerfFieldAccesClass {
        String stringValue;
        int intValue;
        Date dateValue;

        public String getStringValue() {
            return stringValue;
        }
    
        public void setStringValue(String stringValue) {
            this.stringValue = stringValue;
        }
    
        public void setIntValue(int intValue) {
            this.intValue = intValue;
        }
    
        public int getIntValue() {
            return intValue;
        }
    
        public void setDateValue(Date dateValue) {
            this.dateValue = dateValue;
        }
    
        public Date getDateValue() {
            return dateValue;
        }
    
    }

    @Test
    public void testPerfFieldAccess() throws Exception {
        int MAX = 100000;
    
        Map<String, Object> m = new HashMap<String, Object>();
        Wikitty w = createBasicWikitty();
        w.addExtension(EXT_TEST);
        PerfFieldAccesClass z = new PerfFieldAccesClass();
    
        long time = System.currentTimeMillis();
        for (int i=0; i<MAX; i++) {
            // basic valid assignment ...
            String stringValue = "A Test Value";
            m.put(EXTNAME+".fieldName0", stringValue);
    
            int intValue = 123456;
            m.put(EXTNAME + ".fieldName1", intValue);
    
            Date dateValue = new Date();
            m.put(EXTNAME + ".fieldName2", dateValue);
        }
        long timeSetM = System.currentTimeMillis() - time;
    
        time = System.currentTimeMillis();
        String tmp = "";
        for (int i = 0; i < MAX; i++) {
            String stringValue = (String)m.get(EXTNAME + ".fieldName0");
    
            int intValue = (Integer) m.get(EXTNAME + ".fieldName1");
    
            Date dateValue = (Date)m.get(EXTNAME + ".fieldName2");
            tmp = stringValue + intValue + dateValue;
        }
        long timeGetM = System.currentTimeMillis() - time;
    
    
        time = System.currentTimeMillis();
        for (int i=0; i<MAX; i++) {
            // basic valid assignment ...
            String stringValue = "A Test Value";
            w.setField(EXTNAME, "fieldName0", stringValue);
    
            int intValue = 123456;
            w.setField(EXTNAME, "fieldName1", intValue);
    
            w.setField(EXTNAME, "fieldName2", new Date());
        }
        long timeSetW = System.currentTimeMillis() - time;
    
        time = System.currentTimeMillis();
        for (int i=0; i<MAX; i++) {
            // basic valid assignment ...
            String stringValue = w.getFieldAsString(EXTNAME, "fieldName0");
    
            int intValue = w.getFieldAsInt(EXTNAME, "fieldName1");
    
            Date dateValue = w.getFieldAsDate(EXTNAME, "fieldName2");
            tmp = stringValue + intValue + dateValue;
        }
        long timeGetW = System.currentTimeMillis() - time;
    
        time = System.currentTimeMillis();
        for (int i = 0; i < MAX; i++) {
            String stringValue = "A Test Value";
            z.setStringValue(stringValue);
    
            int intValue = 123456;
            z.setIntValue(intValue);
    
            Date dateValue = new Date();
            z.setDateValue(dateValue);
        }
        long timeSetZ = System.currentTimeMillis() - time;
    
        time = System.currentTimeMillis();
        for (int i = 0; i < MAX; i++) {
            // basic valid assignment ...
            String stringValue = z.getStringValue();
    
            int intValue = z.getIntValue();
    
            Date dateValue = z.getDateValue();
            tmp = stringValue + intValue + dateValue;
        }
        long timeGetZ = System.currentTimeMillis() - time;
    
        log.info("Time m set: " + timeSetM + " Time m get: " + timeGetM);
        log.info("Time w set: " + timeSetW + " Time w get: " + timeGetW);
        log.info("Time z set: " + timeSetZ + " Time z get: " + timeGetZ);
    }
}
