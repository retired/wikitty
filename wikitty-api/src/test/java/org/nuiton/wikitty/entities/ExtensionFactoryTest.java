/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.entities;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.wikitty.entities.ExtensionFactory;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.WikittyTypes;
import org.nuiton.wikitty.entities.WikittyExtension;

public class ExtensionFactoryTest {

    @Test
    public void testExtensionFactory() throws Exception {
        WikittyExtension ext = ExtensionFactory.create("testExtension", "1.0")
            .addField("name", WikittyTypes.STRING)
                .notNull()
            .addField("phoneNumbers", WikittyTypes.STRING)
                .minOccur(2)
                .maxOccur(10)
            .addField("matricule", WikittyTypes.NUMERIC)
                .unique()
            .addField("fired", WikittyTypes.BOOLEAN)
            .extension();

        Assert.assertEquals("testExtension", ext.getName());
        Assert.assertEquals("1.0", ext.getVersion());
        checkField( ext, "name", WikittyTypes.STRING, 0, 1, false, true);
        checkField( ext, "phoneNumbers", WikittyTypes.STRING, 2, 10, false, false);
        checkField( ext, "matricule", WikittyTypes.NUMERIC, 0, 1, true, false);
        checkField( ext, "fired", WikittyTypes.BOOLEAN, 0, 1, false, false);
    }

    protected void checkField( WikittyExtension ext, String fieldName, WikittyTypes type,
            int min, int max, boolean unique, boolean notNull) {
        FieldType fieldType = ext.getFieldType(fieldName);
        String str = "Checking " + ext.getName() + "." + fieldName + " field...";
        Assert.assertNotNull(str, fieldType );
        Assert.assertEquals(str, type, fieldType.getType());
        Assert.assertEquals(str, min, fieldType.getLowerBound());
        Assert.assertEquals(str, max, fieldType.getUpperBound());
        Assert.assertEquals(str, unique, fieldType.isUnique());
        Assert.assertEquals(str, notNull, fieldType.isNotNull());
    }
}
