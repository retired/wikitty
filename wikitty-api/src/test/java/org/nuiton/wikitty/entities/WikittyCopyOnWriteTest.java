/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.entities;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyCopyOnWriteTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyCopyOnWriteTest.class);

    @Test
    public void testReplace() throws Exception {
        WikittyLabelImpl label = new WikittyLabelImpl();
        label.addLabels("toto");

        Wikitty w = label.getWikitty();
        WikittyCopyOnWrite copy = new WikittyCopyOnWrite(w);
        WikittyCopyOnWrite copy2 = new WikittyCopyOnWrite(w);

        WikittyLabelHelper.addLabels(copy, "titi");

        // test de replaceWith d'un WImpl vers un WCopyOnWrite
        {
            Assert.assertFalse(WikittyLabelHelper.getLabels(w).equals(WikittyLabelHelper.getLabels(copy)));
            Assert.assertEquals(w.getExtensionNames(), Collections.singleton("WikittyLabel"));
            Assert.assertEquals(copy.getExtensionNames(), Collections.singleton("WikittyLabel"));
            w.replaceWith(copy);
            Assert.assertEquals(WikittyLabelHelper.getLabels(w), WikittyLabelHelper.getLabels(copy));
            Assert.assertEquals(w.getExtensionNames(), Collections.singleton("WikittyLabel"));
            Assert.assertEquals(copy.getExtensionNames(), Collections.singleton("WikittyLabel"));
        }

        // test de replaceWith d'un WCopyOnWrite vers un WImpl
        {
            WikittyLabelHelper.addLabels(w, "tyty");
            Assert.assertFalse(WikittyLabelHelper.getLabels(w).equals(WikittyLabelHelper.getLabels(copy)));
            Assert.assertEquals(w.getExtensionNames(), Collections.singleton("WikittyLabel"));
            Assert.assertEquals(copy.getExtensionNames(), Collections.singleton("WikittyLabel"));
            copy.replaceWith(w);
            Assert.assertEquals(WikittyLabelHelper.getLabels(w), WikittyLabelHelper.getLabels(copy));
            Assert.assertEquals(w.getExtensionNames(), Collections.singleton("WikittyLabel"));
            Assert.assertEquals(copy.getExtensionNames(), Collections.singleton("WikittyLabel"));
        }

        // test de replaceWith d'un WCopyOnWrite vers un WCopyOnWrite
        {
            WikittyLabelHelper.addLabels(copy2, "lala");
            Assert.assertFalse(WikittyLabelHelper.getLabels(copy).equals(WikittyLabelHelper.getLabels(copy2)));
            Assert.assertEquals(copy.getExtensionNames(), Collections.singleton("WikittyLabel"));
            Assert.assertEquals(copy2.getExtensionNames(), Collections.singleton("WikittyLabel"));
            copy2.replaceWith(copy);
            Assert.assertEquals(WikittyLabelHelper.getLabels(copy), WikittyLabelHelper.getLabels(copy2));
            Assert.assertEquals(copy.getExtensionNames(), Collections.singleton("WikittyLabel"));
            Assert.assertEquals(copy2.getExtensionNames(), Collections.singleton("WikittyLabel"));
        }

    }
}
