/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.entities;


import org.junit.Assert;
import org.junit.Test;

/**
 * Test sur les wikitty extensions.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyExtensionTest {

    @Test
    public void extractExtensionName() throws Exception {
        String extName = "MonExtension";
        String fieldName = "monField";
        String fq = extName + "." + fieldName;

        String extNameToTest = WikittyExtension.extractExtensionName(fq);
        Assert.assertEquals(extName, extNameToTest);
    }

    @Test
    public void extractFieldName() throws Exception {
        String extName = "MonExtension";
        String fieldName = "monField";
        String fq = extName + "." + fieldName;

        String fieldNameToTest = WikittyExtension.extractFieldName(fq);
        Assert.assertEquals(fieldName, fieldNameToTest);
    }

    /**
     * Test qu'une extension ne peut pas avoir de caracteres non alphanumeriques.
     */
    @Test(expected=IllegalArgumentException.class)
    public void testWikittyExtensionBadName() {
        new WikittyExtension("invalid name", "1", null );
    }

    /**
     * Test qu'une extension ne peut pas avoir de caracteres non alphanumeriques.
     */
    @Test(expected=IllegalArgumentException.class)
    public void testWikittyExtensionBadName2() {
        WikittyExtension ext = new WikittyExtension();
        ext.setName("été");
    }

    /**
     * Test qu'une extension ne peut pas avoir de caracteres non alphanumeriques.
     */
    @Test(expected=IllegalArgumentException.class)
    public void testWikittyExtensionBadName3() {
        new WikittyExtension("test,test");
    }
}
