/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.entities;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test on generated {@link WikittyTreeNode} class.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class WikittyTreeNodeTest {

    /**
     * Test que les entités générées sont dans les types attendus.
     */
    @Test
    public void testGeneratedExtensionType() {
        WikittyExtension wikittyExtension = WikittyTreeNodeAbstract.extensionWikittyTreeNode;
        Assert.assertEquals(WikittyTypes.WIKITTY, wikittyExtension.getFieldType(WikittyTreeNode.FIELD_WIKITTYTREENODE_PARENT).getType());
        Assert.assertEquals(WikittyTypes.STRING, wikittyExtension.getFieldType(WikittyTreeNode.FIELD_WIKITTYTREENODE_NAME).getType());
        Assert.assertEquals(WikittyTypes.WIKITTY, wikittyExtension.getFieldType(WikittyTreeNode.FIELD_WIKITTYTREENODE_ATTACHMENT).getType());
    }
}
