/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.entities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class BusinessEntityImplTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(BusinessEntityImplTest.class);

    @Test
    public void testCopyFrom() throws Exception {
        WikittyLabel source = new WikittyLabelImpl();
        source.addLabels("1erLabel");
        source.addLabels("2emeLabel");

        WikittyLabel dest = new WikittyLabelImpl();

        dest.copyFrom(source);
        Assert.assertFalse(source.getWikittyId().equals(dest.getWikittyId()));
        Assert.assertEquals(source.getWikittyVersion(), dest.getWikittyVersion());
        Assert.assertEquals(source.getLabels(), dest.getLabels());
    }
    
}
