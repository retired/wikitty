/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.test.entities4;

import org.junit.Test;

/**
 * Test the generation of the model.
 *
 * FIXME sletellier 23/12/10 : Test in fail (http://www.nuiton.org/issues/show/1161)
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 3.0
 */
public class TestEntities {

    @Test
    public void testUser() {
        User u = new UserImpl();

        u.getLastName();
        u.setLastName("lastName");
    }

    @Test
    public void testUser2() {
        User2 u = new User2Impl();

        // wrong code, renamed to "toto"
        // lastName do not exists anymore
        //u.getLastName();
        //u.setLastName("lastName");

        u.getToto();
        u.setToto("toto");
    }

    @Test
    public void testAgent() {
        Agent a = new AgentImpl();

        a.getLastName();
        a.setLastName("lastName");

        a.getToto();
        a.setToto("toto");
    }
}
