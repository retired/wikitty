.. -
.. * #%L
.. * Wikitty :: api
.. * %%
.. * Copyright (C) 2009 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

====================
Le modèle de données
====================

..image:: images/wikitty-diagClass.png

WikittyHook
===========

Cet objet sert à ajouter des fonctionnalités au framework de Wikitty. Pour
cela il suffit d'ajouter un nouvelle objet ayant l'extension WikittyHook
et d'indiquer sur quelle action aura lieu le lancement de ce hook. Pour
chaque action qui a lieu sur le WikittyService les WikittyHook sont
recherché et leur variable Hook est exécuté en fonction du mimetype associé.
Le script du hook a automatiquement accès au variable:

- actionName: nom de l'action
- hook: l'objet wikitty du hook
- ws: WikittyService sous jacent du WikittyServiceHook
- aux arguments de l'action appelé (variable du même nom que l'argument de
  l'action). Si les arguments sont modifiés par le hook (pre-), les nouvelles
  valeurs seront utilisées lors de l'appel de l'action
- event: l'objet WikittyEvent resultat de l'action (null s'il n'est pas disponible)

Valeur possible pour actionToHook et s'il est possible d'indiquer des extensions:

- (pre|post)-store
- (pre|post)-storeExtension
- (pre|post)-delete
- (pre|post)-deleteExtension
- (pre|post)-deleteTree
- (pre|post)-clear
- (pre|post)-login
- (pre|post)-logout
- (pre|post)-replay
- (pre|post)-syncSearchEngine

Quelques exemple d'utilisation:
- recalculer un champs d'un objet avant sont enregistrement
- ne pas authoriser la modification d'un objet, en le supprimant de la liste
  des objets a storer
- créer de nouveaux objets lorsque certain sont sauvegardé
- ...
