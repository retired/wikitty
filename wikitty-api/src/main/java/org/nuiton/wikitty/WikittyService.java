/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty;

import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.services.WikittyEvent;
import org.nuiton.wikitty.services.WikittyListener;
import org.nuiton.wikitty.services.WikittyServiceCached;
import org.nuiton.wikitty.services.WikittyServiceNotifier;
import org.nuiton.wikitty.services.WikittyServiceStorage;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.query.WikittyQueryResultTreeNode;
import org.nuiton.wikitty.search.TreeNodeResult;
import org.nuiton.wikitty.services.WikittyServiceAuthentication;
import org.nuiton.wikitty.services.WikittyServiceAuthorisation;

/**
 * Wikitty service.
 *
 * The main implementation for this interface is {@link WikittyServiceStorage}.
 * It can be used alone but this implementation doesn't deal with all 
 * stuffs described in this interface. Thus, other functionalities are added
 * to the implementation through objects that decorate WikittyServiceStorage :
 *
 * <dl>
 *   <dt>{@link WikittyServiceCached}</dt>
 *     <dd>add a cache for wikitties</dd>
 *   <dt>{@link WikittyServiceAuthentication}</dt>
 *     <dd>add user authentication support</dd>
 *   <dt>{@link WikittyServiceAuthorisation}</dt>
 *     <dd>add right management</dd>
 *   <dt>{@link WikittyServiceNotifier}</dt>
 *     <dd>add notifications between client of the same wikitty service</dd>
 * </dl>
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface WikittyService {

    /*
     * listeners 
     */
    
    /** Event listener type. */
    public enum ServiceListenerType {
        ALL, LOCAL, REMOTE
    }

    /**
     * Add new wikitty service listener.
     * 
     * Warning, {@code listener} is referenced as WeakReference, but sure to
     * another reference to work.
     * 
     * @param listener listener to add
     * @param type type of event to listen
     * 
     * @see ServiceListenerType
     */
    public void addWikittyServiceListener(WikittyListener listener, ServiceListenerType type);
    
    /**
     * Remove wikitty service listener.
     * 
     * Warning, {@code listener} is referenced as WeakReference, but sure to
     * another reference to work.
     * 
     * @param listener listener to remove
     * @param type type of event to listen
     * 
     * @see ServiceListenerType
     */
    public void removeWikittyServiceListener(WikittyListener listener, ServiceListenerType type);

    /*
     * security
     */

    /**
     * Authenticate someone on WikittyService. securityToken returned must be
     * used to call others methods
     *
     * @param login can be application specifique login, but best practice is
     * to use email user
     * @param password
     * @return return token securityToken
     */
    public String login(String login, String password);

    /**
     * Unanthenticate someone by disabled securityToken
     * 
     * @param securityToken security token previously returned by login. If
     * securityToken is not valid, this method do nothing
     */
    public void logout(String securityToken);

    /**
     * Recherche si l'utilisateur n'a pas deja un token, et dans ce cas on
     * retourne le meme token. Sinon on en cree un nouveau
     *
     * @param userId l'utilisateur pour l'equel il faut recherche/creer le token
     * @return le token de l'utilisateur
     */
    public String getToken(String userId);

    /*
     * Storage
     */

    /**
     * Use with caution : It will delete ALL data !
     * This operation should be disabled in production environment.
     * 
     * @param securityToken security token
     */
    public WikittyEvent clear(String securityToken);

    /**
     * Verifie si l'utilisateur lie au securityToken a le droit d'ecrire
     * le Wikitty passe en argument.
     *
     * On ne peut pas passer seulement l'id du wikitty en parametre car de
     * nouvelles extensions ont peut lui etre ajouter depuis la derniere
     * sauvegarde
     *
     * @param securityToken le token de securite qui permet de retrouver
     * l'utilisateur et ainsi verifier les droits
     * @param wikitty le wikitty a sauver
     * @return vrai si l'utilisateur peut sauver l'objet
     */
    public boolean canWrite(String securityToken, Wikitty wikitty);

    /**
     * Verifie que l'utilisateur associe au securityToken peut supprimer le
     * wikitty dont on passe l'identifiant. Seul le propriétaire de l'objet
     * ou un admin peut supprimer un objet.
     *
     * Si l'id de l'objet est invalide, la methode retourne true, car la
     * suppression d'un id invalide ne fait rien
     *
     * @param securityToken security token
     * @param wikittyId wikitty id
     * @return vrai le la suppression ne posera pas de probleme.
     */
    public boolean canDelete(String securityToken, String wikittyId);

    /**
     * Un utilisateur peu lire un objet, s'il est Reader ou a defaut:
     * - owner
     * - AppAdmin
     * - Admin
     * - Writer
     *
     * @param securityToken security token
     * @param wikittyId wikitty id
     *
     * @return vrai si l'utilisateur peut lire l'obbjet. Si l'id est invalide
     * alors on retourne false (l'utilisateur ne peut pas lire un objet qui
     * n'existe pas)
     */
    public boolean canRead(String securityToken, String wikittyId);

    /**
     *  true if wikitty with id exists, even wikitty is deleted
     */
    public boolean exists(String securityToken, String wikittyId);

    /**
     * true if wikitty is deleted, throw an exception if id don't exist
     * @param securityToken
     * @param wikittyId
     * @return
     */
    public boolean isDeleted(String securityToken, String wikittyId);

    /**
     * Replay all events in argument on this WikittyService
     * 
     * @param securityToken security token
     * @param events event to replay
     * @param force for to not change wikitty version (use version in wikitty
     * present in event)
     * @return new event that represent all event passed in argument.
     * if arguement have: store, store, delete, clear, store. Return event
     * resume all by only one clear + store, because all action before clear is
     * not necessary. Similarly for store + delete for the same object.
     * (note: perhaps this broke history, when history are implanted and
     * two serveur must have same history ?)
     */
    public WikittyEvent replay(
            String securityToken, List<WikittyEvent> events, boolean force);

    /**
     * Manage Update and creation.
     *
     * @param securityToken security token
     * @param wikitties list of wikitty to be persisted
     * @param force boolean force non version version increment on saved wikitty
     *              or force version on wikitty creation (version 0.0)
     * @return update response
     */
    public WikittyEvent store(
            String securityToken, Collection<Wikitty> wikitties, boolean force);

    /**
     * Return all extension id (ex: "extName[version])").
     * 
     * @param securityToken security token
     * @return extension ids list
     */
    public List<String> getAllExtensionIds(String securityToken);
    
    /**
     * Return all extension id (ex: "extName[version])") where extensionName is
     * required.
     * 
     * @param securityToken security token
     * @param extensionName extension name
     * @return extension id list
     */
    public List<String> getAllExtensionsRequires(String securityToken, String extensionName);

    /**
     * Manage Update and creation
     *
     * @param securityToken security token
     * @param exts list of wikitty extension to be persisted
     * @return update response
     */
    public WikittyEvent storeExtension(
            String securityToken, Collection<WikittyExtension> exts);

   /**
     * Delete all extension if id exists and no wikitty used this extension.
     * extension name must be just the name (extName)
     *
     * @param securityToken security token
     * @param extNames extension's names to remove
     */
    public WikittyEvent deleteExtension(
            String securityToken, Collection<String> extNames);

    /**
     * Load extension from id. Id is 'name[version]'.
     *
     * @param securityToken security token
     * @param extensionId
     * @return the corresponding object, exception if no such object found.
     */
    public WikittyExtension restoreExtension(
            String securityToken, String extensionId);

    /**
     * Search extension with name in last version.
     * 
     * @param securityToken security token
     * @param name extension name
     * @return the corresponding object, exception if no such object found.
     */
    public WikittyExtension restoreExtensionLastVersion(
            String securityToken, String name);

    /**
     * Search extension with name in last version.
     *
     * @param extensionNames extension name
     * @return extension wanted with dependencies extensions at head of list
     */
    public List<WikittyExtension> restoreExtensionAndDependenciesLastVesion(
            String securityToken, Collection<String> extensionNames);
    
    /**
     * Restore wikitty
     * 
     * @param securityToken security token
     * @param id list of wikitty ids to restore
     * @return list of corresponding wikitty, if one id is not valid (no object
     * or deleted or no authorisation) this id return null and result list can
     * have null elements
     */
   public List<Wikitty> restore(String securityToken, List<String> id);

    /**
     * Delete all object if id exists.
     * 
     * @param securityToken security token
     * @param ids object's ids to remove
     */
    public WikittyEvent delete(String securityToken, Collection<String> ids);

    /**
     * Looking for Wikitty that match criteria. More than one criteria can be
     * passed in parametre, for each criteria in parametre there is a PagedResult
     * associated with the same index.
     *
     * @param securityToken security token
     * @param criteria
     * @return
     * @deprecated since 3.3 use {@link #findAllByQuery(java.lang.String, java.util.List)}
     */
    @Deprecated
    public List<PagedResult<String>> findAllByCriteria(
            String securityToken, List<Criteria> criteria);

    /**
     * Looking for Wikitty that match criteria. More than one criteria can be
     * passed in parametre, for each query in parametre there is a WikittyQueryResult
     * associated with the same index.
     *
     * @param securityToken security token
     * @param queries
     * @return
     * @since 3.3
     */
    public List<WikittyQueryResult<Map<String, Object>>> findAllByQuery(
            String securityToken, List<WikittyQuery> queries);

    /**
     * First lonely (or first one) wikitty object that match criteria, if no
     * wikitty found or first retrived is not authorized for the user return
     * null. for each criteria in parametre there is a result
     * associated with the same index.
     *
     * @param securityToken security token
     * @param criteria
     * @return wikitty id object or null
     * @deprecated since 3.3 use {@link #findByQuery(java.lang.String, java.util.List) }
     */
    @Deprecated
    public List<String> findByCriteria(String securityToken, List<Criteria> criteria);

    /**
     * First lonely (or first one) wikitty object that match query, if no
     * wikitty found or first retrived is not authorized for the user return
     * null. for each query in parametre there is a result
     * associated with the same index.
     *
     * @param securityToken security token
     * @param queries
     * @return wikitty id object or null
     * @since 3.3
     * @deprecated since 3.10 use {@link #findAllByQuery(java.lang.String, java.util.List)}
     * with singleton list or encapsulate WikittyService in {@link WikittyClient} to use findBy method
     */
    @Deprecated
    public List<Map<String, Object>> findByQuery(String securityToken, List<WikittyQuery> queries);

    /*
     * Classification
     * Most of classification purpose is handle by extension mechanisms
     */

    /**
     * Delete specified tree node and all sub nodes.
     * 
     * @param securityToken security token
     * @param treeNodeId tree node id to delete
     * @return delete wikitty ids
     */
    public WikittyEvent deleteTree(String securityToken, String treeNodeId);

    /**
     * Retrieve all node from wikittyId, this node is returned too.
     * Returned wikitty must include the 'WikittyTreeNode' extension.
     *
     * depth ask the recursively level:
     * <ul>
     * <li> 0 return only wikittyId passed in argument</li>
     * <li> 1 return wikittyId passed in argument, and his children</li>
     * <li> ...</li>
     * <li> negative value return all node</li>
     * </ul>
     *
     * if count is true, integer in return map is number of attachment in subtree
     * (recursively). If filter is not null only attachments that satisfy filter
     * are counted.
     * if count is false, all integer are fixed to 0
     *
     * @param securityToken security token
     * @param wikittyId root node to begin
     * @param depth depth of node returned
     * @param count if true return count of attachment
     * @param filter filter on attachment count
     * @return
     * @since 3.1
     * @deprecated since 3.3 use {@link #findTreeNode(java.lang.String, java.lang.String, int, boolean, org.nuiton.wikitty.query.WikittyQuery) }
     */
    @Deprecated
    public TreeNodeResult<String> findTreeNode(
            String securityToken, String wikittyId,
            int depth, boolean count, Criteria filter);

    /**
     * Retrieve all node from wikittyId, this node is returned too.
     * Returned wikitty must include the 'WikittyTreeNode' extension.
     *
     * depth ask the recursively level:
     * <ul>
     * <li> 0 return only wikittyId passed in argument</li>
     * <li> 1 return wikittyId passed in argument, and his children</li>
     * <li> ...</li>
     * <li> negative value return all node</li>
     * </ul>
     *
     * if count is true, integer in return map is number of attachment in subtree
     * (recursively). If filter is not null only attachments that satisfy filter
     * are counted.
     * if count is false, all integer are fixed to 0
     *
     * @param securityToken security token
     * @param wikittyId root node to begin
     * @param depth depth of node returned
     * @param count if true return count of attachment
     * @param filter filter on attachment count
     * @return
     * @since 3.3
     */
    public WikittyQueryResultTreeNode<String> findTreeNode(
            String securityToken, String wikittyId,
            int depth, boolean count, WikittyQuery filter);


    /*
     * history
     */

    /**
     * Restore wikitty in specifique version.
     * Authorisation is checked on last version even for previous wikitty version
     * 
     * @param securityToken security token
     */
    public Wikitty restoreVersion(
            String securityToken, String wikittyId, String version);

    /*
     * admin
     */

    /**
     * Synchronise search engine with wikitty storage engine, i.e. clear and
     * reindex all wikitties.
     * 
     * @param securityToken security token
     */
    public void syncSearchEngine(String securityToken);

}
