/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.addons.importexport;

import java.io.Reader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.addons.WikittyImportExportService.FORMAT;
import org.nuiton.wikitty.services.WikittyServiceTransaction;

public class ImportTask implements Runnable {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ImportTask.class);

    WikittyClient client;
    protected FORMAT format;
    protected Reader reader;
    protected ImportExportMethod importerExporter;

    public ImportTask(WikittyClient client, FORMAT format, Reader reader) {
        super();
        this.client = client;
        this.format = format;
        this.reader = reader;
    }

    /**
     *
     * @param securityToken
     * @param config
     * @param ws
     * @param format
     * @param reader
     * @deprecated since 3.4 {@link #ImportTask(org.nuiton.wikitty.WikittyClient, org.nuiton.wikitty.addons.WikittyImportExportService.FORMAT, java.io.Reader) }
     */
    @Deprecated
    public ImportTask(String securityToken, ApplicationConfig config,
            WikittyService ws, FORMAT format, Reader reader) {
        this(new WikittyClient(config, ws, securityToken), format, reader);
    }

    public WikittyClient getClient() {
        return client;
    }

    @Override
    public void run() {
        WikittyServiceTransaction tx = new WikittyServiceTransaction(
                client.getConfig(), client.getWikittyService());
        // TODO poussin 20101029 rendre configurable le l'auto commit '1000' pour l'import
        tx.setAutoCommit(1000);
        try {
            long time = 0;
            if (log.isInfoEnabled()) {
                time = System.currentTimeMillis();
                log.info("Import in (ms)" + time);
            }
            WikittyClient txClient = new WikittyClient(
                    client.getConfig(), tx, client.getSecurityToken());
            ImportExportMethod importer = format.ieporter();
            importer.importReader(txClient, reader);

            if (log.isInfoEnabled()) {
                time = System.currentTimeMillis() - time;
                log.info("Import in (ms)" + time);
            }
            // don't forget to commit :) with true to force version
            tx.commit(client.getSecurityToken(), true);
        } catch (Exception eee) {
            tx.rollback(client.getSecurityToken());
            throw new WikittyException("Error during import task", eee);
        }
    }
}
