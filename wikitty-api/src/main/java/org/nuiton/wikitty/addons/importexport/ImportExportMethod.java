/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.addons.importexport;

import java.io.Reader;
import java.io.Writer;
import org.nuiton.wikitty.WikittyClient;

import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 * Import / export interface.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface ImportExportMethod {
    
    /**
     * Import data from reader.
     *
     * @param client client
     * @param reader reader
     *
     * @throws Exception
     */
    public void importReader(WikittyClient client, Reader reader) throws Exception;

    /**
     * Import data from reader.
     *
     * @param securityToken security token
     * @param reader reader
     * @param ws wikitty service
     *
     * @throws Exception
     * @deprecated since 3.4 use {@link #importReader(org.nuiton.wikitty.WikittyClient, java.io.Reader) }
     */
    public void importReader(
            String securityToken, Reader reader, WikittyService ws) throws Exception;

    /**
     * Export data to writer.
     *
     * @param client client
     * @param writer writer
     * @param queryResult paged result
     *
     * @throws Exception
     */
    public void exportWriter(WikittyClient client, Writer writer,
             WikittyQueryResult<Wikitty> queryResult) throws Exception;

    /**
     * Export data to writer.
     *
     * @param securityToken security token
     * @param writer writer
     * @param ws wikitty service
     * @param pagedResult paged result
     *
     * @throws Exception
     * @deprecated since 3.4 use {@link #exportWriter(org.nuiton.wikitty.WikittyClient, java.io.Writer, org.nuiton.wikitty.query.WikittyQueryResult) }
     */
    @Deprecated
    public void exportWriter(String securityToken, Writer writer,
            WikittyService ws, PagedResult<Wikitty> pagedResult) throws Exception;
}
