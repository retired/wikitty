/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.addons.importexport;

import java.io.Reader;
import java.io.Writer;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.WikittyTypes;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyUtil;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.query.FacetTopic;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.entities.Element;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ImportExportCSV implements ImportExportMethod {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ImportExportCSV.class);

    /** Wikitty id header name. */
    protected final static String FIELD_WIKITTY_ID = "Wikitty.Id";

    /** Wikitty extensions header name. */
    protected final static String FIELD_WIKITTY_EXT = "Wikitty.Ext";

    /** Pattern form link queries. */
    protected Pattern queryPattern = Pattern.compile("^((\\w+)\\.(\\w+))=(\"(.+)\"|([^\"]+))$");

    @Override
    public void importReader(WikittyClient client, Reader reader) throws Exception {

        // get index of wikitty.id field
        int wikittyIdIndex = -1;
        int wikittyExtIndex = -1;

        // read header to get extension names
        // and build two array for ext and fieldName
        CSVReader csvReader = new CSVReader(reader);
        String[] header = csvReader.readNext();
        String[] ext = new String[header.length];
        String[] fieldsName = new String[header.length];
        for (int i = 0; i < header.length ; ++i) {
            // wikitty id is technical (special management)
            if (FIELD_WIKITTY_ID.equalsIgnoreCase(header[i])) {
                wikittyIdIndex = i;
            }
            // wikitty ext is technical (special management)
            else if (FIELD_WIKITTY_EXT.equalsIgnoreCase(header[i])) {
                wikittyExtIndex = i;
            }
            else {
                ext[i] = header[i].substring(0, header[i].indexOf("."));
                fieldsName[i] = header[i].substring(header[i].indexOf(".") + 1);
            }
        }

        // create a wikitty for each next line
        String[] currentLine;
        while ((currentLine = csvReader.readNext()) != null) {

            // cas d'une ligne vide en fin de fichier
            if (currentLine.length == 1) {
                continue;
            }

            // build wikitty instance with id (if specified)
            Wikitty currentWikitty = null;
            if (wikittyIdIndex != -1) {
                String wikittyId = currentLine[wikittyIdIndex];
                if (StringUtils.isNotBlank(wikittyId)) {
                    currentWikitty = new WikittyImpl(wikittyId);
                }
            }
            if (currentWikitty == null) {
                currentWikitty = new WikittyImpl();
            }

            // add required extension (if specified)
            if (wikittyExtIndex != -1) {
                String extensionList = currentLine[wikittyExtIndex];
                if (StringUtils.isNotBlank(extensionList)) {
                    String[] extensions = extensionList.split(",");
                    for (String extension : extensions) {
                        String extName = extension.trim();
                        addMissingExtension(
                                client, currentWikitty, extName);
                    }
                }
            }

            // manage other fields
            for (int i = 0; i < header.length ; ++i) {

                // wikitty id column, already managed
                if (i == wikittyIdIndex || i == wikittyExtIndex) {
                    continue;
                }

                String extName = ext[i];
                String fieldName = fieldsName[i];
                String value =  currentLine[i];

                // case null or empty
                if (StringUtils.isNotEmpty(value)) {

                    // extension must exists on wikitty to set a field value
                    addMissingExtension(client, currentWikitty, extName);

                    // convert link values (if necessary)
                    value = convertLinkValues(client, value);

                    // add value to correct field
                    FieldType fieldType = currentWikitty.getFieldType(extName + "." + fieldName);
                    if (fieldType.isCollection()) {
                        String[] multiplesValues = StringUtil.split(value);
                        for (String multiplesValue : multiplesValues) {
                            // begin and ends with () only if fields
                            // has multiples values during import
                            if (multiplesValue.startsWith("(") && multiplesValue.endsWith(")")) {
                                multiplesValue = multiplesValue.substring(1, multiplesValue.length() - 1);
                            }

                            // remove non existant wikitty on Wikitty type field
                            if (fieldType.getType().equals(WikittyTypes.WIKITTY)) {

                                // restore works (database) but solr criteria won't
                                Wikitty wikitty = client.restore(multiplesValue);
                                if ( wikitty != null) {
                                    currentWikitty.addToField(extName, fieldName, multiplesValue);
                                }
                            }
                            else {
                                Object v = WikittyUtil.fromStringForExport(fieldType, multiplesValue);
                                currentWikitty.addToField(extName, fieldName, v);
                            }
                        }
                    } else {
                        // remove non existant wikitty on Wikitty type field
                        if (fieldType.getType().equals(WikittyTypes.WIKITTY)) {

                            // restore works (database) but solr criteria won't
                            Wikitty wikitty = client.restore(value);
                            if ( wikitty != null) {
                                currentWikitty.setField(extName, fieldName, value);
                            }
                        }
                        else {
                            Object v = WikittyUtil.fromStringForExport(fieldType, value);
                            currentWikitty.setField(extName, fieldName, v);
                        }
                    }
                }
            }

            // add it into datas
            client.getWikittyService().store(client.getSecurityToken(),
                    Collections.singleton(currentWikitty), true);
        }
    }


    @Override
    public void importReader(String securityToken, Reader reader,
            WikittyService ws) throws Exception {
        WikittyClient client = new WikittyClient(null, ws, securityToken);
        importReader(client, reader);
    }

    /**
     * Recusively add missing extension of not exist and required extension too.
     *
     * @throws WikittyException if the extension does not exist in base
     */
    protected void addMissingExtension(WikittyClient client,
            Wikitty currentWikitty, String extName) {
        // extension must exists on wikitty to set a field value
        if (!currentWikitty.hasExtension(extName)) {
            WikittyExtension extension =
                    client.restoreExtensionLastVersion(extName);

            if (extension != null) {
                List<String> requires = extension.getRequires();
                if (requires != null && !requires.isEmpty()) {
                    // add required extensions BEFORE current
                    for (String require : requires) {
                        String localRequire = require.trim();
                        addMissingExtension(client, currentWikitty, localRequire);
                    }
                }

                currentWikitty.addExtension(extension);
            } else {
                throw new WikittyException("Extension " + extName + " does not exist");
            }
        }
    }

    /**
     * Convert internal queries to search for other real wikitty ids.
     * 
     * @param value
     * @return
     */
    protected String convertLinkValues(WikittyClient client, String value) {

        String originalValue = value;
        String resultValue = "";
        String separator = "";
        boolean correctQueries = true;

        // manage multiples query comma separated
        try {
            String[] queries = StringUtil.split(value, ",");
            
            for (String queryString : queries) {
                Matcher m = queryPattern.matcher(queryString.trim());
                if (m.find()) {
                    String fqField = m.group(1);
                    String fValue = m.group(5);
                    if (fValue == null) {
                        // quoted value
                        fValue = m.group(6);
                    }
                    WikittyQuery query = new WikittyQueryMaker()
                            .eq(fqField, fValue).end();
                    Wikitty wikitty = client.findByQuery(Wikitty.class, query);
                    if (wikitty == null) {
                        correctQueries = false;
                    }
                    else {
                        resultValue += separator + wikitty.getWikittyId();
                        separator = ",";
                    }
                }
                else {
                    // global parsing fail
                    // return original value
                    correctQueries = false;
                }
            }
        }
        catch (StringIndexOutOfBoundsException eee) {
            if (log.isTraceEnabled()) {
                log.trace("Can't split field on , skipping");
            }
        }
        
        // if conversion has not been done, return original value
        if (!correctQueries || StringUtils.isEmpty(resultValue)) {
            resultValue = originalValue;
        }
        return resultValue;
    }

    @Override
    public void exportWriter(WikittyClient client, Writer writer,
            WikittyQueryResult<Wikitty> pagedResult) throws Exception {

        CSVWriter csvWriter = new CSVWriter(writer);

        // write all data into writer
        List<String> extensionHeader = new LinkedList<String>();
        extensionHeader.add(FIELD_WIKITTY_ID);
        extensionHeader.add(FIELD_WIKITTY_EXT);
        for (FacetTopic topic : pagedResult.getTopic(Element.EXTENSION)) {
            String extName = topic.getTopicName();

            WikittyExtension extension = client.restoreExtensionLastVersion(extName);
            String ext = WikittyExtension.computeName(extName);
            for (String fieldName : extension.getFieldNames()) {
                extensionHeader.add(ext + "." + fieldName);
            }
        }
        csvWriter.writeNext(extensionHeader.toArray(new String[extensionHeader.size()]));

        if (log.isDebugEnabled()) {
            log.debug("Exporting wikitty : " + pagedResult.getAll().size() + " results");
        }

        // Export wikitty data
        List<Wikitty> wikitties = pagedResult.getAll();
        for (Wikitty w : wikitties) {

            String[] wikittyField = new String[extensionHeader.size()];

            // first, add technical id
            wikittyField[extensionHeader.indexOf(FIELD_WIKITTY_ID)] = w.getWikittyId();
            // second, add technical ext list
            Collection<String> extNamesCollection = w.getExtensionNames();
            String extNames = StringUtils.join(extNamesCollection, ',');
            wikittyField[extensionHeader.indexOf(FIELD_WIKITTY_EXT)] = extNames;

            // wikitty export must be composed of all field
            // corresponding to header extensions names
            for (String fieldName : w.fieldNames()) {
                String currentField = "";

                FieldType type = w.getFieldType(fieldName);
                if (type.isCollection()) {
                    Object fqField = w.getFqField(fieldName);
                    if (fqField != null) {
                        String separator = "";
                        for (Object o : (Collection<?>) fqField) {
                            String fqFieldValue = WikittyUtil.toStringForExport(type, o);
                            currentField += separator + "(" + fqFieldValue + ")";
                            separator = ",";
                        }
                    }
                } else {
                    String fqFieldValue = WikittyUtil.toStringForExport(type, w.getFqField(fieldName));
                    currentField = fqFieldValue;
                }

                wikittyField[extensionHeader.indexOf(fieldName)] = currentField;
            }

            csvWriter.writeNext(wikittyField);
        }

        csvWriter.close();
    }

    @Override
    public void exportWriter(String securityToken, Writer writer,
            WikittyService ws, PagedResult<Wikitty> pagedResult) throws Exception {
        WikittyClient client = new WikittyClient(null, ws, securityToken);

        Map<String, List<FacetTopic>> facet = new HashMap<String, List<FacetTopic>>();
        for (Map.Entry<String, List<org.nuiton.wikitty.search.FacetTopic>> e : pagedResult.getFacets().entrySet()) {
            String facetName = e.getKey();
            List<FacetTopic> topics = new ArrayList<FacetTopic>();
            
            // since 3.4, facet extension has changed of name
            if (org.nuiton.wikitty.search.operators.Element.ELT_EXTENSION.equals(facetName)) {
                facet.put(Element.EXTENSION.getValue(), topics);
            } else {
                facet.put(facetName, topics);
            }

            for (org.nuiton.wikitty.search.FacetTopic f : e.getValue()) {
                topics.add(new FacetTopic(f.getFacetName(), f.getTopicName(), f.getCount()));
            }
        }

        WikittyQueryResult<Wikitty> queryResult = new WikittyQueryResult(
                pagedResult.getCriteriaName(), pagedResult.getFirstIndice(),
                pagedResult.getNumFound(), null, pagedResult.getQueryString(),
                pagedResult.getAll(), null, null, facet, 0, 0);
        exportWriter(client, writer, queryResult);
    }

}
