/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.addons;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyI18n;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyMetaExtensionUtil;
import org.nuiton.wikitty.WikittyProxy;

/**
 *
 * Cette classe sert a aider a la gestion multi-langue des extensions
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyI18nUtil {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyI18nUtil.class);

    /**
     * Create or load WikittyExtensionTranslation for given extension.
     * If created you must call store if you want keep it in storage
     * @param proxy
     * @param extension
     * @return
     * @deprecated since 3.4 use {@link #getI18n(org.nuiton.wikitty.WikittyClient, org.nuiton.wikitty.entities.WikittyExtension) }
     */
    @Deprecated
    static public WikittyI18n getI18n(WikittyProxy proxy, WikittyExtension extension) {
        String id = WikittyMetaExtensionUtil.generateId(
                WikittyI18n.EXT_WIKITTYI18N,
                extension.getName());

        WikittyI18n result;
        Wikitty w = proxy.restore(id);
        if (w == null) {
            w = new WikittyImpl(id);
        }
        result = new WikittyI18nImpl(w);

        return result;
    }

    /**
     * Create or load WikittyExtensionTranslation for given extension.
     * If created you must call store if you want keep it in storage
     * @param proxy
     * @param extension
     * @return
     */
    static public WikittyI18n getI18n(WikittyClient proxy, WikittyExtension extension) {
        String id = WikittyMetaExtensionUtil.generateId(
                WikittyI18n.EXT_WIKITTYI18N,
                extension.getName());

        WikittyI18n result;
        Wikitty w = proxy.restore(id);
        if (w == null) {
            w = new WikittyImpl(id);
        }
        result = new WikittyI18nImpl(w);

        return result;
    }

}
