/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.addons.importexport;

import java.io.Reader;
import java.io.Writer;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ImportExportXML implements ImportExportMethod {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ImportExportXML.class);

    @Override
    public void importReader(WikittyClient client, Reader reader) throws Exception {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance(
                System.getProperty(XmlPullParserFactory.PROPERTY_NAME), null);
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(reader);

        WikittyExtension ext = null;
        Wikitty w = null;
        String CDATA = null;

        int eventType;
        do {
            eventType = xpp.next();
            String objectVersion = null;
            if (eventType == XmlPullParser.START_DOCUMENT) {
                log.info("start XML import at " + new Date());
            } else if (eventType == XmlPullParser.START_TAG) {
                String name = xpp.getName();
                if ("extension".equals(name)) {
                    String extName = xpp.getAttributeValue(null, "name");
                    String version = xpp.getAttributeValue(null, "version");
                    String requires = xpp.getAttributeValue(null, "requires");
                    // FIXME poussin 20111120 il faut aussi avoir les tagValues de l'extension
                    ext = new WikittyExtension(extName, version, null, requires,
                            new LinkedHashMap<String, FieldType>());
                } else if ("object".equals(name)) {
                    String id = xpp.getAttributeValue(null, "id");
                    objectVersion = xpp.getAttributeValue(null, "version");
                    String extensions = xpp.getAttributeValue(null, "extensions");
                    w = new WikittyImpl(id);
                    String[] extensionList = extensions.split(",");
                    for (String extId : extensionList) {
                        String extName = WikittyExtension.computeName(extId);
                        String extVersion = WikittyExtension.computeVersion(extId);
                        extId = WikittyExtension.computeId(extName, extVersion);
                        WikittyExtension e = client.restoreExtension(extId);
                        if (e == null) {
                            throw new WikittyException("Extension not found : " + extId);
                        }
                        w.addExtension(e);
                    }
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                String name = xpp.getName();
                if ("extension".equals(name)) {
                    client.storeExtension(Collections.singleton(ext));
                    ext = null;
                } else if ("object".equals(name)) {
                    w.setWikittyVersion(objectVersion);
                    client.getWikittyService().store(client.getSecurityToken(),
                            Collections.singleton(w), true);
                    w = null;
                } else if (ext != null && "field".equals(name)) {
                    FieldType type = new FieldType();
                    String fieldName = WikittyUtil.parseField(CDATA, type);
                    ext.addField(fieldName, type);
                } else if (ext != null && "tagvalues".equals(name)) {
                    Map<String, String> tagValues = WikittyUtil.tagValuesToMap(CDATA);
                    ext.setTagValues(tagValues);
                } else if (w != null) {
                    String extensionName = WikittyExtension.extractExtensionName(name);
                    String fieldName = WikittyExtension.extractFieldName(name);
                    FieldType fieldType = w.getFieldType(name);
                    Object v = WikittyUtil.fromStringForExport(fieldType, CDATA);
                    if (fieldType.isCollection()) {
                        w.addToField(extensionName, fieldName, v);
                    } else {
                        w.setField(extensionName, fieldName, v);
                    }
                }
            } else if (eventType == XmlPullParser.TEXT) {
                CDATA = xpp.getText();
            }
        } while (eventType != XmlPullParser.END_DOCUMENT);
    }

    @Override
    public void exportWriter(WikittyClient client, Writer result,
            WikittyQueryResult<Wikitty> queryResult) throws Exception {
        // keep extension already done
        Set<String> extDone = new HashSet<String>();
        result.write("<wikitty>\n");

        List<Wikitty> wikitties = queryResult.getAll();
        for (Wikitty w : wikitties) {
            String extensionList = "";
            for (WikittyExtension ext : w.getExtensions()) {
                String id = ext.getId();
                extensionList += "," + id;
                if (!extDone.contains(id)) {
                    extDone.add(id);
                    String requires = StringUtils.join(ext.getRequires(), ",");
                    result.write("  <extension name='" + ext.getName()
                            + "' version='" + ext.getVersion()
                            + (StringUtils.isBlank(requires) ? "":"' requires='" + requires)
                            + "'>\n");
                    Map<String, String> tagValues = ext.getTagValues();
                    result.write("    <tagvalues>"
                            + WikittyUtil.tagValuesToString(tagValues)
                            + "</tagvalues>\n");
                    for (String fieldName : ext.getFieldNames()) {
                        String def = ext.getFieldType(fieldName).toDefinition(fieldName);
                        result.write("    <field>" + def + "</field>\n");
                    }
                    result.write("  </extension>\n");
                }
            }
            if (!"".equals(extensionList)) {
                // delete first ','
                extensionList = extensionList.substring(1);
            }
            result.write("  <object id='" + w.getWikittyId() + "' version='" + w.getWikittyVersion() + "' extensions='" + extensionList + "'>\n");
            for (String fieldName : w.fieldNames()) {
                FieldType type = w.getFieldType(fieldName);
                if (type.isCollection()) {
                    Object fqField = w.getFqField(fieldName);
                    if (fqField != null) {
                        for (Object o : (Collection) fqField) {
                            String fqFieldValue = WikittyUtil.toStringForExport(type, o);
                            if (fqFieldValue != null) {
                                fqFieldValue = StringEscapeUtils.escapeXml(fqFieldValue);
                                result.write("    <" + fieldName + ">" + fqFieldValue + "</" + fieldName + ">\n");
                            }
                        }
                    }
                } else {
                    String fqFieldValue = WikittyUtil.toStringForExport(type, w.getFqField(fieldName));
                    if (fqFieldValue != null) {
                        fqFieldValue = StringEscapeUtils.escapeXml(fqFieldValue);
                        result.write("    <" + fieldName + ">" + fqFieldValue + "</" + fieldName + ">\n");
                    }
                }
            }
            result.write("  </object>\n");
        }
        result.write("</wikitty>\n");
    }

    @Override
    public void importReader(String securityToken, Reader reader,
            WikittyService ws) throws Exception {
        WikittyClient client = new WikittyClient(null, ws, securityToken);
        importReader(client, reader);
    }

    @Override
    public void exportWriter(String securityToken, Writer result,
            WikittyService ws, PagedResult<Wikitty> pagedResult) throws Exception {

        WikittyClient client = new WikittyClient(null, ws, securityToken);

        WikittyQueryResult<Wikitty> queryResult = new WikittyQueryResult(
                pagedResult.getCriteriaName(), pagedResult.getFirstIndice(),
                pagedResult.getNumFound(), null, pagedResult.getQueryString(),
                pagedResult.getAll(), null, null,null, 0, 0);
        exportWriter(client, result, queryResult);

    }
}
