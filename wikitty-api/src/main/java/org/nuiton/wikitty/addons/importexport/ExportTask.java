/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.addons.importexport;

import java.io.Writer;
import java.util.Collections;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.addons.WikittyImportExportService.FORMAT;
import org.nuiton.wikitty.search.operators.Element;

/**
 * 
 * @author poussin
 * @deprecated since 3.4 use ExportQueryTask with new query api
 */
@Deprecated
public class ExportTask implements Runnable {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ImportTask.class);

    protected WikittyService ws;
    protected FORMAT format;
    protected Criteria criteria;
    protected Writer writer;
    protected String securityToken;

    public ExportTask(String securityToken,
            WikittyService ws, FORMAT format, Criteria criteria, Writer writer) {
        this.securityToken = securityToken;
        this.ws = ws;
        this.format = format;
        this.criteria = criteria;
        this.writer = writer;
    }

    @Override
    public void run() {
        try {
            
            // use a facet to get only extension used in export
            // used for CSV export
            criteria.addFacetField(Element.ELT_EXTENSION);
            
            PagedResult<String> pageResultId = ws.findAllByCriteria(
                    securityToken, Collections.singletonList(criteria)).get(0);
            long time = 0;
            if (log.isInfoEnabled()) {
                time = System.currentTimeMillis();
                log.info("Export started");
            }
            // get Wikitty from Id
            PagedResult<Wikitty> pageResult = pageResultId.cast(securityToken, ws);

            ImportExportMethod exporter = format.ieporter();
            exporter.exportWriter(securityToken, writer, ws, pageResult);
            if (log.isInfoEnabled()) {
                time = System.currentTimeMillis() - time;
                log.info("Export in (ms)" + time);
            }
        } catch (Exception eee) {
            throw new WikittyException("Error during export task", eee);
        }
    }
}
