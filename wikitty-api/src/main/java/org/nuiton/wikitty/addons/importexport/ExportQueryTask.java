/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.addons.importexport;

import java.io.Writer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.addons.WikittyImportExportService.FORMAT;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.entities.Element;

public class ExportQueryTask implements Runnable {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ImportTask.class);

    protected WikittyClient client;
    protected FORMAT format;
    protected WikittyQuery query;
    protected Writer writer;

    public ExportQueryTask(WikittyClient client, FORMAT format, WikittyQuery query, Writer writer) {
        this.client = client;
        this.format = format;
        this.query = query;
        this.writer = writer;
    }

    @Override
    public void run() {
        try {
            
            // use a facet to get only extension used in export
            // used for CSV export
            query.addFacetField(Element.EXTENSION);
            
            WikittyQueryResult<Wikitty> queryResult = client.findAllByQuery(
                    Wikitty.class, query);

            long time = 0;
            if (log.isInfoEnabled()) {
                time = System.currentTimeMillis();
                log.info("Export started");
            }

            ImportExportMethod exporter = format.ieporter();
            exporter.exportWriter(client, writer, queryResult);
            if (log.isInfoEnabled()) {
                time = System.currentTimeMillis() - time;
                log.info("Export in (ms)" + time);
            }
        } catch (Exception eee) {
            throw new WikittyException("Error during export task", eee);
        }
    }
}
