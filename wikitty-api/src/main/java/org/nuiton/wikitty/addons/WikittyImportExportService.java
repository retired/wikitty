/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.addons;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.entities.BusinessEntityImpl;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.addons.importexport.ExportQueryTask;

import org.nuiton.wikitty.addons.importexport.ExportTask;
import org.nuiton.wikitty.addons.importexport.ImportExportCSV;
import org.nuiton.wikitty.addons.importexport.ImportExportMethod;
import org.nuiton.wikitty.addons.importexport.ImportExportXML;
import org.nuiton.wikitty.addons.importexport.ImportTask;
import org.nuiton.wikitty.addons.importexport.JobState;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.search.Search;

/**
 * Import/export service.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyImportExportService {

    static public enum FORMAT {
        XML(new ImportExportXML()),
        CSV(new ImportExportCSV());

        /** ieport == Importer/Exporter */
        protected ImportExportMethod ieporter;
        FORMAT(ImportExportMethod ieporter) {
            this.ieporter = ieporter;
        }

        public ImportExportMethod ieporter() {
            return ieporter;
        }
    };

    WikittyClient client;
    /** directory path where export asynchronous file are stored */
    protected String exportDirectory = "/tmp/";
    /** url used by client to retrieve export file when job is ended */
    protected String exportPublicURL = "file:///tmp/";

    /** Executor that do import export task */
    protected ExecutorService importExportExecutor;

    /** contains all import or export task, key is job id send to client */
    protected Map<String, Future<String>> importExportTask =
            new HashMap<String, Future<String>>();


    public WikittyImportExportService(WikittyClient client) {
        this.client = client;

        ApplicationConfig config = client.getConfig();
        exportDirectory = config.getOption(
                WikittyConfigOption.WIKITTY_EXPORT_DIRECTORY.getKey());
        exportPublicURL = config.getOption(
                WikittyConfigOption.WIKITTY_EXPORT_PUBLICURL.getKey());

        int maxThread = config.getOptionAsInt(
                WikittyConfigOption.WIKITTY_EXPORT_THREADNUMBER.getKey());
        this.importExportExecutor =
                Executors.newFixedThreadPool(maxThread);
    }

    /**
     * 
     * @param config
     * @param securityToken
     * @param ws
     * @deprecated since 3.2 use {@link #WikittyImportExportService(org.nuiton.wikitty.WikittyClient) }
     */
    @Deprecated
    public WikittyImportExportService(
            ApplicationConfig config, String securityToken, WikittyService ws) {
        this(new WikittyClient(config, ws, securityToken));
    }

    public WikittyClient getClient() {
        return client;
    }

    public WikittyService getWikittyService() {
        return getClient().getWikittyService();
    }

    public String getExportDirectory() {
        return exportDirectory;
    }

    public String getExportPublicURL() {
        return exportPublicURL;
    }

    public void syncImport(FORMAT format, String s) {
        Reader reader = new StringReader(s);
        syncImport(format, reader);
    }

    public void syncImport(FORMAT format, Reader reader) {
        ImportTask task = new ImportTask(client, format, reader);
        task.run();
    }

    public void syncImportFromUri(FORMAT format, String uri) {
        try {
            URL url = new URL(uri);
            Reader reader = new InputStreamReader(url.openStream());
            ImportTask task = new ImportTask(client, format, reader);
            task.run();
        } catch (Exception eee) {
            throw new WikittyException(String.format(
                    "Can't import in format %s uri %s", format, uri), eee);
        }
    }

    public String asyncImportFromUri(FORMAT format, String uri) {
        try {
            URL url = new URL(uri);
            Reader reader = new InputStreamReader(url.openStream());
            ImportTask task = new ImportTask(client, format, reader);
            FutureTask<String> future = new FutureTask<String>(task, null);
            importExportExecutor.submit(future);

            String jobId = UUID.randomUUID().toString();
            importExportTask.put(jobId, future);
            return jobId;
        } catch (Exception eee) {
            throw new WikittyException(String.format(
                    "Can't import in format %s uri %s", format, uri), eee);
        }
    }

    /**
     * Asynchronous export by example.
     * 
     * @param format export format
     * @param e sample
     * @return job id
     */
    public String asyncExportAllByExample(FORMAT format, BusinessEntityImpl e) {
        WikittyQuery query = new WikittyQueryMaker().wikitty(e).end();
        String result = asyncExportAllByQuery(format, query);
        return result;
    }

    /**
     * Synchronous export by example.
     * 
     * @param format export format
     * @param e sample
     * @return export string
     */
    public String syncExportAllByExample(FORMAT format, BusinessEntityImpl e) {
        WikittyQuery query = new WikittyQueryMaker().wikitty(e).end();
        String result = syncExportAllByQuery(format, query);
        return result;
    }

    /**
     * Synchronous export by example.
     *
     * @param format export format
     * @param e      sample
     */
    public void syncExportAllByExample(FORMAT format, BusinessEntityImpl e,
                                         Writer writer) {
        WikittyQuery query = new WikittyQueryMaker().wikitty(e).end();
        syncExportAllByQuery(format, query, writer);
    }

    /**
     * Asynchronous export by criteria.
     * 
     * @param format export format
     * @param query criteria
     * @return export as string
     */
    public String asyncExportAllByQuery(FORMAT format, WikittyQuery query) {
        try {
            String jobId = UUID.randomUUID().toString();

            File file = new File(exportDirectory, jobId);
            file.getParentFile().mkdirs();
            String url = exportPublicURL + jobId;
            Writer result = new FileWriter(file);
            ExportQueryTask task = new ExportQueryTask(client, format, query, result);
            FutureTask<String> future = new FutureTask<String>(task, url);
            importExportExecutor.submit(future);

            importExportTask.put(jobId, future);
            return jobId;
        } catch (Exception eee) {
            throw new WikittyException(String.format(
                    "Can't export in format %s", format), eee);
        }
    }

    /**
     * Synchronous export by criteria.
     * 
     * @param format export format
     * @param query criteria
     * @return export as string
     */
    public String syncExportAllByQuery(FORMAT format, WikittyQuery query) {
        StringWriter result = new StringWriter();
        syncExportAllByQuery(format, query, result);
        return result.toString();
    }

    /**
     * Synchronous export by criteria.
     *
     * @param format   export format
     * @param query criteria
     */
    public void syncExportAllByQuery(FORMAT format, WikittyQuery query, Writer writer) {
        ExportQueryTask task = new ExportQueryTask(client, format, query, writer);
        task.run();
    }

    /**
     * Asynchronous export by criteria.
     *
     * @param format export format
     * @param criteria criteria
     * @return export as string
     * @deprecated since 3.4 user {@link #asyncExportAllByQuery(org.nuiton.wikitty.addons.WikittyImportExportService.FORMAT, org.nuiton.wikitty.query.WikittyQuery) }
     */
    @Deprecated
    public String asyncExportAllByCriteria(FORMAT format, Criteria criteria) {
        try {
            String jobId = UUID.randomUUID().toString();

            File file = new File(exportDirectory, jobId);
            file.getParentFile().mkdirs();
            String url = exportPublicURL + jobId;
            Writer result = new FileWriter(file);
            ExportTask task = new ExportTask(client.getSecurityToken(),
                    client.getWikittyService(), format, criteria, result);
            FutureTask<String> future = new FutureTask<String>(task, url);
            importExportExecutor.submit(future);

            importExportTask.put(jobId, future);
            return jobId;
        } catch (Exception eee) {
            throw new WikittyException(String.format(
                    "Can't export in format %s", format), eee);
        }
    }

    /**
     * Synchronous export by criteria.
     *
     * @param format export format
     * @param criteria criteria
     * @return export as string
     * @deprecated since 3.4 user {@link #syncExportAllByQuery(org.nuiton.wikitty.addons.WikittyImportExportService.FORMAT, org.nuiton.wikitty.query.WikittyQuery) }
     */
    @Deprecated
    public String syncExportAllByCriteria(FORMAT format, Criteria criteria) {
        StringWriter result = new StringWriter();
        syncExportAllByCriteria(format, criteria, result);
        return result.toString();
    }

    /**
     * Synchronous export by criteria.
     *
     * @param format   export format
     * @param criteria criteria
     * @deprecated since 3.4 user {@link #syncExportAllByQuery(org.nuiton.wikitty.addons.WikittyImportExportService.FORMAT, org.nuiton.wikitty.query.WikittyQuery, java.io.Writer) }
     */
    @Deprecated
    public void syncExportAllByCriteria(FORMAT format, Criteria criteria, Writer writer) {
        ExportTask task = new ExportTask(client.getSecurityToken(),
                    client.getWikittyService(), format, criteria, writer);
        task.run();
    }

    /**
     * Return job information.
     * 
     * @param jobId job id
     * @return job state
     */
    public JobState infoJob(String jobId) {
        try {
            Future<String> future = importExportTask.get(jobId);
            JobState result = new JobState();
            if (future.isDone()) {
                result.status = "done";
                result.resourceUri = future.get();
            } else if (future.isCancelled()) {
                result.status = "cancelled";
            } else {
                result.status = "inProgress";
            }
            return result;
        } catch (Exception eee) {
            throw new WikittyException(String.format(
                    "Can't retrieve job info for job %s", jobId), eee);
        }
    }

    public void cancelJob(String jobId) {
        Future<String> future = importExportTask.get(jobId);
        future.cancel(true); // true to kill process, perhaps to strong ?
    }

    public void freeJobResource(String jobId) {
        Future<String> future = importExportTask.remove(jobId);
        if (future != null) {
            File file = new File(exportDirectory, jobId);
            file.delete();
        }
    }

}
