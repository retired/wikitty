/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Poussin Benjamin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty;

import static org.nuiton.i18n.I18n.n;

import java.io.File;

import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.wikitty.services.WikittyCacheJCS;
import org.nuiton.wikitty.services.WikittyServiceAccessStat.AccessStatStorageWikitty;
import org.nuiton.wikitty.services.WikittyServiceInMemory;
import org.nuiton.wikitty.services.XMPPNotifierTransporter;

/**
 *
 * @author poussin
 */
public enum WikittyConfigOption implements ConfigOptionDef {

//    WIKITTY_CONFIG_FILE(
//    ApplicationConfig.CONFIG_FILE_NAME,
//    n("Main configuration wikitty file"),
//    "wikitty-config.properties",
//    String.class, true, true),
    WIKITTY_DATA_DIR(
    "wikitty.data.directory",
    n("Data directory where wikitty store data"),
    "./target/data",
    String.class, false, false),
    WIKITTY_STORAGE_CRYPT_PASSWORD(
    "wikitty.storage.crypt.password",
    n("Password utilise pour crypter les champs qui n'ont pas de mot de passe propre"),
    "",
    String.class, false, false),
    WIKITTY_STORAGE_JDBC_QUERY_FILE(
    "wikitty.storage.jdbc.queryfile",
    n("JDBC query configuration file. You can put more than one file"
      + " to load specific SQL statement for your database."
      + " Exemple: wikitty-jdbc-query.properties,wikitty-jdbc-query-mysql.properties"),
    "wikitty-jdbc-query.properties",
    String.class, false, false),
    WIKITTY_STORAGE_JDBC_DRIVER(
    "wikitty.storage.jdbc.driver",
    n("JDBC driver name"),
    "org.h2.Driver",
    String.class, false, false),
    WIKITTY_STORAGE_JDBC_URL(
    "wikitty.storage.jdbc.host",
    n("JDBC url"),
    "jdbc:h2:file:${" + WIKITTY_DATA_DIR.getKey() + "}/db;AUTO_SERVER=TRUE",
    String.class, false, false),
    WIKITTY_STORAGE_JDBC_LOGIN(
    "wikitty.storage.jdbc.login",
    n("JDBC login name"),
    "sa",
    String.class, false, false),
    WIKITTY_STORAGE_JDBC_PASSWORD(
    "wikitty.storage.jdbc.password",
    n("JDBC password"),
    "",
    String.class, false, false),
    WIKITTY_STORAGE_JDBC_XADATASOURCE(
    "wikitty.storage.jdbc.xadatasource",
    n("JDBC xadatasource driver"),
    null, // no default XA otherwize we must set it to empty in all config file
    String.class, false, false),
    WIKITTY_STORAGE_JDBC_XADATASOURCE_H2_URL(
    "wikitty.storage.jdbc.xadatasource.org.h2.jdbcx.JdbcDataSource.URL",
    n("JDBC xadatasource property h2 url"),
    "${" + WIKITTY_STORAGE_JDBC_URL.getKey() + "}",
    String.class, false, false),
    WIKITTY_STORAGE_JDBC_XADATASOURCE_H2_USER(
    "wikitty.storage.jdbc.xadatasource.org.h2.jdbcx.JdbcDataSource.user",
    n("JDBC xadatasource property h2 username"),
    "${" + WIKITTY_STORAGE_JDBC_LOGIN.getKey() + "}",
    String.class, false, false),
    WIKITTY_STORAGE_JDBC_XADATASOURCE_H2_PASSWORD(
    "wikitty.storage.jdbc.xadatasource.org.h2.jdbcx.JdbcDataSource.password",
    n("JDBC xadatasource property h2 password"),
    "${" + WIKITTY_STORAGE_JDBC_PASSWORD.getKey() + "}",
    String.class, false, false),
    WIKITTY_SEARCHENGINE_SOLR_HOME("wikitty.searchengine.solr.home",
    n("SolR directory home. You can put solr.xml config file in. If"
      + " not null, replace solr.solr.home environment vairable"),
    null,
    String.class, false, false),
    /* Solr config name are used directly in solr config file. If
     * you change it, change too in module
     * wikitty-solr-impl/src/main/resources/solrconfig.xml */
    WIKITTY_SEARCHENGINE_SOLR_DIRECTORY_DATA(
    "wikitty.searchengine.solr.directory.data",
    n("Solr data directory"),
    "${" + WIKITTY_DATA_DIR.getKey() + "}/solr",
    File.class, false, false),
    WIKITTY_SEARCHENGINE_SOLR_DIRECTORY_FACTORY(
    "wikitty.searchengine.solr.directory.factory",
    n("SolR storage type possible value: solr.StandardDirectoryFactory"
      + " or org.nuiton.wikitty.storage.solr.RAMDirectoryFactory"
      + " (or when solr 4.0 will be used org.apache.solr.core.RAMDirectoryFactory)"),
    "solr.StandardDirectoryFactory",
    String.class, false, false),
    WIKITTY_SEARCHENGINE_LUCENE_INDEX_DIRECTORY(
    "wikitty.searchengine.lucene.index.directory",
    n("Lucene index directory"),
    "${" + WIKITTY_DATA_DIR.getKey() + "}/lucene",
    String.class, false, false),
    WIKITTY_SEARCHENGINE_LUCENE_INDEX_ANALYZER(
    "wikitty.searchengine.lucene.index.analyzer",
    n("Lucene index analyzer (default to lucene's standard analyzer)"),
    null, String.class, false, false),
    WIKITTY_WIKITTYSERVICE_COMPONENTS(
    "wikitty.WikittyService.components",
    n("WikittyService to use (list must be in right order. "
      + "ex: org.nuiton.wikitty.WikittyServiceStorage,org.nuiton.wikitty.WikittyServiceNotifier,"
      + "org.nuiton.wikitty.WikittyServiceCached,org.nuiton.wikitty.WikittyServiceAuthentication,"
      + "org.nuiton.wikitty.WikittyServiceAuthorisation)"),
    WikittyServiceInMemory.class.getName(),
    String.class, false, false),
    WIKITTY_WIKITTYSERVICEINMEMORY_PERSISTENCE(
    "wikitty.WikittyServiceInMemory.persistence",
    n("Indique si les donnees sont stocker entre deux utilisations."),
    "false",
    Boolean.class, false, false),
    WIKITTY_WIKITTYSERVICEINMEMORY_PERSISTENCE_FILE(
    "wikitty.WikittyServiceInMemory.persistence.file",
    n("Fichier ou les donnees sont stocker"),
    "${" + WIKITTY_DATA_DIR.getKey() + "}/wikitty-inmemory.ser",
    File.class, false, false),
    WIKITTY_WIKITTYSERVICESTORAGE_COMPONENTS(
    "wikitty.WikittyServiceStorage.components",
    n("WikittyServiceStorage component to use"
      + " (ExtensionStorage, WikittyStorage, SearchEngine)"),
    "org.nuiton.wikitty.jdbc.WikittyExtensionStorageJDBC,"
    + "org.nuiton.wikitty.jdbc.WikittyStorageJDBC,"
    + "org.nuiton.wikitty.storage.solr.WikittySearchEngineSolr",
    String.class, false, false),
    WIKITTY_WIKITTYSERVICENOTIFIER_COMPONENTS(
    "wikitty.WikittyServiceNotifier.components",
    n("WikittyServiceNotifier component to use (transporter class"
      + "to use for propagate or listen remote event)"),
    XMPPNotifierTransporter.class.getName(),
    String.class, false, false),
    WIKITTY_TRANSACTION_WIKITTYSERVICE_COMPONENTS(
    "wikitty.transaction.WikittyService.components",
    n("WikittyService to use for transaction (must be in memory)"
      + "ex: org.nuiton.wikitty.services.WikittyServiceInMemory"
      + " or org.nuiton.wikitty.services.WikittyServiceInMemoryJdbcSolr)"),
    WikittyServiceInMemory.class.getName(),
    String.class, false, false),
    WIKITTY_WIKITTYSERVICECACHED_COMPONENTS(
    "wikitty.WikittyServiceCached.components",
    n("Indique le composant a utiliser pour le cache"),
    WikittyCacheJCS.class.getName(),
    String.class, false, false),
    WIKITTY_CACHE_PRIORITY_EXTENSIONS(
    "wikitty.service.cache.priority.extensions",
    n("Indique la liste des extensions qui doivent etre "
      + "conservee prioritairement"),
    "",
    String.class, false, false),
    WIKITTY_CACHE_SIMPLE_REFERENCE_TYPE(
    "wikitty.service.cache.simple.reference.type",
    n("Indique le type de reference a utiliser pour les extensions"
      + " non prioritaires dans le WikittyCacheSimple (soft ou hard)"),
    "soft",
    String.class, false, false),
    WIKITTY_WIKITTYSERVICEACCESSSTAT_COMPONENTS(
    "wikitty.WikittyServiceAccessStat.components",
    n("Indique le composant a utiliser pour le stockage des acces"),
    AccessStatStorageWikitty.class.getName(),
    String.class, false, false),
    WIKITTY_ACCESSSTAT_EXTENSIONS(
    "wikitty.service.accessstat.extensions",
    n("Indique la liste d'extension dont il faut monitorer l'acces"),
    "WikittyPubText,WikittyPubData",
    String.class, false, false),
    @Deprecated
    WIKITTY_WIKITTYSERVICESECURITY_COMPONENTS(
    "wikitty.WikittyServiceSecurity.components",
    n("Indique le composant a utiliser pour l'authentification externe"
      + " ex:WikittyServiceSecurityExternalAuthenticationLDAP"),
    null,
    String.class, false, false),
    @Deprecated // not used any more with new split of authentification and authorization
    WIKITTY_SECURITY_EXTERNAL_AUTHENTICATION_ONLY(
    "wikitty.security.externalAuthenticationOnly",
    n("Indique si on doit utiliser que le composant d'authentification externe"
      + " pour authentifier les utilisateurs. Dans ce cas le module doit"
      + " etre configurer, sinon aucun utilisateur ne pourra se logguer"),
    "false",
    Boolean.class, false, false),
    @Deprecated // not used any more with new split of authentification and authorization
    WIKITTY_SECURITY_EXTERNAL_AUTHENTICATION_LDAP_JNDI(
    "wikitty.security.externalAuthentication.ldap.jndi.",
    n("Prefix a utiliser pour ajouter n'importe quelle option supporte par le"
      + " jndi pour la creation du initialContext. Vous ajoutez derriere"
      + " se prefixe la cle jndi ex: '<prefix>java.naming.security.protocol"),
    null,
    String.class, false, false),
    @Deprecated // not used any more with new split of authentification and authorization
    WIKITTY_SECURITY_EXTERNAL_AUTHENTICATION_LDAP_JNDI_INITIAL_CONTEXT_FACTORY(
    WIKITTY_SECURITY_EXTERNAL_AUTHENTICATION_LDAP_JNDI.getKey() + "java.naming.factory.initial",
    n("Indique l'initial context factory a utiliser par defaut 'com.sun.jndi.ldap.LdapCtxFactory'"),
    "com.sun.jndi.ldap.LdapCtxFactory",
    String.class, false, false),
    WIKITTY_SECURITY_EXTERNAL_AUTHENTICATION_LDAP_JNDI_SECURITY_AUTHENTICATION(
    WIKITTY_SECURITY_EXTERNAL_AUTHENTICATION_LDAP_JNDI.getKey() + "java.naming.security.authentication",
    n("Indique le mode d'authentification a utiliser par defaut 'simple'."
      + " Les modes possibles sont 'simple', 'SSL', 'SASL'"),
    "simple",
    String.class, false, false),
    // LDAP serveur n'est pas en prefix JNDI, car pourrait servir ailleur que pour JNDI
    @Deprecated // not used any more with new split of authentification and authorization
    WIKITTY_SECURITY_EXTERNAL_AUTHENTICATION_LDAP_SERVER(
    "wikitty.security.externalAuthentication.ldap.server",
    n("Indique l'url du serveur LDAP (obligatoire)"),
    null,
    String.class, false, false),
    // LDAP login pattern n'est pas en prefix JNDI, car pourrait servir ailleur que pour JNDI
    @Deprecated // not used any more with new split of authentification and authorization
    WIKITTY_SECURITY_EXTERNAL_AUTHENTICATION_LDAP_LOGIN_PATTERN(
    "wikitty.security.externalAuthentication.ldap.loginPattern",
    n("Le pattern utilise pour generer le DN de l'utilisateur. le '%s' est"
      + " remplace par le login  (obligatoire)."
      + " Par exemple: 'uid=%s,ou=People,dc=codelutin,dc=com'"),
    null,
    String.class, false, false),
    WIKITTY_SERVER_CONFIG(
    "wikitty.service.server.config",
    n("Jetty server configuration file name"),
    "wikitty-server-config.xml",
    String.class, false, false),
    WIKITTY_SERVER_URL(
    "wikitty.service.server.url",
    n("Hessian server url, use by server to know path and port to bind"
      + " servlet and client to contact server"),
    "http://service.codelutin.com/wikitty",
    String.class, false, false),
    WIKITTY_EXPORT_THREADNUMBER(
    "wikitty.addon.export.threadnumber",
    n("number of thread used to export task"),
    "1",
    Integer.class, false, false),
    WIKITTY_EXPORT_DIRECTORY(
    "wikitty.addon.export.directory",
    n("directory path where export asynchronous file are stored"),
    "${" + WIKITTY_DATA_DIR.getKey() + "}/export",
    String.class, false, false),
    WIKITTY_EXPORT_PUBLICURL(
    "wikitty.addon.export.publicurl",
    n("url used by client to retrieve export file when job is ended"),
    "file://${" + WIKITTY_DATA_DIR.getKey() + "}/export/",
    String.class, false, false),
    WIKITTY_CACHE_RESTORE_COPIES(
    "wikitty.service.cache.allwaysRestoreCopies",
    n("Indique si le cache retourne des copies des objets ou des proxies"),
    "false",
    Boolean.class, false, false),
    WIKITTY_EVENT_PROPAGATE("wikitty.service.event.propagate",
    n("Indique si le service d'event propage sur le reseau les evenements"),
    "false",
    Boolean.class, false, false),
    WIKITTY_EVENT_LISTEN(
    "wikitty.service.event.listen",
    n("Indique si le service d'event ecoute sur le reseau les evenements"),
    "false",
    Boolean.class, false, false),
    WIKITTY_EVENT_TRANSPORTER_XMPP_SERVER(
    "wikitty.service.event.transporter.xmpp.server",
    n("XMPP server to use for XMPP transporter"),
    "im.codelutin.com",
    String.class, false, false),
    WIKITTY_EVENT_TRANSPORTER_XMPP_ROOM(
    "wikitty.service.event.transporter.xmpp.room",
    n("Room to use for XMPP transporter"),
    "test@conference.im.codelutin.com",
    String.class, false, false),
    WIKITTY_SLAVE_SYNC_STATE_INTERVALE(
    "wikitty.service.slave.sync.state.intervale",
    n("Intervale in second where slave service save state"
      + " synchronisation time"),
    "300",
    Integer.class, false, false),
    WIKITTY_SLAVE_SYNC_STATE_FILE(
    "wikitty.service.slave.sync.state.file",
    n("File path to store synchronisation state"),
    "${" + WIKITTY_DATA_DIR.getKey() + "}/syncState",
    String.class, false, false),
    WIKITTY_SLAVE_MASTER_URL("wikitty.service.slave.master.hessian.endpoint",
    n("Master url service, where to delegate modification action"),
    null,
    String.class, false, false),
    WIKITTY_SLAVE_MASTER_XMPP_SERVER(
    "wikitty.service.slave.master.xmpp.server",
    n("XMPP server to use for synchronization with master"),
    "im.codelutin.com",
    String.class, false, false),
    WIKITTY_SLAVE_MASTER_XMPP_ROOM(
    "wikitty.service.slave.master.xmpp.room",
    n("Room to use for synchronization with master,"
      + " this room must have history activated"),
    "test@conference.im.codelutin.com",
    String.class, false, false),
    @Deprecated
    WIKITTY_PROXY_TIME_TO_LOG_INFO(
    "wikitty.proxy.timeToLog.info",
    n("maximum time before send log info with time consumed"),
    "1000",
    Integer.class, false, false),
    @Deprecated
    WIKITTY_PROXY_TIME_TO_LOG_WARN(
    "wikitty.proxy.timeToLog.warn",
    n("maximum time before send log warn with time consumed"),
    "3000",
    Integer.class, false, false),
    WIKITTY_CLIENT_TIME_TO_LOG_INFO(
    "wikitty.client.timeToLog.info",
    n("maximum time before send log info with time consumed"),
    "1000",
    Integer.class, false, false),
    WIKITTY_CLIENT_TIME_TO_LOG_WARN(
    "wikitty.client.timeToLog.warn",
    n("maximum time before send log warn with time consumed"),
    "3000",
    Integer.class, false, false),
    WIKITTY_MIGRATION_CLASS(
    "wikitty.migration.class.",
    n("prefix utilise pour indique les classes a utiliser pour la migration d'extension. Par exemple "
      + "wikitty.migration.class.WikittyUser=org.nuiton.wikitty.entities.MigrationUser"),
    "",
    Class.class, false, false),
    @Deprecated // use WIKITTY_SERVICE_TIME_TO_LOG_INFO
    WIKITTY_SECURITY_TIME_TO_LOG_INFO(
    "wikitty.security.timeToLog.info",
    n("maximum time before send log info with time consumed"),
    "100",
    Integer.class, false, false),
    @Deprecated // use WIKITTY_SERVICE_TIME_TO_LOG_WARN
    WIKITTY_SECURITY_TIME_TO_LOG_WARN(
    "wikitty.security.timeToLog.warn",
    n("maximum time before send log warn with time consumed"),
    "500",
    Integer.class, false, false),
    WIKITTY_SERVICE_TIME_TO_LOG_INFO(
    "wikitty.service.timeToLog.info",
    n("maximum time before send log info with time consumed"),
    "100",
    Integer.class, false, false),
    WIKITTY_SERVICE_TIME_TO_LOG_WARN(
    "wikitty.service.timeToLog.warn",
    n("maximum time before send log warn with time consumed"),
    "500",
    Integer.class, false, false),
    WIKITTY_SERVICE_AUTHENTICATION_TIMEOUT(
    "wikitty.WikittyServiceAuthentication.timeout",
    n("maximum time in seconde before remove authentication token in database,"
      + " by default never timeout"),
    "0",
    Integer.class, false, false),
    WIKITTY_SERVICE_AUTHENTICATION_LDAP_AUTOCREATE_USER(
    "wikitty.WikittyServiceAuthenticationLDAP.autoCreateUser",
    n("Si vrai et que l'utilisateur n'est que dans le LDAP, lors de sa premiere"
      + " authentification il sera automatiquement creer dans la base"
      + " Wikitty. Si faux et que l'utilisateur n'existe pas,"
      + " l'authentification echouera toujours tant que l'utilisateur ne"
      + " sera pas cree manuellement dans la base Wikitty"),
    "true",
    Boolean.class, false, false),
    WIKITTY_SERVICE_AUTHENTICATION_LDAP_JNDI(
    "wikitty.WikittyServiceAuthenticationLDAP.jndi.",
    n("Prefix a utiliser pour ajouter n'importe quelle option supporte par le"
      + " jndi pour la creation du initialContext. Vous ajoutez derriere"
      + " se prefixe la cle jndi ex: '<prefix>java.naming.security.protocol"),
    null,
    String.class, false, false),
    WIKITTY_SERVICE_AUTHENTICATION_LDAP_JNDI_INITIAL_CONTEXT_FACTORY(
    WIKITTY_SERVICE_AUTHENTICATION_LDAP_JNDI.getKey() + "java.naming.factory.initial",
    n("Indique l'initial context factory a utiliser par defaut 'com.sun.jndi.ldap.LdapCtxFactory'"),
    "com.sun.jndi.ldap.LdapCtxFactory",
    String.class, false, false),
    WIKITTY_SERVICE_AUTHENTICATION_LDAP_JNDI_SECURITY_AUTHENTICATION(
    WIKITTY_SERVICE_AUTHENTICATION_LDAP_JNDI.getKey() + "java.naming.security.authentication",
    n("Indique le mode d'authentification a utiliser par defaut 'simple'."
      + " Les modes possibles sont 'simple', 'SSL', 'SASL'"),
    "simple",
    String.class, false, false),
    // LDAP serveur n'est pas en prefix JNDI, car pourrait servir ailleur que pour JNDI
    WIKITTY_SERVICE_AUTHENTICATION_LDAP_SERVER(
    "wikitty.WikittyServiceAuthenticationLDAP.server",
    n("Indique l'url du serveur LDAP (obligatoire)"),
    null,
    String.class, false, false),
    // LDAP login pattern n'est pas en prefix JNDI, car pourrait servir ailleur que pour JNDI
    WIKITTY_SERVICE_AUTHENTICATION_LDAP_LOGIN_PATTERN(
    "wikitty.WikittyServiceAuthenticationLDAP.loginPattern",
    n("Le pattern utilise pour generer le DN de l'utilisateur. le '%s' est"
      + " remplace par le login  (obligatoire)."
      + " Par exemple: 'uid=%s,ou=People,dc=codelutin,dc=com'"),
    null,
    String.class, false, false),
    JCS_DEFAULT("jcs.default",
    n("jcs auxiliares to use"),
    "",
    String.class, false, false),
    JCS_DEFAULT_CACHEATTRIBUTES_MAXOBJECTS(
    "jcs.default.cacheattributes.MaxObjects",
    n("jcs maximum number of items allowed in memory"),
    "1000",
    String.class, false, false);
    public String key;
    public String description;
    public String defaultValue;
    public Class<?> type;
    public boolean isTransient;
    public boolean isFinal;

    private WikittyConfigOption(String key, String description,
            String defaultValue, Class<?> type, boolean isTransient, boolean isFinal) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
        this.isTransient = isTransient;
        this.isFinal = isFinal;
    }

    @Override
    public boolean isFinal() {
        return isFinal;
    }

    @Override
    public boolean isTransient() {
        return isTransient;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    public String getOptionValue(ApplicationConfig config) {
        String result = config.getOption(getKey());
        return result;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public void setTransient(boolean isTransient) {
        this.isTransient = isTransient;
    }

    @Override
    public void setFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }
}
