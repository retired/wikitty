/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.AbstractMap.SimpleEntry;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.TimeLog;
import org.nuiton.wikitty.entities.BusinessEntity;
import org.nuiton.wikitty.entities.BusinessEntityImpl;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyGroup;
import org.nuiton.wikitty.entities.WikittyUser;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;
import org.nuiton.wikitty.search.operators.Element;
import org.nuiton.wikitty.services.WikittyEvent;
import org.nuiton.wikitty.services.WikittySecurityUtil;
import org.nuiton.wikitty.services.WikittyServiceEnhanced;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.nuiton.wikitty.entities.WikittyTokenHelper;
import org.nuiton.wikitty.search.TreeNodeResult;

/**
 * Wikitty proxy is used to transform wikitty object used by {@link WikittyService}
 * into business objects used by applications.
 * 
 * It also manage {@link #securityToken} for {@link org.nuiton.wikitty.services.WikittyServiceSecurity}.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 * @deprecated since 3.3 use {@link WikittyClient}
 */
@Deprecated
public class WikittyProxy {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(WikittyProxy.class);
    final static private TimeLog timeLog = new TimeLog(WikittyProxy.class);

    /** Delegated wikitty service. */
    protected WikittyServiceEnhanced wikittyService;

    /**
     * Security token.
     * 
     * @see org.nuiton.wikitty.services.WikittyServiceSecurity#login(String, String)
     */
    protected String securityToken;

    /**
     * Empty constructor (uninitialized wikittyService).
     */
    public WikittyProxy() {
    }

    public WikittyProxy(ApplicationConfig config) {
        if (config != null) {
            long timeToLogInfo = config.getOptionAsInt(WikittyConfigOption.
                    WIKITTY_PROXY_TIME_TO_LOG_INFO.getKey());
            long timeToLogWarn = config.getOptionAsInt(WikittyConfigOption.
                    WIKITTY_PROXY_TIME_TO_LOG_WARN.getKey());
            timeLog.setTimeToLogInfo(timeToLogInfo);
            timeLog.setTimeToLogWarn(timeToLogWarn);
        }
    }

    /**
     * Constructor with wikittyService.
     * 
     * @param wikittyService wikitty service
     */
    public WikittyProxy(WikittyService wikittyService) {
        this();
        setWikittyService(wikittyService);
    }

    public WikittyProxy(ApplicationConfig config, WikittyService wikittyService) {
        this(config);
        setWikittyService(wikittyService);
    }

    static public TimeLog getTimeTrace() {
        return timeLog;
    }

    static public Map<String, TimeLog.CallStat> getCallCount() {
        return timeLog.getCallCount();
    }

    public void login(String login, String password) {
        long start = TimeLog.getTime();
        String result = wikittyService.login(login, password);
        setSecurityToken(result);
        
        timeLog.log(start, "login");
    }

    public void logout() {
        long start = TimeLog.getTime();
        wikittyService.logout(securityToken);
        
        timeLog.log(start, "logout");
    }

    public String getSecurityToken() {
        return securityToken;
    }

    public void setSecurityToken(String securityToken) {
        this.securityToken = securityToken;
    }

    /**
     * get current wikittyUser logged or null if no user logged
     * @return null if no user logged
     */
    public WikittyUser getUser() {
        WikittyUser result = getUser(WikittyUser.class);
        return result;
    }

    /**
     * get current logged user wikitty object
     * @param clazz Business class used as User in your application,
     * this extension should be require WikittyUser.
     * @return null if no user logged
     */
    public <E extends BusinessEntity> E getUser(Class<E> clazz) {
        E result = null;
        if (securityToken != null) {
            //Get the token
            Wikitty securityTokenWikitty = restore(securityToken);
            if (securityTokenWikitty != null) {
                //Get the user
                String userId = WikittyTokenHelper.getUser(securityTokenWikitty);
                result = restore(clazz, userId);
            }
        }
        return result;
    }

    public WikittyService getWikittyService() {
        return wikittyService.getDelegate();
    }

    public void setWikittyService(WikittyService wikittyService) {
        this.wikittyService = new WikittyServiceEnhanced(wikittyService);
    }

    /**
     * Cast Business object to another Business Object
     * If source don't have target extension, this extension is added
     *
     * @param source business entity source
     * @param target business entity class wanted
     * @return new instance of object wanted
     */
    public <E extends BusinessEntity> E cast(BusinessEntity source, Class<E> target) {
        long start = TimeLog.getTime();
        E result = WikittyUtil.newInstance(
                securityToken, wikittyService, target, ((BusinessEntityImpl)source).getWikitty());
        
        timeLog.log(start, "cast");
        return result;
    }

    public <E extends BusinessEntity> E store(E e) {
        Wikitty w = ((BusinessEntityImpl)e).getWikitty();
        store(w);
        return e;
    }

    public Wikitty store(Wikitty w) {
        long start = TimeLog.getTime();
        WikittyEvent resp = wikittyService.store(securityToken, w);
        // update object
        resp.update(w);
        
        timeLog.log(start, "store");
        return w;
    }

    // TODO poussin 20110420 je ne sais pas comment creer un tableau de E pour
    // le retouner. Donc je retourne la liste au lieu d'un toArray sur celle-ci
    public <E extends BusinessEntity> List<E> store(E e1, E e2, E... eN) {
        List<E> es = new ArrayList<E>(eN.length + 2);
        Collections.addAll(es, e1, e2);
        Collections.addAll(es, eN);

        List<E> result = store(es);
        return result;
    }

    public Wikitty[] store(Wikitty w1, Wikitty w2, Wikitty... wN) {
        List<Wikitty> ws = new ArrayList<Wikitty>(wN.length + 2);
        Collections.addAll(ws, w1, w2);
        Collections.addAll(ws, wN);

        List<Wikitty> resultList = storeWikitty(ws);
        Wikitty[] result = resultList.toArray(new Wikitty[resultList.size()]);
        return result;
    }

    /**
     * Store to WikittyService objects.
     * 
     * @param <E> object type
     * @param objets list
     * @return updated objects list
     */
    public <E extends BusinessEntity> List<E> store(List<E> objets) {
        long start = TimeLog.getTime();
        // prepare data to send to service
        List<Wikitty> wikitties = new ArrayList<Wikitty>(objets.size());
        for (E e : objets) {
            if (e == null) {
                wikitties.add(null);
            } else {
                Wikitty w = ((BusinessEntityImpl)e).getWikitty();
                wikitties.add(w);
            }
        }

        // call the service with Wikitty
        WikittyEvent resp = wikittyService.store(securityToken, wikitties);

        // update object
        for (Wikitty w : wikitties) {
            resp.update(w);
        }

        timeLog.log(start, "store<list>");
        return objets;
    }

    public  List<Wikitty> storeWikitty(List<Wikitty> wikitties) {
        long start = TimeLog.getTime();

        // call the service with Wikitty
        WikittyEvent resp = wikittyService.store(securityToken, wikitties);

        // update object
        for (Wikitty w : wikitties) {
            resp.update(w);
        }

        timeLog.log(start, "storeWikitty<list>");
        return wikitties;
    }

    /**
     * Restore wikitty entity with specified id or {@code null} if entity can't be found.
     * 
     * @param <E> object type
     * @param clazz entity class
     * @param id entity id
     * @param checkExtension if true check that Wikitty result has all extension
     * declared in clazz
     * @return wikitty entity with specified id or {@code null} if entity can't be found
     */
    public <E extends BusinessEntity> E restore(Class<E> clazz, String id, boolean checkExtension) {
        try {
            long start = TimeLog.getTime();
            E result = null;
            if (id != null) {
                HashSet<String> extNames = null;

                Wikitty wikitty = wikittyService.restore(securityToken, id);
                if (wikitty != null) {
                    if (checkExtension) {
                        extNames = new HashSet<String>(wikitty.getExtensionNames());
                    }
                    result = WikittyUtil.newInstance(
                            securityToken, wikittyService, clazz, wikitty);

                    if (checkExtension) {
                        // WikittyUtil.newInstance instanciate only BusinessEntityWikittyImpl
                        BusinessEntityImpl b = (BusinessEntityImpl) result;
                        Collection<WikittyExtension> BusinessEntityStaticExtensions = b.getStaticExtensions();
                        for (WikittyExtension ext : BusinessEntityStaticExtensions) {
                            String extensionName = ext.getName();
                            if (!extNames.contains(extensionName)) {
                                // extension wanted by BusinessEntity (clazz)
                                // is not in wikitty, then wikitty is not good type
                                // for business
                                result = null;
                                break;
                            }
                        }
                    }
                }
            }
            
            timeLog.log(start, "restore<Business>");
            return result;
        } catch (SecurityException eee) {
            throw eee;
        } catch (Exception eee) {
            throw new WikittyException("Can't restore wikitty", eee);
        }
    }

    /**
     * Restore wikitty entity with specified id or {@code null} if entity can't be found.
     * 
     * @param id entity id
     * @return wikitty entity with specified id or {@code null} if entity can't be found
     */
    public Wikitty restore(String id) {
        long start = TimeLog.getTime();
        Wikitty result = null;
        if (id != null) {
            result = wikittyService.restore(securityToken, id);
        }
        
        timeLog.log(start, "restore");
    	return result;
    }
    
    /**
     * Restore wikitty entity with specified id or {@code null} if entity can't be found.
     * 
     * @param <E> object type
     * @param clazz entity class
     * @param id entity id
     * @return wikitty entity with specified id or {@code null} if entity can't be found
     */
    public <E extends BusinessEntity> E restore(Class<E> clazz, String id) {
        E result = restore(clazz, id, false);
        return result;
    }

    /**
     * Restore wikitty entity with specified id or {@code null} if entity can't be be found.
     *
     * @param id entity ids if null return is empty list
     * @return wikitty entity with specified id or {@code null} if entity can't be found
     */
    public List<Wikitty> restore(List<String> id) {
        long start = TimeLog.getTime();

        List<Wikitty> result;
        if (id == null) {
            result = new ArrayList<Wikitty>();
        } else {
            result = wikittyService.restore(securityToken, id);
        }

        timeLog.log(start, "restoreWikitty<list>");
        return result;
    }

    /**
     * Restore wikitty entity with specified id or {@code null} if entity
     * can't be be found.
     * 
     * @param <E> object type
     * @param clazz entity class
     * @param id entity ids if null return is empty list
     * @param checkExtension if true check that Wikitty result has all extension
     * @return wikitty entity with specified id or {@code null} if entity
     * can't be found or if one wikitty don't have extension wanted by E type
     */
    public <E extends BusinessEntity> List<E> restore(
            Class<E> clazz, List<String> id, boolean checkExtension) {
        long start = TimeLog.getTime();
        List<E> result = new ArrayList<E>();

        if (id != null) {
            List<Wikitty> wikitties = wikittyService.restore(securityToken, id);
            for (Wikitty w : wikitties) {
                HashSet<String> extNames = null;
                if (checkExtension) {
                    extNames = new HashSet<String>(w.getExtensionNames());
                }

                E dto = WikittyUtil.newInstance(
                        securityToken, wikittyService, clazz, w);
                if (checkExtension) {
                    // WikittyUtil.newInstance instanciate only BusinessEntityWikittyImpl
                    BusinessEntityImpl b = (BusinessEntityImpl) dto;
                    for (WikittyExtension ext : b.getStaticExtensions()) {
                        if (!extNames.contains(ext.getName())) {
                            // extension wanted by BusinessEntity (clazz)
                            // is not in wikitty, then wikitty is not good type
                            // for business
                            dto = null;
                            break;
                        }
                    }
                }

                // Add only entity in good types
                if (dto != null) {
                    result.add(dto);
                }
            }
        }
        timeLog.log(start, "restore<list>");
        return result;
    }

    public <E extends BusinessEntity> List<E> restore(Class<E> clazz, List<String> id) {
        List<E> result = restore(clazz, id, false);
        return result;
    }

    public Set<Wikitty> restore(Set<String> id) {
        ArrayList<String> list = null;
        if (id != null) {
            list = new ArrayList<String>(id);
        }
        List<Wikitty> resultList = restore(list);
        Set<Wikitty> result = new HashSet<Wikitty>(resultList);
        return result;
    }

    public <E extends BusinessEntity> Set<E> restore(Class<E> clazz, Set<String> id) {
        Set<E> result = restore(clazz, id, false);
        return result;
    }

    public <E extends BusinessEntity> Set<E> restore(Class<E> clazz, Set<String> id, boolean checkExtension) {
        ArrayList<String> list = null;
        if (id != null) {
            list = new ArrayList<String>(id);
        }
        List<E> resultList = restore(clazz, list, checkExtension);
        Set<E> result = new HashSet<E>(resultList);
        return result;
    }

    public void delete(String id) {
        long start = TimeLog.getTime();
        wikittyService.delete(securityToken, id);

        timeLog.log(start, "delete");
    }

    public <E extends BusinessEntity> void delete(E object) {
        long start = TimeLog.getTime();
        if (object != null) {
            String id = object.getWikittyId();
            wikittyService.delete(securityToken, id);
        }
        timeLog.log(start, "delete(BusinessEntity)");
    }

    public void delete(Collection<String> ids) {
        long start = TimeLog.getTime();
        wikittyService.delete(securityToken, ids);
        
        timeLog.log(start, "delete<list>");
    }

    public <E extends BusinessEntity> void delete(List<E> objets) {
        long start = TimeLog.getTime();
        
        // prepare data to send to service
        List<String> ids = new ArrayList<String>(objets.size());
        for (E e : objets) {
            if (e != null) {
                String id = e.getWikittyId();
                ids.add(id);
            }
        }

        // call the service with Wikitty
        wikittyService.delete(securityToken, ids);

        timeLog.log(start, "delete<list<BusinessEntity>>");
    }

    /**
     * Null field are not used in search request.
     *
     * @param e sample wikitty
     * @param firstIndex
     * @param endIndex
     * @param fieldFacet
     * @return
     */
    public <E extends BusinessEntityImpl> PagedResult<E> findAllByExample(E e,
            int firstIndex, int endIndex, String ... fieldFacet ) {
        long start = TimeLog.getTime();

        Criteria criteria = Search.query(e.getWikitty()).criteria()
                .setFirstIndex(firstIndex).setEndIndex(endIndex)
                .setFacetField(fieldFacet);

        PagedResult<String> pagedResult = findAllIdByCriteria(criteria);
        // we can use autoconvert = true because search by example add automaticaly
        // restriction on extension
        PagedResult<E> result = (PagedResult<E>)pagedResult.cast(
                this, e.getClass(), true);
        
        timeLog.log(start, "findAllByExample<limit>");
        return result;
    }

    /**
     * Null field are not used in search request.
     * 
     * @param e sample wikitty
     * @return
     */
    public <E extends BusinessEntityImpl> E findByExample(E e) {
        long start = TimeLog.getTime();
        Criteria criteria = Search.query(e.getWikitty()).criteria();

        String id = findIdByCriteria(criteria);
        E result = null;
        if (id != null) {
            Wikitty w = wikittyService.restore(securityToken, id);
            result = (E) WikittyUtil.newInstance(
                    securityToken, wikittyService, e.getClass(), w);
        }
        
        timeLog.log(start, "findByExample");
        return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // FIND ALL BY CRITERIA <E>
    //
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Search object that correspond to criteria and that have all extension
     * needed by BusinessEntity (clazz). If one criteria is null, find all extensions
     * for this criteria else if criteria is empty return nothing.
     * 
     * @param <E> object type
     * @param clazz entity class
     * @param criterias criterias
     * @return paged result
     */
    public <E extends BusinessEntity> List<PagedResult<E>> findAllByCriteria(
            Class<E> clazz, List<Criteria> criterias) {
        long start = TimeLog.getTime();
        List<PagedResult<E>> result = null;
        if (criterias != null) {
            // newInstance only return BusinessEntityWikittyImpl
            BusinessEntityImpl sample =
                    (BusinessEntityImpl) WikittyUtil.newInstance(clazz);

            Wikitty wikitty = sample.getWikitty();
            Collection<String> extensions = wikitty.getExtensionNames();

            List<Criteria> serviceCriterias = new ArrayList<Criteria>(criterias.size());
            for (Criteria criteria : criterias) {

                // on ajoute la condition sur les extensions dans le critere
                // du coup, pour ne pas modifier le critere qui vient en parametre
                // il faut creer un nouveau critere ...
                Criteria serviceCriteria = null;
                if (criteria != null) {
                    serviceCriteria = criteria.clone();

                    // Dont add contraint if using select
                    if (StringUtils.isEmpty(criteria.getSelect())) {

                        // utilisation de cette nouvelle contrainte sur le nouvel objet
                        // creation de la nouvelle contrainte
                        Search search = Search.query(criteria);
                        search = search.exteq(extensions);

                        serviceCriteria.setRestriction(search);
                    }
                }

                // ajout de ce criteria dans la liste de tous les criteres
                serviceCriterias.add(serviceCriteria);
            }

            List<PagedResult<String>> pagedResult = wikittyService.findAllByCriteria(
                    securityToken, serviceCriterias);

            // TODO poussin 20110318 optimize cast. Try to cast all pagedResult id
            // in one call to service. Currently each PagedResult.cast do a call
            result = new ArrayList<PagedResult<E>>(pagedResult.size());
            for (PagedResult<String> p : pagedResult) {
                result.add((PagedResult<E>)p.cast(this, sample.getClass(), true));
            }
        }
        timeLog.log(start, "findAllByCriteria<Business>(List)");
        return result;
    }

    /**
     * Search object that correspond to criteria and that have all extension
     * needed by BusinessEntity (clazz). If criteria is null, find all extensions
     * else if criteria is empty return nothing.
     *
     * @param <E> object type
     * @param clazz entity class
     * @param criteria criteria
     * @return paged result
     */
    public <E extends BusinessEntity> PagedResult<E> findAllByCriteria(
            Class<E> clazz, Criteria criteria) {
        long start = TimeLog.getTime();
        PagedResult<E> result = findAllByCriteria(clazz,
                Collections.singletonList(criteria)).get(0);
        timeLog.log(start, "findAllByCriteria<Business>(One)");
        return result;
    }

    /**
     * Search object that correspond to criteria and that have all extension
     * needed by BusinessEntity (clazz). If criteria is null, find all extensions
     * else if criteria is empty return nothing.
     *
     * @param <E> object type
     * @param clazz entity class
     * @param c1 criteria 1
     * @param c2 criteria 2
     * @param otherCriteria otherCriteria
     * @return paged result
     */
    public <E extends BusinessEntity> PagedResult<E>[] findAllByCriteria(
            Class<E> clazz, Criteria c1, Criteria c2, Criteria... otherCriteria) {
        long start = TimeLog.getTime();
        List<Criteria> criterias = new ArrayList<Criteria>(otherCriteria.length + 2);
        Collections.addAll(criterias, c1, c2);
        Collections.addAll(criterias, otherCriteria);

        List<PagedResult<E>> resultList = findAllByCriteria(clazz, criterias);
        PagedResult<E>[] result = resultList.toArray(new PagedResult[criterias.size()]);
        timeLog.log(start, "findAllByCriteria<Business>(Varargs)");
        return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // FIND ALL BY CRITERIA <Wikitty>
    //
    ///////////////////////////////////////////////////////////////////////////

    public List<PagedResult<Wikitty>> findAllByCriteria(List<Criteria> criteria) {
        long start = TimeLog.getTime();
        List<PagedResult<Wikitty>> result = null;
        if (criteria != null) {
            List<PagedResult<String>> resultId =
                    wikittyService.findAllByCriteria(securityToken, criteria);

            // TODO poussin 20110318 optimize cast. Try to cast all pagedResult id
            // in one call to service. Currently each PagedResult.cast do a call
            result = new ArrayList<PagedResult<Wikitty>>(resultId.size());
            for (PagedResult<String> p : resultId) {
                result.add(p.cast(securityToken, wikittyService));
            }
        }
        timeLog.log(start, "findAllByCriteria(List)");
    	return result;
    }

    public PagedResult<Wikitty> findAllByCriteria(Criteria criteria) {
        long start = TimeLog.getTime();
        PagedResult<Wikitty> result = null;
        if (criteria != null) {
            result = findAllByCriteria(Collections.singletonList(criteria)).get(0);
        }
        timeLog.log(start, "findAllByCriteria(One)");
    	return result;
    }

    public PagedResult<Wikitty>[] findAllByCriteria(
            Criteria c1, Criteria c2, Criteria ... otherCriteria) {
        long start = TimeLog.getTime();

        List<Criteria> criterias = new ArrayList<Criteria>(otherCriteria.length + 2);
        Collections.addAll(criterias, c1, c2);
        Collections.addAll(criterias, otherCriteria);

        List<PagedResult<Wikitty>> resultList = findAllByCriteria(criterias);
        PagedResult<Wikitty>[] result = resultList.toArray(new PagedResult[criterias.size()]);

        timeLog.log(start, "findAllByCriteria(Varargs)");
    	return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // FIND ALL ID BY CRITERIA <String>
    //
    ///////////////////////////////////////////////////////////////////////////

    public List<PagedResult<String>> findAllIdByCriteria(List<Criteria> criteria) {
        long start = TimeLog.getTime();
        List<PagedResult<String>> result = null;
        if (criteria != null) {
            result = wikittyService.findAllByCriteria(securityToken, criteria);
        }
        timeLog.log(start, "findAllIdByCriteria(List)");
    	return result;
    }

    public PagedResult<String> findAllIdByCriteria(Criteria criteria) {
        long start = TimeLog.getTime();
        PagedResult<String> result = null;
        if (criteria != null) {
            result = findAllIdByCriteria(
                    Collections.singletonList(criteria)).get(0);
        }
        timeLog.log(start, "findAllIdByCriteria(One)");
    	return result;
    }

    public PagedResult<String>[] findAllIdByCriteria(
            Criteria c1, Criteria c2, Criteria ... otherCriteria) {
        long start = TimeLog.getTime();

        List<Criteria> criterias = new ArrayList<Criteria>(otherCriteria.length + 2);
        Collections.addAll(criterias, c1, c2);
        Collections.addAll(criterias, otherCriteria);

        List<PagedResult<String>> resultList = findAllIdByCriteria(criterias);
        PagedResult<String>[] result = resultList.toArray(new PagedResult[criterias.size()]);

        timeLog.log(start, "findAllIdByCriteria(Varargs)");
    	return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // FIND ID BY CRITERIA <String>
    //
    ///////////////////////////////////////////////////////////////////////////

    public List<String> findIdByCriteria(List<Criteria> criteria) {
        long start = TimeLog.getTime();
        List<String> result = null;
        if (criteria != null) {
            result = wikittyService.findByCriteria(securityToken, criteria);
        }
        timeLog.log(start, "findIdByCriteria(List)");
    	return result;
    }

    public String findIdByCriteria(Criteria criteria) {
        long start = TimeLog.getTime();
        String result = null;
        if (criteria != null) {
            result = findIdByCriteria(Collections.singletonList(criteria)).get(0);
        }
        timeLog.log(start, "findIdByCriteria(One)");
    	return result;
    }

    public String[] findIdByCriteria(
            Criteria c1, Criteria c2, Criteria... otherCriteria) {
        long start = TimeLog.getTime();

        List<Criteria> criterias = new ArrayList<Criteria>(otherCriteria.length + 2);
        Collections.addAll(criterias, c1, c2);
        Collections.addAll(criterias, otherCriteria);

        List<String> resultList = findIdByCriteria(criterias);
        String[] result = resultList.toArray(new String[criterias.size()]);

        timeLog.log(start, "findIdByCriteria(Varargs)");
    	return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // FIND BY CRITERIA <E>
    //
    ///////////////////////////////////////////////////////////////////////////

    public <E extends BusinessEntity> List<E> findByCriteria(
            Class<E> clazz, List<Criteria> criterias) {
        long start = TimeLog.getTime();
        List<E> result = null;
        if (criterias != null) {
            BusinessEntityImpl sample =
                    (BusinessEntityImpl) WikittyUtil.newInstance(clazz);

            Wikitty wikitty = sample.getWikitty();
            Collection<String> extensions = wikitty.getExtensionNames();
            List<Criteria> serviceCriterias = new ArrayList<Criteria>(criterias.size());
            for (Criteria criteria : criterias) {
                Search search = Search.query(criteria);
                search = search.exteq(extensions);
                criteria = search.criteria();
                serviceCriterias.add(criteria);
            }

            List<String> id = findIdByCriteria(serviceCriterias);
            result = restore(clazz, id);
        }
        timeLog.log(start, "multiFindByCriteria<Business>(List>");
        return result;
    }

    public <E extends BusinessEntity> E findByCriteria(
            Class<E> clazz, Criteria criteria) {
        long start = TimeLog.getTime();
        E result = null;
        if (criteria != null) {
            List<E> criterias = findByCriteria(clazz, Collections.singletonList(criteria));
            if (!criterias.isEmpty()) {
                result = criterias.get(0);
            }
        }
        timeLog.log(start, "findByCriteria<Business>(One)");
        return result;
    }

    public <E extends BusinessEntity> E[] findByCriteria(
            Class<E> clazz, Criteria c1, Criteria c2, Criteria... otherCriteria) {
        long start = TimeLog.getTime();

        List<Criteria> criterias = new ArrayList<Criteria>(otherCriteria.length + 2);
        Collections.addAll(criterias, c1, c2);
        Collections.addAll(criterias, otherCriteria);

        List<E> resultList = findByCriteria(clazz, criterias);
        E[] result = resultList.toArray((E[])Array.newInstance(clazz, resultList.size()));

        timeLog.log(start, "findByCriteria<Business>(One)");
        return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // FIND BY CRITERIA <Wikitty>
    //
    ///////////////////////////////////////////////////////////////////////////

    public List<Wikitty> findByCriteria(List<Criteria> criteria) {
        long start = TimeLog.getTime();
        List<Wikitty> result = null;
        if (criteria != null) {
            List<String> id = findIdByCriteria(criteria);
            result = restore(id);
        }
        timeLog.log(start, "findByCriteria(List)");
    	return result;
    }

    public Wikitty findByCriteria(Criteria criteria) {
        long start = TimeLog.getTime();
        String id = findIdByCriteria(criteria);
        Wikitty wikitty = restore(id);
        timeLog.log(start, "findByCriteria(One)");
    	return wikitty;
    }

    public Wikitty[] findByCriteria(
            Criteria c1, Criteria c2, Criteria... otherCriteria) {
        long start = TimeLog.getTime();

        List<Criteria> criterias = new ArrayList<Criteria>(otherCriteria.length + 2);
        Collections.addAll(criterias, c1, c2);
        Collections.addAll(criterias, otherCriteria);

        List<String> resultList = findIdByCriteria(criterias);
        List<Wikitty> wikitties = restore(resultList);
        Wikitty[] result = wikitties.toArray(new Wikitty[resultList.size()]);

        timeLog.log(start, "findByCriteria(One)");
    	return result;
    }


    ///////////////////////////////////////////////////////////////////////////
    //
    // FIND BY TREE NODE
    //
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Recupere une portion d'arbre a partir de l'id passer en parametre. L'id
     * doit etre celui d'un WikittyTreeNode. Ce WikittyTreeNode est alors le
     * root de l'arbre retourne.
     *
     * Return Wikitty in result, those Wikitties have WikittyTreeNode extension
     *
     * @param wikittyId root
     * @param depth profondeur de noeud a recuperer
     * @param count vrai si l'on veut le nombre de piece attaches sur le noeud
     * (piece des enfants compris)
     * @param filter filter pour compter les pieces attachees
     * @return treeNodeResult of wikitty
     *
     * @since 3.1
     */
    public TreeNodeResult<Wikitty> findTreeNode(
            String wikittyId, int depth, boolean count, Criteria filter) {
        long start = TimeLog.getTime();

    	TreeNodeResult<String> resultId = wikittyService.findTreeNode(
                securityToken, wikittyId, depth, count, filter);

        RetrieveIdVisitor retrieveIdVisitor = new RetrieveIdVisitor();
        resultId.acceptVisitor(retrieveIdVisitor);

        List<String> ids = retrieveIdVisitor.getIds();
        List<Wikitty> wikitties = restore(ids);
        
        IdToObjectConverter<Wikitty> converter =
                new IdToObjectConverter<Wikitty>(ids, wikitties);

        ConvertTreeVisitor<Wikitty> convertVisitor =
                new ConvertTreeVisitor<Wikitty>(converter);

        resultId.acceptVisitor(convertVisitor);

    	TreeNodeResult<Wikitty> result = convertVisitor.getTree();
        timeLog.log(start, "findTreeNode<Wikitty>");
    	return result;
    }

    /**
     * Recupere une portion d'arbre a partir de l'id passer en parametre. L'id
     * doit etre celui d'un WikittyTreeNode. Ce WikittyTreeNode est alors le
     * root de l'arbre retourne.
     *
     * Return E in result
     *
     * @param clazz business class wanted to replace id in TreeNodeResult
     * @param wikittyId root
     * @param depth profondeur de noeud a recuperer
     * @param count vrai si l'on veut le nombre de piece attaches sur le noeud (piece des enfants compris)
     * @param filter filter pour compter les pieces attachees
     * @return
     *
     * @since 3.1
     */
    public <E extends BusinessEntity> TreeNodeResult<E> findTreeNode(
            Class<E> clazz, String wikittyId, int depth,
            boolean count, Criteria filter) {
        long start = TimeLog.getTime();

    	TreeNodeResult<String> resultId = wikittyService.findTreeNode(
                securityToken, wikittyId, depth, count, filter);

        RetrieveIdVisitor retrieveIdVisitor = new RetrieveIdVisitor();
        resultId.acceptVisitor(retrieveIdVisitor);

        List<String> ids = retrieveIdVisitor.getIds();
        List<E> wikitties = restore(clazz, ids);

        IdToObjectConverter<E> converter =
                new IdToObjectConverter<E>(ids, wikitties);

        ConvertTreeVisitor<E> convertVisitor =
                new ConvertTreeVisitor<E>(converter);

        resultId.acceptVisitor(convertVisitor);

    	TreeNodeResult<E> result = convertVisitor.getTree();
        timeLog.log(start, "findTreeNode");
    	return result;
    }

    /**
     * Used to collect all node id
     * @since 3.1
     */
    static private class RetrieveIdVisitor implements TreeNodeResult.Visitor<String> {

        protected List<String> ids = new ArrayList<String>();

        public List<String> getIds() {
            return ids;
        }

        @Override
        public boolean visitEnter(TreeNodeResult<String> node) {
            String id = node.getObject();
            ids.add(id);
            return true;
        }

        @Override
        public boolean visitLeave(TreeNodeResult<String> node) {
            return true;
        }
    }

    /**
     * Converti un id en son object WikittyTreeNode
     * @since 3.1
     */
    static private class IdToObjectConverter<T> implements ConvertTreeVisitor.Converter<String, T> {
        protected Map<String, T> objects = new HashMap<String, T>();
        protected String securityToken;
        protected WikittyService wikittyService;
        public IdToObjectConverter(List<String> ids, List<T> objectList) {

            for (int i = 0; i < ids.size(); i++) {
                this.objects.put(ids.get(i), objectList.get(i));
            }
        }

        @Override
        public T convert(String id) {
            T result = objects.get(id);
            return result;
        }
    }

    /**
     * Parcours un TreeNodeResult et en fait une copie en modifiant le type
     * d'objet stocker dans le noeud grace a un converter, si le converter
     * est null une exception est levee
     *
     * @param <TARGET> le type d'objet pour le nouvel arbre
     * @since 3.1
     */
    static private class ConvertTreeVisitor<TARGET extends Serializable> implements TreeNodeResult.Visitor<String> {

        static private interface Converter<SOURCE, TARGET> {
            public TARGET convert(SOURCE o);
        }
        protected Converter<String, TARGET> converter;
        protected TreeNodeResult<TARGET> tree = null;
        protected LinkedList<TreeNodeResult<TARGET>> stack =
                new LinkedList<TreeNodeResult<TARGET>>();

        public ConvertTreeVisitor(Converter<String, TARGET> converter) {
            this.converter = converter;
            if (converter == null) {
                throw new IllegalArgumentException("Converter can't be null");
            }
        }

        public TreeNodeResult<TARGET> getTree() {
            return tree;
        }

        @Override
        public boolean visitEnter(TreeNodeResult<String> node) {
            String id = node.getObject();
            int count = node.getAttCount();
            
            TARGET object = converter.convert(id);
            TreeNodeResult<TARGET> newNode = new TreeNodeResult<TARGET>(
                    object, count);

            TreeNodeResult<TARGET> parent = stack.peekLast();
            if (parent == null) {
                // le premier noeud, donc le root a retourner plus tard
                tree = newNode;
            } else {
                parent.add(newNode);
            }

            stack.offerLast(newNode);

            return true;
        }

        @Override
        public boolean visitLeave(TreeNodeResult<String> node) {
            stack.pollLast();
            return true;
        }
    }

    /**
     * Recupere une portion d'arbre a partir de l'id passer en parametre. L'id
     * doit etre celui d'un WikittyTreeNode. Ce WikittyTreeNode est alors le
     * root de l'arbre retourne.
     *
     * Return just wikitty Id in result
     *
     * @param wikittyId
     * @param depth
     * @param count
     * @param filter
     * @return
     * @since 3.1
     */
    public TreeNodeResult<String> findAllIdTreeNode(
            String wikittyId, int depth, boolean count, Criteria filter) {
        long start = TimeLog.getTime();
    	TreeNodeResult<String> result = wikittyService.findTreeNode(
                securityToken, wikittyId, depth, count, filter);

        timeLog.log(start, "findAllIdTreeNode");
    	return result;
    }

    /**
     * Delete specified tree node and all sub nodes.
     * 
     * @param treeNodeId tree node id to delete
     * @return {@code true} if at least one node has been deleted
     */
    public WikittyEvent deleteTree(String treeNodeId) {
        long start = TimeLog.getTime();
        WikittyEvent result = wikittyService.deleteTree(securityToken,treeNodeId);
        
        timeLog.log(start, "deleteTree");
        return result;
    }

    /**
     * Restore node with wikittyId passed in argument and count of attachment
     * in subtree begin with this node
     *
     * @param <E>
     * @param clazz
     * @param wikittyId
     * @param filter
     * @return
     * @deprecated since 3.1: use {@link #findTreeNode(java.lang.Class, java.lang.String, int, boolean, org.nuiton.wikitty.search.Criteria)} or open new ticket with your need
     */
    @Deprecated
    public <E extends BusinessEntity> Map.Entry<E, Integer> restoreNode(
            Class<E> clazz, String wikittyId, Criteria filter) {
        long start = TimeLog.getTime();

        TreeNodeResult<E> tree = findTreeNode(clazz, wikittyId, 0, true, filter);
        Map.Entry<E, Integer> result = new SimpleEntry<E, Integer>(
                tree.getObject(), tree.getAttCount());
        
        timeLog.log(start, "restoreNode");
    	return result;
    }

    /**
     * Restore node with wikittyId passed in argument and count of attachment
     * in subtree begin with this node
     *
     * ATTENTION: extension is never checked.
     * 
     * @param <E>
     * @param clazz
     * @param wikittyId
     * @param filter
     * @param checkExtension not used
     * @deprecated since 3.1: use {@link #findTreeNode(java.lang.Class, java.lang.String, int, boolean, org.nuiton.wikitty.search.Criteria)} or open new ticket with your need
     */
    @Deprecated
    public <E extends BusinessEntity> Map.Entry<E, Integer> restoreNode(
            Class<E> clazz, String wikittyId, Criteria filter, boolean checkExtension) {
        Map.Entry<E, Integer> result = restoreNode(clazz, wikittyId, filter);
        return result;
    }

    /**
     * Return only children of wikittyId passed in argument
     *
     * @param clazz
     * @param wikittyId
     * @param filter
     * @return
     *
     * @deprecated since 3.1: use {@link #findTreeNode(java.lang.Class, java.lang.String, int, boolean, org.nuiton.wikitty.search.Criteria) } or open new ticket with your need
     */
    @Deprecated
    public <E extends BusinessEntity> Map<E, Integer> findTreeNode(
            Class<E> clazz, String wikittyId, Criteria filter) {
        long start = TimeLog.getTime();

        TreeNodeResult<E> tree = findTreeNode(clazz, wikittyId, 1, true, filter);

        Map<E, Integer> result = new LinkedHashMap<E, Integer>();
        for (TreeNodeResult<E> child : tree.getChildren()) {
            result.put(child.getObject(), child.getAttCount());
        }

        timeLog.log(start, "findTreeNode");
        return result;
    }

    /**
     * Return only children of wikittyId passed in argument
     * 
     * ATTENTION: extension is never checked.
     *
     * @deprecated since 3.1: use {@link #findTreeNode(java.lang.Class, java.lang.String, int, boolean, org.nuiton.wikitty.search.Criteria) } or open new ticket with your need
     */
    @Deprecated
    public <E extends BusinessEntity> Map<E, Integer> findTreeNode(
            Class<E> clazz, String wikittyId, Criteria filter, boolean checkExtension) {
        Map<E, Integer> result = findTreeNode(clazz, wikittyId, filter);
        return result;
    }

    public Wikitty restoreVersion(String wikittyId, String version) {
        long start = TimeLog.getTime();
        Wikitty result = wikittyService.restoreVersion(
                securityToken, wikittyId, version);
        
        timeLog.log(start, "restoreVersion");
        return result;
    }

    /**
     * XXX poussin 20111229 cette methode n'est pas documentee et ne semble pas
     * utilisee, heureusement car son implantation est fausse :(.
     * On cree un BusinessEntity autour d'un wikitty puis ensuite on check les
     * extension, or lors de la creation du BusinessEntity sur le wikitty celui
     * ci a ajouter toutes les extensions. donc cette methode doit toujours
     * retourne vrai :(
     *
     * @param <E>
     * @param clazz
     * @param wikittyId
     * @return
     */
    public <E extends BusinessEntity> boolean hasType(Class<E> clazz, String wikittyId) {
        try {
            long start = TimeLog.getTime();
            
            boolean result = true;

            E businessObject;
            HashSet<String> extNames;
            Wikitty wikitty = wikittyService.restore(securityToken, wikittyId);
            if (wikitty == null) {
                result = false;
            } else {
                extNames = new HashSet<String>(wikitty.getExtensionNames());
                businessObject = WikittyUtil.newInstance(
                        securityToken, wikittyService, clazz, wikitty);
                // WikittyUtil.newInstance instanciate only BusinessEntityWikittyImpl
                BusinessEntityImpl b = (BusinessEntityImpl) businessObject;
                for (WikittyExtension ext : b.getStaticExtensions()) {
                    if (!extNames.contains(ext.getName())) {
                        // extension wanted by BusinessEntity (clazz)
                        // is not in wikitty, then wikitty is not good type
                        // for business
                        result = false;
                        break;
                    }
                }
            }
            
            timeLog.log(start, "hasType");
            return result;
        } catch (SecurityException eee) {
            throw eee;
        } catch (Exception eee) {
            throw new WikittyException(
                    "Can't retrieve wikitty needed for hasType test", eee);
        }
    }

    /**
     * Manage Update and creation.
     *
     * @param ext extension to be persisted
     * @return update response
     */
    public WikittyEvent storeExtension(WikittyExtension ext) {
        long start = TimeLog.getTime();
        WikittyEvent response =
                wikittyService.storeExtension(securityToken, ext);
        
        timeLog.log(start, "storeExtension");
        return response;
    }

    /**
     * Manage Update and creation.
     *
     * @param exts list of wikitty extension to be persisted
     * @return update response
     */
    public WikittyEvent storeExtension(Collection<WikittyExtension> exts) {
        long start = TimeLog.getTime();
        WikittyEvent response =
                wikittyService.storeExtension(securityToken, exts);
        
        timeLog.log(start, "storeExtension<list>");
        return response;
    }

    /**
     * Load extension from id. Id is 'name[version]'.
     * 
     * @param extensionId extension id to restore
     * @return the corresponding object, exception if no such object found.
     */
    public WikittyExtension restoreExtension(String extensionId) {
        long start = TimeLog.getTime();
        WikittyExtension extension = wikittyService.restoreExtension(securityToken, extensionId);
        
        timeLog.log(start, "restoreExtension");
        return extension;
    }

    /**
     * Search extension with name in last version.
     * 
     * @param extensionName extension name
     * @return the corresponding object, exception if no such object found.
     */
    public WikittyExtension restoreExtensionLastVersion(String extensionName) {
        long start = TimeLog.getTime();
        WikittyExtension extension = wikittyService.restoreExtensionLastVersion(securityToken, extensionName);
        
        timeLog.log(start, "restoreExtensionLastVersion");
        return extension;
    }

    /**
     * Search extension with name in last version.
     *
     * @param extensionNames extension name
     * @return extension wanted with dependencies extensions at head of list
     */
    public List<WikittyExtension> restoreExtensionAndDependenciesLastVesion(Collection<String> extensionNames) {
        long start = TimeLog.getTime();
        List<WikittyExtension> result =
                wikittyService.restoreExtensionAndDependenciesLastVesion(
                securityToken, extensionNames);

        timeLog.log(start, "restoreExtensionAndDependenciesLastVesion");
        return result;

    }

    public void deleteExtension(String extName) {
        long start = TimeLog.getTime();
        wikittyService.deleteExtension(securityToken, extName);

        timeLog.log(start, "deleteExtension");
    }

    public void deleteExtension(Collection<String> extNames) {
        long start = TimeLog.getTime();
        wikittyService.deleteExtension(securityToken, extNames);

        timeLog.log(start, "deleteExtension<list>");
    }

    /**
     * Return all extension id (ex: "extName[version])").
     * 
     * @return extension id list
     */
    public List<String> getAllExtensionIds() {
        long start = TimeLog.getTime();
        List<String> result = wikittyService.getAllExtensionIds(securityToken);
        
        timeLog.log(start, "getAllExtensionIds");
        return result;
    }
    
    /**
     * Return all extension id (ex: "extName[version])") where
     * {@code extensionName} is required.
     * 
     * @param extensionName extension name
     * @return extensions
     */
    public List<String> getAllExtensionsRequires(String extensionName) {
        long start = TimeLog.getTime();
        List<String> result = wikittyService.getAllExtensionsRequires(securityToken, extensionName);
        
        timeLog.log(start, "getAllExtensionsRequires");
        return result;
    }

    /**
     * Use with caution : It will delete ALL indexes from search engine !
     * This operation should be disabled in production environment.
     */
    public WikittyEvent clear() {
        long start = TimeLog.getTime();
        WikittyEvent result = wikittyService.clear(securityToken);
        
        timeLog.log(start, "clear");
        return result;
    }

    /**
     * Synchronize search engine with wikitty storage engine, i.e. clear and
     * reindex all object.
     */
    public void syncSearchEngine() {
        long start = TimeLog.getTime();
        wikittyService.syncSearchEngine(securityToken);
        
        timeLog.log(start, "syncSearchEngine");
    }

    /**
     * Method to get the Wikitty encapsulated into a BusinessEntity
     * @param entity the BusinessEntity encapsulating the Wikitty
     * @return the wikitty encapsulated
     */
    public Wikitty getWikitty(BusinessEntity entity){
        long start = TimeLog.getTime();
        Wikitty result = WikittyUtil.getWikitty(wikittyService, securityToken, entity);
        
        timeLog.log(start, "getWikitty");
        return result;
    }

    /**
     * Check that the logged in user is in a group. A #SecurityException might
     * be thrown at runtime if the #WikittyUser session timed out.
     * @param groupName the name of the group to check
     * @return true is the logged in user is in the group
     */
    public boolean isMember(String groupName) {
        long start = TimeLog.getTime();
        boolean result = false;

        WikittyUser user = getLoggedInUser();

        //Find the group from its name
        Search search = Search.query()
                .eq(Element.ELT_EXTENSION, WikittyGroup.EXT_WIKITTYGROUP)
                .eq(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, groupName);
        Criteria criteria = search.criteria();

        Wikitty group = findByCriteria(criteria);

        if (group != null && user != null) {
            result = WikittySecurityUtil.isMember(wikittyService, securityToken,
                    user.getWikittyId(), group.getWikittyId());
        }

        timeLog.log(start, "isMember");
        return result;
    }

    /**
     * Get the #WikittyUser that is logged in. A #SecurityException might be
     * thrown at runtime if the #WikittyUser session timed out.
     * @return the logged in #WikittyUser
     */
    public WikittyUser getLoggedInUser() {
        long start = TimeLog.getTime();

        String userId = WikittySecurityUtil.getUserForToken(wikittyService,
                securityToken);

        WikittyUser user = restore(WikittyUser.class, userId);

        timeLog.log(start, "getLoggedInUser");
        return user;
    }

}
