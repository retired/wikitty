/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.ObjectUtil;

/**
 * Point d'entre de wikitty, permet de recuperer un WikittyService pour 
 * travailler
 *
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceFactory {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceFactory.class);

    /**
     * Construit l'enchainement des differents WikittyService comme decrit
     * dans le fichier de configuration via la cle:
     * wikitty.WikittyService.components
     * <p>
     * Chaque composant de l'enchainement peut avoir lui aussi ses propres
     * composants dans une cle:
     * wikitty.[nom simple de la classe].components     *
     *
     * @param config
     * @return
     */
    static public WikittyService buildWikittyService(ApplicationConfig config) {
        String prefix = "wikitty.";
        WikittyService result = buildWikittyService(config, prefix);
        return result;
    }

    /**
     * Construit l'enchainement des differents WikittyService comme decrit
     * dans le fichier de configuration via la cle:
     * wikitty.transaction.WikittyService.components
     * <p>
     * Chaque composant de l'enchainement peut avoir lui aussi ses propres
     * composants dans une cle:
     * wikitty.transaction.[nom simple de la classe].components     *
     *
     * @param config
     * @return
     */
    static public WikittyService buildWikittyServiceTransaction(ApplicationConfig config) {
        String prefix = "wikitty.transaction.";
        WikittyService result = buildWikittyService(config, prefix);
        return result;
    }

    static public WikittyService buildWikittyService(
            ApplicationConfig config, String prefix) {

        WikittyService result = null;
        List<Class> layers = getComponents(config, prefix, WikittyService.class);

        Set comp = new HashSet();
        for (Class<WikittyService> clazz : layers) {
            comp.clear();
            comp.add(result);
            comp.add(config);
            List<Class> comps = getComponents(config, prefix, clazz);
            if (comps != null) {
                comp.addAll(comps);
            }

            result = ObjectUtil.newInstance(clazz, comp, true);
        }
        return result;
    }

    /**
     * Recherche dans la config une key de la forme
     * <pre>
     * wikitty.[nom simple de la classe].components
     * </pre>
     * Par exemple pour org.nuiton.wikitty.WikittyService le nom simple est
     * WikittyService.
     * La valeur doit contenir une liste de classes separer par des ','.
     *
     * @param config
     * @param clazz
     * @return la liste de classe trouvee ou null si la cle n'existe pas
     */
    static protected List<Class> getComponents(ApplicationConfig config,
                                               String prefix, Class clazz) {

        String key = prefix + clazz.getSimpleName() + ".components";
        
        // XXX sletellier 13/12/2010 : this code don't compile, provoke an "inconvertible types",
        // this hack force compiler to not check cast
//        List<Class<E>> result = (List<Class<E>>) config.getOptionAsList(key).getOptionAsClass();
        List<Class> result = (List<Class>)config.getOptionAsList(key).getOptionAsClass();

//        String componentsString = config.getOption(key);
//        if (componentsString != null) {
//            String[] componentsList = componentsString.split(",");
//            result = new ArrayList<Class>();
//            for(String className : componentsList) {
//                try {
//                    Class c = Class.forName(className);
//                    result.add(c);
//                } catch (ClassNotFoundException eee) {
//                    throw new WikittyException(String.format(
//                            "Can't find class %s, check your configuration and"
//                            + " your jar file available", className), eee);
//                }
//            }
//        }
        return result;
    }
}
