package org.nuiton.wikitty.query.function;

/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.storage.WikittySearchEngine;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FunctionDistinct extends WikittyQueryFunction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(FunctionDistinct.class);

    public FunctionDistinct(WikittyQueryFunction... args) {
        super("Distinct", null, Arrays.asList(args));
    }

    public FunctionDistinct(String methodName, String name, List<WikittyQueryFunction> args) {
        super(methodName, name, args);
    }

    @Override
    public List<Map<String, Object>> call(
            WikittyQuery query, List<Map<String, Object>> data) {

        List<List<Map<String, Object>>> extractedData =
                new ArrayList<List<Map<String, Object>>>();

        for (WikittyQueryFunction f : getArgs()) {
            List<Map<String, Object>> r = f.call(query, data);
            extractedData.add(r);
        }
        
        List<Map<String, Object>> result = fusion(extractedData);

        // on collecte tous les doublons
        Object[] all = result.toArray();
        // on utilise un set, car il ne faut pas supprimer plusieurs fois le meme element
        Set<Integer> toRemove = new HashSet<Integer>();
        for (int i=0; i<all.length; i++) {
            for (int j=i+1; j<all.length; j++) {
                if (all[i].equals(all[j])) {
                    toRemove.add(j);
                }
            }
        }

        // on supprime les doublons
        List<Integer> index = new ArrayList<Integer>(toRemove);
        Collections.sort(index);
        Collections.reverse(index);
        for (int i : index) {
            result.remove(i);
        }

        return result;
    }

}
