/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.query.conditions.And;
import org.nuiton.wikitty.query.conditions.Between;
import org.nuiton.wikitty.query.conditions.ConditionValueString;
import org.nuiton.wikitty.query.conditions.ContainsAll;
import org.nuiton.wikitty.query.conditions.ContainsOne;
import org.nuiton.wikitty.query.conditions.Equals;
import org.nuiton.wikitty.query.conditions.False;
import org.nuiton.wikitty.query.conditions.Greater;
import org.nuiton.wikitty.query.conditions.GreaterOrEquals;
import org.nuiton.wikitty.query.conditions.Keyword;
import org.nuiton.wikitty.query.conditions.Less;
import org.nuiton.wikitty.query.conditions.LessOrEquals;
import org.nuiton.wikitty.query.conditions.Like;
import org.nuiton.wikitty.query.conditions.Not;
import org.nuiton.wikitty.query.conditions.NotEquals;
import org.nuiton.wikitty.query.conditions.NotNull;
import org.nuiton.wikitty.query.conditions.Null;
import org.nuiton.wikitty.query.conditions.Or;
import org.nuiton.wikitty.query.conditions.Select;
import org.nuiton.wikitty.query.conditions.True;
import org.nuiton.wikitty.query.conditions.Unlike;
import org.nuiton.wikitty.query.function.FunctionFieldValue;
import org.nuiton.wikitty.query.function.FunctionValue;
import org.nuiton.wikitty.query.function.WikittyQueryFunction;

/**
 * Genere un texte representant une query tel qu'un homme pourrait l'ecrire
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyQueryVisitorToString extends WikittyQueryVisitor {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyQueryVisitorToString.class);
    
    public String text = "";

    public String getText() {
        return text;
    }

    @Override
    public boolean visitEnter(WikittyQuery o) {
        text += o.getName() + "(";
        return true;
    }

    @Override
    public void visitLeave(WikittyQuery o, boolean enterResult) {
        text += ")";
    }

    @Override
    public boolean visitEnter(And o) {
        text += WikittyQueryParser.BRACKET_OPEN;
        return true;
    }

    @Override
    public boolean visitMiddle(And o) {
        text += " " + WikittyQueryParser.AND + " ";
        return true;
    }

    @Override
    public void visitLeave(And o, boolean enterResult) {
        text += WikittyQueryParser.BRACKET_CLOSE;
    }

    @Override
    public boolean visitEnter(Or o) {
        text += WikittyQueryParser.BRACKET_OPEN;
        return true;
    }

    @Override
    public boolean visitMiddle(Or o) {
        text += " " + WikittyQueryParser.OR + " ";
        return true;
    }

    @Override
    public void visitLeave(Or o, boolean enterResult) {
        text += WikittyQueryParser.BRACKET_CLOSE;
    }

    @Override
    public boolean visitEnter(Select o) {
        text += WikittyQueryParser.SELECT + " ";
        return true;
    }

    @Override
    public boolean visitMiddle(Select o) {
        text += " " + WikittyQueryParser.WHERE + " " + WikittyQueryParser.BRACKET_OPEN;
        return true;
    }

    @Override
    public void visitLeave(Select o, boolean enterOrMiddleResult) {
        text += WikittyQueryParser.BRACKET_CLOSE;
    }

    @Override
    public boolean visitEnter(Not o) {
        text += WikittyQueryParser.NOT + WikittyQueryParser.BRACKET_OPEN;
        return true;
    }

    @Override
    public void visitLeave(Not o, boolean enterResult) {
        text += WikittyQueryParser.BRACKET_CLOSE;
    }

    @Override
    public boolean visitEnter(Between o) {
        text += o.getElement().getValue()
                + WikittyQueryParser.EQUALS + WikittyQueryParser.SQUARE_BRACKET_OPEN;
        return true;
    }

    @Override
    public boolean visitMiddle(Between o) {
        text += " " + WikittyQueryParser.TO + " ";
        return true;
    }

    @Override
    public void visitLeave(Between o, boolean enterOrMiddleResult) {
        text += WikittyQueryParser.SQUARE_BRACKET_CLOSE;
    }

    @Override
    public void visit(ConditionValueString o) {
        text += WikittyQueryParser.LITERAL_OPEN_DOUBLE + o.getValue() + WikittyQueryParser.LITERAL_CLOSE_DOUBLE;
    }

    @Override
    public boolean visitEnter(ContainsAll o) {
        text += o.getElement().getValue()
                + WikittyQueryParser.EQUALS + WikittyQueryParser.SQUARE_BRACKET_OPEN;
        return true;
    }

    @Override
    public boolean visitMiddle(ContainsAll o) {
        text += WikittyQueryParser.COMMA;
        return true;
    }

    @Override
    public void visitLeave(ContainsAll o, boolean enterOrMiddleResult) {
        text += WikittyQueryParser.SQUARE_BRACKET_CLOSE;
    }

    @Override
    public boolean visitEnter(ContainsOne o) {
        text += o.getElement().getValue()
                + WikittyQueryParser.EQUALS + WikittyQueryParser.CURLY_BRACKET_OPEN;
        return true;
    }

    @Override
    public boolean visitMiddle(ContainsOne o) {
        text += WikittyQueryParser.COMMA;
        return true;
    }

    @Override
    public void visitLeave(ContainsOne o, boolean enterOrMiddleResult) {
        text += WikittyQueryParser.CURLY_BRACKET_CLOSE;
    }

    @Override
    public boolean visitEnter(Equals o) {
        text += o.getElement().getValue();
        if (o.isIgnoreCaseAndAccent()) {
            text += WikittyQueryParser.EQUALS_IGNORE_CASE_AND_ACCENT;
        } else {
            text += WikittyQueryParser.EQUALS;
        }
        return true;
    }

    @Override
    public void visitLeave(Equals o, boolean enterOrMiddleResult) {
        // nothing to do
    }

    @Override
    public void visit(False o) {
        text += WikittyQueryParser.FALSE;
    }

    @Override
    public boolean visitEnter(Greater o) {
        text += o.getElement().getValue()
                + WikittyQueryParser.GREATER;
        return true;
    }

    @Override
    public void visitLeave(Greater o, boolean enterOrMiddleResult) {
                // nothing to do
    }

    @Override
    public boolean visitEnter(GreaterOrEquals o) {
        text += o.getElement().getValue()
                + WikittyQueryParser.GREATER_OR_EQUALS;
        return true;
    }

    @Override
    public void visitLeave(GreaterOrEquals o, boolean enterOrMiddleResult) {
        // nothing to do
    }

    @Override
    public boolean visitEnter(Keyword o) {
        // nothing to do
        return true;
    }

     @Override
    public void visitLeave(Keyword o, boolean enterOrMiddleResult) {
        // nothing to do
    }

    @Override
    public boolean visitEnter(Less o) {
        text += o.getElement().getValue()
                + WikittyQueryParser.LESS;
        return true;
    }

    @Override
    public void visitLeave(Less o, boolean enterOrMiddleResult) {
        // nothing to do
   }

    @Override
    public boolean visitEnter(LessOrEquals o) {
        text += o.getElement().getValue()
                + WikittyQueryParser.LESS_OR_EQUALS;
        return true;
    }

    @Override
    public void visitLeave(LessOrEquals o, boolean enterOrMiddleResult) {
        // nothing to do
    }

    @Override
    public boolean visitEnter(Like o) {
        text += o.getElement().getValue()
                + " " + WikittyQueryParser.LIKE + " ";
        return true;
    }

    @Override
    public void visitLeave(Like o, boolean enterOrMiddleResult) {
        // nothing to do
    }

    @Override
    public boolean visitEnter(Unlike o) {
        text += o.getElement().getValue()
                + " " + WikittyQueryParser.UNLIKE + " ";
        return true;
    }

    @Override
    public void visitLeave(Unlike o, boolean enterOrMiddleResult) {
        // nothing to do
    }

    @Override
    public boolean visitEnter(NotEquals o) {
        text += o.getElement().getValue();
        if (o.isIgnoreCaseAndAccent()) {
            text += WikittyQueryParser.NOT_EQUALS_IGNORE_CASE_AND_ACCENT;
        } else {
            text += WikittyQueryParser.NOT_EQUALS;
        }
        return true;
    }

    @Override
    public void visitLeave(NotEquals o, boolean enterOrMiddleResult) {
        // nothing to do
    }

    @Override
    public void visit(Null o) {
        text += o.getElement().getValue()
                + WikittyQueryParser.EQUALS + WikittyQueryParser.NULL;
    }

    @Override
    public void visit(NotNull o) {
        text += o.getElement().getValue()
                + WikittyQueryParser.NOT_EQUALS + WikittyQueryParser.NULL;
    }

    @Override
    public void visit(True o) {
        text += WikittyQueryParser.TRUE;
    }

    @Override
    public boolean visitEnter(WikittyQueryFunction function) {
        text += function.getMethodName() + WikittyQueryParser.BRACKET_OPEN;
        return true;
    }

    @Override
    public boolean visitMiddle(WikittyQueryFunction function) {
        text += WikittyQueryParser.COMMA;
        return true;
    }

    @Override
    public void visitLeave(WikittyQueryFunction function, boolean enterOrMiddleResult) {
        text +=  WikittyQueryParser.BRACKET_CLOSE;
        if (StringUtils.isNotBlank(function.getName())) {
            text += " " + WikittyQueryParser.AS + " " + function.getName();
        }
    }

    @Override
    public void visit(FunctionValue function) {
        text += '"' + String.valueOf(function.getValue()).replaceAll("\"", "\\\"") + '"';
    }

    @Override
    public void defaultVisit(Object o) {
        throw new UnsupportedOperationException("Not supported:" + o.getClass());
    }

    @Override
    public boolean defaultVisitEnter(Object o) {
        throw new UnsupportedOperationException("Not supported:" + o.getClass());
    }

    @Override
    public boolean defaultVisitMiddle(Object o) {
        throw new UnsupportedOperationException("Not supported:" + o.getClass());
    }

    @Override
    public void defaultVisitLeave(Object o, boolean enterResult) {
        throw new UnsupportedOperationException("Not supported:" + o.getClass());
    }
}
