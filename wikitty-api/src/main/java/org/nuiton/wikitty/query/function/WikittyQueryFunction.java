package org.nuiton.wikitty.query.function;

/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.ObjectUtil;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryVisitor;
import org.nuiton.wikitty.query.WikittyQueryVisitorToString;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.storage.WikittySearchEngine;

/**
 * Herite de Element et utilise value pour stocker la methode a appeler
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public abstract class WikittyQueryFunction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyQueryFunction.class);

    private static final long serialVersionUID = 1L;

    /** le nom de la methode a appeler */
    protected String methodName;
    /** le nom que l'utilisateur a donné au resultat: ex: Sum(amount) as Toto */
    protected String name;
    /** la liste des arguments de la fonction */
    protected List<WikittyQueryFunction> args = new ArrayList<WikittyQueryFunction>();
    /** le nombre d'argument que la function peut prendre. Integer.MAX_VALUE = infini */
    protected int numArg = Integer.MAX_VALUE;

    abstract public List<Map<String, Object>> call(
            WikittyQuery query, List<Map<String, Object>> data);

    static public WikittyQueryFunction createFusionIfNeeded(List<WikittyQueryFunction> args) {
        WikittyQueryFunction result;
        if (args.size() == 1) {
            result = args.get(0);
        } else {
            result = new FunctionFusion(args);
        }
        return result;
    }
    
    static public WikittyQueryFunction create(String methodName, String name, List<WikittyQueryFunction> args) {
        WikittyQueryFunction result;
        if ("fieldValue".equalsIgnoreCase(methodName)) {
            result = new FunctionFieldValue(methodName, name, args);
        } else if ("Fusion".equalsIgnoreCase(methodName)) {
            result = new FunctionFusion(methodName, name, args);
        } else if ("sum".equalsIgnoreCase(methodName)) {
            result = new FunctionSum(methodName, name, args);
        } else if ("min".equalsIgnoreCase(methodName)) {
            result = new FunctionMin(methodName, name, args);
        } else if ("max".equalsIgnoreCase(methodName)) {
            result = new FunctionMax(methodName, name, args);
        } else if ("avg".equalsIgnoreCase(methodName)) {
            result = new FunctionAvg(methodName, name, args);
        } else if ("count".equalsIgnoreCase(methodName)) {
            result = new FunctionCount(methodName, name, args);
        } else if ("distinct".equalsIgnoreCase(methodName)) {
            result = new FunctionDistinct(methodName, name, args);
        } else if ("toBigDecimal".equalsIgnoreCase(methodName)) {
            result = new WikittyQueryFunctionWrapper(WikittyUtil.class.getName() + "#toBigDecimal", name, args);
        } else if ("toBoolean".equalsIgnoreCase(methodName)) {
            result = new WikittyQueryFunctionWrapper(WikittyUtil.class.getName() + "#toBoolean", name, args);
        } else if ("toString".equalsIgnoreCase(methodName)) {
            result = new WikittyQueryFunctionWrapper(WikittyUtil.class.getName() + "#toString", name, args);
        } else if ("toDate".equalsIgnoreCase(methodName)) {
            result = new WikittyQueryFunctionWrapper(WikittyUtil.class.getName() + "#toDate", name, args);
        } else if ("year".equalsIgnoreCase(methodName)) {
            result = new FunctionYear(methodName, name, args);
        } else {
            if (StringUtils.contains(methodName, ObjectUtil.CLASS_METHOD_SEPARATOR)) {
                // c'est une methode quelconque.
                result = new WikittyQueryFunctionWrapper(methodName, name, args);
            } else {
                try {
                    // c'est peut-etre une classe qui herite de WikittyQueryFunction
                    Class c = Class.forName(methodName);
                    result = (WikittyQueryFunction)ConstructorUtils.invokeConstructor(c,
                            new Object[]{methodName, name, args},
                            new Class[]{String.class, String.class, List.class});
                } catch (Exception eee) {
                    throw new WikittyException(String.format(
                            "Can't instantiate function '%s'", methodName), eee);
                }
            }
        }

        return result;
    }

    public WikittyQueryFunction(String methodName, String name, List<WikittyQueryFunction> args) {
        this.methodName = methodName;
        if (name == null) {
            this.name = UUID.randomUUID().toString();
        } else {
            this.name = name;
        }

        addArgs(args);
    }

    @Override
    public boolean equals(Object o) {
        boolean result;
        if (o == null) {
            result = false;
        } else if (this == o) {
            result = true;
        }else if (this.getClass().equals(o.getClass())) {
            WikittyQueryFunction other = (WikittyQueryFunction)o;
            result = StringUtils.equals(this.getMethodName(), other.getMethodName())
                    // TODO a revoir
                    // name est genere pour etre unique s'il n'est pas fixe, donc on ne peut pas faire de check dessus
                    // est-ce que laisser null, si name n'est pas fixer ne serait pas preferable ? Est-ce que les map accept null comme cle ?
//                    && StringUtils.equals(this.getName(), other.getName())
                    && ObjectUtils.equals(this.getArgs(), other.getArgs());
        } else {
            return false;
        }
        return result;
    }

    public void accept(WikittyQueryVisitor visitor) {
        boolean walk = visitor.visitEnter(this);
        if (walk && args != null) {
            boolean notFirst = false;
            for(WikittyQueryFunction a : args) {
                if (notFirst) {
                    walk = visitor.visitMiddle(this);
                    if (!walk) {
                        // le visiteur demande l'arret de la visite
                        break;
                    }
                } else {
                    notFirst = true;
                }
                a.accept(visitor);
            }
        }
        visitor.visitLeave(this, walk);
    }

    public String getName() {
        return name;
    }

    public String getMethodName() {
        return methodName;
    }

    public List<WikittyQueryFunction> getArgs() {
        return args;
    }

    public void addArgs(List<WikittyQueryFunction> args) {
        if (args != null) {
            if (this.args.size() + args.size() <= getNumArg()) {
                this.args.addAll(args);
            } else {
                throw new IllegalStateException(String.format(
                        "This function '%s' can't take more arguments than %s, currently '%s', you try to add %s arguments",
                        this.getClass().getSimpleName(), getNumArg(), this.args.size(), args.size()));
            }
        }
    }

    public void addArgs(WikittyQueryFunction arg) {
        if (acceptMoreArgs()) {
            args.add(arg);
        } else {
            throw new IllegalStateException(String.format(
                    "This function '%s' can't take more arguments, currently '%s'",
                    this.getClass().getSimpleName(), args.size()));
        }
    }

    public int getNumArg() {
        return numArg;
    }

    public boolean acceptMoreArgs() {
        boolean result = args.size() < getNumArg();
        return result;
    }

    @Override
    public String toString() {
        WikittyQueryVisitorToString v = new WikittyQueryVisitorToString();
        accept(v);
        String result = v.getText();
        return result;
    }



    protected List<Map<String, Object>> fusion(List<List<Map<String, Object>>> listData) {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        // check size, all list must have same size
        int size = -1;
        for (List<Map<String, Object>> e : listData) {
            if (size == -1) {
                size = e.size();
            } else {
                if (size != e.size()) {
                    throw new IllegalArgumentException(String.format(
                            "Each list must have same size %s != %s (%s)",
                            size, e.size(), listData));
                }
            }
        }

        // init des maps
        for (int i=0; i<size; i++) {
            result.add(new LinkedHashMap<String, Object>());
        }

        // ajout de chaque map dans la map du resultat
        for (List<Map<String, Object>> e : listData) {
            int i=0;
            for (Map<String, Object> m : e) {
                Map<String, Object> r = result.get(i++);
                r.putAll(m);
            }

        }
        
        return result;
    }

    protected Object getUniqueValue(List<Map<String, Object>> data) {
        Object result = null;
        if (data.size() != 1) {
            throw new IllegalStateException("Data don't contains 1 value exactly");
        }
        for (Map<String, Object> o : data) {
            if (o.size() != 1) {
                throw new IllegalStateException("Map don't contains 1 value exactly");
            } else {
                for (Object s : o.values()) {
                    result = s;
                }
            }
        }
        return result;
    }

    protected Object getUniqueValue(Map<String, Object> o) {
        Object result = null;
        if (o.size() != 1) {
            throw new IllegalStateException("Map don't contains 1 value exactly");
        } else {
            for (Object s : o.values()) {
                result = s;
            }
        }
        return result;
    }

    protected Object getFirstFieldName(List<Map<String, Object>> data) {
        for (Map<String, Object> o : data) {
            for (Object s : o.keySet()) {
                return s;
            }
        }
        return null;
    }

    /**
     *
     * @param fqMethod une methode d du style le.package.objet#method
     * @return
     */
    protected Method getMethod(String fqMethod) {
        List<Method> methods = ObjectUtil.getMethod(fqMethod, true);

        Method result;
        if (methods.isEmpty()) {
            throw new IllegalArgumentException(String.format(
                    "Can't find method '%s'", fqMethod));
        } else {
            if (methods.size() > 1) {
                log.warn(String.format(
                        "More than one method found for '%s', used the first: %s",
                        fqMethod, methods));
            }
            result = methods.get(0);
        }
        return result;
    }

}
