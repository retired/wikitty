package org.nuiton.wikitty.query.function;

/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.query.ListObjectOrMap;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.storage.WikittySearchEngine;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FunctionSum extends WikittyQueryFunction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(FunctionSum.class);

    public FunctionSum(String name) {
        super("Sum", name, null);
    }

    public FunctionSum(String name, WikittyQueryFunction arg) {
        super("Sum", name, Collections.singletonList(arg));
    }

    public FunctionSum(String methodName, String name, List<WikittyQueryFunction> args) {
        super(methodName, name, args);
        if (args.size() != 1) {
            throw new IllegalArgumentException("Sum accept only one argument");
        }
    }

    @Override
    public int getNumArg() {
        return 1;
    }

    @Override
    public List<Map<String, Object>> call(
            WikittyQuery query, List<Map<String, Object>> data) {

        WikittyQueryFunction f = getArgs().get(0);
        data = f.call(query, data);

        BigDecimal total = new BigDecimal(0);
        for (Map<String, Object> o : data) {
            if (o.size() > 1) {
                throw new IllegalStateException("Sum function can't take data, with more than one field");
            } else {
                 // on fait un for mais en fait, il n'y a 0 ou 1 element.
                // Mais je ne vois pas de solution plus simple a ecrire
                for (Object s : o.values()) {
                    if (s instanceof Collection) {
                        for (Object e : (Collection)s) {
                            BigDecimal v = WikittyUtil.toBigDecimal(e);
                            total = total.add(v);
                        }
                    } else {
                        BigDecimal v = WikittyUtil.toBigDecimal(s);
                        total = total.add(v);
                    }
                }
            }
        }

        ListObjectOrMap result = new ListObjectOrMap();
        result.add(getName(), total);
        return result;
    }
}
