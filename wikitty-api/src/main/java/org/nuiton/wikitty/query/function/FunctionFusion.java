package org.nuiton.wikitty.query.function;

/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.storage.WikittySearchEngine;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FunctionFusion extends WikittyQueryFunction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(FunctionFusion.class);

    public FunctionFusion(WikittyQueryFunction... args) {
        super("Fusion", null, Arrays.asList(args));
    }

    public FunctionFusion(List<WikittyQueryFunction> args) {
        super("Fusion", null, args);
    }

    public FunctionFusion(String methodName, String name, List<WikittyQueryFunction> args) {
        super(methodName, name, args);
    }

    @Override
    public List<Map<String, Object>> call(
            WikittyQuery query, List<Map<String, Object>> data) {

        try {
            List<List<Map<String, Object>>> all = new ArrayList<List<Map<String, Object>>>();
            for (WikittyQueryFunction f : getArgs()) {
                all.add(f.call(query, data));
            }

            List<Map<String, Object>> result = fusion(all);
            return result;
        } catch (Exception eee) {
            throw new WikittyException(String.format(
                    "Can't evaluate function %s", this), eee);
        }
    }

}
