/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query;

import java.lang.reflect.Method;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.query.conditions.And;
import org.nuiton.wikitty.query.conditions.Between;
import org.nuiton.wikitty.query.conditions.ConditionValueString;
import org.nuiton.wikitty.query.conditions.ContainsAll;
import org.nuiton.wikitty.query.conditions.ContainsOne;
import org.nuiton.wikitty.query.conditions.Equals;
import org.nuiton.wikitty.query.conditions.False;
import org.nuiton.wikitty.query.conditions.Greater;
import org.nuiton.wikitty.query.conditions.GreaterOrEquals;
import org.nuiton.wikitty.query.conditions.Keyword;
import org.nuiton.wikitty.query.conditions.Less;
import org.nuiton.wikitty.query.conditions.LessOrEquals;
import org.nuiton.wikitty.query.conditions.Like;
import org.nuiton.wikitty.query.conditions.Not;
import org.nuiton.wikitty.query.conditions.NotEquals;
import org.nuiton.wikitty.query.conditions.NotNull;
import org.nuiton.wikitty.query.conditions.Null;
import org.nuiton.wikitty.query.conditions.Or;
import org.nuiton.wikitty.query.conditions.Select;
import org.nuiton.wikitty.query.conditions.True;
import org.nuiton.wikitty.query.conditions.Unlike;
import org.nuiton.wikitty.query.function.FunctionValue;
import org.nuiton.wikitty.query.function.WikittyQueryFunction;

/**
 * Permet d'implanter un visiteur de Query ou Condition. Une fois que vous avez
 * cree votre propre Visiteur pour l'utiliser
 *
 * <pre>
 * WikittyQuery q = ...;
 * MyVisitor v = new MyVisitor();
 * q.accept(v);
 * v.getXXXX()
 * </pre>
 *
 * ATTENTION les sous classes doivent avoir une visibilite public car sinon
 * l'introspection n'arrive pas a executer les methods
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public abstract class WikittyQueryVisitor {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyQueryVisitor.class);

    /**
     * True if we want visit sub-element, otherwize false
     * @param o
     * @return True if we want visit sub-element, otherwize false
     */
    abstract public boolean visitEnter(WikittyQuery o);
    /**
     * Leave method is alway called, but evaluation of enter is passed in
     * arguement
     * 
     * @param o
     * @param enterOrMiddleResult result returned by visitEnter method
     */
    abstract public void visitLeave(WikittyQuery o, boolean enterOrMiddleResult);
    /**
     * True if we want visit sub-element, otherwize false
     * @param o
     * @return True if we want visit sub-element, otherwize false
     */
    abstract public boolean visitEnter(And o);
    /**
     * True if we want visit others elements, otherwize false. If false
     * visitMiddle for this node is never called
     * @param o
     * @return True if we want visit others elements, otherwize false
     */
    abstract public boolean visitMiddle(And o);
    /**
     * Leave method is alway called, but evaluation of enter is passed in
     * arguement
     *
     * @param o
     * @param enterOrMiddleResult result returned by visitEnter method
     */
    abstract public void visitLeave(And o, boolean enterOrMiddleResult);
    /**
     * True if we want visit sub-element, otherwize false
     * visitMiddle for this node is never called
     * @param o
     * @return True if we want visit sub-element, otherwize false
     */
    abstract public boolean visitEnter(Or o);
    /**
     * True if we want visit others elements, otherwize false
     * @param o
     * @return True if we want visit others elements, otherwize false
     */
    abstract public boolean visitMiddle(Or o);
    /**
     * Leave method is alway called, but evaluation of enter is passed in
     * arguement
     *
     * @param o
     * @param enterOrMiddleResult result returned by visitEnter method
     */
    abstract public void visitLeave(Or o, boolean enterOrMiddleResult);
    /**
     * True if we want visit sub-element, otherwize false
     * @param o
     * @return True if we want visit sub-element, otherwize false
     */
    /**
     * True if we want visit sub-element, otherwize false
     * @param o
     * @return True if we want visit sub-element, otherwize false
     */
    abstract public boolean visitEnter(Select o);
    /**
     * True if we want visit others elements, otherwize false
     * @param o
     * @return True if we want visit others elements, otherwize false
     */
    abstract public boolean visitMiddle(Select o);
    /**
     * Leave method is alway called, but evaluation of enter is passed in
     * arguement
     *
     * @param o
     * @param enterOrMiddleResult result returned by visitEnter method
     */
    abstract public void visitLeave(Select o, boolean enterOrMiddleResult);
    /**
     * True if we want visit sub-element, otherwize false
     * @param o
     * @return True if we want visit sub-element, otherwize false
     */
    abstract public boolean visitEnter(Not o);
    /**
     * Leave method is alway called, but evaluation of enter is passed in
     * arguement
     *
     * @param o
     * @param enterOrMiddleResult result returned by visitEnter method
     */
    abstract public void visitLeave(Not o, boolean enterOrMiddleResult);

    abstract public boolean visitEnter(Between o);
    abstract public boolean visitMiddle(Between o);
    abstract public void visitLeave(Between o, boolean enterOrMiddleResult);
    abstract public boolean visitEnter(ContainsAll o);
    abstract public boolean visitMiddle(ContainsAll o);
    abstract public void visitLeave(ContainsAll o, boolean enterOrMiddleResult);
    abstract public boolean visitEnter(ContainsOne o);
    abstract public boolean visitMiddle(ContainsOne o);
    abstract public void visitLeave(ContainsOne o, boolean enterOrMiddleResult);
    abstract public boolean visitEnter(Equals o);
    abstract public void visitLeave(Equals o, boolean enterOrMiddleResult);
    abstract public boolean visitEnter(NotEquals o);
    abstract public void visitLeave(NotEquals o, boolean enterOrMiddleResult);
    abstract public boolean visitEnter(Greater o);
    abstract public void visitLeave(Greater o, boolean enterOrMiddleResult);
    abstract public boolean visitEnter(GreaterOrEquals o);
    abstract public void visitLeave(GreaterOrEquals o, boolean enterOrMiddleResult);
    abstract public boolean visitEnter(Keyword o);
    abstract public void visitLeave(Keyword o, boolean enterOrMiddleResult);
    abstract public boolean visitEnter(Less o);
    abstract public void visitLeave(Less o, boolean enterOrMiddleResult);
    abstract public boolean visitEnter(LessOrEquals o);
    abstract public void visitLeave(LessOrEquals o, boolean enterOrMiddleResult);
    abstract public boolean visitEnter(Like o);
    abstract public void visitLeave(Like o, boolean enterOrMiddleResult);
    abstract public boolean visitEnter(Unlike o);
    abstract public void visitLeave(Unlike o, boolean enterOrMiddleResult);

    abstract public void visit(Null o);
    abstract public void visit(NotNull o);
    abstract public void visit(False o);
    abstract public void visit(True o);

    abstract public void visit(ConditionValueString o);

    abstract public boolean visitEnter(WikittyQueryFunction function);
    abstract public boolean visitMiddle(WikittyQueryFunction function);
    abstract public void visitLeave(WikittyQueryFunction function, boolean enterOrMiddleResult);
    abstract public void visit(FunctionValue function);

    abstract public void defaultVisit(Object o);
    /**
     * True if we want visit sub-element, otherwize false
     * @param o
     * @return True if we want visit sub-element, otherwize false
     */
    abstract public boolean defaultVisitEnter(Object o);
    /**
     * True if we want visit others elements, otherwize false
     * visitMiddle for this node is never called
     * @param o
     * @return True if we want visit others elements, otherwize false
     */
    abstract public boolean defaultVisitMiddle(Object o);
    /**
     * Leave method is alway called, but evaluation of enter is passed in
     * arguement
     *
     * @param o
     * @param enterOrMiddleResult result returned by visitEnter method
     */
    abstract public void defaultVisitLeave(Object o, boolean enterOrMiddleResult);

    public void visit(Object object) {
        if (log.isDebugEnabled()) {
            log.debug(String.format("Visit '%s'",
                    ClassUtils.getShortCanonicalName(object, "null")));
        }
        try {
            Method downPolymorphic = this.getClass().getMethod("visit",
                    new Class[] { object.getClass() });

            if (downPolymorphic == null) {
                defaultVisit(object);
            } else {
                try {
                    downPolymorphic.invoke(this, new Object[] {object});
                } catch (Exception eee) {
                    throw new WikittyException(String.format(
                            "Error during visitor call method '%s'",
                            downPolymorphic), eee);
                }
            }
        }
        catch (NoSuchMethodException eee) {
            log.debug("Can't call specific visit method, call defaultVisit", eee);
            this.defaultVisit(object);
        }
    }
    public boolean visitEnter(Object object) {
        if (log.isDebugEnabled()) {
            log.debug(String.format("VisitEnter '%s'",
                    ClassUtils.getShortCanonicalName(object, "null")));
        }
        boolean result;
        try {
            Method downPolymorphic = this.getClass().getMethod("visitEnter",
                    new Class[] { object.getClass() });

            if (downPolymorphic == null) {
                result = defaultVisitEnter(object);
            } else {
                try {
                    result = (Boolean)downPolymorphic.invoke(this, new Object[] {object});
                } catch (Exception eee) {
                    throw new WikittyException(String.format(
                            "Error during visitor call method '%s'",
                            downPolymorphic), eee);
                }
            }
        }
        catch (NoSuchMethodException eee) {
            log.debug("Can't call specific visit method, call defaultVisitEnter", eee);
            result = defaultVisitEnter(object);
        }
        return result;
    }
    
    public boolean visitMiddle(Object object) {
        if (log.isDebugEnabled()) {
            log.debug(String.format("VisitMiddle '%s'",
                    ClassUtils.getShortCanonicalName(object, "null")));
        }
        boolean result;
        try {
            Method downPolymorphic = this.getClass().getMethod("visitMiddle",
                    new Class[] { object.getClass() });

            if (downPolymorphic == null) {
                result = defaultVisitMiddle(object);
            } else {
                try {
                    result = (Boolean)downPolymorphic.invoke(this, new Object[] {object});
                } catch (Exception eee) {
                    throw new WikittyException(String.format(
                            "Error during visitor call method '%s'",
                            downPolymorphic), eee);
                }
            }
        }
        catch (NoSuchMethodException eee) {
            log.debug("Can't call specific visit method, call defaultVisitMiddle", eee);
            result = defaultVisitMiddle(object);
        }
        return result;
    }

    public void visitLeave(Object object, boolean enterOrMiddleResult) {
        if (log.isDebugEnabled()) {
            log.debug(String.format("VisitLeave '%s'",
                    ClassUtils.getShortCanonicalName(object, "null")));
        }
        try {
            Method downPolymorphic = this.getClass().getMethod("visitLeave",
                    new Class[] { object.getClass(), Boolean.TYPE });

            if (downPolymorphic == null) {
                defaultVisitLeave(object, enterOrMiddleResult);
            } else {
                try {
                    downPolymorphic.invoke(this, new Object[] {object, enterOrMiddleResult});
                } catch (Exception eee) {
                    throw new WikittyException(String.format(
                            "Error during visitor call method '%s'",
                            downPolymorphic), eee);
                }
            }
        }
        catch (NoSuchMethodException eee) {
            log.error("Can't call specific visit method, call defaultVisitLeave", eee);
            this.defaultVisitLeave(object, enterOrMiddleResult);
        }
    }

}
