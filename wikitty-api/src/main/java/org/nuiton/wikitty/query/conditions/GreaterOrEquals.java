/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.query.WikittyQueryMaker;

/**
 * GreatOrEqual operator is used to build restriction containing "element &gt;=
 * value" where element could be a Integer, a Float, a Date, ... (must be Comparable)<br>
 * <br>
 * For example, use: {@link WikittyQueryMaker}.greatEq( myElement, new Date())
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class GreaterOrEquals extends TerminalBinaryOperator {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    public GreaterOrEquals(Element element) {
        super(element);
    }

    public GreaterOrEquals(Element element, String value) {
        super(element, value);
    }

    public GreaterOrEquals(Element element, ConditionValue value) {
        super(element, value);
    }

}
