/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import org.nuiton.wikitty.entities.Element;
import org.apache.commons.lang3.ObjectUtils;
import org.nuiton.wikitty.query.WikittyQueryVisitor;
import org.nuiton.wikitty.query.function.WikittyQueryFunction;

/**
 * Cette condition n'accept pas d'etre utilise dans une autre condition.
 * Il sert a demander l'extraction d'un autre champs que l'id. Si la valeur
 * du champs est retrouvee plusieurs fois, cette valeur n'apparaitra qu'une
 * seule fois dans les resultats (comme 'DISTINCT' en SQL)
 * <p>
 * Il y a donc deux types d'utilisation possible
 * 
 * <pre>
 * SELECT extName.fieldName WHERE condition
 * condition ... IN (SELECT extName.fieldName WHERE other condition)
 * </pre>
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class Select extends ContainerUnaryOperator implements ConditionValue{

    private static final long serialVersionUID = 1L;

    protected WikittyQueryFunction function;

    public Select(WikittyQueryFunction function) {
        this.function = function;
    }

    public Select(WikittyQueryFunction function, Condition restriction) {
        super(restriction);
        this.function = function;
    }

    public WikittyQueryFunction getFunction() {
        return function;
    }

    public void setFunction(WikittyQueryFunction function) {
        this.function = function;
    }

    @Override
    boolean equalsDeep(Object other) {
        boolean result = super.equalsDeep(other);
        if (result) {
            Select op = (Select)other;
            result = ObjectUtils.equals(this.getFunction(), op.getFunction());
        }
        return result;
    }

    @Override
    public void accept(WikittyQueryVisitor visitor) {
        boolean walk = visitor.visitEnter(this);
        if (walk && function != null) {
            function.accept(visitor);
        }
        walk = visitor.visitMiddle(this);
        if (walk && subCondition != null) {
            subCondition.accept(visitor);
        }
        visitor.visitLeave(this, walk);
    }


}
