/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.query.conditions.And;
import org.nuiton.wikitty.query.conditions.Between;
import org.nuiton.wikitty.query.conditions.Condition;
import org.nuiton.wikitty.query.conditions.ConditionValueString;
import org.nuiton.wikitty.query.conditions.ContainsAll;
import org.nuiton.wikitty.query.conditions.ContainsOne;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.query.conditions.Equals;
import org.nuiton.wikitty.query.conditions.False;
import org.nuiton.wikitty.query.conditions.Greater;
import org.nuiton.wikitty.query.conditions.GreaterOrEquals;
import org.nuiton.wikitty.query.conditions.Keyword;
import org.nuiton.wikitty.query.conditions.Less;
import org.nuiton.wikitty.query.conditions.LessOrEquals;
import org.nuiton.wikitty.query.conditions.Like;
import org.nuiton.wikitty.query.conditions.Not;
import org.nuiton.wikitty.query.conditions.NotEquals;
import org.nuiton.wikitty.query.conditions.NotNull;
import org.nuiton.wikitty.query.conditions.Null;
import org.nuiton.wikitty.query.conditions.Or;
import org.nuiton.wikitty.query.conditions.Select;
import org.nuiton.wikitty.query.conditions.True;
import org.nuiton.wikitty.query.conditions.Unlike;
import org.nuiton.wikitty.query.function.FunctionValue;
import org.nuiton.wikitty.query.function.WikittyQueryFunction;

/**
 * This visitor make a deep copy of WikittyQuery.
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 * @see WikittyQuery#copy()
 */
public class WikittyQueryVisitorCopy extends WikittyQueryVisitor {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyQueryVisitorCopy.class);

    protected WikittyQueryMaker queryMaker;
    protected List<WikittyQueryFunction> functions = new ArrayList<WikittyQueryFunction>();

    /**
     * Prend le nombre d'arg passe en parametre et les supprime de la list
     * @param size le nombre de parametre a prendre
     * @return
     */
    protected List<WikittyQueryFunction> getAndClearFunction(int size) {
        int last = functions.size();
        List<WikittyQueryFunction> tmp = functions.subList(last-size, last);
        List<WikittyQueryFunction> result = new ArrayList<WikittyQueryFunction>(tmp);
        tmp.clear();
        return result;
    }
    protected void addFunction(WikittyQueryFunction f) {
        functions.add(f);
    }

    protected WikittyQueryMaker getQueryMaker() {
        if (queryMaker == null) {
            queryMaker = new WikittyQueryMaker();
        }
        return queryMaker;
    }

    public WikittyQuery getQuery() {
        WikittyQuery result = getQueryMaker().getQuery();
        return result;
    }
    
    public Condition getCondition() {
        Condition result = getQueryMaker().getCondition();
        return result;
    }

    @Override
    public boolean visitEnter(WikittyQuery o) {
        WikittyQuery q = getQuery();
        q.setFacetExtension(q.isFacetExtension());
        q.setFacetField(new ArrayList<Element>(o.getFacetField()));
        q.setFacetLimit(o.getFacetLimit());
        q.setFacetMinCount(o.getFacetMinCount());
        q.setOffset(o.getOffset());
        q.setLimit(o.getLimit());
        q.setFacetSort(o.getFacetSort());
        q.setName(o.getName());
        q.setSortAscending(new ArrayList<Element>(o.getSortAscending()));
        q.setSortDescending(new ArrayList<Element>(o.getSortDescending()));
        q.setWikittyFieldSearchDepth(o.getWikittyFieldSearchDepth());

        for (FacetQuery c : o.getFacetQuery()) {
            WikittyQueryVisitorCopy v = new WikittyQueryVisitorCopy();
            c.getCondition().accept(v);
            Condition condition = v.getCondition();
            q.addFacetQuery(c.getName(), condition);
        }
        return true;
    }

    @Override
    public void visitLeave(WikittyQuery o, boolean enterResult) {
    }

    @Override
    public void visit(ConditionValueString o) {
        // not copy o, beacause o is immutable
        getQueryMaker().value(o);
    }

    @Override
    public boolean visitEnter(And o) {
        getQueryMaker().and();
        return true;
    }

    @Override
    public boolean visitMiddle(And o) {
        return true;
    }

    @Override
    public void visitLeave(And o, boolean enterResult) {
        getQueryMaker().close();
    }

    @Override
    public boolean visitEnter(Or o) {
        getQueryMaker().or();
        return true;
    }

    @Override
    public boolean visitMiddle(Or o) {
        return true;
    }

    @Override
    public void visitLeave(Or o, boolean enterResult) {
        getQueryMaker().close();
    }

    @Override
    public boolean visitEnter(Select o) {
        return true;
    }

    @Override
    public boolean visitMiddle(Select o) {
        WikittyQueryFunction f = null;
        if (o.getFunction() != null) {
            List<WikittyQueryFunction> fs = getAndClearFunction(1);
            // normalement il ne devrait rester qu'une element dans functions et on
            // vient de le prendre, si ce n'est pas le cas, c'est un probleme
            if (!functions.isEmpty()) {
                throw new IllegalStateException("function list must be empty here");
            }
            f = fs.get(0);
        }
        getQueryMaker().select(f);
        getQueryMaker().where();
        return true;
    }

    @Override
    public void visitLeave(Select o, boolean enterOrMiddleResult) {
        // do nothing, close is automatic
    }

    @Override
    public boolean visitEnter(Not o) {
        getQueryMaker().not();
        return true;
    }

    @Override
    public void visitLeave(Not o, boolean enterResult) {
        // do nothing, close is automatic
    }

    @Override
    public boolean visitEnter(Between o) {
        getQueryMaker().bw(o.getElement());
        return true;
    }

    @Override
    public boolean visitMiddle(Between o) {
        // do nothing
        return true;
    }

    @Override
    public void visitLeave(Between o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(ContainsAll o) {
        getQueryMaker().containsAll(o.getElement());
        return true;
    }

    @Override
    public boolean visitMiddle(ContainsAll o) {
        // do nothing
        return true;
    }

    @Override
    public void visitLeave(ContainsAll o, boolean enterOrMiddleResult) {
        getQueryMaker().close();
    }

    @Override
    public boolean visitEnter(ContainsOne o) {
        getQueryMaker().containsOne(o.getElement());
        return true;
    }

    @Override
    public boolean visitMiddle(ContainsOne o) {
        // do nothing
        return true;
    }

    @Override
    public void visitLeave(ContainsOne o, boolean enterOrMiddleResult) {
        getQueryMaker().close();
    }

    @Override
    public boolean visitEnter(Equals o) {
        if (o.isIgnoreCaseAndAccent()) {
            getQueryMaker().eqIgnoreCaseAndAccent(o.getElement());
        } else {
            getQueryMaker().eq(o.getElement());
        }
        return true;
    }

    @Override
    public void visitLeave(Equals o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(NotEquals o) {
        if (o.isIgnoreCaseAndAccent()) {
            getQueryMaker().neIgnoreCaseAndAccent(o.getElement());
        } else {
            getQueryMaker().ne(o.getElement());
        }

        return true;
    }

    @Override
    public void visitLeave(NotEquals o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public void visit(False o) {
        getQueryMaker().rFalse();
    }

    @Override
    public void visit(True o) {
        getQueryMaker().rTrue();
    }

    @Override
    public boolean visitEnter(Greater o) {
        getQueryMaker().gt(o.getElement());
        return true;
    }

    @Override
    public void visitLeave(Greater o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(GreaterOrEquals o) {
        getQueryMaker().ge(o.getElement());
        return true;
    }

    @Override
    public void visitLeave(GreaterOrEquals o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(Keyword o) {
        getQueryMaker().keyword();
        return true;
    }

    @Override
    public void visitLeave(Keyword o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(Less o) {
        getQueryMaker().lt(o.getElement());
        return true;
    }

    @Override
    public void visitLeave(Less o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(LessOrEquals o) {
        getQueryMaker().le(o.getElement());
        return true;
    }

    @Override
    public void visitLeave(LessOrEquals o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(Like o) {
        getQueryMaker().like(o.getElement());
        return true;
    }

    @Override
    public void visitLeave(Like o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public boolean visitEnter(Unlike o) {
        getQueryMaker().unlike(o.getElement());
        return true;
    }

    @Override
    public void visitLeave(Unlike o, boolean enterOrMiddleResult) {
        // do nothing
    }

    @Override
    public void visit(Null o) {
        getQueryMaker().isNull(o.getElement());
    }

    @Override
    public void visit(NotNull o) {
        getQueryMaker().isNotNull(o.getElement());
    }


    @Override
    public boolean visitEnter(WikittyQueryFunction function) {
        return true;
    }

    @Override
    public boolean visitMiddle(WikittyQueryFunction function) {
        return true;
    }

    @Override
    public void visitLeave(WikittyQueryFunction function, boolean enterOrMiddleResult) {
        List<WikittyQueryFunction> args = getAndClearFunction(function.getArgs().size());
        WikittyQueryFunction f = WikittyQueryFunction.create(function.getMethodName(), function.getName(), args);
        addFunction(f);
    }

    @Override
    public void visit(FunctionValue function) {
        addFunction(new FunctionValue(function.getName(), function.getValue()));
    }

    @Override
    public void defaultVisit(Object o) {
        throw new UnsupportedOperationException(
                "Not supported (" + o.getClass().getSimpleName() + ").");
    }

    @Override
    public boolean defaultVisitEnter(Object o) {
        throw new UnsupportedOperationException(
                "Not supported (" + o.getClass().getSimpleName() + ").");
    }

    @Override
    public boolean defaultVisitMiddle(Object o) {
        throw new UnsupportedOperationException(
                "Not supported (" + o.getClass().getSimpleName() + ").");
    }

    @Override
    public void defaultVisitLeave(Object o, boolean enterResult) {
        throw new UnsupportedOperationException(
                "Not supported (" + o.getClass().getSimpleName() + ").");
    }
}
