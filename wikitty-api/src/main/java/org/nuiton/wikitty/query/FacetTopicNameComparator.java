/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query;

import java.util.Comparator;

/**
 * Comparateur de topic sur le nom du topic.
 * Cela permet de presenter a l'utilisateur les topics dans l'ordre alphabetique
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FacetTopicNameComparator implements Comparator<FacetTopic> {

    final static public Comparator<FacetTopic> instance =
            new FacetTopicNameComparator();
    final static public Comparator<FacetTopic> instanceCaseSensitive =
            new FacetTopicNameComparator(false);

    protected boolean ignoreCase = true;

    /**
     * You must use {@link #instance} or {@link #instanceCaseSensitive}
     */
    public FacetTopicNameComparator() {
    }

    /**
     * You must use {@link #instance} or {@link #instanceCaseSensitive}
     */
    public FacetTopicNameComparator(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }
    
    public int compare(FacetTopic o1, FacetTopic o2) {
        int result;
        if (ignoreCase) {
            result = o1.getTopicName().compareToIgnoreCase(o2.getTopicName());   
        } else {
            result = o1.getTopicName().compareTo(o2.getTopicName());
        }
        return result;
    }
}
