/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import org.nuiton.wikitty.entities.Element;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.query.WikittyQueryVisitor;

/**
 * classe mere des operateurs unaire et binaire. Lorsqu'on l'utilise pour un
 * unaire seul element ou value est different de null, mais pas les deux en
 * meme temps.
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public abstract class TerminalBinaryOperator extends TerminalOperator {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    protected Element element;
    protected ConditionValue value;

    /**
     * Constructor with all parameters initialized
     * 
     * @param element
     */
    public TerminalBinaryOperator(Element element) {
        this(element, (ConditionValue)null);
    }

     /**
     * Constructor with all parameters initialized
     *
     * @param element
     * @param value
     */
    public TerminalBinaryOperator(Element element, String value) {
        this(element, new ConditionValueString(value));
    }

    /**
     * Constructor with all parameters initialized
     *
     * @param element
     * @param value value is internaly copied to prevent external modification
     */
    public TerminalBinaryOperator(Element element, ConditionValue value) {
        this.element = element;
        this.value = value;
    }

    @Override
    public boolean waitCondition() {
        boolean result = value == null;
        return result;
    }

    @Override
    public Condition addCondition(Condition c) {
        if (waitCondition()) {
            if (c instanceof ConditionValue) {
                    value = (ConditionValue)c;
            } else {
                throw new WikittyException(
                        "Only ConditionValue can be add to TerminalBinaryOperator,"
                        + " but you try to add: "
                        + ClassUtils.getShortCanonicalName(c, "null"));
            }
        } else {
            throw new WikittyException(String.format(
                    "Operator (%s) has already value",
                    getClass().getSimpleName()));
        }
        return this;
    }

    @Override
    public void accept(WikittyQueryVisitor visitor) {
        boolean walk = visitor.visitEnter(this);
        if (walk) {
            value.accept(visitor);
        }
        visitor.visitLeave(this, walk);
    }

    public Element getElement() {
        return element;
    }

    public ConditionValue getValue() {
        return value;
    }

    @Override
    boolean equalsDeep(Object other) {
        TerminalBinaryOperator op = (TerminalBinaryOperator)other;
        boolean result = ObjectUtils.equals(this.getElement(), op.getElement())
                && ObjectUtils.equals(this.getValue(), op.getValue());
        return result;
    }

}
