/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import org.nuiton.wikitty.entities.Element;
import java.util.Collection;

/**
 * ContainsOne permet de definir qu'un champs doit avoir au moins une des valeurs
 * d'une serie de valeurs. Si cette condition est utilisee avec une seul valeur,
 * cela revient a faire un {@link Equals})
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
  */
public class ContainsOne extends TerminalNaryOperator {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    /**
     * Constructor with all parameters initialized
     * 
     * @param element
     * @param values
     */
    public ContainsOne(Element element, Collection<ConditionValue> values) {
        super(element, values);
    }

    public ContainsOne(Element element) {
        super(element);
    }
}
