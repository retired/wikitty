/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import org.nuiton.wikitty.entities.Element;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.query.WikittyQueryVisitor;

/**
 * Classe mere des operateurs ternaire (ex: Between)
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public abstract class TerminalTernaryOperator extends TerminalOperator {

    private static final long serialVersionUID = 1L;

    protected Element element;
    protected ConditionValue min;
    protected ConditionValue max;

    public TerminalTernaryOperator(Element element) {
        this(element, (ConditionValue)null, (ConditionValue)null);
    }

    public TerminalTernaryOperator(Element element, String min, String max) {
        this(element, new ConditionValueString(min), new ConditionValueString(max));
    }

    public TerminalTernaryOperator(Element element, ConditionValue min, ConditionValue max) {
        this.element = element;
        this.min = min;
        this.max = max;
    }

    @Override
    public boolean waitCondition() {
        boolean result = min == null || max == null;
        return result;
    }

    @Override
    public Condition addCondition(Condition c) {
        if (waitCondition()) {
            if (c instanceof ConditionValue) {
                if (min == null) {
                    min = (ConditionValue)c;
                } else {
                    max = (ConditionValue)c;
                }
            } else {
                throw new WikittyException(
                        "Only ConditionValue can be add to TerminalTernaryOperator,"
                        + " but you try to add: "
                        + ClassUtils.getShortCanonicalName(c, "null"));
            }
        } else {
            throw new WikittyException(
                    "TerminalTernaryOperator has already min and max value");
        }
        return this;
    }

    @Override
    public void accept(WikittyQueryVisitor visitor) {
        boolean walk = visitor.visitEnter(this);
        if (walk) {
            min.accept(visitor);
            walk = visitor.visitMiddle(this);
            if (walk) {
                max.accept(visitor);
            }
        }
        visitor.visitLeave(this, walk);
    }

    public Element getElement() {
        return element;
    }

    public ConditionValue getMin() {
        return min;
    }

    public ConditionValue getMax() {
        return max;
    }

    @Override
    boolean equalsDeep(Object other) {
        TerminalTernaryOperator op = (TerminalTernaryOperator)other;
        boolean result = ObjectUtils.equals(this.getElement(), op.getElement())
                && ObjectUtils.equals(this.getMin(), op.getMin())
                && ObjectUtils.equals(this.getMax(), op.getMax());
        return result;
    }
}
