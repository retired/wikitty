/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query;

import java.util.Collection;
import org.nuiton.wikitty.query.function.WikittyQueryFunction;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.query.conditions.And;
import org.nuiton.wikitty.query.conditions.Between;
import org.nuiton.wikitty.query.conditions.Condition;
import org.nuiton.wikitty.query.conditions.ConditionValue;
import org.nuiton.wikitty.query.conditions.ConditionValueString;
import org.nuiton.wikitty.query.conditions.ContainsAll;
import org.nuiton.wikitty.query.conditions.ContainsOne;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.query.conditions.Equals;
import org.nuiton.wikitty.query.conditions.False;
import org.nuiton.wikitty.query.conditions.Greater;
import org.nuiton.wikitty.query.conditions.GreaterOrEquals;
import org.nuiton.wikitty.query.conditions.Keyword;
import org.nuiton.wikitty.query.conditions.Less;
import org.nuiton.wikitty.query.conditions.LessOrEquals;
import org.nuiton.wikitty.query.conditions.Like;
import org.nuiton.wikitty.query.conditions.Not;
import org.nuiton.wikitty.query.conditions.NotEquals;
import org.nuiton.wikitty.query.conditions.Or;
import org.nuiton.wikitty.query.conditions.NotNull;
import org.nuiton.wikitty.query.conditions.Null;
import org.nuiton.wikitty.query.conditions.Select;
import org.nuiton.wikitty.query.conditions.True;
import org.nuiton.wikitty.query.conditions.Unlike;
import org.nuiton.wikitty.query.function.FunctionFieldValue;
import org.nuiton.wikitty.query.function.FunctionValue;
import org.parboiled.BaseParser;
import org.parboiled.Context;
import org.parboiled.Parboiled;
import org.parboiled.Rule;
import org.parboiled.annotations.BuildParseTree;
import org.parboiled.errors.ErrorUtils;
import org.parboiled.parserunners.RecoveringParseRunner;
import org.parboiled.parserunners.ReportingParseRunner;
import org.parboiled.parserunners.TracingParseRunner;
import org.parboiled.support.ParseTreeUtils;
import org.parboiled.support.ParsingResult;
import org.parboiled.support.Var;

/**
 * Cette classe permet d'interpreter une requete faite textuellement en la
 * convertisant en sa representation objet. Si l'objet est instancier pour
 * utiliser les fonctionnalites d'alias, il est possible de l'utiliser dans
 * plusieurs thread concurent. La map d'alias est protegee.
 *
 * Pour plus d'information reportez-vous à la
 * <a href="http://maven-site.nuiton.org/wikitty/user/query.html">documentation</a>
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
@BuildParseTree
public class WikittyQueryParser extends BaseParser<Object> {

    public static final String AS = "AS";
    public static final String DATE = "DATE";
    public static final String SELECT = "SELECT";
    public static final String WHERE = "WHERE";
    public static final String IN = "IN";
    public static final String LITERAL_OPEN_SIMPLE = "'";
    public static final String LITERAL_CLOSE_SIMPLE = "'";
    public static final String LITERAL_OPEN_DOUBLE = "\"";
    public static final String LITERAL_CLOSE_DOUBLE = "\"";
    public static final String NULL = "NULL";
    public static final String TO = "TO";
    public static final String FALSE = "FALSE";
    public static final String TRUE = "TRUE";
    public static final String UNLIKE = "UNLIKE";
    public static final String AND = "AND";
    public static final String COMMA = ",";
    public static final String CURLY_BRACKET_CLOSE = "}";
    public static final String CURLY_BRACKET_OPEN = "{";
    public static final String EQUALS = "=";
    public static final String EQUALS_IGNORE_CASE_AND_ACCENT = "~";
    public static final String GREATER = ">";
    public static final String GREATER_OR_EQUALS = ">=";
    public static final String LESS = "<";
    public static final String LESS_OR_EQUALS = "<=";
    public static final String LIKE = "LIKE";
    public static final String NOT = "NOT";
    public static final String NOT_EQUALS = "!=";
    public static final String NOT_EQUALS_IGNORE_CASE_AND_ACCENT = "!~";
    public static final String OR = "OR";
    public static final String BRACKET_CLOSE = ")";
    public static final String BRACKET_OPEN = "(";
    public static final String SQUARE_BRACKET_CLOSE = "]";
    public static final String SQUARE_BRACKET_OPEN = "[";

    public static final String OFFSET = "#OFFSET";
    public static final String LIMIT = "#LIMIT";
    public static final String DEPTH = "#DEPTH";

    public Rule icOFFSET = IgnoreCase(OFFSET);
    public Rule icLIMIT = IgnoreCase(LIMIT);
    public Rule icDEPTH = IgnoreCase(DEPTH);
    
    public Rule icEXTENSION = IgnoreCase(Element.EXTENSION.getValue());
    public Rule icID = IgnoreCase(Element.ID.getValue());
    public Rule icNOT = IgnoreCase(NOT);
    public Rule icAND = IgnoreCase(AND);
    public Rule icOR = IgnoreCase(OR);
    public Rule icSELECT = IgnoreCase(SELECT);
    public Rule icWHERE = IgnoreCase(WHERE);
    public Rule icIN = IgnoreCase(IN);
    public Rule icTO = IgnoreCase(TO);
    public Rule icFALSE = IgnoreCase(FALSE);
    public Rule icTRUE = IgnoreCase(TRUE);
    public Rule icLIKE = IgnoreCase(LIKE);
    public Rule icUNLIKE = IgnoreCase(UNLIKE);
    public Rule icNULL = IgnoreCase(NULL);
    public Rule icDATE = IgnoreCase(DATE);
    public Rule icAS = IgnoreCase(AS);

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyQueryParser.class);

    protected Map<String, String> alias = new LinkedHashMap<String, String>();

    public WikittyQueryParser() {
    }

    boolean debug(String text, Context context) {
        System.out.println("DEBUG("+context.getCurrentIndex()+"):" + text + "  nodes:" +context.getSubNodes());
        return true;
    }

    /**
     * Retourne une vue non modifiable des alias disponibles.
     * @return
     */
    public Map<String, String> getAlias() {
        return Collections.unmodifiableMap(alias);
    }

    /**
     * Modifie l'ensemble des alias, la map passee en parametre est copiee en
     * interne.
     * @param alias la map des alias qui sera copiee
     * @return le WikittyQueryParser lui meme (this)
     */
    public WikittyQueryParser setAlias(Map<String, String> alias) {
        // on passe par la creation d'une nouvelle map, pour eviter que l'utilisateur
        // ne puisse modifier les alias depuis l'exterieur et eviter le plus
        // possible les synchronize
        this.alias = new LinkedHashMap<String, String>();
        if (alias != null) {
            this.alias.putAll(alias);
        }
        return this;
    }

    public WikittyQueryParser addAlias(String aliasName, String aliasValue) {
        synchronized(alias) {
            alias.put(aliasName, aliasValue);
        }
        return this;
    }

    public WikittyQueryParser clearAlias() {
        // on passe par la creation d'une nouvelle map, pour eviter le plus
        // possible les synchronize
        this.alias = new LinkedHashMap<String, String>();
        return this;
    }

    /**
     * Parse query and use alias added with {@link #addAlias} or {@link #setAlias}
     *
     * @param queryString query to parse
     * @return
     */
    public WikittyQuery parseQuery(String queryString) {
        WikittyQuery result = parse(queryString, alias);
        return result;
    }

    /**
     * Parse query without alias
     *
     * @param queryString query to parse
     * @return
     */
    static public WikittyQuery parse(String queryString) {
        WikittyQuery result = parse(queryString, null);
        return result;
    }

    /**
     * Parse query and use alias in argument
     *
     * @param queryString query to parse
     * @param alias alias to used to change query
     * @return
     */
    static public WikittyQuery parse(String queryString, Map<String, String> alias) {
        queryString = StringUtils.strip(queryString);
        if (queryString == null) {
            queryString = "";
        }
        if (alias != null) {
            String queryStringInit = queryString;
            // on synchronise l'utilisation de la map, pour etre sur qu'il n'y
            // ait pas d'ajout/modif durant son utilisation pour le remplacement
            // on fait direcement les remplacements de la requete car, c'est
            // le seul endroit ou l'alias est utilise, et c'est aussi rapide
            // que de faire une copie de la map
            synchronized(alias) {
                // first replace alias in queryString
                for (Map.Entry<String, String> a : alias.entrySet()) {
                    queryString = queryString.replaceAll(a.getKey(), a.getValue());
                }
            }
            if (log.isDebugEnabled()) {
                log.debug(String.format("QueryString \n'%s' become after alias \n'%s'\naliases are %s",
                        queryStringInit, queryString, alias));
            }
        }

        WikittyQueryParser parser = Parboiled.createParser(WikittyQueryParser.class);

       ParsingResult<?> result = new ReportingParseRunner(parser.start()).run(queryString);
//        ParsingResult<?> result = new TracingParseRunner(parser.start()).run(queryString);
//        ParsingResult<?> result = new RecoveringParseRunner(parser.start()).run(queryString);

        if (result.hasErrors() || !result.matched) {
            System.out.println("\nParse Errors:\n" + ErrorUtils.printParseErrors(result));
        }

        if (log.isDebugEnabled()) {
            log.debug("\nParse Tree:\n" + ParseTreeUtils.printNodeTree(result) + '\n');
        }
//        System.out.println("\nParse Tree:\n" + ParseTreeUtils.printNodeTree(result) + '\n');
        
        WikittyQuery query = (WikittyQuery)result.resultValue;

        return query;
    }

    /**
     * can be field, extension name or id element
     * @param v
     * @return
     */
    protected Element toElement(String v) {
        Element result = Element.get(v);
        return result;
    }

    protected int toInt(String v) {
        int result = Integer.parseInt(v);
        return result;
    }

    /**
     * Remove quote at beginning and ending of String in parameter if necessary
     *
     * <ul>
     * <li>"toto" return toto</li>
     * <li>"toto return "toto</li>
     * <li> toto return toto"</li>
     * <li> to"to return to"to</li>
     * </ul>
     * 
     * @param s
     * @return
     */
    protected String removeQuote(String s) {
        String result = s;
        if (StringUtils.startsWithAny(s, LITERAL_OPEN_SIMPLE, LITERAL_OPEN_DOUBLE)
                && StringUtils.endsWithAny(s, LITERAL_CLOSE_SIMPLE, LITERAL_CLOSE_DOUBLE)) {
            result = StringUtils.substring(s, 1, -1);
        }
        return result;
    }

    protected String toDate(Object s) {
        if (s instanceof ConditionValueString) {
            s = ((ConditionValueString)s).getValue();
        }
        Date d = WikittyUtil.toDate(s);
        String result = WikittyUtil.toString(d);
        return result;
    }

    /**
     * @param list la collection en object pour eviter le cast dans les Rules
     * @param e l'element a ajouter a la liste
     * @return la liste passee en parametre
     */
    protected Object addToList(Object list, Object e) {
        ((Collection)list).add(e);
        return list;
    }

   Rule start() {
       return Sequence(FirstOf(or(), empty()), push(new WikittyQuery((Condition)pop())),
               offset(), limit(), depth(), space(), EOI);
   }

   Rule empty() {
       return Sequence(EMPTY, push(new True()));
   }

   Rule offset() {
       return Optional(space(), icOFFSET, FirstOf(EQUALS, space()), OneOrMore(AnyOf("1234567890")),
               push(((WikittyQuery)pop()).setOffset(toInt(match())))
               );
   }

   Rule limit() {
       return Optional(space(), icLIMIT, FirstOf(EQUALS, space()), OneOrMore(AnyOf("1234567890")),
               push(((WikittyQuery)pop()).setLimit(toInt(match())))
               );
   }

   Rule depth() {
       return Optional(space(), icDEPTH, FirstOf(EQUALS, space()), OneOrMore(AnyOf("1234567890")),
               push(((WikittyQuery)pop()).setWikittyFieldSearchDepth(toInt(match())))
               );
   }

   Rule or() {
        return Sequence(and(), ZeroOrMore(space(), icOR, space(), and(),
                push(new Or((Condition)pop(1), (Condition)pop()))));
    }

    Rule and() {
        return Sequence(term(), ZeroOrMore(
                // when no AND or OR is used, AND is default
                // don't change order of FirstOf, this order is important
                FirstOf(Sequence(space(), icAND, space()), Sequence(space(), TestNot(icOR))),
                term(),
                push(new And((Condition)pop(1), (Condition)pop()))));
    }

    Rule term() {
        return FirstOf(condition(), Parens());
    }

    Rule Parens() {
        return Sequence(space(), BRACKET_OPEN, space(), or(), space(), BRACKET_CLOSE, space());
    }

    Rule condition() {
        // ATTENTION l'ordre est important par exemple le '>' doit etre apres le '>='
        return FirstOf(
                not(), isNull(), isNotNull(), select(),
                greatereq(), lesseq(),
                between(), containsAll(), containsOne(),
                eq(), neq(), eqIgnoreCaseAndAccent(), neqIgnoreCaseAndAccent(),
                less(), greater(), like(), notlike(),
                rTrue(), rFalse(), keyword()
                );
    }

    Rule not() {
        return Sequence(space(), icNOT, space(), term(),
                push(new Not((Condition)pop())));
    }

    /**
     * gere eq, startsWith, endsWith, isNull
     * @return
     */
    Rule isNull() {
        return Sequence(field(), push(match()), space(), EQUALS, space(), icNULL,
                push(new Null(toElement(pop().toString()))));
    }

    /**
     * gere eq, isNull
     * @return
     */
    Rule isNotNull() {
        return Sequence(field(), push(match()), space(), NOT_EQUALS, space(), icNULL,
                push(new NotNull(toElement(pop().toString()))));
    }

    /**
     * gere eq, startsWith, endsWith, isNull
     * @return
     */
    Rule eq() {
        return Sequence(field(), push(match()), space(), EQUALS, space(), value(),
                push(new Equals(toElement(pop(1).toString()), (ConditionValue)pop())));
    }

    /**
     * gere eq, isNull
     * @return
     */
    Rule neq() {
        return Sequence(field(), push(match()), space(), NOT_EQUALS, space(), value(),
                push(new NotEquals(toElement(pop(1).toString()), (ConditionValue)pop())));
    }

    /**
     * gere eq, startsWith, endsWith, isNull
     * @return
     */
    Rule eqIgnoreCaseAndAccent() {
        return Sequence(field(), push(match()), space(), EQUALS_IGNORE_CASE_AND_ACCENT, space(), value(),
                push(new Equals(toElement(pop(1).toString()), (ConditionValue)pop(), true)));
    }

    /**
     * gere eq, isNull
     * @return
     */
    Rule neqIgnoreCaseAndAccent() {
        return Sequence(field(), push(match()), space(), NOT_EQUALS_IGNORE_CASE_AND_ACCENT, space(), value(),
                push(new NotEquals(toElement(pop(1).toString()), (ConditionValue)pop(), true)));
    }

    Rule less() {
        return Sequence(field(), push(match()), space(), LESS, space(), value(),
                push(new Less(toElement(pop(1).toString()), (ConditionValue)pop())));
    }
    Rule lesseq() {
        return Sequence(field(), push(match()), space(), LESS_OR_EQUALS, space(), value(),
                push(new LessOrEquals(toElement(pop(1).toString()), (ConditionValue)pop())));
    }
    Rule greater() {
        return Sequence(field(), push(match()), space(), GREATER, space(), value(),
                push(new Greater(toElement(pop(1).toString()), (ConditionValue)pop())));
    }
    Rule greatereq() {
        return Sequence(field(), push(match()), space(), GREATER_OR_EQUALS, space(), value(),
                push(new GreaterOrEquals(toElement(pop(1).toString()), (ConditionValue)pop())));
    }
    Rule like() {
        return Sequence(field(), push(match()), space(), icLIKE, space(), value(),
                push(new Like(toElement(pop(1).toString()), (ConditionValue)pop())));
    }
    Rule notlike() {
        return Sequence(field(), push(match()), space(), icUNLIKE, space(), value(),
                push(new Unlike(toElement(pop(1).toString()), (ConditionValue)pop())));
    }
    Rule between() {
        return Sequence(field(), push(match()), space(), EQUALS, space(), SQUARE_BRACKET_OPEN, space(),
                value(), space(), icTO, space(),
                value(), space(), SQUARE_BRACKET_CLOSE,
                push(new Between(toElement(pop(2).toString()), (ConditionValue)pop(1), (ConditionValue)pop())));
    }
    Rule containsAll() {
        Var<List<ConditionValue>> elems = new Var<List<ConditionValue>>(new LinkedList<ConditionValue>());
        return Sequence(field(), push(match()), space(), EQUALS, space(), SQUARE_BRACKET_OPEN, space(),
                value(), elems.get().add((ConditionValue)pop()), space(), ZeroOrMore(COMMA, space(),
                value(), elems.get().add((ConditionValue)pop()), space()), SQUARE_BRACKET_CLOSE,
                push(new ContainsAll(toElement(pop().toString()), elems.get())));
    }
    Rule containsOne() {
        Var<List<ConditionValue>> elems = new Var<List<ConditionValue>>(new LinkedList<ConditionValue>());
        return Sequence(field(), push(match()), space(), EQUALS, space(), CURLY_BRACKET_OPEN, space(),
                value(), elems.get().add((ConditionValue)pop()), space(), ZeroOrMore(COMMA, space(),
                value(), elems.get().add((ConditionValue)pop()), space()), CURLY_BRACKET_CLOSE,
                push(new ContainsOne(toElement(pop().toString()), elems.get())));
    }
    Rule select() {
        return Sequence(icSELECT, space(),
                functionOrFieldList() /*, field(), push(match())*/, space(),
                
                FirstOf(Sequence(icWHERE, space(), term()), empty()),
                push(new Select(((WikittyQueryFunction)pop(1)), (Condition)pop())));
    }

    Rule functionOrFieldList() {
        return Sequence(push(new LinkedList()), functionOrFieldOrString(), ZeroOrMore(COMMA, functionOrFieldOrString()),
                push(WikittyQueryFunction.createFusionIfNeeded((List)pop())));
    }
    Rule functionOrFieldOrString() {
        return Sequence(FirstOf(function(), fieldFunction(), stringFunction()),
                push(addToList(pop(1), pop())));
    }

    Rule function() {
        Var<String> alias = new Var<String>();
        return Sequence(OneOrMore(FirstOf(field(), AnyOf("#"))), push(match()), BRACKET_OPEN,
                push(new LinkedList()),
                Optional(functionOrFieldOrString(), ZeroOrMore(COMMA, functionOrFieldOrString())),
                BRACKET_CLOSE, Optional(alias(alias)),
                push(WikittyQueryFunction.create(pop(1).toString(), alias.get(), (List)pop())));
    }

    Rule fieldFunction() {
        Var<String> alias = new Var<String>();
        return Sequence(field(), push(toElement(match())), Optional(alias(alias)),
                push(new FunctionFieldValue(alias.get(), pop().toString())));
    }

    Rule stringFunction() {
        Var<String> alias = new Var<String>();
        return Sequence(valueText(), Optional(alias(alias)),
                push(new FunctionValue(alias.get(), ((ConditionValueString)pop()).getValue() )));
    }

    Rule alias(Var<String> alias) {
        return Sequence(space(), icAS, space(), field(), alias.set(match()));
    }

    Rule element() {
        return Sequence(field(), push(toElement(match())));
    }

    Rule keyword() {
        return Sequence(value(), push(new Keyword().addCondition((ConditionValue)pop())));
    }

    Rule field() {
        return OneOrMore(FirstOf(CharRange('0', '9'), CharRange('a', 'z'), CharRange('A', 'Z'), AnyOf("_.*")));
    }

    Rule rTrue() {
        return Sequence(icTRUE, push(new True()));
    }

    Rule rFalse() {
        return Sequence(icFALSE, push(new False()));
    }

    Rule value() {
        return FirstOf(select(), date(), valueText());
    }

    Rule date() {
        return Sequence(icDATE, BRACKET_OPEN, valueText() ,BRACKET_CLOSE,
                push(new ConditionValueString(toDate(pop()))));
    }

    Rule valueText() {
        return Sequence(FirstOf(StringLiteralDouble(), StringLiteralSimple(), SimpleString()),
                push(new ConditionValueString(removeQuote(match()))));
    }

    /**
     * Une chaine simple sans espace, parenthese, retour chariot, tabulation,
     * accolade, crochet, ", '.
     * @return
     */
    Rule SimpleString() {
        return OneOrMore(FirstOf(
                Escape(),
                Sequence(TestNot(AnyOf(" #\t\r\n\\"
                +COMMA
                +LITERAL_OPEN_SIMPLE+LITERAL_OPEN_DOUBLE
                +BRACKET_OPEN+BRACKET_CLOSE
                +CURLY_BRACKET_OPEN+CURLY_BRACKET_CLOSE
                +SQUARE_BRACKET_OPEN+SQUARE_BRACKET_CLOSE)), ANY)
                )).suppressSubnodes();
    }

    Rule StringLiteralSimple() {
        return Sequence(
                LITERAL_OPEN_SIMPLE,
                ZeroOrMore(
                        FirstOf(
                                EscapeSimple(),
                                Sequence(TestNot(AnyOf("\r\n\\"+LITERAL_CLOSE_SIMPLE)), ANY)
                        )
                ).suppressSubnodes(),
                LITERAL_CLOSE_SIMPLE
                );
    }

    Rule StringLiteralDouble() {
        return Sequence(
                LITERAL_OPEN_DOUBLE,
                ZeroOrMore(
                        FirstOf(
                                EscapeDouble(),
                                Sequence(TestNot(AnyOf("\r\n\\"+LITERAL_CLOSE_DOUBLE)), ANY)
                        )
                ).suppressSubnodes(),
                LITERAL_CLOSE_DOUBLE
                );
    }

    Rule Escape() {
        return Sequence('\\', FirstOf(AnyOf("#btnfr\'\\"
                +LITERAL_OPEN_SIMPLE+LITERAL_CLOSE_SIMPLE+LITERAL_OPEN_DOUBLE+LITERAL_CLOSE_DOUBLE),
                OctalEscape(), UnicodeEscape()));
    }

    Rule EscapeSimple() {
        return Sequence('\\', FirstOf(AnyOf("#btnfr\'\\"+LITERAL_CLOSE_SIMPLE),
                OctalEscape(), UnicodeEscape()));
    }

    Rule EscapeDouble() {
        return Sequence('\\', FirstOf(AnyOf("#btnfr\'\\"+LITERAL_CLOSE_DOUBLE),
                OctalEscape(), UnicodeEscape()));
    }

    Rule OctalEscape() {
        return FirstOf(
                Sequence(CharRange('0', '3'), CharRange('0', '7'), CharRange('0', '7')),
                Sequence(CharRange('0', '7'), CharRange('0', '7')),
                CharRange('0', '7')
        );
    }

    Rule UnicodeEscape() {
        return Sequence(OneOrMore('u'), HexDigit(), HexDigit(), HexDigit(), HexDigit());
    }

    Rule HexDigit() {
        return FirstOf(CharRange('a', 'f'), CharRange('A', 'F'), CharRange('0', '9'));
    }

    Rule space() {
        return ZeroOrMore(AnyOf(" \t\f"));
    }

}
