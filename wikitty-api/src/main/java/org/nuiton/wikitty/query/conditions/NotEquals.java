/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.query.WikittyQueryMaker;

/**
 * NotEquals operator is used to build restriction containing "element != value"
 * where element could be an Id, a String, an Integer, a Float, a Date ... <br>
 * <br>
 * For example, use: {@link WikittyQueryMaker}.neq( myElement , "REF1234567890" )
 * <p>
 * <p>
 * This operator used for String check strict equality (case sensitive)
 * You can used '*' in expression at beginning or ending for String equality.
 *
 * <ul>
 * <li> {@link WikittyQueryMaker}.ne("myext.myfield", "*jour") not match field "bonjour" but match "BONJOUR"</li>
 * </ul>
 *
 * <p>
 * You can specify ignoreCaseAndAccent to check equality. This mode
 * ignore case and accent.
 *
 * <ul>
 * <li> {@link WikittyQueryMaker}.neIgnoreCaseAndAccent("myext.myfield", "*jour") not match field "bonjour" and not match "BONJOUR"</li>
 * </ul>
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class NotEquals extends TerminalBinaryOperator {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    protected boolean ignoreCaseAndAccent = false;

    public NotEquals(Element element) {
        super(element);
    }

    public NotEquals(Element element, String value) {
        super(element, value);
    }

    public NotEquals(Element element, ConditionValue value) {
        super(element, value);
    }

    public NotEquals(Element element, boolean ignoreCaseAndAccent) {
        super(element);
        this.ignoreCaseAndAccent = ignoreCaseAndAccent;
    }

    public NotEquals(Element element, String value, boolean ignoreCaseAndAccent) {
        super(element, value);
        this.ignoreCaseAndAccent = ignoreCaseAndAccent;
    }

    public NotEquals(Element element, ConditionValue value, boolean ignoreCaseAndAccent) {
        super(element, value);
        this.ignoreCaseAndAccent = ignoreCaseAndAccent;
    }

    public boolean isIgnoreCaseAndAccent() {
        return ignoreCaseAndAccent;
    }

}
