/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import org.nuiton.wikitty.entities.ElementField;
import org.apache.commons.lang3.ClassUtils;
import org.nuiton.wikitty.WikittyException;

/**
 * Search keyword in all wikitty. All field is converted to String
 * representation and comparaison is done in ignore case mode, if one field
 * contains value the wikitty must be returned.
 *
 * ex:
 * <ul>
 * <li>field value is 'bonjour le monde'</li>
 * <li>keyword is 'le' or 'bon' or 'nde'</li>
 * </ul>
 *
 * wikitty with this field must be returned
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class Keyword extends TerminalBinaryOperator {

    private static final long serialVersionUID = 1L;

    public Keyword() {
        super(ElementField.ALL_FIELD);
    }

    public Keyword(String value) {
        super(ElementField.ALL_FIELD, value);
    }

    public Keyword(ConditionValueString value) {
        super(ElementField.ALL_FIELD, value);
    }

    @Override
    public Condition addCondition(Condition c) {
        if (waitCondition()) {
            if (c instanceof ConditionValueString) {
                    value = (ConditionValueString)c;
            } else {
                throw new WikittyException(
                        "Only ConditionValueString can be add to Keyword,"
                        + " but you try to add: "
                        + ClassUtils.getShortCanonicalName(c, "null"));
            }
        } else {
            throw new WikittyException(
                    "Keyword has already value");
        }
        return this;
    }
    
}
