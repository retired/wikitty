package org.nuiton.wikitty.query.function;

/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.storage.WikittySearchEngine;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyQueryFunctionWrapper extends WikittyQueryFunction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyQueryFunctionWrapper.class);

    public WikittyQueryFunctionWrapper(String methodName, String name, WikittyQueryFunction... args) {
        super(methodName, name, Arrays.asList(args));
    }

    public WikittyQueryFunctionWrapper(String methodName, String name, List<WikittyQueryFunction> args) {
        super(methodName, name, args);

    }

    @Override
    public List<Map<String, Object>> call(
            WikittyQuery query, List<Map<String, Object>> data) {
        List<List<Map<String, Object>>> param = new ArrayList<List<Map<String, Object>>>();

        // on evalue les parametres que l'on passera a la fonction
        for (WikittyQueryFunction f : getArgs()) {
            param.add(f.call(query, data));
        }

        // on recherche la fonction et on instancie si besoin un objet pour l'executer
        Method method = getMethod(methodName);
        Object target = null;
        if (!Modifier.isStatic(method.getModifiers())) {
            Class clazz = method.getDeclaringClass();
            try {
                target = clazz.newInstance();
            } catch (Exception eee) {
                throw new IllegalStateException(String.format(
                        "Can't instanciate object needed to call function %s",
                        methodName), eee);
            }
        }

        // on initialise tous les iterateurs qui fourniront les parametres
        List<Iterator<Map<String, Object>>> paramIterator = new ArrayList<Iterator<Map<String, Object>>>();
        for(List<Map<String, Object>> l : param) {
            paramIterator.add(l.iterator());
        }

        List<Map<String, Object>> resultListMap = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> p = null;
        while (hasNext(paramIterator)) {
            // on recupere dans p les parametre a utiliser pour le prochain appel
            p = next(paramIterator, p);
            // on appelle la fonction
            Object result = invoke(target, method, p);
            // si le retour n'est pas une map, on le transforme en map
            if (!(result instanceof Map)) {
                result = WikittyUtil.singletonMap(getName(), result);
            }
            resultListMap.add((Map)result);
        }

        return resultListMap;
    }

    /**
     * Il y a un suivant tant qu'un des iterators a un suivant
     * 
     * @param paramIterator
     * @return 
     */
    protected boolean hasNext(List<Iterator<Map<String, Object>>> paramIterator) {
        boolean result = false;
        for (Iterator i : paramIterator) {
            result = result || i.hasNext();
        }
        return result;
    }

    /**
     * Retourne le suivant de chaque iterator, si un iterator n'a plus de suivant,
     * on reutilise l'ancien resultat
     * @param paramIterator
     * @return
     */
    protected List<Map<String, Object>> next(List<Iterator<Map<String, Object>>> paramIterator,
            List<Map<String, Object>> last) {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        int cpt=0;
        for (Iterator<Map<String, Object>> i : paramIterator) {
            if(i.hasNext()) {
                result.add(i.next());
            } else {
                if (last != null) {
                    result.add(last.get(cpt));
                } else {
                    result.add(null);
                }
            }
            cpt++;
        }

        return result;
    }

    /**
     *
     * @param target l'objet sur lequel appeler la methode
     * @param method la methode a appeler
     * @param p les arguments a convertir le mieux possible pour pouvoir appeler la methode
     * @return
     */
    protected Object invoke(Object target, Method method, List<Map<String, Object>> p) {
            Class[] pClass = method.getParameterTypes();
            Object[] param = new Object[pClass.length];

            int cpt = 0;
            for (Class c : pClass) {
                Object value;
                if (Map.class.isAssignableFrom(c)) {
                    value = p.get(cpt);
                } else if (Number.class.isAssignableFrom(c)
                        || Byte.TYPE.isAssignableFrom(c)
                        || Short.TYPE.isAssignableFrom(c)
                        || Integer.TYPE.isAssignableFrom(c)
                        || Long.TYPE.isAssignableFrom(c)
                        || Float.TYPE.isAssignableFrom(c)
                        || Double.TYPE.isAssignableFrom(c)) {
                    value = WikittyUtil.toNumber(c, WikittyUtil.toBigDecimal(takeValue(p.get(cpt))));
                } else if (Date.class.isAssignableFrom(c)) {
                    value = WikittyUtil.toDate(takeValue(p.get(cpt)));
                } else if (Boolean.TYPE.isAssignableFrom(c) || Boolean.class.isAssignableFrom(c)) {
                    value = WikittyUtil.toBoolean(takeValue(p.get(cpt)));
                } else if (byte[].class.isAssignableFrom(c)) {
                    value  = WikittyUtil.toBinary(takeValue(p.get(cpt)));
                } else if (String.class.isAssignableFrom(c)) {
                    value = WikittyUtil.toString(takeValue(p.get(cpt)));
                }else {
                    throw new ClassCastException(String.format(
                            "Object '%s' is not convertible to '%s'"
                            + " (accepted Number, String, Date,"
                            + " Boolean, byte[], Map)",
                            p.get(cpt), c.getName()));
                }
                param[cpt] = value;
                cpt++;
            }

        try {
            Object result = method.invoke(target, param);
            return result;
        } catch (Exception eee) {
            throw new WikittyException(String.format(
                    "Can't execute function (method: '%s' args:'%s')",
                    method, Arrays.toString(param)), eee);
        }
    }

    /**
     * Prend la valeur de la map, la map ne doit contenir qu'une valeur
     * @param map
     * @return null si la map est vide
     */
    protected Object takeValue(Map<String, Object> map) {
        if (map != null) {
            for(Object v : map.values()) {
                return v;
            }
        }
        return null;
    }
}
