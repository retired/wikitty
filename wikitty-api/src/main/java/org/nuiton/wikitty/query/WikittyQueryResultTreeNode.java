/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import javax.swing.tree.DefaultMutableTreeNode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Represente le resultat de recherche sur un arbre.
 * L'iteration se fait en profondeur
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyQueryResultTreeNode<T extends Serializable> extends DefaultMutableTreeNode implements Iterable<WikittyQueryResultTreeNode<T>> {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyQueryResultTreeNode.class);

    private static final long serialVersionUID = 31L;

    /** optional user object */
    protected T object;

    /**
     * Visitor for TreeNodeResult
     * @param <T>
     */
    static public interface Visitor<T extends Serializable> {
        /**
         *
         * @param node node to visit
         * @return if true visit this element
         */
        public boolean visitEnter(WikittyQueryResultTreeNode<T> node);
        /**
         *
         * @param node node visited
         * @return if true visit next element
         */
        public boolean visitLeave(WikittyQueryResultTreeNode<T> node);
    }

    protected int attCount;

    /**
     *
     * @param object L'id, le wikitty ou le BusinessEntity suivant le type de T
     * @param attCount le nombre d'attachment pour ce noeud (avec les sous noeud)
     */
    public WikittyQueryResultTreeNode(T object, int attCount) {
        this(object);
        this.attCount = attCount;
    }

    /**
     * sletellier 20110516 : override all methods using userObject,
     * because this one is transient and getter and setters are not used
     */

    /**
     * Creates a tree node with no parent, no children, but which allows
     * children, and initializes it with the specified user object.
     *
     * @param userObject an Object provided by the user that constitutes
     *                   the node's data
     */
    public WikittyQueryResultTreeNode(T userObject) {
	    this(userObject, true);
    }

    /**
     * Creates a tree node with no parent, no children, initialized with
     * the specified user object, and that allows children only if
     * specified.
     *
     * @param userObject an Object provided by the user that constitutes
     *        the node's data
     * @param allowsChildren if true, the node is allowed to have child
     *        nodes -- otherwise, it is always a leaf node
     */
    public WikittyQueryResultTreeNode(T userObject, boolean allowsChildren) {
        super(userObject, allowsChildren);
        this.object = userObject;
    }

    /**
     * Sets the user object for this node to <code>userObject</code>.
     *
     * @param	userObject	the Object that constitutes this node's
     *                          user-specified data
     * @see	#getUserObject
     * @see	#toString
     */
    public void setUserObject(T userObject) {
        super.setUserObject(userObject);
        this.object = userObject;
    }

    /**
     * Returns this node's user object.
     *
     * @return	the Object stored at this node by the user
     * @see	#toString
     */
    @Override
    public T getUserObject() {
	    return object;
    }

    /**
     * Returns the result of sending <code>toString()</code> to this node's
     * user object, or null if this node has no user object.
     *
     * @see	#getUserObject
     */
    @Override
    public String toString() {
        T userObject = getUserObject();
        if (userObject == null) {
            return null;
        } else {
            return userObject.toString();
        }
    }

    /**
     * Visite en profondeur de l'arbre, il est possible d'arreter la visite
     * soit en entrant dans le noeud soit en sortant du noeud, si respectivement
     * visitEnter ou visitLeave retourne false.
     * 
     * @param visitor
     */
    public boolean acceptVisitor(Visitor<T> visitor) {
        if (visitor.visitEnter(this)) {
            for (Enumeration e = children(); e.hasMoreElements();) {
                WikittyQueryResultTreeNode<T> child = (WikittyQueryResultTreeNode<T>) e.nextElement();
                if (!child.acceptVisitor(visitor)) {
                    break;
                }
            }
        }
        boolean result = visitor.visitLeave(this);
        return result;
    }

    /**
     * Get direct children of this node
     *
     * @return
     */
    public List<WikittyQueryResultTreeNode<T>> getChildren() {
        List<WikittyQueryResultTreeNode<T>> result;
        if (children == null) {
            result = Collections.emptyList();
        } else {
            result = Collections.unmodifiableList(
                    new ArrayList<WikittyQueryResultTreeNode<T>>(children));
        }
        return result;
    }

    /**
     * Iterate on all children or sub-children, in depth first
     * @return
     */
    @Override
    public Iterator<WikittyQueryResultTreeNode<T>> iterator() {
        Iterator<WikittyQueryResultTreeNode<T>> result = new Iterator<WikittyQueryResultTreeNode<T>>() {

            protected Enumeration enumDepth = WikittyQueryResultTreeNode.this.depthFirstEnumeration();
            
            @Override
            public boolean hasNext() {
                return enumDepth.hasMoreElements();
            }

            @Override
            public WikittyQueryResultTreeNode<T> next() {
                WikittyQueryResultTreeNode<T> result = (WikittyQueryResultTreeNode<T>)enumDepth.nextElement();
                return result;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
        return result;
    }

    /**
     * Retourne l'objet associe avec ce noeud (id, wikitty ou BusinessEntity)
     * @return l'objet associe avec ce noeud (id, wikitty ou BusinessEntity)
     */
    public T getObject() {
        return getUserObject();
    }

    /**
     * Return TreeNodeResult where object in TreeNodeResult equals child in
     * parameter
     * @param child
     * @return
     */
    public WikittyQueryResultTreeNode<T> getChild(T child) {
        WikittyQueryResultTreeNode<T> result = null;
        if (child != null) {
            for (Enumeration e = children(); e.hasMoreElements();) {
                WikittyQueryResultTreeNode<T> r = (WikittyQueryResultTreeNode<T>) e.nextElement();
                if (child.equals(r.getObject())) {
                    result = r;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Retourn le nombre d'attachment pour ce noeud (avec les sous noeud)
     * @return
     */
    public int getAttCount() {
        return attCount;
    }

}
