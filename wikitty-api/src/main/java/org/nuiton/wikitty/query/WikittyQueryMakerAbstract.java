/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.BusinessEntityImpl;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.query.conditions.And;
import org.nuiton.wikitty.query.conditions.Between;
import org.nuiton.wikitty.query.conditions.Condition;
import org.nuiton.wikitty.query.conditions.ConditionValue;
import org.nuiton.wikitty.query.conditions.ConditionValueString;
import org.nuiton.wikitty.query.conditions.ContainsAll;
import org.nuiton.wikitty.query.conditions.ContainsOne;
import org.nuiton.wikitty.query.conditions.Equals;
import org.nuiton.wikitty.query.conditions.False;
import org.nuiton.wikitty.query.conditions.Greater;
import org.nuiton.wikitty.query.conditions.GreaterOrEquals;
import org.nuiton.wikitty.query.conditions.Keyword;
import org.nuiton.wikitty.query.conditions.Less;
import org.nuiton.wikitty.query.conditions.LessOrEquals;
import org.nuiton.wikitty.query.conditions.Like;
import org.nuiton.wikitty.query.conditions.Not;
import org.nuiton.wikitty.query.conditions.NotEquals;
import org.nuiton.wikitty.query.conditions.NotNull;
import org.nuiton.wikitty.query.conditions.Null;
import org.nuiton.wikitty.query.conditions.Or;
import org.nuiton.wikitty.query.conditions.Select;
import org.nuiton.wikitty.query.conditions.True;
import org.nuiton.wikitty.query.conditions.Unlike;
import org.nuiton.wikitty.query.function.FunctionAvg;
import org.nuiton.wikitty.query.function.FunctionCount;
import org.nuiton.wikitty.query.function.FunctionDistinct;
import org.nuiton.wikitty.query.function.FunctionFieldValue;
import org.nuiton.wikitty.query.function.FunctionFusion;
import org.nuiton.wikitty.query.function.FunctionMax;
import org.nuiton.wikitty.query.function.FunctionMin;
import org.nuiton.wikitty.query.function.FunctionSum;
import org.nuiton.wikitty.query.function.FunctionValue;
import org.nuiton.wikitty.query.function.WikittyQueryFunction;
import org.nuiton.wikitty.query.function.WikittyQueryFunctionWrapper;

/**
 * Cette objet sert a construire une condition a la facon d'un flux.
 * <p>
 * Condition c = new WikittyQueryMaker().and().eq("ext.field", "toto").eq("ext.field2", 10).getCondition();
 * <p>
 * On peut aussi vouloir continuer avec un WikittyQuery pour cela on peut faire.
 * <p>
 * WikittyQuery q = new WikittyQueryMaker().and().[other condition].end();
 * <p>
 * Si un {@link WikittyQuery} est passé en parametre du constructeur et que la
 * method {@link #end()} est appeler alors la condition creee est envoyee dans
 * le WikittyQuery et le constructeur est fermer (on ne peut plus ajouter de
 * condition)
 * <p>
 * Le WikittyQuery lie avec cet objet lorsqu'on le recupere via la method
 * {@link #getQuery()} a en interne comme condition la valuer courante de la
 * condition en cours d'ecriture
 *
 * Cette classe est abstraite pour facilite l'heritage pour une utilisation dans
 * des applications qui auraient besoin d'ajouter des methodes de construction
 * (par exemple des methodes qui ajouteraient un ensemble de contraintes).
 *
 * Pour cela il faut heriter de cette classe et implanter la methode {@link  #asM}
 * Par exemple:
 * <pre>
 * class MonMaker extends WikittyQueryMakerAbstract&lt;MonMaker&gt; {
 *
 *     // ... constructeurs et methodes supplementaires ...
 *
 *     MonMaker asM() {
 *         return this;
 *     }
 * }
 * </pre>
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public abstract class WikittyQueryMakerAbstract<M extends WikittyQueryMakerAbstract> {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyQueryMakerAbstract.class);

    /** query where to send condition when end() method called */
    protected WikittyQuery query;

    /** query condition */
    protected Condition condition;
    /** stack des conditions non terminales ouvertes */
    protected Deque<Condition> openStack = new LinkedList<Condition>();
    /** stack des function ouvertes */
    protected Deque<WikittyQueryFunction> openStackFunction = new LinkedList<WikittyQueryFunction>();

    public WikittyQueryMakerAbstract() {
    }

    public WikittyQueryMakerAbstract(WikittyQuery query) {
        this.query = query;
    }

    protected abstract M asM();

    public Condition getCondition() {
        return condition;
    }

    /**
     * La query passee dans le constructeur ou une nouvelle query si aucune
     * query n'avait ete passee dans le constructeur
     * 
     * @return
     */
    public WikittyQuery getQuery() {
        if (query == null) {
            query = new WikittyQuery();
        }
        // la condition de la query doit toujours refleter la valeur courante
        // de la condition en cours de creation
        query.setCondition(getCondition());
        return query;
    }

    /**
     * Retourne le stack courant des conditions, si le stack est null cela
     * veut dire que le maker ne permet plus de modification
     * @return
     */
    protected Deque<Condition> getOpenStack() {
        if (openStack == null) {
            throw new WikittyException(
                    "You can't create condition if you have used setCondition method"
                    + " or close last condition");
        }
        return openStack;
    }

    /**
     * Ajout une condition
     *
     */
    protected void addCondition(Condition c) {
        addCondition(c, false);
    }
    /**
     * Ajout une condition. Si terminal est true, alors quelque soit le type de
     * cette condition la stack restera inchange apres l'ajout. Cela permet
     * d'ajouter des conditions non terminale comme des terminales
     */
    protected void addCondition(Condition c, boolean terminal) {
        Condition parent = getOpenStack().peek();
        if (!terminal) {
            // on ne met les conditions toujours dans le stack (meme les terminales)
            // si c'est la premiere cela permet d'indiquer a l'utilisateur
            // qu'il faut commencer par un or, and, not car une exception sera
            // levee car non supprimer par le while dans lequel on ne passe pas
            // car on passe dans le if et non le else (parent == null)
            getOpenStack().push(c);
        }

        if (parent == null) {
            // il n'y a rien dans la stack donc rien dans condition, il faut
            // mettre cette condition dedans
            condition = c;
            // et on ne depile pas le stack
        } else {
            // se add peut lever une exception si parent n'accepte pas cet enfant
            parent.addCondition(c);
            // on depile toutes les conditions qui n'ont plus besoin de renseignement
            // sauf si l'ajout a ete force en terminal
            if (!terminal) {
                closeIfNecessary();
            }
        }
    }

    protected void closeIfNecessary() {
        // on depile toutes les conditions qui n'ont plus besoin de renseignement
        while (getOpenStack().peek() != null
                && !getOpenStack().peek().waitCondition()) {
            getOpenStack().poll();
        }
        if (getOpenStack().size() == 0) {
            // we just close last open condition, set stack to null, to prevent
            // next condition add that is mistake
            openStack = null;
        }
    }

    protected Deque<WikittyQueryFunction> getOpenStackFunction() {
        Condition c = getOpenStack().peek();
        if (c instanceof Select) {
            return openStackFunction;
        } else {
            throw new IllegalStateException("You can use function only in Select clause");
        }
    }

    public M addFunction(WikittyQueryFunction f) {
        WikittyQueryFunction parent = getOpenStackFunction().peek();
        getOpenStackFunction().push(f);

        if (parent != null) {
            parent.addArgs(f);
        }
        return this.asM();
    }

    /**
     * Assigne les functions au Select et prepare l'ecriture de la condition
     * @return
     */
    public M where() {
        Condition c = getOpenStack().peek();
        if (c != null) { // pas de condition on ne fait rien
            if (c instanceof Select) {
                // un select on lui assigne la premiere function
                WikittyQueryFunction root = getOpenStackFunction().peekLast();
                if (root != null) {
                    Select select = (Select)c;
                    WikittyQueryFunction current = select.getFunction();
                    if (current == null) {
                        select.setFunction(root);
                    } else if (current instanceof FunctionFusion) {
                        current.addArgs(root);
                    } else {
                        FunctionFusion fusion = new FunctionFusion(select.getFunction(), root);
                        select.setFunction(fusion);
                    }
                }
                // et on vide la stack de function
                getOpenStackFunction().clear();
            } else {
                throw new IllegalStateException("You can use Where only after select or at beginning of condition");
            }
        }
        return this.asM();
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // F U N C T I O N   F L O W   C R E A T I O N
    //
    ///////////////////////////////////////////////////////////////////////////

    public M fieldValue(String fieldName) {
        return fieldValue(fieldName, null);
    }

    public M fieldValue(Element fieldName) {
        return fieldValue(fieldName.getValue(), null);
    }

    public M fieldValue(Element fieldName, String alias) {
        return fieldValue(fieldName.getValue());
    }

    public M fieldValue(String fieldName, String alias) {
        FunctionFieldValue f = new FunctionFieldValue(alias, fieldName);
        addFunction(f);
        return close();
    }

    public M avg() {
        return avg((String)null, null);
    }

    public M avg(String field) {
        return avg(field, null);
    }

    public M avg(Element field) {
        return avg(field.getValue(), null);
    }

    public M avg(Element field, String alias) {
        return avg(field.getValue(), alias);
    }

    public M avg(String field, String alias) {
        if (field == null) {
            FunctionAvg f = new FunctionAvg(alias);
            addFunction(f);
        } else {
            FunctionAvg f = new FunctionAvg(alias, new FunctionFieldValue(null, field));
            addFunction(f);
            close();
        }
        return this.asM();
    }

    public M count() {
        return count((String)null, null);
    }

    public M count(String field) {
        return count(field, null);
    }

    public M count(Element field) {
        return count(field.getValue(), null);
    }

    public M count(Element field, String alias) {
        return count(field.getValue(), alias);
    }

    public M count(String field, String alias) {
        if (field == null) {
            FunctionCount f = new FunctionCount(alias);
            addFunction(f);
        } else {
            FunctionCount f = new FunctionCount(alias, new FunctionFieldValue(null, field));
            addFunction(f);
            close();
        }
        return this.asM();
    }

    public M max() {
        return max((String)null, null);
    }

    public M max(String field) {
        return max(field, null);
    }

    public M max(Element field) {
        return max(field.getValue(), null);
    }

    public M max(Element field, String alias) {
        return max(field.getValue(), alias);
    }

    public M max(String field, String alias) {
        if (field == null) {
            FunctionMax f = new FunctionMax(alias);
            addFunction(f);
        } else {
            FunctionMax f = new FunctionMax(alias, new FunctionFieldValue(null, field));
            addFunction(f);
            close();
        }
        return this.asM();
    }

    public M min() {
        return min((String)null, null);
    }

    public M min(String field) {
        return min(field, null);
    }

    public M min(Element field) {
        return min(field.getValue(), null);
    }

    public M min(Element field, String alias) {
        return min(field.getValue(), alias);
    }

    public M min(String field, String alias) {
        if (field == null) {
            FunctionMin f = new FunctionMin(alias);
            addFunction(f);
        } else {
            FunctionMin f = new FunctionMin(alias, new FunctionFieldValue(null, field));
            addFunction(f);
            close();
        }
        return this.asM();

    }

    public M sum() {
        return sum((String)null, null);
    }

    public M sum(String field) {
        return sum(field, null);
    }

    public M sum(Element field) {
        return sum(field.getValue(), null);
    }

    public M sum(Element field, String alias) {
        return sum(field.getValue(), alias);
    }

    public M sum(String field, String alias) {
        if (field == null) {
            FunctionSum f = new FunctionSum(alias);
            addFunction(f);
        } else {
            FunctionSum f = new FunctionSum(alias, new FunctionFieldValue(null, field));
            addFunction(f);
            close();
        }
        return this.asM();
    }

    /**
     * You need to close this distinct
     * @return
     */
    public M distinct() {
        FunctionDistinct f = new FunctionDistinct();
        return addFunction(f);
    }

    /**
     * if field is passed in argument then this this distinct is auto closed,
     * otherwize you must clause this distinct with call to {@link #close}
     * or {@link #where} method
     * @return
     */
    public M distinct(String ... fields) {
        boolean mustClose = false;
        FunctionDistinct f = new FunctionDistinct();
        if (fields != null && fields.length > 0) {
            mustClose = true;
            for (String fieldName : fields) {
                f.addArgs(new FunctionFieldValue(null, fieldName));
            }
        }
        addFunction(f);
        if (mustClose) {
            close();
        }
        
        return this.asM();
    }

    /**
     * this distinct is auto closed if argument is not null
     * 
     * @param one use to differenciate this method with Element argument with same with String argument
     * @param fields other element
     * @return
     */
    public M distinct(Element one, Element ... fields) {
        List<String> l = new ArrayList<String>();
        if (one != null) {
            l.add(one.getValue());
        }
        if (fields != null) {
            for (Element e : fields) {
                l.add(e.getValue());
            }
        }
        return distinct(l.toArray(new String[l.size()]));
    }

    /**
     * ajoute une methode definit par l'utilisateur, la syntaxe de methode est
     * package.class#method. Il faut ensuite ajouter des arguments a la methode
     */
    public M function(String method, Object ... args) {
        return function(method, null, args);
    }
    
    /**
     * ajoute une methode definit par l'utilisateur, la syntaxe de methode est
     * [package.class]#method. Il faut ensuite ajouter des arguments a la methode
     *
     * @param method la methode utilisee comme fonction, si l'argument commence
     * par #, le nom de la classe courante lui est ajoute. Cela permet de creer
     * une sous classe a WikittyQueryMakerAbstract pour son projet et d'y mettre
     * l'ensemble des fonctions que l'on utilise dans son projet.
     * @param alias l'alias assigne au resultat
     * @param args les parametre de la fonction, si vide, il faut alors explicitement
     * appeler {@link #close} pour fermer la definition de cette fonction.
     */
    public M function(String method, String alias, Object ... args) {
        if (StringUtils.startsWith(method, "#")) {
            method = this.getClass().getName() + method;
        }
        WikittyQueryFunctionWrapper f = new WikittyQueryFunctionWrapper(method, alias);
        boolean mustClose = false;
        if (args != null && args.length > 0) {
            mustClose = true;
            for (Object o : args) {
                if (o instanceof WikittyQueryFunction) {
                    f.addArgs((WikittyQueryFunction)o);
                } else {
                   f.addArgs(new FunctionValue(null, o));
                }
            }
        }
        addFunction(f);
        if (mustClose) {
            close();
        }
        return this.asM();
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // Q U E R  Y   F L O W   C R E A T I O N
    //
    ///////////////////////////////////////////////////////////////////////////

    // eq(Wikitty|Date|Number|Boolean|String)

    static protected ConditionValue convertToConditionValue(Object o) {
        ConditionValue result;
        if (o instanceof ConditionValue) {
            result = (ConditionValue)o;
        } else {
            String s = WikittyUtil.toString(o);
            result = new ConditionValueString(s);
        }
        return result;
    }

    /**
     * Ajout d'une valeur soit pour une condition soit pour une function de la clause select
     * @param value
     * @return
     */
    public M value(Object value) {
        Condition c = getOpenStack().peek();
        if (c instanceof Select) {
            FunctionValue f = new FunctionValue(null, value);
            addFunction(f);
            close();
        } else {
            ConditionValue v = convertToConditionValue(value);
            addCondition(v);
        }
        return this.asM();
    }

    /**
     * Ajoute une condition, cette condition est prise comme une condition terminal
     * Si l'on veut continuer a construire la requete, il faut avoir ajouter
     * avant une {@link #and()}, {@link #or()}, {@link #not()}, {@link #containsOne(String)}
     * @param c la condition a ajouter
     * @return {@code this} with the {@code c} restriction added.
     */
    public M condition(Condition c) {
        addCondition(c, true);
        return this.asM();
    }

    /**
     * Parse et ajoute un bout de requete. Cette requete est prise comme une
     * condition terminal si l'on veut continuer a construire la requete, il
     * faut avoir ajouter avant une
     * {@link #and()}, {@link #or()}, {@link #not()}, {@link #containsOne(String)}
     * @param query la requete a ajouter (peut-etre vide ou nul, dans ce cas, la
     * condition utiliser est "true")
     * @param includeExtra copy offset, limit, depth found in query if true
     * @return {@code this} with the {@code c} restriction added.
     */
    public M parse(String query, boolean includeExtra) {
        if (StringUtils.isNotBlank(query)) {
            WikittyQuery filter = WikittyQueryParser.parse(query);
            addCondition(filter.getCondition(), true);
            if (includeExtra) {
                WikittyQuery q = getQuery();
                q.setOffset(filter.getOffset());
                q.setLimit(filter.getLimit());
                q.setWikittyFieldSearchDepth(filter.getWikittyFieldSearchDepth());
            }
        } else {
            addCondition(new True());
        }
        return this.asM();
    }

    /**
     * Parse et ajoute un bout de requete. Cette requete est prise comme une
     * condition terminal si l'on veut continuer a construire la requete, il
     * faut avoir ajouter avant une
     * {@link #and()}, {@link #or()}, {@link #not()}, {@link #containsOne(String)}
     * @param query la requete a ajouter (peut-etre vide ou nul, dans ce cas, la
     * condition utiliser est "true")
     * @return {@code this} with the {@code c} restriction added.
     */
    public M parse(String query) {
        return parse(query, false);
    }

    /**
     * Ajoute une contrainte qui cree les conditions en prenant comme exemple
     * l'objet passer en parametre. Seuls les champs non null sont utilises ainsi
     * que la liste des extensions de l'objet
     *
     * @param w le wikitty a prendre comme exemple
     * @return {@code this} with the {@code w} restriction added.
     */
    public M wikitty(Wikitty w) {
        WikittyQueryMaker result = new WikittyQueryMaker().and();

        // result object must have same extension that wikitty example
        result.extContainsAll(w.getExtensionNames());
        
        for (String fqfieldName : w.fieldNames()) {
            Object value = w.getFqField(fqfieldName);
            if (value != null) {
                FieldType type = w.getFieldType(fqfieldName);
                if (type.isCollection()) {
                    result.containsAll(fqfieldName, (Collection<?>)value);
                } else {
                    result.eq(fqfieldName, value);
                }
            }
        }
        addCondition(result.getCondition());

        return this.asM();
    }

    /**
     * Ajoute une contrainte qui cree les conditions en prenant comme exemple
     * l'objet passer en parametre. Seuls les champs non null sont utilises ainsi
     * que la liste des extensions de l'objet
     *
     * @param e l'objet a prendre comme exemple
     * @return {@code this} with the {@code e} restriction added.
     */
    public M wikitty(BusinessEntityImpl e) {
        Wikitty w = e.getWikitty();
        return wikitty(w);
    }

    /**
     * @see #containsAll(Element)
     */
    public M containsAll(Element element) {
        addCondition(new ContainsAll(element));
        return this.asM();
    }

    /**
     * @see #containsAll(String)
     */
    public M containsAll(String element) {
        return containsAll(Element.get(element));
    }

    /**
     * Contains.
     *
     * Search on lists (multivalued fields) that a field contains all the values
     * of the list given in parameter.
     *
     * Ex : The field with value [toto,titi,tutu] contains [titi,tutu] but not
     * [titi,tutu,tata]
     *
     * Force l'ajout du containsAll en terminal (il n'y a pas besoin de faire
     * de {@link #close()}
     *
     * @param fqfield the element on which the restriction is put
     * @param values the values to search in the element
     * @return {@code this} with the {@code contains} restriction added.
     * @see #containsAll(Element)
     */
    public <E> M containsAll(String fqfield, Collection<E> values) {
        return containsAll(Element.get(fqfield), values);
    }

    /**
     * Force l'ajout du containsAll en terminal (il n'y a pas besoin de faire
     * de {@link #close()}
     *
     * @see #containsAll(Element)
     */
    public <E> M containsAll(Element element, Collection<E> values) {
        containsAll(element);
        for (E e : values) {
            value(e);
        }
        close();
        return this.asM();
    }

    /**
     * Search on lists (multivalued fields) that a field contains all the values
     * given in parameter.
     *
     * Ex : The field with value [toto,titi,tutu] contains [titi,tutu] but not
     * [titi,tutu,tata]
     *
     * Ps : Use wildcards if you search for substrings.
     *
     * Force l'ajout du containsAll en terminal (il n'y a pas besoin de faire
     * de {@link #close()}
     *
     * @param fqfield the element on which the restriction is put
     * @param value1 first value to search in the field
     * @param values list of values to search in the field
     * @return {@code this} with the {@code contains} restriction added.
     * @see #containsAll(String)
     */
    public <E> M containsAll(String fqfield, E value1, E ... values) {
        List<E> l = new LinkedList<E>();
        l.add(value1);
        l.addAll(Arrays.asList(values));
        return containsAll(fqfield, l);
    }

    /**
     * @see #containsOne(Element)
     */
    public M containsOne(Element element) {
        addCondition(new ContainsOne(element));
        return this.asM();
    }

    /**
     * @see #containsOne(Element)
     */
    public M containsOne(String element) {
        return containsOne(Element.get(element));
    }

    /**
     * Search if a field is contained in the list of values in parameter
     *
     * Ex : The field with value titi is in [titi,tutu] but not in
     * [tutu,tata]
     *
     * Force l'ajout du containsOne en terminal (il n'y a pas besoin de faire
     * de {@link #close()}
     *
     * @param fqfield the element on which the restriction is put
     * @param values list of values the field must be in
     * @return {@code this} with the {@code in} restriction added.
     * @see #containsOne(Element)
     */
    public <E> M containsOne(String fqfield, Collection<E> values) {
        return containsOne(Element.get(fqfield), values);
    }

    /**
     * Force l'ajout du containsOne en terminal (il n'y a pas besoin de faire
     * de {@link #close()}
     *
     * @see #containsOne(Element)
     */
    public <E> M containsOne(Element element, Collection<E> values) {
        containsOne(element);
        for (E e : values) {
            value(e);
        }
        close();
        return this.asM();
    }

    /**
     * Search if a field is contained in the list of values in parameter
     *
     * Ex : The field with value titi is in [titi,tutu] but not in
     * [tutu,tata]
     *
     * Ps : Use wildcards in the values if you search for substrings.
     *
     * Force l'ajout du containsAll en terminal (il n'y a pas besoin de faire
     * de {@link #close()}
     *
     * @param fqfield the element on which the restriction is put
     * @param value1 first value the field must be in
     * @param values list of values the field must be in
     * @return {@code this} with the {@code in} restriction added.
     * @see #containsOne(String)
     */
    public <E> M containsOne(String fqfield, E value1, E ... values) {
        List<E> l = new LinkedList<E>();
        l.add(value1);
        l.addAll(Arrays.asList(values));
        return containsOne(fqfield, l);
    }

    /**
     * @see Equals
     */
    public M eq(Element element) {
        addCondition(new Equals(element));
        return this.asM();
    }

    /**
     * Equals.
     *
     * Restrict search so that the field value equals the parameter.
     *
     * You might use patterns in your equality.
     *
     * @param fqfield the field on which the search is made
     * @param value the value the element must be equals to
     * @return {@code this}
     * @see Equals
     */
    public M eq(String fqfield, Object value) {
        return eq(Element.get(fqfield), value);
    }

    /**
     * @see Equals
     */
    public M eq(Element element, Object value) {
        ConditionValue s = convertToConditionValue(value);
        addCondition(new Equals(element, s));
        return this.asM();
    }

    /**
     * @see Equals
     */
    public M eqIgnoreCaseAndAccent(Element element) {
        addCondition(new Equals(element, true));
        return this.asM();
    }

    /**
     * Equals.
     *
     * Restrict search so that the field value equals the parameter.
     *
     * You might use patterns in your equality.
     *
     * @param fqfield the field on which the search is made
     * @param value the value the element must be equals to
     * @return {@code this}
     * @see Equals
     */
    public M eqIgnoreCaseAndAccent(String fqfield, Object value) {
        return eqIgnoreCaseAndAccent(Element.get(fqfield), value);
    }

    /**
     * @see Equals
     */
    public M eqIgnoreCaseAndAccent(Element element, Object value) {
        ConditionValue s = convertToConditionValue(value);
        addCondition(new Equals(element, s, true));
        return this.asM();
    }

    /**
     * Extension equals.
     *
     * Restrict search to wikitties that got the extension in parameter.
     *
     * @param extensionName the extension to restrict the results to
     * @return {@code this} with the {@code exteq} restriction added.
     * @see Equals
     */
    public M exteq(String extensionName) {
        return eq(Element.EXTENSION, extensionName);
    }

    /**
     * Id equals.
     *
     * Restrict search to wikitties that got the id in parameter.
     *
     * @param idOrWikitty the id or wikitty to restrict the results to
     * @return {@code this} with the {@code ideq} restriction added.
     * @see Equals
     */
    public M ideq(Object idOrWikitty) {
        return eq(Element.ID, idOrWikitty);
    }

    /**
     * Extension equals.
     *
     * Restrict search to wikitties that got all the extensions in parameter.
     *
     * @param extensionNames list of the extension to restrict the results to
     * @return {@code this} with the {@code exteq} restriction added.
     * @see #containsAll(Element)
     */
    public M extContainsAll(Collection<String> extensionNames) {
        return containsAll(Element.EXTENSION, extensionNames);
    }

    /**
     * @see #containsAll(Element)
     */
    public M extContainsAll(String ext1, String ... exts) {
        List<String> l = new LinkedList<String>();
        l.add(ext1);
        l.addAll(Arrays.asList(exts));
        return containsAll(Element.EXTENSION, l);
    }

    /**
     * Extension equals.
     *
     * Restrict search to wikitties that got one of the extensions in parameter.
     *
     * @param extensionNames list of the extension to restrict the results to
     * @return {@code this} with the {@code exteq} restriction added.
     * @see #containsOne(Element)
     */
    public M extContainsOne(Collection<String> extensionNames) {
        return containsOne(Element.EXTENSION, extensionNames);
    }

    /**
     * @see #containsOne(Element)
     */
    public M extContainsOne(String ext1, String ... exts) {
        List<String> l = new LinkedList<String>();
        l.add(ext1);
        l.addAll(Arrays.asList(exts));
        return containsOne(Element.EXTENSION, l);
    }

    /**
     * @see NotEquals
     */
    public M ne(Element element) {
        addCondition(new NotEquals(element));
        return this.asM();
    }

    /**
     * Not equals.
     *
     * Restrict search to elements that are not equals to the value given in
     * parameter.
     *
     * @param fqfield the element on which the restriction is put
     * @param value the value the element must not be equals to.
     * @return {@code this} with the {@code neq} restriction added.
     * @see NotEquals
     */
    public M ne(String fqfield, Object value) {
        return ne(Element.get(fqfield), value);
    }

    /**
     * @see NotEquals
     */
    public M ne(Element element, Object value) {
        ConditionValue s = convertToConditionValue(value);
        addCondition(new NotEquals(element, s));
        return this.asM();
    }

    /**
     * @see NotEquals
     */
    public M neIgnoreCaseAndAccent(Element element) {
        addCondition(new NotEquals(element, true));
        return this.asM();
    }

    /**
     * Not equals.
     *
     * Restrict search to elements that are not equals to the value given in
     * parameter.
     *
     * @param fqfield the element on which the restriction is put
     * @param value the value the element must not be equals to.
     * @return {@code this} with the {@code neq} restriction added.
     * @see NotEquals
     */
    public M neIgnoreCaseAndAccent(String fqfield, Object value) {
        return neIgnoreCaseAndAccent(Element.get(fqfield), value);
    }

    /**
     * @see NotEquals
     */
    public M neIgnoreCaseAndAccent(Element element, Object value) {
        ConditionValue s = convertToConditionValue(value);
        addCondition(new NotEquals(element, s, true));
        return this.asM();
    }

    /**
     * Extension not equals.
     *
     * Restrict search to wikitties that do not get the extension given in
     * parameter.
     *
     * @param extensionName the extension that the wikitties must not have.
     * @return {@code this} with the {@code extneq} restriction added.
     * @see NotEquals
     */
    public M extne(String extensionName) {
        return ne(Element.EXTENSION, extensionName);
    }

    /**
     * Id not equals.
     *
     * Restrict search to wikitties that do not have the id given in parameter.
     *
     * @param idOrWikitty the id the wikitties must not have.
     * @return {@code this} with the {@code idne} restriction added.
     * @see NotEquals
     */
    public M idne(Object idOrWikitty) {
        return ne(Element.ID, idOrWikitty);
    }

    /**
     * @see Greater
     */
    public M gt(Element element) {
        addCondition(new Greater(element));
        return this.asM();
    }

    /**
     * Greater than.
     *
     * Search if an element value is greater than the parameter.
     *
     * @param fqfield the element on which the restriction is put
     * @param value the value to be compared to
     * @return {@code this} with the {@code gt} restriction added.
     * @see Greater
     */
    public M gt(String fqfield, Object value) {
        return gt(Element.get(fqfield), value);
    }

    /**
     * @see Greater
     */
    public M gt(Element element, Object value) {
        ConditionValue s = convertToConditionValue(value);
        addCondition(new Greater(element, s));
        return this.asM();
    }

    /**
     * @see GreaterOrEquals
     */
    public M ge(Element element) {
        addCondition(new GreaterOrEquals(element));
        return this.asM();
    }

    /**
     * Greater than or equals.
     *
     * Search if an element value is greater than or equals to the parameter.
     *
     * @param fqfield the field on which the search is made
     * @param value the value to be compared to
     * @return {@code this} with the {@code ge} restriction added.
     * @see GreaterOrEquals
     */
    public M ge(String fqfield, Object value) {
        return ge(Element.get(fqfield), value);
    }

    /**
     * @see GreaterOrEquals
     */
    public M ge(Element element, Object value) {
        ConditionValue s = convertToConditionValue(value);
        addCondition(new GreaterOrEquals(element, s));
        return this.asM();
    }

    /**
     * @see Less
     */
    public M lt(Element element) {
        addCondition(new Less(element));
        return this.asM();
    }

    /**
     * Less than.
     *
     * Search if an element value is less than the parameter.
     *
     * @param fqfield the element on which the restriction is put
     * @param value the value to be compared to
     * @return {@code this} with the {@code lt} restriction added.
     * @see Less
     */
    public M lt(String fqfield, Object value) {
        return lt(Element.get(fqfield), value);
    }

    /**
     * @see Less
     */
    public M lt(Element element, Object value) {
        ConditionValue s = convertToConditionValue(value);
        addCondition(new Less(element, s));
        return this.asM();
    }

    /**
     * @see LessOrEquals
     */
    public M le(Element element) {
        addCondition(new LessOrEquals(element));
        return this.asM();
    }

    /**
     * Less than or equals.
     *
     * Search if an element value is less than or equals to the parameter.
     *
     * @param fqfield the element on which the restriction is put.
     * @param value the value to be compared to.
     * @return {@code this} with the {@code le} restriction added.
     * @see LessOrEquals
     */
    public M le(String fqfield, Object value) {
        return le(Element.get(fqfield), value);
    }

    /**
     * @see LessOrEquals
     */
    public M le(Element element, Object value) {
        ConditionValue s = convertToConditionValue(value);
        addCondition(new LessOrEquals(element, s));
        return this.asM();
    }

    /**
     * @see Between
     */
    public M bw(Element element) {
        addCondition(new Between(element));
        return this.asM();
    }

    /**
     * Between.
     *
     * Restrict search so that the element value is between the lower and upper
     * values (it can also be equals).
     *
     * @param fqfield the element on which the restriction is put.
     * @param lowerValue the lower bound.
     * @param upperValue the upper bound.
     * @return {@code this} with the {@code le} restriction added.
     * @see Between
     */
    public M bw(String fqfield, Object lowerValue, Object upperValue) {
        return bw(Element.get(fqfield), lowerValue, upperValue);
    }

    /**
     * @see Between
     */
    public M bw(Element element, Object lowerValue, Object upperValue) {
        ConditionValue min = convertToConditionValue(lowerValue);
        ConditionValue max = convertToConditionValue(upperValue);
        addCondition(new Between(element, min, max));
        return this.asM();
    }

    /**
     * Starts with.
     *
     * Search if an element starts with the value in parameter.
     *
     * @param fqfield the element on which the restriction is put.
     * @param value the value the element must start with.
     * @return {@code this} with the {@code sw} restriction added.
     * @see Equals
     */
    public M sw(String fqfield, String value) {
        return sw(Element.get(fqfield), value);
    }

    /**
     * @see Equals
     */
    public M sw(Element element, String value) {
        addCondition(new Equals(element, value + "*"));
        return this.asM();
    }

    /**
     * Not starts with.
     *
     * Search if an element does not starts with the value in parameter.
     *
     * @param fqfield the element on which the restriction is put.
     * @param value the value the element must not start with.
     * @return {@code this} with the {@code nsw} restriction added.
     * @see NotEquals
     */
    public M notsw(String fqfield, String value) {
        return notsw(Element.get(fqfield), value);
    }

    /**
     * @see NotEquals
     */
    public M notsw(Element element, String value) {
        addCondition(new NotEquals(element, value + "*"));
        return this.asM();
    }

    /**
     * Ends with.
     *
     * Search if an element ends with the value in parameter.
     *
     * @param fqfield the element on which the restriction is put
     * @param value the value the element must ends with.
     * @return {@code this} with the {@code ew} restriction added.
     * @see Equals
     */
    public M ew(String fqfield, Object value) {
        return ew(Element.get(fqfield), value);
    }

    /**
     * @see Equals
     */
    public M ew(Element element, Object value) {
        addCondition(new Equals(element, "*" + value));
        return this.asM();
    }

    /**
     * Not ends with.
     *
     * Search if an element does not ends with the value in parameter.
     *
     * @param fqfield the element on which the restriction is put
     * @param value the value the element must not ends with.
     * @return {@code this} with the {@code notew} restriction added.
     * @see NotEquals
     */
    public M notew(String fqfield, Object value) {
        return notew(Element.get(fqfield), value);
    }

    /**
     * @see NotEquals
     */
    public M notew(Element element, Object value) {
        addCondition(new NotEquals(element, "*" + value));
        return this.asM();
    }

    /**
     * Keyword.
     *
     * Search if the value in parameter is present in any field of any
     * extension.
     *
     * @return {@code this} with the {@code keyword} restriction added.
     * @see Keyword
     */
    public M keyword() {
        addCondition(new Keyword());
        return this.asM();
    }

    /**
     * Keyword.
     *
     * Search if the value in parameter is present in any field of any
     * extension.
     *
     * @param value the value to find.
     * @return {@code this} with the {@code keyword} restriction added.
     * @see Keyword
     */
    public M keyword(Object value) {
        ConditionValue s = convertToConditionValue(value);
        addCondition(new Keyword().addCondition(s));
        return this.asM();
    }

    /**
     * Is null.
     *
     * Check that a field is null.
     *
     * @param fqfield the field that must be null.
     * @return {@code this} with the {@code isNull} restriction added.
     * @see Null
     */
    public M isNull(String fqfield) {
        return isNull(Element.get(fqfield));
    }

    /**
     * @see Null
     */
    public M isNull(Element element) {
        addCondition(new Null(element));
        return this.asM();
    }

    /**
     * Is not null.
     *
     * Check that a field is not null.
     *
     * @param fqfield the field that must not be null.
     * @return {@code this} with the {@code isNotNull} restriction added.
     * @see NotNull
     */
    public M isNotNull(String fqfield) {
        return isNotNull(Element.get(fqfield));
    }

    /**
     * @see NotNull
     */
    public M isNotNull(Element element) {
        addCondition(new NotNull(element));
        return this.asM();
    }

    /**
     * False.
     *
     * Add a restriction that always return false.
     *
     * @return {@code this} with the {@code rFalse} restriction added.
     * @see False
     */
    public M rFalse() {
        addCondition(new False());
        return this.asM();
    }

    /**
     * True.
     *
     * Add a restriction that always return true.
     *
     * @return {@code this} with the {@code rTrue} restriction added.
     * @see True
     */
    public M rTrue() {
        addCondition(new True());
        return this.asM();
    }

    /**
     * @see Like
     */
    public M like(Element element) {
        addCondition(new Like(element));
        return this.asM();
    }

    /**
     * Like.
     *
     * Check that a string is present in a field. For example "tric" is present
     * in "Restriction".
     *
     * @param fqfield the element on which the restriction is put
     * @param value
     * @return {@code this}
     * @see Like
     */
    public M like(String fqfield, Object value) {
        return like(Element.get(fqfield), value);
    }

    /**
     * @see Like
     */
    public M like(Element element, Object value) {
        ConditionValue s = convertToConditionValue(value);
        Like c = new Like(element, s);
        addCondition(c);
        return this.asM();
    }

    /**
     * @see Unlike
     */
    public M unlike(Element element) {
        addCondition(new Unlike(element));
        return this.asM();
    }

    /**
     * Unlike.
     *
     * @param fqfield the element on which the restriction is put
     * @param value
     * @return {@code this}
     * @see Unlike
     */
    public M unlike(String fqfield, Object value) {
        return unlike(Element.get(fqfield), value);
    }

    /**
     * @see Unlike
     */
    public M unlike(Element element, Object value) {
        ConditionValue s = convertToConditionValue(value);
        Unlike c = new Unlike(element, s);
        addCondition(c);
        return this.asM();
    }

    /**
     * Not (sub query). To close this sub query you must used {@link #close()}
     * <ul>
     * <li>ex: WikittyQueryMaker().not().rTrue().close().and().rTrue().rFalse().close().or().rTrue().rFalse().close();</li>
     * </ul>
     *
     * @see Not
     */
    public M not() {
        Condition child = new Not();
        addCondition(child);
        return this.asM();
    }

    /**
     * Or (sub query). To close this sub query you must used {@link #close()}
     * <ul>
     * <li>ex: WikittyQueryMaker().not().rTrue().close().and().rTrue().rFalse().close().or().rTrue().rFalse().close();</li>
     * </ul>
     *
     * @see Or
     */
    public M or() {
        Condition child = new Or();
        addCondition(child);
        return this.asM();
    }

    /**
     * And (sub query). To close this sub query you must used {@link #close()}
     * <ul>
     * <li>ex: WikittyQueryMaker().not().rTrue().close().and().rTrue().rFalse().close().or().rTrue().rFalse().close();</li>
     * </ul>
     *
     * @see And
     */
    public M and() {
        Condition child = new And();
        addCondition(child);

        return this.asM();
    }

    /*
     * Add {@link Select}, this condition must be first or
     * @param element le champs dont il faut extraire les donnees
     * @return {@code this}
     * @see {@link Select}
     */
//    public M select() {
//        return select(null);
//    }

    /*
     * @see {@link Select}
     */
    public M select() {
        return select((WikittyQueryFunction)null);
    }

    /*
     * @see {@link Select}
     */
    public M select(WikittyQueryFunction f) {
        Condition child = new Select(f);

        addCondition(child);
        return this.asM();
    }

    /*
     * @see {@link Select}
     */
    public M select(String e) {
        return select(Element.get(e));
    }

    /*
     * @see {@link Select}
     */
    public M select(Element e) {
        return select(new FunctionFieldValue(null, e.getValue()));
    }

//    public M addFunction(WikittyQueryFunction f) {
//        // ajout function sur select ou args de function
//        // close sur function
//    }

    /**
     * Close last non terminal condition (or, and, not, in). Or Last Function in select clause
     * <ul>
     * <li>ex: WikittyQueryMaker().not().rTrue().close().and().rTrue().rFalse().close().or().rTrue().rFalse().close();</li>
     * </ul>
     * @return
     */
    public M close() {
        Condition c = getOpenStack().peek();
        WikittyQueryFunction f = openStackFunction.peek();
        if (c instanceof Select && f != null) {
            if (openStackFunction.size() == 1) {
                // si on enleve l'element, il n'y a plus rien, ce close est donc
                // l'equivalent d'un Where
                where();
            } else {
                 // on depile (il avait deja ete ajoute au parent, rien d'autre a faire)
                openStackFunction.pop();
            }
        } else {
            getOpenStack().pop(); // on en ferme 1 obligatoirement
            // on cherche a en fermer plus
            closeIfNecessary();
        }
        return this.asM();
    }

    /**
     * Ferme la construction de la condition et la met en place dans la
     * WikittyQuery qui sera retournee
     * 
     * @return un objet WikittyQuery soit un nouveau soit celui passe dans le 
     * constructeur
     */
    public WikittyQuery end() {
        WikittyQuery result = getQuery();
        result.setCondition(getCondition());
        // on interdit les modifications futur
        openStack = null;
        
        return result;
    }
}
