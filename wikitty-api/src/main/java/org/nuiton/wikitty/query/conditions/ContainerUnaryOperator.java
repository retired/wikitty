/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.query.WikittyQueryVisitor;

/**
 * Cette classe est la classe parente de tous les objets ayant en interne
 * une restriction non terminale (ex: Not)
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public abstract class ContainerUnaryOperator extends ContainerOperator {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    protected Condition subCondition;

    public ContainerUnaryOperator() {
    }

    public ContainerUnaryOperator(Condition restriction) {
        this.subCondition = restriction;
    }

    @Override
    public boolean waitCondition() {
        boolean result = subCondition == null;
        return result;
    }

    @Override
    public Condition addCondition(Condition c) {
        if (c instanceof ConditionValue) {
            throw new WikittyException(String.format(
                        "Condition (%s) can't have condition '%s' as child",
                        getClass().getSimpleName(),
                        ClassUtils.getShortCanonicalName(c, "null")));
        } else {
            if (subCondition == null) {
                this.subCondition = c;
            } else {
                throw new WikittyException(String.format(
                        "Condition (%s) can't have more than one condition",
                        getClass().getSimpleName()));
            }
        }
        return this;
    }

    @Override
    public void accept(WikittyQueryVisitor visitor) {
        boolean walk = visitor.visitEnter(this);
        if (walk && subCondition != null) {
            subCondition.accept(visitor);
        }
        visitor.visitLeave(this, walk);
    }

    @Override
    boolean equalsDeep(Object other) {
        ContainerUnaryOperator op = (ContainerUnaryOperator)other;
        boolean result = ObjectUtils.equals(
                this.getSubCondition(), op.getSubCondition());
        return result;
    }

    public Condition getSubCondition() {
        return subCondition;
    }

}
