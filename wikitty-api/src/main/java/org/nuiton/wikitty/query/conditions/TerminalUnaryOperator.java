/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import org.nuiton.wikitty.entities.Element;
import org.apache.commons.lang3.ObjectUtils;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class TerminalUnaryOperator extends TerminalOperator {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    protected Element element;

    /**
     * Constructor with all parameters initialized
     *
     * @param element value is internaly copied to prevent external modification
     */
    public TerminalUnaryOperator(Element element) {
        this.element = element;
    }

    @Override
    public boolean waitCondition() {
        // unary don't has value, then never wait for condition
        return false;
    }

    public Element getElement() {
        return element;
    }

    @Override
    boolean equalsDeep(Object other) {
        TerminalUnaryOperator op = (TerminalUnaryOperator)other;
        boolean result = ObjectUtils.equals(this.getElement(), op.getElement());
        return result;
    }
}
