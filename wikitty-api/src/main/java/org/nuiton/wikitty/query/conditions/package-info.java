/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * Hierarchie
 * <ul>
 * <li> {@link org.nuiton.wikitty.query.conditions.Condition}
 *   <ul>
 *   <li> {@link org.nuiton.wikitty.query.conditions.ContainerOperator} les operateurs qui contiennent d'autre operateur
 *     <ul>
 *       <li> {@link org.nuiton.wikitty.query.conditions.ContainerUnaryOperator} ne contient qu'un fils
 *         <ul>
 *         <li> {@link org.nuiton.wikitty.query.conditions.Not}</li>
 *         <li> {@link org.nuiton.wikitty.query.conditions.Select}</li>
 *         </ul></li>
 *       <li> {@link org.nuiton.wikitty.query.conditions.ContainerNaryOperator} contient N fils, et doivent
 *            explicitement etre ferme via un close dans
 *            {@link org.nuiton.wikitty.query.WikittyQueryMaker#close}
 *         <ul>
 *         <li> {@link org.nuiton.wikitty.query.conditions.And}</li>
 *         <li> {@link org.nuiton.wikitty.query.conditions.Or}</li>
 *        </ul></li>
 *       </ul></li>
 *     </ul></li>
 *   </ul>
 *   <ul>
 *   <li> {@link org.nuiton.wikitty.query.conditions.TerminalOperator} ni champs, ni valeur
 *   <ul>
 *     <li> {@link org.nuiton.wikitty.query.conditions.True}</li>
 *     <li> {@link org.nuiton.wikitty.query.conditions.False}</li>
 *     <li> {@link org.nuiton.wikitty.query.conditions.TerminalUnaryOperator} un champs seulement
 *     <ul>
 *       <li> {@link org.nuiton.wikitty.query.conditions.Null}</li>
 *       <li> {@link org.nuiton.wikitty.query.conditions.NotNull}</li>
 *     </ul></li>
 *     <li> {@link org.nuiton.wikitty.query.conditions.TerminalBinaryOperator} un champs et une valeur
 *     <ul>
 *       <li> {@link org.nuiton.wikitty.query.conditions.Equals}</li>
 *       <li> {@link org.nuiton.wikitty.query.conditions.Greater}</li>
 *       <li> {@link org.nuiton.wikitty.query.conditions.GreaterOrEquals}</li>
 *       <li> {@link org.nuiton.wikitty.query.conditions.Keyword} binary car automatiquement sur tous les champs</li>
 *       <li> {@link org.nuiton.wikitty.query.conditions.Less}</li>
 *       <li> {@link org.nuiton.wikitty.query.conditions.LessOrEquals}</li>
 *       <li> {@link org.nuiton.wikitty.query.conditions.Like}</li>
 *       <li> {@link org.nuiton.wikitty.query.conditions.NotEquals}</li>
 *     </ul></li>
 *     <li> {@link org.nuiton.wikitty.query.conditions.TerminalTernaryOperator} un champs, et deux valeur
 *     <ul>
 *       <li> {@link org.nuiton.wikitty.query.conditions.Between}</li>
 *     </ul></li>
 *     <li> {@link org.nuiton.wikitty.query.conditions.TerminalNaryOperator} un champs et N valeurs, et doivent
 *            explicitement etre ferme via un close dans
 *            {@link org.nuiton.wikitty.query.WikittyQueryMaker#close} si les
 *            valeur ne sont pas passee au moment de la construction
 *     <ul>
 *       <li> {@link org.nuiton.wikitty.query.conditions.ContainsAll}</li>
 *       <li> {@link org.nuiton.wikitty.query.conditions.ContainsOne}</li>
 *     </ul></li>
 *   </ul>
 *   <ul>
 *   <li> {@link org.nuiton.wikitty.query.conditions.ConditionValue} les valeurs possibles pour un champs
 *     <ul>
 *     <li> {@link org.nuiton.wikitty.query.conditions.ConditionValueString}</li>
 *     <li> {@link org.nuiton.wikitty.query.conditions.Select}</li>
 *     </ul></li>
 *   </ul>
 *   </li>
 * </ul>
 */
package org.nuiton.wikitty.query.conditions;
