/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.query.conditions.Condition;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.entities.ElementField;
import org.nuiton.wikitty.query.conditions.Select;

/**
 * Classe permettant de faire des recherches dans les données.
 * <p>
 * Pour creer facilement la condition le plus simple est d'utiliser
 * {@link WikittyQueryMaker} pour creer le WikittyQuery en appelant la
 * method {@link WikittyQueryMaker#end()} a la fin.
 * <p>
 * Pour offrir au utilisateur de vos applications la possibilite d'ecrire
 * eux meme des requetes vous pouvez utiliser {@link WikittyQueryParser}
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyQuery implements Serializable {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyQuery.class);

    private static final long serialVersionUID = 1L;

    /** Use to not limit result. */
    static final public int MAX = Integer.MAX_VALUE;

    /** query name */
    protected String name;
    /** query condition */
    protected Condition condition;
    
    /** First index to get result. */
    protected int offset = 0;
    /** Number of result to retrieve. 100 by default. */
    protected int limit = 100;

    /**
     * nombre minimum de valeur pour qu'une valeur apparaisse dans les facets.
     * par defaut, il doit y avoir plus que 1 valeur.
     */
    protected int facetMinCount = 1;
    /**
     * Nombre maximum de topic par facet a retourner. Par default on en
     * retourne 100.
     */
    protected int facetLimit = 100;
    /** sort topic order, default is sorted on count */
    protected FacetSortType facetSort = FacetSortType.count;

    /** Facet on condition. */
    protected List<FacetQuery> facetQuery;
    /** Facet on field. */
    protected List<Element> facetField;
    /**
     * if true facet is done on extension name. Extention facet can be found
     * in {@link WikittyQueryResult#getFacets()} with key
     * {@link org.nuiton.wikitty.entities.Element#EXTENSION}
     */
    protected boolean facetExtension = false;

    /** Sort ascending on fields. */
    protected List<Element> sortAscending;
    /** Sort descending on fields. */
    protected List<Element> sortDescending;

    /** Profondeur de recherche sur les champs de type wikitty. Ce qui permet
     de ne pas passer un id ou faire un select, mais directement passer une chaine.
     Par defaut pas de recherche profonde, une valeur superieur a 1 est
     dangereuse pour les performances. */
    protected int wikittyFieldSearchDepth = 0;

    /**
     * indique si dans les resultats on doit ajouter les id des objets que
     * l'utilisateur ne peut pas lire.
     * par defaut on ne remonte pas les objets que l'utilisateur ne peut pas lire.
     * Si vrai alors on ne remonte pas les objets, si faux et que la requete
     * n'est pas un select on remonte les objets non lisible
     */
    protected boolean checkAuthorisation = true;

    /** create anonymous query */
    public WikittyQuery() {
    }

    public WikittyQuery(Condition condition) {
        setCondition(condition);
    }

    /**
     * indique si dans les resultats on doit ajouter les id des objets que
     * l'utilisateur ne peut pas lire.
     * par defaut on ne remonte pas les objets que l'utilisateur ne peut pas lire.
     * Si vrai alors on ne remonte pas les objets, si faux et que la requete
     * n'est pas un select on remonte les objets non lisible
     *
     * si checkAuthorisation est:
     * <ul>
     * <li>true alors filtre actif</li>
     * <li>false et requete sans select alors filtre inactif</li>
     * <li>false et requete select alors filtre <strong>actif</strong></li>
     * </ul>
     */
    public boolean isCheckAuthorisation() {
        boolean result = checkAuthorisation || condition instanceof Select;
        return result;
    }

    public void setCheckAuthorisation(boolean checkAuthorisation) {
        this.checkAuthorisation = checkAuthorisation;
    }

    public WikittyQuery copy() {
        WikittyQueryVisitorCopy v = new WikittyQueryVisitorCopy();
        accept(v);
        return v.getQuery();
    }

    @Override
    public String toString() {
        WikittyQueryVisitorToString v = new WikittyQueryVisitorToString();
        accept(v);
        String result = v.getText();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        boolean result;

        if (o == null) {
            result = false;
        } else if (ObjectUtils.equals(this.getClass(), o.getClass())) {
            WikittyQuery other = (WikittyQuery)o;
            result = ObjectUtils.equals(this.getName(), other.getName()) &&
                    ObjectUtils.equals(this.getOffset(), other.getOffset()) &&
                    ObjectUtils.equals(this.getLimit(), other.getLimit()) &&
                    ObjectUtils.equals(this.isFacetExtension(), other.isFacetExtension()) &&
                    ObjectUtils.equals(this.getFacetField(), other.getFacetField()) &&
                    ObjectUtils.equals(this.getSortAscending(), other.getSortAscending()) &&
                    ObjectUtils.equals(this.getSortDescending(), other.getSortDescending()) &&
                    ObjectUtils.equals(this.getFacetLimit(), other.getFacetLimit()) &&
                    ObjectUtils.equals(this.getFacetSort(), other.getFacetSort()) &&
                    ObjectUtils.equals(this.getFacetMinCount(), other.getFacetMinCount()) &&
                    ObjectUtils.equals(this.getFacetQuery(), other.getFacetQuery()) &&
                    ObjectUtils.equals(this.getCondition(), other.getCondition()) &&
                    ObjectUtils.equals(this.getWikittyFieldSearchDepth(), other.getWikittyFieldSearchDepth());
        } else {
            result = false;
        }

        return result;
    }

    /**
     * Inefficient hashCode method but necessary with overloading of equals method
     * This method return hashCode of object class. There is no better way to
     * compute stable hashCode in time
     *
     * @return
     */
    @Override
    public int hashCode() {
        int result = getClass().hashCode();
        return result;
    }

    /** create named query */
    public WikittyQuery(String name) {
        this.name = name;
    }

    /** create named query with condition*/
    public WikittyQuery(String name, Condition condition) {
        this.name = name;
        setCondition(condition);
    }

    /**
     * Indique si la query commence par une clause Select
     * @return 
     * @since 3.9
     */
    public boolean isSelectQuery() {
        boolean result = condition instanceof Select;
        return result;
    }

    /**
     * Retourne la clause Select de la Query, ou null si la query n'a pas de select
     * @return
     * @since 3.10
     */
    public Select getSelect() {
        Select result = null;
        if (isSelectQuery()) {
            result = (Select)getCondition();
        }
        return result;
    }

    /**
     * retourne la clause where de la query, la clause where peut-etre la meme
     * chose que le {@link #getCondition} si la clause n'a pas de select
     * @return
     * @since 3.10
     */
    public Condition getWhere() {
        Condition result = getCondition();
        if (isSelectQuery()) {
            result = getSelect().getSubCondition();
        }
        return result;
    }

    /**
     * retourne une copie de cette query sans la clause select. Si cette
     * query n'avait pas de select la query retournee est strictement equivalente.
     *
     * @return
     * @since 3.10
     */
    public WikittyQuery getWhereQuery() {
        Condition newCond = getWhere();
        // copy de la query sans le select
        WikittyQuery result = this.copy();
        result.setCondition(newCond);
        return result;
    }

    public void accept(WikittyQueryVisitor visitor) {
        boolean walk = visitor.visitEnter(this);
        if (walk && condition != null) {
            condition.accept(visitor);
        }
        visitor.visitLeave(this, walk);
    }

    public String getName() {
        return name;
    }

    public WikittyQuery setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Indique le premier element retourne par la requete. Si la requete peut
     * retourner 100 reponses, et qu'on fixe l'offset a 25 alors il n'y aura
     * au maximume que 75 reponses retournees.
     * @return 
     */
    public int getOffset() {
        return offset;
    }

    /**
     * Indique le premier element retourne par la requete. Si la requete peut
     * retourner 100 reponses, et qu'on fixe l'offset a 25 alors il n'y aura
     * au maximume que 75 reponses retournees.
     * @return
     */
    public WikittyQuery setOffset(int offset) {
        this.offset = offset;
        return this;
    }

    /**
     * @deprecated since 3.5 use {@link #getOffset()}
     */
    @Deprecated
    public int getFirst() {
        return offset;
    }

    /**
     * @deprecated since 3.5 use {@link #setOffset(int)}
     */
    @Deprecated
    public WikittyQuery setFirst(int first) {
        this.offset = first;
        return this;
    }

    /**
     * Get result limit.
     * 
     * 0 return no result (usefull for facets and result count). -1 return all
     * results.
     * 
     * @return result count
     */
    public int getLimit() {
        return limit;
    }

    /**
     * Set result limit.
     * 
     * 0 return no result (usefull for facets and result count). negative value
     * or {@link #MAX} return all results.
     * 
     * @param count new count
     * @return this
     */
    public WikittyQuery setLimit(int count) {
        if (count < 0) {
            count = MAX;
        }
        this.limit = count;
        return this;
    }

    public int getFacetMinCount() {
        return facetMinCount;
    }

    public WikittyQuery setFacetMinCount(int facetMinCount) {
        this.facetMinCount = facetMinCount;
        return this;
    }

    public int getFacetLimit() {
        return facetLimit;
    }

    public WikittyQuery setFacetLimit(int facetLimit) {
        this.facetLimit = facetLimit;
        return this;
    }

    public WikittyQuery setFacetSort(FacetSortType facetSort) {
        this.facetSort = facetSort;
        return this;
    }

    public FacetSortType getFacetSort() {
        return facetSort;
    }

    public List<FacetQuery> getFacetQuery() {
        if (facetQuery == null) {
            facetQuery = new LinkedList<FacetQuery>();
        }
        return facetQuery;
    }

    public WikittyQuery addFacetQuery(FacetQuery facetQuery) {
        getFacetQuery().add(facetQuery);
        return this;
    }

    public WikittyQuery addFacetQuery(String name, Condition condition) {
        getFacetQuery().add(new FacetQuery(name, condition));
        return this;
    }

    public WikittyQuery setFacetQuery(FacetQuery ... facetQuery) {
        this.facetQuery = new LinkedList<FacetQuery>(Arrays.asList(facetQuery));
        return this;
    }

    public List<Element> getFacetField() {
        if (facetField == null) {
            facetField = new LinkedList<Element>();
        }
        return facetField;
    }

    public WikittyQuery addFacetField(Element field) {
        getFacetField().add(field);
        return this;
    }

    public WikittyQuery setFacetField(Element ... facetField) {
        this.facetField = new LinkedList<Element>(Arrays.asList(facetField));
        return this;
    }

    public WikittyQuery setFacetField(List<Element> facetField) {
        this.facetField = facetField;
        return this;
    }

    public boolean isFacetExtension() {
        return facetExtension;
    }

    public WikittyQuery setFacetExtension(boolean facetExtension) {
        this.facetExtension = facetExtension;
        return this;
    }


    /**
     * Get field names where sort is configured ascending.
     *
     * @return field names
     */
    public List<Element> getSortAscending() {
        if (sortAscending == null) {
            sortAscending = new LinkedList<Element>();
        }
        return sortAscending;
    }

    public WikittyQuery addSortAscending(ElementField ... field) {
        getSortAscending().addAll(Arrays.asList(field));
        return this;
    }

    public WikittyQuery setSortAscending(Element ... sortAscending) {
        this.sortAscending = new LinkedList<Element>(Arrays.asList(sortAscending));
        return this;
    }

    public WikittyQuery setSortAscending(List<Element> sortAscending) {
        this.sortAscending = sortAscending;
        return this;
    }

    /**
     * Get field names where sort is configured descending.
     *
     * @return field names
     */
    public List<Element> getSortDescending() {
        if (sortDescending == null) {
            sortDescending = new LinkedList<Element>();
        }
        return sortDescending;
    }

    public WikittyQuery addSortDescending(Element ... field) {
        getSortDescending().addAll(Arrays.asList(field));
        return this;
    }

    public WikittyQuery setSortDescending(Element ... sortDescending) {
        this.sortDescending = new LinkedList<Element>(Arrays.asList(sortDescending));
        return this;
    }

    public WikittyQuery setSortDescending(List<Element> sortDescending) {
        this.sortDescending = sortDescending;
        return this;
    }

    public Condition getCondition() {
        return condition;
    }

    public WikittyQuery setCondition(Condition condition) {
        this.condition = condition;
        return this;
    }

    public int getWikittyFieldSearchDepth() {
        return wikittyFieldSearchDepth;
    }

    public WikittyQuery setWikittyFieldSearchDepth(int wikittyFieldSearchDepth) {
        this.wikittyFieldSearchDepth = wikittyFieldSearchDepth;
        return this;
    }

}
