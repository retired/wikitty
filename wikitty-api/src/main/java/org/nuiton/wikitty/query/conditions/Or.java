/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import java.util.Arrays;
import java.util.List;

import org.nuiton.wikitty.query.WikittyQueryMaker;

/**
 * Or operator is used to build disjunctive restriction for request on content.
 * It take at least 2 args. <br>
 * <br>
 * For example, use:
 * <ul>
 * <li>{@link WikittyQueryMaker}.or( restriction1, restriction2 )</li>
 * <li>{@link WikittyQueryMaker}.or( restriction1, restriction2, restriction3 )</li>
 * <li>{@link WikittyQueryMaker}.or( my_JavaUtilList_Of_Restriction_Witch_Size_Is_Upper_Than2 )</li>
 * </ul>
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class Or extends ContainerNaryOperator {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    public Or(Condition ... c) {
        super(Arrays.asList(c));
    }
    /**
     * Constructor with all parameters initialized
     * 
     * @param conditions
     */
    public Or(List<Condition> conditions) {
        super(conditions);
    }
}
