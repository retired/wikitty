/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query;

import java.util.Comparator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Comparateur de topic sur le nombre d'occurence du topic dans la facet.
 * Cela permet de presenter a l'utilisateur les topics les plus rependu en premier
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FacetTopicCountComparator implements Comparator<FacetTopic> {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(FacetTopicCountComparator.class);

    final static public Comparator<FacetTopic> instance = new FacetTopicCountComparator();

    /**
     * You must use {@link #instance}
     */
    public FacetTopicCountComparator() {
    }

    public int compare(FacetTopic o1, FacetTopic o2) {
        int thisVal = o1.getCount();
        int anotherVal = o2.getCount();
         // code recupere de Integer.compareTo (mais inverse pour les 1/-1)
        return (thisVal<anotherVal ? 1 : (thisVal==anotherVal ? 0 : -1));
    }
}
