/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.query.WikittyQueryMaker;

/**
 * Equals operator is used to build restriction containing "field == value"
 * where value could be an Id, a String, an Integer, a Float or a Date, ...
 * <p>
 * For example, use: {@link WikittyQueryMaker}.eq( myElement , "REF1234567890" )
 * <p>
 * This operator used for String check strict equality (case sensitive)
 * You can used '*' in expression at beginning or ending for String equality.
 *
 * <ul>
 * <li> {@link WikittyQueryMaker}.eq("myext.myfield", "*jour") match field "bonjour" but not "BONJOUR"</li>
 * </ul>
 *
 * <p>
 * You can specify ignoreCaseAndAccent to check equality. This mode
 * ignore case and accent. example: "çéçù" == "CecU" in this mode.
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class Equals extends TerminalBinaryOperator {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    protected boolean ignoreCaseAndAccent = false;

    public Equals(Element element) {
        super(element);
    }

    public Equals(Element element, String value) {
        super(element, value);
    }

    public Equals(Element element, ConditionValue value) {
        super(element, value);
    }

    public Equals(Element element, boolean ignoreCaseAndAccent) {
        super(element);
        this.ignoreCaseAndAccent = ignoreCaseAndAccent;
    }

    public Equals(Element element, String value, boolean ignoreCaseAndAccent) {
        super(element, value);
        this.ignoreCaseAndAccent = ignoreCaseAndAccent;
    }

    public Equals(Element element, ConditionValue value, boolean ignoreCaseAndAccent) {
        super(element, value);
        this.ignoreCaseAndAccent = ignoreCaseAndAccent;
    }

    public boolean isIgnoreCaseAndAccent() {
        return ignoreCaseAndAccent;
    }

}
