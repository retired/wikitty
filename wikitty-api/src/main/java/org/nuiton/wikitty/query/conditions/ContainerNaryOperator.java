/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.query.WikittyQueryVisitor;

/**
 * Cette classe est la classe parente de tous les objets ayant en interne
 * une liste de restrictions non terminale (ex: And, Or)
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public abstract class ContainerNaryOperator extends ContainerOperator {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    protected List<Condition> conditions;

    public ContainerNaryOperator() {
    }

    /**
     * Initialyse condition with list passed in argument, parameter list is
     * copied internaly to prevent extern modification
     * 
     * @param c
     */
    public ContainerNaryOperator(List<Condition> c) {
        this.conditions = new LinkedList<Condition>(c);
    }

    @Override
    public boolean waitCondition() {
        // nary allways wait condition
        return true;
    }

    @Override
    public void accept(WikittyQueryVisitor visitor) {
        boolean walk = visitor.visitEnter(this);
        if (walk && conditions != null) {
            boolean notFirst = false;
            for (Condition r : conditions) {
                if (notFirst) {
                    walk = visitor.visitMiddle(this);
                    if (!walk) {
                        // le visiteur demande l'arret de la visite
                        break;
                    }
                } else {
                    notFirst = true;
                }
                r.accept(visitor);
            }
        }
        visitor.visitLeave(this, walk);
    }

    @Override
    boolean equalsDeep(Object other) {
        ContainerNaryOperator op = (ContainerNaryOperator)other;
        boolean result = ObjectUtils.equals(
                this.getConditions(), op.getConditions());
        return result;
    }

    /**
     * Return Restriction list. This list is never null, but can be empty.
     * 
     * @return conditions list
     */
    public List<Condition> getConditions() {
        if (conditions == null) {
            conditions = new LinkedList<Condition>();
        }
        return conditions;
    }

    @Override
    public Condition addCondition(Condition c) {
        if (c != null) {
            if (c instanceof ConditionValue) {
                throw new WikittyException(String.format(
                        "Condition (%s) can't have condition '%s' as child",
                        getClass().getSimpleName(),
                        ClassUtils.getShortCanonicalName(c, "null")));
            } else {
                getConditions().add(c);
            }
        }
        return this;
    }


}
