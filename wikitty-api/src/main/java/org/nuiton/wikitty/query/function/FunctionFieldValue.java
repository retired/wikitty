package org.nuiton.wikitty.query.function;

/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.query.ListObjectOrMap;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.storage.WikittySearchEngine;

/**
 * Permet de recuperer la valeur d'un champs, les Maps retournees contiennent
 * seulement un champs
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FunctionFieldValue extends WikittyQueryFunction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(FunctionFieldValue.class);

    public FunctionFieldValue(String name, String fieldName) {
        super("fieldValue",
                StringUtils.isNotBlank(name)?name:Element.get(fieldName).getValue(),
                Collections.singletonList((WikittyQueryFunction)new FunctionValue(null, Element.get(fieldName).getValue())));
    }

    public FunctionFieldValue(String methodName, String name, List<WikittyQueryFunction> args) {
        super(methodName, name, args);
        if (args.size() != 1) {
            throw new IllegalArgumentException("FieldValue accept only one argument");
        }
    }

    @Override
    public int getNumArg() {
        return 1;
    }

    @Override
    public List<Map<String, Object>> call(
            WikittyQuery query, List<Map<String, Object>> data) {

        WikittyQueryFunction f = getArgs().get(0);
        List<Map<String, Object>> fieldName = f.call(query, data);

        String field = getUniqueValue(fieldName).toString();

        ListObjectOrMap result = new ListObjectOrMap();

        for (Map<String, Object> m : data) {
            Object val = m.get(field);
            if (val instanceof Collection) {
                for (Object o : (Collection)val) {
                    result.add(field, o);
                }
            } else {
                result.add(field, val);
            }
        }

        return result;
    }
}
