package org.nuiton.wikitty.query.function;

/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.query.ListObjectOrMap;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryVisitor;
import org.nuiton.wikitty.services.WikittyTransaction;
import org.nuiton.wikitty.storage.WikittySearchEngine;

/**
 * Permet de stocker une valeur.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FunctionValue extends WikittyQueryFunction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(FunctionValue.class);

    protected Object value;

    public FunctionValue(String name, Object value) {
        super(null, name, null);
        this.value = value;
    }

    @Override
    public int getNumArg() {
        return 0;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public List<Map<String, Object>> call(
            WikittyQuery query, List<Map<String, Object>> data) {
        ListObjectOrMap result = new ListObjectOrMap();
        result.add(getName(), getValue());
        return result;
    }

    @Override
    public void accept(WikittyQueryVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public boolean equals(Object o) {
        boolean result = super.equals(o)
                && ObjectUtils.equals(this.getValue(), ((FunctionValue)o).getValue());
        return result;
    }

}
