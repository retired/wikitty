/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import org.nuiton.wikitty.entities.Element;

/**
 * Between operator is used to build restriction containing "min &lt; element
 * &lt; max" where element could be a Integer, a Float or a Date. <br>
 * <br>
 * For example, use: RestrictionHelper.between( myElement , "15.5" , "22.5" )
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class Between extends TerminalTernaryOperator {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    /**
     * Constructor with all parameters initialized
     *
     * @param element
     */
    public Between(Element element) {
        super(element);
    }

    /**
     * Constructor with all parameters initialized
     *
     * @param element
     * @param min
     * @param max
     */
    public Between(Element element, String min, String max) {
        super(element, min, max);
    }

    /**
     * Constructor with all parameters initialized
     *
     * @param element
     * @param min
     * @param max
     */
    public Between(Element element, ConditionValue min, ConditionValue max) {
        super(element, min, max);
    }

}
