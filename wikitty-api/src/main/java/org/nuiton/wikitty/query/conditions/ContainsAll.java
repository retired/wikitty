/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.query.conditions;

import org.nuiton.wikitty.entities.Element;
import java.util.Collection;

/**
 * ContainsAll permet de definir qu'un champs doit avoir toutes une serie de
 * valeur en meme temps. Cette condition ne s'applique donc qu'a des champs de
 * type Collection (sauf si on fait un ContainsAll avec une seul valeur, ce qui
 * revient a faire un {@link Equals})
 * <p>
 * For example, use:
 * <ul>
 * <li>RestrictionHelper.containsAll( myElement , "value1" )</li>
 * <li>RestrictionHelper.containsAll( myElement , "value1", "value2", ... )</li>
 * <li>RestrictionHelper.containsAll( myElement ,
 * a_list_containing_at_least_one_string )</li>
 * </ul>
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ContainsAll extends TerminalNaryOperator {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    /**
     * Constructor with all parameters initialized
     * 
     * @param element
     * @param values
     */
    public ContainsAll(Element element, Collection<ConditionValue> values) {
        super(element, values);
    }

    public ContainsAll(Element element) {
        super(element);
    }
}
