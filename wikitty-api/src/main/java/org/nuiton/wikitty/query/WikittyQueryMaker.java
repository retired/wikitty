package org.nuiton.wikitty.query;

/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Classe concrete de construction de requete pour la documentation voir
 * {@link WikittyQueryMakerAbstract}
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyQueryMaker extends WikittyQueryMakerAbstract<WikittyQueryMaker> {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyQueryMaker.class);

    public WikittyQueryMaker() {
        super();
    }

    public WikittyQueryMaker(WikittyQuery query) {
        super(query);
    }

    @Override
    protected WikittyQueryMaker asM() {
        return this;
    }

}
