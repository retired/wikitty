package org.nuiton.wikitty.query.function;

/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.query.ListObjectOrMap;
import org.nuiton.wikitty.query.WikittyQuery;

/**
 * Permet de recuperer l'annee d'un champs DATE, les Maps retournees contiennent
 * seulement un champs
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FunctionYear extends WikittyQueryFunction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(FunctionYear.class);

    public FunctionYear(String name) {
        super("year", name, null);
    }

    public FunctionYear(String name, WikittyQueryFunction arg) {
        super("year", name, Collections.singletonList(arg));
    }

public FunctionYear(String methodName, String name, List<WikittyQueryFunction> args) {
        super(methodName, name, args);
        if (args.size() != 1) {
            throw new IllegalArgumentException("Year accept only one argument");
        }
    }

    @Override
    public int getNumArg() {
        return 1;
    }

    @Override
    public List<Map<String, Object>> call(
            WikittyQuery query, List<Map<String, Object>> data) {

        WikittyQueryFunction f = getArgs().get(0);
        data = f.call(query, data);

        ListObjectOrMap result = new ListObjectOrMap();

        for (Map<String, Object> m : data) {
            for (Map.Entry<String, Object> s : m.entrySet()) {
                String k = StringUtils.defaultIfBlank(getName(), s.getKey());
                Object v = s.getValue();
                if (v instanceof Collection) {
                    result.add(k, getYear((Collection)v));
                } else {
                    result.add(k, getYear(v));
                }
            }
        }

        return result;
    }

    protected Collection getYear(Collection c) {
        Collection result = new ArrayList(c.size());
        for (Object o : c) {
            result.add(getYear(o));
        }
        return result;
    }

    protected Object getYear(Object o) {
        Object result = o;
        Date d = null;
        try {
            d = WikittyUtil.toDate(o);
        } catch (Exception eee) {
            log.debug(String.format("can't convert '%s' object to date, keep object and don't extract year", o), eee);
        }
        if (d != null) {
            result = DateUtil.getDefaultCalendar(d).get(Calendar.YEAR);
        }
        return result;
    }
}
