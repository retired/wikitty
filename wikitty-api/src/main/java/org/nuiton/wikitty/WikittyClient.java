/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.TimeLog;
import org.nuiton.wikitty.entities.BusinessEntity;
import org.nuiton.wikitty.entities.BusinessEntityImpl;
import org.nuiton.wikitty.entities.ElementField;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyField;
import org.nuiton.wikitty.entities.WikittyGroup;
import org.nuiton.wikitty.entities.WikittyTokenHelper;
import org.nuiton.wikitty.entities.WikittyTypes;
import org.nuiton.wikitty.entities.WikittyUser;
import org.nuiton.wikitty.query.ListObjectOrMap;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.query.WikittyQueryResultTreeNode;
import org.nuiton.wikitty.query.conditions.Select;
import org.nuiton.wikitty.services.WikittyEvent;
import org.nuiton.wikitty.services.WikittyExtensionMigrationRegistry;
import org.nuiton.wikitty.services.WikittySecurityUtil;
import org.nuiton.wikitty.services.WikittyServiceEnhanced;

/**
 * Wikitty client is object used in client side to access WikittyService.
 * It is used to transform wikitty object used by {@link WikittyService}
 * into business objects used by applications.
 * 
 * It also manage {@link #securityToken} for {@link org.nuiton.wikitty.WikittyService}.
 *
 * All method that need {@link #securityToken} and {@link org.nuiton.wikitty.WikittyService}
 * must be in this class and not in {@link WikittyUtil}
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyClient {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private TimeLog timeLog = new TimeLog(WikittyClient.class);

    protected ApplicationConfig config = null;
    /** Delegated wikitty service. */
    protected WikittyServiceEnhanced wikittyService;

    /**
     * Security token.
     * 
     * @see org.nuiton.wikitty.services.WikittyServiceAuthentication#login(String, String)
     */
    protected String securityToken;

    /**
     * Empty constructor (uninitialized wikittyService).
     */
    protected WikittyClient() {
    }

    /**
     * Creation du client, le wikittyService est instancier grace au information
     * trouve dans la configuration.
     *
     * @param config
     */
    public WikittyClient(ApplicationConfig config) {
        this(config, WikittyServiceFactory.buildWikittyService(config));
    }

    /**
     * Creation du client en forcant le wikittyService
     *
     * @param config
     * @param wikittyService
     */
    public WikittyClient(ApplicationConfig config, WikittyService wikittyService) {
        this(config, wikittyService, null);
    }

    /**
     * Creation du client en forcant le wikittyService
     *
     * @param config
     * @param wikittyService
     * @param securityToken 
     */
    public WikittyClient(ApplicationConfig config, WikittyService wikittyService, String securityToken) {
        if (config != null) {
            this.config = config;
            long timeToLogInfo = config.getOptionAsInt(WikittyConfigOption.
                    WIKITTY_CLIENT_TIME_TO_LOG_INFO.getKey());
            long timeToLogWarn = config.getOptionAsInt(WikittyConfigOption.
                    WIKITTY_CLIENT_TIME_TO_LOG_WARN.getKey());
            timeLog.setTimeToLogInfo(timeToLogInfo);
            timeLog.setTimeToLogWarn(timeToLogWarn);

        }
        setWikittyService(wikittyService);
        setSecurityToken(securityToken);
    }

    static public TimeLog getTimeTrace() {
        return timeLog;
    }

    static public Map<String, TimeLog.CallStat> getCallCount() {
        return timeLog.getCallCount();
    }

    public WikittyExtensionMigrationRegistry getMigrationRegistry() {
        WikittyExtensionMigrationRegistry result =
                config.getObject(WikittyExtensionMigrationRegistry.class);
        return result;
    }

    public void login(String login, String password) {
        long start = TimeLog.getTime();
        String result = wikittyService.login(login, password);
        setSecurityToken(result);
        
        timeLog.log(start, "login");
    }

    public void logout() {
        long start = TimeLog.getTime();
        wikittyService.logout(securityToken);
        
        timeLog.log(start, "logout");
    }

    public String getSecurityToken(String userId) {
        long start = TimeLog.getTime();
        String result = wikittyService.getToken(userId);
        timeLog.log(start, "logout");
        return result;
    }

    public String getSecurityToken() {
        return securityToken;
    }

    public void setSecurityToken(String securityToken) {
        this.securityToken = securityToken;
    }

    /**
     * get current wikittyUser logged or null if no user logged
     * @return null if no user logged
     */
    public WikittyUser getUser() {
        WikittyUser result = getUser(WikittyUser.class);
        return result;
    }

    /**
     * get current logged user wikitty object
     * @param clazz Business class used as User in your application,
     * this extension should be require WikittyUser.
     * @return null if no user logged
     */
    public <E extends BusinessEntity> E getUser(Class<E> clazz) {
        E result = null;
        if (securityToken != null) {
            //Get the token
            Wikitty securityTokenWikitty = restore(securityToken);
            if (securityTokenWikitty != null) {
                //Get the user
                String userId = WikittyTokenHelper.getUser(securityTokenWikitty);
                result = restore(clazz, userId);
            }
        }
        return result;
    }

    public WikittyService getWikittyService() {
        return wikittyService.getDelegate();
    }

    public void setWikittyService(WikittyService wikittyService) {
        this.wikittyService = new WikittyServiceEnhanced(wikittyService);
    }

    public ApplicationConfig getConfig() {
        return config;
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // STORE
    //
    ////////////////////////////////////////////////////////////////////////////

    public Wikitty store(Wikitty w) {
        long start = TimeLog.getTime();
        WikittyEvent resp = wikittyService.store(securityToken, w);
        // update object
        resp.update(w);

        // on recharge les preload, comme demande au moment du restore initial
        preload(Collections.singletonList(w));

        timeLog.log(start, "store");
        return w;
    }

    public <E extends BusinessEntity> E store(E e) {
        Wikitty w = ((BusinessEntityImpl)e).getWikitty();
        store(w);
        return e;
    }

    public <E extends BusinessEntity> E[] store(E e1, E e2, E... eN) {
        List<E> es = new ArrayList<E>(eN.length + 2);
        Collections.addAll(es, e1, e2);
        Collections.addAll(es, eN);

        List<E> list = store(es);

        E[] result = list.toArray((E[])Array.newInstance(
                eN.getClass().getComponentType(), list.size()));
        return result;
    }

    public Wikitty[] store(Wikitty w1, Wikitty w2, Wikitty... wN) {
        List<Wikitty> ws = new ArrayList<Wikitty>(wN.length + 2);
        Collections.addAll(ws, w1, w2);
        Collections.addAll(ws, wN);

        List<Wikitty> resultList = storeWikitty(ws);
        Wikitty[] result = resultList.toArray(new Wikitty[resultList.size()]);
        return result;
    }

    /**
     * Store to WikittyService objects.
     * 
     * @param <E> object type
     * @param objets list
     * @return updated objects list
     */
    public <E extends BusinessEntity> List<E> store(List<E> objets) {
        long start = TimeLog.getTime();
        // prepare data to send to service
        List<Wikitty> wikitties = new ArrayList<Wikitty>(objets.size());
        for (E e : objets) {
            if (e == null) {
                wikitties.add(null);
            } else {
                Wikitty w = ((BusinessEntityImpl)e).getWikitty();
                wikitties.add(w);
            }
        }

        storeWikitty(wikitties);

        timeLog.log(start, "store<list>");
        return objets;
    }

    public  List<Wikitty> storeWikitty(List<Wikitty> wikitties) {
        long start = TimeLog.getTime();

        // call the service with Wikitty
        WikittyEvent resp = wikittyService.store(securityToken, wikitties);

        // update object
        for (Wikitty w : wikitties) {
            resp.update(w);
        }
        // on recharge les preload, comme demande au moment du restore initial
        preload(wikitties);

        timeLog.log(start, "storeWikitty<list>");
        return wikitties;
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // RESTORE
    //
    ////////////////////////////////////////////////////////////////////////////

    protected Set<Wikitty> getWikittyForPreload(Map<String, Wikitty> available,
            Wikitty w, FieldType type, String fqField) {

        Set<Wikitty> result = new HashSet<Wikitty>();
        
        // contient tous les ids a recuperer
        Set<String> ids = new HashSet<String>();
        if (type.isCollection()) {
            ids.addAll((Collection)w.getFqField(fqField));
        } else {
            ids.add((String)w.getFqField(fqField));
        }

        // on prend tout ce que l'on peut depuis available
        Collection<String> availableIds = CollectionUtils.intersection(ids, available.keySet());
        for (String id : availableIds) {
            result.add(available.get(id));
        }

        // et on on fait un restore sur les reste
        ids.removeAll(availableIds);
        Set<Wikitty> restored = restore(ids);
        // on met les objets restore dans available pour la prochaine fois
        for (Wikitty wikitty : restored) {
            if (wikitty != null) {
                available.put(wikitty.getWikittyId(), wikitty);
            }
        }
        result.addAll(restored);
        
        return result;
    }

    /**
     * Load one wikitty with specified preloadPattern (with only ',').
     *
     * @param w current wikitty to load
     * @param available available wikitty (restored)
     * @param loaded already loaded wikitties + field couple
     * @param preloadPattern pattern used to load
     */
    protected void preload(Wikitty w, Map<String, Wikitty> available,
            Set<String> loaded, String preloadPattern) {
        // s'il reste encore des champs a precharger
        String pattern = StringUtils.substringBefore(preloadPattern, ",");
        if (StringUtils.isNotBlank(pattern)) {
            pattern = pattern.trim();
            // on enleve la tete qui va etre traitee maintenant, et le reste
            // par recursion (nextPattern)
            String nextPattern = StringUtils.substringAfter(preloadPattern, ",");

            String wid = w.getWikittyId();
            for (String fqField : w.fieldNames()) {
                // si le couple wid + fqField n'a pas encore ete traite on le fait
                if (!loaded.contains(wid + "+" + fqField)) {
                    FieldType type = w.getFieldType(fqField);
                    // si le champs est bien de type Wikitty et qu'il respecte
                    // le pattern demande, on le traite
                    if (type.getType() == WikittyTypes.WIKITTY
                            && Pattern.matches(pattern, fqField)) {
                        // on marque le champs comme ayant ete traite, vu qu'on
                        // est en cours (pour eviter les boucles infinies
                        // qui peuvent arriver si une extension declare un champs
                        // pointant sur lui meme
                        loaded.add(wid + "+" + fqField);
                        // on recupere le(s) wikitty(ies) du lien
                        Set<Wikitty> links = getWikittyForPreload(available, w, type, fqField);
                        for (Wikitty link : links) {
                            if (link != null) {
                                // on ajoute ce wikitty dans le preload du wikitty pere
                                w.addPreloaded(link);
                                // si le wikitty a pu etre recupere on fait la suite
                                // sur lui (nextPattern)
                                preload(link, available, loaded, nextPattern);
                                // et on preload aussi pour ses extensions
                                preload(link, available, loaded);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Preload recursivement le wikitty, en fonction de sa chaine preloadPattern
     * et des tags values des extensions qu'il a..
     * @param w le wikitty a precharger
     * @param available la liste des wikitty deja disponible sans restore explicite
     * @param loaded la liste de couple wid + fqfield deja traite
     */
    protected void preload(Wikitty w, Map<String, Wikitty> available,
            Set<String> loaded) {
        if (w != null) {
            for (String p : w.getAllPreloadPattern()) {
                preload(w, available, loaded, p);
            }
        }
    }

    /**
     * Ajoute le preloadPattern comme chaine de preload dans chaque wikitty
     * et fait le prechargement sur chaque wikitty. En sortie de methode les
     * wikitty sont tous precharge comme souhaite.
     * 
     * @param wikitties la liste des wikitties a precharger
     * @param preloadPattern la liste des patterns de prechargement a faire
     */
    public void preload(Collection<Wikitty> wikitties, String ... preloadPattern) {
        String patterns = StringUtils.join(preloadPattern, ";");
        Map<String, Wikitty> available = new HashMap<String, Wikitty>();
        for (Wikitty w : wikitties) {
            if (w != null) {
                available.put(w.getWikittyId(), w);
                if (StringUtils.isNotBlank(patterns)) {
                    // lorsqu'on apelle la methode pour le store, le preloadPattern
                    // est vide mais il ne faut pas perdre ce qu'il y avait avant
                    // dans le wikitty. Donc on ne fait le set que si patterns
                    // n'est pas vide
                    w.setPreloadPattern(patterns);
                }
            }
        }
        Set<String> loaded = new HashSet<String>();
        
        for (Wikitty w : wikitties) {
            if (w != null) {
                preload(w, available, loaded);
            }
        }
    }
    
    /**
     * Restore wikitty entity with specified id or {@code null} if entity can't be be found.
     *
     * @param id entity ids if null return is empty list
     * @param preload patterns des liens qu'il faut precharger par exemple:
     * "Company.employee, Employee.person", "Company.address, Address.*"
     * @return wikitty entity with specified id or {@code null} if entity can't be found
     */
    public List<Wikitty> restore(List<String> id, String ... preload) {
        long start = TimeLog.getTime();

        List<Wikitty> result;
        if (id == null) {
            result = new ArrayList<Wikitty>();
        } else {
            result = wikittyService.restore(securityToken, id);
            preload(result, preload);
        }

        timeLog.log(start, "restore<list>");
        return result;
    }

    /**
     * Restore wikitty entity with specified id or {@code null} if entity can't be found.
     *
     * @param id entity id
     * @return wikitty entity with specified id or {@code null} if entity can't be found
     */
    public Wikitty restore(String id, String ... preload) {
        long start = TimeLog.getTime();
        Wikitty result = null;
        if (id != null) {
            result = restore(Collections.singletonList(id), preload).get(0);
        }

        timeLog.log(start, "restore");
        return result;
    }

    /**
     * Restore wikitty entity with specified id or {@code null} if entity
     * can't be be found, or checkExtension is true and wikitty don't match
     * extension wanted.
     * 
     * @param <E> object type
     * @param clazz entity class
     * @param id entity ids if null return is empty list
     * @param checkExtension if true check that Wikitty result has all extension
     * @return wikitty entity with specified id or {@code null} if entity
     * can't be found or if one wikitty don't have extension wanted by E type
     */
    public <E extends BusinessEntity> List<E> restore(Class<E> clazz,
            List<String> id, boolean checkExtension, String ... preload) {
        long start = TimeLog.getTime();
        List<E> result = new ArrayList<E>();

        if (id != null) {
            List<Wikitty> wikitties = restore(id, preload);

            Collection<String> businessExtension = null;
            if (checkExtension) {
                // Recuperation de la liste des extensions du BusinessEntity resultats
                BusinessEntityImpl sample =
                        (BusinessEntityImpl) WikittyUtil.newInstance(clazz);
                businessExtension = sample.getExtensionNames();
            }

            for (Wikitty w : wikitties) {
                E dto = null;
                if (!checkExtension ||
                        // on ne check pas les extensions ou ...
                        CollectionUtils.subtract(
                        businessExtension, w.getExtensionNames()).isEmpty()) {
                        // ... on a retrouve toutes les extensions du wikitty dans l'objet
                        // on le prend dans les resultats
                        dto = WikittyUtil.newInstance(clazz, w);
                } // sinon on ne prend pas l'objet
                
                // add entity to result after checkExtension
                result.add(dto);
            }
        }
        timeLog.log(start, "restore<list>");
        return result;
    }

    /**
     * Restore wikitty entity with specified id or {@code null} if entity can't be found.
     *
     * @param <E> object type
     * @param clazz entity class
     * @param id entity id
     * @return wikitty entity with specified id or {@code null} if entity can't be found
     */
    public <E extends BusinessEntity> E restore(Class<E> clazz, String id,
            String ... preload) {
        E result = restore(clazz, id, false, preload);
        return result;
    }

    /**
     * Restore wikitty entity with specified id or {@code null} if entity can't be found.
     *
     * @param <E> object type
     * @param clazz entity class
     * @param id entity id
     * @param checkExtension if true check that Wikitty result has all extension
     * declared in clazz
     * @return wikitty entity with specified id or {@code null} if entity can't be found
     */
    public <E extends BusinessEntity> E restore(Class<E> clazz, String id,
            boolean checkExtension, String ... preload) {
        long start = TimeLog.getTime();
        E result = null;
        if (id != null) {
            result = restore(clazz, Collections.singletonList(id),
                    checkExtension, preload).get(0);
        }

        timeLog.log(start, "restore<Business>");
        return result;
    }

    public <E extends BusinessEntity> List<E> restore(Class<E> clazz,
            List<String> id, String ... preload) {
        List<E> result = restore(clazz, id, false, preload);
        return result;
    }

    public Set<Wikitty> restore(Set<String> id, String ... preload) {
        ArrayList<String> list = null;
        if (id != null) {
            list = new ArrayList<String>(id);
        }
        List<Wikitty> resultList = restore(list, preload);
        Set<Wikitty> result = new LinkedHashSet<Wikitty>(resultList);
        return result;
    }

    public <E extends BusinessEntity> Set<E> restore(Class<E> clazz, 
            Set<String> id, String ... preload) {
        Set<E> result = restore(clazz, id, false, preload);
        return result;
    }

    public <E extends BusinessEntity> Set<E> restore(Class<E> clazz, Set<String> id,
            boolean checkExtension, String ... preload) {
        ArrayList<String> list = null;
        if (id != null) {
            list = new ArrayList<String>(id);
        }
        List<E> resultList = restore(clazz, list, checkExtension, preload);
        Set<E> result = new LinkedHashSet<E>(resultList);
        return result;
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // DETELE
    //
    ////////////////////////////////////////////////////////////////////////////

    public void delete(String id) {
        long start = TimeLog.getTime();
        wikittyService.delete(securityToken, id);

        timeLog.log(start, "delete");
    }

    public <E extends BusinessEntity> void delete(E object) {
        long start = TimeLog.getTime();
        if (object != null) {
            String id = object.getWikittyId();
            wikittyService.delete(securityToken, id);
        }
        timeLog.log(start, "delete(BusinessEntity)");
    }

    public void delete(Collection<String> ids) {
        long start = TimeLog.getTime();
        wikittyService.delete(securityToken, ids);
        
        timeLog.log(start, "delete<list>");
    }

    public <E extends BusinessEntity> void delete(List<E> objets) {
        long start = TimeLog.getTime();
        
        // prepare data to send to service
        List<String> ids = new ArrayList<String>(objets.size());
        for (E e : objets) {
            if (e != null) {
                String id = e.getWikittyId();
                ids.add(id);
            }
        }

        // call the service with Wikitty
        wikittyService.delete(securityToken, ids);

        timeLog.log(start, "delete<list<BusinessEntity>>");
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // FIND (ALL) BY EXEMPLE <E>
    //
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Null field are not used in search request.
     *
     * @param e sample wikitty
     * @param first
     * @param limit
     * @param fieldFacet
     * @return
     */
    public <E extends BusinessEntityImpl> WikittyQueryResult<E> findAllByExample(E e,
            int first, int limit, ElementField ... fieldFacet) {
        long start = TimeLog.getTime();

        WikittyQuery query = new WikittyQueryMaker().wikitty(e).end()
                .setOffset(first).setLimit(limit)
                .setFacetField(fieldFacet);

        WikittyQueryResult<E> result =
                findAllByQuery((Class<E>)e.getClass(),
                Collections.singletonList(query), false).get(0);
        
        timeLog.log(start, "findAllByExample<limit>");
        return result;
    }

    /**
     * Null field are not used in search request.
     * 
     * @param e sample wikitty
     * @return
     */
    public <E extends BusinessEntityImpl> E findByExample(E e) {
        long start = TimeLog.getTime();
        WikittyQuery query = new WikittyQueryMaker().wikitty(e).end();

        WikittyQueryResult<E> queryResult =
                findAllByQuery((Class<E>)e.getClass(),
                Collections.singletonList(query), true).get(0);

        E result = queryResult.peek();
        
        timeLog.log(start, "findByExample");
        return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // FIND ALL BY QUERY <E>
    //
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Cette method doit etre l'unique methode finalement utilise par toutes
     * les methodes find avec un cast vers clazz
     *
     * Search object that correspond to criteria and that have all extension
     * needed by BusinessEntity (clazz), if clazz is BusinessEntity class.
     * If one criteria is empty, find all extensions
     * for this criteria else if criteria is null return null.
     *
     * @param <E> object type
     * Can be Wikitty, BusinessEntity, String, Date, Number (returned is BigDecimal), Boolean, byte[]
     * @param clazz entity class, can be null for no conversion
     * @param queries criterias
     * @param limitToFirst if true limit result to first result (first = 0, limit = 1)
     * @return paged result
     */
    protected <E> List<WikittyQueryResult<E>> findAllByQuery(
            Class<E> clazz, List<WikittyQuery> queries, boolean limitToFirst) {
        List<WikittyQueryResult<Map<String, E>>> tmp =
                findAllByQueryAsMap(clazz, queries, limitToFirst);

        List<WikittyQueryResult<E>> result = new ArrayList<WikittyQueryResult<E>>(tmp.size());
        for (WikittyQueryResult p : tmp) {
            result.add(p.convertMapToSimple());
//            result.add(castTo(clazz, p));
        }

        return result;
    }

    /**
     * Cette method doit etre l'unique methode finalement utilise par toutes
     * les methodes find avec un cast vers clazz
     *
     * Search object that correspond to criteria and that have all extension
     * needed by BusinessEntity (clazz), if clazz is BusinessEntity class.
     * If one criteria is empty, find all extensions
     * for this criteria else if criteria is null return null.
     *
     * @param <E> object type
     * Can be Wikitty, BusinessEntity, String, Date, Number (returned is BigDecimal), Boolean, byte[]
     * @param clazz entity class, can be null for no conversion
     * @param queries criterias
     * @param limitToFirst if true limit result to first result (first = 0, limit = 1)
     * @return paged result
     */
    protected <E> List<WikittyQueryResult<Map<String, E>>> findAllByQueryAsMap(
            Class<E> clazz, List<WikittyQuery> queries, boolean limitToFirst) {
        long start = TimeLog.getTime();
        List<WikittyQueryResult<Map<String, E>>> result = null;
        List<WikittyQuery> serviceQueries;
        if (queries != null) {
            if (clazz != null && BusinessEntity.class.isAssignableFrom(clazz)) {
                // on demande un business entity donc on modifie
                // les criteres pour ajouter les contraintes sur les
                // extensions

                // newInstance only return BusinessEntityWikittyImpl
                BusinessEntityImpl sample =
                        (BusinessEntityImpl) WikittyUtil.newInstance((Class<BusinessEntity>)clazz);

                Wikitty wikitty = sample.getWikitty();
                Collection<String> extensions = wikitty.getExtensionNames();

                serviceQueries = new ArrayList<WikittyQuery>(queries.size());
                for (WikittyQuery criteria : queries) {

                    // on ajoute la condition sur les extensions dans le critere
                    // du coup, pour ne pas modifier le critere qui vient en parametre
                    // il faut creer un nouveau critere ...
                    WikittyQuery serviceQuery = null;
                    if (criteria == null) {
                        serviceQuery = new WikittyQueryMaker()
                                .extContainsAll(extensions).end();
                    } else {
                        serviceQuery = criteria.copy();
                        WikittyQueryMaker queryMaker;
                        if (serviceQuery.getCondition() instanceof Select) {
                            // si la condition commence par un select
                            // alors il faut modifier la condition du select
                            Select select = (Select)serviceQuery.getCondition();
                            queryMaker = new WikittyQueryMaker()
                                    .select(select.getFunction())
                                    .and().condition(select.getSubCondition())
                                    .extContainsAll(extensions);

                        } else {
                            // sinon on modifie directement la condition
                            queryMaker = new WikittyQueryMaker()
                                    .and().condition(serviceQuery.getCondition())
                                    .extContainsAll(extensions);
                        }
                        // utilisation de cette nouvelle contrainte sur le nouvel objet
                        serviceQuery.setCondition(queryMaker.getCondition());
                    }

                    // ajout de ce criteria dans la liste de tous les criteres
                    serviceQueries.add(serviceQuery);
                }
            } else {
                // on ne demande pas un business entity donc on ne modifie
                // pas les criteres
                serviceQueries = queries;
            }

            if (limitToFirst) {
                for (WikittyQuery query : serviceQueries) {
                    // FIXME, il faudrait faire des copies pour ne pas modifier les querys de l'utilisateur

                    // si on a un SELECT, il ne faut pas modifier l'offset ni le limit,
                    // car il peut y avoir des methods d'agregats qui ont besoin de toutes les donnees
                    if (!(query.getCondition() instanceof Select)) {
                        query.setOffset(0);
                        query.setLimit(1);
                    }
                    // lorsqu'on limit au premier, c'est qu'on utilise la methode
                    // pour retourne seulement un objet et pas un WikittyQueryResult
                    // on n'a donc pas a calculer les facets
                    query.setFacetExtension(false);
                    query.setFacetField();
                    query.setFacetQuery();
                }
            }

            List<WikittyQueryResult<Map<String, Object>>> pagedResult =
                    findAllByQueryAsMap(serviceQueries);

            result = new ArrayList<WikittyQueryResult<Map<String, E>>>(pagedResult.size());
            for (WikittyQueryResult p : pagedResult) {
                result.add((WikittyQueryResult<Map<String, E>>)castToMap(clazz, p));
            }
        }
        timeLog.log(start, "findAllByQuery<Map<String, E>>(List, limitToFirst)");
        return result;
    }

    /**
     * Search object that correspond to criteria and that have all extension
     * needed by BusinessEntity (clazz), if clazz is BusinessEntity class.
     * If one criteria is empty, find all extensions
     * for this criteria else if criteria is null return null.
     *
     * @param <E> object type
     * Can be Wikitty, BusinessEntity, String, Date, Number (returned is BigDecimal), Boolean, byte[]
     * @param clazz entity class
     * @param queries queries
     * @return paged result
     */
    public <E> List<WikittyQueryResult<E>> findAllByQuery(
            Class<E> clazz, List<WikittyQuery> queries) {
        long start = TimeLog.getTime();
        List<WikittyQueryResult<E>> result = findAllByQuery(clazz, queries, false);
        timeLog.log(start, "findAllByQuery<E>(List)");
        return result;
    }

    /**
     * Search object that correspond to criteria and that have all extension
     * needed by BusinessEntity (clazz), if clazz is BusinessEntity class.
     * If one criteria is empty, find all extensions
     * for this criteria else if criteria is null return null.
     *
     * @param <E> object type
     * Can be Wikitty, BusinessEntity, String, Date, Number (returned is BigDecimal), Boolean, byte[]
     * @param clazz entity class
     * @param query query
     * @return paged result
     */
    public <E> WikittyQueryResult<E> findAllByQuery(
            Class<E> clazz, WikittyQuery query) {
        long start = TimeLog.getTime();
        WikittyQueryResult<E> result = findAllByQuery(clazz,
                Collections.singletonList(query), false).get(0);
        timeLog.log(start, "findAllByQuery<E>(One)");
        return result;
    }

    /**
    /**
     * Search object that correspond to criteria and that have all extension
     * needed by BusinessEntity (clazz), if clazz is BusinessEntity class.
     * If one criteria is empty, find all extensions
     * for this criteria else if criteria is null return null.
     *
     * @param <E> object type
     * Can be Wikitty, BusinessEntity, String, Date, Number (returned is BigDecimal), Boolean, byte[]
     * @param clazz entity class
     * @param q1 query 1
     * @param q2 query 2
     * @param otherQueries otherQueries
     * @return paged result
     */
    public <E> WikittyQueryResult<E>[] findAllByQuery(
            Class<E> clazz, WikittyQuery q1, WikittyQuery q2, WikittyQuery... otherQueries) {
        long start = TimeLog.getTime();
        List<WikittyQuery> queries = new ArrayList<WikittyQuery>(otherQueries.length + 2);
        Collections.addAll(queries, q1, q2);
        Collections.addAll(queries, otherQueries);

        List<WikittyQueryResult<E>> resultList = findAllByQuery(clazz, queries, false);
        WikittyQueryResult<E>[] result = resultList.toArray(new WikittyQueryResult[queries.size()]);
        timeLog.log(start, "findAllByQuery<Business>(Varargs)");
        return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // FIND BY CRITERIA <E>
    //
    ///////////////////////////////////////////////////////////////////////////

    public <E> List<E> findByQuery(Class<E> clazz, List<WikittyQuery> queries) {
        long start = TimeLog.getTime();

        List<WikittyQueryResult<E>> queryResult =
                findAllByQuery(clazz, queries, true);
        List<E> result = new ArrayList<E>(queryResult.size());
        for (WikittyQueryResult<E> r : queryResult) {
            if (r.size() > 0) {
                result.add(r.peek());
            } else {
                result.add(null);
            }
        }

        timeLog.log(start, "findByQuery<E>(List)");
        return result;
    }

    public <E> E findByQuery(Class<E> clazz, WikittyQuery query) {
        long start = TimeLog.getTime();
        E result = null;
        if (query != null) {
            result = findByQuery(clazz, Collections.singletonList(query)).get(0);
        }
        timeLog.log(start, "findByQuery<E>(One)");
        return result;
    }

    public <E> E[] findByQuery(
            Class<E> clazz, WikittyQuery q1, WikittyQuery q2, WikittyQuery... otherQueries) {
        long start = TimeLog.getTime();

        List<WikittyQuery> queries = new ArrayList<WikittyQuery>(otherQueries.length + 2);
        Collections.addAll(queries, q1, q2);
        Collections.addAll(queries, otherQueries);

        List<E> resultList = findByQuery(clazz, queries);
        E[] result = resultList.toArray((E[])Array.newInstance(clazz, resultList.size()));

        timeLog.log(start, "findByQuery<E>(Varargs)");
        return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // FIND ALL FIELD OR ID BY CRITERIA <String>
    //
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Retourne les ids des wikitties qui correspondent au critere, chaque
     * query passees en argument et retourne un WikittyQueryResult.
     * 
     *
     * @param query
     * @return
     */
    public List<WikittyQueryResult<String>> findAllByQuery(List<WikittyQuery> query) {
        long start = TimeLog.getTime();
        List<WikittyQueryResult<String>> result = null;
        if (query != null) {
            result = findAllByQuery(String.class, query);
        }
        timeLog.log(start, "findAllByQuery(List)");
        return result;
    }

    /**
     * Cette method est la seul a reellement faire un appel a findAllByQuery
     * du wikitty service.
     *
     * @param query
     * @return
     */
    public List<WikittyQueryResult<Map<String, Object>>> findAllByQueryAsMap(List<WikittyQuery> query) {
        long start = TimeLog.getTime();
        List<WikittyQueryResult<Map<String, Object>>> result = null;
        if (query != null) {
            result = wikittyService.findAllByQuery(securityToken, query);
        }
        timeLog.log(start, "findAllByQuery(List)");
        return result;
    }

    public WikittyQueryResult<String> findAllByQuery(WikittyQuery query) {
        long start = TimeLog.getTime();
        WikittyQueryResult<String> result = null;
        if (query != null) {
            result = findAllByQuery(Collections.singletonList(query)).get(0);
        }
        timeLog.log(start, "findAllByQuery(One)");
        return result;
    }

    public WikittyQueryResult<Map<String, Object>> findAllByQueryAsMap(WikittyQuery query) {
        long start = TimeLog.getTime();
        WikittyQueryResult<Map<String, Object>> result = null;
        if (query != null) {
            result = findAllByQueryAsMap(Collections.singletonList(query)).get(0);
        }
        timeLog.log(start, "findAllByQuery(One)");
        return result;
    }

    public WikittyQueryResult<String>[] findAllByQuery(
            WikittyQuery c1, WikittyQuery c2, WikittyQuery ... otherCriteria) {
        long start = TimeLog.getTime();

        List<WikittyQuery> criterias = new ArrayList<WikittyQuery>(otherCriteria.length + 2);
        Collections.addAll(criterias, c1, c2);
        Collections.addAll(criterias, otherCriteria);

        List<WikittyQueryResult<String>> resultList = findAllByQuery(criterias);
        WikittyQueryResult<String>[] result = resultList.toArray(new WikittyQueryResult[criterias.size()]);

        timeLog.log(start, "findAllByCriteria(Varargs)");
        return result;
    }

    public WikittyQueryResult<Map<String, Object>>[] findAllByQueryAsMap(
            WikittyQuery c1, WikittyQuery c2, WikittyQuery ... otherCriteria) {
        long start = TimeLog.getTime();

        List<WikittyQuery> criterias = new ArrayList<WikittyQuery>(otherCriteria.length + 2);
        Collections.addAll(criterias, c1, c2);
        Collections.addAll(criterias, otherCriteria);

        List<WikittyQueryResult<Map<String, Object>>> resultList = findAllByQueryAsMap(criterias);
        WikittyQueryResult<Map<String, Object>>[] result = resultList.toArray(new WikittyQueryResult[criterias.size()]);

        timeLog.log(start, "findAllByCriteria(Varargs)");
        return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // FIND ID BY CRITERIA <String>
    //
    ///////////////////////////////////////////////////////////////////////////

    public List<String> findByQuery(List<WikittyQuery> query) {
        long start = TimeLog.getTime();
        List<String> result = null;
        if (query != null) {
            List<WikittyQueryResult<String>> tmp = findAllByQuery(String.class, query, true);
            result = new ArrayList<String>(tmp.size());
            for (WikittyQueryResult<String> wqr : tmp) {
                if (wqr.size() == 0) {
                    result.add(null);
                } else {
                    result.add(wqr.peek());
                }
            }
        }
        timeLog.log(start, "findByQuery(List)");
    	return result;
    }

    public List<Map<String, Object>> findByQueryAsMap(List<WikittyQuery> query) {
        long start = TimeLog.getTime();
        List<Map<String, Object>> result = null;
        if (query != null) {
            List<WikittyQueryResult<Map<String, Object>>> tmp = findAllByQuery(null, query, true);
            result = new ArrayList<Map<String, Object>>(tmp.size());
            for (WikittyQueryResult<Map<String, Object>> wqr : tmp) {
                if (wqr.size() == 0) {
                    result.add(null);
                } else {
                    result.add(wqr.peek());
                }
            }
        }
        timeLog.log(start, "findByQuery(List)");
        return result;
    }

    public <E> List<Map<String, E>> findByQueryAsMap(Class<E> clazz, List<WikittyQuery> query) {
        long start = TimeLog.getTime();
        List<Map<String, E>> result = null;
        if (query != null) {
            List<WikittyQueryResult<Map<String, E>>> tmp = findAllByQueryAsMap(clazz, query, true);
            result = new ArrayList<Map<String, E>>(tmp.size());
            for (WikittyQueryResult<Map<String, E>> wqr : tmp) {
                if (wqr.size() == 0) {
                    result.add(null);
                } else {
                    result.add(wqr.peek());
                }
            }
        }
        timeLog.log(start, "findByQuery(List)");
        return result;
    }

    public String findByQuery(WikittyQuery query) {
        long start = TimeLog.getTime();
        String result = null;
        if (query != null) {
            result = findByQuery(Collections.singletonList(query)).get(0);
        }
        timeLog.log(start, "findByQuery(One)");
    	return result;
    }

    public Map<String, Object> findByQueryAsMap(WikittyQuery query) {
        long start = TimeLog.getTime();
        Map<String, Object> result = null;
        if (query != null) {
            result = findByQueryAsMap(Collections.singletonList(query)).get(0);
        }
        timeLog.log(start, "findByQuery(One)");
        return result;
    }

    public <E> Map<String, E> findByQueryAsMap(Class<E> clazz, WikittyQuery query) {
        long start = TimeLog.getTime();
        Map<String, E> result = null;
        if (query != null) {
            result = findByQueryAsMap(clazz, Collections.singletonList(query)).get(0);
        }
        timeLog.log(start, "findByQuery(One)");
        return result;
    }

    public String[] findByQuery(
            WikittyQuery q1, WikittyQuery q2, WikittyQuery... otherQueries) {
        long start = TimeLog.getTime();

        List<WikittyQuery> queries = new ArrayList<WikittyQuery>(otherQueries.length + 2);
        Collections.addAll(queries, q1, q2);
        Collections.addAll(queries, otherQueries);

        List<String> resultList = findByQuery(queries);
        String[] result = resultList.toArray(new String[queries.size()]);

        timeLog.log(start, "findByQuery(Varargs)");
        return result;
    }

    public Map<String, Object>[] findByQueryAsMap(
            WikittyQuery q1, WikittyQuery q2, WikittyQuery... otherQueries) {
        long start = TimeLog.getTime();

        List<WikittyQuery> queries = new ArrayList<WikittyQuery>(otherQueries.length + 2);
        Collections.addAll(queries, q1, q2);
        Collections.addAll(queries, otherQueries);

        List<Map<String, Object>> resultList = findByQueryAsMap(queries);
        Map<String, Object>[] result = resultList.toArray(new Map[queries.size()]);

        timeLog.log(start, "findByQuery(Varargs)");
        return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // FIND BY TREE NODE
    //
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Recupere une portion d'arbre a partir de l'id passer en parametre. L'id
     * doit etre celui d'un WikittyTreeNode. Ce WikittyTreeNode est alors le
     * root de l'arbre retourne.
     *
     * Return Wikitty in result, those Wikitties have WikittyTreeNode extension
     *
     * @param wikittyId root
     * @param depth profondeur de noeud a recuperer
     * @param count vrai si l'on veut le nombre de piece attaches sur le noeud
     * (piece des enfants compris)
     * @param filter filter pour compter les pieces attachees
     * @return treeNodeResult of wikitty
     *
     * @since 3.1
     */
    public WikittyQueryResultTreeNode<Wikitty> findTreeNode(
            String wikittyId, int depth, boolean count, WikittyQuery filter) {
        long start = TimeLog.getTime();

        WikittyQueryResultTreeNode<String> resultId = wikittyService.findTreeNode(
                securityToken, wikittyId, depth, count, filter);

        WikittyQueryResultTreeNode<Wikitty> result = null;
        if (resultId != null) {
            RetrieveIdVisitor retrieveIdVisitor = new RetrieveIdVisitor();
            resultId.acceptVisitor(retrieveIdVisitor);

            List<String> ids = retrieveIdVisitor.getIds();
            List<Wikitty> wikitties = restore(ids);

            IdToObjectConverter<Wikitty> converter =
                    new IdToObjectConverter<Wikitty>(ids, wikitties);

            ConvertTreeVisitor<Wikitty> convertVisitor =
                    new ConvertTreeVisitor<Wikitty>(converter);

            resultId.acceptVisitor(convertVisitor);

             result = convertVisitor.getTree();
        }
        timeLog.log(start, "findTreeNode<Wikitty>");
        return result;
    }

    /**
     * Recupere une portion d'arbre a partir de l'id passer en parametre. L'id
     * doit etre celui d'un WikittyTreeNode. Ce WikittyTreeNode est alors le
     * root de l'arbre retourne.
     *
     * Return E in result
     *
     * @param clazz business class wanted to replace id in TreeNodeResult
     * @param wikittyId root
     * @param depth profondeur de noeud a recuperer
     * @param count vrai si l'on veut le nombre de piece attaches sur le noeud (piece des enfants compris)
     * @param filter filter pour compter les pieces attachees
     * @return
     *
     * @since 3.1
     */
    public <E extends BusinessEntity> WikittyQueryResultTreeNode<E> findTreeNode(
            Class<E> clazz, String wikittyId, int depth,
            boolean count, WikittyQuery filter) {
        long start = TimeLog.getTime();

        WikittyQueryResultTreeNode<String> resultId = wikittyService.findTreeNode(
                securityToken, wikittyId, depth, count, filter);

        WikittyQueryResultTreeNode<E> result = null;
        if (resultId != null) {
            RetrieveIdVisitor retrieveIdVisitor = new RetrieveIdVisitor();
            resultId.acceptVisitor(retrieveIdVisitor);

            List<String> ids = retrieveIdVisitor.getIds();
            List<E> wikitties = restore(clazz, ids);

            IdToObjectConverter<E> converter =
                    new IdToObjectConverter<E>(ids, wikitties);

            ConvertTreeVisitor<E> convertVisitor =
                    new ConvertTreeVisitor<E>(converter);

            resultId.acceptVisitor(convertVisitor);

            result = convertVisitor.getTree();
        }
        timeLog.log(start, "findTreeNode");
        return result;
    }

    /**
     * Used to collect all node id
     * @since 3.1
     */
    static private class RetrieveIdVisitor implements WikittyQueryResultTreeNode.Visitor<String> {

        protected List<String> ids = new ArrayList<String>();

        public List<String> getIds() {
            return ids;
        }

        @Override
        public boolean visitEnter(WikittyQueryResultTreeNode<String> node) {
            String id = node.getObject();
            ids.add(id);
            return true;
        }

        @Override
        public boolean visitLeave(WikittyQueryResultTreeNode<String> node) {
            return true;
        }
    }

    /**
     * Converti un id en son object WikittyTreeNode.
     * @since 3.1
     */
    static private class IdToObjectConverter<T> implements ConvertTreeVisitor.Converter<String, T> {
        protected Map<String, T> objects = new HashMap<String, T>();

        public IdToObjectConverter(List<String> ids, List<T> objectList) {

            for (int i = 0; i < ids.size(); i++) {
                this.objects.put(ids.get(i), objectList.get(i));
            }
        }

        @Override
        public T convert(String id) {
            T result = objects.get(id);
            return result;
        }
    }

    /**
     * Parcours un TreeNodeResult et en fait une copie en modifiant le type
     * d'objet stocker dans le noeud grace a un converter, si le converter
     * est null une exception est levee
     *
     * @param <TARGET> le type d'objet pour le nouvel arbre
     * @since 3.1
     */
    static private class ConvertTreeVisitor<TARGET extends Serializable>
            implements WikittyQueryResultTreeNode.Visitor<String> {

        static private interface Converter<SOURCE, TARGET> {
            public TARGET convert(SOURCE o);
        }
        protected Converter<String, TARGET> converter;
        protected WikittyQueryResultTreeNode<TARGET> tree = null;
        protected LinkedList<WikittyQueryResultTreeNode<TARGET>> stack =
                new LinkedList<WikittyQueryResultTreeNode<TARGET>>();

        public ConvertTreeVisitor(Converter<String, TARGET> converter) {
            this.converter = converter;
            if (converter == null) {
                throw new IllegalArgumentException("Converter can't be null");
            }
        }

        public WikittyQueryResultTreeNode<TARGET> getTree() {
            return tree;
        }

        @Override
        public boolean visitEnter(WikittyQueryResultTreeNode<String> node) {
            String id = node.getObject();
            int count = node.getAttCount();
            
            TARGET object = converter.convert(id);
            WikittyQueryResultTreeNode<TARGET> newNode = new WikittyQueryResultTreeNode<TARGET>(
                    object, count);

            WikittyQueryResultTreeNode<TARGET> parent = stack.peekLast();
            if (parent == null) {
                // le premier noeud, donc le root a retourner plus tard
                tree = newNode;
            } else {
                parent.add(newNode);
            }

            stack.offerLast(newNode);

            return true;
        }

        @Override
        public boolean visitLeave(WikittyQueryResultTreeNode<String> node) {
            stack.pollLast();
            return true;
        }
    }

    /**
     * Recupere une portion d'arbre a partir de l'id passer en parametre. L'id
     * doit etre celui d'un WikittyTreeNode. Ce WikittyTreeNode est alors le
     * root de l'arbre retourne.
     *
     * Return just wikitty Id in result
     *
     * @param wikittyId
     * @param depth
     * @param count
     * @param filter
     * @return
     * @since 3.1
     */
    public WikittyQueryResultTreeNode<String> findAllIdTreeNode(
            String wikittyId, int depth, boolean count, WikittyQuery filter) {
        long start = TimeLog.getTime();
        WikittyQueryResultTreeNode<String> result = wikittyService.findTreeNode(
                securityToken, wikittyId, depth, count, filter);

        timeLog.log(start, "findAllIdTreeNode");
    	return result;
    }

    /**
     * Delete specified tree node and all sub nodes.
     * 
     * @param treeNodeId tree node id to delete
     * @return {@code true} if at least one node has been deleted
     */
    public WikittyEvent deleteTree(String treeNodeId) {
        long start = TimeLog.getTime();
        WikittyEvent result = wikittyService.deleteTree(securityToken,treeNodeId);
        
        timeLog.log(start, "deleteTree");
        return result;
    }

    public Wikitty restoreVersion(String wikittyId, String version) {
        long start = TimeLog.getTime();
        Wikitty result = wikittyService.restoreVersion(
                securityToken, wikittyId, version);
        
        timeLog.log(start, "restoreVersion");
        return result;
    }

    /**
     * Manage Update and creation.
     *
     * @param ext extension to be persisted
     * @return update response
     */
    public WikittyEvent storeExtension(WikittyExtension ext) {
        long start = TimeLog.getTime();
        WikittyEvent response =
                wikittyService.storeExtension(securityToken, ext);
        
        timeLog.log(start, "storeExtension");
        return response;
    }

    /**
     * Manage Update and creation.
     *
     * @param exts list of wikitty extension to be persisted
     * @return update response
     */
    public WikittyEvent storeExtension(Collection<WikittyExtension> exts) {
        long start = TimeLog.getTime();
        WikittyEvent response =
                wikittyService.storeExtension(securityToken, exts);
        
        timeLog.log(start, "storeExtension<list>");
        return response;
    }

    /**
     * Load extension from id. Id is 'name[version]'.
     * 
     * @param extensionId extension id to restore
     * @return the corresponding object, exception if no such object found.
     */
    public WikittyExtension restoreExtension(String extensionId) {
        long start = TimeLog.getTime();
        WikittyExtension extension = wikittyService.restoreExtension(securityToken, extensionId);
        
        timeLog.log(start, "restoreExtension");
        return extension;
    }

    /**
     * Search extension with name in last version.
     *
     * @param extensionName extension name
     * @return the corresponding object, exception if no such object found.
     */
    public WikittyExtension restoreExtensionLastVersion(String extensionName) {
        long start = TimeLog.getTime();
        WikittyExtension extension = wikittyService.restoreExtensionLastVersion(securityToken, extensionName);

        timeLog.log(start, "restoreExtensionLastVersion");
        return extension;
    }

    /**
     * Search extension with name in last version.
     *
     * @param extensionName extension name
     * @return the corresponding object, exception if no such object found.
     */
    public Collection<WikittyExtension> restoreExtensionLastVersion(String extensionName1, String extensionName2, String ... extensionName) {
        long start = TimeLog.getTime();

        Collection<String> extName = new ArrayList<String>(extensionName.length + 2);
        extName.add(extensionName1);
        extName.add(extensionName2);
        extName.addAll(Arrays.asList(extensionName));

        Collection<WikittyExtension> result = restoreExtensionLastVersion(extName);

        timeLog.log(start, "restoreExtensionLastVersion[...]");
        return result;
    }

    /**
     * Search extension with name in last version.
     *
     * @param extensionName extension name
     * @return the corresponding object, exception if no such object found.
     */
    public Collection<WikittyExtension> restoreExtensionLastVersion(String[] extensionName) {
        long start = TimeLog.getTime();
        Collection<String> extName = new ArrayList<String>();
        if (extensionName != null) {
            extName.addAll(Arrays.asList(extensionName));
        }

        Collection<WikittyExtension> result = restoreExtensionLastVersion(extName);

        timeLog.log(start, "restoreExtensionLastVersion[]");
        return result;
    }

    /**
     * Search extension with name in last version. If extensionNames is null or
     * empty, return all extension
     *
     * @param extensionNames extension name
     * @return the corresponding object, exception if no such object found.
     */
    // TODO poussin 20120531 l'implantation de cette methode devrait etre fait cote serveur, mais
    // cela implique de modifier la signature de tous les WikittyService, donc pour l'instant
    // l'implantation est cote client
    public Collection<WikittyExtension> restoreExtensionLastVersion(Collection<String> extensionNames) {
        long start = TimeLog.getTime();

        if (CollectionUtils.isEmpty(extensionNames)) {
            // recuperation de toutes les extensions
            extensionNames = new LinkedHashSet<String>();
            Collection<String> extIds = getAllExtensionIds();
            for (String id : extIds) {
                String name = WikittyExtension.computeName(id);
                extensionNames.add(name);
            }
        }

        Map<String, WikittyExtension> extensionMaps = new LinkedHashMap<String, WikittyExtension>();
        // chargement de la definition de toutes les extensions
        List<WikittyExtension> exts = restoreExtensionAndDependenciesLastVesion(extensionNames);
        for (WikittyExtension ext : exts) {
            extensionMaps.put(ext.getName(), ext);
        }

        // suppression des extensions non souhaitee
        // le mieux serait d'avoir une method sur le client 'restoreExtensionLastVesion'
        extensionMaps.keySet().retainAll(extensionNames);

        Collection<WikittyExtension> result = extensionMaps.values();

        timeLog.log(start, "restoreExtensionLastVersion");
        return result;
    }

    /**
     * Search extension with name in last version.
     *
     * @param extensionNames extension name
     * @return extension wanted with dependencies extensions at head of list
     */
    public List<WikittyExtension> restoreExtensionAndDependenciesLastVesion(Collection<String> extensionNames) {
        long start = TimeLog.getTime();
        List<WikittyExtension> result =
                wikittyService.restoreExtensionAndDependenciesLastVesion(
                securityToken, extensionNames);

        timeLog.log(start, "restoreExtensionAndDependenciesLastVesion");
        return result;

    }

    /**
     * Supprime les extensions specifique de l'objet metier passe en argument.
     * L'objet ne peut donc plus etre du type passer en arguement, un wikitty
     * generique est donc retourne (le wikitty est celui qui etait en interne
     * de la classe metier).
     *
     * Le wikitty retourne doit etre sauve pour que la modification soit reelle
     *
     * @param e une entity dont on veut supprimer l'extension porter par le type
     * metier
     * @return un wikitty
     * @since 3.10
     */
    public Wikitty removeExtension(BusinessEntity e) {
        Wikitty result = castTo(Wikitty.class, e);
        result.removeExtensions(e.getStaticExtensionNames());
        return result;
    }

    /**
     * Supprime les extensions specifique des objets metier passes en argument.
     * L'objet ne peut donc plus etre du type passer en arguement, un wikitty
     * generique est donc retourne (le wikitty est celui qui etait en interne
     * de la classe metier).
     *
     * Le wikitty retourne doit etre sauve pour que la modification soit reelle
     *
     * @param e liste d'entity dont on veut supprimer l'extension porter par le type
     * metier
     * @return un liste de wikitty
     * @since 3.10
     */
    public List<Wikitty> removeExtension(Collection<BusinessEntity> e) {
        List<Wikitty> result = null;
        if (e != null) {
            result = new ArrayList<Wikitty>(e.size());
            for (BusinessEntity b : e) {
                result.add(removeExtension(b));
            }
        }
        return result;
    }

    /**
     * Supprime les extensions specifique des objets metier passes en argument.
     * L'objet ne peut donc plus etre du type passer en arguement, un wikitty
     * generique est donc retourne (le wikitty est celui qui etait en interne
     * de la classe metier).
     *
     * Le wikitty retourne doit etre sauve pour que la modification soit reelle
     *
     * @param e une ou plusieurs entity dont on veut supprimer l'extension porter par le type
     * metier
     * @return un tableau de wikitty
     * @since 3.10
     */
    public Wikitty[] removeExtension(BusinessEntity ... e) {
        Wikitty[] result = new Wikitty[e.length];
        for (int i=0; i<e.length; i++) {
            result[i] = removeExtension(e[i]);
        }
        return result;
    }

    public void deleteExtension(String extName) {
        long start = TimeLog.getTime();
        wikittyService.deleteExtension(securityToken, extName);

        timeLog.log(start, "deleteExtension");
    }

    public void deleteExtension(Collection<String> extNames) {
        long start = TimeLog.getTime();
        wikittyService.deleteExtension(securityToken, extNames);

        timeLog.log(start, "deleteExtension<list>");
    }

    /**
     * Return all extension id (ex: "extName[version])").
     * 
     * @return extension id list
     */
    public List<String> getAllExtensionIds() {
        long start = TimeLog.getTime();
        List<String> result = wikittyService.getAllExtensionIds(securityToken);
        
        timeLog.log(start, "getAllExtensionIds");
        return result;
    }
    
    /**
     * Return all extension id (ex: "extName[version])") where
     * {@code extensionName} is required.
     * 
     * @param extensionName extension name
     * @return extensions
     */
    public List<String> getAllExtensionsRequires(String extensionName) {
        long start = TimeLog.getTime();
        List<String> result = wikittyService.getAllExtensionsRequires(securityToken, extensionName);
        
        timeLog.log(start, "getAllExtensionsRequires");
        return result;
    }

    /**
     * Use with caution : It will delete ALL data !
     * This operation should be disabled in production environment.
     * 
     * @return clear event
     */
    public WikittyEvent clear() {
        long start = TimeLog.getTime();
        WikittyEvent result = wikittyService.clear(securityToken);
        
        timeLog.log(start, "clear");
        return result;
    }

    /**
     * Synchronize search engine with wikitty storage engine, i.e. clear and
     * reindex all object.
     */
    public void syncSearchEngine() {
        long start = TimeLog.getTime();
        wikittyService.syncSearchEngine(securityToken);
        
        timeLog.log(start, "syncSearchEngine");
    }

    /**
     * Method to get the Wikitty encapsulated into a BusinessEntity
     *
     * This method can go to serveur side, if BusinessEntity is not
     * BusinessEntityImpl, this append when use GWT
     *
     * @param entity the BusinessEntity encapsulating the Wikitty
     * @return the wikitty encapsulated
     * @deprecated since 3.4 use {@link #castTo(java.lang.Class, Object) }
     */
    @Deprecated
    public Wikitty getWikitty(BusinessEntity entity){
        long start = TimeLog.getTime();
        Wikitty result;

        if (entity instanceof BusinessEntityImpl) {
            result = ((BusinessEntityImpl) entity).getWikitty();
        } else {
            String id = entity.getWikittyId();

            result = restore(id);

            //try settings all fields except version
            try {
                //get all fields
                Class entityClass = entity.getClass();
                Field[] fields = entityClass.getDeclaredFields();

                for(Field field:fields){
                    //for each field that got WikittyField annotation
                    if (field.isAnnotationPresent(WikittyField.class)){

                        //get the attribute's wikitty fqn
                        WikittyField annotation = field.getAnnotation(WikittyField.class);
                        String fieldFQN = annotation.fqn();

                        //set the value
                        Method m = entityClass.getMethod("get" + StringUtils.capitalize(field.getName()));
                        Object value = m.invoke(entity);

                        result.setFqField(fieldFQN, value);
                    }
                }
            } catch (Exception eee) {
                throw new WikittyException("Could not transform entity to Wikitty", eee);
            }

            //manually set version
            result.setWikittyVersion(entity.getWikittyVersion());
        }
        
        timeLog.log(start, "getWikitty");
        return result;
    }

    /**
     * Check that the logged in user is in a group. A #SecurityException might
     * be thrown at runtime if the #WikittyUser session timed out.
     * @param groupName the name of the group to check
     * @return true is the logged in user is in the group
     */
    public boolean isMember(String groupName) {
        long start = TimeLog.getTime();
        boolean result = false;

        WikittyUser user = getLoggedInUser();

        //Find the group from its name
        WikittyQuery criteria = new WikittyQueryMaker().and()
                .exteq(WikittyGroup.EXT_WIKITTYGROUP)
                .eq(WikittyGroup.FQ_FIELD_WIKITTYGROUP_NAME, groupName)
                .end();

        Wikitty group = findByQuery(Wikitty.class, criteria);

        if (group != null && user != null) {
            result = WikittySecurityUtil.isMember(wikittyService, securityToken,
                    user.getWikittyId(), group.getWikittyId());
        }

        timeLog.log(start, "isMember");
        return result;
    }

    /**
     * Get the #WikittyUser that is logged in. A #SecurityException might be
     * thrown at runtime if the #WikittyUser session timed out.
     * @return the logged in #WikittyUser
     */
    public WikittyUser getLoggedInUser() {
        long start = TimeLog.getTime();

        String userId = WikittySecurityUtil.getUserForToken(wikittyService,
                securityToken);

        WikittyUser user = restore(WikittyUser.class, userId);

        timeLog.log(start, "getLoggedInUser");
        return user;
    }
    
    /**
     * Convert e parameter to the wanted type and return it.
     * For business object transformation, if some
     * result don't have the right extension (clazz) this extension is
     * automatically added.
     *
     * If possible this method don't instanciated new Object. If e is Wikitty
     * and wanted target is Wikitty a simple cast is done.
     *
     * @param target to cast into.
     * Can be Wikitty, BusinessEntity, String, Date, Number (returned is BigDecimal), Boolean, byte[]
     * @param e object to convert
     * @return Object in right class or Exception.
     * if conversion is impossible
     * @since 3.4
     */
    public <E> E castTo(Class<E> target, Object e) {
        E result;

        if (e == null) {
            result = null;
        } else {
            List<E> tmp = castTo(target, WikittyUtil.singletonList(e));
            result = tmp.get(0);
        }
        
        return result;
    }

    /**
     * Convert all result to the wanted type and return new WikittyQueryResult
     * with this new result list. For business object transformation, if some
     * result don't have the right extension (clazz) this extension is
     * automatically added.
     * 
     * @param queryResult result to convert
     * @param target to cast into.
     * Can be Wikitty, BusinessEntity, String, Date, Number (returned is BigDecimal), Boolean, byte[]
     * @return new WikittyQueryResult with element in right class or Exception
     * if conversion is impossible
     */
    public <E> WikittyQueryResult<E> castTo(Class<E> target,
            WikittyQueryResult queryResult) {
        WikittyQueryResult<E> result = queryResult.castTo(this, target);
        return result;
    }

    /**
     * Convert all result to the wanted type and return new WikittyQueryResult
     * with this new result list. For business object transformation, if some
     * result don't have the right extension (clazz) this extension is
     * automatically added.
     *
     * @param queryResult result to convert
     * @param target to cast into.
     * Can be Wikitty, BusinessEntity, String, Date, Number (returned is BigDecimal), Boolean, byte[]
     * @return new WikittyQueryResult with element in right class or Exception
     * if conversion is impossible
     */
    public <E, F> WikittyQueryResult<Map<String, E>> castToMap(Class<E> target,
            WikittyQueryResult<Map<String, F>> queryResult) {
        WikittyQueryResult<Map<String, E>> result = null;
        if (queryResult != null) {
            result = queryResult.castToMap(this, target);
        }
        return result;
    }

    public <O, N, E extends Map<String, O>, F extends Map<String, N>> List<E> castToMap(Class<O> target, List<F> objects) {
        List result = castTo(target, objects);
        return result;
    }


    public <E, F> List<E> castTo(Class<E> target, List<F> objects) {
        List<E> result = (List<E>)objects;

        if (objects == null
                || target == null
                || objects.size() == 0
                // on demande la conversion en Object, donc aucune conversion
                ||target.isAssignableFrom(Object.class)) {
            // on ne fait rien
        } else if (BusinessEntity.class.isAssignableFrom(target)) {
            // on commence par tout mettre en Wikitty, en utilisant une autre regle
            List<Wikitty> resultTmp = castTo(Wikitty.class, objects);
            ListObjectOrMap list = new ListObjectOrMap(resultTmp);
            // ensuite on cree les businessEntity
            for (ListObjectOrMap.ListObjectOrMapIterator i = list.iter(); i.hasNext();) {
                Wikitty o = (Wikitty)i.next();
                Object business = WikittyUtil.newInstance((Class<BusinessEntity>)target, o);
                i.setValue(business);
            }
        } else {
            ListObjectOrMap list = new ListObjectOrMap(objects);
            // les ids a collecter et restorer, et en valeur si besoin le BusinessEntity associe
            Map<String, ListObjectOrMap.Key> ids = new HashMap();

            for (ListObjectOrMap.ListObjectOrMapIterator i = list.iter(); i.hasNext();) {
                Object o = i.next();

                if (Number.class.isAssignableFrom(target)) {
                    BigDecimal v = WikittyUtil.toBigDecimal(o);
                    Number n = WikittyUtil.toNumber(target, v);
                    i.setValue(n);
                } else if (Date.class.isAssignableFrom(target)) {
                    Date v = WikittyUtil.toDate(o);
                    i.setValue(v);
                } else if (Boolean.class.isAssignableFrom(target)) {
                    Boolean v = WikittyUtil.toBoolean(o);
                    i.setValue(v);
                } else if (byte[].class.isAssignableFrom(target)) {
                    byte[] v = WikittyUtil.toBinary(o);
                    i.setValue(v);
                } else if (String.class.isAssignableFrom(target)) {
                    String v = WikittyUtil.toString(o);
                    i.setValue(v);
                } else if (Wikitty.class.isAssignableFrom(target)) {
                    // On veut des Wikitties en sortie

                    if (o instanceof Wikitty) {
                        // W, rien a faire
                    } else if (o instanceof BusinessEntityImpl) {
                        // BusinessEntityImpl, il faut recuperer les wikitties
                        i.setValue(((BusinessEntityImpl)o).getWikitty());
                    } else if (o instanceof BusinessEntity) {
                        // on collecte tous les ids
                        String id = ((BusinessEntity)o).getWikittyId();
                        ids.put(id, i.getKey());
                    } else if (o instanceof String) {
                        // String, il faudra faire un restore
                        ids.put((String)o, i.getKey());
                    } else {
                        throw new ClassCastException("WikittyQueryResult don't contains"
                                + " object convertible to Wikitty"
                                + " (accepted Wikitty, BusinessEntityImpl, String id) but not "
                                + o.getClass());
                    }
                } else {
                    throw new ClassCastException(String.format(
                            "Object list don't contains"
                            + " object convertible to %s"
                            + " (accepted Wikitty, BusinessEntityImpl, String id, Date,"
                            + " BigDecimal, Boolean, byte[]) but not '%s'",
                            target.getName(), o.getClass()));
                }
            }

            if (!ids.isEmpty()) {
                List<String> idsList = new ArrayList<String>(ids.keySet());
                List<Wikitty> wikitties = restore(idsList);
                for (int i=0; i<wikitties.size(); i++) {
                    String id = idsList.get(i);
                    Wikitty w = wikitties.get(i);

                    ListObjectOrMap.Key key = ids.get(id);
                    Object o = key.getValue();
                    if (w != null && o instanceof BusinessEntity) {
                        // il faut mettre a jour le wikitty avec les champs de BusinessEntity
                        WikittyUtil.updateWikitty(w, (BusinessEntity)o);
                    }
                    key.setValue(w);
                }
            }
        }
        return result;
    }

}
