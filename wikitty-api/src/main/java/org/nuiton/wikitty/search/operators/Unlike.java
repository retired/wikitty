/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.search.operators;

import java.io.Serializable;

/**
 * UnLike is use on String field type, to precise some particularity on search
 * (case insensitive for example).
 *
 * @author martel
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 * @deprecated since 3.3 use new query api {@link org.nuiton.wikitty.query.WikittyQuery}
 */
@Deprecated
public class Unlike extends BinaryOperator implements Serializable {

        // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;
    protected Like.SearchAs searchAs;

    public Unlike() {
    }
    public Like.SearchAs getSearchAs() {
        return searchAs;
    }

    public void setSearchAs(Like.SearchAs searchAs) {
        this.searchAs = searchAs;
    }
}
