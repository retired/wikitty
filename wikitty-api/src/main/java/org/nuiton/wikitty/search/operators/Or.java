/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.search.operators;

import java.io.Serializable;
import java.util.List;

/**
 * Or operator is used to build disjunctive restriction for request on content.
 * It take at least 2 args. <br>
 * <br>
 * For example, use:
 * <ul>
 * <li>RestrictionHelper.or( restriction1, restriction2 )</li>
 * <li>RestrictionHelper.or( restriction1, restriction2, restriction3 )</li>
 * <li>RestrictionHelper.or(
 * my_JavaUtilList_Of_Restriction_Witch_Size_Is_Upper_Than2 )</li>
 * </ul>
 * @deprecated since 3.3 use new query api {@link org.nuiton.wikitty.query.WikittyQuery}
 */
@Deprecated
public class Or extends Restriction implements Serializable {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    protected List<Restriction> restrictions;

    /**
     * Default constructor
     */
    public Or() {
        super();
    }

    /**
     * Constructor with all parameters initialized
     * 
     * @param restrictions
     */
    public Or(List<Restriction> restrictions) {
        this.restrictions = restrictions;
    }

    /**
     * Return restrictions
     * 
     * @return
     */
    public List<Restriction> getRestrictions() {
        return restrictions;
    }

    /**
     * Set a value to parameter restrictions.
     * 
     * @param restrictions
     */
    public void setRestrictions(List<Restriction> restrictions) {
        this.restrictions = restrictions;
    }

    /**
     * Equality test based attributes values
     * 
     * @param other Value to compare
     */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Or)) {
            return false;
        }

        final Or or = (Or) other;

        if ((restrictions == null && or.getRestrictions() != null)
                || (restrictions != null && !restrictions.equals(or
                        .getRestrictions())))
            return false;

        return true;
    }

    public int hashCode() {
        // equals use objects that are not constant through time
        // then, unable to create hashCode from those objects 
        // returning a constant hash-code
        return Or.class.hashCode();
    }

}
