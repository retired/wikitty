/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.search;

import java.io.Serializable;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 * @deprecated since 3.3 use new query api {@link org.nuiton.wikitty.query.WikittyQuery}
 */
@Deprecated
public class FacetTopic implements Serializable {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1408493244549775810L;

    protected String facetName;

    protected String topicName;

    protected int count;

    public FacetTopic(String facetName, String topicName, int count) {
        this.facetName = facetName;
        this.topicName = topicName;
        this.count = count;
    }

    public String getFacetName() {
        return facetName;
    }

    public String getTopicName() {
        return topicName;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        String result = String.format(
                "FacetTopic(facet: '%s' topic: '%s' count: '%s')",
                getFacetName(), getTopicName(), getCount());
        return result;
    }

    
}
