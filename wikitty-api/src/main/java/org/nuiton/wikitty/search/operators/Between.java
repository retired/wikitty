/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.search.operators;

import java.io.Serializable;

/**
 * Between operator is used to build restriction containing "min &lt; element
 * &lt; max" where element could be a Integer, a Float or a Date. <br>
 * <br>
 * For example, use: RestrictionHelper.between( myElement , "15.5" , "22.5" )
 * @deprecated  since 3.3 use new query api {@link org.nuiton.wikitty.query.WikittyQuery}
 */
@Deprecated
public class Between extends Restriction implements Serializable {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    protected Element element;
    protected String min;
    protected String max;

    /**
     * Default constructor
     */
    public Between() {
        super();
    }

    /**
     * Constructor with all parameters initialized
     * 
     * @param element
     * @param min
     * @param max
     */
    public Between(Element element, String min, String max) {
        this.element = element;
        this.min = min;
        this.max = max;
    }

    /**
     * Return element
     * 
     * @return
     */
    public Element getElement() {
        return element;
    }

    /**
     * Set a value to parameter element.
     * 
     * @param element
     */
    public void setElement(Element element) {
        this.element = element;
    }

    /**
     * Return min
     * 
     * @return
     */
    public String getMin() {
        return min;
    }

    /**
     * Set a value to parameter min.
     * 
     * @param min
     */
    public void setMin(String min) {
        this.min = min;
    }

    /**
     * Return max
     * 
     * @return
     */
    public String getMax() {
        return max;
    }

    /**
     * Set a value to parameter max.
     * 
     * @param max
     */
    public void setMax(String max) {
        this.max = max;
    }

    /**
     * Equality test based attributes values
     * 
     * @param other Value to compare
     */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Between)) {
            return false;
        }

        final Between between = (Between) other;

        if ((element == null && between.getElement() != null)
                || (element != null && !element.equals(between.getElement()))) {
            return false;
        }
        if ((min == null && between.getMin() != null) 
                || (min != null && !min.equals(between.getMin()))) {
            return false;
        }
        if ((max == null && between.getMax() != null)
                || (max != null && !max.equals(between.getMax()))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        // equals use objects that are not constant through time
        // then, unable to create hashCode from those objects 
        // returning a constant hash-code
        return Between.class.hashCode();
    }

}
