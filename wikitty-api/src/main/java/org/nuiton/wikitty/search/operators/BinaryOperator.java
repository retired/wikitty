/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.search.operators;

import java.io.Serializable;

/**
 * This class is an abstract class that's used to factor each operator that
 * handle two parameters (=, !=, &lt;, &lt;=, &gt;, &gt;=, end, begin).
 * @deprecated since 3.3 use new query api {@link org.nuiton.wikitty.query.WikittyQuery}
 */
@Deprecated
public class BinaryOperator extends Restriction implements Serializable {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    protected Element element;
    protected String value;

    /**
     * Default constructor
     */
    public BinaryOperator() {
        super();
    }

    /**
     * Constructor with all parameters initialized
     * 
     * @param element
     * @param value
     */
    public BinaryOperator(Element element, String value) {
        this.element = element;
        this.value = value;
    }

    /**
     * Return element
     * 
     * @return
     */
    public Element getElement() {
        return element;
    }

    /**
     * Set a value to parameter element.
     * 
     * @param element
     */
    public void setElement(Element element) {
        this.element = element;
    }

    /**
     * Return value
     * 
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     * Set a value to parameter value.
     * 
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Equality test based attributes values
     * 
     * @param other Value to compare
     */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof BinaryOperator)) {
            return false;
        }

        final BinaryOperator binaryOperator = (BinaryOperator) other;

        if ((element == null && binaryOperator.getElement() != null)
                || (element != null && !element.equals(binaryOperator
                        .getElement()))) {
            return false;
        }
        if ((value == null && binaryOperator.getValue() != null)
                || (value != null && !value.equals(binaryOperator.getValue()))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        // equals use objects that are not constant through time
        // then, unable to create hashCode from those objects 
        // returning a constant hash-code
        return BinaryOperator.class.hashCode();
    }

}
