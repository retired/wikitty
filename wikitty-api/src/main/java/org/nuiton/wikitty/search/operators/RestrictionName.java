/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.search.operators;

/**
 * <p>
 * This enum contains all kind of restriction used to request content. It's used
 * by parser to create lucene request from RestrictionDto.
 * </p>
 * @deprecated since 3.3 use new query api {@link org.nuiton.wikitty.query.WikittyQuery}
 */
@Deprecated
public enum RestrictionName {
    EQUALS,
    NOT_EQUALS,
    LIKE,
    UNLIKE,
    LESS,
    LESS_OR_EQUAL,
    GREATER,
    GREATER_OR_EQUAL,
    CONTAINS,
    STARTS_WITH,
    ENDS_WITH,
    NOT,
    AND,
    OR,
    BETWEEN,
    TRUE,
    FALSE,
    ASSOCIATED,
    IN,
    KEYWORD,
    IS_NULL,
    IS_NOT_NULL
}
