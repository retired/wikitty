/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.search;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.nuiton.wikitty.WikittyException;

import org.nuiton.wikitty.search.operators.Restriction;

/**
 * Criteria represent the search on the wikitty with restriction on field and 
 * informations on facet, pagination and sorting.
 *<p>
 * For search multiple extension use ALL_EXTENSIONS as extension name and
 * specify field type.
 * <p>
 * Example : Criteria.ALL_EXTENSIONS + Criteria.SEPARATOR + &lt;fieldName&gt; + Criteria.SEPARATOR + FieldType.TYPE.&lt;fieldType&gt;
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 * @deprecated since 3.3 use new query api {@link org.nuiton.wikitty.query.WikittyQuery}
 */
@Deprecated
public class Criteria implements Serializable, Cloneable {
    /** serialVersionUID. */
    private static final long serialVersionUID = 2590223960861630283L;

    /** Use to not limit result. */
    static final public int ALL_ELEMENTS = -1;

    /** Search on all extension. */
    static final public String ALL_EXTENSIONS = "*";

    /** Search on all values. */
    static final public String ALL_VALUES = "*";

    /** Separator between extension name, field name and type. */
    static final public String SEPARATOR = ".";

    /**
     * Criteria can have name this help for presentation and is very
     * useful for facet created by criteria.
     */
    protected String name;

    /**
     * Use to return select ids
     * Be carefull, can be long if many result is found
     */
    protected String select;

    /** All restriction on fields. */
    protected Restriction restriction;

    /** First index to get result. */
    protected int firstIndex = 0;
    /** Last index to get result. */
    protected int endIndex = ALL_ELEMENTS;

    /**
     * nombre minimum de valeur pour qu'une valeur apparaisse dans les facets.
     * par defaut, il doit y avoir plus que 1 valeur.
     */
    protected int facetMinCount = 1;
    /**
     * Nombre maximum de facet a retourner apres la requete. Par default on en
     * retourne 100.
     */
    protected int facetLimit = 100;

    /** Facet on criteria. */
    protected List<Criteria> facetCriteria;
    /** Facet on field. */
    protected List<String> facetField;

    /** Sort ascending on fields. */
    protected List<String> sortAscending;
    /** Sort descending on fields. */
    protected List<String> sortDescending;

    /**
     * Default constructor.
     */
    public Criteria() {
    }

    
    /**
     * Cree un nouvel objet criteria ou l'objet restriction est pointer par les
     * deux critere apres le clone. La modification d'une restriction impactera
     * l'autre critere. (Ce comportement peut-etre modifier si besoin).
     * Tous les autres champs sont en deepcopy.
     * 
     * @return
     * @throws WikittyException
     */
    @Override
    public Criteria clone() {
        try {
            Object result = super.clone();
            Criteria clone = (Criteria) result;

            if (facetField != null) {
                clone.facetField = new ArrayList<String>(facetField);
            }
            if (sortAscending != null) {
                clone.sortAscending = new ArrayList<String>(sortAscending);
            }

            if (sortDescending != null) {
                clone.sortDescending = new ArrayList<String>(sortDescending);
            }

            if (facetCriteria != null) {
                clone.facetCriteria = new ArrayList<Criteria>();
                for (Criteria c : facetCriteria) {
                    clone.facetCriteria.add(c.clone());
                }
            }

            if (select != null) {
                clone.select = select;
            }

            return clone;
        } catch (CloneNotSupportedException eee) {
            throw new WikittyException("Can't clone criteria", eee);
        }
    }

    /**
     * Create named criteria.
     * 
     * @param name criteria name
     */
    public Criteria(String name) {
        this();
        this.name = name;
    }

    /**
     * Deserialize xml to criteria.
     * 
     * @param xml xml to deserialize
     * @return criteria criteria
     */
    public static Criteria fromXML(String xml) {
        InputStream inputStream = new ByteArrayInputStream(xml.getBytes());
        XMLDecoder decoder = new XMLDecoder(inputStream);
        Criteria criteria = (Criteria) decoder.readObject();
        return criteria;
    }

    /**
     * Serialize criteria to xml.
     * 
     * @param criteria criteria to serialize
     * @return xml string
     */
    public static String toXML(Criteria criteria) {
        OutputStream outputStream = new ByteArrayOutputStream();
        XMLEncoder encoder = new XMLEncoder(outputStream);
        encoder.writeObject(criteria);
        encoder.close();
        String result = outputStream.toString();
        return result;
    }

    public Restriction getRestriction() {
        return restriction;
    }

    public void setRestriction(Restriction restriction) {
        this.restriction = restriction;
    }

    /**
     * Substitue la restriction de ce critere par celui de search
     * @param search
     */
    public void setRestriction(Search search) {
        this.restriction = search.getRestrictions();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public int getFirstIndex() {
        return firstIndex;
    }

    public Criteria setFirstIndex(int firstIndex) {
        this.firstIndex = firstIndex;
        return this;
    }
    
    public int getEndIndex() {
        return endIndex;
    }
    
    public Criteria setEndIndex(int endIndex) {
        this.endIndex = endIndex;
        return this;
    }

    public int getFacetMinCount() {
        return facetMinCount;
    }

    public Criteria setFacetMinCount(int facetMinCount) {
        this.facetMinCount = facetMinCount;
        return this;
    }

    public int getFacetLimit() {
        return facetLimit;
    }

    public Criteria setFacetLimit(int facetLimit) {
        this.facetLimit = facetLimit;
        return this;
    }

    public List<Criteria> getFacetCriteria() {
        return facetCriteria;
    }
    
    public Criteria addFacetCriteria(Criteria criteria) {
        if (facetCriteria == null) {
            facetCriteria = new ArrayList<Criteria>();
        }
        facetCriteria.add(criteria);
        return this;
    }
    
    public Criteria setFacetCriteria(Criteria[] facetCriteria) {
        this.facetCriteria = Arrays.asList(facetCriteria);
        return this;
    }

    public List<String> getFacetField() {
        return facetField;
    }
    
    public Criteria addFacetField(String field) {
        if (facetField == null) {
            facetField = new ArrayList<String>();
        }
        facetField.add(field);
        return this;
    }

    public Criteria setFacetField(String[] facetField) {
        this.facetField = Arrays.asList(facetField);
        return this;
    }

    /**
     * Get field names where sort is configured ascending.
     * 
     * @return field names
     */
    public List<String> getSortAscending() {
        return sortAscending;
    }

    public Criteria addSortAscending(String ... field) {
        if (sortAscending == null) {
            sortAscending = new ArrayList<String>();
        }
        sortAscending.addAll(Arrays.asList(field));
        return this;
    }

    public Criteria setSortAscending(String ... sortAscending) {
        this.sortAscending = Arrays.asList(sortAscending);
        return this;
    }

    /**
     * Get field names where sort is configured descending.
     * 
     * @return field names
     */
    public List<String> getSortDescending() {
        return sortDescending;
    }

    public Criteria addSortDescending(String ... field) {
        if (sortDescending == null) {
            sortDescending = new ArrayList<String>();
        }
        sortDescending.addAll(Arrays.asList(field));
        return this;
    }

    public Criteria setSortDescending(String ... sortDescending) {
        this.sortDescending = Arrays.asList(sortDescending);
        return this;
    }

    @Override
    public String toString() {
        return toXML(this);
    }
}
