/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.search.operators;

import java.io.Serializable;

/**
 * This element is a symbolic interface implemented by all operators used to
 * request contents (And, Or, Not, Equals, NotEquals, EndsWith, ...).
 * @deprecated since 3.3 use new query api {@link org.nuiton.wikitty.query.WikittyQuery}
 */
@Deprecated
public class Restriction implements Serializable {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    protected RestrictionName name;

    /**
     * Default constructor
     */
    public Restriction() {
        super();
    }

    /**
     * Constructor with all parameters initialized
     * 
     * @param name
     */
    public Restriction(RestrictionName name) {
        this();
        this.name = name;
    }

    /**
     * Return name
     * 
     * @return
     */
    public RestrictionName getName() {
        return name;
    }

    /**
     * Set a value to parameter name.
     * 
     * @param name
     */
    public void setName(RestrictionName name) {
        this.name = name;
    }

    /**
     * Equality test based attributes values
     * 
     * @param other Value to compare
     */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Restriction)) {
            return false;
        }

        final Restriction restrictionDto = (Restriction) other;

        if ((name == null && restrictionDto.getName() != null)
                || (name != null && !name.equals(restrictionDto.getName()))) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        // equals use objects that are not constant through time
        // then, unable to create hashCode from those objects 
        // returning a constant hash-code
        return Restriction.class.hashCode();
    }

}
