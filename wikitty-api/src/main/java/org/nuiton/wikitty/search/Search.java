/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.search;

import org.nuiton.wikitty.search.operators.Like;
import org.nuiton.wikitty.search.operators.Restriction;
import org.nuiton.wikitty.search.operators.SubSearch;
import org.nuiton.wikitty.search.operators.Element;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.WikittyUtil;

/**
 * Helper to create a criteria with restrictions.
 *
 * To create a search, use Search.query() and add restrictions to that
 * query. In the end, create the criteria from the query and use it on find
 * methods.
 *
 * Element :
 * * &lt;extensionName&gt;.&lt;fieldName&gt;[.&lt;fieldType&gt;] : search on an extension and field with specific type (optional)
 * * Criteria.ALL_EXTENSIONS.&lt;fieldName&gt;.&lt;fieldType&gt; : search on all extension and field name with specific type
 *
 * &lt;fieldType&gt; specify search on field as NUMERIC, STRING, WIKITTY, BOOLEAN, DATE. Use Element.ElementType to specify type.
 *
 * Some patterns might be available depending on the restriction used. They are :
 * * '*' replace any number of characters
 * * '?' replace one character
 * @deprecated since 3.3 use new query api {@link org.nuiton.wikitty.query.WikittyQuery}
 */
@Deprecated
public class Search {

    /** Default operator type between all search condition. */
    public enum KIND {
        AND, OR, NOT
    }

    /** Defaut kind to {@link KIND#AND}. */
    protected KIND kind = KIND.AND;

    protected List<Restriction> restrictions;

    protected List<Search> subSearchs;

    public Search() {
        restrictions = new ArrayList<Restriction>();
        subSearchs = new ArrayList<Search>();
    }

    /**
     * Create Search query with field in wikitty argument.
     * Null field are not used in search request.
     *
     * @param wikitty example use to create query
     * @return query
     */
    static public Search query(Wikitty wikitty) {
        Search result = Search.query();
        result.kind = KIND.AND;
        // result object must have same extension that wikitty example
        for (String extName : wikitty.getExtensionNames()) {
            result.eq(Element.ELT_EXTENSION, extName);
        }

        for (String fqfieldName : wikitty.fieldNames()) {
            Object value = wikitty.getFqField(fqfieldName);
            if (value != null) {
                FieldType type = wikitty.getFieldType(fqfieldName);
                if (type.isCollection()) {
                    Collection<?> collection = (Collection<?>) value;
                    for (Object o : collection) {
                        String strValue = WikittyUtil.toStringForSearchEngine(type, o);
                        result.eq(fqfieldName, strValue);
                    }
                } else {
                    String strValue = WikittyUtil.toStringForSearchEngine(type, value);
                    result.eq(fqfieldName, strValue);
                }
            }
        }

        return result;
    }

    /**
     * Create new {@code Search} object with default kind to {@link KIND#AND}.
     * 
     * @return Search helper
     */
    public static Search query() {
        Search search = query(KIND.AND);
        return search;
    }

    /**
     * Create new {@code Search} object with custom kind.
     * 
     * @param kind kind
     * @return Search helper
     */
    public static Search query(KIND kind) {
        Search search = new Search();
        search.kind = kind;
        return search;
    }

    /**
     * Create a new query on an existing criteria to add new constraints to
     * the existing criteria.
     * 
     * @param criteria the existing criteria
     * @return a new query to add constraints to the criteria.
     */
    public static Search query(Criteria criteria) {
        Search search = query();
        if (criteria != null) {
            search.restrictions.add(criteria.getRestriction());
        }
        return search;
    }
    
    protected static Element elt(String element) {
        Element elm = new Element();
        elm.setName(element);
        return elm;
    }

    /**
     * Contains.
     *
     * Search on lists (multivalued fields) that a field contains all the values
     * of the list given in parameter.
     *
     * Ex : The field with value [toto,titi,tutu] contains [titi,tutu] but not
     * [titi,tutu,tata]
     *
     * Ps : Use wildcards if you search for substrings.
     *
     * @param element the element on which the restriction is put
     * @param values the values to search in the element
     * @return {@code this} with the {@code contains} restriction added.
     */
    public Search contains(String element, Collection<String> values) {
        restrictions.add(RestrictionHelper.contains(elt(element),
                new ArrayList<String>(values)));
        return this;
    }

    /**
     * Contains.
     *
     * Search on lists (multivalued fields) that a field contains all the values
     * given in parameter.
     *
     * Ex : The field with value [toto,titi,tutu] contains [titi,tutu] but not
     * [titi,tutu,tata]
     *
     * Ps : Use wildcards if you search for substrings.
     *
     * @param element the element on which the restriction is put
     * @param value1 first value to search in the field
     * @param values list of values to search in the field
     * @return {@code this} with the {@code contains} restriction added.
     */
    public Search contains(String element, String value1, String ... values) {
        restrictions.add(RestrictionHelper.contains(elt(element),
                value1, values));
        return this;
    }

    /**
     * In.
     *
     * Search if a field is contained in the list of values in parameter
     *
     * Ex : The field with value titi is in [titi,tutu] but not in
     * [tutu,tata]
     *
     * Ps : Use wildcards in the values if you search for substrings.
     *
     * @param element the element on which the restriction is put
     * @param values list of values the field must be in
     * @return {@code this} with the {@code in} restriction added.
     */
    public Search in(String element, Collection<String> values) {
        restrictions.add(RestrictionHelper.in(elt(element),
                new ArrayList<String>(values)));
        return this;
    }

    /**
     * In.
     *
     * Search if a field is contained in the list of values in parameter
     *
     * Ex : The field with value titi is in [titi,tutu] but not in
     * [tutu,tata]
     *
     * Ps : Use wildcards in the values if you search for substrings.
     *
     * @param element the element on which the restriction is put
     * @param value1 first value the field must be in
     * @param values list of values the field must be in
     * @return {@code this} with the {@code in} restriction added.
     */
    public Search in(String element, String value1, String ... values) {
        restrictions.add(RestrictionHelper.in(elt(element),
                value1, values));
        return this;
    }

    /**
     * Equals.
     *
     * Restrict search so that the field value equals the parameter.
     *
     * You might use patterns in your equality.
     *
     * @param element the field on which the search is made
     * @param value the value the element must be equals to
     * @return {@code this}
     */
    public Search eq(String element, String value) {
        restrictions.add(RestrictionHelper.eq(elt(element), value));
        return this;
    }

    /**
     * Extension equals.
     *
     * Restrict search to wikitties that got the extension in parameter.
     *
     * @param value the extension to restrict the results to
     * @return {@code this} with the {@code exteq} restriction added.
     */
    public Search exteq(String value) {
        restrictions.add(RestrictionHelper.eq(elt(Element.ELT_EXTENSION), value));
        return this;
    }

    /**
     * Id equals.
     *
     * Restrict search to wikitties that got the id in parameter.
     *
     * @param value the id to restrict the results to
     * @return {@code this} with the {@code ideq} restriction added.
     */
    public Search ideq(String value) {
        restrictions.add(RestrictionHelper.eq(elt(Element.ELT_ID), value));
        return this;
    }

    /**
     * Equals.
     *
     * Restrict search so that the field value equals all the members of the
     * list in parameters.
     *
     * @param element the element on which the restriction is put
     * @param values list of values that the element must be equals to
     * @return {@code this} with the {@code ideq} restriction added.
     */
    @Deprecated
    public Search eq(String element, Collection<String> values) {
        // erreur, car si le search est fait avec un OR au lieu d'un AND
        // on a pas ce que l'on veut.
        for (String value : values) {
            restrictions.add(RestrictionHelper.eq(elt(element), value));
        }
        return this;
    }

    /**
     * Extension equals.
     *
     * Restrict search to wikitties that got all the extensions in parameter.
     *
     * @param values list of the extension to restrict the results to
     * @return {@code this} with the {@code exteq} restriction added.
     */
    public Search exteq(Collection<String> values) {
        // erreur, car si le search est fait avec un OR au lieu d'un AND
        // on a pas ce que l'on veut.
        for (String value : values) {
            restrictions.add(RestrictionHelper.eq(elt(Element.ELT_EXTENSION), value));
        }
        return this;
    }

    /**
     * Not equals.
     *
     * Restrict search to elements that are not equals to the value given in
     * parameter.
     * 
     * @param element the element on which the restriction is put
     * @param value the value the element must not be equals to.
     * @return {@code this} with the {@code neq} restriction added.
     */
    public Search neq(String element, String value) {
        restrictions.add(RestrictionHelper.neq(elt(element), value));
        return this;
    }

    /**
     * Extension not equals.
     *
     * Restrict search to wikitties that do not get the extension given in
     * parameter.
     *
     * @param value the extension that the wikitties must not have.
     * @return {@code this} with the {@code extneq} restriction added.
     */
    public Search extneq(String value) {
        restrictions.add(RestrictionHelper.neq(elt(Element.ELT_EXTENSION), value));
        return this;
    }

    /**
     * Id not equals.
     *
     * Restrict search to wikitties that do not have the id given in parameter.
     *
     * @param value the id the wikitties must not have.
     * @return {@code this} with the {@code idneq} restriction added.
     */
    public Search idneq(String value) {
        restrictions.add(RestrictionHelper.neq(elt(Element.ELT_ID), value));
        return this;
    }

    /**
     * Greater than.
     *
     * Search if an element value is greater than the parameter.
     * 
     * @param element the element on which the restriction is put
     * @param value the value to be compared to
     * @return {@code this} with the {@code gt} restriction added.
     */
    public Search gt(String element, String value) {
        restrictions.add(RestrictionHelper.great(elt(element), value));
        return this;
    }

    /**
     * Greater than or equals.
     *
     * Search if an element value is greater than or equals to the parameter.
     * 
     * @param element the field on which the search is made
     * @param value the value to be compared to
     * @return {@code this} with the {@code ge} restriction added.
     */
    public Search ge(String element, String value) {
        restrictions.add(RestrictionHelper.greatEq(elt(element), value));
        return this;
    }

    /**
     * Less than.
     *
     * Search if an element value is less than the parameter.
     * 
     * @param element the element on which the restriction is put
     * @param value the value to be compared to
     * @return {@code this} with the {@code lt} restriction added.
     */
    public Search lt(String element, String value) {
        restrictions.add(RestrictionHelper.less(elt(element), value));
        return this;
    }

    /**
     * Less than or equals.
     *
     * Search if an element value is less than or equals to the parameter.
     * 
     * @param element the element on which the restriction is put.
     * @param value the value to be compared to.
     * @return {@code this} with the {@code le} restriction added.
     */
    public Search le(String element, String value) {
        restrictions.add(RestrictionHelper.lessEq(elt(element), value));
        return this;
    }

    /**
     * Between.
     *
     * Restrict search so that the element value is between the lower and upper
     * values (it can also be equals).
     * 
     * @param element the element on which the restriction is put.
     * @param lowerValue the lower bound.
     * @param upperValue the upper bound.
     * @return {@code this} with the {@code le} restriction added.
     */
    public Search bw(String element, String lowerValue, String upperValue) {
        restrictions.add(RestrictionHelper.between(elt(element), lowerValue, upperValue));
        return this;
    }

    /**
     * Starts with.
     *
     * Search if an element starts with the value in parameter.
     *
     * @param element the element on which the restriction is put.
     * @param value the value the element must start with.
     * @return {@code this} with the {@code sw} restriction added.
     */
    public Search sw(String element, String value) {
        restrictions.add(RestrictionHelper.start(elt(element), value));
        return this;
    }

    /**
     * Not starts with.
     *
     * Search if an element does not starts with the value in parameter.
     * 
     * @param element the element on which the restriction is put.
     * @param value the value the element must not start with.
     * @return {@code this} with the {@code nsw} restriction added.
     */
    public Search nsw(String element, String value) {
        restrictions.add(RestrictionHelper.not(
                        RestrictionHelper.start(elt(element), value)));
        return this;
    }

    /**
     * Ends with.
     *
     * Search if an element ends with the value in parameter.
     * 
     * @param element the element on which the restriction is put
     * @param value the value the element must ends with.
     * @return {@code this} with the {@code ew} restriction added.
     */
    public Search ew(String element, String value) {
        restrictions.add(RestrictionHelper.end(elt(element), value));
        return this;
    }

    /**
     * Not ends with.
     *
     * Search if an element does not ends with the value in parameter.
     *
     * @param element the element on which the restriction is put
     * @param value the value the element must not ends with.
     * @return {@code this} with the {@code notew} restriction added.
     */
    public Search notew(String element, String value) {
        restrictions.add(RestrictionHelper.not(
                        RestrictionHelper.end(elt(element), value)));
        return this;
    }

    /**
     * Keyword.
     *
     * Search if the value in parameter is present in any field of any
     * extension.
     * 
     * @param value the value to find.
     * @return {@code this} with the {@code keyword} restriction added.
     */
    public Search keyword(String value) {
        restrictions.add(RestrictionHelper.keyword(value));
        return this;
    }

    /**
     * Is null.
     *
     * Check that a field is null.
     *
     * @param fieldName the field that must be null.
     * @return {@code this} with the {@code isNull} restriction added.
     */
    public Search isNull(String fieldName) {
        restrictions.add(RestrictionHelper.isNull(fieldName));
        return this;
    }

    /**
     * Is not null.
     *
     * Check that a field is not null.
     *
     * @param fieldName the field that must not be null.
     * @return {@code this} with the {@code isNotNull} restriction added.
     */
    public Search isNotNull(String fieldName) {
        restrictions.add(RestrictionHelper.isNotNull(fieldName));
        return this;
    }

    /**
     * False.
     *
     * Add a restriction that always return false.
     *
     * @return {@code this} with the {@code rFalse} restriction added.
     */
    public Search rFalse() {
        restrictions.add(RestrictionHelper.rFalse());
        return this;
    }

    /**
     * True.
     *
     * Add a restriction that always return true.
     *
     * @return {@code this} with the {@code rTrue} restriction added.
     */
    public Search rTrue() {
        restrictions.add(RestrictionHelper.rTrue());
        return this;
    }

    /**
     * Like.
     *
     * Check that a string is present in a field. For example "tric" is present
     * in "Restriction".
     *
     * @param element the element on which the restriction is put
     * @param value
     * @param searchAs
     * @return {@code this}
     */
    public Search like(String element, String value, Like.SearchAs searchAs) {
        restrictions.add(RestrictionHelper.like(elt(element), value, searchAs));
        return this;
    }

    public Search like(String element, String value) {
        restrictions.add(RestrictionHelper.like(
                elt(element), value, Like.SearchAs.AsText));
        return this;
    }

    /**
     * Unlike.
     *
     * @param element the element on which the restriction is put
     * @param value
     * @param searchAs
     * @return {@code this}
     */
    public Search unlike(String element, String value, Like.SearchAs searchAs) {
        restrictions.add(RestrictionHelper.unlike(elt(element), value, searchAs));
        return this;
    }

    public Search unlike(String element, String value) {
        restrictions.add(RestrictionHelper.unlike(
                elt(element), value, Like.SearchAs.AsText));
        return this;
    }

    /**
     * Not (sub query).
     * 
     * @return sub query
     */
    public Search not() {
        Search not = Search.query(KIND.NOT);
        subSearchs.add(not);

        Search search = Search.query(kind);
        not.subSearchs.add(search);

        return search;
    }

    /**
     * Or (sub query).
     * 
     * @return sub query
     */
    public Search or() {
        Search search = Search.query(KIND.OR);
        subSearchs.add(search);
        return search;
    }

    /**
     * And (sub query).
     * 
     * @return sub query
     */
    public Search and() {
        Search search = Search.query(KIND.AND);
        subSearchs.add(search);
        return search;
    }

    /**
     * Add {@link SubSearch} to allow search on association (like sql join)
     *
     * @param foreignFieldName association fieldName
     * @return sub query
     */
    public Search associated(String foreignFieldName) {
        Search search = new SubSearch(foreignFieldName);
        subSearchs.add(search);
        return search;
    }
    
    /**
     * Return named criteria.
     * 
     * @param name name of criteria
     * @return new criteria
     */
    public Criteria criteria(String name) {
        Criteria criteria = new Criteria(name);
        Restriction result = getRestrictions();
        criteria.setRestriction(result);
        return criteria;
    }

    /**
     * Return unnamed criteria.
     * 
     * @return new criteria
     */
    public Criteria criteria() {
        Criteria criteria = criteria(null);
        return criteria;
    }

    protected Restriction getRestrictions() throws UnsupportedOperationException {
        Restriction result;
        if (restrictions.isEmpty() && subSearchs.isEmpty()) {
            result = RestrictionHelper.rFalse();

        } else if (restrictions.size() == 1 && subSearchs.isEmpty()) {
            // WARN , was restrictions.remove(0); but uncommented :(
            result = restrictions.get(0);

        } else if (subSearchs.size() == 1 && restrictions.isEmpty()) {
            Search subSearch = subSearchs.get(0);
            result = subSearch.getRestrictions();

            if (kind == KIND.NOT) {
                result = RestrictionHelper.not(result);
            }

        } else {
            List<Restriction> allRestrictions = new ArrayList<Restriction>(restrictions);
            for (Search subSearch : subSearchs) {
                Restriction restriction = subSearch.getRestrictions();
                allRestrictions.add(restriction);
            }
            switch (kind) {
                case OR:
                    result = RestrictionHelper.or(allRestrictions);
                    break;
                case AND:
                    result = RestrictionHelper.and(allRestrictions);
                    break;
                default:
                    throw new UnsupportedOperationException("Can't handle restriction kind " + kind.name());
            }
        }
        return result;
    }

    
}
