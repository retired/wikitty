/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.search.operators;

import java.io.Serializable;

/**
 * NotEquals operator is used to build restriction containing "element != value"
 * where element could be an Id, a String, a multimedia, a closed list, an
 * Integer, a Float or a Date. <br>
 * <br>
 * For example, use: RestrictionHelper.neq( myElement , "REF1234567890" )
 * @deprecated since 3.3 use new query api {@link org.nuiton.wikitty.query.WikittyQuery}
 */
@Deprecated
public class NotEquals extends BinaryOperator implements Serializable {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor
     */
    public NotEquals() {
        super();
    }

    /**
     * Equality test based attributes values
     * 
     * @param other Value to compare
     */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof NotEquals)) {
            return false;
        }

        final NotEquals notEquals = (NotEquals) other;

        return super.equals(notEquals);
    }

    public int hashCode() {
        // equals use objects that are not constant through time
        // then, unable to create hashCode from those objects 
        // returning a constant hash-code
        return NotEquals.class.hashCode();
    }

}
