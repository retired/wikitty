/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.search.operators;

import java.io.Serializable;
import java.util.List;

/**
 * Contains operator is used to build restriction containing "(element like
 * *value1 or element like value1*) and (element like *value2 or element like
 * value2*)" where element could be a String, a multimedia, a text or an xhtml
 * <br>
 * <br>
 * For example, use:
 * <ul>
 * <li>RestrictionHelper.contains( myElement , "value1" )</li>
 * <li>RestrictionHelper.contains( myElement , "value1", "value2", ... )</li>
 * <li>RestrictionHelper.contains( myElement ,
 * a_list_containing_at_least_one_string )</li>
 * </ul>
 * @deprecated since 3.3 use new query api {@link org.nuiton.wikitty.query.WikittyQuery}
 */
@Deprecated
public class Contains extends Restriction implements Serializable {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    protected Element element;
    protected List<String> value;

    /**
     * Default constructor
     */
    public Contains() {
        super();
    }

    /**
     * Constructor with all parameters initialized
     * 
     * @param element
     * @param value
     */
    public Contains(Element element, List<String> value) {
        this.element = element;
        this.value = value;
    }

    /**
     * Return element
     * 
     * @return
     */
    public Element getElement() {
        return element;
    }

    /**
     * Set a value to parameter element.
     * 
     * @param element
     */
    public void setElement(Element element) {
        this.element = element;
    }

    /**
     * Return value
     * 
     * @return
     */
    public List<String> getValue() {
        return value;
    }

    /**
     * Set a value to parameter value.
     * 
     * @param value
     */
    public void setValue(List<String> value) {
        this.value = value;
    }

    /**
     * Equality test based attributes values
     * 
     * @param other Value to compare
     */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Contains)) {
            return false;
        }

        final Contains contains = (Contains) other;

        if ((element == null && contains.getElement() != null)
                || (element != null && !element.equals(contains.getElement())))
            return false;
        if ((value == null && contains.getValue() != null)
                || (value != null && !value.equals(contains.getValue())))
            return false;

        return true;
    }

    public int hashCode() {
        // equals use objects that are not constant through time
        // then, unable to create hashCode from those objects 
        // returning a constant hash-code
        return Contains.class.hashCode();
    }

}
