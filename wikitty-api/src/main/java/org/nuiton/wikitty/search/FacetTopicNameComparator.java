/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.search;

import java.util.Comparator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 * @deprecated  since 3.3 use new query api {@link org.nuiton.wikitty.query.WikittyQuery}
 */
@Deprecated
public class FacetTopicNameComparator implements Comparator<FacetTopic> {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(FacetTopicNameComparator.class);

    protected boolean ignoreCase = true;

    public FacetTopicNameComparator() {
    }

    public FacetTopicNameComparator(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }
    
    public int compare(FacetTopic o1, FacetTopic o2) {
        int result;
        if (ignoreCase) {
            result = o1.getTopicName().compareToIgnoreCase(o2.getTopicName());   
        } else {
            result = o1.getTopicName().compareTo(o2.getTopicName());
        }
        return result;
    }
}
