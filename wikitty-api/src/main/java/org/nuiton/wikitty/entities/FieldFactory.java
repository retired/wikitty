/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.entities;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

import org.nuiton.wikitty.entities.WikittyTypes;

public class FieldFactory {
    
    protected ExtensionFactory parentFactory;
    protected String fieldName; 
    protected WikittyTypes type;
    protected int minOccur = 0, maxOccur = 1; 
    protected boolean unique = false;
    protected Map<String, String> tagValues = new HashMap<String, String>();
    
    public FieldFactory(ExtensionFactory parentFactory, String fieldName, WikittyTypes type) {
        this.parentFactory = parentFactory;
        this.fieldName = fieldName;
        this.type = type;
    }
    
    public FieldFactory addField(String name, WikittyTypes type) {
        append();
        return parentFactory.addField(name, type);
    }
    
    public WikittyExtension extension() {
        append();
        return parentFactory.extension();
    }
    
    protected void append() {
        FieldType fieldType = new FieldType(type, minOccur, maxOccur);
        for (Map.Entry<String, String> entry : tagValues.entrySet()) {
            String tag = entry.getKey();
            String value = entry.getValue();
            fieldType.addTagValue(tag, value);
        }
        parentFactory.add(fieldName, fieldType);
    }
    
    public FieldFactory maxOccur(int max) {
        maxOccur = max;
        return this;
    }
    
    public FieldFactory minOccur(int min) {
        minOccur = min;
        return this;
    }

    /**
     * Si le champs est une collection indique que la collection ne peut pas
     * contenir de doublon (Set)
     * @return
     */
    public FieldFactory unique() {
        addTagValue(FieldType.TAG_UNIQUE, "true");
        return this;
    }

    public FieldFactory notNull() {
        addTagValue(FieldType.TAG_NOT_NULL, "true");
        return this;
    }

    /**
     * Ajoute le tag crypt au champs
     * @param algo l'algo de cryptage par exemple Blowfish
     * @param password le mot de passe, ou null pour utiliser le mot de passe general
     * @return
     */
    public FieldFactory crypt(String algo, String password) {
        if (StringUtils.isNotBlank(password)) {
            algo += ":" + password;
        }
        addTagValue(FieldType.TAG_CRYPT, "true");
        return this;
    }

    /**
     * Ajoute la liste de valeurs autorises pour ce champs.
     * Par exemple pour un champs de type Wikitty: "Person,Employee,Company"
     * 
     * @param values La liste de valeur en representation chaine (le separateur
     * est la virgule)
     * @return
     */
    public FieldFactory allowed(String values) {
        addTagValue(FieldType.TAG_ALLOWED, values);
        return this;
    }

    public FieldFactory allowedQuery(String allowedQuery) {
        addTagValue(FieldType.TAG_ALLOWED_QUERY, allowedQuery);
        return this;
    }

    public FieldFactory choice(String values) {
        addTagValue(FieldType.TAG_CHOICE, values);
        return this;
    }

    public FieldFactory choiceQuery(String choiceQuery) {
        addTagValue(FieldType.TAG_CHOICE_QUERY, choiceQuery);
        return this;
    }

    /**
     * Met a vrai la valeur indexed
     * @return
     */
    public FieldFactory indexed() {
        addTagValue(FieldType.TAG_INDEXED, "true");
        return this;
    }

    /**
     * Met a false la valeur indexed
     * @return
     */
    public FieldFactory notIndexed() {
        addTagValue(FieldType.TAG_INDEXED, "false");
        return this;
    }

    /**
     * Ajout le pattern que doit respecter le champs.
     * Exemple: "A.*" pour un champs commencant par A
     * @param pattern
     * @return
     */
    public FieldFactory pattern(String pattern) {
        addTagValue(FieldType.TAG_PATTERN, pattern);
        return this;
    }

    public FieldFactory subtype(String type) {
        addTagValue(FieldType.TAG_SUBTYPE, type);
        return this;
    }

    public FieldFactory min(String v) {
        addTagValue(FieldType.TAG_MIN, v);
        return this;
    }

    public FieldFactory minQuery(String query) {
        addTagValue(FieldType.TAG_MIN_QUERY, query);
        return this;
    }

    public FieldFactory max(String v) {
        addTagValue(FieldType.TAG_MAX, v);
        return this;
    }

    public FieldFactory maxQuery(String query) {
        addTagValue(FieldType.TAG_MAX_QUERY, query);
        return this;
    }


    public FieldFactory addTagValue(String tag, String value) {
        tagValues.put(tag, value);
        return this;
    }


}
