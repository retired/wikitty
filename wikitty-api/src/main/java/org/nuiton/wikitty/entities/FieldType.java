/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.generator.WikittyTagValue;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FieldType implements WikittyTagValue, Serializable {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(FieldType.class);

    /** serialVersionUID. */
    private static final long serialVersionUID = -4375308750387837026L;

    public static final int NOLIMIT = Integer.MAX_VALUE;

    protected WikittyTypes type;
    protected int lowerBound;
    protected int upperBound;

    /** used to store tag/value used by client side ex: editor=xhtml */
    Map<String, String> tagValues = new HashMap<String, String>();

    public FieldType() {
    }

    public FieldType(WikittyTypes type, int lowerBound, int upperBound) {
        this.type = type;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    public void addTagValue(String tag, String value) {
        tagValues.put(tag, value);
    }

    public String getTagValue(String tag) {
        String result = StringUtils.defaultIfBlank(tagValues.get(tag), "");
        return result;
    }

    public Set<String> getTagNames() {
        return tagValues.keySet();
    }

    public Map<String, String> getTagValues() {
        return tagValues;
    }

    public void setTagValues(Map<String, String> tagValues) {
        this.tagValues = tagValues;
    }

    /**
     * Return true if this field have upperBound &gt; 1.
     * 
     * @return {@code true} is field is collection
     */
    public boolean isCollection() {
        return upperBound > 1;
    }

    /**
     * Return string definition for this field.
     * 
     * @param name field name used for definition
     * @return field definition
     */
    public String toDefinition(String name) {
        String result = type + " " + name;
        if (lowerBound != 0 || upperBound != 0) {
            if (upperBound != NOLIMIT) {
                result += "[" + lowerBound + "-" + upperBound + "]";
            } else {
                result += "[" + lowerBound + "-*]";
            }
        }
        result += WikittyUtil.tagValuesToString(tagValues);
        return result;
    }

    /**
     * Convert value in argument in right type for this FieldType. Don't support
     * collection.
     *
     * @param value value to convert
     * @return object in type of this FieldType
     */
    public Object getContainedValidObject( Object value ) {
        // to permit simple migration from cardinality N to 1 for field
        if (value != null && (value instanceof Collection
                // small exception for binary where primitive type is Array of byte
                || (value.getClass().isArray() && type != WikittyTypes.BINARY))) {
            int size = CollectionUtils.size(value);
            if (size > 0) {
                value = CollectionUtils.get(value, 0);
                if (size > 1) {
                    log.warn("migrate field cardinality N to 1, but collection contains " +
                            size + " values, only first is migrate, all other are loose");
                }
          }
        }

        Object result = null;
        switch (type) {
        case BINARY:
            result = WikittyUtil.toBinary(value); break;
        case DATE:
            result = WikittyUtil.toDate(value); break;
        case NUMERIC:
            result = WikittyUtil.toBigDecimal(value); break;
        case BOOLEAN:
            result = WikittyUtil.toBoolean(value); break;
        case STRING:
            result = WikittyUtil.toString(value); break;
        default:
            // if type is not found then type is business type
            // and is wikitty object
            result = WikittyUtil.toWikitty(value); break;
        }
        return result;
    }

    /**
     * Return a valid value for this field.
     * 
     * @param value is casted if possible to an actual correct value.
     * @return value validity
     * @throws WikittyException if value can't be obtained
     */
    public Object getValidValue(Object value) throws WikittyException {
        if (value == null && hasDefault()) {
            value = getDefault();
        }

        if (value == null && isNotNull()) {
            throw new WikittyException("Value can't be null for this field");
        }

        Object result;
        if (value == null) {
            result = null;
        } else if (isCollection()) {
            if ( !(value instanceof Collection || value.getClass().isArray()) ) {
                // to simplify migration from field with cardinality 1 to N
                // create collection with value
                value = new Object[]{value};
            }
            Collection<Object> col;
            if (isUnique()) {
                col = new LinkedHashSet<Object>();
            } else {
                col = new ArrayList<Object>();
            }

            // copy all value in new collections
            if (value instanceof Collection) {
                for ( Object o : (Collection<?>) value ) {
                    Object tmp = getContainedValidObject(o);
                    if (tmp != null) {
                        col.add( tmp );
                    }
                }
            } else {
                // is Array
                for ( Object o : (Object[]) value ) {
                    Object tmp = getContainedValidObject(o);
                    if (tmp != null) {
                        col.add( tmp );
                    }
                }
            }
            result = col;

            // check bound condition
            if (!(getLowerBound() <= col.size() && col.size() <= getUpperBound())) {
                // if upper bound reached, throw an exception
                throw new WikittyException(String.format(
                        "Can't set collection value, bad size collection: %s <= %s <= %s is false",
                        getLowerBound(), col.size(), getUpperBound()));
            }
        } else {
            result = getContainedValidObject(value);
        }
        return result;
    }

    /**
     * Test if value in argument is valid for this field type.
     * 
     * @param value to test
     * @return true if value is valid
     */
    public boolean isValidValue(Object value) {
        boolean result;
        if (value == null && isNotNull()) {
            result = false;
        } else {
            try {
                getValidValue(value);
                // no exception value is valid
                result = true;
            } catch (Exception eee) {
                if (log.isDebugEnabled()) {
                    log.debug("Can't convert value, value is not valid, return false", eee);
                }
                // exception value is not valid
                result = false;
            }
        }
        return result;
    }

    public WikittyTypes getType() {
        return type;
    }

    public void setType(WikittyTypes type) {
        this.type = type;
    }

    public int getLowerBound() {
        return lowerBound;
    }

    public void setLowerBound(int lowerBound) {
        this.lowerBound = lowerBound;
    }

    public int getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(int upperBound) {
        this.upperBound = upperBound;
    }

    /**
     * @see #TAG_FIELD_INDEX
     */
    public boolean hasFieldIndex() {
        return null != getTagValue(TAG_FIELD_INDEX);
    }

    /**
     * @see #TAG_FIELD_INDEX
     */
    public double getFieldIndex() {
        double result = Double.MAX_VALUE;
        String fieldIndex = getTagValue(TAG_FIELD_INDEX);
        if (fieldIndex != null) {
            try {
                result = Double.parseDouble(fieldIndex);
            } catch (NumberFormatException eee) {
                if (log.isDebugEnabled()) {
                    log.debug(String.format("Bad fieldIndex value '%s'", fieldIndex), eee);
                }
            }
        }
        return result;
    }

    /**
     * @see #TAG_DEFAULT_VALUE
     */
    public boolean hasDefault() {
        return tagValues.containsKey(TAG_DEFAULT_VALUE);
    }

    public Object getDefault() {
        Object result = null;
        
        Object val = tagValues.get(TAG_DEFAULT_VALUE);
        if (val != null) {
            if (isCollection()) {
                val = Collections.singleton(val);
            }
            result = getValidValue(val);
        }

        return result;
    }

    /**
     * @see #TAG_UNIQUE
     */
    public boolean isUnique() {
        return "true".equalsIgnoreCase(getTagValue(TAG_UNIQUE));
    }

    /**
     * @see #TAG_NOT_NULL
     */
    public boolean isNotNull() {
        return "true".equalsIgnoreCase(getTagValue(TAG_NOT_NULL));
    }

    /**
     * @see #TAG_INDEXED
     */
    public boolean isIndexed() {
        String indexed = getTagValue(TAG_INDEXED);
        boolean result =
                !isCrypted() && !StringUtils.equalsIgnoreCase("false", indexed);
        return result;
    }

    /**
     * @see #TAG_PATTERN
     */
    public boolean hasPattern() {
        boolean result = StringUtils.isNotBlank(getTagValue(TAG_PATTERN));
        return result;
    }

    /**
     * @see #TAG_PATTERN
     */
    public String getPattern() {
        String result = getTagValue(TAG_PATTERN);
        return result;
    }

    /**
     * @see #TAG_CRYPT
     */
    public boolean isCrypted() {
        boolean result = StringUtils.isNotBlank(getTagValue(TAG_CRYPT));
        return result;
    }

    /**
     * @see #TAG_CRYPT
     */
    public String getCryptAlgo() {
        String crypt = getTagValue(TAG_CRYPT);
        String result = StringUtils.substringBefore(crypt, ":");
        return result;
    }

    /**
     * @return password or "" if general password must be used
     * @see #TAG_CRYPT
     */
    public String getCryptPassword() {
        String crypt = getTagValue(TAG_CRYPT);
        String result = StringUtils.substringAfter(crypt, ":");
        return result;
    }

    /**
     * @see #TAG_SUBTYPE
     */
    public String getSubtype() {
        String result = getTagValue(TAG_SUBTYPE);
        if (StringUtils.isBlank(result)) {
            switch(type) {
                case DATE:
                    result = "";
                    break;
                case NUMERIC:
                    result = "real";
                    break;
                case STRING:
                    result = "monoline";
                    break;
            }
        }
        return result;
    }
    /**
     * @see #TAG_ALLOWED
     */
    public String getAllowed() {
        String allowed = getTagValue(TAG_ALLOWED);
        return allowed;
    }
    /**
     * @see #TAG_ALLOWED_QUERY
     */
    public String getAllowedQuery() {
        String allowed = getTagValue(TAG_ALLOWED_QUERY);
        return allowed;
    }
    /**
     * @see #TAG_CHOICE
     */
    public String getChoice() {
        String result = getTagValue(TAG_CHOICE);
        return result;
    }
    /**
     * @see #TAG_CHOICE_QUERY
     */
    public String getChoiceQuery() {
        String result = getTagValue(TAG_CHOICE_QUERY);
        return result;
    }
    /**
     * @see #TAG_MIN
     */
    public String getMin() {
        String result = getTagValue(TAG_MIN);
        return result;
    }
    /**
     * @see #TAG_MIN_QUERY
     */
    public String getMinQuery() {
        String result = getTagValue(TAG_MIN_QUERY);
        return result;
    }
    /**
     * @see #TAG_MAX
     */
    public String getMax() {
        String result = getTagValue(TAG_MAX);
        return result;
    }
    /**
     * @see #TAG_MAX_QUERY
     */
    public String getMaxQuery() {
        String result = getTagValue(TAG_MAX_QUERY);
        return result;
    }
    /**
     * @see #TAG_TO_STRING
     */
    public String getToString() {
        String result = getTagValue(TAG_TO_STRING);
        return result;
    }
    /**
     * @return list or null if not spcified allowed extension
     * @see #TAG_ALLOWED
     */
    public List<String> getAllowedAsList() {
        List<String> result = null;
        String allowed = getAllowed();
        if (StringUtils.isNotBlank(allowed)) {
            String[] v = allowed.split("\\s*,\\s*");
            result = Arrays.asList(v);
        }
        return result;
    }

    /**
     * Si les extensions autorisees pour ce champs est contrainte par le tag value
     * {@link #TAG_ALLOWED} alors retourne true, sinon false.
     * @see #TAG_ALLOWED
     */
    public boolean isRestrited() {
        boolean result = hasAllowed() || hasAllowedQuery();
        return result;
    }

    public boolean hasAllowed() {
        boolean result = StringUtils.isNotBlank(getAllowed());
        return result;
    }

    public boolean hasAllowedQuery() {
        boolean result = StringUtils.isNotBlank(getAllowedQuery());
        return result;
    }
    public boolean hasChoice() {
        boolean result = StringUtils.isNotBlank(getChoice());
        return result;
    }

    public boolean hasChoiceQuery() {
        boolean result = StringUtils.isNotBlank(getChoiceQuery());
        return result;
    }
    public boolean hasMin() {
        boolean result = StringUtils.isNotBlank(getMin());
        return result;
    }
    public boolean hasMinQuery() {
        boolean result = StringUtils.isNotBlank(getMinQuery());
        return result;
    }

    public boolean hasMax() {
        boolean result = StringUtils.isNotBlank(getMax());
        return result;
    }
    public boolean hasMaxQuery() {
        boolean result = StringUtils.isNotBlank(getMaxQuery());
        return result;
    }
    public boolean hasToString() {
        boolean result = StringUtils.isNotBlank(getToString());
        return result;
    }
}
