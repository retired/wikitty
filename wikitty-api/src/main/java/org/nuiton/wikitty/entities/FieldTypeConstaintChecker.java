/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.entities;


import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryParser;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 * Cet objets sert a gerer les contraintes qui peuvent exister sur un champs.
 * On ne les implantes pas dans le champs directement, car l'objet champs aurait
 * alors des dependances vers des objets complexe (WikittyClient, WikittyService,
 * Wikitty) chose que l'on ne veut pas.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FieldTypeConstaintChecker {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(FieldTypeConstaintChecker.class);

    protected WikittyService ws;
    protected String token;

    /**
     * Create FieldTypeConstaintChecker usable in client side
     * @param client client used to do request if needed by tag value
     */
    public FieldTypeConstaintChecker(WikittyClient client) {
        this.ws = client.getWikittyService();
        this.token = client.getSecurityToken();
    }

    /**
     * this constructor must be used only be framework in WikittyServiceStorage.
     * @param ws
     */
    public FieldTypeConstaintChecker(WikittyService ws) {
        this.ws = ws;
    }

    /**
     * check all contraints
     * 
     * @param fqfield
     * @param field
     * @param value
     * @param errors can be null
     *
     * @return true if all contraints is satified, otherwize false
     */
    public boolean isValid(String fqfield, FieldType field, Object value, Collection<String> errors) {
        boolean result = true;
        // notnull
        result = result && isNotNull(fqfield, field, value, errors);
        // unique de collection
        result = result && isUnique(fqfield, field, value, errors);
        // pattern
        result = result && checkPattern(fqfield, field, value, errors);
        // min et max pour les date et numeric
        result = result && checkMin(fqfield, field, value, errors);
        result = result && checkMax(fqfield, field, value, errors);
        // allowed
        result = result && isAllowed(fqfield, field, value, errors);
        return result;
    }

    /**
     * Si le champs n'a pas de valeur (null) met en place la valeur par defaut
     * 
     * @param w
     * @param fqfield
     * @param type
     * @param value
     */
    public Object checkDefault(Wikitty w, String fqfield, FieldType type, Object value) {
        Object result = value;
        if (value == null && type.hasDefault()) {
            result = type.getDefault();
            w.setFqField(fqfield, result);
        }
        return result;
    }

    public Object getMin(FieldType field) {
        String min = null;
        if (field.hasMinQuery()) {
            String query = field.getMinQuery();
            WikittyQuery q = WikittyQueryParser.parse(query);
            WikittyQueryResult queryResult = ws.findAllByQuery(token, Collections.singletonList(q)).get(0).convertMapToSimple();
            if (queryResult.size() > 0 && queryResult.peek() != null) {
                min = String.valueOf(queryResult.peek());
            }
        }

        if (min == null && field.hasMin()) {
            min = field.getMin();
        }

        Object result = null;
        if (min != null) {
            result = field.getContainedValidObject(min);
        }
        return result;
    }
    public Date getMinAsDate(FieldType field) {
        Date result = (Date)getMin(field);
        return result;
    }
    public BigDecimal getMinAsBigDecimal(FieldType field) {
        BigDecimal result = (BigDecimal)getMin(field);
        return result;
    }
    public Object getMax(FieldType field) {
        String max = null;
        if (field.hasMaxQuery()) {
            String query = field.getMaxQuery();
            WikittyQuery q = WikittyQueryParser.parse(query);
            WikittyQueryResult queryResult = ws.findAllByQuery(token, Collections.singletonList(q)).get(0).convertMapToSimple();
            if (queryResult.size() > 0 && queryResult.peek() != null) {
                max = String.valueOf(queryResult.peek());
            }
        }

        if (max == null && field.hasMax()) {
            max = field.getMax();
        }

        Object result = null;
        if (max != null) {
            result = field.getContainedValidObject(max);
        }
        return result;
    }
    public Date getMaxAsDate(FieldType field) {
        Date result = (Date)getMax(field);
        return result;
    }
    public BigDecimal getMaxAsBigDecimal(FieldType field) {
        BigDecimal result = (BigDecimal)getMax(field);
        return result;
    }

    public boolean isAllowed(String fqfield, FieldType field, Object value, Collection<String> errors) {
        boolean result = true;
        if (value != null && (field.hasAllowed() || field.hasAllowedQuery())) {
            switch(field.getType()) {
                case STRING:
                    if (field.isCollection()) {
                        result = isAllowedString(fqfield, field, (Collection<String>)value, errors);
                    } else {
                        result = isAllowedString(fqfield, field, Collections.singleton((String)value), errors);
                    }
                    break;
                case WIKITTY:
                    if (field.isCollection()) {
                        result = isAllowedWikitty(fqfield, field, (Collection<String>)value, errors);
                    } else {
                        result = isAllowedWikitty(fqfield, field, Collections.singleton((String)value), errors);
                    }
                    break;
                default:
                    result = true;
            }
        }
        return result;
    }

    /**
     * allowed contient une liste d'extension possible, allowedQuery retourne
     * une liste d'objet possible. Si les deux existes seul allowedQuery est
     * utilisee, allowed dans ce cas a d'autre usage comme la navigation
     * entre entity, ou permettre la creation d'une nouvelle entity pour ce
     * champs suivant une des extensions declaree.
     *
     * @param fqfield
     * @param field
     * @param ids
     * @param errors
     * @return
     */
    protected boolean isAllowedWikitty(String fqfield, FieldType field,
            Collection<String> ids, Collection<String> errors) {
        boolean result = true;
        // par defaut s'il n'y a rien a verifier le resultat est vrai
        if (!CollectionUtils.isEmpty(ids)) {
            // creation de la requete qui ramene tous les ids d'objets autorisé
            WikittyQuery q = null;
            if (field.hasAllowedQuery()) {
                // construire les requetes
                q = WikittyQueryParser.parse(field.getAllowedQuery());

            } else if (field.hasAllowed()) {
                WikittyQueryMaker maker = new WikittyQueryMaker().or();
                for (String extName : field.getAllowedAsList()) {
                    maker.exteq(extName);
                }
                q = maker.end();
            }

            List<String> unallowed = null;
            // si il y a une requete qui contraint ...
            if (q != null) {
                // ... on lui ajoute la liste des ids qu'a le champs comme valeur
                // et le resultat doit etre identique a cette liste.
                // sinon cela veut dire que certain id du champs ne sont pas
                // dans les valeurs acceptables
                WikittyQuery checkQuery = new WikittyQueryMaker().and()
                        .condition(q.getCondition())
                        .containsOne(Element.ID, ids).end()
                        .setOffset(0).setLimit(WikittyQuery.MAX);
                WikittyQueryResult<String> queryResult =
                        ws.<String>findAllByQuery(token, Collections.singletonList(checkQuery)).get(0).convertMapToSimple();
                List<String> idResult = queryResult.getAll();

                result = idResult.size() == ids.size();
                if (!result) {
                    unallowed = ListUtils.removeAll(ids, idResult);
                }
                if (log.isDebugEnabled() && !result) {
                    log.debug(String.format(
                            "For field '%s' allowed contraint not checked for: %s (ids=%s, idResult=%s)(request=%s)",
                            fqfield, unallowed, ids, idResult, queryResult.getQueryString()));
                }

            }


//            List<String> allowed = null;
//            if (q != null) {
//                q.setOffset(0);
//                q.setLimit(WikittyQuery.MAX);
//
//                WikittyQuery wq = new WikittyQueryMaker().containsOne(Element.ID, ids).end();
//                wq.setOffset(0);
//                wq.setLimit(WikittyQuery.MAX);
//
//                List<WikittyQueryResult<String>> queryResult =
//                        ws.findAllByQuery(token, Arrays.asList(q, wq));
//                allowed = queryResult.get(0).getAll();
//                List<String> findedIds = queryResult.get(1).getAll();
//
//                // on ne check que sur les ids trouve dans la base et on espere
//                // que les autres sont bon :). En fait, lorsqu'on store plusieurs
//                // wikitties en meme temps donc un wikitty qui doit etre la valeur
//                // d'un champs d'un autre. Celui ci n'est pas retrouvable dans la
//                // base et donc on ne peut pas verifier facilement la contrainte.
//                // Pour l'instant on ne fait rien, car les solutions seraient trop
//                // couteuse (creeer un storage in memory y mettre les objets a
//                // checker, faire le check en requetant le vrai repo et le in memory
//                // si tout va bien faire le store reel sur le vrai repo.
//                result = allowed.containsAll(findedIds);
//                if (log.isDebugEnabled() && findedIds.size() != ids.size()) {
//                    log.debug(String.format(
//                            "For field '%s' allowed contraint not checked for: %s",
//                            fqfield, CollectionUtils.disjunction(findedIds, ids)));
//                }
//            }

            if (errors != null && !result) {
                errors.add(String.format(
                        "Field '%s' contains unallowed Wikitty values %s",
                        fqfield, unallowed));
            }

            
            // FIXME poussin 20120406: on ne peut pas garantir cette contrainte
            // actuellement car l'objet qui est mis dan le champs a checker peut
            // aussi etre un objet en cours de stockage. Donc pour l'instant on ne
            // leve pas d'erreur, mais on mes un warn dans les logs.
            //
            // Une solution serait de creeer un storage in memory y mettre les
            // objets a checker, faire le check en requetant le vrai repo et sur
            // les ids en erreur refaire la meme requete sur le in memory
            if (!result) {
                result = true;
                if (log.isDebugEnabled()) {
                    log.debug(String.format(
                            "Field '%s' contains unallowed Wikitty values %s",
                            fqfield, unallowed));
                }
            }
            // FIN du FIXME
        }

        return result;
    }

    protected boolean isAllowedString(String fqfield, FieldType field,
            Collection<String> values, Collection<String> errors) {
        boolean result = false;

        List<String> allowed = null;
        // on commence par ce qui ne demande pas a faire une requete
        if (field.hasAllowed()) {
            allowed = field.getAllowedAsList();
            result = allowed.containsAll(values);
        }

        if (!result && field.hasAllowedQuery()) {
            // construire les requetes
            String query = field.getAllowedQuery();
            WikittyQuery q = WikittyQueryParser.parse(query);
            q.setOffset(0).setLimit(WikittyQuery.MAX);
            WikittyQueryResult<String> queryResult =
                    ws.findAllByQuery(token, Collections.singletonList(q)).get(0).convertMapToSimple();
            allowed = queryResult.getAll();
            result = allowed.containsAll(values);
        }

        if (errors != null && !result) {
            errors.add(String.format(
                    "Field '%s' contains unallowed string values: %s allowed: %s",
                    fqfield, values, allowed));
        }
        return result;
    }

    public boolean checkPattern(String fqfield, FieldType field, Object value, Collection<String> errors) {
        boolean result = true;
        if (value != null && field.hasPattern() && field.getType() == WikittyTypes.STRING) {
            String pattern = field.getPattern();
            result = Pattern.matches(pattern, (String)value);
            if (errors != null && !result) {
                errors.add(String.format(
                        "Field '%s' must match pattern '%s' but value is '%s'",
                        fqfield, pattern, value));
            }
        }
        return result;
    }

    public boolean isNotNull(String fqfield, FieldType field, Object value, Collection<String> errors) {
        boolean result = true;
        if (field.isNotNull()) {
            result = value != null;
        }

        if (errors != null && !result) {
            errors.add(String.format("Field '%s' contains null value", fqfield));
        }
        return result;
    }

    /**
     * Vrai si la collection ne contient pas de doublon.
     * @param field
     * @param value
     * @return
     */
    public boolean isUnique(String fqfield, FieldType field, Object value, Collection<String> errors) {
        boolean result = true;
        if (field.isUnique() && value != null && field.isCollection() && !(value instanceof Set)) {
            Collection c = (Collection)value;
            Set s = new HashSet(c);
            result = s.size() == c.size();
        }
        
        if (errors != null && !result) {
            errors.add(String.format("Field '%s' contains duplicate value", fqfield));
        }
        return result;
    }

    public boolean checkMin(String fqfield, FieldType field, Object value, Collection<String> errors) {
        boolean result = true;
        if (value != null && (field.hasMin() || field.hasMinQuery())) {
            switch(field.getType()) {
                case DATE: {
                    if (field.isCollection()) {
                        result = checkMinDate(fqfield, field,
                                (Collection<Date>)value, errors);
                    } else {
                        result = checkMinDate(fqfield, field,
                                Collections.singleton((Date)value), errors);
                    }
                }
                break;
                case NUMERIC: {
                    if (field.isCollection()) {
                        result = checkMinBigDecimal(fqfield, field,
                                (Collection<BigDecimal>)value, errors);
                    } else {
                        result = checkMinBigDecimal(fqfield, field,
                                Collections.singleton((BigDecimal)value), errors);
                    }
                }
                break;
                default:
                    result = true;
            }
        }
        return result;
    }

    protected boolean checkMinDate(String fqfield, FieldType field,
            Collection<Date> values, Collection<String> errors) {
        boolean result = true;
        Date min = getMinAsDate(field);
        for (Date b : values) {
            result = min.compareTo(b) <= 0;
            if (!result) {
                break;
            }
        }
        if (errors != null && !result) {
            errors.add(String.format(
                    "Field '%s' contains value higher then '%s': %s",
                    fqfield, min, values));
        }
        return result;
    }

    protected boolean checkMinBigDecimal(String fqfield, FieldType field,
            Collection<BigDecimal> values, Collection<String> errors) {
        boolean result = true;
        BigDecimal min = getMinAsBigDecimal(field);
        for (BigDecimal b : values) {
            result = min.compareTo(b) <= 0;
            if (!result) {
                break;
            }
        }
        if (errors != null && !result) {
            errors.add(String.format(
                    "Field '%s' contains value higher then '%s': %s",
                    fqfield, min, values));
        }
        return result;
    }

    public boolean checkMax(String fqfield, FieldType field, Object value, Collection<String> errors) {
        boolean result = true;
        if (value != null && (field.hasMax() || field.hasMaxQuery())) {
            switch(field.getType()) {
                case DATE: {
                    if (field.isCollection()) {
                        result = checkMaxDate(fqfield, field,
                                (Collection<Date>)value, errors);
                    } else {
                        result = checkMaxDate(fqfield, field,
                                Collections.singleton((Date)value), errors);
                    }
                }
                break;
                case NUMERIC: {
                    if (field.isCollection()) {
                        result = checkMaxBigDecimal(fqfield, field,
                                (Collection<BigDecimal>)value, errors);
                    } else {
                        result = checkMaxBigDecimal(fqfield, field,
                                Collections.singleton((BigDecimal)value), errors);
                    }
                }
                break;
                default:
                    result = true;
            }
        }
        return result;
    }

    protected boolean checkMaxDate(String fqfield, FieldType field,
            Collection<Date> values, Collection<String> errors) {
        boolean result = true;
        Date max = getMaxAsDate(field);
        for (Date b : values) {
            result = max.compareTo(b) >= 0;
            if (!result) {
                break;
            }
        }
        if (errors != null && !result) {
            errors.add(String.format(
                    "Field '%s' contains value lower then '%s': %s",
                    fqfield, max, values));
        }
        return result;
    }

    protected boolean checkMaxBigDecimal(String fqfield, FieldType field,
            Collection<BigDecimal> values, Collection<String> errors) {
        boolean result = true;
        BigDecimal max = getMaxAsBigDecimal(field);
        for (BigDecimal b : values) {
            result = max.compareTo(b) >= 0;
            if (!result) {
                break;
            }
        }
        if (errors != null && !result) {
            errors.add(String.format(
                    "Field '%s' contains value lower then '%s': %s",
                    fqfield, max, values));
        }
        return result;
    }

}
