/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.entities;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Collection;

/**
 * Used as parent interface to all Business class.
 * 
 * getWikitty and setWikitty is not in interface because this objet for some
 * implementation (GWT) is to complexe
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface BusinessEntity extends Serializable, Cloneable {

    /**
     * Return wikitty id (uuid).
     * 
     * @return wikitty id
     */
    public String getWikittyId();

    /**
     * Return wikitty version (x.y).
     * 
     * @return wikitty version
     */
    public String getWikittyVersion();

    /**
     * Only framework can use this method.
     * 
     * @param version version to set
     */
    public void setWikittyVersion(String version);

    /**
     * Return list of all extension name that this entity has by definition
     * (design time) and not extension added during execution time
     * @return static extension name
     * @since 3.10
     */
    public Collection<String> getStaticExtensionNames();

    /**
     * Return list of all extension for this object.
     * 
     * @return extension names
     */
    public Collection<String> getExtensionNames();
    
    /**
     * Return list of all field for this object.
     * 
     * @param ext extension to get fields
     * @return fields collections
     */
    public Collection<String> getExtensionFields(String ext);

    /**
     * Return field value for the specific extension and field.
     *
     * @param ext extension
     * @param fieldName field name
     * @return field value
     */
    public Object getFieldAsObject(String ext, String fieldName);

    /**
     * Return field value for the specific extension and field.
     *
     * @param ext extension
     * @param fieldName field name
     * @return field value
     * @deprecated use getFieldAsObject
     */
    @Deprecated
    public Object getField(String ext, String fieldName);

    /**
     * Set field value for the specific extension and field.
     *
     * This method don't check the type of value for this field.
     *
     * @param ext extension
     * @param fieldName field name
     * @param value value to set
     */
    public void setField(String ext, String fieldName, Object value);

    /**
     * Copy all field version included from source to current bean (only
     * id is not copied, but must be the same)
     * @param source
     */
    public void copyFrom(BusinessEntity source);

    /**
     * Return the field type.
     * 
     * @param ext extension
     * @param fieldName field name
     * @return feild type
     */
    //public FieldType getFieldType(String ext, String fieldName);
    
    /**
     * Add {@link PropertyChangeListener}.
     *
     * @param listener listener to add
     */
    void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Remove {@link PropertyChangeListener}.
     *
     * @param listener listener to remove
     */
    void removePropertyChangeListener(PropertyChangeListener listener);

    /**
     * Add {@link PropertyChangeListener} on {@code propertyName}.
     *
     * @param propertyName property name
     * @param listener listener to add
     */
    void addPropertyChangeListener(String propertyName, PropertyChangeListener listener);

    /**
     * Remove {@link PropertyChangeListener} on {@code propertyName}
     *
     * @param propertyName property name
     * @param listener listener listener to remove
     */
    void removePropertyChangeListener(String propertyName, PropertyChangeListener listener);

}
