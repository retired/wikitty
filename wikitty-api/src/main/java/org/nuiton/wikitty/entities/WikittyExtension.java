/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.entities;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.generator.WikittyTagValue;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyExtension implements Serializable {

    /** serialVersionUID. */
    private static final long serialVersionUID = -3598621577607442972L;

    /** Field name pattern only word character [a-zA-Z_0-9] is accepted */
    static protected Pattern NamePattern = Pattern.compile("^\\w+$");

    /** Field name pattern only word character [a-zA-Z_0-9] is accepted */
    static protected Pattern fieldNamePattern = Pattern.compile("^\\w+$");

    /**
     * Property change support.
     * 
     * Warning, this transient field is null after deserialization.
     */
    protected transient PropertyChangeSupport propertyChangeSupport;

    /** Name of this extension. */
    protected String name;

    /**
     * Name of others extensions needed to put this extension to object.
     */
    protected List<String> requires;

    /**
     * use to know version objet, when you change field number, type or other
     * you must change version number.
     */
    protected String version = WikittyUtil.DEFAULT_VERSION;

    /** used to store tag/value used by client side ex: updatedDate=101212 */
    protected Map<String, String> tagValues = new HashMap<String, String>();

    /**
     * fields use ordered map, to keep order insertion of field
     * key: field name
     * value: field type
     */
    protected LinkedHashMap<String, FieldType> fields = new LinkedHashMap<String, FieldType>();

    /**
     * Default constructor.
     * 
     * Used by hibernate.
     */
    public WikittyExtension() {
        
    }

    public WikittyExtension(String name) {
        setName(name);
    }

    public WikittyExtension(String name, String version,
            LinkedHashMap<String, FieldType> fields) {
        this(name, version, (Map)null, (List)null, fields);
    }

    public WikittyExtension(String name, String version, Map<String, String> tagValues,
            String requires, LinkedHashMap<String, FieldType> fields) {
        this(name, version, tagValues, StringUtils.split(requires, ","), fields);
    }

    public WikittyExtension(String name, String version, Map<String, String> tagValues,
            String[] requires, LinkedHashMap<String, FieldType> fields) {
        this(name, version, tagValues,
                ArrayUtils.isEmpty(requires)?(List)null:Arrays.asList(requires),
                fields);
    }

    public WikittyExtension(String name, String version, Map<String, String> tagValues,
            List<String> requires, LinkedHashMap<String, FieldType> fields) {
        if (version == null) {
            throw new IllegalArgumentException("Version must not be null");
        }
        setName(name);
        this.version = WikittyUtil.normalizeVersion(version);
        setRequires(requires);
        setTagValues(tagValues);
        if (fields != null) {
            addField(fields);
        }
    }

    protected PropertyChangeSupport getPropertyChangeSupport() {
        if (propertyChangeSupport == null) {
            propertyChangeSupport = new PropertyChangeSupport(this);
        }
        return propertyChangeSupport;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        getPropertyChangeSupport().addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        getPropertyChangeSupport().removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
            PropertyChangeListener listener) {
        getPropertyChangeSupport().addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(String propertyName,
            PropertyChangeListener listener) {
        getPropertyChangeSupport().removePropertyChangeListener(propertyName, listener);
    }

    public WikittyExtension cloneForUpgrade() {
        String nextRevision = WikittyUtil.incrementMajorRevision(getVersion());
        
        LinkedHashMap<String, FieldType> nextFields = null;
        if (fields != null) {
            nextFields = new LinkedHashMap<String, FieldType>();
            for (Map.Entry<String, FieldType> entry : fields.entrySet()) {
                FieldType type = entry.getValue();
                FieldType nextType = new FieldType(
                        type.getType(), type.getLowerBound(), type.getUpperBound());
                Set<String> tagNames = type.getTagNames();
                if (tagNames != null) {
                    for (String tagName : tagNames) {
                        String tagValue = type.getTagValue(tagName);
                        nextType.addTagValue(tagName, tagValue);
                    }
                }
                nextFields.put(entry.getKey(), nextType);
            }
        }

        WikittyExtension result = new WikittyExtension(
                name, nextRevision, tagValues, requires, nextFields);
        return result;
    }

    /**
     * Compute id for extension name and version in argument.
     * 
     * @param name extension name
     * @param version extension version
     * @return extension string id
     */
    static public String computeId(String name, String version) {
        String result = name + "[" + version + "]";
        return result;
    }

    /**
     * Extract name from extension id
     *
     * @param id id like MonExtension[3.0]
     * @return extension name. Example 'MonExtension'
     */
    static public String computeName(String id) {
        int i = id.lastIndexOf("[");
        String result = id;
        if (i != -1) {
            result = id.substring(0, i);
        }
        return result;
    }

    /**
     * Extract extension version from extension id. If id contains no version
     * this method return '0.0'.
     *
     * @param id id like MonExtension[3.0]
     * @return extension version. Example '3.0'
     */
    static public String computeVersion(String id) {
        int b = id.lastIndexOf("[");
        int e = id.lastIndexOf("]");
        String result = null;
        if (b != -1 && e != -1) {
            result = id.substring(b+1, e);
        }
        result = WikittyUtil.normalizeVersion(result);
        return result;
    }

    static public boolean isFqField(String fieldName) {
        int i = fieldName.indexOf(WikittyUtil.FQ_FIELD_NAME_SEPARATOR);
        boolean result = i > 0;
        return result;
    }

    /**
     * Extract extension name from fully qualified field name
     *
     * @param fqFieldName fully qualified field name like 'WikittyUser.login'
     * @return return extension name. Example 'WikittyUser'
     * @throws org.nuiton.wikitty.WikittyException if bad fqFieldName format
     */
    static public String extractExtensionName(String fqFieldName) {
        int i = fqFieldName.indexOf(WikittyUtil.FQ_FIELD_NAME_SEPARATOR);
        if (i > 0) {
            String result = fqFieldName.substring(0, i);
            return result;
        } else {
            throw new IllegalArgumentException(String.format(
                    "Your argument '%s' is not fully qualified field name", fqFieldName));
        }
    }

    /**
     * Extract field name from fully qualified field name (suppression [n/m] if
     * field is collection) and suppress TYPE if needed
     *
     * @param fqFieldName fully qualified field name like 'WikittyUser.login'
     * @return return field name. Example 'login'
     */
    static public String extractFieldName(String fqFieldName) {
        int i = fqFieldName.indexOf(WikittyUtil.FQ_FIELD_NAME_SEPARATOR);
        if (i > 0) {
            String result = fqFieldName.substring(i+1);
            int b = result.lastIndexOf("[");
            if (b > 0) {
                result = result.substring(0, b);
            }

            return result;
        } else {
            throw new IllegalArgumentException(String.format(
                    "Your argument '%s' is not fully qualified field name", fqFieldName));
        }
    }

    public String getId() {
        String result = computeId(getName(), getVersion());
        return result;
    }

    public String getName() {
        return name;
    }

    /**
     * Set extension name.
     * 
     * Check for invalid extension name (non alphanumeric characters).
     * 
     * @param name name
     */
    public void setName(String name) {

        // check alphanumeric characters
        if (name == null) {
            throw new IllegalArgumentException("Name must not be null");
        }
        if (!NamePattern.matcher(name).matches()) {
            throw new IllegalArgumentException("Name contains non alphanumeric characters");
        }

        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<String> getRequires() {
        return requires;
    }

    public void setRequires(List<String> requires) {
        if (requires == null) {
            requires = Collections.EMPTY_LIST;
        }
        this.requires = Collections.unmodifiableList(new ArrayList<String>(requires));
        getPropertyChangeSupport().firePropertyChange("requires", null, this.requires);
    }
    
    public FieldType getFieldType(String fieldName) {
        return fields.get(fieldName);
    }

    public Collection<String> getFieldNames() {
        Collection<String> result = fields.keySet();
        return result;
    }

    /**
     * Cette methode ne doit-etre utilise que par des methodes qui tri par la
     * suite la Map de champs, car celle-ci ne le fait pas
     * @param newFields
     */
    protected void addField(Map<String, FieldType> newFields) {
        // check fieldname and fieldType
        for (Map.Entry<String, FieldType> e : newFields.entrySet()) {
            if (null == e.getValue()) {
                throw new IllegalArgumentException(
                        "FieldType must not be null for field '" + e.getKey() +"'");
            }
            Matcher matcher = fieldNamePattern.matcher(e.getKey());
            if(!matcher.find()) {
                throw new IllegalArgumentException(
                        "For field name [" + e.getKey() +"], only word character [a-zA-Z_0-9] is accepted");
            }
        }

        fields.putAll(newFields);
        // sort field on TAG_FIELD_INDEX after add
        fields = sortField(fields);
        
        getPropertyChangeSupport().firePropertyChange("fields", null, fields);
    }

    /**
     * Ajoute un nouveau champs a l'extension, la liste des champs est automatiquement
     * trie après chaque insertion.
     * @param fieldName le nom du champs
     * @param type le type du champs contenant un tag TAG_FIELD_INDEX pour le tri
     */
    public void addField(String fieldName, FieldType type) {
        if (null == type) {
            throw new IllegalArgumentException(
                    "FieldType must not be null for field '" + fieldName +"'");
        }
        Matcher matcher = fieldNamePattern.matcher(fieldName);
        if(matcher.find()) {
            fields.put(fieldName, type);
            // sort field on TAG_FIELD_INDEX after add
            fields = sortField(fields);
            // TODO EC20100610 null for old value
            getPropertyChangeSupport().firePropertyChange("fields", null, fields);
        } else {
            throw new IllegalArgumentException("For field name [" + fieldName +"], only word character [a-zA-Z_0-9] is accepted");
        }
    }

    /**
     * Tri la map en entree suivant la valeur de TAG_FIELD_INDEX de la valeur
     */
    protected LinkedHashMap<String, FieldType> sortField(Map<String, FieldType> map) {
        List<Map.Entry<String, FieldType>> list =
                new ArrayList<Map.Entry<String, FieldType>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, FieldType>>() {
            public int compare(Map.Entry<String, FieldType> o1, Map.Entry<String, FieldType> o2) {
                Double f1 = o1.getValue().getFieldIndex();
                Double f2 = o2.getValue().getFieldIndex();
                int result = f1.compareTo(f2);
                return result;
            }
        });

        LinkedHashMap<String, FieldType> result = new LinkedHashMap<String, FieldType>();
        for (Map.Entry<String, FieldType> e : list) {
            result.put(e.getKey(), e.getValue());
        }
        return result;
    }

    public void removeField(String fieldName) {
        fields.remove(fieldName);
        // TODO EC20100610 null for old value
        getPropertyChangeSupport().firePropertyChange("fields", null, fields);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    public void addTagValue(String tag, String value) {
        tagValues.put(tag, value);
        // TODO EC20100610 null for old value
        getPropertyChangeSupport().firePropertyChange("tagValues", null, tagValues);
    }

    public String getTagValue(String tag) {
        String result = tagValues.get(tag);
        return result;
    }

    public Set<String> getTagNames() {
        return tagValues.keySet();
    }

    public Map<String, String> getTagValues() {
        return tagValues;
    }

    /**
     * Set all tag values. If argument is null, new empty map is created.
     * 
     */
    public void setTagValues(Map<String, String> tagValues) {
        if (tagValues == null) {
            tagValues = new HashMap<String, String>();
        }
        Map<String, String> oldValue = this.tagValues;
        this.tagValues = tagValues;
        getPropertyChangeSupport().firePropertyChange("tagValues", oldValue, tagValues);
    }
    
    public boolean hasTagValueToString() {
        boolean result = StringUtils.isNotBlank(getTagValueToString());
        return result;
    }
    
    public String getTagValueToString() {
        String result = getTagValue(WikittyTagValue.TAG_TO_STRING);
        return result;
    }

    public boolean hasTagValueSortOrder() {
        boolean result = StringUtils.isNotBlank(getTagValueSortOrder());
        return result;
    }
    
    public String getTagValueSortOrder() {
        String result = getTagValue(WikittyTagValue.TAG_SORT_ORDER);
        return result;
    }

    /**
     * Retourne la liste des champs a trier de facon ascendante
     * @return une liste contenant les champs de tri ou une liste vide
     */
    public List<Element> getSortAscending() {
        List<Element> result = new ArrayList<Element>();
        String sortOrder = getTagValueSortOrder();
        String[] orders = StringUtils.split(sortOrder, ",");
        if (orders != null) {
            for (String order : orders) {
                String s = StringUtils.substringAfterLast(order, " ");
                s = StringUtils.trim(s);
                if (StringUtils.isBlank(s) || StringUtils.equalsIgnoreCase("asc", s)) {
                    String field = StringUtils.substringBefore(order, " ");
                    field = StringUtils.trim(field);
                    result.add(Element.get(field));
                }
            }
        }
        return result;
    }

    /**
     * Retourne la liste des champs a trier de facon descendante
     * @return une liste contenant les champs de tri ou une liste vide
     */
    public List<Element> getSortDescending() {
        List<Element> result = new ArrayList<Element>();
        String sortOrder = getTagValueSortOrder();
        String[] orders = StringUtils.split(sortOrder, ",");
        if (orders != null) {
            for (String order : orders) {
                String s = StringUtils.substringAfterLast(order, " ");
                s = StringUtils.trim(s);
                if (StringUtils.equalsIgnoreCase("desc", s)) {
                    String field = StringUtils.substringBefore(order, " ");
                    field = StringUtils.trim(field);
                    result.add(Element.get(field));
                }
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof WikittyExtension) {
            WikittyExtension other = (WikittyExtension)obj;
            result = this.getId().equals(other.getId());
        }
        return result;
    }

    @Override
    public String toString() {
        return getId();
    }
    
    public String toDefinition() {
        String result = "Extension " + getId();
        result += WikittyUtil.tagValuesToString(tagValues);
        result += " {\n";
        for (String fieldName : getFieldNames()) {
            result += fields.get(fieldName).toDefinition(fieldName) + "\n";
        }
        result += "}";
        return result;
    }

    /**
     * Return toString representation. Use tagValue 'toString' format if exist,
     * else standard Wikitty.toString is call
     * 
     * @param wikitty
     * @return
     */
    public String toString(Wikitty wikitty) {
        String result;
        String format = getTagValue(WikittyTagValue.TAG_TO_STRING);
        if (StringUtils.isNotBlank(format)) {
            result = WikittyUtil.format(format, wikitty);
        } else {
            result = wikitty.toStringAllField();
        }
        return result;
    }
}
