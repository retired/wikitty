/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.entities;

/**
 * Represente un element pour les recherches sur les arbres. Vous devez utiliser
 * <ul>
 * <li> {@link ElementNode#FIELD_NODE_ROOT} utilise pour creer une condition sur le noeud root de l'arbre</li>
 * <li> {@link ElementNode#FIELD_NODE_PATH} utilise pour creer une condition sur un noeud ou un de ses peres</li>
 * <li> {@link ElementNode#FIELD_NODE_DEPTH} utilise pour creer une condition sur la profondeur d'un noeud (root=1)</li>
 * </ul>
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ElementNode extends Element {

    private static final long serialVersionUID = 1L;

    public static final String FIELD_NODE_ROOT = "rootNode";
    public static final String FIELD_NODE_PATH = "pathNode";
    public static final String FIELD_NODE_DEPTH = "depthNode";


    /** protected because you must used constant defined in {@link Element} */
    protected ElementNode(String v) {
        super(v);
    }
}
