/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.entities;

import org.nuiton.wikitty.entities.WikittyTypes;

/**
 * Element qui porte sur un champs. Le champs doit-ete de la forme
 * extname.fieldname. Mais extname ou fieldname ou les deux peuvent etre
 * remplace par des *. Au lieu de faire '*.*' il est preferable d'utiliser
 * {@link #ALL_FIELD}
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ElementField extends Element {

    private static final long serialVersionUID = 1L;

    final static public String ALL_EXTENSION = "*";

    /**
     * Le parametre doit etre completement qualifier c-a-d: extName.fieldName.
     * ex: User.firstName.
     * @param fqfield
     */
    public ElementField(String fqfield) {
        super(fqfield);
    }

    /**
     * Ce constructeur permet de creer un champs, si a la place de extensionName
     * vous passez {@link #ALL_EXTENSION} le champs represente tous les champs
     * partant se nom sur toutes les extensions
     * @param extensionName
     * @param fieldName
     */
    public ElementField(String extensionName, String fieldName) {
        super(extensionName + FQ_FIELD_NAME_SEPARATOR + fieldName);
    }

    /**
     * Cree un champs sur toutes les extensions ex: "*.name" en specifiant le
     * type du champs ex: "*.birthday.DATE"
     *
     * @param fieldName le champs sans l'extension
     * @param type le type que le champs doit avoir
     */
    public ElementField(String fieldName, WikittyTypes type) {
        // le separateur entre le type et le champs est le meme que entre
        // l'extension et le champs, on utilise la meme methode
        super(ALL_EXTENSION + FQ_FIELD_NAME_SEPARATOR + fieldName + FQ_FIELD_NAME_SEPARATOR + type.name());
    }


}
