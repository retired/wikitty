/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.entities;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.ObjectUtil;
import org.nuiton.util.VersionUtil;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.generator.WikittyTagValue;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.math.BigDecimal;
import java.util.AbstractList;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Wikitty implementation.
 *
 * {@link #getDirty()} contains 2 types of info :
 * <ul>
 * <li>the name of a modified field (extension.field)</li>
 * <li>an extension added or deleted (extension)</li>
 * </ul>
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyImpl implements Wikitty {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyImpl.class);

    /** serialVersionUID. */
    private static final long serialVersionUID = 4910886672760691052L;

    /** Technical id for this wikitty object. id must be never null. */
    protected String id;

    /** Current version of this wikitty object. */
    protected String version = WikittyUtil.DEFAULT_VERSION;

    /** If not null, date of deletion, if date this object is marked as deleted. */
    protected Date deleteDate = null;

    /**
     * Used to add property change support to wikitty object.
     * 
     * Warning, this field can be null after deserialization.
     */
    private transient PropertyChangeSupport propertyChange;

    /**
     * key: field name prefixed by extension name (dot separator)
     * value: value of field
     */
    protected HashMap<String, Object> fieldValue = new HashMap<String, Object>();

    /**
     * all field name currently modified (field name = extension . fieldname)
     */
    protected Set<String> fieldDirty = new HashSet<String>();

    /**
     * Map is LinkedHashMap to maintains order like user want
     * key: extension name
     * value: extension definition
     */
    protected Map<String, WikittyExtension> extensions =
            new LinkedHashMap<String, WikittyExtension>();
    
    /**
     * Pattern de prechargement demande par l'utilisateur lors de la restauration
     * du wikitty. Peut-etre null si aucun prechargement n'est demande.
     */
    protected String preloadPattern;

    /**
     * Ensemble des liens vers des Wikitty precharge. Cette map peut-etre null
     * si aucun prechargement n'existe.
     * <ul>
     * <li> key: wikitty id</li>
     * <li> value: wikitty</li>
     * </ul>
     */
    // poussin 20120305 peut-etre mettre transient le champs ? a voir a l'usage
    protected Map<String, Wikitty> preloaded;

    public WikittyImpl() {
        this(null);
    }

    public WikittyImpl(String id) {
        if (id == null) {
            this.id = WikittyUtil.genUID();
        } else {
            this.id = id;
        }
    }

    @Override
    public String getPreloadPattern() {
        return preloadPattern;
    }

    @Override
    public void setPreloadPattern(String preloadPattern) {
        this.preloadPattern = preloadPattern;
    }

    @Override
    public Set<String> getAllPreloadPattern() {
        Set<String> result = new HashSet<String>();
        String[] preloadPatterns = StringUtils.split(getPreloadPattern(), ";");
        if (preloadPatterns != null) {
            result.addAll(Arrays.asList(preloadPatterns));
        }

        for (WikittyExtension ext : getExtensions()) {
            preloadPatterns = StringUtils.split(
                    ext.getTagValue(WikittyTagValue.TAG_PRELOAD), ";");
            if (preloadPatterns != null) {
                result.addAll(Arrays.asList(preloadPatterns));
            }
        }
        result.remove(null);
        return result;
    }

    @Override
    public Map<String, Wikitty> getPreloaded() {
        if (preloaded == null) {
            preloaded = new HashMap<String, Wikitty>();
        }
        return Collections.unmodifiableMap(preloaded);
    }

    @Override
    public void setPreloaded(Map<String, Wikitty> preloaded) {
        // on fait une copie et on la rend non modifiable.
        Map<String, Wikitty> map = new HashMap<String, Wikitty>();
        if (preloaded != null) {
            map.putAll(preloaded);
        }
        this.preloaded = map;
    }

    @Override
    public void addPreloaded(Wikitty w) {
        if (w != null) {
            if (preloaded == null) {
                preloaded = new HashMap<String, Wikitty>();
            }
            this.preloaded.put(w.getWikittyId(), w);
        }
    }

    /**
     * Replace all field of current wikitty with field found in w.
     * This two wikitty must have same id.
     *
     * This method is used to clone Wikitty too
     * 
     * @param w wikitty where we take information
     */
    @Override
    public void replaceWith(Wikitty w) {
        replaceWith(w, false);
    }

    /**
     * Replace all field of current wikitty with field found in w.
     * This two wikitty must have same id if force is false.
     *
     * This method is used to clone Wikitty too
     *
     * @param w wikitty where we take information
     */
    @Override
    public void replaceWith(Wikitty w, boolean force) {
        // il faut que ce soit le meme objet mais pas la meme instance
        // car ca ne sert a rien de copier un objet sur lui meme
        if (this == w) {
            return;
        }
        // all time search for WikittyImpl to do copy (prevent error)
        if (w instanceof WikittyCopyOnWrite) {
            replaceWith(((WikittyCopyOnWrite)w).getTarget(), force);
        } else if (force || this.getWikittyId().equals(w.getWikittyId())) {
            // poussin 20120305 quid des listeners ? ne faudrait-il pas les copier aussi ?
            // mais si on les copie, comment l'utilisateur sera-t-il qu'il faut les supprimer
            // ne risque t-il pas d'y avoir des listeners recevant trop de notification.
            // le plus simple est donc pour l'instant de ne pas copier les listeners, a la
            // charge du developpeur d'appli, lorsqu'il fait un clone de remettre les listeners
            // qu'il souhaite

            // copy preload (not in deep copy mode)
            this.setPreloaded(w.getPreloaded());
            this.setPreloadPattern(w.getPreloadPattern());

            this.extensions.clear();
            this.fieldValue.clear();
            for (WikittyExtension ext : w.getExtensions()) {
                String extName = ext.getName();
                this.extensions.put(extName, ext);
                for(String fieldName : ext.getFieldNames()) {
                    Object value = w.getFieldAsObject(extName, fieldName);
                    if (value != null) {
                        // seul les collections sont a cloner, sinon ce ne sont
                        // que des types primitifs immutable
                        if (value instanceof Collection && value instanceof Cloneable) {
                            try {
                                value = ObjectUtil.clone((Cloneable) value);
                            } catch (CloneNotSupportedException eee) {
                                throw new WikittyException(String.format(
                                        "Can't copy field %s.%s",
                                        extName, fieldName), eee);
                            }
                        }
                        this.setField(extName, fieldName, value);
                    }
                }
            }

            // Dont keep version if force
            if (!force) {
                // we must change version after field value copy, because
                // copy increment version because it use setField method
                this.version = w.getWikittyVersion();
                this.deleteDate = w.getDeleteDate();
            }

            clearDirty();
        } else {
            throw new IllegalArgumentException("Wikitty in argument don't have same id");
        }
    }

    /**
     * Always call this method because field is transient.
     * 
     * @return
     */
    protected PropertyChangeSupport getPropertyChangeSupport() {
        if (propertyChange == null) {
            propertyChange = new PropertyChangeSupport(this);
        }
        return propertyChange;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#addPropertyChangeListener(java.beans.PropertyChangeListener)
     */
    @Override
    public synchronized void addPropertyChangeListener(
            PropertyChangeListener listener) {
        getPropertyChangeSupport().addPropertyChangeListener(listener);
    }


    /*
     * @see org.nuiton.wikitty.Wikitty#removePropertyChangeListener(java.beans.PropertyChangeListener)
     */
    @Override
    public synchronized void removePropertyChangeListener(
            PropertyChangeListener listener) {
        getPropertyChangeSupport().removePropertyChangeListener(listener);
    }


    /*
     * @see org.nuiton.wikitty.Wikitty#addPropertyChangeListener(java.lang.String, java.beans.PropertyChangeListener)
     */
    @Override
    public synchronized void addPropertyChangeListener(String propertyName,
            PropertyChangeListener listener) {
        getPropertyChangeSupport().addPropertyChangeListener(propertyName, listener);
    }


    /*
     * @see org.nuiton.wikitty.Wikitty#removePropertyChangeListener(java.lang.String, java.beans.PropertyChangeListener)
     */
    @Override
    public synchronized void removePropertyChangeListener(String propertyName,
            PropertyChangeListener listener) {
        getPropertyChangeSupport().removePropertyChangeListener(propertyName, listener);
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getWikittyId()
     */
    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getWikittyId() {
        return id;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#isDeleted()
     */
    @Override
    public boolean isDeleted() {
        boolean result = deleteDate != null;
        return result;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getDeleteDate()
     */
    @Override
    public Date getDeleteDate() {
        return deleteDate;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#setDeleteDate(java.util.Date)
     */
    @Override
    public void setDeleteDate(Date delete) {
        this.deleteDate = delete;
    }

    /**
     * Mark field as dirty.
     * 
     * @param ext
     * @param fieldName
     */
    protected void setFieldDirty(String ext, String fieldName,
            Object oldValue, Object newValue) {
        String key = WikittyUtil.getFQFieldName(ext, fieldName);
        incrementVersion(key);
        getPropertyChangeSupport().firePropertyChange(key, oldValue, newValue);
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#addExtension(org.nuiton.wikitty.WikittyExtension)
     */
    @Override
    public void addExtension(WikittyExtension ext) {
        // on check qu'on avait pas deja cette extension dans une version plus
        // recente, dans ce cas, on leve une exception en disant qu'il faut
        // se mettre a jour
        WikittyExtension currentExt = extensions.get(ext.getName());
        if (currentExt != null &&
                VersionUtil.greaterThan(currentExt.getVersion(), ext.getVersion())) {
            throw new WikittyException(String.format(
                    "In wikitty '%s' You try to put extension '%s' in older version (%s) than"
                    + " current version (%s). You must update your software.",
                    getWikittyId(), ext.getName(), ext.getVersion(), currentExt.getVersion()));
        }

        // on check les dependances
        List required = ext.getRequires();
        if (required != null && !required.isEmpty() &&
                !extensions.keySet().containsAll(required)) {
            throw new WikittyException(String.format(
                    "You try to add extension '%s' that" +
                    " required not available extension '%s' in this wikitty",
                    ext.getName(), required));
        }

        extensions.put(ext.name, ext);
        incrementVersion(ext.name);

        // mise en place des valeurs par defaut des champs de l'extension
        for (String fieldName : ext.getFieldNames()) {
            FieldType type = ext.getFieldType(fieldName);
            if (type.hasDefault()) {
                setField(ext.name, fieldName, type.getDefault());
            }
        }
    }

    @Override
    public void removeExtension(String ext) {
        Collection<WikittyExtension> exts = getExtensionDependencies(ext, true);
        exts.add(extensions.get(ext));
        for (WikittyExtension e : exts) {
            String extName = e.getName();
            extensions.remove(extName);
            for (String field : e.getFieldNames()) {
                fieldValue.remove(WikittyUtil.getFQFieldName(extName, field));
            }
        }
    }

    @Override
    public void removeExtensions(Collection<String> exts) {
        for (String e : exts) {
            removeExtension(e);
        }
    }

    /**
     * @see Wikitty#hasMetaExtension(String, String)
     */
    @Override
    public boolean hasMetaExtension(String metaExtensionName,
                                    String extensionName) {
        String metaExtensionFQName = WikittyUtil.getFQMetaExtensionName(
                                                        metaExtensionName,
                                                        extensionName);
        boolean hasMetaExtension = extensions.containsKey(metaExtensionFQName);
        return hasMetaExtension;
    }
    
    /**
     * @see Wikitty#addMetaExtension(WikittyExtension, WikittyExtension)
     */
    @Override
    public void addMetaExtension(WikittyExtension metaExtension,
                                 WikittyExtension extension) {
        addMetaExtension(metaExtension, extension.getName());
    }

    /**
     * @see Wikitty#addMetaExtension(WikittyExtension, String)
     */
    @Override
    public void addMetaExtension(WikittyExtension metaExtension,
                                 String extensionName) {
        if (hasExtension(extensionName)) {
            String metaExtensionFQName = WikittyUtil.getFQMetaExtensionName(
                                                        metaExtension.getName(),
                                                        extensionName);
            extensions.put(metaExtensionFQName, metaExtension);
            incrementVersion(metaExtensionFQName);
        } else {
            throw new IllegalArgumentException("this wikitty doesn't have an extension named " + extensionName);
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#addExtension(java.util.Collection)
     */
    @Override
    public void addExtension(Collection<WikittyExtension> exts) {
        for (WikittyExtension ext : exts) {
            addExtension(ext);
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#hasExtension(java.lang.String)
     */
    @Override
    public boolean hasExtension(String extName) {
        return extensions.containsKey(extName);
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#hasField(java.lang.String, java.lang.String)
     */
    @Override
    public boolean hasField(String extName, String fieldName) {
        boolean result = false;
        WikittyExtension ext = extensions.get(extName);
        if (ext != null) {
            result = ext.getFieldType(fieldName) != null;
        }
        return result;
    }

    @Override
    public boolean hasField(String fqfieldName) {
        boolean result = false;
        try {
            String extName = WikittyExtension.extractExtensionName(fqfieldName);
            String fieldName = WikittyExtension.extractFieldName(fqfieldName);
        
            result = hasField(extName, fieldName);
        } catch (Exception eee) {
            log.trace("Invalide field name is never field", eee);
        }
        return result;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getExtension(java.lang.String)
     */
    @Override
    public WikittyExtension getExtension(String ext) {
        WikittyExtension result = extensions.get(ext);
        return result;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getExtensionNames()
     */
    @Override
    public Collection<String> getExtensionNames() {
        Collection<String> result = extensions.keySet();
        return result;
    }

    @Override
    public Collection<String> getExtensionFields(String ext) {
        Collection<String> result = getExtension(ext).getFieldNames();
        return result;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getExtensions()
     */
    @Override
    public Collection<WikittyExtension> getExtensions() {
        Collection<WikittyExtension> result = extensions.values();
        return result;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getExtensionDependencies(java.lang.String, boolean)
     */
    @Override
    public Collection<WikittyExtension> getExtensionDependencies(String ext, boolean recursively) {
        Collection<WikittyExtension> result = new HashSet<WikittyExtension>();
        Collection<WikittyExtension> all = extensions.values();
        for (WikittyExtension dependency : all) {
            List<String> requires = dependency.getRequires();
            if(requires != null && !requires.isEmpty() && requires.contains(ext)) {
                result.add(dependency);
                if(recursively) {
                    String dependencyName = dependency.getName();
                    Collection<WikittyExtension> dependencies =
                            getExtensionDependencies(dependencyName, recursively);
                    result.addAll(dependencies);
                }
            }
        }
        return result;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldType(java.lang.String)
     */
    @Override
    public FieldType getFieldType(String fqfieldName) {
        String extName = WikittyExtension.extractExtensionName(fqfieldName);
        String fieldName = WikittyExtension.extractFieldName(fqfieldName);
        FieldType result = getFieldType(extName, fieldName);
        return result;
    }

    @Override
    public FieldType getFieldType(String extName, String fieldName) {
        WikittyExtension ext = getExtension(extName);
        if (ext == null) {
            throw new WikittyException(String.format(
                    "Extension '%s' doesn't exists on wikitty '%s'", extName, id));
        } else {
            int crochet = fieldName.indexOf("[");
            if (crochet != -1) {
                fieldName = fieldName.substring(0, crochet);
            }
            FieldType result = ext.getFieldType(fieldName);
            if (result == null) {
                throw new WikittyException(String.format(
                        "field '%s' doesn't exists on extension '%s'", fieldName, extName));

            }
            return result;
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#setField(java.lang.String, java.lang.String, java.lang.Object)
     */
    @Override
    public void setField(String ext, String fieldName, Object value) {
        if (! hasField(ext, fieldName)) {
            String def = "";
            for ( WikittyExtension extension : extensions.values() ) {
                def += extension.toDefinition() + "\n";
            }
            throw new WikittyException(String.format(
                    "field '%s' is not valid, extensions definition : %s",
                    WikittyUtil.getFQFieldName(ext, fieldName), def));
        }
        String key = WikittyUtil.getFQFieldName(ext, fieldName);

        // take old value if needed
        Object oldValue = null;
        if (getPropertyChangeSupport().hasListeners(key)) {
            oldValue = fieldValue.get(key);
        }

        // put new value
        FieldType fieldType = getExtension(ext).getFieldType(fieldName);
        Object validValue = fieldType.getValidValue(value);
        fieldValue.put(key, validValue);

        // if user pass in argument object that permit to take wikitty,
        // we put it, in preload map
        if (value != null && fieldType.getType() == WikittyTypes.WIKITTY) {
            if (fieldType.isCollection()) {
                if (value instanceof Collection) {
                    for ( Object o : (Collection<?>) value ) {
                        Wikitty w = WikittyUtil.getWikitty(o);
                        addPreloaded(w);
                    }
                } else {
                    // Array
                    for ( Object o : (Object[]) value ) {
                        Wikitty w = WikittyUtil.getWikitty(o);
                        addPreloaded(w);
                    }
                }
            } else {
                Wikitty w = WikittyUtil.getWikitty(value);
                addPreloaded(w);
            }
        }

        // mark field dirty and call listener
        setFieldDirty(ext, fieldName, oldValue, validValue);
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldAsObject(java.lang.String, java.lang.String)
     */
    @Override
    public Object getFieldAsObject(String ext, String fieldName) {
        if (!hasField(ext, fieldName)) {
            WikittyExtension extension = extensions.get(ext);
            String type;
            Collection list;
            if (extension == null) {
                type = "extension";
                list = getExtensionNames();
            } else {
                type = "fieldname";
                list = extension.getFieldNames();
            }
            throw new WikittyException(String.format(
                    "field '%s' is not a valid field. acceptable %s are: %s",
                    WikittyUtil.getFQFieldName(ext, fieldName), type, list));
        }
        String key = WikittyUtil.getFQFieldName(ext, fieldName);
        Object result = fieldValue.get(key);
        return result;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldAsString(java.lang.String, java.lang.String)
     */
    @Override
    public byte[] getFieldAsBytes(String ext, String fieldName) {
        Object value = getFieldAsObject(ext, fieldName);
        try {
            byte[] result = WikittyUtil.toBinary(value);
            return result;
        } catch (WikittyException eee) {
            throw new WikittyException(String.format(
                    "field '%s' is not a valid byte[]",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldAsBoolean(java.lang.String, java.lang.String)
     */
    @Override
    public boolean getFieldAsBoolean(String ext, String fieldName) {
        Object value = getFieldAsObject(ext, fieldName);
        try {
            boolean result = WikittyUtil.toBoolean(value);
            return result;
        } catch (WikittyException eee) {
            throw new WikittyException(String.format(
                    "field '%s' is not a valid boolean",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldAsBigDecimal(java.lang.String, java.lang.String)
     */
    @Override
    public BigDecimal getFieldAsBigDecimal(String ext, String fieldName) {
        Object value = getFieldAsObject(ext, fieldName);
        try {
            BigDecimal result = WikittyUtil.toBigDecimal(value);
            return result;
        } catch (WikittyException eee) {
            throw new WikittyException(String.format(
                    "field '%s' is not a valid numeric",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldAsInt(java.lang.String, java.lang.String)
     */
    @Override
    public int getFieldAsInt(String ext, String fieldName) {
        try {
            BigDecimal value = getFieldAsBigDecimal(ext, fieldName);
            int result = value.intValue();
            return result;
        } catch (WikittyException eee) {
            throw new WikittyException(String.format(
                    "field '%s' is not a valid int",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldAsLong(java.lang.String, java.lang.String)
     */
    @Override
    public long getFieldAsLong(String ext, String fieldName) {
        try {
            BigDecimal value = getFieldAsBigDecimal(ext, fieldName);
            long result = value.longValue();
            return result;
        } catch (WikittyException eee) {
            throw new WikittyException(String.format(
                    "field '%s' is not a valid int",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldAsFloat(java.lang.String, java.lang.String)
     */
    @Override
    public float getFieldAsFloat(String ext, String fieldName) {
        try {
            BigDecimal value = getFieldAsBigDecimal(ext, fieldName);
            float result = value.floatValue();
            return result;
        } catch (WikittyException eee) {
            throw new WikittyException(String.format(
                    "field '%s' is not a valid float",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldAsDouble(java.lang.String, java.lang.String)
     */
    @Override
    public double getFieldAsDouble(String ext, String fieldName) {
        try {
            BigDecimal value = getFieldAsBigDecimal(ext, fieldName);
            double result = value.doubleValue();
            return result;
        } catch (WikittyException eee) {
            throw new WikittyException(String.format(
                    "field '%s' is not a valid float",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldAsString(java.lang.String, java.lang.String)
     */
    @Override
    public String getFieldAsString(String ext, String fieldName) {
        Object value = getFieldAsObject(ext, fieldName);
        try {
            String result = WikittyUtil.toString(value);
            return result;
        } catch (WikittyException eee) {
            throw new WikittyException(String.format(
                    "field '%s' is not a valid String",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldAsDate(java.lang.String, java.lang.String)
     */
    @Override
    public Date getFieldAsDate(String ext, String fieldName) {
        Object value = getFieldAsObject(ext, fieldName);
        try {
            Date result = WikittyUtil.toDate(value);
            return result;
        } catch (WikittyException eee) {
            throw new WikittyException(String.format(
                    "field '%s' is not a valid Date",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldAsWikitty(java.lang.String, java.lang.String)
     */
    @Override
    public String getFieldAsWikitty(String ext, String fieldName) {
        Object value = getFieldAsObject(ext, fieldName);
        String result = WikittyUtil.toWikitty(value);
        return result;
    }

    @Override
    public Wikitty getFieldAsWikitty(String extName, String fieldName, boolean exceptionIfNotLoaded) {
        String id = getFieldAsWikitty(extName, fieldName);
        Wikitty result = null;
        if (id != null) {
            result = getPreloaded().get(id);
            if (exceptionIfNotLoaded && result == null) {
                throw new WikittyException(String.format(
                        "field %s.%s is not preloaded", extName, fieldName));
            }
        }
        return result;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldAsList(java.lang.String, java.lang.String, java.lang.Class)
     */
    @Override
    public <E> List<E> getFieldAsList(String ext, String fieldName, Class<E> clazz) {
        try {
            Collection<E> collection = (Collection<E>) getFieldAsObject(ext, fieldName);
            if (collection != null) {
                // return unmodiable collection that check type of element
                return new UnModifiableCopyList<E>(clazz, collection);
            }
            return null;
        } catch (Exception eee) {
            throw new WikittyException(String.format(
                    "Can't get value to field '%s'",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }

    @Override
    public List<Wikitty> getFieldAsWikittyList(
            String ext, String fieldName, boolean exceptionIfNotLoaded) {

        List<Wikitty> result = null;

        Collection<String> collection = getFieldAsList(ext, fieldName, String.class);
        if (collection != null) {
            result = new ArrayList<Wikitty>(collection.size());
            for (String s : collection) {
                Wikitty w = getPreloaded().get(s);
                if (exceptionIfNotLoaded && w == null) {
                    throw new WikittyException(
                            String.format("Wikitty for field %s.%s is not preloaded (%s)",
                            ext, fieldName, s));
                }
                result.add(w);
            }

            // return unmodiable collection
            result = Collections.unmodifiableList(result);
        }
        return result;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFieldAsSet(java.lang.String, java.lang.String, java.lang.Class)
     */
    @Override
    public <E> Set<E> getFieldAsSet(String ext, String fieldName, Class<E> clazz) {
        try {
            Collection<E> collection = (Collection<E>) getFieldAsObject(ext, fieldName);
            if (collection != null) {
                // return unmodifable Set
                return new UnModifiableCopySet<E>(clazz, collection);
            }
            return null;
        } catch (Exception eee) {
            throw new WikittyException(String.format(
                    "Can't get value to field '%s'",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }

    @Override
    public Set<Wikitty> getFieldAsWikittySet(
            String ext, String fieldName, boolean exceptionIfNotLoaded) {

        Set<Wikitty> result = null;

        Collection<String> collection = getFieldAsSet(ext, fieldName, String.class);
        if (collection != null) {
            result = new LinkedHashSet<Wikitty>();
            for (String s : collection) {
                Wikitty w = getPreloaded().get(s);
                if (exceptionIfNotLoaded && w == null) {
                    throw new WikittyException(
                            String.format("Wikitty for field %s.%s is not preloaded (%s)",
                            ext, fieldName, s));
                }
                result.add(w);
            }

            // return unmodiable collection
            result = Collections.unmodifiableSet(result);
        }
        return result;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#addToField(java.lang.String, java.lang.String, java.lang.Object)
     */
    @Override
    public void addToField(String ext, String fieldName, Object value) {
        try {
            FieldType fieldType = getExtension(ext).getFieldType(fieldName);
            Collection col = (Collection) getFieldAsObject(ext, fieldName);
            if (col == null) {
                if (fieldType.isUnique()) {
                    // LinkedHashSet to try to maintains order
                    col = new LinkedHashSet();
                } else {
                    col = new ArrayList();
                }
                col.add(value);
                setField(ext, fieldName, col);
                // no call dirty, because already done in setField
            } else {
                // check upper bound only if col exists,
                // because ask upper bound == 0 is ridiculous

                Object validValue = fieldType.getContainedValidObject(value);
                if (fieldType.isUnique() && col.contains(validValue)) {
                    // do nothing, only add if not already in unique collection
                } else {
                    if (col.size() + 1 > fieldType.getUpperBound()) {
                        // if upper bound reached, throw an exception
                        throw new WikittyException(String.format(
                                "Can't add value for field '%s', upper bound is reached",
                                WikittyUtil.getFQFieldName(ext, fieldName)));
                    }
                    col.add(validValue);
                    // if user pass in argument object that permit to take wikitty,
                    // we put it, in preload map
                    if (fieldType.getType() == WikittyTypes.WIKITTY) {
                        Wikitty w = WikittyUtil.getWikitty(value);
                        addPreloaded(w);
                    }
                    setFieldDirty(ext, fieldName, null, col);
                }
            }
        } catch (Exception eee) {
            throw new WikittyException(String.format(
                    "Can't add value to field '%s'",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#removeFromField(java.lang.String, java.lang.String, java.lang.Object)
     */
    @Override
    public void removeFromField(String ext, String fieldName, Object value) {
        try {
            Collection col = (Collection) getFieldAsObject(ext, fieldName);
            if (col != null) {
                FieldType type = getExtension(ext).getFieldType(fieldName);
                Object validValue = type.getContainedValidObject(value);
                if (col.contains(validValue)) {
                    if (col.size() - 1 < type.getLowerBound()) {
                        throw new WikittyException(String.format(
                                "Can't remove value for field '%s', lower bound is reached",
                                WikittyUtil.getFQFieldName(ext, fieldName)));
                    } else {
                        if (col.remove(validValue)) {
                            // field is dirty only if remove is done
                            setFieldDirty(ext, fieldName, null, col);
                        }
                    }
                }
            }
        } catch (Exception eee) {
            throw new WikittyException(String.format(
                    "Can't remove value for field '%s'",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#clearField(java.lang.String, java.lang.String)
     */
    @Override
    public void clearField(String ext, String fieldName) {
        FieldType type = getExtension(ext).getFieldType(fieldName);
        if (type.getLowerBound() > 0) {
            throw new WikittyException(String.format(
                    "Can't clear values for field '%s', lower bound is > 0",
                    WikittyUtil.getFQFieldName(ext, fieldName)));
        }
        try {
            Collection col = (Collection) getFieldAsObject(ext, fieldName);
            if (col != null) {
                col.clear();
                setFieldDirty(ext, fieldName, null, col);
            }
        } catch (Exception eee) {
            throw new WikittyException(String.format(
                    "Can't clear value for field '%s'",
                    WikittyUtil.getFQFieldName(ext, fieldName)), eee);
        }
    }
    
    /**
     * @see org.nuiton.wikitty.entities.Wikitty#addToField(String, Object)
     */
    @Override
    public void addToField(String fqFieldName, Object value) {
        String[] extAndField = fqFieldName.split("\\.");
        addToField(extAndField[0], extAndField[1], value);
    }

    /**
     * @see Wikitty#removeFromField(String, Object)
     */
    @Override
    public void removeFromField(String fqFieldName, Object value) {
        String[] extAndField = fqFieldName.split("\\.");
        removeFromField(extAndField[0], extAndField[1], value);
    }

    /**
     * @see Wikitty#clearField(String)
     */
    @Override
    public void clearField(String fqFieldName) {
        String[] extAndField = fqFieldName.split("\\.");
        clearField(extAndField[0], extAndField[1]);
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof Wikitty) {
            Wikitty other = (Wikitty) obj;
            result = id.equals(other.getWikittyId());
        }
        return result;
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return super.hashCode();
        } else {
            return id.hashCode();
        }
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#fieldNames()
     */
    @Override
    public Set<String> fieldNames() {
        return fieldValue.keySet();
    }

    @Override
    public Map<String, Object> getFieldValue() {
        return Collections.unmodifiableMap(fieldValue);
    }

    @Override
    public Set<String> getAllFieldNames() {
        Set<String> result = new HashSet<String>();
        for (WikittyExtension ext : getExtensions()) {
            String extName = ext.getName() + WikittyUtil.FQ_FIELD_NAME_SEPARATOR;
            for (String f : ext.getFieldNames()) {
                result.add(extName + f);
            }
        }
        return result;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getFqField(java.lang.String)
     */
    @Override
    public Object getFqField(String fqFieldName) {
        Object result;
        if (Element.ID.getValue().equalsIgnoreCase(fqFieldName)) {
            result = getWikittyId();
        } else if (Element.ID.getValue().equalsIgnoreCase(fqFieldName)) {
            result = getExtensionNames();
        } else {
            result = fieldValue.get(fqFieldName);
        }
        return result;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#getWikittyVersion()
     */
    @Override
    public String getVersion() {
        return version;
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#setWikittyVersion(java.lang.String)
     */
    @Override
    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String getWikittyVersion() {
        return version;
    }

    @Override
    public void setWikittyVersion(String version) {
        this.version = version;
    }

    /**
     * @see Wikitty#getDirty()
     */
    public Set<String> getDirty() {
        return fieldDirty;
    }
    
    /*
     * @see org.nuiton.wikitty.Wikitty#clearDirty()
     */
    @Override
    public void clearDirty() {
        fieldDirty.clear();
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#setFqField(java.lang.String, java.lang.Object)
     */
    @Override
    public void setFqField(String fieldName, Object value) {
        String ext = WikittyExtension.extractExtensionName(fieldName);
        String field = WikittyExtension.extractFieldName(fieldName);
        setField(ext, field, value);
    }

    /*
     * @see org.nuiton.wikitty.Wikitty#isEmpty()
     */
    @Override
    public boolean isEmpty() {
        return fieldValue.isEmpty();
    }

    /**
     * Try to find extension with toString tagValue. The first tagValue found
     * is used. If no toString tagValue found, then {@link #toStringAllField()}
     * is used.
     *
     * @return
     */
    @Override
    public String toString() {
        WikittyExtension extFormat = null;
        WikittyExtension[] exts =
                getExtensions().toArray(new WikittyExtension[getExtensions().size()]);
        // on parcours a l'inverse la liste, car les premieres extensions sont
        // les extensions requises et donc moins specifiques que les dernieres
        for (int i=exts.length-1; i>=0; i--) {
            WikittyExtension ext = exts[i];
            String format = ext.getTagValue(WikittyTagValue.TAG_TO_STRING);
            if (StringUtils.isNotBlank(format)) {
                extFormat = ext;
                break;
            }
        }

        String result;
        if (extFormat == null) {
            result = toStringAllField();
        } else {
            result = extFormat.toString(this);
        }
        return result;
    }

    /**
     * Print all field of all extension
     */
    @Override
    public String toStringAllField() {
        boolean cr = true;
        String str = "[" + getWikittyId() + ":" + getWikittyVersion() + "] {";
        for ( String extName : getExtensionNames() ) {
            WikittyExtension ext = getExtension(extName);
            str += (cr ? "\n" : "") + "\t* " + extName + "\n";
            cr = false;
            for ( String fieldName : ext.getFieldNames() ) {
                str += "\t\t* " + fieldName + " = " + getFieldAsString(extName, fieldName) + "\n";
            }
        }
        str += "}";
        return str;
    }

    /**
     * Return String representation of this wikitty with toString specific
     * format of extension passed in argument. If this extension doesn't have
     * specifique toString, normal toString is called {@link #toString()}
     */
    @Override
    public String toString(String extName) {
        WikittyExtension ext = getExtension(extName);
        String result = ext.toString(this);
        return result;
    }

    /**
     * Clone is deep for extension and values
     * @return
     * @throws CloneNotSupportedException
     */
    @Override
    public WikittyImpl clone() throws CloneNotSupportedException {
        WikittyImpl result = new WikittyImpl(this.id);
        result.replaceWith(this);
        return result;
    }

    /**
     * unmodifiable collection, source is copied to prevent source modification
     * after creation.
     */
    static class UnModifiableCopyList<E> extends AbstractList<E> {
        protected Class<E> clazz;
        protected List<E> contained;

        public UnModifiableCopyList(Class<E> clazz, Collection<E> source) {
            this.clazz = clazz;
            // make copy to prevent modification of source collection
            contained = new ArrayList<E>(source);
        }

        @Override
        public E get(int index) {
            return WikittyUtil.cast(contained.get(index), clazz);
        }
        @Override
        public int size() {
            return contained.size();
        }
        // no need to Override iterator, because AbstractList based all iterator
        // operation on AbstractList aperation
    }


    /**
     * unmodifiable collection, source is copied to prevent source modification
     * after creation.
     */
    static class UnModifiableCopySet<E> extends AbstractSet<E> {
        protected Class<E> clazz;
        protected Set<E> contained;
        public UnModifiableCopySet(Class<E> clazz, Collection<E> source) {
            this.clazz = clazz;
            // make copy to prevent modification of source collection
            contained = new LinkedHashSet<E>(source);
        }

        @Override
        public int size() {
            return contained.size();
        }
        @Override
        public Iterator<E> iterator() {
            return new Iterator<E>() {
                Iterator containedIterator = contained.iterator();
                public boolean hasNext() {
                    return containedIterator.hasNext();
                }

                public E next() {
                    Object o = containedIterator.next();
                    return WikittyUtil.cast(o, clazz);
                }

                public void remove() {
                    throw new UnsupportedOperationException("Not supported operation");
                }
            };
        }
    }

    protected void incrementVersion(String cause) {
        //Indexation solr vérifie si un objet à changé seulement si fieldDirty est non vide
        fieldDirty.add(cause);
        version = WikittyUtil.incrementMinorRevision(version);
    }

}
