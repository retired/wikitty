/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.entities;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.nuiton.wikitty.WikittyUtil;

import org.nuiton.wikitty.entities.WikittyTypes;

public class ExtensionFactory {

    protected String name;
    protected String version;
    protected Map<String, String> tagValues;
    protected String requiredExtension;

    protected LinkedHashMap<String, FieldType> fields;

    protected ExtensionFactory(String name, String version) {
        this.name = name;
        this.version = version;
        this.fields = new LinkedHashMap<String, FieldType>();
    }

    protected ExtensionFactory(String name, String version, String requiredExtension) {
        this(name, version);
        this.requiredExtension = requiredExtension;
    }

    public static ExtensionFactory create(String name, String version) {
        return new ExtensionFactory(name, version);
    }

    public static ExtensionFactory create(String name, String version, String requiredExtension) {
        return new ExtensionFactory(name, version, requiredExtension);
    }

    public FieldFactory addField(String name, WikittyTypes fieldType) {
        return new FieldFactory(this, name, fieldType);
    }

    public WikittyExtension extension() {
        WikittyExtension ext = new WikittyExtension(
                name, version, tagValues, requiredExtension, fields);
        return ext;
    }

    public ExtensionFactory addTagValues(Map<String, String> tagValues) {
        if (this.tagValues == null) {
            this.tagValues = new HashMap<String, String>();
        }
        this.tagValues.putAll(tagValues);
        return this;
    }

    public ExtensionFactory addTagValues(String tagValues) {
        if (this.tagValues == null) {
            this.tagValues = new HashMap<String, String>();
        }
        Map<String, String> tv = WikittyUtil.tagValuesToMap(tagValues);
        this.tagValues.putAll(tv);
        return this;
    }

    protected void add(String fieldName, FieldType fieldType) {
        fields.put(fieldName, fieldType);
    }
}
