/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.entities;

/**
 * Enumeration de tous les types de champs supporte par Wikitty
 *
 * @author poussin
 */
public enum WikittyTypes {
    BINARY, BOOLEAN, DATE, NUMERIC, STRING, WIKITTY;

    /**
     * convert string to TYPE, this method accept not trimed and not well
     * cased string (difference with valueOf)
     * @param name
     * @return TYPE else exception is thrown
     */
    public static WikittyTypes parse(String name) {
        WikittyTypes result = valueOf(name.trim().toUpperCase());
        return result;
    }

}
