/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.entities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyMetaExtensionUtil {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyMetaExtensionUtil.class);

    private static String SEPARATOR = ":";

    /**
     * generate id for meta extension and extension
     * @return a wikitty id
     */
    static public String generateId(
            String metaExtensionName, String extensionName) {
        return String.format("%s%s%s", metaExtensionName, SEPARATOR, extensionName);
    }

    /**
     * Extract meta extension name from wikittyId. If Id is not meta extension
     * id, return null
     */
    static public String extractMetaName(String id) {
        String[] ids = id.split(SEPARATOR);
        if (ids.length == 2) {
            return ids[0];
        } else {
            return null;
        }
    }

    /**
     * Extract extension name from wikittyId. If Id is not meta extension
     * id, return null
     */
    static public String extractExtName(String id) {
        String[] ids = id.split(SEPARATOR);
        if (ids.length == 2) {
            return ids[1];
        } else {
            return null;
        }
    }

}
