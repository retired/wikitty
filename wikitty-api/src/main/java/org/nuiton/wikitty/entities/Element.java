/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.entities;

import java.io.Serializable;
import org.nuiton.wikitty.WikittyConstants;

/**
 * Class mere qui determine sur quoi porte une condition. Les classes filles
 * possible sont par exemple: ElementField, ElementId, ElementExtension
 *
 * This class and sub classes must be very simple because this class is used
 * in Wikitty interface that GWT used.
 *
 * @author poussin
 * @version $Revision$
 * @since 3.3
 *
 * Last update: $Date$
 * by : $Author$
 */
public class Element implements WikittyConstants, Serializable {

    // serialVersionUID is used for serialization.
    private static final long serialVersionUID = 1L;

    /** utilise pour creer une condition sur le noeud root de l'arbre */
    public static final ElementNode  NODE_ROOT = new ElementNode(ElementNode.FIELD_NODE_ROOT);
    /** utilise pour creer une condition sur un noeud ou un de ses peres (le chemin d'acces a un noeud)*/
    public static final ElementNode  NODE_PATH = new ElementNode(ElementNode.FIELD_NODE_PATH);
    /** utilise pour creer une condition sur la profondeur d'un noeud (root=1) */
    public static final ElementNode  NODE_DEPTH = new ElementNode(ElementNode.FIELD_NODE_DEPTH);

    public static final ElementId ID = new ElementId();
    public static final ElementExtension EXTENSION = new ElementExtension();
    public static final ElementField ALL_FIELD =
            new ElementField("*" + FQ_FIELD_NAME_SEPARATOR + "*");

    protected String value;

    public Element() {
    }

    public Element(String value) {
        this.value = value;
    }

    public static Element get(String v) {
        Element result;
        if (Element.ID.getValue().equalsIgnoreCase(v)) {
            result = Element.ID;
        } else if (Element.EXTENSION.getValue().equalsIgnoreCase(v)) {
            result = Element.EXTENSION;
        } else if (Element.NODE_ROOT.getValue().equalsIgnoreCase(v)) {
            result = Element.NODE_ROOT;
        } else if (Element.NODE_PATH.getValue().equalsIgnoreCase(v)) {
            result = Element.NODE_PATH;
        } else if (Element.NODE_DEPTH.getValue().equalsIgnoreCase(v)) {
            result = Element.NODE_DEPTH;
        } else if (Element.ALL_FIELD.getValue().equalsIgnoreCase(v)) {
            result = Element.ALL_FIELD;
        } else if ("*".equalsIgnoreCase(v)) {
            result = Element.ALL_FIELD;
        } else {
            result = new ElementField(v);
        }
        return result;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result;

        if (this == obj) {
            result = true;
        } else if (obj == null) {
            result = false;
        } else if (this.getClass().equals(obj.getClass())) {
            Element e = (Element)obj;
            result = (this.getValue() == e.getValue() // == si les deux sont null
                    || (this.getValue() != null && e.getValue() != null
                    && this.getValue().equals(e.getValue())));
        } else {
            result = false;
        }
        return result;
    }

    @Override
    public int hashCode() {
        // a priori, pas de probleme pour utiliser value pour la hash car pas
        // moyen de changer sa valeur, une fois l'objet cree
        return getValue()==null?0:this.getValue().hashCode();
    }

    @Override
    public String toString() {
        return value;
    }

}
