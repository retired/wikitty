/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty;

import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;

import static org.nuiton.i18n.I18n.t;

/**
 * Configuration de tous les modules Wikitty. La configuration par defaut doit
 * permettre un bon fonctionnement de wikitty pour quelqu'un souhaitant faire un
 * essaie rapide. La configuration actuelle:
 * <ul>
 * <li> wikitty-config.properties comme fichier de configuration</li>
 * <li> WikittyServiceInMemory</li>
 * <li> pas de notification reseau</li>
 * <li> /tmp pour les exports</li>
 * </ul>
 *
 * Si on utilise WikittyServiceStorage au lieu de WikittyServiceInMemory par defaut
 * <ul>
 * <li> base h2 embarque</li>
 * <li> solr</li>
 * </ul>
 *
 * Si on utilise la notification des events
 * <ul>
 * <li> transporter XMPPNotifierTransporter</li>
 * <li> serveur im.codelutin.com</li>
 * <li> room test@conference.im.codelutin.com</li>
 * </ul>
 *
 * Des la creation de l'objet les fichiers de configuration sont recherches.
 *
 * @author poussin
 * @version $Revision$
 *                  extends ApplicationConfig
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyConfig {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyConfig.class);

    private WikittyConfig() {
    }


    static public ApplicationConfig getConfig() {
        return getConfig(null, null);
    }

    static public ApplicationConfig getConfig(String[] args) {
        return getConfig(null, null, args);
    }

    /**
     * Create WikittyConfig and load particular configuration filename
     * @param configFilename name of wikitty config file
     */
    static public ApplicationConfig getConfig(String configFilename) {
        return getConfig(null, configFilename);
    }

    /**
     * Create WikittyConfig and use props as default value
     *
     * @param props as default value
     */
    static public ApplicationConfig getConfig(Properties props) {
        return getConfig(props, null);
    }

    static public ApplicationConfig getConfig(
            Properties props, String configFilename, String ... args) {
        ApplicationConfig config = new ApplicationConfig(props, configFilename);
        config.loadDefaultOptions(WikittyConfigOption.values());
        config.loadActions(WikittyConfigAction.values());
        
        try {
            config.parse(args);
        } catch (ArgumentsParserException eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't load wikitty configuration", eee);
            }
        }
        return config;
    }    

    /**
     * Par default on utilise le mot de passe en parametre, sauf s'il est blanc
     * ou null, dans se cas on utilise le mot de passe general.
     * @param config
     * @param password
     * @return
     */
    static public String getCryptPassword(ApplicationConfig config, String password) {
        if (StringUtils.isBlank(password)) {
            password = config.getOption(WikittyConfigOption.WIKITTY_STORAGE_CRYPT_PASSWORD.getKey());
        }
        return password;
    }

    static public void help() {
        System.out.println("Wikitty configuration and action");
        System.out.println("Options (set with --option <key> <value>:");
        for (WikittyConfigOption o : WikittyConfigOption.values()) {
            log.debug("\t" + o.key + "(" + o.defaultValue + "):" + t(o.description));
        }

        log.debug("Actions:");
        for (WikittyConfigAction a : WikittyConfigAction.values()) {
            log.debug("\t" + java.util.Arrays.toString(a.aliases) + "(" + a.action + "):" + t(a.description));
        }
        System.exit(0);
    }

}
