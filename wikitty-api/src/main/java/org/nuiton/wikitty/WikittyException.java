/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty;

/**
 * Wikitty exception.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public WikittyException(String message) {
        super(message);
    }

    /**
     * TODO EC20100921 add message with cause use {@link #WikittyException(String, Throwable)}
     */
    @Deprecated
    public WikittyException(Throwable cause) {
        super(cause);
    }

    public WikittyException(String message, Throwable cause) {
        super(message, cause);
    }

}
