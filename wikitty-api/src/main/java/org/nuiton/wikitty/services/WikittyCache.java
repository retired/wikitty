/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;

/**
 * Interface commune a toutes les implantations du cache. Les implantations
 * doivent faire le moins de travail possible, donc s'il y a des choses communes
 * il faut les coder dans {@link WikittyServiceCached}
 *
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface WikittyCache {

    /**
     * Clear all cache.
     */
    void clearWikitty();

    boolean existsWikitty(String id);

    /**
     * Return wikitty object if is in the cache, null otherwize.
     *
     * @param id
     * @return wikitty object or null
     */
    Wikitty getWikitty(String id);

    /**
     * put wikitty in cache
     * one in cache
     * @param e
     */
    void putWikitty(Wikitty e);

    /**
     * Remove wikitty from cache.
     *
     * @param id wikitty id to remove
     */
    void removeWikitty(String id);

    public boolean existsExtension(String id);

    /**
     * Return extension object if is in the cache, null otherwize.
     *
     * @param id
     * @return extension object or null
     */
    public WikittyExtension getExtension(String id);

    /**
     * put extension in cache
     * @param e
     */
    public void putExtension(WikittyExtension e);

    /**
     * Remove extension from cache.
     *
     * @param id wikitty id to remove
     */
    public void removeExtension(String id);

    /**
     * Clear all cache.
     */
    public void clearExtension();

}
