/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.StringUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.entities.WikittyCopyOnWrite;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyUtil;

/**
 * Override some method of WikittyService to use cache
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceCached extends WikittyServiceDelegator {
    
    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceCached.class);

    /** Cache. */
    protected WikittyCache cache = null;

    /** pour recevoir les events du serveur et synchroniser le cache */
    protected RemoteWikittyListener remoteWikittyListener;
    
    /** cache policy (configuration)
     * 
     * if true the cache will always restore copies of the wikitty
     * if false the cache will restore wikitties that do a lazy copy when the
     * wikitty is modified ({@link WikittyCopyOnWrite} instances)
     * 
     * default set to false. To change this value, use 
     * {@link WikittyServiceCached#WikittyServiceCached(ApplicationConfig, WikittyService, WikittyCache)}
     * and set the good property.
     */
    protected boolean allwaysRestoreCopies = false;

    /** nombre de solicitation du cache */
    protected long asked = 0;
    /** nombre de fois que l'objet demande n'etait pas dans le cache */
    protected long missed = 0;

    /**
     * Constructor with configuration.
     * 
     * @param config config
     * @param ws delegate service
     * @param cache wikity cache
     */
    public WikittyServiceCached(ApplicationConfig config, WikittyService ws, WikittyCache cache) {
        super(ws);
        // this.ws = ws;
        this.cache = cache;

        if (cache == null) {
            if (log.isWarnEnabled()) {
                String cacheClassName = config.getOption(WikittyConfigOption.
                    WIKITTY_WIKITTYSERVICECACHED_COMPONENTS.getKey());
                log.warn(String.format(
                        "No cache class implementation available (%s), use simple cache",
                        cacheClassName));
            }
            this.cache = new WikittyCacheSimple(config);
        }


        // add service listener for synchronisation listener for remote event
        // register all time, but perhaps Notifier service don't listen for
        // remote event and cache can't be notified. But this is normal
        // behavior
        try {
            remoteWikittyListener = new RemoteWikittyListener(this);
            addWikittyServiceListener(
                    remoteWikittyListener, ServiceListenerType.REMOTE);
        } catch(UnsupportedOperationException eee) {
            log.warn("no WikittyServiceNotifier available, cache don't listen event");
        }

        if (config != null) {
            // reading configuration and set allwaysRestoreCopies accordingly
            allwaysRestoreCopies =
                    config.getOptionAsBoolean(
                    WikittyConfigOption.WIKITTY_CACHE_RESTORE_COPIES.getKey());
        }
    }

    protected void statAdd(int asked, int missed) {
        this.asked += asked;
        this.missed += missed;
        if(log.isDebugEnabled()) {
            String total = StringUtil.convertMemory(Runtime.getRuntime().totalMemory());
            String free = StringUtil.convertMemory(Runtime.getRuntime().freeMemory());
            String msg = String.format("cache stat (missed/asked): %s/%s (memory %s/%s [total/free])",
                    this.missed, this.asked, total, free);
            log.debug(msg);
        }
    }

    /**
     * Retourne le nombre de solicitation du cache.
     * 
     * @return
     */
    public long getAsked() {
        return asked;
    }

    /**
     * Retourne le nombre de fois que l'element dans le cache demande n'y etait
     * pas.
     * 
     * @return
     */
    public long getMissed() {
        return missed;
    }

    /**
     * Wrap the wikitty or copy it according to allwaysRestoreCopies value.
     */
    protected Wikitty wrapWikitty(Wikitty wikitty) {
        Wikitty result = null;

        // Restored wikitty can be null
        if (wikitty != null) {
            if (allwaysRestoreCopies) {
                try {
                    result = wikitty.clone();
                } catch (CloneNotSupportedException eee) {
                    // si on arrive pas a faire un clone, on retourne null
                    // comme si l'objet n'etait pas dans le cache, pour que
                    // l'application fonctionne tout de meme mais en mode
                    // degrade (sans cache)
                    log.error(String.format(
                            "Cache doesn't work, unable to clone %s", wikitty), eee);
                }
            } else {
                // normalement, on a que des WikittyImpl ici, mais on fait
                // un petit check pour etre sur qu'on ne wrap pas un object wrappe
                if (wikitty instanceof WikittyCopyOnWrite) {
                    wikitty = ((WikittyCopyOnWrite)wikitty).getTarget();
                }
                result = new WikittyCopyOnWrite(wikitty);
            }
        }
        return result;
    }

    //
    // seulement les methodes suivantes doivent acceder au cache pour centraliser
    // son access
    //
    
    protected void cacheClearWikitty() {
        cache.clearWikitty();
    }

    protected void cacheClearExtension() {
        cache.clearExtension();
    }

    /**
     * Only WikittyImpl can be put in real cache implementation. If argument
     * is WikittyCopyOnWrite, we must take internal wikitty to put in cache
     */
    protected void cachePutWikitty(Wikitty w) {
        if (w != null) {
            if (w instanceof WikittyCopyOnWrite) {
                w = ((WikittyCopyOnWrite) w).getTarget();
            }

            Wikitty old = cache.getWikitty(w.getWikittyId());
            if (old == null
                    || WikittyUtil.versionGreaterThan(w.getWikittyVersion(), old.getWikittyVersion())) {
                cache.putWikitty(w);

                if (log.isTraceEnabled()) {
                    log.trace("Replace cached wikitty : new version " + w.getWikittyVersion()
                            + " > old version " + (old == null ? null : old.getWikittyVersion()));
                }
            } else {
                if (log.isTraceEnabled()) {
                    log.trace(String.format(
                            "Ignoring putWikittyEvent : new version %s < old version %s",
                            w.getWikittyVersion(), old.getWikittyVersion()));
                }
            }
        }
    }

    /**
     * Only WikittyImpl can be put in real cache implementation. If argument
     * is WikittyCopyOnWrite, we must take internal wikitty to put in cache
     */
    protected void cachePutExtension(WikittyExtension ext) {
        if (ext != null) {
            if (!cache.existsExtension(ext.getId())) {
                cache.putExtension(ext);

                if (log.isTraceEnabled()) {
                    log.trace(String.format(
                            "Replace cached wikitty extension '%s'", ext.getId()));
                }
            } else {
                if (log.isTraceEnabled()) {
                    log.trace(String.format(
                            "Ignoring put wikitty extension for '%s'", ext.getId()));
                }
            }
        }
    }

    protected void cachePutWikitty(Collection<Wikitty> wikitties) {
        for (Wikitty w : wikitties) {
            cachePutWikitty(w);
        }
    }
    protected void cachePutExtension(Collection<WikittyExtension> wikitties) {
        for (WikittyExtension w : wikitties) {
            cachePutExtension(w);
        }
    }
    protected void cacheRemoveWikitty(String id) {
        if (id != null) {
            cache.removeWikitty(id);
        }
    }

    protected void cacheRemoveExtension(String extId) {
        if (extId != null) {
            if (log.isDebugEnabled()) {
                log.debug("Remove extension from cache " + extId);
            }
            cache.removeExtension(extId);
        }
    }

    protected void cacheRemoveWikitty(Collection<String> ids) {
        for (String id : ids) {
            cacheRemoveWikitty(id);
        }
    }

    protected void cacheRemoveExtension(Collection<String> extIds) {
        for (String extId : extIds) {
            cacheRemoveExtension(extId);
        }
    }

    protected Wikitty cacheGetWikitty(String id) {
        Wikitty result = null;
        if (id != null) {
            result = cache.getWikitty(id);

            // all time wrap, only WikittyImpl are in cache
            result = wrapWikitty(result);
        }
        return result;
    }
    protected WikittyExtension cacheGetExtensions(String id) {
        WikittyExtension result = null;
        if (id != null) {
            result =cache.getExtension(id);
        }

        return result;
    }

    //
    // surcharge des methodes du WikittyService
    //
    
    @Override
    public WikittyEvent clear(String securityToken) {
        WikittyEvent result = getDelegate().clear(securityToken);
        processEvent(result);
        return result;
    }

    /**
     * delete objets in cache
     * @param ids
     */
    @Override
    public WikittyEvent delete(String securityToken, Collection<String> ids) {
        WikittyEvent result = getDelegate().delete(securityToken, ids);
        processEvent(result);
        return result;
    }

    /**
     * just wrap service method
     *
     * @return
     */
    @Override
    public List<String> getAllExtensionIds(String securityToken) {
        // NOTE poussin 20101219: si on veut utiliser le cache il faut une
        // methode specifique
        return getDelegate().getAllExtensionIds(securityToken);
    }

    /**
     * just wrap service method
     *
     * @param extensionName
     * @return
     */
    @Override
    public List<String> getAllExtensionsRequires(
            String securityToken, String extensionName) {
        // TODO poussin 20100412: perhaps use cache for extension ?
        return getDelegate().getAllExtensionsRequires(securityToken, extensionName);
    }

    /**
     * Overriden to put all restored object from server in cache
     *
     * @param securityToken security token
     * @param ids wikitty ids to restore
     * @return wikitty list
     */
    @Override
    public List<Wikitty> restore(String securityToken, List<String> ids) {
        ArrayList<String> notInCache = new ArrayList<String>();
        // linked to maintains the ordre
        LinkedHashMap<String, Wikitty> fromCache =
                new  LinkedHashMap<String, Wikitty>();
        for (String id : ids) {
            // w is automaticaly wrapped
            Wikitty w = cacheGetWikitty(id);
            fromCache.put(id, w); // put all to maintains order
            if (w == null) { // if not found on cache, ask the server
                notInCache.add(id);
            }
        }

        // retrieve missing object
        if (!notInCache.isEmpty()) {
            List<Wikitty> missingInCache = getDelegate().restore(securityToken, notInCache);

            cachePutWikitty(missingInCache);

            for (Wikitty w : missingInCache) {
                // add missing object
                if (w != null) {

                    // wrap new new retrieved wikitties
                    w = wrapWikitty(w);
                    fromCache.put(w.getWikittyId(), w);
                }
            }
        }

        statAdd(ids.size(), notInCache.size());
        
        return new ArrayList<Wikitty>(fromCache.values());
    }

    /**
     * just wrap service method
     *
     * @param name
     * @return
     */
    @Override
    public WikittyExtension restoreExtensionLastVersion(
            String securityToken, String name) {
        // NOTE poussin 20100412: comment faire pour utiliser le cache ? on ne
        // peut que stocker le resultat sinon il faudrait etre sur d'avoir toutes
        // les extensions dans le cache.
        // Sinon il faudrait mettre des methodes specifiques dans le cache pour
        // cette methode, mise en cache lors du 1er appel, et restitution du
        // meme resultat pour les appels suivants tant que pas de modification
        // de l'extension (ou de clear/remove)
        WikittyExtension result = getDelegate().restoreExtensionLastVersion(securityToken, name);
        cachePutExtension(result);
        return result;
    }

    /**
     * just wrap service method
     *
     * @param wikittyId
     * @return
     */
    @Override
    public WikittyEvent deleteTree(String securityToken, String wikittyId) {
        WikittyEvent result = getDelegate().deleteTree(securityToken, wikittyId);
        processEvent(result);
        return result;
    }

    /**
     * Overriden to put wikitty in cache
     *
     * @param wikitties
     * @param force boolean force non version version increment on saved wikitty
     *              or force version on wikitty creation (version 0.0)
     * @return
     */
    @Override
    public WikittyEvent store(String securityToken,
            Collection<Wikitty> wikitties, boolean force) {
        WikittyEvent result = getDelegate().store(securityToken, wikitties, force);

        processEvent(result);
        
        return result;
    }

    @Override
    public WikittyEvent storeExtension(String securityToken,
            Collection<WikittyExtension> exts) {
        WikittyEvent result = getDelegate().storeExtension(securityToken, exts);
        processEvent(result);
        return result;
    }

    @Override
    public WikittyEvent deleteExtension(
            String securityToken, Collection<String> extNames) {
        WikittyEvent result = getDelegate().deleteExtension(securityToken, extNames);
        processEvent(result);
        return result;
    }

    @Override
    public WikittyExtension restoreExtension(String securityToken, String id) {
        WikittyExtension result = getDelegate().restoreExtension(securityToken, id);
        cachePutExtension(result);
        return result;
    }

    @Override
    public Wikitty restoreVersion(
            String securityToken, String wikittyId, String version) {
        // not put it in cache ? cache doesn't support more than one version of wikitty
        return getDelegate().restoreVersion(securityToken, wikittyId, version);
    }

    @Override
    public WikittyEvent replay(
            String securityToken, List<WikittyEvent> events, boolean force) {
        WikittyEvent result = getDelegate().replay(securityToken, events, force);
        processEvent(result);
        return result;
    }


    /**
     * Process event to update cache with data in event
     *
     * @param e
     */
    protected void processEvent(WikittyEvent e) {
        
        if (log.isDebugEnabled()) {
            log.debug("Cache receive event : " + e);
        }

        // check clear must be the first, if event have clear and other type
        // clear is all time play first
        if (e.getType().contains(
                WikittyEvent.WikittyEventType.CLEAR_WIKITTY)
                || e.getType().contains(
                WikittyEvent.WikittyEventType.CLEAR_EXTENSION)) {
            cacheClearWikitty();
            cacheClearExtension();
        } else {
            if (e.getType().contains(WikittyEvent.WikittyEventType.PUT_WIKITTY)) {
                cachePutWikitty(e.getWikitties().values());
            }
            if (e.getType().contains(WikittyEvent.WikittyEventType.REMOVE_WIKITTY)) {
                cacheRemoveWikitty(e.getRemoveDate().keySet());
            }
            if (e.getType().contains(WikittyEvent.WikittyEventType.PUT_EXTENSION)) {
                cachePutExtension(e.getExtensions().values());
            }
            if (e.getType().contains(WikittyEvent.WikittyEventType.REMOVE_EXTENSION)) {
                cacheRemoveExtension(e.getDeletedExtensions());
            }
        }
    }
    
    /**
     * Classe permettant de recevoir les events distants et mettre a jour le cache
     */
    static public class RemoteWikittyListener implements WikittyListener {

        protected WikittyServiceCached wsCached;

        public RemoteWikittyListener(WikittyServiceCached wsCached) {
            this.wsCached = wsCached;
        }

        /*
         * @see org.nuiton.wikitty.WikittyListener#clearWikitty()
         */
        @Override
        public void clearWikitty(WikittyEvent event) {
            wsCached.processEvent(event);
        }

        /*
         * @see org.nuiton.wikitty.WikittyListener#putWikitty(org.nuiton.wikitty.Wikitty[])
         */
        @Override
        public void putWikitty(WikittyEvent event) {
            wsCached.processEvent(event);
        }

        /*
         * @see org.nuiton.wikitty.WikittyListener#removeWikitty(java.lang.String[])
         */
        @Override
        public void removeWikitty(WikittyEvent event) {
            wsCached.processEvent(event);
        }

        /*
         * @see org.nuiton.wikitty.WikittyListener#putExtension(org.nuiton.wikitty.WikittyExtension[])
         */
        @Override
        public void putExtension(WikittyEvent event) {
            wsCached.processEvent(event);
        }

        @Override
        public void removeExtension(WikittyEvent event) {
            wsCached.processEvent(event);
        }

        /*
         * @see org.nuiton.wikitty.WikittyListener#clearExtension()
         */
        @Override
        public void clearExtension(WikittyEvent event) {
            wsCached.processEvent(event);
        }

    }
}
