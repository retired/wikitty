/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.TimeLog;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyUser;
import org.nuiton.wikitty.entities.WikittyUserHelper;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;

/**
 * Implantation de base de l'authentification sur Wikitty lui même.
 * Cherche a authentifier l'utilisateur sur un objet WikittyUser via son
 * login et mot de passe. Ce service doit retourne un token valide si
 * l'authorisation a fonctionne.
 *
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceAuthentication extends WikittyServiceAuthenticationAbstract {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceAuthentication.class);

    /** use to trace time of security code, timelog must not include delegator
     * time in this class */
    final static private TimeLog timeLog = new TimeLog(WikittyServiceAuthentication.class);

    public WikittyServiceAuthentication(ApplicationConfig config, WikittyService ws) {
        super(config, ws);
        if (config != null) {
            long timeToLogInfo = config.getOptionAsInt(WikittyConfigOption.
                    WIKITTY_SERVICE_TIME_TO_LOG_INFO.getKey());
            long timeToLogWarn = config.getOptionAsInt(WikittyConfigOption.
                    WIKITTY_SERVICE_TIME_TO_LOG_WARN.getKey());
            timeLog.setTimeToLogInfo(timeToLogInfo);
            timeLog.setTimeToLogWarn(timeToLogWarn);
        }
    }

    /**
     * L'exception lever en cas de mauvais login ou mot de passe contient toujours
     * le même message pour ne pas aider les attaquants a trouver des comptes
     * existant.
     *
     * @param login le login (ne doit pas etre vide)
     * @param password le mot de passe de l'utilisateur
     * @return le token de securite a utiliser pour les autres appels
     * @throws SecurityException si l'authentification echoue
     */
    @Override
    public String login(String login, String password) {
        long start = TimeLog.getTime();

        if (StringUtils.isBlank(login)) {
            if (log.isDebugEnabled()) {
                log.debug(String.format("User try to authenticate with bad blank login: '%s'", login));
            }
            throw new SecurityException("bad login or password");
        }

        // recherche de l'utilisateur
        WikittyQuery criteria = new WikittyQueryMaker()
                .eq(WikittyUser.FQ_FIELD_WIKITTYUSER_LOGIN, login).end();
        String userId = getAnonymousClient().findByQuery(criteria);

        if (userId == null) {
            if (log.isDebugEnabled()) {
                log.debug(String.format("User try to authenticate with bad login: '%s'", login));
            }
            throw new SecurityException("bad login or password");
        }

        // on a trouver l'utilisateur on le restore pour verifier le mot de passe
        Wikitty user = WikittyServiceEnhanced.restore(
                getDelegate(), null, userId);
        // check password is valid
        if (!StringUtils.equals(WikittyUserHelper.getPassword(user), password)) {
            if (log.isDebugEnabled()) {
                log.debug(String.format("User '%s' try to authenticate with bad password", login));
            }
            throw new SecurityException("bad login or password");
        }
        
        String tokenId = getToken(userId);
        if (log.isDebugEnabled()) {
            log.debug(String.format("User logged: '%s'", login));
        }

        timeLog.log(start, "login");
        return tokenId;
    }

}
