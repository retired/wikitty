/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import com.arjuna.ats.arjuna.common.ObjectStoreEnvironmentBean;
import com.arjuna.ats.internal.arjuna.objectstore.VolatileStore;
import com.arjuna.common.internal.util.propertyservice.BeanPopulator;
import java.util.HashMap;
import java.util.Map;
import javax.transaction.Status;
import javax.transaction.TransactionManager;
import javax.transaction.UserTransaction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyException;

/***
 * Manage JTA transaction. You can store some information during execution.
 * Each thread have only one transaction. To get current transaction use
 * {@link WikittyTransaction#get()} method
 */
public class WikittyTransaction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyTransaction.class);

    /** permet d'attacher n'importe quoi a une transaction */
    protected Map<Object, Object> tagValues;

    /** if true begin has been called for this transaction */
    protected boolean started = false;

    private static ThreadLocal<WikittyTransaction> wikittyTransaction =
            new ThreadLocal<WikittyTransaction>() {
        @Override
        protected synchronized WikittyTransaction initialValue() {
            return new WikittyTransaction();
        }
    };

    private WikittyTransaction() {
        // pour regler le probleme de repertoire ObjectStore qui apparaisse
        // en conjonction avec le fichier de config jbossts-properties.xml
        // voir:
        // - http://docs.redhat.com/docs/en-US/JBoss_Enterprise_Application_Platform/4.2/html/JBoss_Transactions_Programmers_Guide/ch07.html
        // - https://issues.jboss.org/browse/JBTM-852
        BeanPopulator.getNamedInstance(
                ObjectStoreEnvironmentBean.class, "communicationStore")
                .setObjectStoreType(VolatileStore.class.getName());

        tagValues = new HashMap<Object, Object>();
    }

    /**
     * return current transaction used by current thread. If no transaction
     * existe, create new one.
     * YOU MUST COMMIT OR ROLLBACK TRANSACTION AFTER USE
     * @return
     */
    static public WikittyTransaction get() {
        return wikittyTransaction.get();
    }

    public Object getTagValue(Object tag) {
        return tagValues.get(tag);
    }

    public void setTagValue(Object tag, Object value) {
        tagValues.put(tag, value);
    }

    public boolean isStarted() {
        return started;
    }

    protected void setStarted(boolean started) {
        this.started = started;
    }

    public UserTransaction getUserTransaction() {
        return com.arjuna.ats.jta.UserTransaction.userTransaction();
    }

    public TransactionManager getTransactionManager() {
        return com.arjuna.ats.jta.TransactionManager.transactionManager();
    }

    public void begin() {
        if (isStarted()) {
            throw new WikittyException("Transaction is already started");
        }
        UserTransaction userTransaction = getUserTransaction();
        try {
            if (log.isDebugEnabled()) {
                log.debug("Begin transaction");
            }
            userTransaction.setTransactionTimeout(3600); // FIXME: jru 20100115 error in service if call during lot of times
            userTransaction.begin();
            setStarted(true);
        } catch (Exception eee) {
            throw new WikittyException("Error on begin JTA transaction", eee);
        }
    }

    public void commit() {
        UserTransaction userTransaction = getUserTransaction();
        try {
            if (log.isDebugEnabled()) {
                log.debug("Commit transaction");
            }
            userTransaction.commit();
            setStarted(false);
        } catch (Exception eee) {
            throw new WikittyException("Error on commit JTA transaction", eee);
        }
    }

    public void rollback() {
        UserTransaction userTransaction = getUserTransaction();
        try {
            if(userTransaction.getStatus() != Status.STATUS_NO_TRANSACTION
                    && userTransaction.getStatus() != Status.STATUS_UNKNOWN) {
                if (log.isDebugEnabled()) {
                    log.debug("Rollback transaction");
                }
                userTransaction.rollback();
            }
            setStarted(false);
        } catch (Exception eee) {
            throw new WikittyException("Error on roolback JTA transaction", eee);
        }
    }
    

}
