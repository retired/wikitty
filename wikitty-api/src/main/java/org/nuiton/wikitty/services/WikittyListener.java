/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.services;

/**
 * Permet d'ajouter des listeners sur les methodes de modification de
 * WikittyService.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public interface WikittyListener {

    // this constant must be up to date with real method name
    // this is used during fire event
    final static public String PUT_WIKITTY_METHOD = "putWikitty";
    final static public String REMOVE_WIKITTY_METHOD = "removeWikitty";
    final static public String CLEAR_WIKITTY_METHOD = "clearWikitty";
    final static public String PUT_EXTENSION_METHOD = "putExtension";
    final static public String REMOVE_EXTENSION_METHOD = "removeExtension";
    final static public String CLEAR_EXTENSION_METHOD = "clearExtension";

    public void putWikitty(WikittyEvent event);
    public void removeWikitty(WikittyEvent event);
    public void clearWikitty(WikittyEvent event);

    /** toto[1.0] */
    public void putExtension(WikittyEvent event);
    public void removeExtension(WikittyEvent event);
    public void clearExtension(WikittyEvent event);
    
}
