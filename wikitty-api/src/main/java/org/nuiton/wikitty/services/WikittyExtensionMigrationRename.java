/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.FieldType;
import java.util.Collection;
import org.apache.commons.lang3.StringUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyService;

/**
 * Basic extension migration use to manage rename field name. Order to detect a
 * rename, not creation a new field, you should use tag value "renameFrom" with
 * name of use in before extension.
 *
 * @author ruchaud
 */
public class WikittyExtensionMigrationRename implements WikittyExtensionMigration {

    static private Log log = LogFactory.getLog(WikittyExtensionMigrationRename.class);

    public static final String TAG_RENAME = "renameFrom";

    @Override
    public Wikitty migrate(WikittyService service,
            Wikitty oldWikitty, Wikitty newWikitty,
            WikittyExtension oldExt, WikittyExtension newExt) {

        String extName = newExt.getName();

        // Migrate field name
        Collection<String> oldFieldNames = oldExt.getFieldNames();
        Collection<String> newFieldNames = newExt.getFieldNames();
        for (String fieldName : newFieldNames) {
            FieldType fieldType = newExt.getFieldType(fieldName);

            String renameFrom = fieldType.getTagValue(TAG_RENAME);
            log.debug("Scan rename migration on " + extName + "." + fieldName + ":" + TAG_RENAME + "=" + renameFrom);

            if(StringUtils.isNotBlank(renameFrom)) {
                Object value = oldWikitty.getFieldAsObject(extName, renameFrom);
                log.debug("Rename " + extName + "." + fieldName + "=" + value);
                newWikitty.setField(extName, fieldName, value);
            } else {
                if(oldFieldNames.contains(fieldName)) {
                    Object value = oldWikitty.getFieldAsObject(extName, fieldName);
                    log.debug("Copy " + extName + "." + fieldName + "=" + value);
                    newWikitty.setField(extName, fieldName, value);
                }
            }
        }

        return newWikitty;
    }

}
