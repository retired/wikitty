/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.services;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.nuiton.wikitty.entities.Wikitty;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.jcs.access.GroupCacheAccess;
import org.apache.jcs.access.exception.CacheException;
import org.apache.jcs.engine.control.CompositeCacheManager;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.entities.WikittyExtension;

/**
 * Implantation du cache base sur le projet apache JCS
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyCacheJCS implements WikittyCache {

    static private Log log = LogFactory.getLog(WikittyCacheJCS.class);

    protected Set<String> priorityExtensions = new HashSet<String>();

    /**
     * cache ou sont stocke les objects qui doivent rester le plus possible
     * en memoire
     */
    protected GroupCacheAccess priorityCache;
    /**
     * cache pour les autres wikitties
     */
    protected GroupCacheAccess wikittyCache;

    /**
     * cache pour les extensions
     */
    protected GroupCacheAccess extensionCache;

    /**
     *
     * @param config not used currently but necessary in futur to configure the cache
     * Create a soft cache.
     */
    public WikittyCacheJCS(ApplicationConfig config) {
        try {
            List<String> priorityExtensions = config.getOptionAsList(
                    WikittyConfigOption.WIKITTY_CACHE_PRIORITY_EXTENSIONS.getKey()).getOption();
            getPriorityExtensions().addAll(priorityExtensions);

            CompositeCacheManager cacheMgr = CompositeCacheManager.getUnconfiguredInstance();
            cacheMgr.configure(config.getFlatOptions());
            priorityCache = new GroupCacheAccess(cacheMgr.getCache("priority"));
            wikittyCache = new GroupCacheAccess(cacheMgr.getCache("wikitty"));
            extensionCache = new GroupCacheAccess(cacheMgr.getCache("extension"));
            
            // JCS ne permet pas de passer un Properties mais seulement un nom de fichier :(
//            JCS.setConfigFilename(config.getConfigFileName());
//            wikittyCache = JCS.getInstance("wikitty");
        } catch (Exception eee) {
            throw new WikittyException("Can't initialise JCS cache", eee);
        }
    }

    /**
     * Liste des extensions a conserver prioritairement
     * @return
     */
    public Set<String> getPriorityExtensions() {
        return priorityExtensions;
    }

    @Override
    public boolean existsWikitty(String id) {
        Object o = priorityCache.get(id);
        boolean result = (o != null);
        if (!result) {
            o = wikittyCache.get(id);
            result = (o != null);
        }

        return result;
    }

    /**
     * Return wikitty object if is in the cache, null otherwize.
     * 
     * @param id
     * @return wikitty object or null
     */
    @Override
    public Wikitty getWikitty(String id) {
        if (id == null) {
            return null;
        }
        Wikitty result = (Wikitty)priorityCache.get(id);
        if (result == null) {
            result = (Wikitty) wikittyCache.get(id);
        }
        return result;
    }

    /**
     * Only realy put wikitty in cache, if not in cache or version is newer than
     * one in cache
     * @param e
     */
    @Override
    public void putWikitty(Wikitty e) {
        if (e != null) {
            try {
                if (Collections.disjoint(priorityExtensions,e.getExtensionNames())) {
                    // le wikitty ne contient pas d'extension prioritaire
                    // on le met dans le cache commun
                    wikittyCache.put(e.getWikittyId(), e);
                } else {
                    priorityCache.put(e.getWikittyId(), e);
                }
            } catch (CacheException eee) {
                log.error(String.format("Can't put wikitty %s in cache", e), eee);
            }
        }
    }

    /**
     * Remove wikitty from cache.
     * 
     * @param id wikitty id to remove
     */
    @Override
    public void removeWikitty(String id) {
        try {
            priorityCache.remove(id);
            wikittyCache.remove(id);
        } catch (CacheException eee) {
            log.error(String.format("Can't remove wikitty %s in cache", id), eee);
        }
    }

    /**
     * Clear all cache.
     */
    @Override
    public void clearWikitty() {
        try {
            priorityCache.clear();
            wikittyCache.clear();
        } catch (CacheException eee) {
            log.error(String.format("Can't clear wikitty cache"), eee);
        }
    }

    @Override
    public boolean existsExtension(String id) {
        Object o = extensionCache.get(id);
        boolean result = (o != null);
        return result;
    }

    /**
     * Return wikitty object if is in the cache, null otherwise.
     *
     * @param extId extension id
     * @return wikitty object or null
     */
    @Override
    public WikittyExtension getExtension(String extId) {
        WikittyExtension result = (WikittyExtension)extensionCache.get(extId);
        return result;
    }

    /**
     * Only realy put wikitty in cache, if not in cache or version is newer than
     * one in cache
     * @param e
     */
    @Override
    public void putExtension(WikittyExtension e) {
        if (e != null) {
            try {
                extensionCache.put(e.getId(), e);
            } catch (CacheException eee) {
                log.error(String.format("Can't put extension %s in cache", e), eee);
            }
        }
    }

    /**
     * Remove wikitty from cache.
     *
     * @param extId wikitty id to remove
     */
    @Override
    public void removeExtension(String extId) {
        try {
            extensionCache.remove(extId);
            
            if (log.isDebugEnabled()) {
                log.debug("Remove extension from JCS cache " + extId);
            }
        } catch (CacheException eee) {
            log.error(String.format("Can't remove extensions %s in cache", extId), eee);
        }
    }

    /**
     * Clear all cache.
     */
    @Override
    public void clearExtension() {
        try {
            extensionCache.clear();
        } catch (CacheException eee) {
            log.error(String.format("Can't clear extension cache"), eee);
        }
    }
}
