/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.services;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.ListenerSet;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.WikittyService;

/**
 * Wikitty service notifier.
 * 
 * Currently based on jgroups.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class WikittyServiceNotifier extends WikittyServiceDelegator {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceNotifier.class);

    /** Wikitty service listener (all event). */
    protected ListenerSet<WikittyListener> allWikittyServiceListeners;

    /** Wikitty service listener (only for local event). */
    protected ListenerSet<WikittyListener> localWikittyServiceListeners;

    /** Wikitty service listener (only for remote event). */
    protected ListenerSet<WikittyListener> remoteWikittyServiceListeners;

    /** notifier */
    protected WikittyListener notifier;

    /**
     * Tous les events en attentent d'etre envoyer aux listeners
     */
    protected LinkedBlockingQueue<WikittyEvent> eventToSend;

    /** thread utilise pour evoyer les events */
    protected EventThread eventThread;

    /**
     * Constructor with configuration.
     *
     * @param config config to use
     * @param ws delegate service
     * @param transporter transporter to use for remote event (listen or propagate).
     * this transporter can be null if we don't want propagate or listen remote
     * event
     */
    public WikittyServiceNotifier(ApplicationConfig config,
            WikittyService ws, RemoteNotifierTransporter transporter) {
        super(ws);

        // listeners
        allWikittyServiceListeners = new ListenerSet<WikittyListener>();
        localWikittyServiceListeners = new ListenerSet<WikittyListener>();
        remoteWikittyServiceListeners = new ListenerSet<WikittyListener>();

        eventToSend = new LinkedBlockingQueue<WikittyEvent>();
        
        eventThread = new EventThread(eventToSend,
                allWikittyServiceListeners, localWikittyServiceListeners,
                remoteWikittyServiceListeners);

        if (transporter != null) {
            transporter.setWikittyServiceNotifier(this);
            notifier = new EventPropagator(config, this, transporter);

            // FIX poussin 201O1126 remplacement du ALL par LOCAL
            // sinon on renvoie des events qui nous ont ete envoyes :(
            // ca risque de boucler :(
            this.addWikittyServiceListener(notifier, WikittyService.ServiceListenerType.LOCAL); // weak reference
        }
        if (log.isInfoEnabled()) {
            if (transporter == null) {
                log.info("RemoteNotifier synchronisation not used ");
            } else {
                log.info("RemoteNotifier transporter: " + transporter.getClass().getName());
            }
        }
    }

    @Override
    public void addWikittyServiceListener(WikittyListener listener, ServiceListenerType type) {
        // not delegated
        switch (type) {
            case ALL :
                synchronized(allWikittyServiceListeners) {
                    allWikittyServiceListeners.add(listener);
                }
                break;
            case LOCAL :
                synchronized(localWikittyServiceListeners) {
                    localWikittyServiceListeners.add(listener);
                }
                break;
            case REMOTE :
                synchronized(remoteWikittyServiceListeners) {
                    remoteWikittyServiceListeners.add(listener);
                }
                break;
        }
    }

    @Override
    public void removeWikittyServiceListener(WikittyListener listener, ServiceListenerType type) {
        // not delegated
        switch (type) {
            case ALL :
                synchronized(allWikittyServiceListeners) {
                    allWikittyServiceListeners.remove(listener);
                }
                break;
            case LOCAL :
                synchronized (localWikittyServiceListeners) {
                    localWikittyServiceListeners.remove(listener);
                }
                break;
            case REMOTE :
                synchronized(remoteWikittyServiceListeners) {
                    remoteWikittyServiceListeners.remove(listener);
                }
                break;
        }
    }

    @Override
    public WikittyEvent clear(String securityToken) {
        WikittyEvent result = getDelegate().clear(securityToken);
        fireEvent(result);
        return result;
    }

    @Override
    public WikittyEvent store(String securityToken,
            Collection<Wikitty> wikitties, boolean force) {
        WikittyEvent result = getDelegate().store(securityToken, wikitties, force);

        // notify listeners
        fireEvent(result);
        return result;
    }

    @Override
    public WikittyEvent storeExtension(String securityToken,
            Collection<WikittyExtension> exts) {
        WikittyEvent result = getDelegate().storeExtension(securityToken, exts);
        fireEvent(result);
        return result;
    }

    @Override
    public WikittyEvent deleteExtension(
            String securityToken, Collection<String> extNames) {
        WikittyEvent result = getDelegate().deleteExtension(securityToken, extNames);
        fireEvent(result);
        return result;
    }

    @Override
    public WikittyEvent delete(String securityToken, Collection<String> ids) {
        WikittyEvent result = getDelegate().delete(securityToken, ids);
        // notify listeners
        fireEvent(result);
        return result;
    }

    @Override
    public WikittyEvent deleteTree(String securityToken, String wikittyId) {
        WikittyEvent result = getDelegate().deleteTree(securityToken, wikittyId);
        fireEvent(result);
        return result;
    }

    @Override
    public WikittyEvent replay(
            String securityToken, List<WikittyEvent> events, boolean force) {
        WikittyEvent result = getDelegate().replay(securityToken, events, force);
        // notify listeners
        fireEvent(result);
        return result;
    }

    /**
     * Fire event to all registred listener.
     *
     * Take care about {@link WikittyEvent#isRemote()} for fire.
     *
     * @param event event to fire
     */
    protected void fireEvent(final WikittyEvent event) {
        // ajout d'un thread, car si les listeners doivent
        // ouvrir une transaction WikittyTransaction
        // alors que celui qui lance l'event en a une ouverte
        // cela cause une exception JTA
        EventThread thread = getEventThread();

        // si le thread de notification est en cours d'arret on leve une exception
        if (thread.stopAsked()) {
            throw new WikittyException(
                    "Event thread dispatcher is stopped, no more event can be send");
        } else {
            eventToSend.offer(event);

            if (!thread.isAlive()) {
                // on demarre le thread que lorsqu'il y a le premier event d'arrive
                thread.start();
            }
        }
    }

    /**
     * fire event passed in argument. Before fire, change source to current
     * WikittyServiceNotifier and set remote event to true.
     */
    public void processRemoteEvent(WikittyEvent event) {
         //source is transient, add it here :
        event.setSource(this);
        event.setRemote(true); // received event became remote
        fireEvent(event);
    }

    /**
     * Retourne le dernier thread utiliser pour envoyer les events.
     *
     * @return
     */
    public EventThread getEventThread() {
        return eventThread;
    }

    @Override
    protected void finalize() throws Throwable {
        getEventThread().askStop();
        super.finalize();
    }

    /**
     * Thread utilise pour envoyer les events. On rend accessible ce thread
     * pour pouvoir y acceder depuis l'exterieur (pour l'instant pour les tests
     * mais peut-etre plus tard du monitoring). Il permet a un thread d'attendre
     * qu'un evenement leve a une certaine heure est bien ete dispatchee grace a
     * la methode waitfor
     */
    static public class EventThread extends Thread {

        protected boolean mustBeRunning = true;
        
        protected SortedMap<Long, Object> waiter = new TreeMap<Long, Object>();
        /**
         * reference vers la collection qui contient les events a envoyer
         */
        protected LinkedBlockingQueue<WikittyEvent> eventToSend;

        /** Wikitty service listener (all event). */
        protected ListenerSet<WikittyListener> allWikittyServiceListeners;

        /** Wikitty service listener (only for local event). */
        protected ListenerSet<WikittyListener> localWikittyServiceListeners;

        /** Wikitty service listener (only for remote event). */
        protected ListenerSet<WikittyListener> remoteWikittyServiceListeners;

        /** heure du dernier event envoye */
        protected long lastEventTime = 0;

        public EventThread(LinkedBlockingQueue<WikittyEvent> eventToSend,
                ListenerSet<WikittyListener> allWikittyServiceListeners,
                ListenerSet<WikittyListener> localWikittyServiceListeners,
                ListenerSet<WikittyListener> remoteWikittyServiceListeners) {
            super("wikitty-event-thread");
            this.eventToSend = eventToSend;
            this.allWikittyServiceListeners = allWikittyServiceListeners;
            this.localWikittyServiceListeners = localWikittyServiceListeners;
            this.remoteWikittyServiceListeners = remoteWikittyServiceListeners;
        }
        
        /**
         * demande l'arret du thread, ne doit être appeler que par le finalize
         * du WikittyServiceNotifier
         */
        protected void askStop() {
            this.mustBeRunning = false;
        }

        /**
         * retourne vrai si on a demande l'arret du thread
         * @return
         */
        public boolean stopAsked() {
            return !mustBeRunning;
        }

        /**
         * thread that want wait for particulare event to be processed, can be 
         * call this method with event time in argument. Used only in unit test
         * but this is necessary for test.
         */
        public void waitFor(long eventTime) throws InterruptedException {
            Object mutex = null;
            sleep(1); // sleep 1 millis to prevent problem with 2 events in same millis
            synchronized (waiter) {
                if (eventTime <= lastEventTime) {
                    // le thread demande a attendre un event deja passe
                    // on le met donc pas en attente
                    if (log.isDebugEnabled()) {
                        log.debug("event deja passe " + eventTime + " <= " + lastEventTime);
                    }
                    return;
                }
                mutex = waiter.get(eventTime);
                if (mutex == null) {
                    mutex = new Object();
                    waiter.put(eventTime, mutex);
                }
            }
            synchronized(mutex) {
                mutex.wait();
            }
        }

        @Override
        public void run() {
            while(mustBeRunning) {
                processEventQueue();
            }
            // le thread est arrete, force l'envoi de tous les events pour
            // liberer correctement tous les threads en attente
            // plus aucun event ne doit etre accepte dans la queue (voir method fireEvent)
            processEventQueue();
        }

        protected void processEventQueue() {
            try {
                WikittyEvent event;
                // on attend pas indefiniment un event, car il faut verifier
                // aussi que personne n'a arrete le thread
                while (null != (event = eventToSend.poll(5, TimeUnit.SECONDS))) {
                    try {
                        synchronized (allWikittyServiceListeners) {
                            for(WikittyEvent.WikittyEventType type : event.getType()) {
                                allWikittyServiceListeners.fire(
                                        type.listenerMethodName, event);
                            }
                        }
                    } catch (Exception eee) {
                        log.error("Can't notify listener", eee);
                    }
                    try {
                        if (event.isRemote()) {
                            synchronized (remoteWikittyServiceListeners) {
                                for (WikittyEvent.WikittyEventType type : event.getType()) {
                                    remoteWikittyServiceListeners.fire(
                                            type.listenerMethodName, event);
                                }
                            }
                        } else {
                            synchronized (localWikittyServiceListeners) {
                                for (WikittyEvent.WikittyEventType type : event.getType()) {
                                    localWikittyServiceListeners.fire(
                                            type.listenerMethodName, event);
                                }
                            }
                        }
                    } catch (Exception eee) {
                        log.error("Can't notify listener", eee);
                    }
                    synchronized (waiter) {
                        // on met a jour l'heure du dernier event envoye
                        lastEventTime = event.getTime();

                        // on previent les threads en attente si besoin

                        // dans un premier temps on ne recupere que ceux
                        // inferieur a event.getTime()
                        SortedMap<Long, Object> subwaiter =
                                waiter.headMap(event.getTime());
                        for (Iterator<Map.Entry<Long, Object>> i = subwaiter.entrySet().iterator(); i.hasNext();) {
                            Object mutex = i.next().getValue();
                            i.remove();
                            synchronized (mutex) {
                                mutex.notifyAll();
                            }
                        }
                        // dans un second temps on verifie si le suivant ne
                        // serait pas egal a event.getTime()
                        if (!waiter.isEmpty()) {
                            Long time = waiter.firstKey();
                            // il pourrait y avoir plusieurs event avec la meme heure
                            // il faut bien tous les liberer
                            while (time.equals(event.getTime())) {
                                // il est bien egal on l'enleve aussi
                                Object mutex = waiter.remove(time);
                                synchronized (mutex) {
                                    mutex.notifyAll();
                                }

                                if (!waiter.isEmpty()) {
                                    time = waiter.firstKey();
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                }
            } catch (InterruptedException eee) {
                log.error("Notification thread error", eee);
            }
        }
    };

    /**
     * This interface must be implemented to send and received remote message.
     * Only sendMessage method is in interface but you must write receive
     * method too, but this method is protocol specific and can't appear in
     * interface
     */
    static public interface RemoteNotifierTransporter {

        /**
         * this method must be call before RemoteNotifierTransporter utilisation
         * to indicate which service use it
         * @param ws
         */
        public void setWikittyServiceNotifier(WikittyServiceNotifier ws);
        
        /**
         * Send a jgroup message to all other channel member.
         *
         * @param event message to send
         */
        public void sendMessage(WikittyEvent event) throws Exception;
    }

    /**
     * Class used to notify remote listener. This class is realy activate
     * only if wikitty.notifier.transporter.class configuration is found and
     * wikitty.service.event.propagateEvent is true
     */
    static public class EventPropagator implements WikittyListener {

        /** to use log facility, just put in your code: log.info(\"...\"); */
        static private Log log = LogFactory.getLog(EventPropagator.class);

        /** Notifier service reference reference. */
        protected WikittyServiceNotifier ws;

        protected RemoteNotifierTransporter transporter;

        public EventPropagator(ApplicationConfig config,
                WikittyServiceNotifier ws, RemoteNotifierTransporter transporter) {
            this.ws = ws;
            this.transporter = transporter;
        }

        /**
         * Send a jgroup message to all other channel member.
         *
         * @param event message to send
         */
        protected void sendMessage(WikittyEvent event) {
            try {
                if (log.isDebugEnabled()) {
                    log.debug("Try to send message : " + event);
                }

                transporter.sendMessage(event);

                if (log.isDebugEnabled()) {
                    log.debug("Message is sent : " + event);
                }
            } catch (Exception eee) {
                if (log.isErrorEnabled()) {
                    log.error("Can't send message", eee);
                }
            }
        }

        /*
         * @see org.nuiton.wikitty.WikittyListener#putWikitty(org.nuiton.wikitty.Wikitty[])
         */
        @Override
        public void putWikitty(WikittyEvent event) {
            sendMessage(event);
        }

        /*
         * @see org.nuiton.wikitty.WikittyListener#removeWikitty(java.lang.String[])
         */
        @Override
        public void removeWikitty(WikittyEvent event) {
            sendMessage(event);
        }

        /*
         * @see org.nuiton.wikitty.WikittyListener#clearWikitty()
         */
        @Override
        public void clearWikitty(WikittyEvent event) {
            sendMessage(event);
        }

        /*
         * @see org.nuiton.wikitty.WikittyListener#putExtension(org.nuiton.wikitty.WikittyExtension[])
         */
        @Override
        public void putExtension(WikittyEvent event) {
            sendMessage(event);
        }

        @Override
        public void removeExtension(WikittyEvent event) {
            sendMessage(event);
        }

        /*
         * @see org.nuiton.wikitty.WikittyListener#clearExtension()
         */
        @Override
        public void clearExtension(WikittyEvent event) {
            sendMessage(event);
        }
    }
}
