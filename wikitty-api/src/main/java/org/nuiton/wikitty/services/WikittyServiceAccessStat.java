/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.entities.BusinessEntity;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyAccessStatHelper;
import org.nuiton.wikitty.entities.WikittyAccessStatImpl;
import org.nuiton.wikitty.entities.WikittyTokenHelper;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceAccessStat extends WikittyServiceDelegator {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceAccessStat.class);

    static public interface AccessStatStorage {
        /**
         * @param ws service that ask stockage
         * @param securityToken 
         * @param stats WikittyAccessStat wikitty
         */
        public void store(WikittyServiceAccessStat ws, String securityToken,
                Collection<Wikitty> stats);
    }

    /** store access in wikitty */
    static public class AccessStatStorageWikitty implements AccessStatStorage {
        @Override
        public void store(WikittyServiceAccessStat ws, String securityToken,
                Collection<Wikitty> stats) {
            ws.getDelegate().store(securityToken, stats, false);
        }
    }
    /** store access via common-logging, you must configure it to store
     "AccessStat" logger correctly for info level */
    static public class AccessStatStorageLog implements AccessStatStorage {
        /** to use log facility, just put in your code: log.info(\"...\"); */
        static private Log log = LogFactory.getLog("AccessStat");
        @Override
        public void store(WikittyServiceAccessStat ws, String securityToken,
                Collection<Wikitty> stats) {
            for(Wikitty w : stats) {
                Date date = WikittyAccessStatHelper.getDate(w);
                String token = WikittyAccessStatHelper.getToken(w);
                String user = WikittyAccessStatHelper.getUser(w);
                String restored = WikittyAccessStatHelper.getRestored(w);
                log.info(String.format("[%1$tF %1$tT] in session '%2$s' user '%3$s' restore '%4$s'", date, token, user, restored));
            }
        }
    }

    protected AccessStatStorage statStorage;
    protected Set<String> extensions = new HashSet<String>();

    public WikittyServiceAccessStat(ApplicationConfig config, WikittyService service,
            AccessStatStorage statStorage) {
        super(service);
        this.statStorage = statStorage;
        if (statStorage == null) {
            if (log.isWarnEnabled()) {
                String statSto = config.getOption(WikittyConfigOption.
                    WIKITTY_WIKITTYSERVICEACCESSSTAT_COMPONENTS.getKey());
                log.warn(String.format(
                        "No AccessStatStorage available, access stat can't work (%s)",
                        statSto));
            }
        } else {
            // if no statStorage, make empty extensions to never call statStorage
            List<String> exts = config.getOptionAsList(WikittyConfigOption.
                    WIKITTY_ACCESSSTAT_EXTENSIONS.getKey()).getOption();
            if (log.isInfoEnabled()) {
                log.info(String.format("Monitor access to extensions %s", exts));
            }
            extensions.addAll(exts);
        }
    }

    /**
     * retourne l'id du user associe au token
     * @param securityToken
     * @return null si l'id du user n'a pas pu etre recuperer
     */
    protected String getUserId(String securityToken) {
        String result = null;
        // recuperation de l'utilisateur associe au securityToken
        // le securityToken est aussi l'id de l'objet
        if (securityToken != null) {
            Wikitty securityTokenWikitty = WikittyServiceEnhanced.restore(
                    getDelegate(), securityToken, securityToken);
            if (securityTokenWikitty == null) {
                // no exception, this service must never faild
                log.warn("bad (obsolete ?) token");
            } else {
                result = WikittyTokenHelper.getUser(securityTokenWikitty);
            }
        }
        return result;
    }

    /**
     * Indique si dans la liste des extensions passees en parametre il y en
     * a au moins une a surveiller
     *
     * @param exts
     * @return vrai s'il y a au moins une extension a surveiller
     */
    protected boolean isMonitored(Collection<String> exts) {
        boolean result = false;
        for (String ext : exts) {
            result = extensions.contains(ext);
            if (result) {
                break;
            }
        }
        return result;
    }

    /**
     * Add WikittyAccessStat in storage if necessary, one for each object
     *
     * @param securityToken
     * @param wikitties
     */
    protected void addStat(String securityToken, Collection wikitties) {
        if (statStorage == null) {
            return;
        }
        boolean userLoaded = false;
        String user = null;

        List<Wikitty> stats = new ArrayList<Wikitty>(wikitties.size());
        for (Object o : wikitties) {
            String id = null;
            Collection<String> exts = null;
            if (o instanceof Wikitty) {
                Wikitty w = ((Wikitty)o);
                id = w.getWikittyId();
                exts = w.getExtensionNames();
            } else if (o instanceof BusinessEntity) {
                BusinessEntity e = ((BusinessEntity) o);
                id = e.getWikittyId();
                exts = e.getExtensionNames();
            }

            if (exts != null && isMonitored(exts)) {
                // on recupere le user que maintenant car potentiellement il n'y a
                // rien a creer si aucun objet n'est dans la liste des extensions
                // a surveiller
                if (!userLoaded) {
                    user = getUserId(securityToken);
                    userLoaded = true;
                }

                WikittyAccessStatImpl stat = new WikittyAccessStatImpl();
                stat.setDate(new Date());
                stat.setToken(securityToken);
                stat.setUser(user);
                stat.setRestored(id);

                stats.add(stat.getWikitty());
            }
        }
        if (stats.size() > 0 ) {
            statStorage.store(this, securityToken, stats);
        }
    }

    @Override
    public List<Wikitty> restore(String securityToken, List<String> id) {
        List<Wikitty> result = super.restore(securityToken, id);
        addStat(securityToken, result);
        return result;
    }

    @Override
    public Wikitty restoreVersion(String securityToken, String wikittyId, String version) {
        Wikitty result = super.restoreVersion(securityToken, wikittyId, version);
        addStat(securityToken, Collections.singleton(result));
        return result;
    }

}
