/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.services;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyAuthorisation;
import org.nuiton.wikitty.entities.WikittyAuthorisationHelper;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyGroup;
import org.nuiton.wikitty.entities.WikittyGroupImpl;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyMetaExtensionUtil;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.entities.WikittyUser;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.search.Search;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittySecurityHelper {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittySecurityHelper.class);

    /** nom du groupe des administrateurs de l'application */
    static final public String WIKITTY_APPADMIN_GROUP_NAME = "WikittyAppAdmin";

    /**
     * get the id of a user given his login.
     *
     * @param login the login of the user to search for
     * @return a wikitty id
     * @deprecated since 3.4 {@link #getUserWikittyId(org.nuiton.wikitty.WikittyClient, java.lang.String) }
     */
    @Deprecated
    static public String getUserWikittyId(WikittyProxy proxy, String login) {
        String userWikittyId = null;
        Wikitty user = proxy.findByCriteria(Search.query().eq(
                WikittyUser.FQ_FIELD_WIKITTYUSER_LOGIN, login).criteria());
        if (user != null) {
            userWikittyId = user.getWikittyId();
        }
        return userWikittyId;
    }

    /**
     * get the id of a user given his login.
     *
     * @param login the login of the user to search for
     * @return a wikitty id
     */
    static public String getUserWikittyId(WikittyClient client, String login) {
        WikittyQuery query = new WikittyQueryMaker().eq(
                WikittyUser.FQ_FIELD_WIKITTYUSER_LOGIN, login).end();
        String userWikittyId = client.findByQuery(String.class, query);
        return userWikittyId;
    }

    /**
     * create appAdminGroup and add current user as first member
     *
     * @deprecated Use #WikittySecurityUtil.createAppAdminGroup
     */
    @Deprecated
    static public WikittyGroup createAppAdminGroup(WikittyUser user) {
        WikittyGroup result = new WikittyGroupImpl();
        result.setName(WIKITTY_APPADMIN_GROUP_NAME);

        String firstUserId = user.getWikittyId();
        result.addMembers(firstUserId);

        return result;
    }

    /**
     * create wikitty that represent a <strong>level 2</strong> security policy
     * on the given extension.
     * 
     * Store must check if this security policy doesn't already exist
     *
     */
    static public Wikitty createExtensionAuthorisation(WikittyUser owner,
                                        WikittyExtension extension) {

        String wikittyAuthorisationId = WikittyMetaExtensionUtil.generateId(
                WikittyAuthorisation.EXT_WIKITTYAUTHORISATION, extension.getName());
        Wikitty result = new WikittyImpl(wikittyAuthorisationId);
        WikittyAuthorisationHelper.addExtension(result);
        WikittyAuthorisationHelper.setOwner(result, owner.getWikittyId());
        return result;
    }

    /**
     * 
     * @param proxy
     * @param extension
     * @return
     * @deprecated since 3.4 use {@link #restoreExtensionAuthorisation(org.nuiton.wikitty.WikittyClient, org.nuiton.wikitty.entities.WikittyExtension) }
     */
    @Deprecated
    static public Wikitty restoreExtensionAuthorisation(
            WikittyProxy proxy, WikittyExtension extension) {
        String wikittyAuthorisationId = WikittyMetaExtensionUtil.generateId(
                WikittyAuthorisation.EXT_WIKITTYAUTHORISATION, extension.getName());
        Wikitty result = proxy.restore(wikittyAuthorisationId);
        return result;
    }

    static public Wikitty restoreExtensionAuthorisation(
            WikittyClient client, WikittyExtension extension) {
        String wikittyAuthorisationId = WikittyMetaExtensionUtil.generateId(
                WikittyAuthorisation.EXT_WIKITTYAUTHORISATION, extension.getName());
        Wikitty result = client.restore(wikittyAuthorisationId);
        return result;
    }

}
