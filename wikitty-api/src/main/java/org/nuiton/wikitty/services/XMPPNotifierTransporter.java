/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyUtil;

/**
 * Transporter d'event via xmpp. Pour que ca fonctionne il faut un serveur
 * xmpp avec une room
 * <p>
 * Configuration
 * <ul>
 * <li>wikitty.xmpp.server = adresse du serveur (ex: im.codelutin.com)</li>
 * <li>wikitty.xmpp.room = adresse de la room (ex: wikitty-event@im.codelutin.com)</li>
 * </ul>
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class XMPPNotifierTransporter implements PacketListener,
        WikittyServiceNotifier.RemoteNotifierTransporter {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(XMPPNotifierTransporter.class);

    static final public String PROPERTY_EVENT_NAME = "wikitty-event";

    /** Indique si les events sont propages (true) via le transporter  */
    protected boolean propagateEvent = false;
    /** Indique si les events sont ecoutes (true) via le transporter  */
    protected boolean listenEvent = false;

    /** Notifier service reference reference. */
    protected WikittyServiceNotifier ws;

    // Keep room addresse and pseudo to verify that messages event are not send by us
    protected String room;
    protected String pseudo;

    protected XMPPConnection connection;
    protected MultiUserChat muc;

    /**
     * 
     * @param config
     */
    public XMPPNotifierTransporter(ApplicationConfig config) {
        propagateEvent = config.getOptionAsBoolean(WikittyConfigOption.
                WIKITTY_EVENT_PROPAGATE.getKey());
        listenEvent = config.getOptionAsBoolean(WikittyConfigOption.
                WIKITTY_EVENT_LISTEN.getKey());

        initXMPP(config);
    }

    public WikittyServiceNotifier getWikittyServiceNotifier() {
        return ws;
    }

    @Override
    public void setWikittyServiceNotifier(WikittyServiceNotifier ws) {
        this.ws = ws;
    }

    /**
     * Si persistent est vrai alors il faut toujours utilise le meme user id
     *
     * @param config
     */
    protected void initXMPP(ApplicationConfig config) {
        // on verifie qu'on a besoin reellement de faire l'init
        if (propagateEvent || listenEvent) {
            String server = config.getOption(
                    WikittyConfigOption.WIKITTY_EVENT_TRANSPORTER_XMPP_SERVER.getKey());

            // Keep them to verify that is not us notifications
            room = config.getOption(WikittyConfigOption.
                    WIKITTY_EVENT_TRANSPORTER_XMPP_ROOM.getKey());
            pseudo = WikittyUtil.getUniqueLoginName();
            try {
                if (log.isInfoEnabled()) {
                    log.info("Try to connect to xmpp serveur " + server
                            + " with pseudo " + pseudo + " in room " + room);
                }
                connection = new XMPPConnection(server);
                connection.connect();
                connection.loginAnonymously();

                DiscussionHistory history = new DiscussionHistory();
                history.setMaxChars(0);

                // connection to the volatile room
                muc = new MultiUserChat(connection, room);
                muc.join(pseudo, "", history, 4000);
                
                if (listenEvent) {
                    muc.addMessageListener(this);
                }
            } catch (Exception eee) {
                throw new WikittyException("Can't connect to xmpp serveur", eee);
            }
        }
    }

    @Override
    public void sendMessage(WikittyEvent event) throws Exception {
        if (propagateEvent) {
            Message message = muc.createMessage();
            message.setBody(event.getType().toString());
            message.setProperty(PROPERTY_EVENT_NAME, event);

            muc.sendMessage(message);
        }
    }

    /**
     * used for MUC message
     * @param packet
     */
    @Override
    public void processPacket(Packet packet) {

        // Dont listen own events
        String name = room + "/" + pseudo;
        if (!name.equals(packet.getFrom())) {

            Object event = packet.getProperty(PROPERTY_EVENT_NAME);

            if (log.isDebugEnabled()) {
                log.debug("Receive message : " + event);
            }

            if (event instanceof WikittyEvent) {
                ws.processRemoteEvent((WikittyEvent)event);
            }
        }
    }

}
