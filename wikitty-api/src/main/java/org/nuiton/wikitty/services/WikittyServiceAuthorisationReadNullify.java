/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.entities.Wikitty;

/**
 * Cette classe au lieu de lever une exception si l'utilisateur n'a pas le droit
 * de lire un object, le remplace par null.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceAuthorisationReadNullify extends WikittyServiceAuthorisation {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(WikittyServiceAuthorisationReadNullify.class);

    /**
     *
     * @param config
     * @param ws
     */
    public WikittyServiceAuthorisationReadNullify(ApplicationConfig config, WikittyService ws) {
        super(config, ws);
    }

    /**
     * default implementation throw an exception if read is not allowed
     * you can create sub class that return null, or other to replace
     * unreadable Wikitty.
     */
    protected Wikitty refuseUnauthorizedRead( String securityToken,
                                           String userId,
                                           Wikitty wikitty) {
        if (wikitty != null) {
            for (String extensionName : wikitty.getExtensionNames()) {
                if ( ! canRead(securityToken, userId, extensionName, wikitty)) {
                    return null;
                }
            }
        }
        return wikitty;
    }

}
