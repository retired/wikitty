/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin, Benjamin Poussin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.TimeLog;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.FieldTypeConstaintChecker;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyTreeNode;
import org.nuiton.wikitty.entities.WikittyTreeNodeHelper;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.query.WikittyQueryResultTreeNode;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;
import org.nuiton.wikitty.search.TreeNodeResult;
import org.nuiton.wikitty.storage.WikittyExtensionStorage;
import org.nuiton.wikitty.storage.WikittySearchEngine;
import org.nuiton.wikitty.storage.WikittyStorage;

/**
 * WikittyService is main service
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceStorage implements WikittyService {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(WikittyServiceStorage.class);
    final static private TimeLog timeLog = new TimeLog(WikittyServiceStorage.class);

    /** Default migration use to migrate a wikitty in last extension version */
    protected WikittyExtensionMigration defaultExtensionMigration =
            new WikittyExtensionMigrationRename();

    protected FieldTypeConstaintChecker constraintChecker =
            new FieldTypeConstaintChecker(this);

    protected ApplicationConfig config;
    protected WikittySearchEngine searchEngine;
    protected WikittyExtensionStorage extensionStorage;
    protected WikittyStorage wikittyStorage;

    /**
     * TODO poussin 20101027 remove it when all used WikittyServiceHelper.build
     *
     * Used by specific child
     * {@code org.nuiton.wikitty.storage.solr.WikittyServiceSolr}
     */
    protected WikittyServiceStorage(ApplicationConfig config) {
        this.config = config;
    }

    public WikittyServiceStorage(ApplicationConfig config,
            WikittyExtensionStorage extensionStorage,
            WikittyStorage wikittyStorage,
            WikittySearchEngine searchEngine) {
        this.config = config;
        this.extensionStorage = extensionStorage;
        this.wikittyStorage = wikittyStorage;
        this.searchEngine = searchEngine;
    }

    public WikittySearchEngine getSearchEngine() {
        return searchEngine;
    }

    public WikittyExtensionStorage getExtensionStorage() {
        return extensionStorage;
    }

    public WikittyStorage getWikittyStorage() {
        return wikittyStorage;
    }

    /*
     * @see org.nuiton.wikitty.WikittyService#addWikittyServiceListener(org.nuiton.wikitty.WikittyListener, org.nuiton.wikitty.WikittyService.ServiceListenerType)
     */
    @Override
    public void addWikittyServiceListener(WikittyListener listener, ServiceListenerType type) {
        throw new UnsupportedOperationException("Can't add listener on " + WikittyServiceStorage.class.getName());
    }

    /*
     * @see org.nuiton.wikitty.WikittyService#removeWikittyServiceListener(org.nuiton.wikitty.WikittyListener, org.nuiton.wikitty.WikittyService.ServiceListenerType)
     */
    @Override
    public void removeWikittyServiceListener(WikittyListener listener, ServiceListenerType type) {
        throw new UnsupportedOperationException("Can't remove listener on " + WikittyServiceStorage.class.getName());
    }

    @Override
    public String login(String login, String password) {
        log.warn("login asked, but there is no security service");
        return null;
    }

    @Override
    public void logout(String securityToken) {
        log.warn("logout asked, but there is no security service");
    }

    @Override
    public String getToken(String user) {
        log.warn("getToken asked, but there is no security service");
        return null;
    }

    @Override
    public boolean canWrite(String securityToken, Wikitty wikitty) {
        return true;
    }

    @Override
    public boolean canDelete(String securityToken, String wikittyId) {
        return true;
    }

    @Override
    public boolean canRead(String securityToken, String wikittyId) {
        return true;
    }

    /**
     * Check les contraintes sur les champs:
     * <ul>
     * <li> notNull: exception</li>
     * <li> pattern: exception</li>
     * <li> extensionAllowed: exception</li>
     * <li> default: changement de la valeur null par default</li>
     * </ul>
     *
     * @param wikitties
     */
    protected void checkConstraint(Collection<Wikitty> wikitties) {
        for(Wikitty w : wikitties) {
            for(WikittyExtension ext : w.getExtensions()) {
                for (String fieldName : ext.getFieldNames()) {
                    String fqfield = WikittyUtil.getFQFieldName(ext.getName(), fieldName);
                    FieldType type = ext.getFieldType(fieldName);
                    Object value = w.getFqField(fqfield);

                    List<String> errors = new LinkedList<String>();

                    value = constraintChecker.checkDefault(w, fqfield, type, value);

                    if (!constraintChecker.isValid(fqfield, type, value, errors)) {
                        throw new WikittyException(String.format(
                                "Field constraint error %s",
                                StringUtils.join(errors, "\n")));

                    }
                }
            }
        }
    }

    @Override
    public WikittyEvent store(String securityToken,
            Collection<Wikitty> wikitties, boolean force) {

        // use all time Set to prevent duplicated wikitty in collection
        wikitties = new LinkedHashSet<Wikitty>(wikitties);

        // suppress null wikitty
        wikitties.removeAll(Collections.singletonList(null));

        checkConstraint(wikitties);

        // update/store extension if necessary
        Set<WikittyExtension> allExtensions = new LinkedHashSet<WikittyExtension>();
        for (Wikitty w : wikitties) {
            // collect all extensions used by all wikitties
            allExtensions.addAll(w.getExtensions());
        }

        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            // try to commit command
            WikittyEvent extUpdate =
                    getExtensionStorage().store(tx, allExtensions);
            WikittyEvent wikUpdate =
                    getWikittyStorage().store(tx, wikitties, force);
            getSearchEngine().store(tx, wikitties, force);

            WikittyEvent result = new WikittyEvent(this);
            // prepare update client response
            result.add(extUpdate);
            result.add(wikUpdate);

            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't store wikitty", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    @Override
    public List<String> getAllExtensionIds(String securityToken) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            List<String> result = getExtensionStorage().getAllExtensionIds(tx);

            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't retrieve all extension's ids", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    @Override
    public List<String> getAllExtensionsRequires(
            String securityToken, String extensionName) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            List<String> result = getExtensionStorage()
                    .getAllExtensionsRequires(tx, extensionName);

            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException(String.format(
                    "Can't retrieve all required extension for %s", extensionName), eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    /**
     * Check some constraint on extension
     * <ul>
     * <li> extension name (ex: '#--AAA' is invalide name)</li>
     * <li> extension version (ex: '-1.0' is invalide version)</li>
     * <li> extension field name (ex: '=na' is invalide name)</li>
     * </ul>
     *
     * @see WikittyUtil#extensionNamePattern
     * @see WikittyUtil#extensionFieldNamePattern
     * @param exts extentions
     */
    protected void checkExtension(Collection<WikittyExtension> exts) {
        for (WikittyExtension ext : exts) {
            // if extension version is invalide, raise exception
            if (WikittyUtil.versionGreaterThan("0", ext.getVersion())) {
                throw new WikittyException(String.format(
                        "Invalide extension version %s", ext.getVersion()));
            }

            // if extension name is invalide, raise exception
            if (!ext.getName().matches(WikittyUtil.extensionNamePattern)) {
                throw new WikittyException(String.format(
                        "Invalide extension name %s", ext.getName()));
            }

            // if field name is invalide, raise exception
            for(String fieldName : ext.getFieldNames()) {
                if (!fieldName.matches(WikittyUtil.extensionFieldNamePattern)) {
                    throw new WikittyException(String.format(
                        "Invalide extension field name '%s' for extension '%s'",
                        fieldName, ext.getName()));
                }
            }
        }
    }

    @Override
    public WikittyEvent storeExtension(String securityToken,
            Collection<WikittyExtension> exts) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            // check extension
            checkExtension(exts);
            WikittyEvent result =
                    getExtensionStorage().store(tx, exts);
            
            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't store extensions", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    @Override
    public WikittyEvent deleteExtension(
            String securityToken, Collection<String> extNames) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            // check that all extensions are not used
            for(String name : extNames) {
                // only name are stored in index, search only on name
                WikittyQuery query = new WikittyQueryMaker().exteq(name).end();
                query.setLimit(0); // result is not use, just numFound
                WikittyQueryResult<Map<String, Object>> wikittyWithExt = findAllByQuery(
                        securityToken, Collections.singletonList(query)).get(0);
                int numFound = wikittyWithExt.getTotalResult();
                if (numFound > 0) {
                    throw new WikittyException(String.format(
                            "Can't delete %s extension, this extension"
                            + " is in used by %s wikitty",
                            name, numFound));
                }
            }

            WikittyEvent result =
                    getExtensionStorage().delete(tx, extNames);

            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't delete extensions", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    @Override
    public WikittyExtension restoreExtension(
            String securityToken, String extensionId) {
         WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            //split the id to ensure that version is normalized
            String name = WikittyExtension.computeName(extensionId);
            String version = WikittyExtension.computeVersion(extensionId);

            WikittyExtension result = getExtensionStorage().restore(tx, name, version);

            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't restore extensions", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    @Override
    public WikittyExtension restoreExtensionLastVersion(
            String securityToken, String name) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            String version = getExtensionStorage().getLastVersion(tx, name);
            if (version == null) {
                if (txBeginHere) {
                    tx.commit();
                }
                return null;
            }


            WikittyExtension result = getExtensionStorage().restore(tx, name, version);

            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't restore extensions", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    @Override
    public List<WikittyExtension> restoreExtensionAndDependenciesLastVesion(
            String securityToken, Collection<String> extensionNames) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            List<WikittyExtension> result = new ArrayList<WikittyExtension>();

            for (String extName : extensionNames) {
                WikittyExtension ext = restoreExtensionLastVersion(
                        securityToken, extName);
                if (ext != null) {
                    // on recherche les dependances de cette extension ...
                    List<String> requires = ext.getRequires();
                    if (CollectionUtils.isNotEmpty(requires)) {
                        List<WikittyExtension> dependencies =
                                restoreExtensionAndDependenciesLastVesion(
                                securityToken, requires);
                        // ... et on les ajoute avant dans le resultat
                        result .addAll(dependencies);
                    }
                    result.add(ext);
                }
            }

            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't restore extensions", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    /**
     * restore one wikitty
     *
     * @param securityToken security token previously returned by login. If
     * securityToken is not valid, this method do nothing
     * @param id to restore
     * @return wikitty found
     */
    protected Wikitty restore(String securityToken, String id) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (id == null) {
                return null;
            }

            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            if (!getWikittyStorage().exists(tx, id)) {
                // object doesn't exist, we return null
                return null;
            }

            if (getWikittyStorage().isDeleted(tx, id)) {
                // object deleted, we return null
                return null;
            }
            Wikitty result = getWikittyStorage().restore(tx, id);
            if (result != null) {
                result = upgradeData(securityToken, result);
            }
            
            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't store extensions", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    @Override
    public List<Wikitty> restore(String securityToken, List<String> ids) {
        List<Wikitty> result = new ArrayList<Wikitty>();
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            for (String id : ids) {
                Wikitty w = restore(securityToken, id);
                // on l'ajoutde tout le temps, meme si w est nul lorsqu'il y a
                // une demande et qu'elle echoue on ajout
                // bien null, pour qu'il y ait une correspondance 1 pour 1
                // avec la demande
                result.add(w);
            }
            return result;
        } catch (WikittyException ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null) {
                tx.rollback();
            }
            throw new WikittyException("Can't restore wikitty", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    /**
     * Recursively add extension with requires ones (orderer).
     * 
     * @param extNames Extension name to load with required ones
     * @param exts collection to put all extension result
     * @return result argument
     */
    protected LinkedHashSet<WikittyExtension> getExtension(
            Collection<String> extNames, LinkedHashSet<WikittyExtension> exts) {
        for (String name : extNames) {
            WikittyExtension ext = restoreExtensionLastVersion(null, name);
            List<String> requires = ext.getRequires();
            if (CollectionUtils.isNotEmpty(requires)) {
                getExtension(requires, exts);
            }
            exts.add(ext);
        }
        return exts;
    }

    /**
     * Donne la classe de migration a utiliser pour une extension particuliere
     * @param extensionName le nom de l'extension dont on veut la classe de migration
     * si aucune classe de migration n'est enregistrer pour l'extension demande
     * la classe de migration par defaut est utilisee
     * @return
     */
    protected WikittyExtensionMigration getMigration(String extensionName) {
        WikittyExtensionMigration result = null;

        WikittyExtensionMigrationRegistry registry =
                config.getObject(WikittyExtensionMigrationRegistry.class);
        result = registry.get(extensionName);
        // @deprecated a supprimer lorsque ce sera supprime de l'api
        if (result == null) {
            result = WikittyExtensionMigration.migrationRegistry.get(extensionName);
        }

        if (result == null) {
            result = defaultExtensionMigration;
        }
        return result;
    }

    /**
     * Upgrade wikitty and saved it if a migration is done.
     * On passe directement de la version du wikitty a la derniere version connue
     * car une installation pourrait faire un upgrade de plusieurs version
     * et donc certaine version de l'extension pourrait ne pas exister en local
     *
     * La sauvegarde des objets migrer est necessaire au cas on le developpeur
     * qui a implanter la migration a besoin de creer de nouveau objet durant
     * la migration. Si on ne sauve pas l'objet initial, alors celui-ci pourrait
     * etre potentiellement migrer plusieurs fois et donc des objets annexes
     * seraient crees a chaque migration, au lieu d'etre crees une seul fois.
     *
     * @param securityToken security token previously returned by login. If
     * securityToken is not valid, this method do nothing
     * @param wikitty to upgrade
     * @return wikitty upgraded
     */
    protected Wikitty upgradeData(String securityToken, Wikitty wikitty) {
        Wikitty result = wikitty;

        LinkedHashSet<WikittyExtension> newExt = getExtension(wikitty.getExtensionNames(), new LinkedHashSet<WikittyExtension>());
        Collection diff = CollectionUtils.disjunction(wikitty.getExtensions(), newExt);
        if (!diff.isEmpty()) {
            // les extensions on change, il faut faire la migration
            // on commence par cree le nouveau wikitty avec les nouvelles extensions
            String wikittyId = wikitty.getWikittyId();
            String wikittyVersion = wikitty.getWikittyVersion();

            result = new WikittyImpl(wikittyId);
            result.setWikittyVersion(wikittyVersion);
            result.addExtension(newExt);

            // on migre ou copie extension par extension tous les champs
            Collection<WikittyExtension> extensions = wikitty.getExtensions();
            for (WikittyExtension extension : extensions) {
                String extensionName = extension.getName();

                if (log.isDebugEnabled()) {
                    log.debug("extensionName="  + extensionName);
                }

                WikittyExtension currentExtension = extension;
                String currentExtensionVersion = currentExtension.getVersion();

                WikittyExtension lastExtension = result.getExtension(extensionName);
                String lastExtensionVersion = lastExtension.getVersion();

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Migrate extension '%s' from '%s' to '%s'",
                            extensionName, currentExtensionVersion, lastExtensionVersion));
                }


                WikittyExtensionMigration migration = getMigration(extensionName);

                // no condition on extension, version, otherwize field is not copied in result, all extension must be migrated
                result = migration.migrate(this, wikitty, result,
                        currentExtension, lastExtension);
            }

            // if migration is done, store modified version and return it
            WikittyClient wc = new WikittyClient(config, this, securityToken);
            result = wc.store(result);
        }
        
        return result;
    }

    @Override
    public WikittyEvent delete(String securityToken,
            Collection<String> ids) throws WikittyException {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            // work only on valid id
            Collection<Wikitty> storedWikitties = new LinkedHashSet<Wikitty>();
            // copy ids because we can remove some element, and modify it
            // use set to prevent id duplication and preformance (contains method call)
            Set<String> idSet = new LinkedHashSet<String>(ids);
            for (Iterator<String> i = idSet.iterator(); i.hasNext();) {
                String id = i.next();
                // test if wikitty exists
                if (!getWikittyStorage().exists(tx, id)) {
                    // don't exist, remove this id in id list
                    i.remove();
                    // go to the next id, because this id doesn't exist and can't
                    // be used in tree
                    continue;
                }
                if (getWikittyStorage().isDeleted(tx, id)) {
                    // already deleted, remove this id in id list
                    i.remove();
                    // go to the next id, because this id already deleted and can't
                    // be used in tree
                    continue;
                }

                // Store node with have deleted node as parent
                WikittyQuery query =
                        new WikittyQueryMaker().eq(WikittyTreeNode.
                        FQ_FIELD_WIKITTYTREENODE_PARENT, id).end();
                List<String> wikittyNodesId = findAllByQuery(
                        securityToken, Collections.singletonList(query))
                        .get(0).convertMapToSimpleString().getAll();
                for (String wikittyNodeId : wikittyNodesId) {
                    if (!idSet.contains(wikittyNodeId)) {
                        Wikitty treeNode = restore(
                                securityToken, wikittyNodeId);
                        WikittyTreeNodeHelper.setParent(treeNode, (String)null);
                        storedWikitties.add(treeNode);
                    }
                }

                // Store node with have deleted child
                query = new WikittyQueryMaker().eq(WikittyTreeNode.
                        FQ_FIELD_WIKITTYTREENODE_ATTACHMENT, id).end();
                wikittyNodesId = findAllByQuery(
                        securityToken, Collections.singletonList(query))
                        .get(0).convertMapToSimpleString().getAll();
                for (String wikittyNodeId : wikittyNodesId) {
                    if (!idSet.contains(wikittyNodeId)) {
                        Wikitty treeNode = restore(
                                securityToken, wikittyNodeId);
                        WikittyTreeNodeHelper.removeAttachment(treeNode, id);
                        storedWikitties.add(treeNode);
                    }
                }
            }

            WikittyEvent eventDelete =
                    getWikittyStorage().delete(tx, idSet);
            getSearchEngine().delete(tx, idSet);

            WikittyEvent eventStore = store(securityToken, storedWikitties, false);

            WikittyEvent result = new WikittyEvent(this);
            result.add(eventDelete);
            result.add(eventStore);

            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't delete wikitty", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    /**
     * Use with caution : It will delete ALL data !
     * This operation should be disabled in production environment.
     */
    @Override
    public WikittyEvent clear(String securityToken) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            getSearchEngine().clear(tx);
            WikittyEvent eventWik = getWikittyStorage().clear(tx);
            WikittyEvent eventExt = getExtensionStorage().clear(tx);

            WikittyEvent result = new WikittyEvent(this);
            result.add(eventWik);
            result.add(eventExt);

            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't clear all data", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    /**
     * Assume that this PagedResult contains wikitty id as result and
     * return new PagedResult with Wikitty instance
     */
    @Override
    public List<PagedResult<String>> findAllByCriteria(
            String securityToken, List<Criteria> criteria) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            List<PagedResult<String>> result =
                    new ArrayList<PagedResult<String>>(criteria.size());
            for (Criteria c : criteria) {
                if (c == null) {
                    result.add(null);
                } else {
                    PagedResult<String> searchResult =
                            getSearchEngine().findAllByCriteria(tx, c);
                    result.add(searchResult);
                }
            }

            if (criteria.size() != result.size()) {
                log.error(String.format("Criteria input list (%s) has not same size that result list (%s)",
                        criteria.size(), result.size()));
            }
            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Error during find", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    @Override
    public List<String> findByCriteria(String securityToken, List<Criteria> criteria) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            List<String> result = new ArrayList<String>(criteria.size());
            List<Criteria> criteriaLimit = new ArrayList<Criteria>(criteria.size());
            for (Criteria c : criteria) {
                Criteria cLimit = Search.query(c).criteria()
                        .setFirstIndex(0).setEndIndex(1);
                criteriaLimit.add(cLimit);
            }

            List<PagedResult<String>> idsList =
                    findAllByCriteria(securityToken, criteriaLimit);

            for (PagedResult<String> ids : idsList) {
                if (ids.size() > 0) {
                    result.add(ids.getFirst());
                } else {
                    result.add(null);
                }
            }
            
            if (criteria.size() != result.size()) {
                log.error(String.format("Criteria input list (%s) has not same size that result list (%s)",
                        criteria.size(), result.size()));
            }
            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Error during find", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    /**
     * Assume that this PagedResult contains wikitty id as result and
     * return new PagedResult with Wikitty instance
     */
    @Override
    public List<WikittyQueryResult<Map<String, Object>>> findAllByQuery(
            String securityToken, List<WikittyQuery> queries) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            List<WikittyQueryResult<Map<String, Object>>> result =
                    new ArrayList<WikittyQueryResult<Map<String, Object>>>(queries.size());
            for (WikittyQuery c : queries) {
                if (c == null) {
                    result.add(null);
                } else {
                    try {
                        long startTime = System.nanoTime();
                        WikittyQueryResult<Map<String, Object>> searchResult =
                                getSearchEngine().findAllByQuery(tx, c);
                        long estimatedTime = System.nanoTime() - startTime;
                        searchResult.setTimeQuery(estimatedTime);
                        result.add(searchResult);
                    } catch (Exception eee) {
                        throw new WikittyException(String.format(
                                "Can't evaluate query '%s'", c), eee);
                    }
                }
            }

            if (queries.size() != result.size()) {
                log.error(String.format("Query input list (%s) has not same size that result list (%s)",
                        queries.size(), result.size()));
            }
            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Error during find", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    @Override
    public List<Map<String, Object>> findByQuery(String securityToken, List<WikittyQuery> queries) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(queries.size());
            List<WikittyQuery> queriesLimited = new ArrayList<WikittyQuery>(queries.size());
            for (WikittyQuery c : queries) {
                WikittyQuery cLimit = c.copy().setOffset(0).setLimit(1);
                queriesLimited.add(cLimit);
            }

            List<WikittyQueryResult<Map<String, Object>>> idsList =
                    findAllByQuery(securityToken, queriesLimited);

            for (WikittyQueryResult<Map<String, Object>> ids : idsList) {
                if (ids.size() > 0) {
                    result.add(ids.peek());
                } else {
                    result.add(null);
                }
            }

            if (queries.size() != result.size()) {
                log.error(String.format("Query input list (%s) has not same size that result list (%s)",
                        queries.size(), result.size()));
            }
            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Error during find", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    @Override
    public WikittyEvent deleteTree(String securityToken, String thesaurusId) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            List<String> allTreeNodeId = getRecursiveTreeNodeId(
                    securityToken, thesaurusId);
            WikittyEvent result = delete(securityToken, allTreeNodeId);
           
            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't delete tree", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    /**
     * Get recursive id of tree node children of {@code treeNodeId}.
     * 
     * @param securityToken security token
     * @param treeNodeId tree node id
     * @return all id of {@code treeNodeId}'s children 
     */
    protected List<String> getRecursiveTreeNodeId(String securityToken, String treeNodeId) {
        WikittyQuery query = new WikittyQueryMaker()
                .eq(WikittyTreeNode.FQ_FIELD_WIKITTYTREENODE_PARENT, treeNodeId).end();
        
        WikittyQueryResult<String> childTreeNodeIds = findAllByQuery(
                securityToken, Collections.singletonList(query)).get(0).convertMapToSimpleString();
        List<String> treeNodeIds = new ArrayList<String>();
        treeNodeIds.add(treeNodeId);
        for (String childTreeNodeId : childTreeNodeIds) {
            List<String> subTreeNodeIds = getRecursiveTreeNodeId(securityToken, childTreeNodeId);
            treeNodeIds.addAll(subTreeNodeIds);
        }
        return treeNodeIds;
    }

    @Override
    public TreeNodeResult<String> findTreeNode(String securityToken,
            String wikittyId, int depth, boolean count, Criteria filter) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            TreeNodeResult<String> result = null;
            Wikitty w = restore(securityToken, wikittyId);
            if(w != null) {
                result = getSearchEngine().findAllChildrenCount(
                        tx, wikittyId, depth, count, filter);
            }

            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't restore children", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    @Override
    public WikittyQueryResultTreeNode<String> findTreeNode(String securityToken,
            String wikittyId, int depth, boolean count, WikittyQuery filter) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            WikittyQueryResultTreeNode<String> result = null;
            Wikitty w = restore(securityToken, wikittyId);
            if(w != null) {
                result = getSearchEngine().findAllChildrenCount(
                        tx, wikittyId, depth, count, filter);
            }

            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't restore children", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    @Override
    public Wikitty restoreVersion(String securityToken,
            String wikittyId, String version) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * This method do some commit during execution. Transaction must be not
     * started when we call it.
     *
     * @param securityToken security token previously returned by login. If
     * securityToken is not valid, this method do nothing
     */
    @Override
    public void syncSearchEngine(final String securityToken) {
        final WikittyTransaction tx = WikittyTransaction.get();
        if (tx.isStarted()) {
            throw new WikittyException("Transaction must be not started for syncSearchEngine method");
        }

        boolean txBeginHere = false;
        try {
            log.info("Reindexation started");
            final int numberForCommit = 1000; // TODO poussin 20110221 configurable variable
            final WikittySearchEngine searchEngine = getSearchEngine();
            final List<Wikitty> wikitties = new ArrayList<Wikitty>(numberForCommit);

            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }
            searchEngine.clear(tx);
            if (txBeginHere) {
                tx.commit();
            }
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            WikittyStorage.DataStatistic stat = getWikittyStorage().getDataStatistic(tx);
            final long max = stat.getActiveWikitties();

            if (txBeginHere) {
                tx.commit();
            }
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            log.info("Reindexation 0%");
            getWikittyStorage().scanWikitties(tx, new WikittyStorage.Scanner() {
                long lastPurcent = -1;
                long current = 0;
                int count = 0;
                long startLoadStoreCommit = TimeLog.getTime();

                @Override
                public void scan(String wikittyId) {
                    Wikitty wikitty = restore(securityToken, wikittyId);
                    Date deleteDate = wikitty.getDeleteDate();
                    if(deleteDate == null) {
                        current++;
                        count ++;
                        wikitties.add(wikitty);

                        if(count == numberForCommit) {
                            // Reindex
                            long startStore = TimeLog.getTime();
                            searchEngine.store(tx, wikitties, true);
                            tx.commit();
                            // Reinit
                            count = 0;
                            wikitties.clear();
                            tx.begin();
                            timeLog.log(startStore, "StoreAndCommit",
                                    String.format("nb %s", numberForCommit));
                            startLoadStoreCommit = timeLog.log(startLoadStoreCommit,
                                    "LoadAndStoreAndCommit", String.format("nb %s", numberForCommit));
                            // si on a les statistiques a disposition on affiche
                            // l'etat d'avancement dans les logs
                            if (max > 0) {
                                long purcent = current * 100 / max;
                                // on affiche l'etat d'avancement environ tous les 10%
                                if (purcent / 10 > lastPurcent) {
                                    lastPurcent = purcent / 10;
                                    log.info(String.format("Reindexation %s%%", purcent));
                                }
                            }
                        }
                    }
                }
            });

            // Last wikitties
            searchEngine.store(tx, wikitties, true);
            log.info("Reindexation finished");
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't sync searchable index with data", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    /*
     * Question:
     * <li> on ne force pas le store de wikitty, ils vont donc avoir potentiellement
     * des versions differentes sur plusieurs serveurs, est-ce problematique ?
     * <li> on ne passe pas la date de suppression des wikitties, ils vont donc
     * avoir différente date de suppression sur différent serveur, est-ce problematique ?
     */
    @Override
    public WikittyEvent replay(
            String securityToken, List<WikittyEvent> events, boolean force) {
        // indique qu'il faut vider la base avant de faire les ajouts
        boolean mustClear = false;

        // tous les objets a sauver
        Map<String, Wikitty> toAddWikitty = new LinkedHashMap<String, Wikitty>();
        // tous les id a supprimer
        Map<String, Date> toRemoveWikitty = new LinkedHashMap<String, Date>();

        // toutes les extensions a sauver
        Set<WikittyExtension> toAddExt = new LinkedHashSet<WikittyExtension>();
        // toutes les extensions a supprimer
        Set<String> toDeleteExt = new LinkedHashSet<String>();

        // recherche un event avec un clear pour ne pas jouer des events inutiles
        // recherche un store + delete du meme wikitty
        // recherche le dernier store du wikitty
        for (WikittyEvent e : events) {
            // check clear must be the first, if event have clear and other type
            // clear is all time play first
            if (e.getType().contains(
                    WikittyEvent.WikittyEventType.CLEAR_WIKITTY)
                    || e.getType().contains(
                    WikittyEvent.WikittyEventType.CLEAR_EXTENSION)) {
                mustClear = true;
                toAddWikitty.clear();
                toRemoveWikitty.clear();
                toAddExt.clear();
            }
            if (e.getType().contains(WikittyEvent.WikittyEventType.PUT_WIKITTY)) {
                for (Wikitty w : e.getWikitties().values()) {
                    toAddWikitty.put(w.getWikittyId(), w);
                }
            }
            if (e.getType().contains(WikittyEvent.WikittyEventType.REMOVE_WIKITTY)) {
                for (Map.Entry<String, Date> entry : e.getRemoveDate().entrySet()) {
                    toAddWikitty.remove(entry.getKey());
                    toRemoveWikitty.put(entry.getKey(), entry.getValue());
                }
            }
            if (e.getType().contains(WikittyEvent.WikittyEventType.PUT_EXTENSION)) {
                for (WikittyExtension ext : e.getExtensions().values()) {
                    toAddExt.add(ext);
                }
            }
            if (e.getType().contains(WikittyEvent.WikittyEventType.REMOVE_EXTENSION)) {
                for (String extName : e.getDeletedExtensions()) {
                    toDeleteExt.add(extName);
                }
            }
        }

        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            WikittyEvent result = new WikittyEvent(this);
            if (mustClear) {
                WikittyEvent eventClear = clear(securityToken);
                result.add(eventClear);
            }
            WikittyEvent eventStoreExtension =
                    storeExtension(securityToken, toAddExt);
            result.add(eventStoreExtension);

            WikittyEvent eventDeleteExtension =
                    deleteExtension(securityToken, toDeleteExt);
            result.add(eventDeleteExtension);

            WikittyEvent eventStoreWikitty =
                    store(securityToken, toAddWikitty.values(), force);
            result.add(eventStoreWikitty);

            WikittyEvent eventDeleteWikitty =
                    delete(securityToken, toRemoveWikitty.keySet());
            result.add(eventDeleteWikitty);

            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't replay data", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }

    }

    @Override
    public boolean exists(String securityToken, String wikittyId) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            boolean result = getWikittyStorage().exists(tx, wikittyId);

            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't test existance", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

    @Override
    public boolean isDeleted(String securityToken, String wikittyId) {
        WikittyTransaction tx = WikittyTransaction.get();
        boolean txBeginHere = false;
        try {
            if (!tx.isStarted()) {
                tx.begin();
                txBeginHere = true;
            }

            boolean result = getWikittyStorage().isDeleted(tx, wikittyId);
            return result;
        } catch (WikittyException ex) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw ex;
        } catch (Exception eee) {
            if (tx != null && tx.isStarted()) {
                tx.rollback();
            }
            throw new WikittyException("Can't test existance", eee);
        } finally {
            if (txBeginHere && tx != null && tx.isStarted()) {
                tx.commit();
            }
        }
    }

}
