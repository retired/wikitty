/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

/**
 * Pour permettre de géré l'authentification par un mecanisme externe (ex:LDAP)
 * a wikitty en plus du mecanisme traditionnel, il faut implanter cette
 * interface et la declarer dans le fichier de configuration
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 * @deprecated since 3.4 use WikittyServiceAuthentication and WikittyServiceAuthorisation
 */
@Deprecated
public interface WikittyServiceSecurityExternalAuthentication {

    /**
     * Authentifie l'utilisateur ayant pour login et password les arguments.
     * La methode retourne true, si l'utilisateur a pu etre authentifier.
     *
     * @param login
     * @param password
     * @return true si l'utilisateur existe et que le mot de passe est le bon
     */
    public boolean login(String login, String password);

}
