/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2012 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfigOption;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 * @deprecated since 3.4 use WikittyServiceAuthenticationLDAP and WikittyServiceAuthorisation
 */
@Deprecated
public class WikittyServiceSecurityExternalAuthenticationLDAP
        implements WikittyServiceSecurityExternalAuthentication{

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceSecurityExternalAuthenticationLDAP.class);

    protected ApplicationConfig config;
    protected Properties jndiProp;
    protected String ldapLoginPattern;

    public WikittyServiceSecurityExternalAuthenticationLDAP(ApplicationConfig config) {
        this.config = config;

        // on charge toutes les options jndi
        Properties jndiPropTmp = config.getOptionStartsWith(WikittyConfigOption
                .WIKITTY_SERVICE_AUTHENTICATION_LDAP_JNDI.getKey());

        jndiProp = new Properties();
        for (Enumeration<String> e=(Enumeration<String>)jndiPropTmp.propertyNames(); e.hasMoreElements();) {
            String key = e.nextElement();
            String value = jndiPropTmp.getProperty(key);
            int debut = WikittyConfigOption.WIKITTY_SERVICE_AUTHENTICATION_LDAP_JNDI.getKey().length();
            key = key.substring(debut);
            jndiProp.setProperty(key, value);
        }

        // on charge l'url du serveur ldap
        String serverUrl = config.getOption(
                WikittyConfigOption.WIKITTY_SERVICE_AUTHENTICATION_LDAP_SERVER.getKey());
        jndiProp.put(Context.PROVIDER_URL, serverUrl);

        // on charge le pattern des logins (DN)
        ldapLoginPattern = config.getOption(
                WikittyConfigOption.WIKITTY_SERVICE_AUTHENTICATION_LDAP_LOGIN_PATTERN.getKey());
    }

    public boolean login(String login, String password) {
        boolean result = false;
        String ldapLogin = String.format(ldapLoginPattern, login);

        // on fait une copie pour etre thread safe (si plusieurs thread appele
        // cette methode en meme temps
        Hashtable<String, String> env = new Hashtable(jndiProp);
        env.put(Context.SECURITY_PRINCIPAL, ldapLogin);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            DirContext dirContext = new InitialDirContext(env);
            dirContext.close();
            result = true;
        } catch (NamingException eee) {
            log.debug(String.format(
                    "Erreur lors de l'acces au serveur LDAP pour l'utilisateur %s -> %s",
                    login, ldapLogin), eee);
        }
        return result;
    }

}
