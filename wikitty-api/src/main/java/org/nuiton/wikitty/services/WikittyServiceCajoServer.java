/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;


import gnu.cajo.invoke.Remote;
import gnu.cajo.utils.ItemServer;
import java.net.URL;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyService;

/**
 * Cajo server to used wikitty as client/server on cajo protocol (rmi based
 * protocol). see <a href="http://cajo.dev.java.net">http://cajo.dev.java.net</a>
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceCajoServer extends WikittyServiceDelegator {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceCajoServer.class);

    protected int port = 80;
    protected String path = "";

    public WikittyServiceCajoServer(ApplicationConfig config, WikittyService ws) {
        try {
            setDelegate(ws);

            // port and path from hessian server url in config
            String urlString = config.getOption(WikittyConfigOption.
                    WIKITTY_SERVER_URL.getKey());

            // get port and path part of server url, only that is used for cajo
            if (urlString != null && !"".equals(urlString)) {
                URL url = new URL(urlString);
                if (-1 != url.getPort()) {
                    port = url.getPort();
                }
                if (null != url.getPath()) {
                    path = url.getPath();
                }
            }
            
            if ("".equals(path)) {
                path = "WikittyService";
            } else if(path.startsWith("/")) {
                path = path.substring(1);
            }
            
            Remote.config(null, port, null, 0);
            ItemServer.bind(this, path);
            log.info(String.format("The server is running on %s:%s/%s",
                    Remote.getDefaultServerHost(), Remote.getDefaultServerPort(), path) );
        } catch (Exception eee) {
            throw new WikittyException("Can't start wikitty server", eee);
        }
    }

    /**
     * get port number of the service, default: 80
     * @return
     */
    public int getPort() {
        return port;
    }

    /**
     * get path used to bind server, default: WikittyService
     * @return
     */
    public String getPath() {
        return path;
    }

    /**
     * stop the server
     */
    public void stop() {
        ItemServer.unbind(path);
    }
}
