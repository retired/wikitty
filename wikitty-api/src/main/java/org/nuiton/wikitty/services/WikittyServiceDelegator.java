/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.query.WikittyQueryResultTreeNode;
import org.nuiton.wikitty.search.TreeNodeResult;

/**
 * Wikitty service delegator.
 *
 * Cette classe est abstraite car sa seul utilisation est en en heritant. Cela
 * aide pour l'ajout de methode sur WikittyService et voir les sous classes qui
 * doivent implanter ou non cette nouvelle methode de facon moins automatique
 * qu'une simple delegation
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
abstract public class WikittyServiceDelegator implements WikittyService {

    /** Delegated wikitty service. */
    protected WikittyService delegate;
    protected WikittyClient anonymousClient;

    public WikittyServiceDelegator() {
    }

    public WikittyServiceDelegator(WikittyService delegate) {
        setDelegate(delegate);
    }

    /**
     * Get delegated service.
     * 
     * @return delegate service
     */
    public WikittyService getDelegate() {
        return delegate;
    }

    public WikittyClient getAnonymousClient() {
        if (anonymousClient == null) {
            anonymousClient = new WikittyClient(null, delegate);
        }
        return anonymousClient;
    }

    public WikittyClient getClient(String token) {
        return new WikittyClient(null, delegate, token);
    }

    /**
     * Set delegated service.
     * 
     * @param delegate delegate
     */
    public void setDelegate(WikittyService delegate) {
        this.delegate = delegate;
    }

    @Override
    public void addWikittyServiceListener(WikittyListener listener,
            ServiceListenerType type) {
        delegate.addWikittyServiceListener(listener, type);
    }

    @Override
    public void removeWikittyServiceListener(WikittyListener listener,
            ServiceListenerType type) {
        delegate.removeWikittyServiceListener(listener, type);
    }

    @Override
    public String login(String login, String password) {
        return delegate.login(login, password);
    }

    @Override
    public void logout(String securityToken) {
        delegate.logout(securityToken);
    }

    @Override
    public String getToken(String user) {
        return delegate.getToken(user);
    }

    @Override
    public WikittyEvent clear(String securityToken) {
        return delegate.clear(securityToken);
    }

    @Override
    public WikittyEvent replay(
            String securityToken, List<WikittyEvent> events, boolean force) {
        return delegate.replay(securityToken, events, force);
    }

    @Override
    public WikittyEvent store(
            String securityToken, Collection<Wikitty> wikitties, boolean force) {
        return delegate.store(securityToken, wikitties, force);
    }

    @Override
    public List<String> getAllExtensionIds(String securityToken) {
        return delegate.getAllExtensionIds(securityToken);
    }

    @Override
    public List<String> getAllExtensionsRequires(String securityToken,
            String extensionName) {
        return delegate.getAllExtensionsRequires(securityToken, extensionName);
    }

    @Override
    public WikittyEvent storeExtension(String securityToken,
            Collection<WikittyExtension> exts) {
        return delegate.storeExtension(securityToken, exts);
    }

    @Override
    public WikittyEvent deleteExtension(
            String securityToken, Collection<String> extNames) {
        return delegate.deleteExtension(securityToken, extNames);
    }

    @Override
    public WikittyExtension restoreExtension(String securityToken, String id) {
        return delegate.restoreExtension(securityToken, id);
    }

    @Override
    public WikittyExtension restoreExtensionLastVersion(
            String securityToken, String name) {
        return delegate.restoreExtensionLastVersion(securityToken, name);
    }

    public List<WikittyExtension> restoreExtensionAndDependenciesLastVesion(
            String securityToken, Collection<String> extensionNames) {
        return delegate.restoreExtensionAndDependenciesLastVesion(
                securityToken, extensionNames);
    }

    @Override
    public List<Wikitty> restore(String securityToken, List<String> id) {
        return delegate.restore(securityToken, id);
    }

    @Override
    public WikittyEvent delete(String securityToken, Collection<String> ids) {
        return delegate.delete(securityToken, ids);
    }

    @Override
    public List<PagedResult<String>> findAllByCriteria(
            String securityToken, List<Criteria> criteria) {
        return delegate.findAllByCriteria(securityToken, criteria);
    }

    @Override
    public List<String> findByCriteria(String securityToken, List<Criteria> criteria) {
        return delegate.findByCriteria(securityToken, criteria);
    }

    @Override
    public TreeNodeResult<String> findTreeNode(String securityToken, String wikittyId, int depth, boolean count, Criteria filter) {
        return delegate.findTreeNode(securityToken, wikittyId, depth, count, filter);
    }

    @Override
    public List<WikittyQueryResult<Map<String, Object>>> findAllByQuery(
            String securityToken, List<WikittyQuery> queries) {
        return delegate.findAllByQuery(securityToken, queries);
    }

    @Override
    public List<Map<String, Object>> findByQuery(String securityToken, List<WikittyQuery> queries) {
        return delegate.findByQuery(securityToken, queries);
    }

    @Override
    public WikittyQueryResultTreeNode<String> findTreeNode(String securityToken, String wikittyId, int depth, boolean count, WikittyQuery filter) {
        return delegate.findTreeNode(securityToken, wikittyId, depth, count, filter);
    }

    @Override
    public WikittyEvent deleteTree(String securityToken, String wikittyId) {
        return delegate.deleteTree(securityToken, wikittyId);
    }

    @Override
    public Wikitty restoreVersion(String securityToken, String wikittyId,
            String version) {
        return delegate.restoreVersion(securityToken, wikittyId, version);
    }

    @Override
    public void syncSearchEngine(String securityToken) {
        delegate.syncSearchEngine(securityToken);
    }

    @Override
    public boolean canWrite(String securityToken, Wikitty wikitty) {
        return delegate.canWrite(securityToken, wikitty);
    }

    @Override
    public boolean canDelete(String securityToken, String wikittyId) {
        return delegate.canDelete(securityToken, wikittyId);
    }

    @Override
    public boolean canRead(String securityToken, String wikittyId) {
        return delegate.canRead(securityToken, wikittyId);
    }

    @Override
    public boolean exists(String securityToken, String wikittyId) {
        return delegate.exists(securityToken, wikittyId);
    }

    @Override
    public boolean isDeleted(String securityToken, String wikittyId) {
        return delegate.isDeleted(securityToken, wikittyId);
    }
}
