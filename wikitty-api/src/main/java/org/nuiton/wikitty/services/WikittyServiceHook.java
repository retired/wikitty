/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;


import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.ScriptEvaluator;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyHook;
import org.nuiton.wikitty.entities.WikittyHookHelper;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 * Cette classe permet d'intercepter les modifications faites via les differentes
 * methodes de modification des données et d'executer les differents WikittyHook
 * enregistres.
 *
 * Les WikittyHook peuvent
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceHook extends WikittyServiceDelegator {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceHook.class);

    final static public String PRE_STORE = "pre-store";
    final static public String POST_STORE = "post-store";
    final static public String PRE_STORE_EXTENSION = "pre-storeExtension";
    final static public String POST_STORE_EXTENSION = "post-storeExtension";
    final static public String PRE_DELETE = "pre-delete";
    final static public String POST_DELETE = "post-delete";
    final static public String PRE_DELETE_EXTENSION = "pre-deleteExtension";
    final static public String POST_DELETE_EXTENSION = "post-deleteExtension";
    final static public String PRE_DELETE_TREE = "pre-deleteTree";
    final static public String POST_DELETE_TREE = "post-deleteTree";
    final static public String PRE_CLEAR = "pre-clear";
    final static public String POST_CLEAR = "post-clear";
    final static public String PRE_LOGIN = "pre-login";
    final static public String POST_LOGIN = "post-login";
    final static public String PRE_LOGOUT = "pre-logout";
    final static public String POST_LOGOUT = "post-logout";
    final static public String PRE_REPLAY = "pre-replay";
    final static public String POST_REPLAY = "post-replay";
    final static public String PRE_SYNC_SEARCH_ENGINE = "pre-syncSearchEngine";
    final static public String POST_SYNC_SEARCH_ENGINE = "post-syncSearchEngine";

    /**
     *
     * @param config not use currently but needed in futur
     * @param ws
     */
    public WikittyServiceHook(ApplicationConfig config, WikittyService ws) {
        super(ws);
    }

    protected Collection<Wikitty> getHook(String securityToken, String actionName) {
        WikittyQuery query = new WikittyQueryMaker()
                .and()
                   .exteq(WikittyHook.EXT_WIKITTYHOOK)
                   .eq(WikittyHook.FQ_FIELD_WIKITTYHOOK_ACTIONTOHOOK, actionName)
                .end();
        WikittyQueryResult<String> ids = getClient(securityToken).findAllByQuery(query);

        List<Wikitty> result = getDelegate().restore(securityToken, ids.getAll());

        return result;
    }

    protected Map<String, Object> callHook(String securityToken, String actionName,
            Map<String, Object> args, WikittyEvent event) {
        Collection<Wikitty> hooks = getHook(securityToken, actionName);

        WikittyService ws = getDelegate();
        args.put("actionName", actionName);
        args.put("ws", ws);
        args.put("event", event);

        for (Wikitty hook : hooks) {
            String hookName = WikittyHookHelper.getName(hook);
            String mimetype = WikittyHookHelper.getMimetype(hook);
            String script = WikittyHookHelper.getScript(hook);

            args.put("hook", hook);

            args = ScriptEvaluator.exec(null, hookName, script, mimetype, args);
        }
        return args;
    }

    @Override
    public WikittyEvent store(String securityToken, Collection<Wikitty> wikitties, boolean force) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("securityToken", securityToken);
        args.put("wikitties", wikitties);
        args.put("force", force);
        
        args = callHook(securityToken, PRE_STORE, args, null);

        securityToken = (String)args.get("securityToken");
        wikitties = (Collection<Wikitty>)args.get("wikitties");
        force = (Boolean)args.get("force");

        WikittyEvent result = super.store(securityToken, wikitties, force);

        callHook(securityToken, POST_STORE, args, result);

        return result;
    }

    @Override
    public WikittyEvent storeExtension(String securityToken, Collection<WikittyExtension> exts) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("securityToken", securityToken);
        args.put("exts", exts);

        args = callHook(securityToken, PRE_STORE_EXTENSION, args, null);

        securityToken = (String)args.get("securityToken");
        exts = (Collection<WikittyExtension>) args.get("exts");

        WikittyEvent result = super.storeExtension(securityToken, exts);

        callHook(securityToken, POST_STORE_EXTENSION, args, result);

        return result;
    }

    @Override
    public WikittyEvent delete(String securityToken, Collection<String> ids) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("securityToken", securityToken);
        args.put("ids", ids);

        args = callHook(securityToken, PRE_DELETE, args, null);

        securityToken = (String)args.get("securityToken");
        ids = (Collection<String>) args.get("ids");

        WikittyEvent result = super.delete(securityToken, ids);

        callHook(securityToken, POST_DELETE, args, result);

        return result;
    }

    @Override
    public WikittyEvent deleteExtension(String securityToken, Collection<String> extNames) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("securityToken", securityToken);
        args.put("extNames", extNames);

        args = callHook(securityToken, PRE_DELETE_EXTENSION, args, null);

        securityToken = (String)args.get("securityToken");
        extNames = (Collection<String>) args.get("extNames");

        WikittyEvent result = super.deleteExtension(securityToken, extNames);

        callHook(securityToken, POST_DELETE_EXTENSION, args, result);

        return result;
    }

    @Override
    public WikittyEvent deleteTree(String securityToken, String wikittyId) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("securityToken", securityToken);
        args.put("wikittyId", wikittyId);

        args = callHook(securityToken, PRE_DELETE_TREE, args, null);

        securityToken = (String)args.get("securityToken");
        wikittyId = (String) args.get("wikittyId");

        WikittyEvent result = super.deleteTree(securityToken, wikittyId);

        callHook(securityToken, POST_DELETE_TREE, args, result);

        return result;
    }

    @Override
    public WikittyEvent clear(String securityToken) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("securityToken", securityToken);

        args = callHook(securityToken, PRE_CLEAR, args, null);

        securityToken = (String)args.get("securityToken");

        WikittyEvent result = super.clear(securityToken);

        callHook(securityToken, POST_CLEAR, args, result);

        return result;
    }

    @Override
    public String login(String login, String password) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("login", login);
        args.put("password", password);

        args = callHook(null, PRE_LOGIN, args, null);

        login = (String)args.get("login");
        password = (String) args.get("password");

        String result = super.login(login, password);

        callHook(null, POST_LOGIN, args, null);

        return result;
    }

    @Override
    public void logout(String securityToken) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("securityToken", securityToken);

        args = callHook(securityToken, PRE_LOGOUT, args, null);

        securityToken = (String)args.get("securityToken");

        super.logout(securityToken);

        callHook(securityToken, POST_LOGOUT, args, null);
    }

    @Override
    public WikittyEvent replay(String securityToken, List<WikittyEvent> events, boolean force) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("securityToken", securityToken);
        args.put("events", events);
        args.put("force", force);

        args = callHook(securityToken, PRE_REPLAY, args, null);

        securityToken = (String)args.get("securityToken");
        events = (List<WikittyEvent>) args.get("events");
        force = (Boolean)args.get("force");

        WikittyEvent result = super.replay(securityToken, events, force);

        callHook(securityToken, POST_REPLAY, args, result);

        return result;
    }

    @Override
    public void syncSearchEngine(String securityToken) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("securityToken", securityToken);

        args = callHook(securityToken, PRE_SYNC_SEARCH_ENGINE, args, null);

        securityToken = (String)args.get("securityToken");

        super.syncSearchEngine(securityToken);

        callHook(securityToken, POST_SYNC_SEARCH_ENGINE, args, null);

    }

}
