/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.Wikitty;
import java.util.HashMap;
import java.util.Map;
import org.nuiton.wikitty.WikittyService;

/**
 * Interface used to migrate Wikitty data from one WikittyExtension version
 * to another version
 * 
 * Your implementation can have constructor with ApplicationConfig as argument.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface WikittyExtensionMigration {

    // TODO poussin 20090902 permit configuration of migrationRegistry
    /**
     * use to put migration class for extension.
     * key: extensionName, value: migration class
     * @deprecated use new {@link WikittyExtensionMigrationRegistry}
     */
    @Deprecated
    static public Map<String, WikittyExtensionMigration> migrationRegistry =
            new HashMap<String, WikittyExtensionMigration>();

    /**
     * Migrate wikitty data from oldExt version to newExt.
     * 
     * @param service Wikitty service that do migration
     * @param oldWikitty Wikitty object that contains data in old version
     * extension format
     * @param newWikitty Wikitty object that contains data in new version
     * extension format after call
     * @param oldExt old extension definition
     * @param newExt new extension definition
     * @return newWikitty argument
     */
    public Wikitty migrate(WikittyService service,
            Wikitty oldWikitty, Wikitty newWikitty,
            WikittyExtension oldExt, WikittyExtension newExt);

}
