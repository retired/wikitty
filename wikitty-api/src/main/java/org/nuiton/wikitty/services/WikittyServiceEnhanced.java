/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.WikittyService;

/**
 * Same as delegator but with some helpfull method for developer.  This new
 * methods are methods with less arguments or simple argument and not collection
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceEnhanced extends WikittyServiceDelegator {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceEnhanced.class);

    public WikittyServiceEnhanced(WikittyService service) {
        super(service);
    }

    public WikittyEvent store(String securityToken, Wikitty wikitty) {
        return store(securityToken, Collections.singleton(wikitty), false);
    }

    public WikittyEvent store(String securityToken, Wikitty w1, Wikitty w2, Wikitty ... wN) {
        List<Wikitty> ws = new ArrayList<Wikitty>(wN.length + 2);
        Collections.addAll(ws, w1, w2);
        Collections.addAll(ws, wN);

        return store(securityToken, ws, false);
    }

    public WikittyEvent store(String securityToken,
            Collection<Wikitty> wikitties) {
        return store(securityToken, wikitties, false);
    }

    public WikittyEvent storeExtension(String securityToken,
            WikittyExtension ext) {
        return storeExtension(securityToken, Collections.singleton(ext));
    }

    /**
     *
     * @param securityToken security token
     * @param id object id to restore
     * @return the corresponding object, or null if object doesn't exist, is
     * deleted or you don't have authorisation (you can check authorisation
     * before call restore with {@link #canRead(java.lang.String, java.lang.String)}
     */
    public Wikitty restore(String securityToken, String id) {
        Wikitty result = restore(this, securityToken, id);
        return result;
    }

    public WikittyEvent delete(String securityToken, String id) {
        return delete(securityToken, Collections.singleton(id));
    }

    public WikittyEvent deleteExtension(String securityToken, String extName) {
        return deleteExtension(securityToken, Collections.singleton(extName));
    }

    /**
     * Conveniant static method usefull in other WikittyService implementation
     * where we don't wan't instanciate WikittyServiceEnhanced
     *
     * @param securityToken security token
     * @param id object id to restore
     * @return the corresponding object, or null if object doesn't exist, is
     * deleted or you don't have authorisation (you can check authorisation
     * before call restore with {@link #canRead(java.lang.String, java.lang.String)}
     */
    static public Wikitty restore(WikittyService ws, String securityToken, String id) {
        Wikitty result = null;
        List<Wikitty> resultList =
                ws.restore(securityToken, Collections.singletonList(id));
        if (resultList != null && resultList.size() > 0) {
            result = resultList.get(0);
        }
        return result;
    }

}
