/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.services;


import gnu.cajo.utils.extra.TransparentItemProxy;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyService;

/**
 * Cajo client part, this client must be used with {@link WikittyServiceCajoServer}
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyServiceCajoClient extends WikittyServiceDelegator {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyServiceCajoClient.class);

    public WikittyServiceCajoClient(ApplicationConfig config) {
        try {
            String url = config.getOption(WikittyConfigOption.
                    WIKITTY_SERVER_URL.getKey());

            // cajo url is not http or other protocol, url must start with //
            // example: //localhost:1198/ws
            // remove protocol
            int i = url.indexOf("://");
            if (i >= 0) {
                url = url.substring(i+1);
            }

            log.info(String.format("Looking for serveur '%s'", url));
            WikittyService ws = (WikittyService) TransparentItemProxy.getItem(
                    url, new Class[]{WikittyService.class});
            setDelegate(ws);
        } catch (Exception eee) {
            throw new WikittyException("Can't find wikitty server", eee);
        }
    }

}
