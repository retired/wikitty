/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.storage;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyObsoleteException;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyCopyOnWrite;
import org.nuiton.wikitty.services.WikittyEvent;
import org.nuiton.wikitty.services.WikittyTransaction;

public class WikittyStorageInMemory implements WikittyStorage {

    protected Map<String, Wikitty> wikitties = new LinkedHashMap<String, Wikitty>();

    public Map<String, Wikitty> getWikitties() {
        return wikitties;
    }

    public void setWikitties(Map<String, Wikitty> wikitties) {
        this.wikitties = wikitties;
    }

    @Override
    public WikittyEvent store(WikittyTransaction transaction, Collection<Wikitty> wikitties, boolean force) {
        WikittyEvent result = new WikittyEvent(this);
        for (Wikitty wikitty : wikitties) {
            // begin with clone of wikitty to prevent modification of
            // wikitty passed as argument
            try {
                wikitty = wikitty.clone();
            } catch (CloneNotSupportedException eee) {
                throw new WikittyException("Can't clone ?", eee);
            }
            String actualVersion = null;
            Date deletionDate = null;
            Wikitty inMemoryWikitty = this.wikitties.get(wikitty.getWikittyId());
            if (inMemoryWikitty != null) {
                actualVersion = inMemoryWikitty.getWikittyVersion();
                deletionDate = inMemoryWikitty.getDeleteDate();
            }
            String requestedVersion = wikitty.getWikittyVersion();
            String newVersion = null;
            if (force) {
                    // requestedVersion is never null
                    // versionGreaterThan support null for actualVersion
                    // and in this case return true
                    // see javadoc for more explanation
                    if (WikittyUtil.versionGreaterThan(requestedVersion, actualVersion)) {
                        newVersion = requestedVersion;
                    } else {
                        // else take actualVersion as base version
                        newVersion = WikittyUtil.incrementMajorRevision(actualVersion);
                    }
            } else {
                if (WikittyUtil.versionEquals(actualVersion, requestedVersion)
                         && deletionDate == null) {
                    // no modification, continue
                    continue;
                } else if (WikittyUtil.versionGreaterThan(actualVersion, requestedVersion)) {
                    throw new WikittyObsoleteException(String.format("Your wikitty '%s' is obsolete", wikitty.getWikittyId()));
                } else {
                    newVersion = WikittyUtil.incrementMajorRevision(actualVersion);
                }
            }
            wikitty.setWikittyVersion(newVersion);
            wikitty.clearDirty();
            try {
                this.wikitties.put(wikitty.getWikittyId(), wikitty.clone());
            } catch (CloneNotSupportedException eee) {
                throw new WikittyException("Can't clone ?", eee);
            }
            result.addWikitty(wikitty);
        }
        return result;
    }

    @Override
    public WikittyEvent delete(WikittyTransaction transaction, Collection<String> idList) throws WikittyException {
        WikittyEvent result = new WikittyEvent(this);
        Date now = new Date();
        for (String id : idList) {
            Wikitty w = wikitties.get(id);
            if (w != null && !w.isDeleted()) {
                w.setDeleteDate(now);
                result.addRemoveDate(id, now);
            }
        }
        return result;
    }

    @Override
    public boolean exists(WikittyTransaction transaction, String id) {
        boolean result = wikitties.containsKey(id);
        return result;
    }

    @Override
    public boolean isDeleted(WikittyTransaction transaction, String id) {
        boolean result = false;
        Wikitty w = wikitties.get(id);
        if (w == null) {
            throw new WikittyException(String.format("No wikitty with id '%s'", id));
        } else {
            result = w.isDeleted();
        }
        return result;
    }

    @Override
    public Wikitty restore(WikittyTransaction transaction, String id, String... fqFieldName) throws WikittyException {
        Wikitty result = wikitties.get(id);
        if (result.isDeleted()) {
            result = null;
        }
        
        // we must return copy on write, otherwize if two call to restore is done
        // we return the same instance and it's very bad :(
        result = new WikittyCopyOnWrite(result);
        return result;
    }

    @Override
    public void scanWikitties(WikittyTransaction transaction, Scanner scanner) {
        Collection<Wikitty> all = wikitties.values();
        for (Wikitty wikitty : all) {
            scanner.scan(wikitty.getWikittyId());
        }
    }

    @Override
    public WikittyEvent clear(WikittyTransaction transaction) {
        wikitties = new LinkedHashMap<String, Wikitty>();
        WikittyEvent result = new WikittyEvent(this);
        result.addType(WikittyEvent.WikittyEventType.CLEAR_WIKITTY);
        return result;
    }

    @Override
    public DataStatistic getDataStatistic(WikittyTransaction transaction) {
        long active = 0;
        long deleted = 0;
        Collection<Wikitty> all = wikitties.values();
        for (Wikitty wikitty : all) {
            if (wikitty.isDeleted()) {
                deleted++;
            } else {
                active++;
            }
        }
        DataStatistic result = new DataStatistic(active, deleted);
        return result;
    }

}
