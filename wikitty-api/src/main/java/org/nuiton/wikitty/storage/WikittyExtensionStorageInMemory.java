/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.storage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.services.WikittyEvent;
import org.nuiton.wikitty.services.WikittyTransaction;

public class WikittyExtensionStorageInMemory implements WikittyExtensionStorage {

    /** key: extName[version], value: extension object */
    protected Map<String, WikittyExtension> extensions;

    public WikittyExtensionStorageInMemory() {
        this.extensions = new HashMap<String, WikittyExtension>();
    }

    public Map<String, WikittyExtension> getExtensions() {
        return extensions;
    }

    public void setExtensions(Map<String, WikittyExtension> extensions) {
        this.extensions = extensions;
    }
    
    @Override
    public WikittyEvent store(WikittyTransaction transaction, Collection<WikittyExtension> exts) throws WikittyException {
        WikittyEvent result = new WikittyEvent(this);
        for (WikittyExtension ext : exts) {
            // on ajoute que s'il n'y est pas deja
            if (!extensions.containsKey(ext.getId())) {
                extensions.put(ext.getId(), ext);
                result.addExtension(ext);
            }
        }
        return result;
    }

    @Override
    public WikittyEvent delete(WikittyTransaction transaction, Collection<String> extNames) {
        WikittyEvent result = new WikittyEvent(this);
        Set<String> extToDelete = new HashSet<String>(extNames);
        for (Iterator<Map.Entry<String, WikittyExtension>> i = extensions.entrySet().iterator(); i.hasNext();) {
            Map.Entry<String, WikittyExtension> entry = i.next();
            String name = WikittyExtension.computeName(entry.getKey());
            if (extToDelete.contains(name)) {
                i.remove();
                result.addDeletedExtension(name);
            }
        }
        return result;
    }

    @Override
    public boolean exists(WikittyTransaction transaction, String id) {
        boolean result = extensions.containsKey(id);
        return result;
    }

    @Override
    public List<String> getAllExtensionIds(WikittyTransaction transaction) {
        List<String> result = new ArrayList<String>(extensions.keySet());
        return result;
    }

    @Override
    public List<String> getAllExtensionsRequires(WikittyTransaction transaction, String extensionName) {
        ArrayList<String> ids = new ArrayList<String>();
        Collection<WikittyExtension> values = extensions.values();
        if (values != null) {
            for (WikittyExtension extension : values) {
                if (extension.getRequires().contains(extensionName)) {
                    ids.add(extension.getId());
                }
            }
        }
        return ids;
    }

    @Override
    public String getLastVersion(WikittyTransaction transaction, String extName) {
        String result = null;
        Set<String> extensionIds = extensions.keySet();
        for (String extensionId : extensionIds) {
            String extensionVersion = WikittyExtension.computeVersion(extensionId);
            if (extensionId.startsWith(extName) && (result == null || WikittyUtil.versionGreaterThan(extensionVersion, result))) {
                result = extensionVersion;
            }
        }
        return result;
    }

    @Override
    public WikittyExtension restore(WikittyTransaction transaction, String name, String version) throws WikittyException {
        String id = WikittyExtension.computeId(name, version);
        WikittyExtension result = extensions.get(id);
        if (result == null) {
            throw new WikittyException(String.format("No extension with id '%s'", id));
        }
        return result;
    }

    @Override
    public WikittyEvent clear(WikittyTransaction transaction) {
        extensions = new HashMap<String, WikittyExtension>();
        WikittyEvent result = new WikittyEvent(this);
        result.addType(WikittyEvent.WikittyEventType.CLEAR_EXTENSION);
        return result;
    }
}
