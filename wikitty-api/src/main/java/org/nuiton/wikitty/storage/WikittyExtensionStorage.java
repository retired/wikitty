/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.storage;

import java.util.Collection;
import java.util.List;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.services.WikittyEvent;
import org.nuiton.wikitty.services.WikittyTransaction;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface WikittyExtensionStorage {

    /**
     * Store extension in storage.
     * 
     * @param transaction the current transaction
     * @param extensions the extensions to store
     * @return information usefull for client side update data
     */
    WikittyEvent store(WikittyTransaction transaction,
            Collection<WikittyExtension> extensions);

    /**
     * delete extensions
     *
     * @param transaction transaction
     * @param extNames extension name (extName)
     */
    public WikittyEvent delete(
            WikittyTransaction transaction, Collection<String> extNames);

    /**
     * Return true if id exists in storage.
     * 
     * @param transaction the current transaction
     * @param id an extension id
     * @return true if the extension exists
     */
    boolean exists(WikittyTransaction transaction, String id);

    /**
     * Return all extension ids.
     * 
     * @param transaction the current transaction
     * @return a list of extension ids
     */
    List<String> getAllExtensionIds(WikittyTransaction transaction);

    /**
     * Return all extension ids where the specified extensionName is required.
     * 
     * @param transaction the current transaction
     * @param extensionName the extension required
     * @return a list of extension ids
     */
    List<String> getAllExtensionsRequires(WikittyTransaction transaction,
            String extensionName);
    
    /**
     * return last version available for specified extension name.
     * 
     * @param transaction the current transaction
     * @param extName name of extension
     * @return last version availble for this version, or null if extension
     * doesn't exist
     */
    String getLastVersion(WikittyTransaction transaction, String extName);

    /**
     * Restore one extension from storage, if not found an exception is thrown.
     * 
     * @param transaction the current transaction
     * @param name extension name to restore
     * @param version extension version to restore
     * @return an extension
     * @throws WikittyException if exception during restore
     */
    WikittyExtension restore(WikittyTransaction transaction, String name, String version)
            throws WikittyException;

    /**
     * Remove all extensions.
     * 
     * @param transaction transaction
     */
    public WikittyEvent clear(WikittyTransaction transaction);
    
}
