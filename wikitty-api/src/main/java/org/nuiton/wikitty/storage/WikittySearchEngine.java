/*
 * #%L
 * Wikitty :: api
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.wikitty.storage;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.WikittyException;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.query.WikittyQueryResultTreeNode;
import org.nuiton.wikitty.search.TreeNodeResult;
import org.nuiton.wikitty.services.WikittyTransaction;

/**
 * WikittySearchEngine is used to abstract search engine used in WikittyService.
 *
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface WikittySearchEngine {

    /**
     * Remove all data in index
     */
    public void clear(WikittyTransaction transaction);

    /**
     * Store wikitty in storage
     * Tree are reindexed if necessary.
     * @param force if true, force indexation of wikitty, otherwize only dirty
     * or new wikitties are indexed
     */
    public void store(WikittyTransaction transaction,
            Collection<Wikitty> wikitties, boolean force);

    /**
     * Delete all object with idList argument. If id is not valid or don't exist.
     * Tree are reindexed if necessary.
     * 
     * @param idList list of ids to delete
     * @throws WikittyException
     */
    public void delete(WikittyTransaction transaction,
            Collection<String> idList) throws WikittyException;

    /**
     * @deprecated since 3.3 use {@link #findAllByCriteria(org.nuiton.wikitty.services.WikittyTransaction, org.nuiton.wikitty.search.Criteria)}
     */
    @Deprecated
    public PagedResult<String> findAllByCriteria(WikittyTransaction transaction, Criteria criteria);
    
    /**
     * Find all values that satisfy queries constraint. Values is Wikitty's id
     * if there is no Select condition, otherwize is String that represent
     * field value and can be String representation of
     * Wikitty, Date, Boolean, Numeric, Binary, String.
     * Or TODO (map? array?)
     *
     * @param transaction
     * @param queries
     * @return id of wikitties
     * @since 3.3
     */
    public WikittyQueryResult<Map<String, Object>> findAllByQuery(
            WikittyTransaction transaction, WikittyQuery queries);

    /**
     * Find all children ids with attachment count for a node wikitty.
     * If same attachment found many time in subtree this attachment is count
     * only once.
     *
     * If we have:
     * <ul>
     *  <li> w Node (4)
     *   <ul>
     *     <li> child1 (3) </li>
     *     <li> child2 (4) </li>
     *     <li> child3 (2)
     *      <ul>
     *       <li> subchild1 (1) </li>
     *       <li> subchild2 (5) </li>
     *     </ul>
     *     </li>
     *     <li> child4 (3) </li>
     *     <li> child5 (7) </li>
     *   </ul>
     *  </li>
     * </ul>
     *
     * return count for: child1(3), child2(4), child3(8), child4(3), child5(7)
     * and for the child3 count we have count of subchild1 and subchild2 in
     *
     * Node and subchild are returned according to depth
     *
     * @param transaction
     * @param wikittyId root node to begin
     * @param depth depth of node returned, -1 to retrieve all child level
     * @param count if true return count of attachment
     * @param filter filter on attachment count
     * @return Tree start with wikittyId as root
     * @throws WikittyException if wikittyId is not WikittyTreeNode
     * @deprecated since 3.3 use {@link #findAllChildrenCount(org.nuiton.wikitty.services.WikittyTransaction, java.lang.String, int, boolean, org.nuiton.wikitty.query.WikittyQuery) }
     */
    @Deprecated
    public TreeNodeResult<String> findAllChildrenCount(WikittyTransaction transaction,
            String wikittyId, int depth, boolean count, Criteria filter);

    /**
     * Find all children ids with attachment count for a node wikitty.
     * If same attachment found many time in subtree this attachment is count
     * only once.
     *
     * If we have:
     * <ul>
     *  <li> w Node (4)
     *   <ul>
     *     <li> child1 (3) </li>
     *     <li> child2 (4) </li>
     *     <li> child3 (2)
     *      <ul>
     *       <li> subchild1 (1) </li>
     *       <li> subchild2 (5) </li>
     *     </ul>
     *     </li>
     *     <li> child4 (3) </li>
     *     <li> child5 (7) </li>
     *   </ul>
     *  </li>
     * </ul>
     *
     * return count for: child1(3), child2(4), child3(8), child4(3), child5(7)
     * and for the child3 count we have count of subchild1 and subchild2 in
     *
     * Node and subchild are returned according to depth
     *
     * @param transaction
     * @param wikittyId root node to begin
     * @param depth depth of node returned, -1 to retrieve all child level
     * @param count if true return count of attachment
     * @param filter filter on attachment count
     * @return Tree start with wikittyId as root
     * @throws WikittyException if wikittyId is not WikittyTreeNode
     * @since 3.3
     */
    public WikittyQueryResultTreeNode<String> findAllChildrenCount(WikittyTransaction transaction,
            String wikittyId, int depth, boolean count, WikittyQuery filter);

}
