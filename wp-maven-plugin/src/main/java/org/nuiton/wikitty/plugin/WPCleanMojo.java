/*
 * #%L
 * Wikitty :: publication Maven plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.plugin;

import org.nuiton.util.FileUtil;
import org.nuiton.wikitty.publication.synchro.WikittyPublicationFileSystem;

import java.io.File;
import java.io.FileFilter;
import java.util.List;

/**
 * Goal to remove properties files used by WikittyService over file system to
 * ensure transformation wikitty-file. Delete ".wp" dirs.
 *
 * @author mfortun <manoel.fortun@gmail.com>
 * @goal clean
 * @requiresProject true
 * @requiresOnline true
 * @requiresDependencyResolution runtime
 * @since 3.2
 */
public class WPCleanMojo extends AbstractWPMojo {

    protected FileFilter propertiesDirFilter = new FileFilter() {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory()
                   && pathname.getName().equals(
                    WikittyPublicationFileSystem.PROPERTY_DIRECTORY);

        }
    };

    @Override
    protected void init() throws Exception {

    }

    @Override
    protected void doAction() throws Exception {

        File baseDir = project.getBasedir();
        // filter all properties dir 
        List<File> propertiesDir = FileUtil.getFilteredElements(baseDir,
                                                                propertiesDirFilter, true);
        // delete them
        for (File fipropsdir : propertiesDir) {
            if (getLog().isDebugEnabled()) {
                getLog().debug("Delete dir:" + fipropsdir);
            }
            FileUtil.deleteRecursively(fipropsdir);
        }

    }

}
