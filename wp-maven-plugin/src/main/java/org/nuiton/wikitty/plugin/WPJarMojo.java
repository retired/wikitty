/*
 * #%L
 * Wikitty :: publication Maven plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.plugin;

import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.publication.WikittyFileUtil;
import org.nuiton.wikitty.publication.WikittyPublicationConstant;
import org.nuiton.wikitty.publication.externalize.WikittyPublicationExternalize;
import org.nuiton.wikitty.publication.synchro.WikittyPublicationFileSystem;
import org.nuiton.wikitty.publication.synchro.WikittyPublicationSynchronize;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.Search;

import java.io.File;

/**
 * To create a wikitty publication jar.
 *
 * @author tchemit <chemit@codelutin.com>, mfortun <manoel.fortun@gmail.com>
 * @version $Id$
 * @goal jar
 * @requiresProject true
 * @requiresOnline true
 * @requiresDependencyResolution runtime
 * @since 3.2
 */
public class WPJarMojo extends AbstractWPLoadDependencyMojo {

    static public String EXTERNALIZE_PREFIX = "externalize-";

    @Override
    protected void init() throws Exception {
        super.init();
        // TODO
    }

    @Override
    protected void doAction() throws Exception {

        // to made a well formed jar we need to synchronise first
        // with this we will obtain the same directory/label configuration
        // that we will have on a classic service

        // construct uri of the file system wikitty service uri
        // with the correct label as fragment.
        String origin = getProject().getBasedir().toURI().toURL()
                .toExternalForm();

        origin += WikittyPublicationConstant.LABEL_DELIM + SRC_DIR_NAME
                  + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR + MAIN_DIR_NAME;

        // Construct first inside the build dir a temporary repositorie
        String buildDir = getProject().getBuild().getDirectory();
        File targetFile = new File(buildDir);
        createDirectoryIfNecessary(targetFile);

        String appPath = WikittyFileUtil.labelToPath(applicationName);
        File appsDir = new File(buildDir + File.separator + appPath);

        createDirectoryIfNecessary(appsDir);
        // Construct the uri over file system to ensure that wikitties will
        // have the correct 
        String target = targetFile.toURI().toURL().toExternalForm();
        target += WikittyPublicationConstant.LABEL_DELIM + applicationName;


        // launch syncrhonise with recursion enabled and delete and existing option
        // disable
        WikittyPublicationSynchronize.synchronisationServices(origin, target,
                                                              true, false, false);


        // then externalize the target that have the correct label
        ApplicationConfig appconfig = new ApplicationConfig();

        // construct application config with the current wikitty service over
        // file system
        appconfig.setOption(
                WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS.getKey(),
                WikittyPublicationFileSystem.class.getName());

        appconfig.setOption(WikittyConfigOption.WIKITTY_SERVER_URL.getKey(),
                            target);

        // the criteria to externalize all the wikitty
        Criteria critOnWikittyWithLabel = Search.query().keyword("*")
                .criteria();

        String jarName = EXTERNALIZE_PREFIX + applicationName;

        // construct the jar with a custom name, in the build dir
        // the correct config for the service a the criteria
        WikittyPublicationExternalize.externalize(appconfig,
                                                  critOnWikittyWithLabel, targetFile, jarName);

    }

}
