/*
 * #%L
 * Wikitty :: publication Maven plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.plugin;

import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.installer.ArtifactInstaller;
import org.apache.maven.artifact.repository.ArtifactRepositoryFactory;

import java.io.File;

/**
 * This mojo is for init for the goal that need to construct the application
 * this mojo, with init will read pom file to load dependency declared and so
 * needed by the wikitty publication application project. This will copy those
 * needed dependency to the right directory: src/main/resources/jar
 *
 * @author mfortun
 * @requiresProject true
 */
public abstract class AbstractWPLoadDependencyMojo extends AbstractWPMojo {

    /** @component */
    protected ArtifactInstaller installer;

    /** @component */
    protected ArtifactRepositoryFactory repositoryFactory;

    /**
     * Used to look up Artifacts in the remote repository.
     *
     * @component
     */
    protected ArtifactFactory factory;

    @Override
    protected void init() throws Exception {

        CopyDependencyUtil cpDep = new CopyDependencyUtil();

        // construct path to jar dir
        File basedir = getProject().getBasedir();
        File jarDir = new File(basedir.getAbsolutePath() + File.separator
                               + SRC_DIR_NAME + File.separator + MAIN_DIR_NAME
                               + File.separator + RESOURCES_DIR_NAME + File.separator
                               + JAR_RESOURCES_DIR_NAME);

        // initialize param for dependency copy
        cpDep.setRepositoryFactory(repositoryFactory);
        cpDep.setInstaller(installer);
        cpDep.setProject(project);
        cpDep.setOutputDirectory(jarDir);
        cpDep.setFactory(factory);
        cpDep.setLog(getLog());
        // run that will copy jar declared as dependency into the resource/jar
        // dir
        cpDep.execute();
    }

}
