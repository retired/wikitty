/*
 * #%L
 * Wikitty :: publication Maven plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.plugin;

import org.nuiton.wikitty.publication.WikittyFileUtil;
import org.nuiton.wikitty.publication.WikittyPublicationConstant;
import org.nuiton.wikitty.publication.synchro.WikittyPublicationSynchronize;

/**
 * To update a wikitty publication project.
 *
 * @author tchemit <chemit@codelutin.com>, mfortun <manoel.fortun@gmail.com>
 * @version $Id$
 * @goal update
 * @requiresProject true
 * @requiresOnline true
 * @requiresDependencyResolution runtime
 * @since 3.2
 */
public class WPUpdateMojo extends AbstractWPLoadDependencyMojo {

    @Override
    protected void init() throws Exception {
        super.init();
        // TODO
    }

    @Override
    protected void doAction() throws Exception {
        // Same as deploy

        // construct uri of the file system wikitty service uri
        // with the correct label as fragment.
        String origin = getProject().getBasedir().toURI().toURL()
                .toExternalForm();
        origin += WikittyPublicationConstant.LABEL_DELIM + SRC_DIR_NAME
                  + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR + MAIN_DIR_NAME;

        // Construct the target uri with the correct label as fragment
        // to ensure that wikitty under src/main/java label will be under
        // application name label
        String target = wikittyServiceUrl;
        target += WikittyPublicationConstant.LABEL_DELIM + applicationName;

        // launch syncrhonise with recursion enabled and delete and existing
        // option
        // disable
        WikittyPublicationSynchronize.synchronisationServices(origin, target,
                                                              true, false, false);
    }

}
