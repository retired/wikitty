/*
 * #%L
 * Wikitty :: publication Maven plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.plugin;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfigOption;
import org.nuiton.wikitty.publication.PropertiesExtended;
import org.nuiton.wikitty.publication.WikittyFileUtil;
import org.nuiton.wikitty.publication.WikittyPublicationConstant;
import org.nuiton.wikitty.publication.WikittyPublicationFallbackService;
import org.nuiton.wikitty.publication.synchro.WikittyPublicationFileSystem;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * To run a wikitty publication project.
 *
 * @author tchemit <chemit@codelutin.com>, mfortun <manoel.fortun@gmail.com>
 * @version $Id$
 * @goal run
 * @requiresProject true
 * @requiresOnline true
 * @requiresDependencyResolution runtime
 * @since 3.2
 */
public class WPRunMojo extends AbstractWPLoadDependencyMojo {

    private static final String DEPENDCY_TYPE_WAR = "war";

    protected static final String PUBLICATION_WAR_ARTIFACT_ID = "wikitty-publication-ui";

    protected static final String PUBLICATION_WAR_GROUP_ID = "org.nuiton.wikitty";

    public static final String FALLBACK_PROPERTIE_FILE_NAME = "fallback.properties";

    /**
     * The component that is used to resolve additional artifacts required.
     *
     * @required
     * @component
     */
    private ArtifactResolver artifactResolver;

    /**
     * The component used for creating artifact instances.
     *
     * @required
     * @component
     */
    private ArtifactFactory artifactFactory;


    /**
     * URL of server to use to log into server.
     *
     * @parameter expression="${publicationVersion}"
     * @required
     */
    protected String publicationVersion;


    /**
     * Local Repository.
     *
     * @parameter expression="${localRepository}"
     * @required
     * @readonly
     */
    protected ArtifactRepository localRepository;

    /**
     * Remote repositories used for the project.
     *
     * @parameter expression="${project.remoteArtifactRepositories}"
     * @required
     * @readonly
     */
    protected List remoteRepositories;


    @Override
    protected void init() throws Exception {
        super.init();
        // TODO
    }

    @Override
    protected void doAction() throws Exception {

        // search for the wikitty publication ui war
        File webApp = resolveFile();

        // prepare the fall back uri that will be a wikitty service orver
        // file system, our current src/main dir with the application
        String build = project.getBuild().getDirectory();
        File buildDir = new File(build);
        createDirectoryIfNecessary(buildDir);

        // construct props dir that will contain properties for fallback
        File propsFile = new File(build + File.separator
                                  + FALLBACK_PROPERTIE_FILE_NAME);
        if (propsFile.exists()) {
            deleteFile(propsFile);
        }
        createNewFile(propsFile);
        // construct propertie for fallback service
        String fileSystemUrlService = getProject().getBasedir().toURI().toURL()
                .toExternalForm();

        fileSystemUrlService += WikittyPublicationConstant.LABEL_DELIM
                                + SRC_DIR_NAME + WikittyFileUtil.WIKITTY_LABEL_SEPARATOR
                                + MAIN_DIR_NAME;

        PropertiesExtended propsFSServiceFall = new PropertiesExtended(
                propsFile);

        propsFSServiceFall.put(
                WikittyConfigOption.WIKITTY_WIKITTYSERVICE_COMPONENTS.getKey(),
                WikittyPublicationFileSystem.class.getName());
        propsFSServiceFall.put(WikittyConfigOption.WIKITTY_SERVER_URL.getKey(),
                               fileSystemUrlService);
        propsFSServiceFall.store();

        // set the propertie to find our propertieFile
        // TODO mfortun-2011-08-17 need to put the complete path to the file ?
        System.setProperty(
                WikittyPublicationFallbackService.WIKITTY_FALLBACK_FILE_KEY,
                FALLBACK_PROPERTIE_FILE_NAME);
        // use to specify were to found our properties file
        System.setProperty(ApplicationConfig.CONFIG_PATH, getProject()
                .getBuild().getDirectory());

        // run jetty with our application inside fallback in wikitty publication
        JettyUtil jettyRunMojo = new JettyUtil();
        jettyRunMojo.setWebApp(webApp);
        jettyRunMojo.setProject(project);

        jettyRunMojo.execute();

    }

    /*
     * from org.codehaus.mojo.license.DefaultThirdPartyTool
     * by tchemit
     */
    private File resolveFile() throws IOException,
            ArtifactResolutionException, ArtifactNotFoundException {
        File result;

        // TODO: this is a bit crude - proper type, or proper handling as
        // metadata rather than an artifact in 2.1?
        Artifact artifact = artifactFactory.createArtifactWithClassifier(
                PUBLICATION_WAR_GROUP_ID, PUBLICATION_WAR_ARTIFACT_ID,
                publicationVersion, DEPENDCY_TYPE_WAR, StringUtils.EMPTY);
        // TODO mfortun-2011-08-18 change for version
        try {
            artifactResolver.resolve(artifact, remoteRepositories, localRepository);

            result = artifact.getFile();

            // we use zero length files to avoid re-resolution (see below)
            if (result.length() == 0) {
                getLog().debug("Skipped third party descriptor");
            }
        } catch (ArtifactNotFoundException e) {
            getLog().debug(
                    "Unable to locate third party files descriptor : " + e);

            // we can afford to write an empty descriptor here as we don't
            // expect it to turn up later in the remote
            // repository, because the parent was already released (and
            // snapshots are updated automatically if changed)
            result = new File(localRepository.getBasedir(),
                              localRepository.pathOf(artifact));

            createNewFile(result);
        }

        return result;
    }


}
