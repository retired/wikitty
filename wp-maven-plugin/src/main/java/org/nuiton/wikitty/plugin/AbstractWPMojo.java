/*
 * #%L
 * Wikitty :: publication Maven plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.plugin;

import org.apache.maven.project.MavenProject;
import org.nuiton.plugin.AbstractPlugin;

/**
 * Abstract mojo for all mojos of the WP module.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 3.2
 */
public abstract class AbstractWPMojo extends AbstractPlugin {

    public static final String SRC_DIR_NAME = "src";

    public static final String MAIN_DIR_NAME = "main";

    public static final String APPLICATION_DIR_NAME = "wp";

    public static final String RESOURCES_DIR_NAME = "resources";

    public static final String IMAGES_RESOURCES_DIR_NAME = "images";

    public static final String JAR_RESOURCES_DIR_NAME = "jar";

    /**
     * Project.
     *
     * @parameter default-value="${project}"
     * @required
     * @readonly
     * @since 3.2
     */
    protected MavenProject project;

    /**
     * A flag to activate verbose mode.
     *
     * @parameter expression="${wp.verbose}"  default-value="${maven.verbose}"
     */
    protected boolean verbose;

    /**
     * The mandatory application name.
     *
     * @parameter expression="${wp.applicationName}"
     * @required
     */
    protected String applicationName;

    /**
     * The mandatory wikitty service url.
     *
     * @parameter expression="${wp.wikittyServiceUrl}"
     * @required
     */
    protected String wikittyServiceUrl;

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getWikittyServiceUrl() {
        return wikittyServiceUrl;
    }

    public void setWikittyServiceUrl(String wikittyServiceUrl) {
        this.wikittyServiceUrl = wikittyServiceUrl;
    }
}
