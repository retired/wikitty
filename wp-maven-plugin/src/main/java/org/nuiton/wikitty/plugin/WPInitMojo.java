/*
 * #%L
 * Wikitty :: publication Maven plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.plugin;

import java.io.File;

/**
 * To init a wikitty publication project. To init directory architecture for
 * wikitty publication project.
 *
 * @author tchemit <chemit@codelutin.com>, mfortun <manoel.fortun@gmail.com>
 * @version $Id$
 * @goal init
 * @requiresProject true
 * @requiresOnline true
 * @requiresDependencyResolution runtime
 * @since 3.2
 */
public class WPInitMojo extends AbstractWPMojo {


    @Override
    protected void init() throws Exception {
        // TODO
    }

    @Override
    protected void doAction() throws Exception {
        File basedir = getProject().getBasedir();
        /*
         * Create path for application
         * 
         * src/main/wp, src/main/ressource/images, src/main/ressource/jar
         */
        File srcDir = new File(basedir.getAbsolutePath() + File.separator
                               + SRC_DIR_NAME);
        File mainDir = new File(srcDir.getAbsolutePath() + File.separator
                                + MAIN_DIR_NAME);
        File appDir = new File(mainDir.getAbsolutePath() + File.separator
                               + APPLICATION_DIR_NAME);
        File ressourceDir = new File(mainDir.getAbsolutePath() + File.separator
                                     + RESOURCES_DIR_NAME);
        File imgDir = new File(ressourceDir.getAbsolutePath() + File.separator
                               + IMAGES_RESOURCES_DIR_NAME);
        File jarDir = new File(ressourceDir.getAbsolutePath() + File.separator
                               + JAR_RESOURCES_DIR_NAME);

        // create dirs
        createDirectoryIfNecessary(srcDir);
        createDirectoryIfNecessary(mainDir);
        createDirectoryIfNecessary(appDir);
        createDirectoryIfNecessary(ressourceDir);
        createDirectoryIfNecessary(imgDir);
        createDirectoryIfNecessary(jarDir);
    }
}
