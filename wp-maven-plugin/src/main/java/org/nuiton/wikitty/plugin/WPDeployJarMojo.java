/*
 * #%L
 * Wikitty :: publication Maven plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.plugin;

import org.apache.maven.artifact.manager.WagonConfigurationException;
import org.apache.maven.artifact.manager.WagonManager;
import org.apache.maven.artifact.versioning.ComparableVersion;
import org.apache.maven.execution.MavenExecutionRequest;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.site.wagon.repository.Repository;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Proxy;
import org.apache.maven.settings.Server;
import org.apache.maven.settings.Settings;
import org.apache.maven.settings.crypto.DefaultSettingsDecryptionRequest;
import org.apache.maven.settings.crypto.SettingsDecrypter;
import org.apache.maven.settings.crypto.SettingsDecryptionResult;
import org.apache.maven.wagon.CommandExecutionException;
import org.apache.maven.wagon.CommandExecutor;
import org.apache.maven.wagon.ConnectionException;
import org.apache.maven.wagon.ResourceDoesNotExistException;
import org.apache.maven.wagon.TransferFailedException;
import org.apache.maven.wagon.UnsupportedProtocolException;
import org.apache.maven.wagon.Wagon;
import org.apache.maven.wagon.authentication.AuthenticationException;
import org.apache.maven.wagon.authentication.AuthenticationInfo;
import org.apache.maven.wagon.authorization.AuthorizationException;
import org.apache.maven.wagon.observers.Debug;
import org.apache.maven.wagon.proxy.ProxyInfo;
import org.codehaus.plexus.PlexusConstants;
import org.codehaus.plexus.PlexusContainer;
import org.codehaus.plexus.component.configurator.ComponentConfigurationException;
import org.codehaus.plexus.component.configurator.ComponentConfigurator;
import org.codehaus.plexus.component.repository.exception.ComponentLifecycleException;
import org.codehaus.plexus.component.repository.exception.ComponentLookupException;
import org.codehaus.plexus.configuration.PlexusConfiguration;
import org.codehaus.plexus.configuration.xml.XmlPlexusConfiguration;
import org.codehaus.plexus.context.Context;
import org.codehaus.plexus.context.ContextException;
import org.codehaus.plexus.personality.plexus.lifecycle.phase.Contextualizable;
import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.StringUtils;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * To deploy a wikitty publication jar.
 *
 * @author tchemit <chemit@codelutin.com>, mfortun <manoel.fortun@gmail.com>
 * @version $Id$
 * @goal deploy-jar
 * @requiresProject true
 * @requiresOnline true
 * @requiresDependencyResolution runtime
 * @since 3.2
 */
public class WPDeployJarMojo extends AbstractWPLoadDependencyMojo implements
        Contextualizable {

    /**
     * Id of server.
     *
     * @parameter expression="${serverId}"
     * @required
     */
    protected String serverId;

    /**
     * URL of server to use to log into server.
     *
     * @parameter expression="${serverUrl}"
     * @required
     */
    protected String serverUrl;

    /**
     * Whether to run the "chmod" command on the remote site after the deploy.
     * Defaults to "true".
     *
     * @parameter expression="${maven.site.chmod}" default-value="true"
     */
    protected boolean chmod;

    /**
     * The mode used by the "chmod" command. Only used if chmod = true. Defaults
     * to "g+w,a+rX".
     *
     * @parameter expression="${maven.site.chmod.mode}" default-value="g+w,a+rX"
     */
    protected String chmodMode;

    /**
     * The options used by the "chmod" command. Only used if chmod = true.
     * Defaults to "-Rf".
     *
     * @parameter expression="${maven.site.chmod.options}" default-value="-Rf"
     */
    protected String chmodOptions;

    /** @component */
    protected WagonManager wagonManager;

    /**
     * The current user system settings for use in Maven.
     *
     * @parameter expression="${settings}"
     * @required
     * @readonly
     */
    protected Settings settings;

    /**
     * @parameter expression="${session}"
     * @required
     * @readonly
     */
    protected MavenSession mavenSession;

    protected PlexusContainer container;

    protected Repository repository;

    /**
     * All available wagons.
     *
     * @component role="org.apache.maven.wagon.Wagon"
     * @since 1.0
     */
    protected Map<String, Wagon> wagons;

    /** @component */
    protected SettingsDecrypter settingsDecrypter;

    protected ProxyInfo proxyInfo;

    protected Wagon wagon;

    /**
     * file to deploy
     *
     * @parameter expression="${fileToDeploy}"
     * @required
     */
    protected File fileToDeploy;

    @Override
    protected void init() throws Exception {
        super.init();

        // if file not declare seach for the default file construct by
        // wp:jar
        if (fileToDeploy == null || !fileToDeploy.exists()) {
            String file = getProject().getBuild().getDirectory();
            file += File.separator + WPJarMojo.EXTERNALIZE_PREFIX
                    + applicationName + ".jar";
            fileToDeploy = new File(file);
        }

        repository = new Repository(serverId, serverUrl);

        if (!fileToDeploy.exists()) {
            throw new MojoExecutionException("The file to deploy "
                                             + fileToDeploy + " does not exist");
        }

        if (!isMaven3OrMore()) {
            proxyInfo = getProxyInfo();
        } else {

            proxyInfo = getProxy();
        }

        wagon = getWagon(repository, wagonManager);

    }

    @Override
    protected void doAction() throws Exception {

        if (getLog().isDebugEnabled()) {
            getLog().debug(
                    "Deploying to '" + repository.getUrl()
                    + "',\n    Using credentials from server id '"
                    + repository.getId() + "'");
        }

        try {

            configureWagon(wagon, repository.getId(), settings, container,
                           getLog());

            AuthenticationInfo authenticationInfo = wagonManager
                    .getAuthenticationInfo(repository.getId());
            getLog().debug(
                    "authenticationInfo with id '"
                    + repository.getId()
                    + "': "
                    + (authenticationInfo == null ? "-"
                                                  : authenticationInfo.getUserName()));

            try {
                Debug debug = new Debug();

                wagon.addSessionListener(debug);

                wagon.addTransferListener(debug);

                if (proxyInfo != null) {
                    getLog().debug("connect with proxyInfo");
                    wagon.connect(repository, authenticationInfo, proxyInfo);
                } else if (authenticationInfo != null) {
                    getLog().debug(
                            "connect with authenticationInfo and without proxyInfo");
                    wagon.connect(repository, authenticationInfo);
                } else {
                    getLog().debug(
                            "connect without authenticationInfo and without proxyInfo");
                    wagon.connect(repository);
                }

                getLog().info("Pushing " + fileToDeploy);

                wagon.put(fileToDeploy, fileToDeploy.getName());

            } catch (ResourceDoesNotExistException e) {
                throw new MojoExecutionException("Error uploading site", e);
            } catch (TransferFailedException e) {
                throw new MojoExecutionException("Error uploading site", e);
            } catch (AuthorizationException e) {
                throw new MojoExecutionException("Error uploading site", e);
            } catch (ConnectionException e) {
                throw new MojoExecutionException("Error uploading site", e);
            } catch (AuthenticationException e) {
                throw new MojoExecutionException("Error uploading site", e);
            }

            if (chmod) {
                try {
                    if (wagon instanceof CommandExecutor) {
                        CommandExecutor exec = (CommandExecutor) wagon;
                        exec.executeCommand("chmod " + chmodOptions + " "
                                            + chmodMode + " " + repository.getBasedir());
                    }
                    // else ? silently ignore, FileWagon is not a
                    // CommandExecutor!
                } catch (CommandExecutionException e) {
                    throw new MojoExecutionException("Error uploading site", e);
                }
            }
        } finally {
            try {
                wagon.disconnect();
            } catch (ConnectionException e) {
                getLog().error("Error disconnecting wagon - ignored", e);
            }
        }

    }

    protected boolean isMaven3OrMore() {
        return new ComparableVersion(getMavenVersion())
                       .compareTo(new ComparableVersion("3.0")) >= 0;
    }

    protected String getMavenVersion() {
        // This relies on the fact that MavenProject is the in core classloader
        // and that the core classloader is for the maven-core artifact
        // and that should have a pom.properties file
        // if this ever changes, we will have to revisit this code.
        Properties properties = new Properties();
        InputStream in = MavenProject.class
                .getClassLoader()
                .getResourceAsStream(
                        "META-INF/maven/org.apache.maven/maven-core/pom.properties");
        try {
            properties.load(in);
        } catch (IOException ioe) {
            return "";
        } finally {
            IOUtil.close(in);
        }

        return properties.getProperty("version").trim();
    }

    private String getSupportedProtocols() {
        Set<String> protocols = wagons.keySet();

        return StringUtils.join(protocols.iterator(), ", ");
    }

    /**
     * <p>
     * Get the <code>ProxyInfo</code> of the proxy associated with the
     * <code>host</code> and the <code>protocol</code> of the given
     * <code>repository</code>.
     * </p>
     * <p>
     * Extract from <a
     * href="http://java.sun.com/j2se/1.5.0/docs/guide/net/properties.html">
     * J2SE Doc : Networking Properties - nonProxyHosts</a> : "The value can be
     * a list of hosts, each separated by a |, and in addition a wildcard
     * character (*) can be used for matching"
     * </p>
     * <p>
     * Defensively support for comma (",") and semi colon (";") in addition to
     * pipe ("|") as separator.
     * </p>
     *
     * @return a ProxyInfo object instantiated or <code>null</code> if no
     *         matching proxy is found
     */
    public ProxyInfo getProxyInfo() {

        ProxyInfo proxyInfo = wagonManager.getProxy(repository.getProtocol());

        if (proxyInfo == null) {
            return null;
        }

        String host = repository.getHost();
        String nonProxyHostsAsString = proxyInfo.getNonProxyHosts();
        String[] nonProxyHosts = StringUtils
                .split(nonProxyHostsAsString, ",;|");
        for (int i = 0; i < nonProxyHosts.length; i++) {
            String nonProxyHost = nonProxyHosts[i];
            if (StringUtils.contains(nonProxyHost, "*")) {
                // Handle wildcard at the end, beginning or middle of the
                // nonProxyHost
                int pos = nonProxyHost.indexOf('*');
                String nonProxyHostPrefix = nonProxyHost.substring(0, pos);
                String nonProxyHostSuffix = nonProxyHost.substring(pos + 1);
                // prefix*
                if (StringUtils.isNotEmpty(nonProxyHostPrefix)
                    && host.startsWith(nonProxyHostPrefix)
                    && StringUtils.isEmpty(nonProxyHostSuffix)) {
                    return null;
                }
                // *suffix
                if (StringUtils.isEmpty(nonProxyHostPrefix)
                    && StringUtils.isNotEmpty(nonProxyHostSuffix)
                    && host.endsWith(nonProxyHostSuffix)) {
                    return null;
                }
                // prefix*suffix
                if (StringUtils.isNotEmpty(nonProxyHostPrefix)
                    && host.startsWith(nonProxyHostPrefix)
                    && StringUtils.isNotEmpty(nonProxyHostSuffix)
                    && host.endsWith(nonProxyHostSuffix)) {
                    return null;
                }
            } else if (host.equals(nonProxyHost)) {
                return null;
            }
        }
        return proxyInfo;
    }

    /**
     * Get proxy information for Maven 3.
     *
     * @return
     */
    private ProxyInfo getProxy() {
        String protocol = repository.getProtocol();
        String url = repository.getUrl();

        getLog().debug("repository protocol " + protocol);

        String originalProtocol = protocol;
        // olamy: hackish here protocol (wagon hint in fact !) is dav
        // but the real protocol (transport layer) is http(s)
        // and it's the one use in wagon to find the proxy arghhh
        // so we will check both
        if (StringUtils.equalsIgnoreCase("dav", protocol)
            && url.startsWith("dav:")) {
            url = url.substring(4);
            if (url.startsWith("http")) {
                try {
                    URL urlSite = new URL(url);
                    protocol = urlSite.getProtocol();
                    getLog().debug(
                            "found dav protocol so transform to real transport protocol "
                            + protocol);
                } catch (MalformedURLException e) {
                    getLog().warn("fail to build URL with " + url);
                }

            }
        } else {
            getLog().debug("getProxy 'protocol': " + protocol);
        }
        if (mavenSession != null && protocol != null) {
            MavenExecutionRequest request = mavenSession.getRequest();

            if (request != null) {
                List<Proxy> proxies = request.getProxies();

                if (proxies != null) {
                    for (Proxy proxy : proxies) {
                        if (proxy.isActive()
                            && (protocol.equalsIgnoreCase(proxy
                                                                  .getProtocol()) || originalProtocol
                                .equalsIgnoreCase(proxy.getProtocol()))) {
                            SettingsDecryptionResult result = settingsDecrypter
                                    .decrypt(new DefaultSettingsDecryptionRequest(
                                            proxy));
                            proxy = result.getProxy();

                            ProxyInfo proxyInfo = new ProxyInfo();
                            proxyInfo.setHost(proxy.getHost());
                            // so hackish for wagon the protocol is https for
                            // site dav : dav:https://dav.codehaus.org/mojo/
                            proxyInfo.setType(protocol);// proxy.getProtocol()
                            // );
                            proxyInfo.setPort(proxy.getPort());
                            proxyInfo
                                    .setNonProxyHosts(proxy.getNonProxyHosts());
                            proxyInfo.setUserName(proxy.getUsername());
                            proxyInfo.setPassword(proxy.getPassword());

                            getLog().debug(
                                    "found proxyInfo " + "host:port "
                                    + proxyInfo.getHost() + ":"
                                    + proxyInfo.getPort() + ", "
                                    + proxyInfo.getUserName());

                            return proxyInfo;
                        }
                    }
                }
            }
        }
        getLog().debug(
                "getProxy 'protocol': " + protocol + " no ProxyInfo found");
        return null;
    }

    private Wagon getWagon(
            org.apache.maven.wagon.repository.Repository repository,
            WagonManager manager) throws MojoExecutionException {
        Wagon wagon;

        try {
            wagon = manager.getWagon(repository);
        } catch (UnsupportedProtocolException e) {
            String shortMessage = "Unsupported protocol: '"
                                  + repository.getProtocol() + "' for site deployment to "
                                  + "distributionManagement.site.url=" + repository.getUrl()
                                  + ".";
            String longMessage = "\n"
                                 + shortMessage
                                 + "\n"
                                 + "Currently supported protocols are: "
                                 + getSupportedProtocols()
                                 + ".\n"
                                 + "    Protocols may be added through wagon providers.\n"
                                 + "    For more information, see "
                                 + "http://maven.apache.org/plugins/maven-site-plugin/examples/adding-deploy-protocol.html";

            getLog().error(longMessage);

            throw new MojoExecutionException(shortMessage);
        } catch (TransferFailedException e) {
            throw new MojoExecutionException("Unable to configure Wagon: '"
                                             + repository.getProtocol() + "'", e);
        }

        if (!wagon.supportsDirectoryCopy()) {
            throw new MojoExecutionException("Wagon protocol '"
                                             + repository.getProtocol()
                                             + "' doesn't support directory copying");
        }

        return wagon;
    }

    /**
     * Configure the Wagon with the information from serverConfigurationMap (
     * which comes from settings.xml )
     *
     * @param wagon
     * @param repositoryId
     * @param settings
     * @param container
     * @param log
     * @throws WagonConfigurationException
     * @todo Remove when
     * {@link WagonManager#getWagon(org.apache.maven.wagon.repository.Repository)
     * is available}. It's available in Maven 2.0.5.
     */
    private static void configureWagon(Wagon wagon, String repositoryId,
                                       Settings settings, PlexusContainer container, Log log)
            throws TransferFailedException {
        log.debug(" configureWagon ");

        // MSITE-25: Make sure that the server settings are inserted
        for (int i = 0; i < settings.getServers().size(); i++) {
            Server server = settings.getServers().get(i);
            String id = server.getId();

            log.debug("configureWagon server " + id);

            if (id != null && id.equals(repositoryId)) {
                if (server.getConfiguration() != null) {
                    PlexusConfiguration plexusConf = new XmlPlexusConfiguration(
                            (Xpp3Dom) server.getConfiguration());

                    ComponentConfigurator componentConfigurator = null;
                    try {
                        componentConfigurator = (ComponentConfigurator) container
                                .lookup(ComponentConfigurator.ROLE, "basic");
                        componentConfigurator.configureComponent(wagon,
                                                                 plexusConf, container.getContainerRealm());
                    } catch (ComponentLookupException e) {
                        throw new TransferFailedException(
                                "While configuring wagon for \'"
                                + repositoryId
                                + "\': Unable to lookup wagon configurator."
                                + " Wagon configuration cannot be applied.",
                                e);
                    } catch (ComponentConfigurationException e) {
                        throw new TransferFailedException(
                                "While configuring wagon for \'"
                                + repositoryId
                                + "\': Unable to apply wagon configuration.",
                                e);
                    } finally {
                        if (componentConfigurator != null) {
                            try {
                                container.release(componentConfigurator);
                            } catch (ComponentLifecycleException e) {
                                log.error("Problem releasing configurator - ignoring: "
                                          + e.getMessage());
                            }
                        }
                    }
                }
            }
        }
    }

    /** {@inheritDoc} */
    public void contextualize(Context context) throws ContextException {
        container = (PlexusContainer) context.get(PlexusConstants.PLEXUS_KEY);
    }

}