/*
 * #%L
 * Wikitty :: publication Maven plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.plugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.eclipse.jetty.util.Scanner;
import org.eclipse.jetty.xml.XmlConfiguration;
import org.mortbay.jetty.plugin.AbstractJettyMojo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * New definition of the JettyRunWarMojo
 * to set the protected visibility and getter and setter for the webApp file
 *
 * @author mfortun <manoel.fortun@gmail.com>
 */
public class JettyUtil extends AbstractJettyMojo {

    /**
     * The location of the war file.
     *
     * @parameter expression="${project.build.directory}/${project.build.finalName}.war"
     * @required
     */
    protected File webApp;


    public File getWebApp() {
        return webApp;
    }


    public void setWebApp(File webApp) {
        this.webApp = webApp;
    }

    @Override
    public void configureWebApplication() throws Exception {
        super.configureWebApplication();

        webAppConfig.setWar(webApp.getCanonicalPath());
    }

    @Override
    public void checkPomConfiguration() throws MojoExecutionException {
        return;
    }

    @Override
    public void configureScanner() throws MojoExecutionException {
        ArrayList scanList = new ArrayList();
        scanList.add(getProject().getFile());
        scanList.add(webApp);
        setScanList(scanList);

        ArrayList listeners = new ArrayList();
        listeners.add(new Scanner.BulkListener() {
            public void filesChanged(List changes) {
                try {
                    boolean reconfigure = changes.contains(getProject().getFile().getCanonicalPath());
                    restartWebApp(reconfigure);
                } catch (Exception e) {
                    getLog().error("Error reconfiguring/restarting webapp after change in watched files", e);
                }
            }
        });
        setScannerListeners(listeners);

    }

    @Override
    public void restartWebApp(boolean reconfigureScanner) throws Exception {
        getLog().info("Restarting webapp ...");
        getLog().debug("Stopping webapp ...");
        webAppConfig.stop();
        getLog().debug("Reconfiguring webapp ...");

        checkPomConfiguration();

        // check if we need to reconfigure the scanner,
        // which is if the pom changes
        if (reconfigureScanner) {
            getLog().info("Reconfiguring scanner after change to pom.xml ...");
            ArrayList scanList = getScanList();
            scanList.clear();
            scanList.add(getProject().getFile());
            scanList.add(webApp);
            setScanList(scanList);
            getScanner().setScanDirs(scanList);
        }

        getLog().debug("Restarting webapp ...");
        webAppConfig.start();
        getLog().info("Restart completed.");
    }


    @Override
    public void finishConfigurationBeforeStart() {
        return;
    }

    @Override
    public void applyJettyXml() throws Exception {
        if (getJettyXmlFiles() == null)
            return;

        for (File xmlFile : getJettyXmlFiles()) {
            getLog().info("Configuring Jetty from xml configuration file = " + xmlFile.getCanonicalPath());
            XmlConfiguration xmlConfiguration = new XmlConfiguration(xmlFile.toURI().toURL());
            xmlConfiguration.configure(server);
        }
    }

    public void setProject(MavenProject project) {
        this.project = project;
    }
}
