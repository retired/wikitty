.. -
.. * #%L
.. * Wikitty
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
Wikitty
=======

Wikitty is a key/value storage and search system.

Concept
=======

A Java object or a relational table are frozen when writing code or creating
database. To make them evolve, you have to go back into the code, write a
migration process,...

It is also really hard to maintain a coherent code when you want to develop the
same application for similar needs with only little differences. Between the
common code and the specific code, it can easily be a real burden.

Wikitty tries to bring some answers to those problems bringing a technical
solution easy to put into place and to use.

An object never has predefined fields, but only a unique id. You can add
extensions to this object. An extension is the definition of a group of fields.
To knw all the fields of an object, you just have to know which extensions have
been added. For example, we can define the extension Person and the extension
Employee and use them on the same object. The said object will have all the
fields of the Person extension and all the fields of the Employee extension and
will carry the two notions. If the project is used by a company that needs to
add fields to the Employee extension, there is no need to modify the existing
extensions or the database scheme. You just need to create a new extension,
depending on Employee extension, and that you will use to display your Employee
data. If the main project evolve, there is absolutely no impact on the specific
extension used by the company. The core application developers do not have to
worry (or know) about specific stuff that might have been developed by others.
The same way, there is no maintenance costs for people that have made
specific extensions, those extensions are re-usable from version to version
without any modification or maintenance.

Wikitty core brings the base services :

- Bean generating for easy use by developers.
- A WikittyProxy class that masks the Wikitty objects and only uses generated
  Beans.
- Creation, restoration, update, delete of entities.
- Search for entities (full text, facets, ...)
- Tree notion

Some other extra-services exists :

- security : authentification and authorisation
- cache
- notification (synchronisation between two servers, client/server notification,
  listener on modifications)

Complementary add-ons :
- Labels (to labelize objects)
- Import/Export
- Internationalization (i18n)

Technical concept
=================

The concept is two have a core as simple as possible, with a few methods (around
20).
- read
- write
- search
- login/logout
- rights checking (read, write, delete)

The new services (cache, security, notification, ...) are on top
of the core to provide the services.

The core delegate all the work to 3 services that can be implemented using
different technologies :
- extensions storage (in-memory, jdbc, hbase, jpa, ...)
- data storage (in-memory, jdbc, hbase, jpa, ...)
- indexation and search (in-memory, SolR, ...)

So it is possible to add new high-level services, or to add new ways to store
the info.

For some needs, there is no need to implement a new level (WikittyService), so
we speak of AddOn. Those AddOns work directly using the WikittyService API, this
is for example the case for import/export.

The lient part of the applications only work through WikittyProxy who masks
the Wikitty object notion to return Beans that can be easily used by the
developer.

High-level service
==================

A high-level service can contain :
- specific extensions
- a specific service layer
- a Helper class
