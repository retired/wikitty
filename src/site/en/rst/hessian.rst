.. -
.. * #%L
.. * Wikitty
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
Hessian
=======

Wikitty provides the modules necessary to be used as a client/server based on
Hessian.

It is compounded of :
  * a war application that, reading a configuration file, is capable of
    providing a service on any Wikitty storage type.
  * a Factory that returns a proxy on a distant service.

Server
------

The server is available as a a web app (war) ready to be used. You can download
it on this adress : http://www.nuiton.org/projects/wikitty/files

By default, this web app is based on the configuration file
``/etc/wikitty-hessian.properties`` (under Linux). This configuration file
contains 3 configuration options specific to Hessian :

  * wikitty.hessian.usecache : add cache use
  * wikitty.hessian.usenotification : add notification use
  * wikitty.hessian.usesecurity : add security use

Then the configuration file contains the specific configuration of the
storage to use.

Here is a configuration example::

  # security
  wikitty.hessian.usesecurity=true
  wikitty.service.login=xxxx
  wikitty.service.password=zzzz

  # notification
  wikitty.hessian.usenotification=true
  wikitty.service.event.jgroupschannelname=myjgroupchannel
  wikitty.service.event.propagateEvent=false

  # cache
  wikitty.hessian.usecache=true
  wikitty.service.cache.listenevents=true

  # solr configuration
  solr.data.dir=/var/lib/myapp/solr

The application is then accessible on "/wikitty" uri following the deployment
context.
For example : http://localhost:8080/myapp/wikitty

Client
------

The interface for the service can be obtained using the available Factory in
``wikitty-hessian-client`` module.

Example::

  WikittyService service = getWikittyService("http://localhost/wikitty");

The ``service`` can then be used as a local service.
