.. -
.. * #%L
.. * Wikitty
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
Manage data migration using Wikitty
-----------------------------------

Presentation
============

In Wikitty, migration is done each time you load an object. If an object is
present in the database in an old version, when it is loaded from the database,
it is automatically migrated to the new version. This way, you avoid costly
migration at initialisation.

The only thing to bear in mind is that data which was not migrated is still
indexed in the old version and might not be returned by requests. If you need
the data to be indexed on the new version, you still can migrate all your
objects at initialisation by loading all the objects and store them.

How migrate data properly in Wikitty
====================================

After your application launch and before any request, you should store all the
extensions needed by your application, in their last version ::

  store(myExtension);

Then you need to add to the registry all the specific migrations for
extensions::

  WikittyExtensionMigration.migrationRegistry.put("myExtension", myMigrationClass);

You can now use your data properly, they will be migrated at loading time.

If you want to migrate objects before using them (for requests purpose for
example), for each version, search all you objects, restore and then store
them::

  search(Client)
  restore(Client)
  store(Client)

Take care to the cost of the indexation compared to the data volume.

Specific migrations
===================

Wikitty makes an automatic migration in cas of deleted fields, added fields or
renamed fields (need to add a renameFrom tag on the field).

For other kind of migration, you might want to create your own migration class
that implements WikittyExtensionMigration. This class only needs to make the
migration from a version to the next version, for example version 1 to version 2.
In cas of a migration needed from version 1 to version 4, Wikitty will
automatically call the classes to migrate form version 1 to 2, then from version
2 to 3 and so on.
