.. -
.. * #%L
.. * Wikitty
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Ce document est fait pour contenir toutes les normalisations autour de Wikitty

Identifiant des wikitties
=========================

Les identifiants sont de la forme UUID[_<specifique extension>]. Donc tous les
identifiants commencent par un UUID et peuvent être séparé d'une extension
spécifique pour certains besoins par un '_' (underscore).

C'est le mécanisme de base de génération des wikitty-id. Il y a toutefois
des exceptions :
* Pour les méta-extensions : chacun des wikitties aura pour
identifiant par exemple « WikittySecurity:MonExtension ».
Cela permet de court-cuircuiter la recherche solR, et un passage par
le réseau à chaque store/restore.

Un wikitty en base a toujours un identifiant, dès instanciation.

WikittyServiceNotifier
======================

Tous les évènements passant par le WikittyServiceNotifier sont envoyé au
RemoteNotifier s'il est indiqué dans la configuration, qu'il faut propager les
évènements (wikitty.service.event.propagateEvent). Il faut alors dans ce cas
définir le transporter à utiliser (wikitty.notifier.transporter.class).

Le transporter sert à envoyer(serveur) et recevoir(client) les évènements. Le
même transporter doit être utilisé pour le client et le serveur.

Il existe aujourd'hui deux types de transporter différents (jgroups, xmpp).
L'implantation jgroups pose quelque soucis (ne fonctionne pas toujours). Il
est donc préférable d'utiliser le transporter xmpp.

Il est bien sûr possible d'empiler deux couches de WikittyServiceNotifier
avec des transporters différents coté serveur pour permettre à différents
types de client de fonctionner en même temps (FIXME poussin 20101115 il n'est
pas actuellement possible de configurer ce fonctionnement)

Pour utiliser le transporter xmpp il faut que vous ayez un serveur xmpp avec
une room acceptant le login anonyme. Pour cette fonctionnalité, il n'y a pas
besoin que la room soit archivée.

Synchronisation de serveur
==========================

On peut avoir besoin d'avoir deux serveurs qui soit synchronisé.
Actuellement il n'existe que du maitre/esclave.

Pour mettre en place la synchronisation il faut que le serveur maitre
utilise la couche de notification avec le transporter xmpp. Il faut que la
room xmpp soit archivée.

Pour que la synchornisation fonctionne il faut:
- garantir que tous les events soient bien envoyés
- garantir que tous les events soitent bien reçus

et ceci même si le serveur xmpp s'arrête ou que le client s'arrête. Si le
serveur s'arrête plus aucun event ne sera produit. Il faut seulement
garantir que lorsqu'un event à été enregistrer sur le serveur il sera bien
envoyé aux clients.

Lorsque le serveur envoie un event, il lui fixe un numero d'ordre qu'il
stocke et qu'il incrémentera et réutilisera pour l'envoi suivant.

Lorsque le client réceptionne l'event, il vérifie qu'il n'y a pas de rupture
dans la séquence des numeros d'envois. Si c'est le cas, il doit récupérer
les numeros manquants. (il redemande l'historique des messages pour les N
messages qui lui manque)

A intervale régulier il enregistre un fichier de synchro contenant, la date du serveur
(il lui demande via le protocole xmpp) et le numero (+id?) du dernier event
reçu. Ce fichier n'est enregistré que si on est bien à jour dans les données
reçu (pas de rupture détectée dans la sequence des events).

Lorsque le client démarera pour la 1ère fois, il faut que la base de données
ait été remplie manuellement, son indexe recréer, et le fichier de synchro
créé convenablement.

Lorsque le client redémarre après un arrêt, il redemande l'historique depuis
la date du fichier de synchro.

Le numéro unique d'event est généré par la couche de persistance et stocké
sur le serveur en même temps que les données.


Deux types de client:
- volatile: ceux intéressé pour récupérer les events lorsqu'ils sont présent
- persistant: ceux intéressé pour récupérer tous les events même lorsqu'ils sont non dispo (coupure réseau)

Le client ouvre un socket et le serveur laisse la connexion ouverte pour pouvoir
envoyer régulièrement les infos de notification.
Si un client volatile disparait (impossible de lui envoyer l'info), on
l'enleve de la liste des clients a prevenir
Si un client persistant disparait, on met dans une file toutes les
notifications qu'on a pas pu lui envoyer et on lui renvera tout ce qu'il a
manqué lorsqu'il sera de retour. (utile pour la synchro serveur)

Localisation
============

Il s'agit de traduire les noms de champs et pas les valeurs.On rajoute une méta-extension WikittyI18n aux extensions du wikitty.

Cette pseudo-extension a deux champs :
* language String
* translation String

personne:wikittyI18n.language=fr,en,es
personne:wikittyI18n.translation=[fr:name=prénom,surname=nom],[en:name=name,surname=surname]
personne.name=value

ton extension : security des champs de sécurité
ton extension : i18n des champs i18n
ton extension.champs=valeur dans la langue

un wikitty ne peut exister que dans une langue à la fois, si tu veux plusieurs langues,
tu charges plusieurs fois le wikitty.

du coup ton wikitty a une version et une langue. 
On stocke dans la définition de l'extension chargée la langue dans laquelle elle a été chargée.
si pas de i18n, du coup c'est la langue par défaut à chaque fois.

Au moins les writers peuvent créer une nouvelle langue.

pas de sécurité par langue, trop le bordel !

il serait agréable que seules les extensions demandées soient chargées.

dans les restore il faut ajouter (extensions, id, + préchargement d'extensions)

attention au moment de la migration
au moment du chargement




On ajoute WikittyService#allowTranslation(token, extension, boolean)


Pseudo extension
================

migration : méthode migrate(oldExtension, newExtension, Wikitty, Locale) Locale pouvant etre null.
