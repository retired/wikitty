.. -
.. * #%L
.. * Wikitty
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin, Benjamin Poussin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=========================
Fonctionnement de Wikitty
=========================

Concept métier
==============

Un objet java ou une table relationnelle sont des choses figées au moment de
l'écriture du code ou la création de la base de données. Pour les faire évoluer
par la suite, il faut donc retourner dans le code, prévoir la migration,
etc.

Il est aussi difficile de maintenir un code cohérent lorsque l'on veut
développer la même application pour des besoins semblable mais qui ont
chacun de petites spécificités qui rend le code difficile à maintenir sur le
long terme, entre le code commun et le code spécifique.

Wikitty essaie de répondre à ces différentes problèmatiques en apportant une
solution technique simple à mettre en oeuvre et à utiliser.

Un objet n'a pas de champs prédéfini, mais seulement un identifiant unique.
Pour connaître les champs disponibles sur un objet, il faut regarder les
extensions qui lui ont été ajoutées. Une extension est la définition d'un
ensemble de champs. Par exemple nous pouvons définir l'extension Personne et
l'extension Employer et les utiliser sur le même objet. L'objet aura donc
tous les champs Personne et tous les champs Employer il portera donc les
deux notions en même temps. Si pour le projet est utilisé par une société
qui à besoin de champs complémentaire à Employer, il n'y a pas besoin de
modifier les extensions existantes ou modifier le schéma de la base. Il faut
simplement créer une extension spécifique que l'on ajoutera à Employer et
que l'on pourra utilisé pour la visualisation de l'Employer. Si le projet
principale évolue, cela n'a aucun impact sur l'extension spécifique de cette
société. Il n'y a donc aucun effort à faire pour maintenir des extensions
spécifiques. Les développeurs du coeur de l'application n'ont pas besoin de
se soucier de chose spécifique qui aurait pu être développer. De la même
façon, il n'y a pas de surcoût de maintenance pour les personnes ayant fait
des extensions spécifiques, ces extensions sont réutilisables de version en
version sans modification ou maintenance.

Le coeur de Wikitty apporte les services de base:

- génération de Bean métier pour une utilisation simple par le développeur
- une classe WikittyClient qui masque les objets Wikitty et manipule les Bean
  métier généré et offre tout un ensemble de service complémentaire.
- Création, sauvegarde, restoration des Entités
- Recherche des entités (fulltext, facette, ...)
- notion d'arbre

Les services additionnels existants:

- sécurité: authentification et autorisation
- cache
- notification (synchronisation de deux serveurs, notification en
  client/serveur, listener sur les modifications)

Les addons complémentaires:

- notion de label (pour étiqueter des objets)
- import/export
- internationnalisation (i18n)

Concept technique
=================

Le concept est d'avoir un coeur le plus simple possible avec quelques methodes
(une vingtaine).

- lecture
- ecriture
- recherche
- login/logout
- check de droit (read, write, delete)

Les nouveaux services (cache, securité, notification, ...) s'intercale en
couche au dessus du coeur pour founir les services.

Le coeur délègue le travail à trois services qui peuvent être implanté avec
des techologies différentes:

- stockage des extensions (mémoire, jdbc, hbase, jpa, ...)
- stockage des données (mémoire, jdbc, hbase, jpa, ...)
- indexation et recherche (mémoire, SolR, ...)

Il est donc possible d'ajouter de nouveau service de haut niveau, ou
d'ajouter de nouvelle façon de stocker l'information.

Pour certain besoin il n'y a pas d'obligation d'implanter une nouvelle
chouche (WikittyService). On parle alors de AddOn. C'est addon travail
directement grace à l'API WikittyService, c'est le cas par exemple de
l'import/export.

La partie cliente des applications ne travail qu'avec le WikittyClient qui
masque la notion d'objet Wikitty pour retourner des beans metiers facilement
manipulable par le développeur.

Service haut niveau
===================

Un service de haut niveau peut contenir:

- des extensions spécifiques
- une couche de service specifiques
- une classe Helper
