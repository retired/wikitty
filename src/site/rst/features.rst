.. -
.. * #%L
.. * Wikitty
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

===============
Fonctionnalités
===============

Indexation/Recherche/Facettes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Wikitty est un système qui, en plus de stocker les données, les indexe et permet
de les rechercher en fournissant les réponses facetisées. Ainsi, vous pouvez
réduire le nombre de requêtes nécessaires à l'affichage d'un écran complexe.

Typiquement, la majorité des sites de e-commerce affiche des résultats de
recherche facettisés, il est donc potentiellement possible d'afficher la
page de résultats avec une seule requête.

Securité
~~~~~~~~

Wikitty fournit un système de gestion des droits en lecture et en écriture pour
les entitées. Vous pouvez aller jusqu'à limiter l'accès à certains objet d'un
type à certains utilisateurs. Vous pouvez également accorder des droits
différents à certains groupes de champs des objets (sécurité au niveau des extensions).

Prenons par example un objet Employe.
Cet objet peut avoir des champs de type salaire,... qui ne sont modifiables que
par les membres du service RH, mais visibles par l'employé, alors que les
champs adresse par example sont modifiables par l'employé et les membres du
service RH.

Transactions
~~~~~~~~~~~~

Wikitty fournit un système de transactions. A la lecture des données, il
recherche dans la transaction puis dans la base de donnée s'il ne trouve pas la
donnée. Lorsqu'il écrit une donnée, il l'écrit dans la transaction. Au commit de
la transaction, toutes les opérations sont envoyées à la base de donnée Wikitty,
si un problème survient (mauvais droits, ...), la transaction n'est pas
enregistrée. Pour les recherches, une fusion est opérée entre les résultats sur
la transaction et ceux sur la base Wikitty.

Notifications
~~~~~~~~~~~~~

Wikitty fournit un système de notifications basé sur les évènements, permettant
à un client de savoir qu'un objet a été modifié en base par un autre client par
exemple et ainsi rafraichir la vue de l'application.

Il est possible de dissocier les évènements envoyés par d'autres clients de
ceux que l'on a créés afin de différencier le traitement si nécessaire.

Cache
~~~~~

Wikitty fournit un système de cache qui peut être réparti entre les clients,
les clients et le serveur,... garantissant toujours la cohérence des données
tout en réduisant les temps de réponse et les accès réseau/disque,...

Si le cache reçoit des notifications d'objets modifiés en base, il les
enlèves de sa base, à la prochaine requête sur cet objet, il ira le chercher
dans la base.

Migration
~~~~~~~~~

Wikitty gère tout seul les migrations nécessaires suite aux changements apportés
au modèle métier. Le développeur n'a plus à se soucier des migrations de
données. Les données sont migrées à la lecture, réduisant ainsi le temps de
migration des données au démarrage de votre application.

Les migrations complexes sont toujours possibles en surchargeant la migration
automatique de Wikitty.

Import/Export
~~~~~~~~~~~~~

Wikitty fournit un add-on d'import/export. Cet add-on permet d'importer et
exporter des données au format CSV tout en gérant les problèmes de données
modifiées, version des données,...

Le format CSV est spécifique à Wikitty et non modifiable, mais il permet
l'insertion de jeux de tests, par exemple, sans passer par des imports/exports
métiers couteux en développement et en tests.

Réplication
~~~~~~~~~~~

Wikitty fournit un service de réplication qui permet de disposer d'un serveur
maître, responsable de la cohérence des données. et de serveurs esclaves. Cela
permet de disposer de plusieurs serveurs et ainsi répartir la charge entre ces
derniers.

Label
~~~~~

Wikitty permet de labelliser les données afin de grouper des données de types
différents ensembles.
