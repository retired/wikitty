.. -
.. * #%L
.. * Wikitty
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=======
Wikitty
=======

Qu'est-ce que Wikitty ?
-----------------------

Wikitty est un système de stockage et de recherche de type clé/valeur
proposant de nombreux services destinées à la création simples d'applications
modulaires.

Les besoins
~~~~~~~~~~~

Un objet java ou une table relationnelle sont des choses figées au moment de
l'écriture du code ou la création de la base de données. Pour les faire évoluer
par la suite, il faut donc retourner dans le code, prévoir la migration,
etc.

Il est aussi difficile de maintenir un code cohérent lorsque l'on veut
développer la même application pour des besoins semblable mais qui ont
chacun de petites spécificités qui rend le code difficile à maintenir sur le
long terme, entre le code commun et le code spécifique.

Wikitty essaie de répondre à ces différentes problèmatiques en apportant une
solution technique simple à mettre en oeuvre et à utiliser.

Type d'applications
~~~~~~~~~~~~~~~~~~~

Wikitty a été créé avec l'idée de fournir, essentiellement pour les applications
de gestion, que ce soit de la gestion de contenus, de documents ou de sociétés.
Ces applications ont souvent une base commune qui nécessite d'être adaptée aux
besoins de chacun. Wikitty cherche a simplifier la création et la maintenance de
ce genre d'applications.

Avantages
~~~~~~~~~

Wikitty présente plusieurs avantages au développeur :

- **Modularité** : Vous pouvez choisir d'utiliser, toutes les fonctionnalités
  proposées, aucune, certaines, uniquement par configuration. Les modules
  peuvent être rajoutés au fur et à mesure de l'évolution de votre application.
  Vous pouvez ainsi commencer par une application simple et rajouter
  des services avec le temps, comme le cache, la sécurité, la réplication, ...
- **Modèle métier simple à modifier** : Wikitty gère tout seul les changements
  dans le modèle métier de votre application. Vous n'avez plus à vous soucier
  des migrations de données,... De plus, les données sont migrées à la lecture,
  réduisant ainsi le temps de migration des données au démarrage de votre
  application. Et si vous souhaitez effectuer une migration complexe, vous
  pouvez toujours surcharger la migration automatique de Wikitty et ainsi rester
  complètement maître des données.
- ** Génération des objets métier**:Vous n'avez pas à vous soucier de comment
  faire pour que les objets métiers soient gérés par Wikitty, ils sont générés à
  partir du modèle UML métier de l'application.
- **Héritage multiple**: Wikitty permet une chose qui n'est habituellement pas
  possible d'effectuer en Java : l'héritage multiple entre les objets métier.
  Vous n'avez ainsi plus de limitation dans la modélisation de votre métier et
  pouvez le représenter tel qu'il est.

Les fonctionnalités
-------------------

Les fonctionnalités principales de Wikitty sont :

- Indexation/Recherche/Facettes
- Securité
- Transactions
- Notifications
- Cache
- Migration
- Import/Export
- Réplication
- Label/Groupe

Pour plus d'informations sur les différentes fonctionnalités, référez-vous à la
`page correspondante`_

.. _page correspondante: features.html

Technologies
------------

Wikitty est basé sur un système de briques : les services et les add-ons.
Wikitty fournit une implémentation de chaque brique basé sur une technologie,
mais il est tout a fait possible de changer d'implémentation et vous pouvez
d'ailleurs réaliser vos propres implémentations.

- **Indexation** : Actuellement basée sur SolR
- **Stockage** des données : Actuellement basé sur JDBC, mais nous avons aussi
  des prototypes basés sur HBase et JPA.
- **Transport des données** (pour les services de notification notamment) est
  actuellement basé sur XMPP. Nous avons aussi un prototype basé sur JMS.
- Administration : Nous avons réalisé un prototype d'interface d'administration
  des données basé sur la technologie Zk.

Déploiements types
------------------

Web - Application Swing monoposte
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dans le cas d'un déploiement web ou d'une application cliente mono-poste, les
services métiers appelent directement le
serveur Wikitty. Ce dernier peut comprendre les couches de sécurité, cache et
notification suivant les besoins.

.. image:: schemes/webDeployment.png

Application Swing multiposte
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dans le cas d'une application Swing multiposte, les services métiers utilisent
un client Wikitty qui possèdent les couches de sécurité, cache et notification.
Le client envoie les modifications et les requêtes à un serveur Wikitty qui
possède lui aussi les couches de sécurité, cache et notification. Lorsqu'une
modification est apportée sur le serveur Wikitty, tous les clients sont notifiés
et peuvent mettre à jour ou pas les écrans.

.. image:: schemes/swingDeployment.png

Multi serveur
~~~~~~~~~~~~~

Dans le cas d'un déploiement multi-serveur, les écritures et lectures passent
par un serveur esclave, les écritures sont déléguées au serveur maître puis
réeffectuées sur les serveurs esclaves. Les lectures sont effectuées sur le
serveur esclave.

.. image:: schemes/multiServerDeployment.png

