.. -
.. * #%L
.. * Wikitty
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

======================================
Générer son modèle métier vers Wikitty
======================================

Modélisation
============

Vous pouvez modéliser votre modèle métier en utilisant n'importe quel modéliseur
UML pouvant enregistrer le modèle au format xmi (ArgoUML, Topcased,...). Les
entités que vous souhaitez générer doivent avoir le stéréotype *entity*.

Configuration Maven
===================

Pour générer le code, nous utilisons EUGene_ et son plugin Maven.

Dans le pom de votre projet, rajoutez la définition du plugin::

    <plugin>
      <groupId>org.nuiton.eugene</groupId>
      <artifactId>maven-eugene-plugin</artifactId>
      <version>${eugeneVersion}/version>
      <executions>
          <execution>
              <id>api generator</id>
              <phase>process-sources</phase>
              <configuration>
                  <inputs>
                      <input>zargo:src/main/xmi/:*.zargo</input>
                      <input>classpath:model:/:wikitty.objectmodel
                      </input>
                  </inputs>
                  <fullPackagePath>your.app.package</fullPackagePath>
                  <defaultPackage>your.app.package</defaultPackage>
                  <extractedPackages>
                      your.app.package,org.nuiton.wikitty
                  </extractedPackages>
                  <generatedPackages>your.app.package</generatedPackages>
                  <templates>
                      org.nuiton.wikitty.generator.WikittyMetaGenerator,
                      org.nuiton.wikitty.generator.WikittyDTOGenerator
                  </templates>
              </configuration>
              <goals>
                  <goal>smart-generate</goal>
              </goals>
          </execution>
      </executions>
    </plugin>

Dans la section *inputs*, on spécifie le(s) modèle(s) métier::

    zargo:src/main/xmi/:*.zargo

ainsi que le modèle Wikitty (en cas de dépendance de votre modèle métier vers le
modèle Wikitty).

Dans la section *templates*, vous pouvez spécifier quoi générer :
*org.nuiton.wikitty.generator.WikittyMetaGenerator* pour la génération Wikitty
standard et *org.nuiton.wikitty.generator.WikittyDTOGenerator* pour la
génération des DTOs si besoin (utilisation d'une UI GWT par exemple).

Génération
==========

La génération est effectuée ensuite à chaque fois que vous lancez un build.

Vous pourriez être étonné que le lien entre vos entités soit représenté par une
chaîne de caractère. En fait, dans vos entité Wikitty, un lien vers un autre
Wikitty est représenté par son identifiant qui est une chaîne de caractères.
Ainsi, un getXXXXX vous retournera l'identifiant d'un Wikitty qu'il faudra
récupérer en passant par le WikittyClient. Vous pouvez alors utiliser le cache
pour limiter les appels côté serveur.

Subtilités de modélisation
==========================

Dépendances vers l'API de Wikitty
---------------------------------

Il est possible de rajouter une dépendance vers un objet de l'API Wikitty (par
example WikittyUser) en rajoutant simplement la classe dans votre modèle dans le
package *org.nuiton.wikitty.entities*. Elle ne sera pas générée mais interviendra
comme une autre classe de votre modèle (héritage,...).

.. image:: wikittyDependency.png

Arbres
------

Si vous souhaitez qu'une entité crée un arbre, vous avez juste à faire hériter
cette entité de WikittyTreeNode (pas besoin de référence sur elle-même).  De
cette manière, la gestion de l'arbre est assurée par Wikitty, de même que
l'indexation et la recherche.

.. image:: wikittyTree.png

.. _EUGene: http://maven-site.nuiton.org/eugene/

Tag Value disponible
====================

L'ensemble des tags disponible se trouve dans la classe WikittyTagValue.

:version: s'applique au extension, indique la version de l'extension. Il est
  obligatoire de modifier la valeur de la version lorsque vous modifiez la
  définition de votre extension.

:alternativeName: s'applique au champs, ce tag peut etre mis pour qu'a la
  generation un des accesseur avec un autre nom soit genere.

:documentation: s'applicque au extension et au champs. Il indique la
  documentation a mettre lors de la generation peut aussi servir de
  documentation utilisateur a l'execution.

:toString: s'applique au extension et indique la facon de representer une
  extension textuellement. Par exemple::

  %Person.lastName$s %Person.firstName$s: %Person.birthday$tm %Person.birthday$te,%Person.birthday$tY
  Hello %Person.firstName|unknow$s

In last example, if firstName field doesn't exist, unknow is used

:sortOrder: s'applique au extension et indique le tri par defaut pour cette
  extension. C'est une liste des champs dans l'importance de l'ordre de trie.
  Il est possible d'ajouter asc ou desc apres le champs pour indique qu'il faut
  trier en ordre croissant ou decroissant. Par exemple::

  "Person.lastName asc, Person.firstName, Person.birthday desc"

:Unique: s'applique au champs de type collection et indique s'il vaut
  true que la collection ne peut pas contenir de doublon (Set) si unique
  est false alors la collection peut contenir des doublons (List).

:notNull: s'applique au champs et indique que le champs ne peut pas etre null,
  il doit forcement avoir une valeur lors de la sauvegarde.

:pattern: s'applique au champs et indique pour une String indique que le champs
  doit respecter un certain pattern, sinon la sauvegarde echoue. Pour un numeric
  ou une date pattern peut-etre utilise comme masque de saisie. Exemple::

  on"A.*" le champs doit commencer par 'A'
  "[0-9]+\.[0-9][0-9]" le nombre doit avoir deux chiffres apres la virgule

:indexed: s'applique au champs et indique si un champs doit etre indexe ou non.
  par defaut si ce tag value n'existe pas il vaut true. Il faut donc le
  positionner explicitement a false si on ne veut pas indexer un champs

:crypt: s'applique au champs et indique que le champs doit etre crypte avant
  d'etre sauve et decrypter lors de la restauration, de plus le champs ne
  sera pas indexe. Si le cryptage n'est pas possible, la sauvegarde ne se
  fera pas (Exeption). Par exemple::

    crypt=Blowfish:password

:extensionAllowed: s'applique au champs de type Wikitty et indique que le champs
  ne peut prendre comme valeur que des wikitties ayant les extensions specifiees.
  Par exemple::

    Person,Employee;Company
    Ici il faut que l'objet ait les extensions Person et Employee ou l'extension Company

:preload: s'applique au extension et indique les champs de type Wikitty qui
  doivent etre preloade lors du chargement de cette extension. Par exemple::

    preload="Company.employee,Employee.person;Company.address"
