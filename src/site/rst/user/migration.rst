.. -
.. * #%L
.. * Wikitty
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Gérer la migration des données dans Wikitty
-------------------------------------------

Présentation
============

Dans Wikitty, la migration est effectuée à chaque fois que vous chargez un
objet. Si un objet est présent dans la base de donnée dans une ancienne version,
quand il est chargé depuis la base de donnée, il est automatiquement migré dans
la nouvelle version. De cette manière, vous pouvez éviter les migrations
coûteuses à l'initialisation.

La seule chose à garder à l'esprit est que les données qui n'ont pas encore
été migrée sont encore indexées dans l'ancienne version et peuvent ne pas être
retournées par certaines requêtes. Si vous avez besoin que les données soient
indexées dans la nouvelle version, vous pouvez toujours migrer tous vos objets
à l'initialisation en chargeant les objets et en les enregistrant.

Comment migrer proprement les données dans Wikitty
==================================================

Après avoir lancé votre application et avant toutes vos requêtes, vous devez
enregistrer toutes les extensions dont votre application a besoin dans leur
dernière version::

  wikittyClient.storeExtension(myExtension);

ou::

  wikittyClient.storeExtension(myEntityImpl.extensionMyEntity)

Si vous avez des migrations spécifiques à effectuer, ajoutez au registre toutes
les migrations spécifiques des extensions::

  wikittyClient.getMigrationRegistry().put("myExtension", myMigrationClass);

Vous pouvez maintenant utiliser vos données simplement, elles seront migrées
au chargement.

Si vous voulez migrer vos objets avant de les utiliser (pour quelle soit 
convenablement indexée par exemple), pour chaque version, recherchez vos objets,
restaurez les puis enregistrez-les::

  WikittyQueryResult<Client> all = wikittyClient.findAllByExample(new Client());
  wikittyClient.store(all.getAll());

Attention au coût d'une indexation au vu du volume de données, il n'est pas
toujours judicieux de réindexer toutes les données. Si vous avez beaucoup
d'objet, il est potentiellement judicieux d'utiliser la pagination pour restaurer
les objets par paquet de 1000 par exemple::

  int LIMIT = 1000;
  int first = 0;
  Client example = new Client();
  WikittyQueryResult<Client> all = wikittyClient.findAllByExample(example, first, LIMIT);
  while (all.size() >= LIMIT) {
    wikittyClient.store(all.getAll());
    first += LIMIT;
    all = wikittyClient.findAllByExample(example, first, LIMIT);
  }
  wikittyClient.store(all.getAll());

Migrations spécifiques
======================

Wikitty réalise une migration automatique en cas de champs supprimés, ajoutés
ou renommés (dans ce cas, il faut ajouter un tag renameFrom sur le champ).

Pour toutes les autre migrations, vous devrez créer votre propre classe de
migration qui implante WikittyExtensionMigration. Cette classe n'a besoin de
faire la migration d'une version à l'autre, par exemple de la version 1 à la
version 2. Dans le cas d'une migration de la version 1 à la version 4, Wikitty
appellera automatiquement les classes pour migrer de la version 1 à la version 2
puis de la version 2 à la version 3 et ainsi de suite.

Le plus simple pour imlanter votre propre migration est le plus souvent d'étendre
la classe WikittyExtensionMigrationRename qui va permettre de géré le plus de
chose possible. Il ne vous restera ensuite que les modifications que vous
souhaitez réellement faire à implanter.::

    public static class MyMigration extends WikittyExtensionMigrationRename {
        public Wikitty migrate(WikittyService service, Wikitty wikitty,
                WikittyExtension oldExt, WikittyExtension newExt) {
            // ici le wikitty contient l'ancienne extension et les anciens champs
            Wikitty result = super.migrate(service, wikitty, oldExt, newExt);
            // ici result est la version migre de l'objet (nouvelle extension)
            // Vous pouvez faire ici des traitement spécifique
            return result;
        }
    }

Utilisation du fichier de configuration
=======================================

Pour simplifier l'enregistrement des classes de migration et évité de devoir
passer par de la programmation vous pouvez ajouter des entrées dans le fichier
de configuration, par exemple pour utilier la classe **monpackage.MyMigration**
pour la migration de l'extension **MyExtensionName**, ajoutez dans le fichier::

  wikitty.migration.class.MyExtensionName=monpackage.MyMigration

