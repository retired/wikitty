.. -
.. * #%L
.. * Wikitty
.. * %%
.. * Copyright (C) 2009 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=========================
FAQ - Foire aux questions
=========================

Comment récupérer uniquement les objets que l'utilisateur peut lire ?
=====================================================================

Dans Wikitty, si vous gérez la sécurité et qu'un utilisateur n'a pas le droit de
lire un objet que vous demandez, null sera retourné. Dans le cas de listes
d'objets, cela peut vite devenir génant de devoir vérifier que l'objet retourné
n'est pas nul. Une manière de contourner ceci est de limiter les résultats de
recherche aux objets que l'utilisateur à le droit de lire.

Exemple
~~~~~~~

Dans cet exemple, le droit de lecture est accordé soit au privateGroup, soit à
tout le monde. On vérifie donc que l'utilisateur authentifié fait soit partie du
privateGroup soit on restreint les résultats aux objets dont le reader n'est pas
le privateGroup::

  // Récupère le privateGroup (méthode métier)
  WikittyGroup privateGroup = getPrivateGroup();

  // Récupère l'utilisateur authentifié
  WikittyUser user = wikittyClient.getUser();

  // Récupère les membres du privateGroup
  Set<String> members = privateGroup.getMembers();

  // Vérifie si l'utilisateur fait partie du privateGroup
  if (members != null && user != null &&
        members.contains(user.getWikittyId())) {
    // L'utilisateur fait partie du privateGroup, on ne change rien à la
    // requête.
  } else {
    // L'utilisateur ne fait pas partie du privateGroup, on restreint
    // donc les résultats de notre query.
    query.neq(WikittyAuthorisation.FQ_FIELD_WIKITTYAUTHORISATION_READER,
            privateGroup.getWikittyId());
  }
