.. -
.. * #%L
.. * Wikitty
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Hessian
=======

Wikitty fournit de base les modules nécéssaires à une utilisation de type
client serveur basé sur hessian.

Il est composé :

  * d'une application war qui, en lisant une configuration, est capable de
    fournir un service sur n'importe quel type de stockage wikitty
  * d'une factory revoyant un proxy hessian sur un service distant

Serveur
-------

Le serveur est disponible sous forme d'une application web (war) prete à
l'emploi. Vous pouvez la télécharger à l'adresse suivante :
http://www.nuiton.org/projects/wikitty/files

Par défaut, cette application web se base sur le fichier de configuration
``/etc/wikitty-hessian.properties`` (sous linux). Ce fichier de configuration
contient 3 options de configuration spécifique à hessian:

  * wikitty.hessian.usecache : ajout du cache
  * wikitty.hessian.usenotification : ajout de la notification
  * wikitty.hessian.usesecurity : ajout de la securité

Ensuite, le fichier de configuration contient la configuration specifique
à l'implementation du stockage à utiliser.

Voici un exemple de configuration::

  # security
  wikitty.hessian.usesecurity=true
  wikitty.service.login=xxxx
  wikitty.service.password=zzzz
  
  # notification
  wikitty.hessian.usenotification=true
  wikitty.service.event.jgroupschannelname=myjgroupchannel
  wikitty.service.event.propagateEvent=false
  
  # cache
  wikitty.hessian.usecache=true
  wikitty.service.cache.listenevents=true
  
  # solr configuration
  solr.data.dir=/var/local/myapp/solr

L'application est ensuite accessible sur l'uri "/wikitty" suivant le contexte
de déploiement.
Par exemple : http://localhost:8080/myapp/wikitty

Client
------

L'interface sur le service s'obtient en utilisant la factory disponible
dans le modules ``wikitty-hessian-client``.

Example::

  WikittyService service = getWikittyService("http://localhost/wikitty");

Le ``service`` s'utilise ensuite comme un service local.
