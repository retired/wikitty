.. -
.. * #%L
.. * Wikitty
.. * %%
.. * Copyright (C) 2009 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

======================
Recherche avec Wikitty
======================

Cette API est complètement dépréciée, il faut maintenant utiliser l'API QUERY

Wikitty permet de rechercher parmis les entités enregistrées tout en fournissant
les fonctionalités de pagination. Pour cela vous pouvez construire
programmatiquement vos recherches en utilisant un système de Criteria.

Structure d'une requête
-----------------------

Pour créer une recherche, vous commencez par initier un arbre de contrainte::

  Search myQuery = Search.query();

La racine est un noeud ET. Vous pouvez ensuite rajouter des contraintes ou des
sous-noeuds.

Par exemple, la requette A ET (B OU C) se représente en arbre::

  ET
  |- A
  `- OU
     |- B
     `- C

Et de manière programmatique, nous aurons (les contraintes A, B et C sont
représentées par des méthodes similaires. Elle devraient-être remplacées par les
vraies méthodes de contrainte)::

  //Ajout de la contrainte A

  myQuery.A();

  //Ajout du node OU, on le récupère pour lui ajouter des fils
  Search orNode = myQuery.or();

  //Ajout des fils du OU
  orNode.B().C();

Les contraintes disponibles
---------------------------

De nombreuses contraintes sont disponibles pour créer vos recherches :

* Egalité : eq - Vérifie si un champ est égal a une valeur.

* Non égalité : neq - Vérifie si un champ n'est pas égal a une valeur.

* Contient : contains - Vérifie si un champ multivalué contient une valeur.

* Dans : in - Vérifie si un champ est compris dans une liste de valeurs.

* Egalité d'extension : exteq - Vérifie si un Wikitty possède une extension

* Non égalité d'extension : extneq - Vérifie qu'un Wikitty ne porte pas une
extension.

* Égalité d'extension : ideq - Vérifie qu'un Wikitty porte l'identifiant

* Non égalité d'identifiant : idneq - Vérifie qu'un Wikitty ne porte pas
l'identifiant.

* Similitude : like - Vérifie qu'une chaîne de caractère est présente dans un
champ (ex : "pit" est présent dans "chapiteau").

* Non similitude : unlike - Vérifie qu'une chaîne de caractère n'est pas
présente dans un champ.

* Supérieur strictement : gt - Vérifie qu'un champ est strictement supérieur à
une valeur.

* Supérieur ou égal : ge - Vérifie si un champ est supérieur ou égal à une
valeur.

* Inférieur strictement : lt - Vérifie si un champ est strictement inférieur à
une valeur.

* Inférieur ou égal : le - Vérifie si un champ est inférieur ou égal à une
valeur.

* Entre : bw - Vérifie si un champ est compris entre deux valeurs.

* Commence par : sw - Vérifie si un champ de type chaîne commence par une valeur.

* Ne commence pas par : nsw - Vérifie si un champ de type chaîne ne commence pas
par une valeur.

* Termine par : ew - Vérifie si un champ de type chaîne termine par une valeur.

* Ne termine pas par : notew - Vérifie si un champs de type chaîne ne termine
pas par une valeur.

* Mot-clé : keyword - Vérifie si la valeur est présente dans n'importe quel
champ du Wikitty.

* Nul : isNull - Vérifie si la valeur d'un champ est nulle.

* Non nul : isNotNull - Vérifie si la valeur d'un champ n'est pas nulle.

Les noeuds à sous-requête
-------------------------

Les noeuds à sous requêtes permettent de faire des requêtes complexes.

Non : not - Résultat inverse de la sous-requête.

Ou : or - Ou entre les différentes sous-requêtes.

Et : and - Et entre les différentes sous-requêtes.

Requête associée : associated - Permet d'effectuer des jointures.

Les critères
------------

On obtient le critère de recherche à partir de la requête en faisant ::

  mySearch.criteria();

Les critères de recherches permettent d'ajouter un ordre ou des facettes :

* Nom : name - Nommage du criteria.

* Restrictions : restriction - Liste des restrictions du criteria.

* Selection : select - Ajout d'un select pour définir le champs des Ids à retourner.

  .. Note:: Peut entrainer des problèmes de performances si beaucoups de résultats sont retournés.

* Premier index : firstIndex - Permet de définir le premier index à retourner. Principalement utilisé pour la pagination.

* Dernier index : endIndex - Permet de définir le dernier index à retourner. Principalement utilisé pour la pagination.

* [TODO] facetMinCount

* [TODO] facetLimit

* [TODO] facetCriteria

* [TODO] facetField

* Tri : sortAscending - Ajout d'un ou plusieurs champs pour le tri ascendant du résultat.

* Tri : sortDescending - Ajout d'un ou plusieurs champs pour le tri descendant du résultat.

La recherche
------------

On effectue une recherche sur un proxy, en utilisant un criteria. Si on ne
spécifie pas le type d'objet retourné, on obtiendra un Wikitty. Sinon, on
obtiendra le type d'objet demandé. Cela permet de s'abstraire complètement de
Wikitty et ne manipuler que des objets métiers.

Les résultats
-------------

Lorsqu'on effectue une recherche, on obtient un PagedResult. En effet, les
résultats étant paginés, on obtient tout ou partie des résultats.
Un PagedResult contient le nombre total de résultats, les résultats de la page
ainsi que le premier index, le nombre de résultats et les facettes si elles ont
été utilisées pour la recherche.
