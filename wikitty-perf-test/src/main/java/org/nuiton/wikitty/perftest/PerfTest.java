/*
 * #%L
 * Wikitty :: wikitty-perf-test
 * %%
 * Copyright (C) 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.perftest;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyConfig;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyServiceFactory;
import org.nuiton.wikitty.entities.ExtensionFactory;
import org.nuiton.wikitty.entities.WikittyTypes;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.nuiton.config.ApplicationConfig;

public class PerfTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(PerfTest.class);

    public static void main(String[] args) {
        ApplicationConfig config = WikittyConfig.getConfig(args);
        int threadsNb = 20;
        int researches = 2001;
        int loopsToAvg = 5;
        int wikittiesToCreate = 70;
        if (threadsNb > researches) {
            threadsNb = researches;
        }
        boolean skipSearchesTests = false;
        boolean skipLabelsTests = false;

        WikittyService ws = WikittyServiceFactory.buildWikittyService(config);
        WikittyProxy proxy = new WikittyProxy(ws);
        WikittyExtension ext = ExtensionFactory.create("perfTest", "1")
            .addField("name", WikittyTypes.STRING).addField("surname", WikittyTypes.STRING)
            .addField("age", WikittyTypes.NUMERIC).addField("height", WikittyTypes.NUMERIC)
            .addField("birth", WikittyTypes.DATE).addField("wedding", WikittyTypes.DATE)
            .addField("isTrue", WikittyTypes.BOOLEAN).addField("isFalse", WikittyTypes.BOOLEAN)
            .addField("wiki1", WikittyTypes.WIKITTY).addField("wiki2", WikittyTypes.WIKITTY).extension();

        Random rand = new Random();
        List<String> ids = new ArrayList<String>();
        for (int i = 0; i < wikittiesToCreate; i++) {
            Wikitty w = new WikittyImpl();
            ids.add(w.getId());
            w.addExtension(ext);
            w.setField(ext.getName(), "name", RandomStringUtils.randomAscii(15));
            w.setField(ext.getName(), "surname", RandomStringUtils.randomAscii(20));
            w.setField(ext.getName(), "age", rand.nextInt(110));
            w.setField(ext.getName(), "height", rand.nextInt(230));
            w.setField(ext.getName(), "isTrue", rand.nextInt(1) == 0);
            w.setField(ext.getName(), "isFalse", rand.nextInt(1) == 1);
            w.setField(ext.getName(), "birth", "01/01/" + rand.nextInt(2010));
            w.setField(ext.getName(), "wiki1", w);
            proxy.store(w);
        }

        PerfTestUtils.out("Tests on searches :");
        if (!skipSearchesTests) {
            PerfTestUtils.out("== Tests on fields of type STRING ==");
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "name", "toto", true);
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "name", "toto", false);

            PerfTestUtils.out("------------\n");
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "surname", "a", true);
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "surname", "a", false);

            PerfTestUtils.out("== Tests on fields of type NUMERIC ==");
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "age", "40", true);
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "age", "40", false);

            PerfTestUtils.out("------------\n");
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "height", "100", true);
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "height", "100", false);

            PerfTestUtils.out("== Tests on a field of type BOOLEAN ==");
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "isTrue", "true", true);
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "isTrue", "true", false);

            PerfTestUtils.out("== Tests on a field of type DATE ==");
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "birth", "06/07/1720", true);
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "birth", "06/07/1720", false);

            PerfTestUtils.out("== Tests on a field of type WIKITTY ==");
            String wikittySearched = proxy.restore(ids.get(rand.nextInt(wikittiesToCreate))).toString();
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "wiki1", wikittySearched, true);
            PerfTestSearch.search(ws, ext, loopsToAvg, researches, wikittiesToCreate, threadsNb, "wiki1", wikittySearched, false);
        } else {
            PerfTestUtils.out("Skipped");
        }

        PerfTestUtils.out("Tests on labels :");
        if (!skipLabelsTests) {
            PerfTestLabelAdd.addLabel(proxy, ids, loopsToAvg, threadsNb);
            PerfTestLabelSearch.searchLabel(proxy, ids, loopsToAvg, threadsNb);
        } else {
            PerfTestUtils.out("Skipped");
        }
    }
}
