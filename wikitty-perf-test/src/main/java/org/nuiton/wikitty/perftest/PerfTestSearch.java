/*
 * #%L
 * Wikitty :: wikitty-perf-test
 * %%
 * Copyright (C) 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.perftest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;
import org.nuiton.wikitty.services.WikittyServiceEnhanced;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PerfTestSearch {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(PerfTestSearch.class);

    private static class PerfTestSearchThread extends Thread {

        private int researches;
        private WikittyService ws;
        private WikittyExtension ext;
        private boolean withRestore;
        private String searchIn;
        private String searched;

        PerfTestSearchThread(int researches, WikittyService ws, WikittyExtension ext, boolean withRestore, String searchIn, String searched) {
            this.researches = researches;
            this.ws = ws;
            this.ext = ext;
            this.withRestore = withRestore;
            this.searchIn = searchIn;
            this.searched = searched;
        }

        @Override
        public void run() {
            searchGt(researches, ws, ext, searchIn, searched, withRestore);
        }
    }

    /**
     * Searches everything greater than the value `searched` in the field `searchIn`
     * 
     * @param researches : the number of researches to do
     * @param ws : the wikitty service
     * @param ext : the wikitty extension
     * @param searchIn : the field name
     * @param searched : the field value to search
     * @param withRestore : if the wikitty will be restored after the search
     */
    private static void searchGt(int researches, WikittyService ws, WikittyExtension ext, String searchIn, String searched, boolean withRestore) {
        for (int i = 0; i < researches; i++) {
            Criteria criteria = Search.query().gt(ext.getName() + "." + searchIn, searched).criteria();
            List<PagedResult<String>> result = ws.findAllByCriteria(null, Collections.singletonList(criteria));
            List<String> found = result.get(0).getAll();
            for (String aFound : found) {
                if (withRestore) {
                    Wikitty wikittyFound = WikittyServiceEnhanced.restore(ws, null, aFound);
                    wikittyFound.getFieldAsString(ext.getName(), searchIn);
                }
            }
        }
    }

    /**
     * Searches with one thread and then displays the result
     * 
     * @param ws : the wikitty service
     * @param ext : the wikitty extension
     * @param loopsToAvg : the number of times the labels searches will be made in order to give a more accurate result
     * @param researches : the number of researches
     * @param wikittiesCreated : the number of wikitties in the proxy
     * @param searchIn : the field name
     * @param searched : the field value to search
     * @param withRestore : if the wikitty will be restored after the search 
     */
    public static void oneThreadSearch(WikittyService ws, WikittyExtension ext, int loopsToAvg, int researches, 
	    int wikittiesCreated, String searchIn, String searched, boolean withRestore) {

        long time = System.currentTimeMillis();

        for (int i = 0; i < loopsToAvg; i++) {
            searchGt(researches, ws, ext, searchIn, searched, withRestore);
        }
        time = System.currentTimeMillis() - time;
        if (withRestore) {
            PerfTestUtils.out("With WikittyServiceEnhanced.restore : ");
        } else {
            PerfTestUtils.out("Without WikittyServiceEnhanced.restore : ");
        }
        PerfTestUtils.out("1 thread took (average on " + loopsToAvg + " loops) " + (time / loopsToAvg) + " ms to do " + researches +
            " researches on field " + searchIn + " in " + wikittiesCreated + " wikitties\n");
    }

    /**
     * Searches with n threads and then displays the result 
     * 
     * @param ws : the wikitty service
     * @param ext : the wikitty extension
     * @param loopsToAvg : the number of times the labels searches will be made in order to give a more accurate result
     * @param researches : the number of researches
     * @param wikittiesCreated : the number of wikitties in the proxy
     * @param threadsNb : the number of threads that will be created to do a part of the search
     * @param searchIn : the field name
     * @param searched : the field value to search
     * @param withRestore : if the wikitty will be restored after the search 
     */
    public static void nThreadsSearch(WikittyService ws, WikittyExtension ext, int loopsToAvg, int researches,
	    int wikittiesCreated, int threadsNb, String searchIn, String searched, boolean withRestore) {

        List<PerfTestSearchThread> threads = new ArrayList<PerfTestSearchThread>();
        long time = System.currentTimeMillis();
        int threadMod = ((researches % threadsNb) != 0 ? 1 : 0);

        for (int j = 0; j < loopsToAvg; j++) {
            for (int i = 0; i < threadsNb; i++) {
                threads.add(new PerfTestSearchThread(researches / threadsNb, ws, ext, withRestore, searchIn, searched));
            }
            if (threadMod == 1) {
                threads.add(new PerfTestSearchThread(researches % threadsNb, ws, ext, withRestore, searchIn, searched));
            }
            for (int i = 0; i < (threadsNb + threadMod); i++) {
                threads.get(i).start();
            }
            for (int i = 0; i < (threadsNb + threadMod); i++) {
                try {
                    threads.get(i).join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            threads.clear();
        }
        time = System.currentTimeMillis() - time;
        if (withRestore) {
            PerfTestUtils.out("With WikittyServiceEnhanced.restore : ");
        } else {
            PerfTestUtils.out("Without WikittyServiceEnhanced.restore : ");
        }
        PerfTestUtils.out(threadsNb + (threadMod == 1 ? "(+1)" : "") + " threads took (average on " + loopsToAvg + " loops) " + (time / loopsToAvg) +
            " ms to do " + (researches / threadsNb) + " researches each " + (threadMod == 1 ? "(+" + (researches % threadsNb) + " researches) " : "") +
            "on field " + searchIn + " in " + wikittiesCreated + " wikitties\n");
    }
    
    /**
     * Launches the searches with one thread and then with n threads
     * 
     * @param ws : the wikitty service
     * @param ext : the wikitty extension
     * @param loopsToAvg : the number of times the labels searches will be made in order to give a more accurate result
     * @param researches : the number of researches
     * @param wikittiesCreated : the number of wikitties in the proxy
     * @param threadsNb : the number of threads that will be created to do a part of the search
     * @param searchIn : the field name
     * @param searched : the field value to search
     * @param withRestore : if the wikitty will be restored after the search 
     */
    public static void search(WikittyService ws, WikittyExtension ext, int loopsToAvg, int researches,
	    int wikittiesCreated, int threadsNb, String searchIn, String searched, boolean withRestore) {

        oneThreadSearch(ws, ext, loopsToAvg, researches, wikittiesCreated, searchIn, searched, withRestore);
        nThreadsSearch(ws, ext, loopsToAvg, researches, wikittiesCreated, threadsNb, searchIn, searched, withRestore);
    }
}
