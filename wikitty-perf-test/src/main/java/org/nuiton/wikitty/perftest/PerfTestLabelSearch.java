/*
 * #%L
 * Wikitty :: wikitty-perf-test
 * %%
 * Copyright (C) 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.perftest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.addons.WikittyLabelUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PerfTestLabelSearch {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(PerfTestLabelSearch.class);

    private static class PerfTestLabelSearchThread extends Thread {
        private int labelsToSearch;
        private WikittyProxy proxy;
        private List<String> ids;
        private int startAt;

        /**
         *
         * @param labelsToSearch
         * @param proxy
         * @param ids
         * @param startAt
         */
        PerfTestLabelSearchThread(int labelsToSearch, WikittyProxy proxy, List<String> ids, int startAt) {
            this.labelsToSearch = labelsToSearch;
            this.proxy = proxy;
            this.ids = ids;
            this.startAt = startAt;
        }

        @Override
        public void run() {
            Set<String> labels = new HashSet<String>();

            for (int i = startAt; i < (labelsToSearch + startAt); i++) {
                labels.addAll(WikittyLabelUtil.findAllAppliedLabels(proxy, ids.get(i)));
            }
            for (String label : labels) {
                WikittyLabelUtil.findAllByLabel(proxy, label, startAt, labelsToSearch + startAt);
            }
        }
    }

    /**
     * Searches all labels applied to a wikitty and then retrieves the wikitties associated with these labels
     * 
     * @param proxy : the WikittyProxy to search in
     * @param ids : the wikitties ids to search labels on
     * @param loopsToAvg : the number of times the searches will be made in order to give a more accurate result
     * @param threadsNb : the number of threads that will be created to do a part of the search
     */
    public static void searchLabel(WikittyProxy proxy, List<String> ids, int loopsToAvg, int threadsNb) {
        long time = System.currentTimeMillis();
        int wikittiesCreated = ids.size();
        Set<String> labels = new HashSet<String>();

        for (int k = 0; k < loopsToAvg; k++) {
            for (String id : ids) {
                labels.addAll(WikittyLabelUtil.findAllAppliedLabels(proxy, id));
            }
            int size = labels.size();

            for (String label : labels) {
                WikittyLabelUtil.findAllByLabel(proxy, label, 0, size);
            }
        }
        time = System.currentTimeMillis() - time;
        PerfTestUtils.out("1 thread took (average on " + loopsToAvg + " loops) " +
                (time / loopsToAvg) + " ms to find " + labels.size() +
                " labels on " + wikittiesCreated + " wikitties\n");

        List<PerfTestLabelSearchThread> threads = new ArrayList<PerfTestLabelSearchThread>();

        time = System.currentTimeMillis();
        int threadMod = ((labels.size() % threadsNb) != 0 ? 1 : 0);

        for (int j = 0; j < loopsToAvg; j++) {
            for (int i = 0; i < threadsNb; i++) {
                threads.add(new PerfTestLabelSearchThread(wikittiesCreated / threadsNb, proxy, ids, i * (wikittiesCreated / threadsNb)));
            }
            if (threadMod == 1) {
                threads.add(new PerfTestLabelSearchThread(wikittiesCreated % threadsNb, proxy, ids, threadsNb * (wikittiesCreated / threadsNb)));
            }
            for (int i = 0; i < (threadsNb + threadMod); i++) {
                threads.get(i).start();
            }
            for (int i = 0; i < (threadsNb + threadMod); i++) {
                try {
                    threads.get(i).join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            threads.clear();
        }
        time = System.currentTimeMillis() - time;
        PerfTestUtils.out(threadsNb + (threadMod == 1 ? "(+1)" : "") + " threads took (average on " + loopsToAvg + " loops) " +
            (time / loopsToAvg) + " ms to find " + (labels.size() / threadsNb) + " labels each " +
            (threadMod == 1 ? "(+" + (labels.size() % threadsNb) + " labels)" : "") + " on " + wikittiesCreated + " wikitties\n");
    }
}
