/*
 * #%L
 * Wikitty :: wikitty-perf-test
 * %%
 * Copyright (C) 2010 CodeLutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.wikitty.perftest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.addons.WikittyLabelUtil;

import java.util.ArrayList;
import java.util.List;

public class PerfTestLabelAdd {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(PerfTestLabelAdd.class);

    private static class PerfTestLabelAddThread extends Thread {
        private int labelsToAdd;
        private WikittyProxy proxy;
        private List<String> ids;
        private int startAt;

        /**
         *
         * @param labelsToAdd
         * @param proxy
         * @param ids
         * @param startAt
         */
        PerfTestLabelAddThread(int labelsToAdd, WikittyProxy proxy, List<String> ids, int startAt) {
            this.labelsToAdd = labelsToAdd;
            this.proxy = proxy;
            this.ids = ids;
            this.startAt = startAt;
        }

        @Override
        public void run() {
            long time = System.currentTimeMillis();

            for (int i = startAt; i < (labelsToAdd + startAt); i++) {
                WikittyLabelUtil.addLabel(proxy, ids.get(i), ids.get(i) + time);
            }
        }
    }

    /**
     * Adds labels on the wikitties ids and stores them in the proxy
     * 
     * @param proxy : the WikittyProxy to add labels in
     * @param ids : the wikitties ids to add labels on
     * @param loopsToAvg : the number of times the labels will be added in order to give a more accurate result
     * @param threadsNb : the number of threads that will be created to add a part of the labels
     */
    public static void addLabel(WikittyProxy proxy, List<String> ids, int loopsToAvg, int threadsNb) {
        long time = System.currentTimeMillis();
        int wikittiesToCreate = ids.size();

        for (int k = 0; k < loopsToAvg; k++) {
            for (String id : ids) {
                WikittyLabelUtil.addLabel(proxy, id, id + time);
            }
        }
        time = System.currentTimeMillis() - time;
        PerfTestUtils.out("1 thread took (average on " + loopsToAvg + " loops) " +
                (time / loopsToAvg) + " ms to add " + wikittiesToCreate +
                " labels on " + wikittiesToCreate + " wikitties\n");

        List<PerfTestLabelAddThread> threads = new ArrayList<PerfTestLabelAddThread>();

        time = System.currentTimeMillis();
        int threadMod = ((wikittiesToCreate % threadsNb) != 0 ? 1 : 0);

        for (int j = 0; j < loopsToAvg; j++) {
            for (int i = 0; i < threadsNb; i++) {
                threads.add(new PerfTestLabelAddThread(wikittiesToCreate / threadsNb, proxy, ids, i * (wikittiesToCreate / threadsNb)));
            }
            if (threadMod == 1) {
                threads.add(new PerfTestLabelAddThread(wikittiesToCreate % threadsNb, proxy, ids, threadsNb * (wikittiesToCreate / threadsNb)));
            }
            for (int i = 0; i < (threadsNb + threadMod); i++) {
                threads.get(i).start();
            }
            for (int i = 0; i < (threadsNb + threadMod); i++) {
                try {
                    threads.get(i).join();
                } catch (InterruptedException eee) {
                    // FIXME sletellier 21/12/10 : add some msg
                    log.error(eee);
                }
            }
            threads.clear();
        }
        time = System.currentTimeMillis() - time;
        PerfTestUtils.out(threadsNb + (threadMod == 1 ? "(+1)" : "") + " threads took (average on " + loopsToAvg + " loops) " + (time / loopsToAvg) +
            " ms to add " + (wikittiesToCreate / threadsNb) + " labels each " + (threadMod == 1 ? "(+" + (wikittiesToCreate % threadsNb) +
                " labels) " : "") + "on " + wikittiesToCreate + " wikitties\n");
    }
}
